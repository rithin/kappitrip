/*----------------------------------------------------*/
/*  Quantity Buttons with Total Value Counter
/*
/*  Author: Vasterad
/*  Version: 1.0
/*----------------------------------------------------*/

function qtySum(){
    var arr = $('.qtyInput');
        //document.getElementsById('qtyInput');
    var tot=0;
    for(var i=0;i<arr.length;i++){
        if(parseInt(arr[i].value))
            tot += parseInt(arr[i].value);
    }
    if(tot>1){
        if(isOdd(tot)==1){
           // console.log('odd no');
          //  $("#ExtraRoom").attr("style","display:block;")
        }else{
            //console.log('even no');
           // $("#ExtraRoom").attr("style","display:none;")
        }
    }
    
    var cardQty = document.querySelector(".qtyTotal");
    cardQty.innerHTML = tot;
} 
qtySum();
function isOdd(num) { return num % 2; }
$(function() {

   $(".qtyButtons input").after('<div class="qtyInc"></div>');
   $(".qtyButtons input").before('<div class="qtyDec"></div>');

   $(".qtyDec, .qtyInc").on("click", function() {

      var $button = $(this);
      var maxAdultCnt=document.getElementById("adultCount").max;
      var maxInfantCnt=document.getElementById("infantCount").max;
      var maxChildCnt=document.getElementById("childCount").max;
      var maxAllowedCnt=document.getElementById("maxAllowedCount").value;
      var oldValue = $button.parent().find("input").val();
       
      var clickedItem=$button.parent().find("input").attr('id');
     
      var currentAdultCount=$('#adultCount').val();
      var currentChildCount=$('#childCount').val();
      var totalCurrentCount =(parseFloat(currentAdultCount)+parseFloat(currentChildCount));
      if ($button.hasClass('qtyInc')) {
          
          if(parseFloat(oldValue) < parseFloat(maxAdultCnt) &&  (clickedItem=="adultCount") && (parseFloat(totalCurrentCount)<= parseFloat(maxAllowedCnt)))
            var newVal= parseFloat(oldValue) + 1;
          else if(parseFloat(oldValue) < parseFloat(maxInfantCnt) &&  (clickedItem=="infantCount")  )
             var newVal= parseFloat(oldValue) + 1;
          else if(parseFloat(oldValue) < parseFloat(maxChildCnt) &&  (clickedItem=="childCount") && (parseFloat(totalCurrentCount)<=parseFloat(maxAllowedCnt)))
             var newVal= parseFloat(oldValue) + 1;
          
          // console.log("new val===>"+newVal);
      } else if ($button.hasClass('qtyDec')) {
           var newVal =0;
         // don't allow decrementing below zero
          if (parseFloat(oldValue) > 0) {
            var newVal = parseFloat(oldValue) - 1;
          } else {
            newVal = 0;
          }          
         
      }
      if(parseFloat(newVal)>=0){
          $button.parent().find("input").val(newVal);
          qtySum();
          $(".qtyTotal").addClass("rotate-x");
      }

   });

   // Total Value Counter Animation
   function removeAnimation() { $(".qtyTotal").removeClass("rotate-x"); }
        const counter = document.querySelector(".qtyTotal");
        counter.addEventListener("animationend", removeAnimation);

});

// Adjusting Panel Dropdown Width
$(window).on('load resize', function() {
   var panelTrigger = $('.booking-widget .panel-dropdown a');
   $('.booking-widget .panel-dropdown .panel-dropdown-content').css({
      'width' : panelTrigger.outerWidth()
   });
});