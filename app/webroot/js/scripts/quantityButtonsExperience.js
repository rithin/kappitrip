/*----------------------------------------------------*/
/*  Quantity Buttons with Total Value Counter
/*
/*  Author: Vasterad
/*  Version: 1.0
/*----------------------------------------------------*/

function qtySum(){
    var arr = $('.qtyInput');
        //document.getElementsById('qtyInput');
    var tot=0;
    for(var i=0;i<arr.length;i++){
        if(parseInt(arr[i].value))
            tot += parseInt(arr[i].value);
    }
    if(tot>1){
//        if(isOdd(tot)==1){
//           // console.log('odd no');
//            $("#ExtraRoom").attr("style","display:block;")
//        }else{
//            //console.log('even no');
//            $("#ExtraRoom").attr("style","display:none;")
//        }
    }
   
	var cardQty = document.querySelector(".qtyTotal");	
	cardQty.innerHTML = tot;
	
} 
qtySum();
function isOdd(num) { return num % 2; }
$(function() {

   $(".qtyButtons input").after('<div class="qtyInc"></div>');
   $(".qtyButtons input").before('<div class="qtyDec"></div>');

   $(".qtyDec, .qtyInc").on("click", function() {

      var $button = $(this);
      var maxAdultCnt=document.getElementById("adultCount").max;
      var maxAllowedCnt=document.getElementById("maxAllowedCount").value;
      var oldValue = $button.parent().find("input").val();
//console.log("oldval==>"+oldValue);
      
      var clickedItem=$button.parent().find("input").attr('id');
     
      var currentAdultCount=$('#adultCount').val();
     
      var totalCurrentCount =currentAdultCount;
      if ($button.hasClass('qtyInc')) {
      //  console.log("oldval==>"+oldValue+" "+"maxAdultCnt==>"+maxAdultCnt+" "+"totalCurrentCount==>"+totalCurrentCount+" "+"maxAllowedCnt==>"+maxAllowedCnt+"  >>"+clickedItem);
		  
          if( (parseFloat(oldValue) <  parseFloat(maxAdultCnt)) &&  (clickedItem=="adultCount") && ( parseFloat(totalCurrentCount)<=  parseFloat(maxAllowedCnt)) )
            var newVal= parseFloat(oldValue) + 1;
        
           console.log(newVal);
          
      } else if ($button.hasClass('qtyDec')) {
           var newVal =0;
         // don't allow decrementing below zero
         if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
         } else {
            newVal = 0;
         }
      }
        if(newVal>0){
              $button.parent().find("input").val(newVal);
              qtySum();
              $(".qtyTotal").addClass("rotate-x");
        }

   });

   // Total Value Counter Animation
   function removeAnimation() { $(".qtyTotal").removeClass("rotate-x"); }

   const counter = document.querySelector(".qtyTotal");
   counter.addEventListener("animationend", removeAnimation);

});

// Adjusting Panel Dropdown Width
$(window).on('load resize', function() {
   var panelTrigger = $('.booking-widget .panel-dropdown a');
   $('.booking-widget .panel-dropdown .panel-dropdown-content').css({
      'width' : panelTrigger.outerWidth()
   });
});