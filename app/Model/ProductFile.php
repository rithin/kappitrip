<?php
App::uses('AuthComponent', 'Controller/Component');

class ProductFile extends AppModel {

	 public $name = 'ProductFile';
	 public $useTable = 'product_files';
     public $belongsTo = array(
        'Product' => array(
            'className' => 'Product',
            'foreignKey' => 'product_id'
        )
     );

}
?>
