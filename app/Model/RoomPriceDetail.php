<?php 
App::uses('AuthComponent', 'Controller/Component');
class RoomPriceDetail extends AppModel {
	
	 public $name = 'room_price_details'; 
     public $belongsTo = array(
        'Homestay' => array(
            'className' => 'Homestay',
            'foreignKey' => 'homestay_id'
        ),
         'RoomType' => array(
            'className' => 'RoomType',
            'foreignKey' => 'room_type_id'
        ),
      );
    
  
    public function beforeSave($options = array()) {
    	
    	return true;
    }
}
?>