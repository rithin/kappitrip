<?php
App::uses('AppModel', 'Model');

class ExperienceLanguage extends AppModel
{
	public $name = 'ExperienceLanguage';
 	public $useTable = 'experience_languages';
    public $belongsTo = array(
        'Experience' => array(
            'className' => 'Experience',
            'foreignKey' => 'experience_id'
        )
     );

}
?>
