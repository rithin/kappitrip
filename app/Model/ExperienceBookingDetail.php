<?php
App::uses('AppModel', 'Model');

class ExperienceBookingDetail extends AppModel
{
	public $name = 'ExperienceBookingDetail';
    public $useTable = 'experience_booking_details';
    public $belongsTo = array(
        'Experience' => array(
            'className' => 'Experience',
            'foreignKey' => 'experience_id'
        )
     );
	
	
}
?>
