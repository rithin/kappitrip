<?php
App::uses('AuthComponent', 'Controller/Component');

class Product extends AppModel {

	 public $name = 'Product';
	 public $useTable = 'products';
     public $hasMany = array(
					  'ProductFile'=> array(
					  'className' => 'ProductFile',
					  'foreignKey' => 'product_id'
				  ),  
                    
	   );
    

}
?>
