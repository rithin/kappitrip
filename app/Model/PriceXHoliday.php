<?php 
App::uses('AuthComponent', 'Controller/Component');
class PriceXHoliday extends AppModel {
	
	public $name = 'PriceXHoliday'; 
	public $useTable = 'price_x_holiday';
	public $belongsTo = array(
        'PriceX' => array(
            'className' => 'PriceX',
            'foreignKey' => 'price_x_id'
        )
     );
}
?>