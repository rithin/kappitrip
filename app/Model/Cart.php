<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');
 
class Cart extends AppModel {
 
    public $useTable = false;
    public function addProduct($productId,$numOfQty=null) {
        $allProducts = $this->readProduct();
        //debug($allProducts);
        //debug($productId);
        //debug($numOfQty);
        if (null!=$allProducts) {
            if (array_key_exists($productId, $allProducts)) {
                if($numOfQty>0){
                    for($i=1;$i<=$numOfQty;$i++){
                        $allProducts[$productId]++;
                    }

                }else{
                    $allProducts[$productId]++;
                }
            } else {
                $allProducts[$productId] = 1;
            }
        } else {
           
            
            $allProducts[$productId] = $numOfQty;
        }
         
        $this->saveProduct($allProducts);
    }
    
    
    public function deleteProduct($productId,$numOfQty=null) {
        $allProducts = $this->readProduct();
       
        if (null!=$allProducts) {
            if (array_key_exists($productId, $allProducts)) {
                unset($allProducts[$productId]);
            } 
        } 
         
        $this->saveProduct($allProducts);
    }
     
    /*
     * get total count of products
     */
  public function getCount() {
        $allProducts = $this->readProduct();
         
        if (count($allProducts)<1) {
            return 0;
        }
         
        $count = 0;
        foreach ($allProducts as $product) {
            $count=$count+$product;
        }
         
        return $count;
    }
 
    /*
     * save data to session
     */
    public function saveProduct($data) {
        return CakeSession::write('cart',$data);
        
        //debug(CakeSession::read('cart'));
            //die;
    }
 
    /*
     * read cart data from session
     */
    public function readProduct() {
        return CakeSession::read('cart');
    }
    
    public function clearCart(){
       
        CakeSession::delete('cart');
    }
 
}