<?php
App::uses('AuthComponent', 'Controller/Component');

class HomestayAmenity extends AppModel {

	 public $name = 'HomestayAmenity';
	 public $useTable = 'homestay_amenities';
    
    public $belongsTo = array(
        'Homestay' => array(
            'className' => 'Homestay',
            'foreignKey' => 'homestay_id'
        ),
        'Amenity' => array(
            'className' => 'Amenity',
            'foreignKey' => 'amenity_id'
        )
     );

}
?>
