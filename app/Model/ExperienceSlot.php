<?php
App::uses('AppModel', 'Model');

class ExperienceSlot extends AppModel
{
	public $name = 'ExperienceSlot';
    public $useTable = 'experience_slots';
    public $belongsTo = array(
        'Experience' => array(
            'className' => 'Experience',
            'foreignKey' => 'experience_id'
        )
     );
	
	 var $virtualFields = array(
           'no_of_slots' => 'COUNT(ExperienceSlot.experience_id)'
        );
}
?>
