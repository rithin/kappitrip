<?php 
App::uses('AuthComponent', 'Controller/Component');
class Homestay extends AppModel {
	
	 public $name = 'Homestay'; 
	 public $hasMany = array(
					  'HomestayFile'=> array(
					  'className' => 'HomestayFile',
					  'foreignKey' => 'homestay_id'
				  ),  
                    'HomestayAmenity'=> array(
					  'className' => 'HomestayAmenity',
					  'foreignKey' => 'homestay_id'
				  ),  
                    'RoomPriceDetail'=> array(
					  'className' => 'RoomPriceDetail',
					  'foreignKey' => 'homestay_id'
				  ), 
	   );
    
    
    
     public $validate = array(


         );

    public function beforeSave($options = array()) {
    	
    	return true;
    }
}
?>