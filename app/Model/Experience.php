<?php 
App::uses('AuthComponent', 'Controller/Component');
class Experience extends AppModel {
	
 public $name = 'Experience'; 
 public $hasMany = array(
					  'ExperienceFile'=> array(
					  'className' => 'ExperienceFile',
					  'foreignKey' => 'experience_id'
				  ),  
                    'ExperienceSlot'=> array(
					  'className' => 'ExperienceSlot',
					  'foreignKey' => 'experience_id'
				  ),  
                    'ExperienceLanguage'=> array(
					  'className' => 'ExperienceLanguage',
					  'foreignKey' => 'experience_id'
				  ), 
	   );
 public $validate = array(

        
    );

public function beforeSave($options = array()) {
    	
    	return true;
    }
}
?>
