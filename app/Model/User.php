<?php 
App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {
	
	 public $name = 'User'; 
//	 public $hasOne = array(
//					  'SupplierDetail'=> array(
//					  'className' => 'SupplierDetail',
//					  'foreignKey' => 'user_id'
//				  ),
//	 );
//         
//         public $hasMany = array(
//					  'WishList'=> array(
//					  'className' => 'WishList',
//					  'foreignKey' => 'user_id'
//				  ),
//             
//                                          'Order'=> array(
//					  'className' => 'Order',
//					  'foreignKey' => 'user_id'
//				  ),
//             
//                                          
//	 );
	 public $validate = array(
//           'username' => array(
//            'notEmpty'=>array(
//                'rule' => 'notBlank',
//                'message' => 'A email is required'
//            ),
//        	'email_idRule-1' => array(
//        		'rule' => 'Email',
//        		'message' => "Please enter valid email address.",
//        		'last' => true
//        	),
//         ),
        'password' => array(
            'notEmpty'=>array(
                'rule' => 'notBlank',
                'message' => 'A password is required'
            )
        ),
        'confirm_password' => array(
            'notEmpty'=>array(
                'rule' => 'notBlank',
                'message' => 'Please confirm your password'
            ),
            'compare'    => array(
                'rule'      => array('validate_passwords'),
                'message' => 'The passwords you entered do not match.',
            )
        )
        
     );
    public function validate_passwords() {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['confirm_password'];
    }
    public function beforeSave($options = array()) {
    	if (isset($this->data[$this->alias]['password'])) { 
    		$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
    	}
    	return true;
    }
}
?>