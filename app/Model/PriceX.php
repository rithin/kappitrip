<?php 
App::uses('AuthComponent', 'Controller/Component');
class PriceX extends AppModel {
	
	public $name = 'PriceX'; 
	public $useTable = 'price_x';
	public $hasMany = array(
			'PriceXSeasonal'=> array(
			'className' => 'PriceXSeasonal',
			'foreignKey' => 'price_x_id'
			 ),
			'PriceXHoliday'=> array(
			'className' => 'PriceXHoliday',
			'foreignKey' => 'price_x_id'
			 )
	   );
}
?>