<?php
App::uses('AuthComponent', 'Controller/Component');

class Amenity extends AppModel {

	 public $name = 'Amenity';
	 public $useTable = 'amenities';
    
     public $hasMany = array(					 
                    'HomestayAmenity'=> array(
					  'className' => 'HomestayAmenity',
					  'foreignKey' => 'amenity_id'
				  ),  
	   );

}
?>
