<?php
App::uses('AuthComponent', 'Controller/Component');

class OccupancyDetail extends AppModel {

	 public $name = 'OccupancyDetail';
	 public $useTable = 'occupancy_details';
    
    
    var $virtualFields = array(
            'no_of_bookings' => 'COUNT(OccupancyDetail.room_price_detail_id)'
        );

}
?>
