<?php
App::uses('AppModel', 'Model');

class ExperienceFile extends AppModel
{
	public $name = 'ExperienceFile';
	public $belongsTo = array(
        'Experience' => array(
            'className' => 'Experience',
            'foreignKey' => 'experience_id'
        )
     );
	
}
?>
