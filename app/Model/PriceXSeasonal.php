<?php 
App::uses('AuthComponent', 'Controller/Component');
class PriceXSeasonal extends AppModel {
	
	public $name = 'PriceXSeasonal'; 
	public $useTable = 'price_x_seasonal';
	
	public $belongsTo = array(
        'PriceX' => array(
            'className' => 'PriceX',
            'foreignKey' => 'price_x_id'
        )
     );

}
?>