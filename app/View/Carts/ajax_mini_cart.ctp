<ul class="header-cart-wrapitem">
<?php  
    $grand_total=0;
    foreach($products as $item) {
    $grand_total +=($item['Product']['count'] * $item['Product']['unit_price']);
    ?>
    <li class="header-cart-item">
        <div class="header-cart-item-img">
            <img src="<?php echo Configure::read('app_root_path').$item['ProductFile']['0']['img_path']."/".$item['ProductFile']['0']['thumbnail_img_name']; ?>" alt="<?php echo $item['Product']['product_name']; ?>">
        </div>

        <div class="header-cart-item-txt">
            <a href="#" class="header-cart-item-name"><?php echo $item['Product']['product_name']; ?></a>

				<span class="header-cart-item-info"><?php echo $item['Product']['count']; ?> x <i class="fa fa-fw">&#xf156;</i><?php echo ($item['Product']['count'] * $item['Product']['unit_price']); ?></span>
        </div>
    </li>
<?php } ?>                          
</ul>

<div class="header-cart-total">	Total: <i class="fa fa-fw">&#xf156;</i> <?php echo $grand_total; ?>
</div>