

	<!-- Title Page -->
<!--
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-01.jpg);">
		<h2 class="l-text2 t-center">
			Cart
		</h2> 
	</section>
-->

<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo Configure::read('app_root_path'); ?>/images/checkout-banner.jpg);">
		<h2 class="l-text2 t-center">
			Cart
		</h2>
		<p class="m-text13 t-center">
			Choose our safe checkout method.
		</p>
	</section>

	<!-- Cart -->
	<section class="cart bgwhite p-t-70 p-b-100">
		<div class="container">
			<!-- Cart item -->
            <?php echo $this->Form->create('Cart',array('url'=>array('action'=>'update')));?>
			<div class="container-table-cart pos-relative">
				<div class="wrap-table-shopping-cart bgwhite">
					<table class="table-shopping-cart">
						<tr class="table-head">
							<th class="column-1"></th>
							<th class="column-2">Product</th>
							<th class="column-3">Price</th>
							<th class="column-4 p-l-70">Quantity</th>
							<th class="column-5">Total</th>
						</tr>
              
                        <?php  
                            $grand_total=0;
                            foreach($products as $item) {
                            $grand_total +=($item['Product']['count'] * $item['Product']['unit_price']);
                        ?>

						<tr class="table-row">
							<td class="column-1">
								<div class="cart-img-product b-rad-4 o-f-hidden">
									<img src="<?php echo Configure::read('app_root_path').$item['ProductFile']['0']['file_path']."/".$item['ProductFile']['0']['file_name']; ?>" alt="<?php echo $item['Product']['product_name']; ?>">
                                     <?php echo $this->Form->hidden('product_id.',array('value'=>$item['Product']['id']));?>
								</div>
							</td>
							<td class="column-2"><?php echo $item['Product']['product_name']; ?></td>
							<td class="column-3"><i class="fa fa-fw">&#xf156;</i><?php echo $item['Product']['unit_price'];?></td>
							<td class="column-4">
								<div class="flex-w bo5 of-hidden w-size17">
									<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
									</button>
                                   
									<input class="size8 m-text18 t-center num-product" type="number" name="data[Cart][count][]" value="<?php echo $item['Product']['count']; ?>">
                               

									<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
							</td>
							<td class="column-5"><?php echo $item['Product']['count']; ?> x <i class="fa fa-fw">&#xf156;</i><?php echo ($item['Product']['count'] * $item['Product']['unit_price']); ?></td>
						</tr>
<?php } ?>  
         
						
					</table>
				</div>
			</div>

			<div class="flex-w flex-sb-m p-t-25 p-b-25 bo8 p-l-35 p-r-60 p-lr-15-sm">
				<div class="flex-w flex-m w-full-sm">
					<div class="size11 bo4 m-r-10">
						<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="coupon-code" placeholder="Coupon Code">
					</div>

					<div class="size12 trans-0-4 m-t-10 m-b-10 m-r-10">
						<!-- Button -->
						<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
							Apply coupon
						</button>
					</div>
				</div>

				<div class="size10 trans-0-4 m-t-10 m-b-10">
					<!-- Button -->
					
                     <?php echo $this->Form->submit('Update Cart',array('class'=>'"flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4','div'=>false));?>
				</div>
			</div>

        <?php echo $this->Form->end(); ?>     
			<!-- Total -->
            <?php echo $this->Form->create('Cart',array('url'=>array('action'=>'make_payment')));
            
             echo $this->Form->input('tid',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tid','autofocus','error'=>false,'value'=>strtotime(date('Y-m-d H:i:s'))));
            
               echo $this->Form->input('merchant_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'merchant_id','autofocus','error'=>false,'value'=>4538));
            
               echo $this->Form->input('order_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'order_id','autofocus','error'=>false,'value'=>strtotime(date('Y-m-d H:i:s'))));  
                
               echo $this->Form->input('currency',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'currency','autofocus','error'=>false,'value'=>'INR')); 
            
               echo $this->Form->input('amount',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'amount','autofocus','error'=>false,'value'=>$grand_total));
            
               echo $this->Form->input('language',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'language','autofocus','error'=>false,'value'=>'EN'));
            
               echo $this->Form->input('redirect_url',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'redirect_url','autofocus','error'=>false,'value'=>'http://localhost/coorgexpress/carts/purchase_response'));
            
               echo $this->Form->input('cancel_url',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'cancel_url','autofocus','error'=>false,'value'=>'http://localhost/coorgexpress/carts/purchase_cancel'));
            
            
        ?>
            <!-- Billing Address Starts -->
            <div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm boxaddress" style="">
				<h5 class="m-text20 p-b-24">
					Billing Address
				</h5>
				
				<div class="flex-w flex-sb bo10 p-t-15 p-b-20">
					<div class="w-size20 w-full-sm">
						
                        <div class="size13 bo4 m-b-12"> 
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Billing][billing_name]" placeholder="Name">
						</div>
                                                                   
                        <div class="size13 bo4 m-b-12">
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Billing][billing_email]" placeholder="Email">
						</div>
                        
                        <div class="size13 bo4 m-b-12"> 
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Billing][billing_tel]" placeholder="Phone Number">
						</div>
                       
                        <div class="size13 bo4 m-b-12">
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Billing][billing_address]" placeholder="Address Line">
						</div>                     
                        
                       
                        <div class="size13 bo4 m-b-12">
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Billing][billing_zip]" placeholder="Postcode">
						</div>                        
                    
						<div class="rs2-select2 rs3-select2 rs4-select2 bo4 of-hidden w-size21 m-t-8 m-b-12">
							<select class="selection-2" name="data[Billing][billing_country]">
								<option>Select a country...</option>
								<option>India</option>								
							</select>
						</div>                        
                      
                        <div class="rs2-select2 rs3-select2 rs4-select2 bo4 of-hidden w-size21 m-t-8 m-b-12">
							<select class="selection-2" name="data[Billing][billing_state]">
								<option>Select a State...</option>
								<option>Karnatak</option>								
							</select>
						</div>
                      
                        <div class="size13 bo4 m-b-12">
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Billing][billing_city]" placeholder="City">
						</div>        
                        
					</div>
				</div>

			
			</div>
             <!-- Billing Address  Ends-->      
            
            <!-- Shipping Address Starts -->
            
            <div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm boxaddress" style="">
				<h5 class="m-text20 p-b-24">
					Shipping Address
				</h5>

				<!--  -->
				<div class="flex-w flex-sb bo10 p-t-15 p-b-20">
					

					<div class="w-size20 w-full-sm">
					
                        <div class="size13 bo4 m-b-12"> 
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Shipping][delivery_name]" placeholder="Name">
						</div>                        
                        
                        <div class="size13 bo4 m-b-12">
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Shipping][delivery_email]" placeholder="Email">
						</div>
                        
                        <div class="size13 bo4 m-b-12"> 
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Shipping][delivery_tel]" placeholder="Contact No">
						</div>
                     
                        <div class="size13 bo4 m-b-12">
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Shipping][delivery_address]" placeholder="Address Line 1">
						</div>
                      
                       
                        <div class="size13 bo4 m-b-12">
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Shipping][delivery_zip]" placeholder="Postcode">
						</div>                        
                   
						<div class="rs2-select2 rs3-select2 rs4-select2 bo4 of-hidden w-size21 m-t-8 m-b-12">
							<select class="selection-2" name="data[Shipping][delivery_country]">
								<option>Select a country...</option>
								<option>India</option>								
							</select>
						</div>                        
                     
                        <div class="rs2-select2 rs3-select2 rs4-select2 bo4 of-hidden w-size21 m-t-8 m-b-12">
							<select class="selection-2" name="data[Shipping][delivery_state]">
								<option>Select a State...</option>
								<option>Karnatak</option>								
							</select>
						</div>
                       
                        <div class="size13 bo4 m-b-12">
						<input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="data[Shipping][delivery_city]" placeholder="City">
						</div>  
					</div>
				</div>

			</div>
            
             <!-- Shipping Address Ends -->
    
             <!-- Checkout Method starts-->
            
			<div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm" style=" max-width:378px; max-height: 580px; ">
				<h5 class="m-text20 p-b-24">
					Cart Totals
				</h5>

				<!--  -->
				<div class="flex-w flex-sb-m p-b-12">
					<span class="s-text18 w-size19 w-full-sm">
						Subtotal:
					</span>

					<span class="m-text21 w-size20 w-full-sm">
						<i class="fa fa-fw">&#xf156;</i> <?php echo $grand_total; ?>
					</span>
				</div>

				<!--  -->
				<div class="flex-w flex-sb bo10 p-t-15 p-b-20">
					<div class="s-text18 w-size19 w-full-sm">
						<div style="float:left;">Shipping:Free</div>
					</div>
                    <br/> <br/><br/> <br/><br/> <br/>
					<div class="w-size20 w-full-sm">
					   <div class="payment-tab">
                                <div class="payment-tab-trigger">
                                    <input type="radio" name="cardType" id="creditCart" value="creditCard">
                                    <label for="creditCart" style="float:left;">Credit / Debit Card / Net Banking (Powered By CCAvenue)</label>
                                    <img class="payment-logo" src="https://i.imgur.com/IHEKLgm.png" alt="" width="120">
                                </div>
                      </div>
					

					</div>
				</div>

				<!--  -->
				<div class="flex-w flex-sb-m p-t-26 p-b-30">
					<span class="m-text22 w-size19 w-full-sm">
						Total:
					</span>

					<span class="m-text21 w-size20 w-full-sm">
						<i class="fa fa-fw">&#xf156;</i> <?php echo $grand_total; ?>
					</span>
				</div>

				<div class="size15 trans-0-4">
					<!-- Button -->
					
                    
                    <?php echo $this->Form->submit('Make Payment',array('class'=>'flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4','div'=>false));?>
				</div>
			</div>
           <!-- Checkout Method Ends-->     
          <?php echo $this->Form->end(); ?>        
            
		</div>
	</section>

<style>

    .boxaddress{
        float:left;
        width: 396px !important;
        min-height: 600px;
    }
    .w-size20{
        width:380px !important;
        max-width:380px !important;
        
    }
    .size13{
        width:380px !important;
        max-width:318px !important;
    }
    

</style>