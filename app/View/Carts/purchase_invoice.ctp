<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Invoice</title>
    <?php echo $this->Html->css('invoice.css'); ?>
</head> 

<body>

<!-- Print Button -->
<a href="javascript:window.print()" class="print-button">Print this invoice</a>

<!-- Invoice -->
<div id="invoice">

	<!-- Header -->
	<div class="row">
		<div class="col-md-6">
			<div id="logo"><img src="images/logo.png" alt=""></div>
		</div>

		<div class="col-md-6">	

			<p id="details">
				<strong>Order:</strong> #<?php echo $paymentResponseDetailArr['ProductPaymentResponse']['order_id']; ?> <br>
				<strong>Issued:</strong> <?php echo date('d-m-Y'); ?> <br>
			
			</p>
		</div>
	</div>


	<!-- Client & Supplier -->
	<div class="row">
		<div class="col-md-12">
			<h2>Invoice</h2>
		</div>

		<div class="col-md-4">	
			<strong class="margin-bottom-5">Supplier</strong>
			<p>
				KappiTrip. <br>
				Rangasamudra,Kushalnagar <br>
				Kodagu- 571234,Karnataka <br>
			</p>
		</div>

		<div class="col-md-4">	
			<strong class="margin-bottom-5">Billing</strong>
			<p>
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['billing_name']); ?><br>
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['billing_address']); ?> <br>
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['billing_city']); ?>, 
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['billing_zip']); ?>, 
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['billing_state']); ?> <br>
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['billing_country']); ?> <br>
			</p>
		</div>
		<div class="col-md-4">	
			<strong class="margin-bottom-5">Shipping</strong>
			<p>
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['delivery_name']); ?><br>
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['delivery_address']); ?> <br>
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['delivery_city']); ?>, 
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['delivery_zip']); ?>, 
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['delivery_state']); ?> <br>
				<?php echo ucfirst($paymentResponseDetailArr['ProductPaymentResponse']['billing_country']); ?> <br>
			</p>
		</div>
		
	</div>


	<!-- Invoice -->
	<div class="row">
		<div class="col-md-12">
			<table class="margin-top-20">
				<tr>
					<th>Product</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Total</th>
				</tr>
        <?php  
            $grand_total=0;
            foreach($productOrderDetailArr as $item) {
            $grand_total +=($item['ProductOrderDetail']['quantity'] * $item['ProductOrderDetail']['unit_price']);
          ?>
				<tr>
					<td><?php echo $item['Product']['product_name']; ?></td> 
					<td>Rs.<?php echo $item['ProductOrderDetail']['unit_price'];?></td>
					<td><?php echo $item['ProductOrderDetail']['quantity']; ?></td>
					<td><?php echo $item['ProductOrderDetail']['quantity']; ?> x Rs.<?php echo ($item['ProductOrderDetail']['quantity']* $item['ProductOrderDetail']['unit_price']); ?></td>
				</tr>
        <?php } ?>         
        
			</table>
		</div>
		
		<div class="col-md-4 col-md-offset-8">	
			<table id="totals">
				<tr>
					<th>Total Amount Paid</th> 
					<th><span>INR <?php echo $grand_total; ?></span></th>
				</tr>
			</table>
		</div>
	</div>


	<!-- Footer -->
	<div class="row">
		<div class="col-md-12">
			<ul id="footer">
				<li><span><?php echo Configure::read('domain_name'); ?></span></li>
				<li><?php echo Configure::read('enquiry_email'); ?></li>
<!--				<li>(123) 123-456</li>-->
			</ul>
		</div>
	</div>
		
</div>


</body>
</html>
