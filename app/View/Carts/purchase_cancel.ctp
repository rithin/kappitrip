

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Purchase Cancelled</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>/">Home</a></li>
						<li>Purchase Cancelled</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<div class="booking-confirmation-page">
				<i class="fa fa-check-circle"></i>
				<h2 class="margin-top-30">Payment has been cancelled. Please try again!</h2>
				<p>You'll receive a cancellation email at <?php echo $billing_email;?></p>
				
			</div>

		</div>
	</div>
</div>
<!-- Container / End -->
