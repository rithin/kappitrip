
<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Checkout</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Checkout</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->


<section class="cart-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>shoping cart</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						 <?php echo $this->Form->create('Cart',array('url'=>array('action'=>'update')));?>
						<div class="wrap-cart">
						<?php  
                                $grand_total=0;

                                if($cart_count>0){

                                foreach($products as $item) {
                                $grand_total +=($item['Product']['count'] * $item['Product']['unit_price']);
                                ?>
							
							<div class="cart-card">
								<img src="<?php echo Configure::read('app_root_path').$item['ProductFile']['0']['img_path']."/".$item['ProductFile']['0']['thumbnail_img_name']; ?>" alt="<?php echo $item['Product']['product_name']; ?>"  width="100" height="109">
                                     <?php echo $this->Form->hidden('product_id.',array('value'=>$item['Product']['id']));?>
								<div class="cart-card-info">
									<div class="cart-card-name">
										<p class="h4"><?php echo $item['Product']['product_name']; ?></p>
										<p><span>sku: </span><?php echo $item['Product']['sku']; ?></p>
										<p class="availability"><i class="fa fa-check" aria-hidden="true"></i> In Stock</p>
									</div>
									<div class="cart-card-price">
										<p class="once-price"><i class="fa fa-fw">&#xf156;</i><?php echo $item['Product']['unit_price'];?></p>
									</div>
                                                                       
									<div class="cart-card-quantity">
										<div class="quantity">
											<div class="inner-quantity">
												<p>Quantity</p>
												<div class="counters">
													<i class="fa fa-caret-left decrement" aria-hidden="true"></i>
													<input class="count" name="data[Cart][count][]" value="<?php echo $item['Product']['count']; ?>">
													<i class="fa fa-caret-right increment" aria-hidden="true"></i>
												</div>
											</div>
										</div>
									</div>
									<div class="cart-card-all-prise">
										<p class="all-price"> <i class="fa fa-fw">&#xf156;</i><?php echo ($item['Product']['count'] * $item['Product']['unit_price']); ?></p>
									</div>
									
								<div class="">
<!--									product-control-buttons-->
									<!--<span><i class="fa fa-pencil" aria-hidden="true"></i></span>-->
									<span title="Delete" class="delete-product" onclick="deleteProduct('<?php echo $item['Product']['id'];?>')"><i class="fa fa-times" aria-hidden="true"></i></span>
								</div>
                                                                        
								</div>
								
							</div>
						<?php } 

                                         }else{

                                            echo 'Your Shopping Cart is empty';

                                         }

                                         ?> 	
						
					 </div>

					 <div class="cart-control-buttons">
							<a href="<?php echo Configure::read('app_root_path'); ?>shop" class="continue-shopping"><i class="fa fa-arrow-left" aria-hidden="true"></i> continue shopping</a>
<!--							<a href="#" class="button">Clear  cart</a>-->
							<button type="submit" class="button">Update cart</button>
					 </div>
						
					<?php echo $this->Form->end(); ?>     	
						
						
                                        <!-- ============================== SHIPPING		================================ -->
					<div class="col-lg-12 col-md-12 padding-right-10 padding-top-20" id="cart_shipping_div">
					 <?php   
						
						$order_total=($grand_total+$shipping_charge);
						 echo $this->Form->create('Cart',array('url'=>array('action'=>'confirm_purchase')));

						 echo $this->Form->input('tid',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tid','autofocus','error'=>false,'value'=>strtotime(date('Y-m-d H:i:s'))));

						   echo $this->Form->input('merchant_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'merchant_id','autofocus','error'=>false,'value'=>4538));

						   echo $this->Form->input('order_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'order_id','autofocus','error'=>false,'value'=>strtotime(date('Y-m-d H:i:s'))));  

						   echo $this->Form->input('currency',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'currency','autofocus','error'=>false,'value'=>'INR')); 

						   echo $this->Form->input('amount',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'amount','autofocus','error'=>false,'value'=>$order_total));

						   echo $this->Form->input('language',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'language','autofocus','error'=>false,'value'=>'EN'));

						   echo $this->Form->input('redirect_url',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'redirect_url','autofocus','error'=>false,'value'=>Configure::read('app_root_path').'carts/purchase_response'));
            
             				echo $this->Form->input('cancel_url',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'cancel_url','autofocus','error'=>false,'value'=>Configure::read('app_root_path').'carts/purchase_cancel'));

				?>


                               <div id="billing_address_area">
                                    
                                <h3 class="margin-top-0 margin-bottom-20">Billing Address</h3>

                                <div class="checkboxes margin-bottom-10">

                                <?php echo $this->Form->input('same_shipping_billing', array(
                                  'type'=>'checkbox','id'=>'same_shipping_billing','value'=>'1','checked'=>'checked','label'=>'Billing and Shipping address are same')); ?>
             
                                </div>


                                <div class="row">

                                

				<div class="col-md-6">
					<label>Billing Name</label>
                                        <?php 
					 echo $this->Form->input('Billing.billing_name',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_name','autofocus','error'=>false));
                                        ?>
				</div>
				

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>E-Mail Address</label>
						<?php 
					 echo $this->Form->input('Billing.billing_email',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_email','autofocus','error'=>false));
                                            ?>
						<i class="im im-icon-Mail"></i>
					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Phone</label>
						<?php 
                                                echo $this->Form->input('Billing.billing_tel',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_tel','autofocus','error'=>false));
                                            ?>
						<i class="im im-icon-Phone-2"></i>
					</div>
				</div>
                
                
                                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Address </label>
						<?php 
					 echo $this->Form->input('Billing.billing_address',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_address','autofocus','error'=>false));
                                        ?>

					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Postcode </label>
						<?php 
					 echo $this->Form->input('Billing.billing_zip',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_zip','autofocus','error'=>false));
                                        ?>

					</div>
				</div>
                
                   
                                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>City</label>
						<?php 
					 echo $this->Form->input('Billing.billing_city',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_city','autofocus','error'=>false));
                                        ?>

					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>State</label>
                           <?php  
                              echo $this->Form->input('Billing.billing_state', array('options'=>$states_arr, 'div'=>false,  'label'=>false, 'type'=>'select', 'id' =>'billing_state', 'empty'=>'--Select--')); 

                            ?>
					</div>
				</div>
                
                                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Country</label>
						<?php 
					 echo $this->Form->input('Billing.billing_country',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_country','autofocus','error'=>false));
                                        ?>

					</div>
				</div>
                                </div>

                                </div>
                            
                                
                                <div id="shipping_address_area">
                                    
                                <h3 class="margin-top-20 margin-bottom-30">Shipping Address</h3>

                                <div class="row">

				<div class="col-md-6">
					<label>Billing Name</label>
                                        <?php 
					 echo $this->Form->input('Shipping.delivery_name',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'delivery_name','autofocus','error'=>false));
                                        ?>
				</div>
				

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>E-Mail Address</label>
						<?php 
					 echo $this->Form->input('Shipping.delivery_email',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'delivery_email','autofocus','error'=>false));
                                            ?>
						<i class="im im-icon-Mail"></i>
					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Phone</label>
						<?php 
                                                echo $this->Form->input('Shipping.delivery_tel',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'delivery_tel','autofocus','error'=>false));
                                            ?>
						<i class="im im-icon-Phone-2"></i>
					</div>
				</div>
                
                
                                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Address </label>
						<?php 
					 echo $this->Form->input('Shipping.delivery_address',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'delivery_address','autofocus','error'=>false));
                                        ?>

					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Postcode </label>
						<?php 
					 echo $this->Form->input('Shipping.delivery_zip',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'delivery_zip','autofocus','error'=>false));
                                        ?>

					</div>
				</div>
                
                   
                                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>City</label>
						<?php 
					 echo $this->Form->input('Shipping.delivery_city',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'delivery_city','autofocus','error'=>false));
                                        ?>

					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>State</label>
                                                <?php  
                                                echo $this->Form->input('Shipping.delivery_state', array('options'=>$states_arr, 'div'=>false,  'label'=>false, 'type'=>'select', 'id' =>'delivery_state', 'empty'=>'--Select--')); 

                                                ?>
					</div>
				</div>
                
                                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Country</label>
						<?php 
					 echo $this->Form->input('Shipping.delivery_country',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'delivery_country','autofocus','error'=>false));
                                        ?>

                                        </div>
				</div>
                           </div>

                        </div>

                        </div>


			  <div class="col-md-12" id="payment_method_div">
                	<h3 class="margin-top-55 margin-bottom-30">Payment Method</h3>
					<div class="payment">
							<div class="payment-tab active">
								<div class="payment-tab-trigger">
									<input type="radio" name="data[Cart][payment_method]" id="payment_method_creditcard" value="creditCard" checked="checked" >
									<label for="creditCart">Credit / Debit Card / Net Banking (Powered By CCAvenue)</label>
									<img class="payment-logo" src="https://i.imgur.com/IHEKLgm.png" alt="">
								</div>
							</div>
                                                        
<!--
                           <div class="payment-tab">
								<div class="payment-tab-trigger">
									<input type="radio" name="data[Cart][payment_method]" id="payment_method_cod" checked="checked" value="cod">
									<label for="cod">Cash On Delivery</label>
									
								</div>
							</div>
-->
					 </div>
				  	<input type="submit" class="button booking-confirmation-btn margin-top-40 margin-bottom-65" name="btn_booking" value="Confirm and Pay" onclick="javascript:return doConfirmPayment();" />
						<?php echo $this->Form->end(); ?> 
			   </div>

						
		 </div>
		 <div class="col-md-4" id="cart_summary_div">
						<form action="#" class="total-form">
							<div class="cart-totals">
								<p class="h4">cart totals</p>
								<div class="subtotal">
									<p class="h5">subtotal
										<span> <i class="fa fa-fw">&#xf156;</i> <?php echo $grand_total; ?>
										</span>
										</p>
								</div>
								<div class="shipping">
									<p><span>shipping</span></p>
									<div class="shipping-form">
										<input type="radio" name="ship" id="ship1" checked="checked" value="<?php echo $shipping_charge; ?>">
										<label for="ship1">Flat rate: <i class="fa fa-fw">&#xf156;</i><?php echo $shipping_charge; ?></label>									
										
									</div>
								</div>
							</div>
							<div class="order-totals">
								<p class="h4">order totals 
									<span><i class="fa fa-fw">&#xf156;</i><?php echo ($grand_total + 30);?></span>
								</p>
<!--
								<div class="inner-order-totals">
									<p class="h5 cupon-head active_accordeon">apply cupon <i class="fa fa-angle-down" aria-hidden="true"></i></p>
									<div class="cupon-accordeon ">
										<div class="cupon-form">
											<input type="text" placeholder="Enter discount code" required="">
											<a href="#" class="button white">apply discount</a>
										</div>
									</div>
								</div>
-->
							</div>
<!--							<button type="submit">proceed to checkout</button>-->
						</form>
					</div>
				</div>
			</div>
		</section>





<style>
.widget-tabs li {
	padding: 0 0 12px 0 !important;
    margin: 4px 0 0 0 !important;
	
}
.list_team {
    list-style-type: square;
    color: red;
}
.widget h3 {
	color:red;
}
.blog-post{
	text-align: justify;
	}
.post-meta li{
		float:left !important;
	}
.col-lg-3, .col-md-4{
		padding-right: 0px !important; 
	}
</style>

<script>
$(document).on('ready', function() {
  

    increase();

  $('#shipping_address_area').hide();


    $("#same_shipping_billing").click(function () {
            if ($(this).is(":checked")) {
                $("#shipping_address_area").hide();
               
            } else {
                $("#shipping_address_area").show();
            }
    });


    var cart_count = "<?php echo $cart_count; ?>";

    if(cart_count==0){

    $('#cart_shipping_div').hide();
    $('#payment_method_div').hide();
    $('#cart_summary_div').hide();
    $('.cart-control-buttons').hide();

    }

    

    if(cart_count>0){

        
        check_user_login();

    }

});

function check_user_login(){

            $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/check_user_login",
                
                success:function(response){
                    if(response==1){
                       
                        $('#logged_in_user_id').val('1');

			
                    }
                    else{
                        
                            $('#logged_in_user_id').val('0');

                        $.magnificPopup.open({
			items: {
				src: '#sign-in-dialog'
			},
			type: 'inline',
			closeOnBgClick:false,
			enableEscapeKey: false,
                        closeOnContentClick: false,
                        showCloseBtn : false,
                        });


                            
                        }
    
                }

         });

}

function deleteProduct(product_id){

        
        var data={'product_id':product_id};

            $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/delete_product_from_cart",
                data: data,
                success:function(response){
                    
                     var url = window.location.href;                  
                     window.location = url;
    
                }

         });


}

function increase(){
    
        $(".decrement").on("click",function(){
            var e=$(this).closest(".counters").find(".count"),o=e.val();o>1?e.val(o=+o-1):e.val(o=1)}),
            $(".increment").on("click",function(){var e=$(this).closest(".counters").find(".count"),o=e.val();e.val(o=+o+1)})
}

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}



function doConfirmPayment(){	
	

        var flag = true;

        var shipping_address_required = 0;

        if(!$("#same_shipping_billing").is(":checked")){
            shipping_address_required = 1;
        }

        

        $('#billing_name').val($('#billing_name').val().trim());
        $('#billing_email').val($('#billing_email').val().trim());
        $('#billing_tel').val($('#billing_tel').val().trim());
        $('#billing_address').val($('#billing_address').val().trim());
        $('#billing_zip').val($('#billing_zip').val().trim());
 

        $('#delivery_name').val($('#delivery_name').val().trim());
        $('#delivery_email').val($('#delivery_email').val().trim());
        $('#delivery_tel').val($('#delivery_tel').val().trim());
        $('#delivery_address').val($('#delivery_address').val().trim());
        $('#delivery_zip').val($('#delivery_zip').val().trim());

        $('#billing_name').css('border','1px solid #e6e6e6');
	$('#billing_email').css('border','1px solid #e6e6e6');
	$('#billing_tel').css('border','1px solid #e6e6e6');
	$('#billing_address').css('border','1px solid #e6e6e6');
        $('#billing_zip').css('border','1px solid #e6e6e6');
	$('#billing_city').css('border','1px solid #e6e6e6');
	$('#billing_state').css('border','1px solid #e6e6e6');


        contact_email = $('#billing_email').val();

        if(shipping_address_required==1){

        $('#delivery_name').css('border','1px solid #e6e6e6');
	$('#delivery_email').css('border','1px solid #e6e6e6');
	$('#delivery_tel').css('border','1px solid #e6e6e6');
	$('#delivery_address').css('border','1px solid #e6e6e6');
        $('#delivery_zip').css('border','1px solid #e6e6e6');
	$('#delivery_city').css('border','1px solid #e6e6e6');
	$('#delivery_state').css('border','1px solid #e6e6e6');
 
	var delivery_email=$('#delivery_email').val();

        }


	if($('#billing_name').val().trim()==""){
		flag =false;
		$('#billing_name').css('border','1px solid red');		
	}
	else if($('#billing_email').val()=="" || validateEmail(contact_email)==false ){
		flag =false;
		$('#billing_email').css('border','1px solid red');
	}
	else if($('#billing_tel').val()==""){
		flag =false;
		$('#billing_tel').css('border','1px solid red');
	}
	else if($('#billing_address').val()==""){
		flag =false;
		$('#billing_address').css('border','1px solid red');
	}
        else if($('#billing_zip').val()==""){
		flag =false;
		$('#billing_zip').css('border','1px solid red');
	}
	else if($('#billing_city').val()==""){
		flag =false;
		$('#billing_city').css('border','1px solid red');
	}
	else if($('#billing_state').val()==""){
		flag =false;
		$('#billing_state').css('border','1px solid red');
	}
        else if(shipping_address_required==1){

            if($('#delivery_name').val()==""){
		flag =false;
		$('#delivery_name').css('border','1px solid red');		
            }
            else if($('#delivery_email').val()=="" || validateEmail(delivery_email)==false ){
		flag =false;
		$('#delivery_email').css('border','1px solid red');
            }
            else if($('#delivery_tel').val()==""){
		flag =false;
		$('#delivery_tel').css('border','1px solid red');
            }
            else if($('#delivery_address').val()==""){
		flag =false;
		$('#delivery_address').css('border','1px solid red');
            }
            else if($('#delivery_zip').val()==""){
		flag =false;
		$('#delivery_city').css('border','1px solid red');
            }
            else if($('#delivery_zip').val()==""){
		flag =false;
		$('#delivery_city').css('border','1px solid red');
            }
            else if($('#delivery_state').val()==""){
		flag =false;
		$('#delivery_state').css('border','1px solid red');
            }

            

        }else{


        $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/check_user_login",
                
                success:function(response){
                    if(response==1){
                       
                        $('#logged_in_user_id').val('1');

                        flag=true;
			
                    }
                    else{
                        
                            $('#logged_in_user_id').val('0');

                              $.magnificPopup.open({
			items: {
				src: '#sign-in-dialog'
			},
			type: 'inline',
			closeOnBgClick:false,
			enableEscapeKey: false,
                        closeOnContentClick: false,
                        showCloseBtn : false,
                        });


                        flag=false;
                            
                        }
    
                }

         });


        }

       

        return flag;

	
}
</script>

</script>