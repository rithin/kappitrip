

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Purchase Processed</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>/">Home</a></li>
						<li>Purchase Processed</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<div class="booking-confirmation-page">
				<i class="fa fa-check-circle"></i>
				<h2 class="margin-top-30">Thanks for your purchase!</h2>
				<!--<p>You'll receive a confirmation email at <?php echo $billing_email; ?></p>-->
				<a href="<?php echo Configure::read('app_root_path'); ?>carts/purchase_invoice/<?php  echo $order_id; ?>" class="button margin-top-30" target="_blank">View Invoice</a>
			</div>

		</div>
	</div>
</div>
<!-- Container / End -->
