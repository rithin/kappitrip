  <?php          
   if(count($homestayList)>0){
	   ?>
<!-- Sorting / Layout Switcher -->

 <?php 
	$keyItem=0;
	foreach($homestayList as $item){ 
     if($item['Homestay']['seo_unique_url']!=""){            
			$keyItem++;$keyItem=0;
?>
				<!-- Listing Item -->
				<div class="col-lg-4 col-md-6 item_list_box" >
					<a target="_blank" href="<?php echo  Configure::read('app_root_path')."handpicked-stays/".$item['Homestay']['seo_unique_url']; ?>" class="listing-item-container compact" data-marker-id="3" style="margin-bottom:10px !important; text-decoration: none;" >
						<div class="listing-item">
							<img src="<?php echo Configure::read('app_root_path').$item['HomestayFile']['0']['file_path']."/".$item['HomestayFile']['0']['file_name']; ?>" alt="" >


						</div>
					
					<figcaption title="<?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Homestay']['description']),0,100)), 0, 10))." ....";?>" class="fig-desc-strip">
						  <div class="caption-title"> 
							<span class="pull-right ng-binding">$<?php echo number_format($item['Homestay']['base_price'],2); ?></span>
							<span class="text-ellipsis ng-binding"><?php echo $item['Homestay']['name']; ?></span>
						  </div>
						<div class="clearfix"></div>
						  <span class="text-muted sub-title">
							<span class="pull-right ng-binding "><?php echo $item['PropertyType']['name']; ?></span> 
							<span class="text-ellipsis ng-binding"> <i class="sl sl-icon-location"></i> &nbsp;<?php echo $item['Location']['name']; ?>, <?php echo $item['City']['name']; ?></span>	
						  </span>
						<span class="sub-title" > 
							<br/>
							  <?php for($k=1;$k<=$item['Homestay']['rating'];$k++){ ?>
										 <i class="fa fa-fw">&#xf005;</i>
							  <?php } ?>
					    </span>
                    </figcaption>
					</a>
<!--					    <div class="fb-share-button fb-share" data-href="<?php //echo  Configure::read('app_root_path')."handpicked-stays/".$item['Homestay']['seo_unique_url']; ?>" data-layout="button_count" data-size="large" style="postion:absolute; width:100px; height:40px;"></div>-->
				</div>
				
					<?php if($keyItem==3 || $keyItem==6 || $keyItem==9) {
	
					?>   
<!--                        <div class="clearfix"></div>-->
                        <?php } ?>

                
    <?php } 
        }
    ?>        
				<!-- Listing Item / End -->

<?php }else{ ?>    
	
	<div style="
    text-align: center;
    height: 250px;
    padding-top: 100px;
    font-stretch: extra-expanded;
    font-family: monospace;
    color: brown;
    font-size: larger;
">
			No items found. Please try different combination of search.
</div>

<?php }?>


	<!-- Pagination -->
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12">
						<!-- Pagination -->
				<div id="pagination" class="pagination-container margin-top-20 margin-bottom-40" style=" text-align: center;">
					<?php echo $perpageresult; ?>
				</div>
			</div>
		</div>
				<!-- Pagination / End -->	