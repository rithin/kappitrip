<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
<script src='https://cdn.jsdelivr.net/jquery.mixitup/2.1.11/jquery.mixitup.min.js'></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<?php echo $this->Html->css('listing_filter.css'); ?>
<!-- Content  ================================================== -->

<div class="container  " >
	<div class="row margin-bottom-25" style="border-bottom: #F0F0F0 1px solid;">
		<div class="col-md-4">
		  <a href="#" class="cd-filter-trigger">Filters</a> 
		</div>
			<div class="col-md-8 ">
					<span class="heading" > 
						<h2>Our handpicked stays. </h2>

					</span>
		   </div>
		 
    </div>
<!--	<div class="row" style=" display:flex;justify-content: space-between; ">-->
		<!-- Filter left side -->
		
			
		
		<!-- Sidebar================================================== -->
		<div class="col-lg-3 col-md-4 cd-filter" style="border-right:1px solid #d0d0d0;">
			  <a href="#" class="cd-close"><i class="icon ent-close"></i> close </a>
			<div class="clearfix"></div>
			<div class="sidebar  margin-top-80" style=" position: -webkit-sticky;position: sticky;top: 0; "  >

				<!-- Widget -->
				<div class="widget margin-bottom-10">
					<h4 class="margin-top-0 margin-bottom-10">Show results for</h4>
<!--
					<div class="col-md-12 margin-bottom-20">
						<button class="button " onclick="javascript:filterListing('basicSearch')" > Search</button>
						<button class="button " onclick="javascript:ClearAll('clear')" > Clear All</button>
					</div>
-->
					<!-- Row -->
					<div class="row with-forms">
						<!-- Cities -->
						<div class="col-md-12 checkboxes margin-bottom-10">
					
						<input id="instantBook" value="3" type="checkbox" name="instantBook" style="height:17px; width:17px;" onclick="javascript:filterListing('instantBook')">
							<label for="instantBook" style="display:inline; font-weight:bold; padding-top: 5px;">Instant Book</label>
							 <a href=""><button class="button " onclick="javascript:ClearAll('clear')" > Clear All</button></a>
						</div>
					</div>
					<!-- Row / End -->

					<!-- Row -->
					<div class="row with-forms">
						<!-- Cities -->
						<div class="col-md-12">
						
							<?php echo $this->Form->input('search_text',array('div'=>false, 'label'=>false, 'id'=>'search_text','autofocus','value'=>$text_search,'placeholder'=>'What are you looking for?','error'=>false));?>
						</div>
					</div>
					<!-- Row / End -->
					<!-- Row -->
					<div class="row with-forms">
						<!-- Type -->
						<div class="col-md-12">
						
                           <?php  
                                echo $this->Form->input('city', array('options'=>$cities_arr,'div'=>false, 'label'=>false,  'class'=>'chosen-select',  'type'=>'select', 'id' =>'city', 'data-placeholder'=>'Location','empty'=>'Location')); 

                             ?>
						</div>
					</div>
					<!-- Row / End -->

					<!-- Row -->
<!--
					<div class="row with-forms">
						
						<div class="col-md-12">
						
                           <?php  
                               // echo $this->Form->input('location', array('options'=>$locations_arr,'div'=>false, 'label'=>false,  'class'=>'chosen-select',  'type'=>'select', 'id' =>'location', 'data-placeholder'=>'Location','empty'=>'Location')); 

                             ?>
						</div>
					</div>
-->
					<!-- Row / End -->
                    
                    <!-- Row -->
					<div class="row with-forms">
						<div class="col-md-12">
                        <?php  
                              echo $this->Form->input('property_type', array('options'=>$property_type_arr,'div'=>false, 'label'=>false,  'class'=>'chosen-select',  'type'=>'select', 'id' =>'property_type', 'data-placeholder'=>'Property Type','empty'=>'Property Type','selected'=>$property_type_id)); 

                            ?>
                        </div>
					</div>
					<!-- Row / End -->


					<br>

					<!-- Area Range -->
					<div class="range-slider">
						<input class="distance-radius" type="range" min="80" max="1500" step="1" value="500" data-title=" Price Range" onclick="filterListing('priceSlider')">
					</div>


					<!-- More Search Options -->
					<a href="#" class="more-search-options-trigger margin-bottom-5 margin-top-20" data-open-title="More Filters" data-close-title="More Filters"></a>
					
					<div class="more-search-options relative">

						<!-- Checkboxes -->
						<div class="checkboxes one-in-row margin-bottom-15" id="amenityChkbox">
					
							 <?php 
                                    $half=round(count($amenities_arr)/2);
                                    $cnt_i=0;
                                    foreach($amenities_arr as $key=>$amenity) {
                                                    
//                                       if( $half == $cnt_i){
//                                             echo '<div class="col-md-6">';
//                                        }
//                                       if($cnt_i==0){
//                                           echo '<div class="col-md-6">';
//                                        }
                                   ?>
									
                                     <input id="check-<?php echo $key+10;?>" value="<?php echo $amenity['Amenity']['id']; ?>" type="checkbox" name="check">
									<label for="check-<?php echo $key+10;?>"><?php echo $amenity['Amenity']['amenity'];?></label>
										
                                    <?php
//                                      if($cnt_i == count($amenities_arr)){
//                                           echo '</div>';
//                                        }
//                                       if($cnt_i== ($half-1)){
//                                            echo '</div>';
//                                         }
//                                                 
//                                         $cnt_i++;
                                                
								      } ?>
					
						</div>
						<!-- Checkboxes / End -->

					</div>
					<!-- More Search Options / End -->

				<button class="button fullwidth margin-top-25" onclick="javascript:filterListing('basicSearch')" > Search</button>

				</div>
				<!-- Widget / End -->

			</div>
		</div>
		<!-- Sidebar / End -->
		<!-- filter Section / End -->		
		<section class="cd-gallery">
			<div class="col-lg-12 col-md-12 ">
				
				<div class="clearfix"></div>
				<div class="row" >	
						<div id="ajaxListing">	
							<input type="hidden" name="rowcount" id="rowcount" />
						</div> <!-- Ajax Div Ends here -->		
				</div>
		 	</div>
		</section>
	
	
<!--
	
	</div>
-->
</div>


<!-- Maps -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyD8ZT5sP1TnREvkCV8oF8zBO071OE-SXmw&sensor=false&amp;language=en"></script>
<?php echo $this->Html->script('scripts/infobox.min.js'); ?>
<?php echo $this->Html->script('scripts/markerclusterer.js'); ?>
<?php echo $this->Html->script('scripts/maps.js'); ?>

<script >

		//open/close lateral filter
		$('.cd-filter-trigger').on('click', function(){

			triggerFilter(true);
		});
		$('.cd-filter .cd-close').on('click', function(){
			triggerFilter(false);
		});

		function triggerFilter($bool) {
			var elementsToTrigger = $([$('.cd-filter-trigger'), $('.cd-filter'), $('.cd-tab-filter'), $('.cd-gallery')]);
			elementsToTrigger.each(function(){
				$(this).toggleClass('filter-is-visible', $bool);
			});
		}

	
	
$( document ).ready(function() {
	if($('#property_type').val()!=0){
			filterListing('basicSearch'); 
		}
	if($('#search_text').val()!=0){
			filterListing('basicSearch'); 
		}
});	
function ClearAll(action){
    if(action=='clear'){
        $('#amenityChkbox input:checkbox').removeAttr('checked');        
        $('#property_type').val('').trigger('chosen:updated');
		$('#city').val('').trigger('chosen:updated');
       // $('#location').val('').trigger('chosen:updated');
        $('#search_text').val('');
        filterListing(''); 
		$('#instantBook input:checkbox').removeAttr('checked'); 
    }
}
function slectedAmenities(){
    var selectedAmenities = [];
    var selectedAmenitiesJson="";
     $('#amenityChkbox input:checked').each(function() {
        selectedAmenities.push($(this).val());
     });
    if(selectedAmenities.length>0){
     selectedAmenitiesJson=JSON.stringify(selectedAmenities);
    }
    return selectedAmenitiesJson;
}
    
function selectedDates(){
    var startDate=$('#startDate').val();
    var endDate=$('#endDate').val();
    var fltrString={"startDate": startDate,"endDate":endDate};
    if(startDate==""){
        console.log('startdate is null');
        return;
    }        
    var selectedDateJson=JSON.stringify(fltrString);
    console.log(fltrString);
    return selectedDateJson;
}

function selectedPriceSlider(){
    var maxprice="";
     maxprice= $('.range-output').html();
    return maxprice;
}
function filterListing(filterType,type,pageNo){
	if($(window).width() < 767)
	{
   			triggerFilter(false);// closing the search box
	} 	
	
	var urlExtraParams='';
    var data='';
    if(filterType =='Amenities'){      
          data='filterType='+filterType;
    } 
    if(filterType =='priceSlider'){  
          data='filterType='+filterType;
    }	
	if(filterType=='instantBook'){
		  data='filterType='+filterType;
	}	
	if(filterType=='basicSearch'){
        data='filterType='+filterType;
	}
    
    data= data+'&search_text='+$('#search_text').val();
  // data= data+'&location='+$('#location').val();
	data= data+'&city='+$('#city').val();
    data= data+'&property_type='+$('#property_type').val();
    if( selectedPriceSlider()>800){        
        data= data+'&selectedPriceRange='+selectedPriceSlider();
    }
    if(document.getElementById('instantBook').checked)
	       data= data+'&instantBook=Yes';
	else
			data= data+'&instantBook=""';
    
    data= data+'&selectedAmenitiesJson='+slectedAmenities();
    
    if(type=='pagination'){
		data= data+'&rowcount='+$("#rowcount").val()+'&pagination_setting='+$("#pagination-setting").val();
		urlExtraParams='?page='+pageNo;
	}
    
   // console.log('sasasasasasasa');
    $.ajax({
        type: 'POST',
        url: "<?php echo Configure::read('app_root_path'); ?>listing/ajaxFilter"+urlExtraParams,
        data: data,
        timeout: 30000,
        error: function()
         {
             return true;
        },
        beforeSend: function(){
              $('div#dim').fadeIn(400);
        },  
        complete : function(){
         $('div#dim').fadeOut(200);
        },
        success:function(html){
          //  $('.loading-overlay').hide();
            $('#ajaxListing').html(html);
        }
    });
    
    
}
    
 // date time picker for check in and checkout dates filter at listing page.
//$('.range_inputs applyBtn').val('dddd')
   

$(function() {

      $('.checkInbtn').daterangepicker(
          {
          autoUpdateInput: false,
              locale: {
                  cancelLabel: 'Clear'
              }
           
          }
          
      );
      $(".applyBtn").attr("onclick","filterListing('checkInCheckOut')");
      $('.checkInbtn').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
          var selectedDate= picker.startDate.format('DD MMM')+' - '+picker.endDate.format('DD MMM');
          $('#startDate').val(picker.startDate.format('YYYY-MM-DD'));
          $('#endDate').val(picker.endDate.format('YYYY-MM-DD'));
          $('.checkInbtn').html(selectedDate);
          
          filterListing('checkInCheckOut')
          
      });

      $('checkInbtn').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
          $('.checkInbtn').html('');
      });

});
	
function searchProperty(){
		return true;
 }
  function getresult(url) {
	  var query =url;
	  var vars = query.split("?");
	  for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");		
		if (pair[0] == 'page') {
		  var pageNo= pair[1];
		}
	  } 
	  filterListing('basicSearch','pagination',pageNo);

       /*     $.ajax({
                url: url,
                type: "GET",
                data:  {
					
                    rowcount:$("#rowcount").val(),"pagination_setting":$("#pagination-setting").val()
                },
                timeout: 30000,
                error: function()
                 {
                     return true;
                },
                beforeSend: function(){
                      $('div#dim').fadeIn(400);
                },  
                complete : function(){
                 $('div#dim').fadeOut(200);
                },
                success: function(data){
                    $("#ajaxListing").html(data);
                    //setInterval(function() {$("#overlay").hide(); },500);
                },
                error: function() 
                { } 	        
           });
		   */
}	
function changePagination(option) {
        if(option!= "") {
            getresult("<?php echo Configure::read('app_root_path'); ?>listing/ajaxFilter");
        }
}	
$( document ).ready(function() { 
    //getresult("<?php echo Configure::read('app_root_path'); ?>listing/ajaxFilter");
	filterListing('basicSearch');
});

	
</script>



    
<style>
    .checkInbtn{
           font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 15px !important;
    line-height: 18px !important;
    letter-spacing: 0.2px !important;
    padding-top: 6px !important;
    padding-bottom: 6px !important;
    color: #484848 !important;
    background: none !important;
    border: 1px solid #dce0e0 !important;
    border-radius: 4px !important;
    cursor: pointer !important;
    display: inline-block !important;
    padding-left: 12px !important;
    padding-right: 12px !important;
    position: relative !important;
    text-align: center !important;
    text-decoration: none !important;
    width: auto !important; 
    }
	
 .main-search-input-item { padding-left: 0px !important; padding-right: 0px !important; }
	
.link {padding: 10px 15px;background: transparent;border:#bccfd8 1px solid;border-left:0px;cursor:pointer;color:#607d8b}
.disabled {cursor:not-allowed;color: #bccfd8;}
.current {background: #bccfd8;}
.first{border-left:#bccfd8 1px solid;}
.question {font-weight:bold;}
.answer{padding-top: 10px;}
#pagination{margin-top: 20px;padding-top: 30px;border-top: #F0F0F0 1px solid;}
.dot {padding: 10px 15px;background: transparent;border-right: #bccfd8 1px solid;}
#overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: absolute;left: 0;top: 0;width: 100%;height: 100%;display: none;}
#overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
.page-content {padding: 20px;margin: 0 auto;}
.pagination-setting {padding:10px; margin:5px 0px 10px;border:#bccfd8  1px solid;color:#607d8b;}
</style>
