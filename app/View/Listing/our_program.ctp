
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />   -->
<?php // echo $this->Html->css('bootstrap-iso.css'); ?>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />



<!-- Content  ================================================== -->

<div class="container  ">
	<div class="row margin-bottom-25" style="border-bottom: #F0F0F0 1px solid;">
		
    </div>
	<div class="row">
		<!-- Filter left side -->
		
		<!-- Sidebar================================================== -->
		<div class="col-lg-3 col-md-4">
			<div class="sidebar">

				<!-- Widget -->
				<div class="widget margin-bottom-40">
					<h3 class="margin-top-0 margin-bottom-30">Filters</h3>
					<div class="col-md-12 margin-bottom-20">
						<button class="button " onclick="javascript:filterListing('basicSearch')" > Search</button>
						<button class="button " onclick="javascript:ClearAll('clear')" > Clear All</button>
					</div>
					<!-- Row -->
					<div class="row with-forms">
						<!-- Cities -->
						<div class="col-md-12 checkboxes margin-bottom-10">
					
						<input id="instantBook" value="3" type="checkbox" name="instantBook" style="height:17px; width:17px;" onclick="javascript:filterListing('instantBook')">
							<label for="instantBook" style="display:inline; font-weight:bold; padding-top: 5px;">Instant Book</label>
								</div>
					</div>
					<!-- Row / End -->

					<!-- Row -->
					<div class="row with-forms">
						<!-- Cities -->
						<div class="col-md-12">
						
							<?php echo $this->Form->input('search_text',array('div'=>false, 'label'=>false, 'id'=>'search_text','autofocus','value'=>$text_search,'placeholder'=>'What are you looking for?','error'=>false));?>
						</div>
					</div>
					<!-- Row / End -->


					<!-- Row -->
					<div class="row with-forms">
						<!-- Type -->
						<div class="col-md-12">
						 
                           <?php  
                                echo $this->Form->input('city', array('options'=>$cities_arr,'div'=>false, 'label'=>false,  'class'=>'chosen-select',  'type'=>'select', 'id' =>'city', 'data-placeholder'=>'Location','empty'=>'Location' ,'selected'=>$city_id)); 

                             ?>
						</div>
					</div>
					<!-- Row / End -->
                    
                    <!-- Row -->
					<div class="row with-forms">
						<div class="col-md-12">
                        <?php  
                              echo $this->Form->input('property_type', array('options'=>$property_type_arr,'div'=>false, 'label'=>false,  'class'=>'chosen-select',  'type'=>'select', 'id' =>'property_type', 'data-placeholder'=>'Property Type','empty'=>'Property Type','selected'=>$property_type_id)); 

                            ?>
                        </div>
					</div>
					<!-- Row / End -->


					<br>

					<!-- Area Range -->
					<div class="range-slider">
						<input class="distance-radius" type="range" min="10" max="1000" step="50" value="50" data-title="Products Price Range" onclick="filterListing('priceSlider')">
					</div>


					<!-- More Search Options -->
					<a href="#" class="more-search-options-trigger margin-bottom-5 margin-top-20" data-open-title="More Filters" data-close-title="More Filters"></a>
					
					<div class="more-search-options relative">

						<!-- Checkboxes -->
						<div class="checkboxes one-in-row margin-bottom-15" id="amenityChkbox">
					
							 <?php 
                                    $half=round(count($amenities_arr)/2);
                                    $cnt_i=0;
                                    foreach($amenities_arr as $key=>$amenity) {
                                                    
//                                       if( $half == $cnt_i){
//                                             echo '<div class="col-md-6">';
//                                        }
//                                       if($cnt_i==0){
//                                           echo '<div class="col-md-6">';
//                                        }
                                   ?>
									
                                     <input id="check-<?php echo $key+10;?>" value="<?php echo $amenity['Amenity']['id']; ?>" type="checkbox" name="check">
									<label for="check-<?php echo $key+10;?>"><?php echo $amenity['Amenity']['amenity'];?></label>
										
                                    <?php
//                                      if($cnt_i == count($amenities_arr)){
//                                           echo '</div>';
//                                        }
//                                       if($cnt_i== ($half-1)){
//                                            echo '</div>';
//                                         }
//                                                 
//                                         $cnt_i++;
                                                
								      } ?>
					
						</div>
						<!-- Checkboxes / End -->

					</div>
					<!-- More Search Options / End -->

				<button class="button fullwidth margin-top-25" onclick="javascript:filterListing('basicSearch')" > Search</button>

				</div>
				<!-- Widget / End -->

			</div>
		</div>
		<!-- Sidebar / End -->
		<!-- filter Section / End -->		
		
		<div class="col-lg-9 col-md-8">
			<div class="col-md-12 margin-bottom-25">
				
				 <div class="col-lg-4 col-md-6">
					 <a href="<?php echo Configure::read('app_root_path')."handpicked-stays"; ?>">
					<div class="dashboard-stat color-5">
						<div class="dashboard-stat-content stat-content-block"><h4><?php echo $active_stay;?></h4> <span>Plantation Stays</span></div>
						<div class="dashboard-stat-icon"><i class="im im-icon-Home-4"></i></div>
					</div>
					</a>
			   </div> 
				 <div class="col-lg-4 col-md-6">
					<a href="<?php echo Configure::read('app_root_path')."handpicked-experiences"; ?>">
					<div class="dashboard-stat color-6">
						<div class="dashboard-stat-content stat-content-block"><h4><?php echo $active_experience;?></h4> <span>Experiences </span></div>
						<div class="dashboard-stat-icon"><i class="im im-icon-Trekking"></i></div>
					</div>
					 </a>
			   </div>
				 <div class="col-lg-4 col-md-6">
					 <a href="<?php echo Configure::read('app_root_path')."events"; ?>">
					<div class="dashboard-stat color-7">
						<div class="dashboard-stat-content stat-content-block"><h4><?php echo $active_event;?></h4> <span>Coffe Events </span></div>
						<div class="dashboard-stat-icon"><i class="im im-icon-Coffee"></i></div>
					</div>
					 </a>
			   </div>
				
			</div>
			<div class="clearfix"></div>
			
				<div class="row">	
					<div id="ajaxListing">	
						<input type="hidden" name="rowcount" id="rowcount" />
					</div> <!-- Ajax Div Ends here -->		
				 </div>

		</div>
  
	
	
	
	</div>
</div>


<!-- Maps -->


<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyD8ZT5sP1TnREvkCV8oF8zBO071OE-SXmw&sensor=false&amp;language=en"></script>
<?php echo $this->Html->script('scripts/infobox.min.js'); ?>
<?php echo $this->Html->script('scripts/markerclusterer.js'); ?>
<?php echo $this->Html->script('scripts/maps.js'); ?>

<script >
$( document ).ready(function() {
	if($('#property_type').val()!=0){
			filterListing('basicSearch'); 
		}
	if($('#search_text').val()!=0){
			filterListing('basicSearch'); 
		}
});	
function ClearAll(action){
    if(action=='clear'){
        $('#amenityChkbox input:checkbox').removeAttr('checked');        
        $('#property_type').val('').trigger('chosen:updated');
		$('#city').val('').trigger('chosen:updated');
        //$('#location').val('').trigger('chosen:updated');
        $('#search_text').val('');
        filterListing(''); 
		$('#instantBook input:checkbox').removeAttr('checked'); 
    }
}
function slectedAmenities(){
    var selectedAmenities = [];
    var selectedAmenitiesJson="";
     $('#amenityChkbox input:checked').each(function() {
        selectedAmenities.push($(this).val());
     });
    if(selectedAmenities.length>0){
     selectedAmenitiesJson=JSON.stringify(selectedAmenities);
    }
    return selectedAmenitiesJson;
}
    
function selectedDates(){
    var startDate=$('#startDate').val();
    var endDate=$('#endDate').val();
    var fltrString={"startDate": startDate,"endDate":endDate};
    if(startDate==""){
        console.log('startdate is null');
        return;
    }        
    var selectedDateJson=JSON.stringify(fltrString);
    console.log(fltrString);
    return selectedDateJson;
}

function selectedPriceSlider(){
    var maxprice="";
     maxprice= $('.range-output').html();
    return maxprice;
}
function filterListing(filterType){
	
    var data='';
    if(filterType =='Amenities'){      
          data='filterType='+filterType;
    } 
    if(filterType =='priceSlider'){  
          data='filterType='+filterType;
    }	
	if(filterType=='instantBook'){
		  data='filterType='+filterType;
	}	
	if(filterType=='basicSearch'){
        data='filterType='+filterType;
	}
    
    data= data+'&search_text='+$('#search_text').val();
   // data= data+'&location='+$('#location').val();
	 data= data+'&city='+$('#city').val();
    data= data+'&property_type='+$('#property_type').val();
    if( selectedPriceSlider()>800){        
        data= data+'&selectedPriceRange='+selectedPriceSlider();
    }
    if(document.getElementById('instantBook').checked)
	       data= data+'&instantBook=Yes';
	else
			data= data+'&instantBook=""';
    
    data= data+'&selectedAmenitiesJson='+slectedAmenities();
    
    
   // console.log(filterType);
   //console.log(data);
    $.ajax({
        type: 'POST',
        url: "<?php echo Configure::read('app_root_path'); ?>listing/searchAjaxFilter",
        data: data,
        timeout: 30000,
        error: function()
         {
             return true;
        },
        beforeSend: function(){
              $('div#dim').fadeIn(400);
        },  
        complete : function(){
         $('div#dim').fadeOut(200);
        },
        success:function(html){
          //  $('.loading-overlay').hide();
            $('#ajaxListing').html(html);
        }
    });
    
    
}
    
 // date time picker for check in and checkout dates filter at listing page.
//$('.range_inputs applyBtn').val('dddd')
   

$(function() {

      $('.checkInbtn').daterangepicker(
          {
          autoUpdateInput: false,
              locale: {
                  cancelLabel: 'Clear'
              }
           
          }
          
      );
      $(".applyBtn").attr("onclick","filterListing('checkInCheckOut')");
      $('.checkInbtn').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
          var selectedDate= picker.startDate.format('DD MMM')+' - '+picker.endDate.format('DD MMM');
          $('#startDate').val(picker.startDate.format('YYYY-MM-DD'));
          $('#endDate').val(picker.endDate.format('YYYY-MM-DD'));
          $('.checkInbtn').html(selectedDate);
          
          filterListing('checkInCheckOut')
          
      });

      $('checkInbtn').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
          $('.checkInbtn').html('');
      });

});
	
function searchProperty(){
		return true;
 }
  function getresult(url) {

            $.ajax({
                url: url,
                type: "GET",
                data:  {
                    rowcount:$("#rowcount").val(),"pagination_setting":$("#pagination-setting").val()
                },
                timeout: 30000,
                error: function()
                 {
                     return true;
                },
                beforeSend: function(){
                      $('div#dim').fadeIn(400);
                },  
                complete : function(){
                 $('div#dim').fadeOut(200);
                },
                success: function(data){
                    $("#ajaxListing").html(data);
                    //setInterval(function() {$("#overlay").hide(); },500);
                },
                error: function() 
                { } 	        
           });
}	
function changePagination(option) {
        if(option!= "") {
            getresult("<?php echo Configure::read('app_root_path'); ?>listing/searchAjaxFilter");
        }
}	
$( document ).ready(function() { 
   // getresult("<?php //echo Configure::read('app_root_path'); ?>listing/searchAjaxFilter");
	filterListing('basicSearch');
});

	
</script>



    
<style>
    .checkInbtn{
           font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 15px !important;
    line-height: 18px !important;
    letter-spacing: 0.2px !important;
    padding-top: 6px !important;
    padding-bottom: 6px !important;
    color: #484848 !important;
    background: none !important;
    border: 1px solid #dce0e0 !important;
    border-radius: 4px !important;
    cursor: pointer !important;
    display: inline-block !important;
    padding-left: 12px !important;
    padding-right: 12px !important;
    position: relative !important;
    text-align: center !important;
    text-decoration: none !important;
    width: auto !important; 
    }
	
 .main-search-input-item { padding-left: 0px !important; padding-right: 0px !important; }
	
.link {padding: 10px 15px;background: transparent;border:#bccfd8 1px solid;border-left:0px;cursor:pointer;color:#607d8b}
.disabled {cursor:not-allowed;color: #bccfd8;}
.current {background: #bccfd8;}
.first{border-left:#bccfd8 1px solid;}
.question {font-weight:bold;}
.answer{padding-top: 10px;}
#pagination{margin-top: 20px;padding-top: 30px;border-top: #F0F0F0 1px solid;}
.dot {padding: 10px 15px;background: transparent;border-right: #bccfd8 1px solid;}
#overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: absolute;left: 0;top: 0;width: 100%;height: 100%;display: none;}
#overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
.page-content {padding: 20px;margin: 0 auto;}
.pagination-setting {padding:10px; margin:5px 0px 10px;border:#bccfd8  1px solid;color:#607d8b;}
</style>
