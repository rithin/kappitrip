  <?php          
   if(count($experienceList)>0){
	   ?>  
<!-- Sorting / Layout Switcher -->
 <div class="row fs-switcher">
       <div class="col-md-6">
					<!-- Showing Results -->
		<p class="showing-results"><?php echo count($experienceList); ?> Results Found </p>
     </div>
  </div>
   <?php 
	$keyItem=0;
    foreach($experienceList as $item){
      if($item['Experience']['seo_unique_url']!=""){
        $keyItem++;
        
?>
				<!-- Listing Item -->
				<div class="col-lg-4 col-md-6 item_list_box">
				 <a  target="_blank" href="<?php echo  Configure::read('app_root_path')."handpicked-experiences/".$item['Experience']['seo_unique_url']; ?>" class="listing-item-container compact" data-marker-id="3" style="margin-bottom:10px !important; text-decoration: none;overflow: hidden;" >
					<?php if($item['Deal']['id']!=''){
						$offer_text="";
				
							if($item['Deal']['offer_type']=='percentage')
								$offer_text=$item['Deal']['offer_value']."% OFF till ".date('jS-M',strtotime($item['Deal']['end_date']));
							else
								$offer_text=$item['Deal']['offer_value']."Rs OFF till ".date('jS-M',strtotime($item['Deal']['end_date']));
						?>
                       <div class="listing-badge now-open" style="right:-42px;top:39px;font-weight: bold;"><?php echo $offer_text;?></div> 
                        
                      <?php }?>	
						
						<div class="listing-item">
							<img src="<?php echo Configure::read('app_root_path').$item['ExperienceFile']['0']['file_path']."/".$item['ExperienceFile']['0']['file_name']; ?>" alt="" >


						</div>
					
					<figcaption title="<?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Experience']['title']),0,100)), 0, 10))." ....";?>" class="fig-desc-strip">
						  <span class="caption-title"> 
							<span class="pull-right ng-binding">
							<?php 
		  						$item_price=$item['ExperienceSlot'][0]['price'];
		  						$offer_price=0;
		  						if($item['Deal']['id']!=''){
									if($item['Deal']['offer_type']=='percentage')
										$offer_price=($item_price-(($item_price*$item['Deal']['offer_value'])/100));
									else
										$offer_price=($item_price-$item['Deal']['offer_value']);
								 }	
		  						if($offer_price>0){
									echo "₹"."<strike>".number_format($item_price,2)."</strike> <br/>";
		  							echo "₹".number_format($offer_price,2);
								}
		  						else
									echo "₹".number_format($item_price,2);
							?>
							 </span>
							<span class="text-ellipsis ng-binding"><?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Experience']['name']),0,20)), 0, 10))." ...";?></span>
						  </span>
						<div class="clearfix"></div>
						  <span class="text-muted sub-title">
							<span class="pull-right ng-binding"><?php echo $item['ExperienceType']['type']; ?></span> 
							<span class="text-ellipsis ng-binding"> <i class="sl sl-icon-location"></i> &nbsp;<?php echo $item['Location']['name']; ?>, <?php echo $item['City']['name']; ?></span>
						  </span>
						<span class="sub-title" > 
							<br/>
							 <?php for($k=1;$k<=$item['Experience']['rating'];$k++){ ?>
										 <i class="fa fa-fw">&#xf005;</i>
							  <?php } ?>
					    </span>
                    </figcaption>
					</a>
				</div>
		      <?php if($keyItem==3 || $keyItem==6 || $keyItem==9) { ?>   <div class="clearfix"></div> <?php } ?>
                
    <?php 
        }
   
    } ?>        
				<!-- Listing Item / End -->

<?php }else{ ?>    
	
	<div style="
    text-align: center;
    height: 250px;
    padding-top: 100px;
    font-stretch: extra-expanded;
    font-family: monospace;
    color: brown;
    font-size: larger;
">
			No items found. Please try different combination of search.
</div>

<?php }?>


	<!-- Pagination -->
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12">
						<!-- Pagination -->
				<div id="pagination" class="pagination-container margin-top-20 margin-bottom-40" style=" text-align: center;">
					<?php echo $perpageresult; ?>
				</div>
			</div>
		</div>
				<!-- Pagination / End -->	