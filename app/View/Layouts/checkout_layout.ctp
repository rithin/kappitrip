<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
	<link rel='shortcut icon' href='<?php echo Configure::read('app_root_path'); ?>favicon.ico' type='image/x-icon' />
<title><?php echo ".:: KappiTrip.com ::. ". "STAYS. COFFEE and EXPERIENCES."; ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<!--
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">
-->

<?php echo $this->Html->css('style.css'); ?>
<?php echo $this->Html->css('kapptrip_custom.css'); ?>
<?php echo $this->Html->css('colors/main.css',array('id'=>'colors')); ?>
<!--    jquery loading first-->
<?php echo $this->Html->script('scripts/jquery-2.2.0.min.js'); ?>

	
</head>

<body>
<div id='dim' style="display:none;"><div style="text-align:center;"><img src="<?php echo Configure::read('app_root_path'); ?>images/loading.gif" alt="Loading... please wait" title="Loading... please wait" /></div></div>
<!-- Wrapper -->
<div id="wrapper"  <?php  if($this->request->controller=="listing" ) echo "class='bootstrap-iso'"; ?>  >

<!-- Header Container
================================================== -->
<header id="header-container">

	
	<!-- Header -->
	<div id="header" <?php  if($this->request->controller=="detail" ) echo "class='not-sticky'"; ?> style="padding: 0px 0 4px 0 !important;  box-shadow: 0 0 1px 0 rgba(0, 0, 0, 0.12);"> 
		
		<div class="clearfix"></div>
		<div class="container" >
			
			<!-- Left Side Content -->
			<div class="left-side" style="width:70% !important;">
				
				<!-- Logo max-height: 44px; -->
				<div id="logo" >
					<a href="<?php echo Configure::read('app_root_path'); ?>">             
						<img src="<?php echo Configure::read('app_root_path'); ?>images/logo120_156.png" alt="KaapiTrip.com" style="    max-height: 86px;"> 
						
<!--						<h4 style="float:left;padding-top: 10px;font-size: 28px;">KAPPI<span style="color:#c7a17a">TRIP</span></h4>-->
                    </a>
				</div>

				<!-- Mobile Navigation -->
				<div class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>

				<!-- Main Navigation -->
				   		<?php echo $this->element('menu/top_menu'); ?>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / start -->
		<div class="right-side" >
			<!-- User Menu -->
			<div class="user-menu">
				<div class="user-name">					
					<?php if($this->Session->check('Auth.User')) {
					 $icon_name=substr(ucfirst($this->Session->read('Auth.User.first_name')),0,1);
					
					?>
						<span class="guest_icon"><?php echo $icon_name;?></span> <?php echo @substr(ucfirst($this->Session->read('Auth.User.first_name')),0,7); ?>
					<?php } else { ?>
							<span class="guest_icon">G</span> Guest
					 <?php } ?>
				</div>
					<ul>
						<?php if($this->Session->check('Auth.User')) { ?>
							<li>Welcome, <?php echo ucfirst($this->Session->read('Auth.User.first_name')); ?> </li>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>user/profile"><i class="im im-icon-Professor"></i> My Profile</a> </li>
	   				<li><a href="<?php echo Configure::read('app_root_path'); ?>user/bookings"><i class="im im-icon-Calendar"></i>My Bookings</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>user/orders"><i class="im im-icon-Full-Bag"></i>My Orders</a></li>
	   				<li><a href="<?php echo Configure::read('app_root_path'); ?>user/settings"><i class="sl sl-icon-settings"></i>Settings</a> </li>
							<li><a href="#" onclick="logout_url_redirect();"><i class="sl sl-icon-power"></i> Logout</a></li>
						<?php } else { ?>
							<li><a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim" onclick="javascript:inititePopup()"><i class="sl sl-icon-user"></i> 
								Sign In/ Register</a></li>
							 <?php } ?>
					</ul>
			</div>
		
            <div class="cartbox">
					<a href="javascript:void(0);" class="js-show-header-dropdown"> 
						<div class="wrapper-left">			
							<span class="header-icons-noti" id="cartCounter"></span>
							<span class="carticon">
								<img src="<?php echo Configure::read('app_root_path'); ?>images/mini-cart.png"> 
							</span>
							
<!--							<span><p class="price" id="cartPrice"></p></span>-->
						</div>
						
					</a>
				<div class="header-cart header-dropdown">
					<div id="ajaxMiniCart"></div>
					 <div class="header-cart-buttons">
						<div class="header-cart-wrapbtn">
							<a href="<?php echo Configure::read('app_root_path'); ?>carts/single_page_checkout" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4"><button class="button fullwidth" >View Cart</button></a>
						</div>
						<div class="header-cart-wrapbtn">
							<a href="<?php echo Configure::read('app_root_path'); ?>carts/single_page_checkout" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4"><button class="button fullwidth" >Checkout</button></a>
						</div>
					</div>
				</div>
<!--
				<div class="wrapper-right">
						<button class="button fullwidth" >Checkout</button>
				</div>
-->
		 </div>
			
	   </div>
			<!-- Right Side Content / End -->

				<!-- Sign In Popup -->
            
        <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

				<div class="small-dialog-header">
					<h3>Sign In</h3>
				</div>

				<!--Tabs -->
				<div class="sign-in-form style-1">

					<ul class="tabs-nav">
						<li class=""><a href="#tab1">Log In</a></li>
						<li><a href="#tab2">Register</a></li>
                        <li><a href="#tab3">Forgot Password</a></li>
					</ul>

					<div class="tabs-container alt">

						<!-- Login -->
						<div class="tab-content" id="tab1" style="display: none;">
                                                    
                          <!--<div class="form-row form-row-wide">
                             <a class=""  href="#" onclick="facebook_url_redirect();"><img style="max-height:20px;" src="<?php echo Configure::read('app_root_path'); ?>images/fb_icon.png"> SIGN IN WITH FACEBOOK </a> 
                              <a class="" href="#" onclick="google_url_redirect();"><img style="max-height:40px;" src="<?php echo Configure::read('app_root_path'); ?>images/google_icon.png"> SIGN IN WITH GOOGLE </a> 
                            </div>-->
                                 
                        
							 <?php                         
                                 $action = Configure::read('app_root_path')."users/customer_login";
                                 echo $this->Form->create('User', array('id'=>'customer_login', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','autocomplete'=>'off','class'=>'form-horizontal ','name'=>'formSignin', 'novalidate' => true));  
                         ?>  
                                <span style="color:red;" id="displayMessage"> </span>
								<p class="form-row form-row-wide">
									<label for="username">Username:
										<i class="im im-icon-Male"></i>
								        <?php echo $this->Form->input('login_username',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'login_username','autofocus','data-rule-required'=>"true",'error'=>false));?>
									</label>
								</p>

								<p class="form-row form-row-wide">
									<label for="password">Password:
										<i class="im im-icon-Lock-2"></i>
                                        <?php echo $this->Form->input('login_password',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'login_password','type'=>'password','autofocus','data-rule-required'=>"true",'error'=>false));?>
									</label>
									<span class="lost_password">
                                        <a href="javascript:lostPass();" >Lost Your Password?</a> 
									</span>
								</p>

								<div class="form-row">
									<input type="button" class="button border margin-top-5"  name="login" onclick="javascript:submit_login()" value="Login" />
									<div class="checkboxes margin-top-10">
										<input id="remember-me" type="checkbox" name="check">
										<label for="remember-me">Remember Me</label>
									</div>
								</div>
								
							 <?php echo $this->Form->end(); ?>  
						</div>

						<!-- Register -->
						<div class="tab-content" id="tab2" style="display: none;">
							
				    <?php                         
                                 $action = Configure::read('app_root_path')."users/customer_register";
                             echo $this->Form->create('User', array('id'=>'customer_register', 'type'=>'file','autocomplete'=>'off', 'class'=>'form-horizontal', 'name'=>'formCustomerRegister'));  
                         ?>  
							
						
						
						
							<p class="form-row form-row-wide">
								<label for="username2">Email:
									<i class="im im-icon-Male"></i>
									 <?php echo $this->Form->input('register_username',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'register_username','autofocus','error'=>false));?>
								</label>
							</p>
								
							<p class="form-row form-row-wide">
								<label for="email2">First Name:
									<i class="im im-icon-Mail"></i>
									 <?php echo $this->Form->input('register_first_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'register_first_name','autofocus','error'=>false));?>
								</label>
							</p>
                            <p class="form-row form-row-wide">
								<label for="email2">Last Name:
									<i class="im im-icon-Mail"></i>
									 <?php echo $this->Form->input('register_last_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'register_last_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
								</label>
							</p>
                            <p class="form-row form-row-wide">
								<label for="email2">Date of Birth:
									<i class="im im-icon-Mail"></i>
									 <?php echo $this->Form->input('register_dob',array('div'=>false, 'label'=>false, 'class'=>'mask_date form-control', 'id'=>'register_dob','autofocus','error'=>false,'alt'=>'YYYY-MM-DD'));?>
								</label>
							</p>
							<p class="form-row form-row-wide">
								<label for="password1">Password:
									<i class="im im-icon-Lock-2"></i>
									 <?php echo $this->Form->input('register_password',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'register_password','type'=>'password','autofocus','data-rule-required'=>"true",'error'=>false));?>
								</label>
							</p>

							<p class="form-row form-row-wide">
								<label for="password2">Repeat Password:
									<i class="im im-icon-Lock-2"></i>
									 <?php echo $this->Form->input('register_re_password',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'register_re_password','type'=>'password','autofocus','data-rule-required'=>"true",'error'=>false));?>
								</label>
							</p>
							<div id="RegisterMessage" style="display:none; color:red;" ></div>
                                                        <div id="successMessage" style="display:none; color:green;" ></div>
							<input type="submit" class="button border fw margin-top-10" name="register"  value="Register" />
	
							 <?php echo $this->Form->end(); ?>  
						</div>
                        
                        <div class="tab-content" id="tab3" style="display: none;">
                        <?php                         
                            $action = Configure::read('app_root_path')."users/submit_forgot_password";
                             echo $this->Form->create('User', array('id'=>'forgot_password', 'type'=>'file','autocomplete'=>'off', 'class'=>'form-horizontal', 'name'=>'formForgotPassword'));  
                            
                          ?> 
                            <span style="color:red;" id="displayMessageForgot"> </span>
                            <p class="form-row form-row-wide" id="forgot_pass">
								<label for="username2">Email:
									<i class="im im-icon-Male"></i>
									 <?php echo $this->Form->input('forgot_email',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'forgot_email','autofocus','error'=>false));?>
								</label>
							</p>
                            
                            <input type="button" class="button border fw margin-top-10" name="forgot_pass_btn" id="forgot_pass_btn"  value="Forgot Password" onclick="javascript:submit_forgot_password()"  />
                            
                            <?php echo $this->Form->end(); ?>  
                            
                        </div>
                        

					</div>
				</div>
			</div>
			
			<!-- Sign In Popup / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>
    


<div class="clearfix"></div>
<!-- Header Container / End -->
<?php
	if($this->Session->read('Auth.User.id')){
		$logged_in_user_id=$this->Session->read('Auth.User.id');
	}else{
		$logged_in_user_id=0;
	}
?>
<input type="hidden" name="logged_in_user_id" id="logged_in_user_id" value="<?php echo $logged_in_user_id; ?>" />
  <?php echo $content_for_layout; ?>      
<!-- Footer
================================================== -->
   		<?php echo $this->element('footer'); ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


</div>
<!-- Wrapper / End -->



<!-- Scripts
================================================== -->

<?php echo $this->Html->script('scripts/mmenu.min.js'); ?>
<?php echo $this->Html->script('scripts/chosen.min.js'); ?>
<?php echo $this->Html->script('scripts/slick.min.js'); ?>
<?php echo $this->Html->script('scripts/rangeslider.min.js'); ?>
<?php echo $this->Html->script('scripts/magnific-popup.min.js'); ?>

<?php echo $this->Html->script('scripts/waypoints.min.js'); ?>
<?php echo $this->Html->script('scripts/counterup.min.js'); ?>
<?php echo $this->Html->script('scripts/jquery-ui.min.js'); ?>
<?php echo $this->Html->script('scripts/tooltips.min.js'); ?>
<?php echo $this->Html->script('scripts/custom.js'); ?>
<?php echo $this->Html->script('scripts/kappitrip_custom.js'); ?>
    
    
<?php echo $this->Html->script('scripts/validationengine/languages/jquery.validationEngine-en.js'); ?>
<?php echo $this->Html->script('scripts/validationengine/jquery.validationEngine.js'); ?>
<?php echo $this->Html->script('scripts/jquery-validation/jquery.validate.js'); ?>
<?php echo $this->Html->script('scripts/maskedinput/jquery.maskedinput.min.js'); ?>

	
<script>
    
function lostPass() 
{
    $('#tab1').hide();
    $('#tab2').hide();
    $('#tab3').show();
}
function inititePopup(){
    $('#forgot_pass').show();
    $('#forgot_pass_btn').show();  
    $('#displayMessageForgot').hide();
}
    
$(document).ready(function() { 
    
            
   $("formSignin").submit(function(e){
        e.preventDefault();
    });
    $("formCustomerRegister").submit(function(e){
        e.preventDefault();
    });
    
    $("#register_dob").mask('99-99-9999');
    $.validator.addMethod(
        "indianDate",
        function(value, element) {
            // put your own logic here, this is just a (crappy) example
            return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/);
        },
        "Please enter a date in the format dd/mm/yyyy."
    );
    
 });
	

 function facebook_url_redirect(){
   var facebook_url =  "<?php echo Configure::read('app_root_path'); ?>" +"auth/facebook";
   var current_url = window.location.href;   
   var current_url = encodeURIComponent(current_url);
   dataString={"current_url":current_url};
   var dataString=JSON.stringify(dataString);
   
   $.ajax
        ({
             type: "POST",
             url: "<?php echo Configure::read('app_root_path'); ?>users/save_current_url",
             data: "dataString="+dataString,  
             dataType:'json',
             success: function(loginResponse)
             {
                if(loginResponse.response=="success"){
                            
                    window.location = facebook_url;
                }
             }
    }); 
   
   return false;
}


function google_url_redirect(){

   var google_url =  "<?php echo Configure::read('app_root_path'); ?>" +"auth/google";
   var current_url = window.location.href;
   var current_url = encodeURIComponent(current_url);
   dataString={"current_url":current_url};
   var dataString=JSON.stringify(dataString);
   
   $.ajax
        ({
             type: "POST",
             url: "<?php echo Configure::read('app_root_path'); ?>users/save_current_url",
             data: "dataString="+dataString, 
             dataType:'json',
             success: function(loginResponse)
             {
                if(loginResponse.response=="success"){
                                     
                    window.location = google_url;
                }
             }
    }); 
   
   return false;
}


function logout_url_redirect(){
   var logout_url =  "<?php echo Configure::read('app_root_path'); ?>users/logout";
   var current_url = window.location.href;
   
   var current_url = encodeURIComponent(current_url);
   dataString={"current_url":current_url};
   var dataString=JSON.stringify(dataString);
   
   $.ajax
        ({
             type: "POST",
             url: "<?php echo Configure::read('app_root_path'); ?>users/save_current_url",
             data: "dataString="+dataString, 
             dataType:'json',
             success: function(loginResponse)
             {
                if(loginResponse.response=="success"){
                                     
                    window.location = logout_url;
                }
             }
    }); 
   
   return false;


}
 
function submit_forgot_password(){
    if($('#forgot_email').val()==""){
        $('#displayMessageForgot').html('Please enter correct username/email.');
    }else{
         var forgot_email=$('#forgot_email').val();       
         forgotDataString={"forgot_email":forgot_email};
        var forgotDataJson=JSON.stringify(forgotDataString);
        $.ajax
        ({
             type: "POST",
             url: "<?php echo Configure::read('app_root_path'); ?>users/submit_forgot_password",
             data: "forgotData="+forgotDataJson, 
             dataType:'json',
             success: function(forgotData)
             {
                if(forgotData.response=="success"){
                     $('#forgot_pass').hide();
                     $('#forgot_pass_btn').hide();                    
                     $('#displayMessageForgot').css('color','green');
                    $('#displayMessageForgot').css('font-weight','bold');
                     $('#displayMessageForgot').html('Please check your email and click on the link to reset the password.')
                }
                if(forgotData.response=="wrong_email"){
                     $('#forgot_email').css('border','1px solid red');
                     $('#displayMessageForgot').html('Oops, Something wrong. Please make sure the email you entered is correct.')
                 }
             }
        }); 
        
    }

}
 	
	
function submit_login(){
    var data=null;
    
    if($('#login_username').val()==""){
        $('#displayMessage').html('Please enter correct username.');
    }
    else if($('#login_password').val()==""){
        $('#displayMessage').html('Please enter correct password.');
    }
    else{
        var username=$('#login_username').val();
        var password=$('#login_password').val();       
        loginDataString={"username":username,"password":password};
        var loginDataJson=JSON.stringify(loginDataString);
        $.ajax
        ({
             type: "POST",
             url: "<?php echo Configure::read('app_root_path'); ?>users/customer_login",
             data: "loginData="+loginDataJson, 
             dataType:'json',
             success: function(loginResponse)
             {
                if(loginResponse.response=="success"){
                    var url = window.location.href;                  
                    window.location = url;
                }
                 if(loginResponse.response=="Auth Login Failed"){
                     $('#login_username').css('border','1px solid red');
                     $('#login_password').css('border','1px solid red');
                     $('#displayMessage').html('Oops, Something wrong. Please make sure the credentials are correct.')
                 }
             }
        }); 
    }
 
}    
    
    
$(document).ready(function() {
     
    $("#customer_register").validate({
                ignore: [],
                rules: {                                            
                        'data[User][register_username]': {
                                required: true,
                                 email: true
                        },
                        'data[User][register_password]': {
                                required: true,
                                minlength: 5,
                                maxlength: 10
                        },
                        'data[User][register_re_password]': {
                                required: true,
                                minlength: 5,
                                maxlength: 10,
                                equalTo: "#register_password"
                        },
                        'data[User][register_dob]': {
                                required: true,
                                indianDate : true
                        },
                        'data[User][register_first_name]': {
                                required: true,
                               
                        },
                        'data[User][register_last_name]': {
                                required: true,
                                
                        }
             },
                
        submitHandler: function(form) {
             var register_username=$('#register_username').val();
            var register_password=$('#register_password').val();
            var register_dob=$('#register_dob').val();
            var register_first_name=$('#register_first_name').val();
            var register_last_name=$('#register_last_name').val();
            var registerDataString={"register_username":register_username,"register_password":register_password, "register_dob":register_dob,"register_first_name":register_first_name, "register_last_name":register_last_name};
            var registerDataJson=JSON.stringify(registerDataString);

            request =  $.ajax
                ({
                    type: "POST",
                    url: "<?php echo Configure::read('app_root_path'); ?>users/customer_register",
                    data: "register="+registerDataJson,
                    dataType:'json',
                    success: function(registerResponse)
                    {
						//console.log(registerResponse);
						if(registerResponse.response=="email_exist"){
//console.log(registerResponse);
							$('#register_username').css('border','1px solid red;');
							$('#RegisterMessage').show();
							$('#RegisterMessage').html('Email already exist.')
						}else{
							
							$('#successMessage').show();
                                                        $('#RegisterMessage').hide();
							$('#successMessage').html('Registration completed Successfully.Please login again.');
						}


                    }
                }); 
           
            return false;
        }
    
     });   
    
});   
	
	
//ajax for loading mini cart
 $(document).ready(function(){      
    var dataInput;
    $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/getCartCount",
                data: dataInput,                
                success:function(dataCount){
                    $('#cartCounter').html(dataCount);
					var miniCart=$.parseJSON(dataCount);
                    $('#cartCounter').html(miniCart.itemCount);
					var cartTotal= miniCart.cartTotal;
					$('#cartPrice').html(" ₹ "+cartTotal.toFixed(2));
                }
    });
    $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/ajax_mini_cart",
                data: dataInput,
                beforeSend:function(html){
                    $('.loading-overlay').show();
                },
                success:function(cartDetails){
                    $('.loading-overlay').hide();
                    $('#ajaxMiniCart').html(cartDetails);
                }
    });
 });      
     
</script>
<style>
    .error{
        color:darkred !important;
        font-weight: 200;
    }

 div#dim
{
    background-color:#564733;
    -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=70)";
    filter: alpha(opacity=70);
    opacity:.9;
    height:100%;
    width:100%;
    position:fixed;
    display:none;
    cursor:wait;
	z-index:10000000;
	top:0px;
  right:0px;
}

div#dim div
{

    position:relative;
    
    z-index:10000000;
    text-align:center;
    cursor:wait;
}

div#dim div img
{
    z-index:10000000;
    text-align:center;
    border:none;
    cursor:wait;
}
</style>    

    
</body>
</html>