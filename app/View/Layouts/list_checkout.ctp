<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo ".:: CoorgExpress.com ::. ". "Find the best homestay in coorg.,........"; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/favicon.png"/>
<!--===============================================================================================-->
	<?php echo $this->Html->css('vendor/bootstrap/css/bootstrap.min.css'); ?> 
<!--===============================================================================================-->
	<?php echo $this->Html->css('fonts/font-awesome-4.7.0/css/font-awesome.min.css'); ?> 
<!--===============================================================================================-->
	<?php echo $this->Html->css('fonts/themify/themify-icons.css'); ?>
<!--===============================================================================================-->
	<?php echo $this->Html->css('fonts/Linearicons-Free-v1.0.0/icon-font.min.css'); ?> 
<!--===============================================================================================-->
	<?php echo $this->Html->css('fonts/elegant-font/html-css/style.css'); ?> 
<!--===============================================================================================-->
	<?php echo $this->Html->css('vendor/animate/animate.css'); ?> 
<!--===============================================================================================-->
	<?php echo $this->Html->css('vendor/css-hamburgers/hamburgers.min.css'); ?> 
<!--===============================================================================================-->
	<?php echo $this->Html->css('vendor/animsition/css/animsition.min.css'); ?>
<!--===============================================================================================-->
	<?php echo $this->Html->css('vendor/select2/select2.min.css'); ?>
<!--===============================================================================================-->
	<?php echo $this->Html->css('vendor/slick/slick.css'); ?>
<!--===============================================================================================-->
	<?php echo $this->Html->css('vendor/noui/nouislider.min.css'); ?> 
<!--===============================================================================================-->
	<?php echo $this->Html->css('fashe/util.css'); ?>
	<?php echo $this->Html->css('fashe/main.css'); ?>
<!--===============================================================================================-->
    	<?php echo $this->Html->script('vendor/jquery/jquery-3.2.1.min.js'); ?>
<!--===============================================================================================-->

</head>
<body class="animsition">

	<!-- Header -->
	<header class="header1">
		<!-- Header desktop -->
		<div class="container-menu-header">
			<div class="topbar">
<!--
				<div class="topbar-social">
					<a href="#" class="topbar-social-item fa fa-facebook"></a>
					<a href="#" class="topbar-social-item fa fa-instagram"></a>
					<a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
					<a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
					<a href="#" class="topbar-social-item fa fa-youtube-play"></a>
				</div>

				<span class="topbar-child1">
					Free shipping for standard order over $100
				</span>

				<div class="topbar-child2">
					<span class="topbar-email">
						fashe@example.com
					</span>

					<div class="topbar-language rs1-select2">
						<select class="selection-1" name="time">
							<option>USD</option>
							<option>EUR</option>
						</select>
					</div>
				</div>
-->
			</div>

			<div class="wrap_header">
				<!-- Logo -->
				<a href="<?php echo Configure::read('app_root_path'); ?>" class="logo">
					<img src="<?php echo Configure::read('app_root_path'); ?>/images/coorgexpress.jpg" style="float:left;" alt="CoorgExpress.com"> <h4 style="float:left">COORG<span style="color:red">EXPRESS</span></h4>
				</a>

				<!-- Menu -->
				<div class="wrap_menu">
					<nav class="menu">
						<ul class="main_menu">
							<li>
								<a href="<?php echo Configure::read('app_root_path'); ?>">Home</a>
								
							</li>

							<li>
								<a href="<?php echo Configure::read('app_root_path'); ?>listing/">Stays</a>
							</li>

							<li class="sale-noti">
								<a href="<?php echo Configure::read('app_root_path'); ?>listing/productList">Products</a>
							</li>

							<li>
								<a href="#">Pages</a>
                                   <ul class="sub_menu">
                                        <li><a href="pages-user-profile.html">About US</a></li>
                                        <li><a href="pages-booking.html">Why We Are?</a></li>
                                        <li><a href="pages-blog.html">Blog</a>
                                            <ul class="sub-menu">
                                                <li><a href="pages-blog.html">Blog</a></li>
                                                <li><a href="pages-blog-post.html">Blog Post</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="pages-contact.html">Contact</a></li>

                                    </ul>
							</li>
						</ul>
					</nav>
				</div>

				<!-- Header Icon -->
				<div class="header-icons">
					<a href="#" class="header-wrapicon1 dis-block">
						<img src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/icon-header-01.png" class="header-icon1" alt="Login">
					</a>

					<span class="linedivide1"></span>

					<div class="header-wrapicon2">
						
						first
						<img src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="View Cart">
						<span class="header-icons-noti" id="cartCounter">0</span>

						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
                            <div id="ajaxMiniCart">
                            </div>
							<div class="header-cart-buttons">
								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="<?php echo Configure::read('app_root_path'); ?>carts/view" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										View Cart
									</a>
								</div>

								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="<?php echo Configure::read('app_root_path'); ?>carts/view" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Check Out
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a href="index.html" class="logo-mobile">
				<img src="<?php echo Configure::read('app_root_path'); ?>/images/coorgexpress.jpg" alt="CoorgExpress.com">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">
					<a href="#" class="header-wrapicon1 dis-block">
						<img src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						second one
						<img src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICONghfhfh">
						<span class="header-icons-noti">0</span>

						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
							<div id="ajaxMiniCart">
                            </div>

							<div class="header-cart-buttons">
								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										View Cart
									</a>
								</div>

								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Check Out
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">
					<li class="item-menu-mobile">
						<a href="<?php echo Configure::read('app_root_path'); ?>">Home</a>
						
					</li>
					<li class="item-menu-mobile">
						<a href="<?php echo Configure::read('app_root_path'); ?>listing/">Stays</a>
					</li>

					<li class="item-menu-mobile">
						<a href="<?php echo Configure::read('app_root_path'); ?>listing/productList">Products</a>
					</li>

					<li class="item-menu-mobile">
						<a href="#">Pages</a>
                        <ul class="sub-menu">
							 <li><a href="pages-user-profile.html">About US</a></li>
                             <li><a href="pages-booking.html">Why We Are?</a></li>
                             <li><a href="pages-blog.html">Blog</a>
                                 <ul class="sub-menu">
                                    <li><a href="pages-blog.html">Blog</a></li>
                                    <li><a href="pages-blog-post.html">Blog Post</a></li>
                                  </ul>
                             </li>
                             <li><a href="pages-contact.html">Contact</a></li>
						</ul>
						<i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>


					</li>

					
				</ul>
			</nav>
		</div>
	</header>

<?php echo $content_for_layout; ?>    
	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					GET IN TOUCH
				</h4>

				<div>
					<p class="s-text7 w-size27">
						Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
					</p>

					<div class="flex-m p-t-30">
						<a href="#" class="fs-18 color1 p-r-20 fa fa-facebook"></a>
						<a href="#" class="fs-18 color1 p-r-20 fa fa-instagram"></a>
						<a href="#" class="fs-18 color1 p-r-20 fa fa-pinterest-p"></a>
						<a href="#" class="fs-18 color1 p-r-20 fa fa-snapchat-ghost"></a>
						<a href="#" class="fs-18 color1 p-r-20 fa fa-youtube-play"></a>
					</div>
				</div>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Categories
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Men
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Women
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Dresses
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Sunglasses
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Links
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Search
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							About Us
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Contact Us
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Returns
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Help
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Track Order
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Returns
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Shipping
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							FAQs
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					Newsletter
				</h4>

				<form>
					<div class="effect1 w-size9">
						<input class="s-text7 bg6 w-full p-b-5" type="text" name="email" placeholder="email@example.com">
						<span class="effect1-line"></span>
					</div>

					<div class="w-size2 p-t-20">
						<!-- Button -->
						<button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
							Subscribe
						</button>
					</div>

				</form>
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">
			<a href="#">
				<img class="h-size2" src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/paypal.png" alt="IMG-PAYPAL">
			</a>

			<a href="#">
				<img class="h-size2" src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/visa.png" alt="IMG-VISA">
			</a>

			<a href="#">
				<img class="h-size2" src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/mastercard.png" alt="IMG-MASTERCARD">
			</a>

			<a href="#">
				<img class="h-size2" src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/express.png" alt="IMG-EXPRESS">
			</a>

			<a href="#">
				<img class="h-size2" src="<?php echo Configure::read('app_root_path'); ?>/fashe_images/icons/discover.png" alt="IMG-DISCOVER">
			</a>

			<div class="t-center s-text8 p-t-20">
				Copyright © 2018 All rights reserved. | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
			</div>
		</div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection -->
	<div id="dropDownSelect1"></div>
	<div id="dropDownSelect2"></div>



<!--===============================================================================================-->
	<?php echo $this->Html->script('vendor/animsition/js/animsition.min.js'); ?>
<!--===============================================================================================-->
	<?php echo $this->Html->script('vendor/bootstrap/js/popper.js'); ?>
	<?php echo $this->Html->script('vendor/bootstrap/js/bootstrap.min.js'); ?>
<!--===============================================================================================-->
	<?php echo $this->Html->script('vendor/select2/select2.min.js'); ?>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});

		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect2')
		});
	</script>
<!--===============================================================================================-->
	<?php echo $this->Html->script('vendor/daterangepicker/moment.min.js'); ?>
	<?php echo $this->Html->script('vendor/daterangepicker/daterangepicker.js'); ?>
<!--===============================================================================================-->
	<?php echo $this->Html->script('vendor/slick/slick.min.js'); ?>
	<?php echo $this->Html->script('fashe/slick-custom.js'); ?>
<!--===============================================================================================-->
	<?php echo $this->Html->script('vendor/sweetalert/sweetalert.min.js'); ?>
	<script>
         $('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});

		$('.btn-addcart-product-detail').each(function(){
			var nameProduct = $('.product-detail-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		}); 
        
    </script>

<!--===============================================================================================-->
<?php echo $this->Html->script('vendor/noui/nouislider.min.js'); ?>
    <script type="text/javascript">
<?php if(($this->params['controller']=='listing') &&  $this->params['action']=='productListA'){    
    ?>
	
	
		/*[ No ui ]
	    ===========================================================*/
	    var filterBar = document.getElementById('filter-bar');

	    noUiSlider.create(filterBar, {
	        start: [ 50, 200 ],
	        connect: true,
	        range: {
	            'min': 50,
	            'max': 200
	        }
	    });

	    var skipValues = [
	    document.getElementById('value-lower'),
	    document.getElementById('value-upper')
	    ];

	    filterBar.noUiSlider.on('update', function( values, handle ) {
	        skipValues[handle].innerHTML = Math.round(values[handle]) ;
	    });
        
      
    
 <?php }else { ?>   
        
       
        
<?php } ?>        
        
//ajax for loading mini cart
  $(document).ready(function(){      
    var dataInput;
    $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/getCartCount",
                data: dataInput,                
                success:function(dataCount){
                    $('#cartCounter').html(dataCount);
                }
    });
    $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/ajax_mini_cart",
                data: dataInput,
                beforeSend:function(html){
                    $('.loading-overlay').show();
                },
                success:function(cartDetails){
                    $('.loading-overlay').hide();
                    $('#ajaxMiniCart').html(cartDetails);
                }
    });
 });      
        
</script>
<!--===============================================================================================-->
	<?php echo $this->Html->script('fashe/main.js'); ?>

</body>
</html>
