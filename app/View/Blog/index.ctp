<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Blog</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Blog</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">

	<!-- Blog Posts -->
	<div class="blog-page">
	<div class="row">


		<!-- Post Content -->
		<div class="col-lg-9 col-md-8 padding-right-30">

        <?php foreach($blogPosts as $postData) { ?>
			<!-- Blog Post -->
             <a href="<?php echo Configure::read('app_root_path')."blog/post/".$postData['BlogPost']['id']; ?>" >
			<div class="blog-post single-post">
				
               
				<!-- Img -->
				<img class="post-img" src="<?php echo Configure::read('app_root_path').$postData['BlogPost']['file_path']."/".$postData['BlogPost']['file_name']; ?>" alt="" style="height: 343px;width: 773px;">
             
				
				<!-- Content -->
				<div class="post-content">

					<h3><?php echo $postData['BlogPost']['title']; ?></h3>

					<ul class="post-meta">
						<li><?php echo date("F d, Y",strtotime($postData['BlogPost']['posted_date'])); ?></li>
						<li>Posted By: <?php echo ucfirst($postData['BlogPost']['posted_by']); ?></li>
						
					</ul>
                    <div style="    text-align: justify;">
                    <?php
                            $content = preg_replace("/<img[^>]+\>/i", "(image) ", $postData['BlogPost']['content']);  
                            $content=trim($postData['BlogPost']['content']);
							$content = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($content))))));					
							echo substr ($content,0,400);                    
                      ?>
                    </div>
					


					<div class="clearfix"></div>

				</div>
			</div>
               </a>
          <?php } ?>  
            
			<!-- Blog Post / End -->


			<!-- Post Navigation -->
<!--
			<ul id="posts-nav" class="margin-top-0 margin-bottom-45">
				<li class="next-post">
					<a href="#"><span>Next Post</span>
					The Best Cofee Shops In Sydney Neighborhoods</a>
				</li>
				<li class="prev-post">
					<a href="#"><span>Previous Post</span>
					Hotels for All Budgets</a>
				</li>
			</ul>
-->




			<!-- Related Posts -->
			<div class="clearfix"></div>
			<h4 class="headline margin-top-25">Related Posts</h4>
			<div class="row">

				<!-- Blog Post Item -->
				<?php foreach($realatedPosts as $posts){ ?>
				<div class="col-md-6">
					<a href="<?php echo Configure::read('app_root_path')."blog/post/".$posts['BlogPost']['id']; ?>" class="blog-compact-item-container">
						<div class="blog-compact-item">
							<img src="<?php echo Configure::read('app_root_path').$posts['BlogPost']['file_path']."/".$posts['BlogPost']['file_name']; ?>" alt="">
							<span class="blog-item-tag"  style="font-weight: bold;">Posted By: <?php echo ucfirst($posts['BlogPost']['posted_by']); ?></span>
							<div class="blog-compact-item-content">
								<ul class="blog-post-tags">
									<li><?php echo date("F d, Y",strtotime($posts['BlogPost']['posted_date'])); ?></li>
								</ul>
								<h3><?php echo $posts['BlogPost']['title']; ?></h3>
								<p><?php $content = preg_replace("/<img[^>]+\>/i", "(image) ", $posts['BlogPost']['content']);   
									$content = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($content))))));
										echo substr ($content,0,200);	
									?></p>
							</div>
						</div>
					</a>
				</div>
				
				<!-- Blog post Item / End -->
				<?php } ?>
				

			</div>
			<!-- Related Posts / End -->


			<div class="margin-top-50"></div>

		<div class="clearfix"></div>


	</div>
	<!-- Content / End -->



	<!-- Widgets -->
	<div class="col-lg-3 col-md-4">
		<div class="sidebar right">
			<!-- Widget -->
			<div class="widget margin-top-40">

				<h3>Popular Posts</h3>
				<ul class="widget-tabs">
					<?php foreach($sideWidgetPosts as $posts){ ?>
					
					<!-- Post #1 -->
					<li>
						<div class="widget-content">
								<div class="widget-thumb">
								<a href="<?php echo Configure::read('app_root_path')."blog/post/".$posts['BlogPost']['id']; ?>"><img src="<?php echo Configure::read('app_root_path').$posts['BlogPost']['file_path']."/".$posts['BlogPost']['file_name']; ?>" alt=""></a>
							</div>
							
							<div class="widget-text">
								<h5><a href="<?php echo Configure::read('app_root_path')."blog/post/".$posts['BlogPost']['id']; ?>"><?php echo $posts['BlogPost']['title']; ?> </a></h5>
								<span><?php echo date("F d, Y",strtotime($posts['BlogPost']['posted_date'])); ?></span>
							</div>
							<div class="clearfix"></div>
						</div>
					</li>
					
				<?php } ?>

				</ul>

			</div>
			<!-- Widget / End-->



			<div class="clearfix"></div>
			<div class="margin-bottom-40"></div>
		</div>
	</div>
	
		
  </div>
	<!-- Sidebar / End -->


</div>
</div>
