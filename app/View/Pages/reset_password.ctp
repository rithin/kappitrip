
<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Reset Password</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Reset Password</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">

	   <div class="row">

		<!-- Contact Details -->
		<div class="col-md-4">

			<h4 class="headline margin-bottom-30">Find Us There</h4>

			<!-- Contact Details -->
			<div class="sidebar-textbox">
				<p>If you are facing any trouble resetting the password, please contact our customer care. Coorgexpress.com will be happy to help you 24/7.</p>

				<ul class="contact-details">
					<li><i class="im im-icon-Phone-2"></i> <strong>Phone:</strong> <span>+91 96323 38111 / +91 97418 30222 </span></li>
					<li><i class="im im-icon-Globe"></i> <strong>Web:</strong> <span><a href="https://www.coorgexpress.com">www.coorgexpress.com</a></span></li>
					<li><i class="im im-icon-Envelope"></i> <strong>E-Mail:</strong> <span><a href="mailto:info@coorgexpress.com"> info@coorgexpress.com  </a></span></li>
				</ul>
			</div>

		</div>

		<!-- Contact Form -->
		<div class="col-md-8">

			<section id="contact">
				<h4 class="headline margin-bottom-35"  style="margin-left: 200px;">Reset Password</h4>

				<div id="contact-message"></div> 
                <span style="color:red; display:none;" id="displayMessageFrm"></span>

					 <?php                         
                            $action = Configure::read('app_root_path')."users/reset_password";
                            echo $this->Form->create('UserReset', array('id'=>'customer_login', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','autocomplete'=>'off','class'=>'form-horizontal ','name'=>'formSignin', 'novalidate' => true));  
                
                            echo $this->Form->input('user_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'user_id','autofocus','error'=>false,'value'=>$user_id));
                            echo $this->Form->input('token',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'token','autofocus','error'=>false,'value'=>$token));
                             echo $this->Form->input('username',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'username','autofocus','error'=>false,'value'=>$email_id));
                            echo $this->Form->input('user_reset_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'user_reset_id','autofocus','error'=>false,'value'=>$user_reset_id));
                         ?>  

                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <span style="color:red;" id="errorMessage"> </span>
                                <?php
                                if($error_flag=='invalid_token') { 
                                    echo  '<span style="color:red;" id="displayMessage">Invalid Token, please request for the password reset again and follow the instructions in the email. </span>';
                                }
                                if($error_flag=='') { ?>
                                <div id="reset_pass">
                                    <div class="col-md-12" id="">
                                        <div>
                                            <?php 
                                             echo $this->Form->input('password',array('div'=>false, 'type'=>'password', 'label'=>false, 'id'=>'password','autofocus','required'=>true,'placeholder'=>'Password','error'=>false));
                                            ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12" id="">
                                        <div>

                                            <?php 
                                             echo $this->Form->input('retype_password',array('div'=>false, 'type'=>'password', 'label'=>false, 'id'=>'retype_password','autofocus','required'=>true,'placeholder'=>'Re Type Password','error'=>false));
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                 
                            </div>
                            
                             <div class="col-md-3">
                            </div>
                      </div>
                        <?php if($error_flag=='') { ?>
                      <div class="row" id="reset_pass_btn">
                          <div class="col-md-3"> </div>
                          <div class="col-md-6">
                               <input type="button" class="submit button" id="submit" value="Reset Password" onclick="javascript:password_reset()" >
                          </div>
                          <div class="col-md-3"> </div>
                    </div>
                <?php } ?>
					<?php echo $this->Form->end(); ?>  
			</section>
		</div>
		<!-- Contact Form / End -->

	</div>
</div>

<script>

function password_reset(){
   
    if($('#password').val()==""){
        $('#errorMessage').html('Please enter a valid password.');
    }
    else if($('#retype_password').val()==""){
        $('#errorMessage').html('Please enter a valid password.');
    }
    else if($('#password').val()!=$('#retype_password').val()){
      
         $('#errorMessage').html('Password and Confirm Password are not matching.');
    }
    else{
        $('#errorMessage').hide();
         var password=$('#password').val();   
         var token=$('#token').val();
         var user_id=$('#user_id').val();   
         var username=$('#username').val();
        var user_reset_id=$('#user_reset_id').val();
         resetPassDataString={"password":password,"token":token,"user_id":user_id,"username":username,"user_reset_id":user_reset_id};
        var resetPassDataJson=JSON.stringify(resetPassDataString);
        $.ajax
        ({
             type: "POST",
             url: "<?php echo Configure::read('app_root_path'); ?>users/reset_password",
             data: "resetPassData="+resetPassDataJson, 
             dataType:'json',
             success: function(resetPassData)
             {
                 // console.log(resetPassData);
                 $('#displayMessageFrm').show();
                if(resetPassData.response=="success"){
                   // console.log('success');
                     $('#reset_pass').hide(); 
                     $('#reset_pass_btn').hide(); 
                     $('#displayMessageFrm').css('color','green');
                     $('#displayMessageFrm').css('font-weight','bold');
                     $('#displayMessageFrm').html('Your password has been updated successfully. Please click <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim" style="text-decoration:none;"><i class="im im-icon-Hand-Touch"></i> Sign In</a> to login again ');
                }
                if(resetPassData.response=="failed"){
                   //  console.log('failed');
                     $('#reset_pass').hide();  
                     $('#reset_pass_btn').hide(); 
                     $('#displayMessageFrm').html('Oops, Something wrong. Please request for password reset again and follow the instructions in email.')
                 }
                 // $('#displayMessage').html('sdfds');
               //  $('#contact-message').html('this is a test');
                 
             }
        }); 
        
    }

}


</script>




<style>
.widget-tabs li {
	padding: 0 0 12px 0 !important;
    margin: 4px 0 0 0 !important;
	
}
.list_team {
    list-style-type: square;
    color: red;
}
.widget h3 {
	color:red;
}
.blog-post{
	text-align: justify;
	}
.post-meta li{
		float:left !important;
	}
.col-lg-3, .col-md-4{
		padding-right: 0px !important; 
	}
</style>
