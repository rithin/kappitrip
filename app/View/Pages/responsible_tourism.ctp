
<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Responsible Travel</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Responsible Travel</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">

	<!-- Blog Posts -->
	<div class="blog-page">
	<div class="row">


		<!-- Post Content -->
		<div class="col-lg-9 col-md-8 padding-right-30">


			<!-- Blog Post -->
			<div class="blog-post single-post">
				
				<!-- Img -->
				<img class="post-img" src="<?php echo Configure::read('app_root_path'); ?>images/Responsible-tourism-banner.jpg" alt="" style="max-height:300px;" >

				
				<!-- Content -->
				<div class="post-content">

					<h3>Travel Conduct Policy. A Clean Coorg Initiative.</h3>

				

					<p>We follow a strict <b>“ Leave No Trace ”</b> policy while visiting Coorg/Western Ghats.</p>

					<h4 class="headline margin-top-25">Plan ahead & Prepare</h4>

					<p>
						<ul class="list-2">
							<li>Coorg/Western Ghats is a eco sensitive area .Plan your trip by thinking about how it will impact the environment and also how it may affect others as well. </li>
						</ul>					
					</p>
					
					<h4 class="headline margin-top-25">Dispose of Waste Properly</h4>

					<p>
						<ul class="list-2">
							<li>You are responsible for anything you bring into the place. Carry out all your trash.</li>
							<li>Pick up trash that others may have missed or that were dropped by accident.</li>
						</ul>					
					</p>
			
					<h4 class="headline margin-top-25">Minimize impact</h4>

					<p>
						<ul class="list-2">
							<li>Wherever possible, avoid the use of plastic bags and disposable plastic water bottles.</li>
							<li>Do not remove/Tamper objects from the natural environment or any site you visit.</li>
							<li>Take only pictures, leave only the lightest of footprints, and bring home only memories.</li>
						</ul>					
					</p>
		
					<h4 class="headline margin-top-25">Respect People, Place & Privacy</h4>

					<p>
						Travelers are expected to respect others. This includes other travellers, Coorgexpress staff, third-party service providers and the local people and cultures during the visit.
					</p>
					
					<h4 class="headline margin-top-25">Responsible Travel with Indigenous People </h4>

					<p>
						Connecting curious travellers with best local experiences of the Indigenous communities is an essential part of our identity, apart from tourism, we also promote the artisan’s products through our marketplace to which supports their well-being.
					</p>
		
					<h4 class="headline margin-top-25">Respect Wildlife</h4>

					<p>
						<ul class="list-2">
							<li>Observe wildlife from a distance.</li>
							<li>Do Not Feed Animals.</li>
						</ul>					
					</p>
					
					<div class="clearfix"></div>

				</div>
			</div>
			<!-- Blog Post / End -->


			<!-- Post Navigation -->
			<ul id="posts-nav" class="margin-top-0 margin-bottom-45">
				<li class="next-post">
					<a href="<?php echo Configure::read('app_root_path'); ?>pages/our_story"><span>Our Story</span>
					Togther Everyone Achieves More</a>
				</li>
				<li class="prev-post">
					<a href="<?php echo Configure::read('app_root_path'); ?>pages/about_coorg"><span>About Coorg</span>
					Coorg- All about its People, Coffee, Culture and awesome Weather</a>
				</li>
			</ul>


			


			<!-- Related Posts -->
			<div class="clearfix"></div>


			<div class="margin-top-50"></div>

	</div>
	<!-- Content / End -->



	<!-- Widgets -->
	<div class="col-lg-3 col-md-4">
		<div class="sidebar right">

			<!-- Widget -->
			<div class="widget margin-top-40">
				<h3>Got any questions?</h3>
				<div class="info-box margin-bottom-10">
					<p>Having any questions? Feel free to ask!</p>
					<a href="mailto:info@coorgexpress.com" class="button fullwidth margin-top-20"><i class="fa fa-envelope-o"></i> Drop Us a Line</a>
				</div>
			</div>
			<!-- Widget / End -->


			<!-- Widget -->
			<div class="widget margin-top-40">

				<h3>Our Team</h3>
				<ul class="list_team">

					<!-- Post #1 -->
					<li>
						<h4>Ricky Monnappa</h4>
						<ul class="post-meta">
							<li>Founder</li>
							<li>Sports Freak</li>
							<li>Hockey Lover</li>
							<li>Avid Trekker</li>
							<li>Coffee Lover</li>
							<li>Dreamer</li>
						</ul>
					</li>
					<div class="clearfix"></div>
					<!-- Post #2 -->
					<li>
						<h4>Sahanaa Nayak</h4>

						<ul class="post-meta">
							<li>Coastal Gypsy</li>
							<li>Tea Addict</li>
							<li>Pencil Lover</li>
							<li>Opinionated</li>
							<li>Communication Expert </li> 
							<li>Content Wizard</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					
					<!-- Post #3 -->
					<div class="clearfix"></div>
					<li>
						<h4>Likith Ponnanna</h4>

						<ul class="post-meta">
							<li>Wanderer</li>
							<li>Avid Trekker</li>
							<li>Hiking Enthusiast</li>
							<li>Multitasker</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					<div class="clearfix"></div>
					<li>
						<h4>Karan Subbaiah</h4>

						<ul class="post-meta">
							<li>Cyclist</li>
							<li>Avid Trekker</li>
							<li>Experience Coordinator</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					
					

				</ul>

			</div>
			<!-- Widget / End-->
<div class="clearfix"></div>

			<!-- Widget -->
			<div class="widget margin-top-40">
				<h3 class="margin-bottom-25">Social</h3>
				<ul class="social-icons rounded">
					<li><a class="facebook" href="https://www.facebook.com/coorgexpress/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/coorgexpress" target="_blank"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="https://plus.google.com/+Coorgexpress" target="_blank"><i class="icon-gplus"></i></a></li>
					<li><a class="instagram" href="https://www.instagram.com/coorgexpress/" target="_blank"><i class="icon-instagram"></i></a></li>
				</ul>

			</div>
			<!-- Widget / End-->

			<div class="clearfix"></div>
			<div class="margin-bottom-40"></div>
		</div>
	</div>
	</div>
	<!-- Sidebar / End -->


</div>
</div>
<style>

.widget-tabs li {
	padding: 0 0 12px 0 !important;
    margin: 4px 0 0 0 !important;
	
}
.list_team {
    list-style-type: square;
    color: red;
}
.widget h3 {
	color:red;
}
.blog-post{
	text-align: justify;
	}
.post-meta li{
		float:left !important;
	}
.col-lg-3, .col-md-4{
		padding-right: 0px !important; 
	}
</style>