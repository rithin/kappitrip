
<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>About Coorg</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>About Coorg</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">

	<!-- Blog Posts -->
	<div class="blog-page">
	<div class="row">


		<!-- Post Content -->
		<div class="col-lg-9 col-md-8 padding-right-30">


			<!-- Blog Post -->
			<div class="blog-post single-post">
				
				<!-- Img -->
				<img class="post-img" src="<?php echo Configure::read('app_root_path'); ?>images/about-coorg-banner.jpg" alt="" style="max-height:300px;" >

				
				<!-- Content -->
				<div class="post-content">

					<h3>Coorg- All about its People, Coffee, Culture and awesome Weather</h3>

				

					<p>The wonder that India is elegantly bestowed is with number of hill stations that can offer you respite from the blazing heat and giving you rejuvenating and inspiring experience amidst the comforting environments of the hills and some very pleasing people to host you.  Coorg calls...!  Take a time off and explore this little land flourishing in all its abundance of nature and tranquillity.</p>

					<div class="post-quote">
						<span class="icon"></span>
						<blockquote>
							Kodagu or the anglicized Coorg was once hailed as the “Scotland of India “.The tiny district in southern Karnataka is a rich agricultural Land .With an Ancient heritage that has influences of to the erstwhile colonial Rule.
						</blockquote>
					</div>

					<p>The land of Coorg is a different facet of India: unsullied, beautiful, green, and exciting with the lovely blue Sahyadri Mountains, dense forested valleys, gurgling mountain streams and waterfalls. It is the land of coffee resplendent with red berries, cardamom fields covered with white and purple blossoms, long stretches of paddy fields, old silver oak trees laden with huge hives, and of a sturdy handsome people called the Coorgs, who charm by their hospitality and cuisine, their intriguing customs and the reverence with which they worship their river, the Cauvery. </p>
					
					<p>The hill station of Coorg in the Western Ghats of Karnataka unveils like a Pandora box, where there is something for everyone. From the nature buffs to tranquillity loving folks, religiously inclined to honeymooners and audacious souls, Coorg is an ideal place for everyone. Coorg due to its location has various sightseeing places and adventure activities for adrenaline junkies. It can also be your quiet escape.Trekking,Zip line rafting , Kayaking , Coffee plantation tours ,Experiential kodava Dining , jungle safari , sport fishing camping you’ve got it all . In addition, there are number of traveller attractions in vicinity like Madikeri Fort, Abbi Falls, Iruppu Falls, Talakaveri, Bhagamandala, Padi Iggutappa Temple, Somwarpet, Little Tibet (Tibetan refugee's colony). To make your stay in Coorg unforgettable we have number of adventures and experience Packages minutely crafted to suit the new as well as the frequent travellers. </p>
					<p>
					Coorg has a pleasant climate throughout the year; the best time to visit Coorg depends on what activities you would like to do. Rain in Coorg lasts from June to October, during this time the vegetation is lush green, the streams and waterfalls are full. This is the time to go rafting. Trekking and camping become is done on the drier months of year.
					</p>

					<h4>Ethnic Kodavas</h4>
					<p>
						The Kodavas or the Coorgs are primarily a warrior community. Over the centuries, many of the Royal dynasties of South India, Including the Kadambas, the Gangas, the cholas, and the hoysalas ruled the Kodagu region, influencing the original culture and customs of the Kodavas.The Kodavas have immensely contributed to the Indian Army and to the game of field Hockey <br>
						The Kodavas are worshipers of ancestors and traditionally celebrate festivals.

					</p>

					<h4 class="headline margin-top-25">Land of rich produce</h4>
					<ul class="list-4">
						<li><b>Coffee - </b>The World’s Best Shade-Grown ’Mild’ Coffee. Coorg coffee is the most extraordinary of beverages, offering intriguing subtlety and stimulating intensity. India is the only country that grows all of its coffee under shade. Typically mild and not too acidic, these coffees possess an exotic full-bodied taste and a fine aroma. Coorg produces more coffee — Arabica and Robusta, the two main varieties of beans grown for consumption.</li>
						<li><b>Spices - </b>Pepper & Cardamom are two widely grown spices in Coorg. Apart from these, Bird Eye Chili usually grown wild in the coffee states and a basic ingredient    to the all the local delicacies.</li>
						
						<li><b>Fruits - </b>Rain forest fruits like Oranges, Bitter lime, Avocados, Jack fruit, Rain forest Plum and many more are found plenty in Coorg.</li>
						<li><b>Honey - </b>Coorg Honey is one of the best in India. Apart from extracting  honey by beekeeping Coorg honey is also extracted from the wild Woods.</li>
					</ul>
					
					
					<div class="clearfix"></div>

				</div>
			</div>
			<!-- Blog Post / End -->


			<!-- Post Navigation -->
			<ul id="posts-nav" class="margin-top-0 margin-bottom-45">
				<li class="next-post">
					<a href="<?php echo Configure::read('app_root_path'); ?>pages/our_story"><span>Our Story</span>
					Togther Everyone Achieves More</a>
				</li>
				<li class="prev-post">
					<a href="<?php echo Configure::read('app_root_path'); ?>pages/responsible_tourism"><span>Responsible Tourism</span>
					 A Clean Coorg Initiative</a>
				</li>
			</ul>


			


			<!-- Related Posts -->
			<div class="clearfix"></div>


			<div class="margin-top-50"></div>

	</div>
	<!-- Content / End -->



	<!-- Widgets -->
	<div class="col-lg-3 col-md-4">
		<div class="sidebar right">

			<!-- Widget -->
			<div class="widget margin-top-40">
				<h3>Got any questions?</h3>
				<div class="info-box margin-bottom-10">
					<p>Having any questions? Feel free to ask!</p>
					<a href="mailto:info@coorgexpress.com" class="button fullwidth margin-top-20"><i class="fa fa-envelope-o"></i> Drop Us a Line</a>
				</div>
			</div>
			<!-- Widget / End -->


			<!-- Widget -->
			<div class="widget margin-top-40">

				<h3>Kodavas Festivals</h3>
				<ul class="widget-tabs">

					<!-- Post #1 -->
					<li>
						<h5>Kailpodh (Festival of Arms) </h5>
					</li>
					
					<!-- Post #2 -->
					<li>
						<h5>Kaveri Sankramana (Worship of river Kaveri)</h5>
					</li>
					
					<!-- Post #3 -->
					<li>
						<h5>Puttari (Harvest festival)</h5>
					</li>
					
					<li>
						<h5>Bhagavathi Festival (Annual Village Festival)</h5>
					</li>
					
					<li>
						<h5>Bodd Namme (Festival)</h5>
					</li>

				</ul>

			</div>
			<!-- Widget / End-->


			<!-- Widget -->
			<div class="widget margin-top-40">
				<h3 class="margin-bottom-25">Social</h3>
				<ul class="social-icons rounded">
					<li><a class="facebook" href="https://www.facebook.com/coorgexpress/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/coorgexpress" target="_blank"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="https://plus.google.com/+Coorgexpress" target="_blank"><i class="icon-gplus"></i></a></li>
					<li><a class="instagram" href="https://www.instagram.com/coorgexpress/" target="_blank"><i class="icon-instagram"></i></a></li>
				</ul>

			</div>
			<!-- Widget / End-->

			<div class="clearfix"></div>
			<div class="margin-bottom-40"></div>
		</div>
	</div>
	</div>
	<!-- Sidebar / End -->


</div>
</div>
<style>

.widget-tabs li {
	padding: 0 0 12px 0 !important;
    margin: 4px 0 0 0 !important;
	
}
.widget-tabs {
    list-style-type: square;
    color: red;
}
.widget h3 {
	color:red;
}
.blog-post{
	text-align: justify;
	}
.col-lg-3, .col-md-4{
		padding-right: 0px !important; 
	}
</style>