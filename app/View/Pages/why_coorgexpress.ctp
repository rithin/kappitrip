
<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Why Coorgexpress?</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Why Coorgexpress</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">

	<!-- Blog Posts -->
	<div class="blog-page">
	<div class="row">


		<!-- Post Content -->
		<div class="col-lg-9 col-md-8 padding-right-30">


			<!-- Blog Post -->
			<div class="blog-post single-post">
				
				<!-- Img -->
				<img class="post-img" src="<?php echo Configure::read('app_root_path'); ?>images/why-coorgexpress-banner.jpg" alt="" style="max-height:300px;" >

				
				<!-- Content -->
				<div class="post-content">

					<h3>Stays, Wilderness, Adventures and Experiences</h3>

				

					<p>Not just for great price and value,You should be on Coorgexpress.com because we showcase:</p>

					<h4 class="headline margin-top-25">Handpicked Stays </h4>

					<p>The SCOTLAND OF INDIA offers unique home stays and hotels to hide amidst the mist kissed coffee estates in tranquillity, our options will provide you with the most authentic experience possible and will always reflect the local character and spirit of your stay. Drink freshly brewed coffee from the backyard and unwind at dusk with local folk music & bonfire sipping on country made wine. </p>
					
					<div class="post-quote">
						<span class="icon"></span>
						<blockquote>
							Each Stay listed is personally visited & verified and met our standards.
						</blockquote>
					</div>
					
					<h4 class="headline margin-top-25">Fascinating Wilderness </h4>

					<p>
						Come get lost in the most amazing stretches of forest land the country has to offer, NAGARHOLE & DUBARE with their vast expanse of woodland and a haven for Wildlife. Come stalk the elusive big cat, see the tusker at Dubare and get intimate and up close with it or lose yourself in the stunningly beautiful bird life.
					</p>
					
					<div class="post-quote">
						<span class="icon"></span>
						<blockquote>
							Our Nature tours are conducted be expert naturalist with in depth knowledge.
						</blockquote>
					</div>					
					
					
					<h4 class="headline margin-top-25">Unbelievable Adventures </h4>

					<p>
						Come with your hiking shoes to stroll through some aromatic coffee plantation or get your Adeline fix through a host of adventure sports, from forest trekking to river rafting.
					</p>
					
					<div class="post-quote">
						<span class="icon"></span>
						<blockquote>
							We offer camping, Cycling, Trekking and other host of adventures to select from.
						</blockquote>
					</div>
					
					<h4 class="headline margin-top-25">Unique Experiences </h4>

					<p>
						We pioneer in the offbeat experiences and give a personalized touch to our travellers seeking exclusive Tours. We conduct tours such as Birding ,Cookery, Bee keeping & Other Unconventional experiences.
					</p>
					
					<div class="post-quote">
						<span class="icon"></span>
						<blockquote>
							Go local and experience best of Coorg.
						</blockquote>
					</div>
					
					
					<div class="clearfix"></div>

				</div>
			</div>
			<!-- Blog Post / End -->


			<!-- Post Navigation -->
			<ul id="posts-nav" class="margin-top-0 margin-bottom-45">
				<li class="prev-post">
					<a href="<?php echo Configure::read('app_root_path'); ?>pages/responsible_tourism"><span>Responsible Tourism</span>
					 A Clean Coorg Initiative</a>
				</li>
				<li class="next-post">
					<a href="<?php echo Configure::read('app_root_path'); ?>pages/our_story"><span>Our Story</span>
					Togther Everyone Achieves More</a>
				</li>
			</ul>


			


			<!-- Related Posts -->
			<div class="clearfix"></div>


			<div class="margin-top-50"></div>

	</div>
	<!-- Content / End -->



	<!-- Widgets -->
	<div class="col-lg-3 col-md-4">
		<div class="sidebar right">

			<!-- Widget -->
			<div class="widget margin-top-40">
				<h3>Got any questions?</h3>
				<div class="info-box margin-bottom-10">
					<p>Having any questions? Feel free to ask!</p>
					<a href="mailto:info@coorgexpress.com" class="button fullwidth margin-top-20"><i class="fa fa-envelope-o"></i> Drop Us a Line</a>
				</div>
			</div>
			<!-- Widget / End -->


			<!-- Widget -->
			<div class="widget margin-top-40">

				<h3>Our Team</h3>
				<ul class="list_team">

					<!-- Post #1 -->
					<li>
						<h4>Ricky Monnappa</h4>
						<ul class="post-meta">
							<li>Founder</li>
							<li>Sports Freak</li>
							<li>Hockey Lover</li>
							<li>Avid Trekker</li>
							<li>Coffee Lover</li>
							<li>Dreamer</li>
						</ul>
					</li>
					<div class="clearfix"></div>
					<!-- Post #2 -->
					<li>
						<h4>Sahanaa Nayak</h4>

						<ul class="post-meta">
							<li>Coastal Gypsy</li>
							<li>Tea Addict</li>
							<li>Pencil Lover</li>
							<li>Opinionated</li>
							<li>Communication Expert </li> 
							<li>Content Wizard</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					
					<!-- Post #3 -->
					<div class="clearfix"></div>
					<li>
						<h4>Likith Ponnanna</h4>

						<ul class="post-meta">
							<li>Wanderer</li>
							<li>Avid Trekker</li>
							<li>Hiking Enthusiast</li>
							<li>Multitasker</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					<div class="clearfix"></div>
					<li>
						<h4>Karan Subbaiah</h4>

						<ul class="post-meta">
							<li>Cyclist</li>
							<li>Avid Trekker</li>
							<li>Experience Coordinator</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					
					

				</ul>

			</div>


			<!-- Widget -->
			<div class="widget margin-top-40">
				<h3 class="margin-bottom-25">Social</h3>
				<ul class="social-icons rounded">
					<li><a class="facebook" href="https://www.facebook.com/coorgexpress/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/coorgexpress" target="_blank"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="https://plus.google.com/+Coorgexpress" target="_blank"><i class="icon-gplus"></i></a></li>
					<li><a class="instagram" href="https://www.instagram.com/coorgexpress/" target="_blank"><i class="icon-instagram"></i></a></li>
				</ul>

			</div>
			<!-- Widget / End-->

			<div class="clearfix"></div>
			<div class="margin-bottom-40"></div>
		</div>
	</div>
	</div>
	<!-- Sidebar / End -->


</div>
</div>
<style>

.widget-tabs li {
	padding: 0 0 12px 0 !important;
    margin: 4px 0 0 0 !important;
	
}
.widget-tabs {
    list-style-type: square;
    color: red;
}
.widget h3 {
	color:red;
}
.blog-post{
	text-align: justify;
	}
.col-lg-3, .col-md-4{
		padding-right: 0px !important; 
	}
</style>