
<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Our Story</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Our Story</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">

	<!-- Blog Posts -->
	<div class="blog-page">
	<div class="row">


		<!-- Post Content -->
		<div class="col-lg-9 col-md-8 padding-right-30">


			<!-- Blog Post -->
			<div class="blog-post single-post">
				
				<!-- Img -->
				<img class="post-img" src="<?php echo Configure::read('app_root_path'); ?>images/our-story-banner.jpg" alt="" style="max-height:300px;" >

				
				<!-- Content -->
				<div class="post-content">

					<h3>Togther Everyone Achieves More</h3>

				

					<p> <b>Coorgexpress</b>, that started out as a travel portal exclusively for Coorg has now found it calling in several other destinations of Western Ghats. Coorgexpress  was started by Ricky Monnappa, the founder of organisation as a final  year project for his master’s with a passion to change the way people travelled to Coorg.  </p>

					<div class="post-quote">
						<span class="icon"></span>
						<blockquote>
							We fix you a holiday, " a pretty good one we must say! ". 
						</blockquote>
					</div>

					<p>We, the team of Coorgexpress is vigorously into promoting the authentic cultural heritage of Coorg in terms of food, homemade wines and several other homemade products exclusively marketed to encourage and bring into picture the best and rich produce of this land. Coorgexpress has been actively participating in sharing cultural and social responsibilities. </p>
					
					<p>
					We have also undertaken an initiative of Clean Coorg Campaign to create awareness not only about cleanliness around Coorg, but also to make people (both hosts and tourists) aware of the responsible and sustainable tourism to help conserve the monuments, the environment, the wildlife and the holy Kaveri.
					Unlike other travel portals we not only thrive in giving out the best of stays, but we also bring the best of adventures and experiences to our customers looking for a perfect vacation. The very essence of the land of Coorg, the nature can be experienced in its best form through various adventure activities exclusively hosted by us. The stays are handpicked and experienced by the team and qualify as the best around Coorg to make your stay well worth it. We aim at bringing Coorg and its exquisiteness on the World tourism.
 					</p>
					
					
					<div class="clearfix"></div>

				</div>
			</div>
			<!-- Blog Post / End -->


					<!-- Post Navigation -->
			<ul id="posts-nav" class="margin-top-0 margin-bottom-45">
				<li class="prev-post">
					<a href="<?php echo Configure::read('app_root_path'); ?>pages/responsible_tourism"><span>Responsible Tourism</span>
					 A Clean Coorg Initiative</a>
				</li>
				<li class="next-post">
					<a href="<?php echo Configure::read('app_root_path'); ?>pages/why_coorgexpress">
						<span>Why Coorgexpress</span>
					Stays, Wilderness, Adventures and Experiences</a>
				</li>
			</ul>


			


			<!-- Related Posts -->
			<div class="clearfix"></div>


			<div class="margin-top-50"></div>

	</div>
	<!-- Content / End -->



	<!-- Widgets -->
	<div class="col-lg-3 col-md-4">
		<div class="sidebar right">

			<!-- Widget -->
			<div class="widget margin-top-40">
				<h3>Got any questions?</h3>
				<div class="info-box margin-bottom-10">
					<p>Having any questions? Feel free to ask!</p>
					<a href="mailto:info@coorgexpress.com" class="button fullwidth margin-top-20"><i class="fa fa-envelope-o"></i> Drop Us a Line</a>
				</div>
			</div>
			<!-- Widget / End -->


			<!-- Widget -->
			<div class="widget margin-top-40">

				<h3>Our Team</h3>
				<ul class="list_team">

					<!-- Post #1 -->
					<li>
						<h4>Ricky Monnappa</h4>
						<ul class="post-meta">
							<li>Founder</li>
							<li>Sports Freak</li>
							<li>Hockey Lover</li>
							<li>Avid Trekker</li>
							<li>Coffee Lover</li>
							<li>Dreamer</li>
						</ul>
					</li>
					<div class="clearfix"></div>
					<!-- Post #2 -->
					<li>
						<h4>Sahanaa Nayak</h4>

						<ul class="post-meta">
							<li>Coastal Gypsy</li>
							<li>Tea Addict</li>
							<li>Pencil Lover</li>
							<li>Opinionated</li>
							<li>Communication Expert </li> 
							<li>Content Wizard</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					
					<!-- Post #3 -->
					<div class="clearfix"></div>
					<li>
						<h4>Likith Ponnanna</h4>

						<ul class="post-meta">
							<li>Wanderer</li>
							<li>Avid Trekker</li>
							<li>Hiking Enthusiast</li>
							<li>Multitasker</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					<div class="clearfix"></div>
					<li>
						<h4>Karan Subbaiah</h4>

						<ul class="post-meta">
							<li>Cyclist</li>
							<li>Avid Trekker</li>
							<li>Experience Coordinator</li>
							<li>True Coorgexpresser</li>
						</ul>
					</li>
					
					

				</ul>

			</div>
			<!-- Widget / End-->
				<div class="clearfix"></div>

			<!-- Widget -->
			<div class="widget margin-top-40">
				<h3 class="margin-bottom-25">Social</h3>
				<ul class="social-icons rounded">
					<li><a class="facebook" href="https://www.facebook.com/coorgexpress/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/coorgexpress" target="_blank"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="https://plus.google.com/+Coorgexpress" target="_blank"><i class="icon-gplus"></i></a></li>
					<li><a class="instagram" href="https://www.instagram.com/coorgexpress/" target="_blank"><i class="icon-instagram"></i></a></li>
				</ul>

			</div>
			<!-- Widget / End-->

			<div class="clearfix"></div>
			<div class="margin-bottom-40"></div>
		</div>
	</div>
	</div>
	<!-- Sidebar / End -->


</div>
</div>
<style>
.widget-tabs li {
	padding: 0 0 12px 0 !important;
    margin: 4px 0 0 0 !important;
	
}
.list_team {
    list-style-type: square;
    color: red;
}
.widget h3 {
	color:red;
}
.blog-post{
	text-align: justify;
	}
.post-meta li{
		float:left !important;
	}
.col-lg-3, .col-md-4{
		padding-right: 0px !important; 
	}
</style>