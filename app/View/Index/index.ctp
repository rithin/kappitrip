
<?php echo $this->Html->css('auto_complete.css'); ?>
<?php echo $this->Html->css('style-wowslider.css'); ?>


<!--   ===========================================   Banner   ================================================== -->
<div class="main-search-container" data-background-image="images/main_banners/pexels-photo-434213.jpg" >
<!--	 data-background-image="images/main_banners/main-kapptrip-banner02.jpg"-->
	
<!--
	<div id="wowslider-container1">
		<div class="ws_images">
		 <ul>
			 <?php //foreach($sliders as $item){ ?>
			<li><img src="<?php //echo Configure::read('app_root_path').$item['Slider']['file_path']."/".$item['Slider']['file_name']; ?>" alt="<?php// echo $item['Slider']['title']; ?>" title="<?php// echo $item['Slider']['description']; ?>" id="wows_<?php //echo $item['Slider']['sort_order']; ?>" /></li>
			 <?php // } ?>
			
		 </ul>
		</div>
		<div class="ws_shadow"></div>
	</div>
		
-->
		
	<div class="main-search-inner" >
<!--style="z-index:999; top:-75%;"-->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 style="text-transform: uppercase;  font-weight: bold; color: #3c1813;">Stays, Coffee and Experiences.</h3>
<?php                         
                    $action = Configure::read('app_root_path')."pages/searchDirection";
                  echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file', 'novalidate' => true)); 
               ?>  
					<div class="main-search-input">
 				
						<div class="main-search-input-item">
<!--							<input type="text" placeholder="What are you looking for?" value=""/>-->
							<input type="text" name="text_search" placeholder="Explore top rated stays, tours,activities and more.." id="autocomplete" />
<!--                            style="position: absolute; z-index: 2; background: transparent;"
            <input type="text" name="country" id="autocomplete-ajax-x" disabled="disabled" style="color: #CCC; position: absolute; background: transparent; z-index: 1;"/>-->
							<input type="hidden" name="search_type" id="search_type" value=""/>
							<input type="hidden" name="search_field" id="search_field" value=""/>
							<input type="hidden" name="search_field_value" id="search_field_value" value=""/>
							
							
						</div>
                       <div class="clearfix"></div>
						<button class="button" >Search</button>

					</div>
					  <?php echo $this->Form->end(); ?>  
				</div>
			</div>
		</div>

	</div>
</div>


<!-- Content
================================================== -->


	<div class="container category-baner-wrap  margin-top-50">
					<div class="row">
						<div class="col-md-4">
							<a href="<?php echo  Configure::read('app_root_path')."plantation-stays" ?>">
							<div class="category-baner category-baner-1">
								<div class="category-baner-text">
									<h2>Estate Stays!</h2>
									<h3>incredible stays</h3>
<!--									<a href="<?php echo  Configure::read('app_root_path')."plantation-stays" ?>" class="button">book now</a>-->
								</div>
							</div>
								</a>
						</div>
						<div class="col-md-4">	
							<a href="<?php echo  Configure::read('app_root_path')."plantation-stays" ?>">
							<div class="category-baner category-baner-2">
								<div class="category-baner-text">
									<h2>Farm Tours!</h2>
									<h3>Astonishing Experiences</h3>
<!--									<a href="<?php echo  Configure::read('app_root_path')."handpicked-experiences" ?>" class="button">book now</a>-->
								</div>
							</div>	
								</a>
						</div>
						<div class="col-md-4">
							<a href="<?php echo  Configure::read('app_root_path')."plantation-stays" ?>">
							<div class="category-baner category-baner-3">
								<div class="category-baner-text">
									<h2>Products!</h2>
									<h3>All of Coffee</h3>
<!--									<a href="<?php echo  Configure::read('app_root_path')."shop" ?>" class="button">shop now</a>-->
								</div>
							</div>
							</a>
						</div>
			</div>
	</div>





<!-- Fullwidth Section Listing homes Start -->
<section class="fullwidth margin-top-10 padding-top-15 padding-bottom-10" data-background-color="#ffff">

	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<h2 class="headline left margin-bottom-15">
					Stays
				<span>View our <i> handpicked </i> stay</span>
				</h2>
			</div>
		</div>
	</div>

	<!-- Carousel / Start -->
	<div class="simple-fw-slick-carousel dots-nav" role="toolbar">
<?php foreach($homestayList as $item){ ?>
		
		<!-- Listing Item -->
				<div class="col-lg-4 col-md-6" style="margin-bottom:20px;">
					<a href="<?php echo Configure::read('app_root_path')."plantation-stays/".$item['Homestay']['seo_unique_url'];?>" class="listing-item-container compact" data-marker-id="3" style="margin-bottom:10px !important; text-decoration: none;     overflow: hidden;" >
                     
                    <?php if($item['Homestay']['high_demand']=='Yes'){ ?>
                       <div class="listing-badge now-open">High Demand</div> 
                        
                    <?php }?>
                        
						<div class="listing-item">
							<img src="<?php echo Configure::read('app_root_path').$item['HomestayFile']['0']['file_path']."/".$item['HomestayFile']['0']['file_name']; ?>" alt="" >


						</div>
					
					<figcaption title="<?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Homestay']['description']),0,100)), 0, 10))." ....";?>" style=" padding: 5px;   color: #333; position: relative; z-index: 3; ">
						  <span class="caption-title"> 
							<span class="pull-right ng-binding">₹<?php echo number_format($item['Homestay']['base_price'],2); ?></span>
							<span class="text-ellipsis ng-binding"><?php echo $item['Homestay']['name']; ?></span>
						  </span>
						<div class="clearfix"></div>
						  <span class="text-muted sub-title">
							<span class="pull-right ng-binding"><?php echo $item['PropertyType']['name']; ?></span> 
							<span class="text-ellipsis ng-binding"> <i class="sl sl-icon-location"></i> &nbsp;<?php echo $item['Location']['name']; ?>, <?php echo $item['City']['name']; ?></span>
							  <?php for($k=1;$k<=$item['Homestay']['rating'];$k++){ ?>
										 <i class="fa fa-fw">&#xf005;</i>
							  <?php } ?>
							  
						  </span>
                    </figcaption>
					</a>
				</div>
		<!-- Listing Item / End -->
<?php } ?>

	</div>
	<!-- Carousel / End -->

</section>
<!-- Fullwidth Section /  Listing homes  End -->


<!-- Fullwidth Section Listing Experiences Start -->
<section class="fullwidth margin-top-10 padding-top-15 padding-bottom-10" data-background-color="#ffff">

	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<h3 class="headline left margin-bottom-45">
					Experiences
				<span>View our <i> handpicked </i> Experiences</span>
				</h3>
			</div>
		</div>
	</div>

	<!-- Carousel / Start -->
	<div class="simple-fw-slick-carousel dots-nav" role="toolbar">
<?php foreach($experienceList as $item){ ?>
		<!-- Listing Item -->
		<div class="fw-carousel-item">
			<a href="<?php echo Configure::read('app_root_path')."handpicked-experiences/".$item['Experience']['seo_unique_url'];?>" class="listing-item-container compact" style="height:auto !important;">
				<div class="listing-item">
					<img src="<?php echo $item['ExperienceFile']['file_path']."/".$item['ExperienceFile']['file_name']; ?>" alt="<?php echo $item['Experience']['name']; ?>">

<!--					<div class="listing-badge now-open">High Demand</div>-->

					<div class="listing-item-content">
<!--						<div class="numerical-rating" data-rating="<?php echo $item['Experience']['rating']; ?>"></div>-->
						<h3><?php echo $item['Experience']['name']; ?></h3>
						<span><?php echo $item['Location']['name']; ?>, <?php echo $item['City']['name']; ?></span>
					</div>
<!--					<span class="like-icon"></span>-->
				</div>
			</a>
		</div>
		<!-- Listing Item / End -->
<?php } ?>

	</div>
	<!-- Carousel / End -->

</section>
<!-- Fullwidth Section /  Listing Experiences  End -->




<!-- Fullwidth Section Listing Events Start -->
<section class="fullwidth margin-top-10 padding-top-15 padding-bottom-10" data-background-color="#ffff">

	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<h3 class="headline left margin-bottom-15">
					Events
				<span>View our <i> handpicked </i> events</span>
				</h3>
			</div>
		</div>
	</div>

	<!-- Carousel / Start -->
	<div class="simple-fw-slick-carousel dots-nav" role="toolbar">
<?php foreach($eventList as $item){ ?>
		
		<!-- Listing Item -->
				<div class="col-lg-4 col-md-6" style="margin-bottom:20px;">
					<a href="<?php echo Configure::read('app_root_path')."handpicked-experiences/".$item['Experience']['seo_unique_url'];?>" class="listing-item-container compact" data-marker-id="3" style="margin-bottom:10px !important; text-decoration: none;" >
						<div class="listing-item">
							<img src="<?php echo $item['ExperienceFile']['file_path']."/".$item['ExperienceFile']['file_name']; ?>" alt="<?php echo $item['Experience']['name']; ?>">
							<div class="listing-badge now-open" style="background-color: gray; border: 1px solid white;font-weight: bold; font-family: monospace;"><?php echo date('D, M d',strtotime($item['Experience']['event_date'])); ?></div>
<!--
							<div class="listing-item-details">

								<ul>
									<li>Starting from $59 per night</li>
								</ul>

							</div>
							<div class="listing-item-content">
								<div class="numerical-rating" data-rating="<?php //echo number_format($item['Homestay']['rating'],1); ?>"></div>
								
							</div>
-->

						</div>
					
					<figcaption title="<?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Experience']['about_experience']),0,100)), 0, 10))." ....";?>" style=" padding: 5px;   color: #333; position: relative; z-index: 3; ">
						  <span class="caption-title"> 
							<span class="pull-right ng-binding">₹<?php echo number_format($item['Experience']['event_price'],2); ?></span>
							<span class="text-ellipsis ng-binding"><?php echo $item['Experience']['name']; ?></span>
						  </span>
						<div class="clearfix"></div>
						  <span class="text-muted sub-title">
							<span class="pull-right ng-binding"><?php echo $item['ExperienceType']['type']; ?></span> 
							<span class="text-ellipsis ng-binding"> <i class="sl sl-icon-location"></i> &nbsp;<?php echo $item['Location']['name']; ?>, <?php echo $item['City']['name']; ?></span>
							  <?php for($k=1;$k<=$item['Experience']['rating'];$k++){ ?>
										 <i class="fa fa-fw">&#xf005;</i>
							  <?php } ?>
							  
						  </span>
                    </figcaption>
					</a>
				</div>
		<!-- Listing Item / End -->
<?php } ?>

	</div>
	<!-- Carousel / End -->

</section>
<!-- Fullwidth Section /  Listing Events End -->





<!-- Fullwidth Section Listing Deals Start -->
<section class="fullwidth margin-top-10 padding-top-15 padding-bottom-10" data-background-color="#ffff">

	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<h2 class="headline left margin-bottom-15">
					Deals
				<span>View our <i> special </i> deals</span>
				</h2>
			</div>
		</div>
	</div>

	<!-- Carousel / Start -->
 <div class="simple-fw-slick-carousel dots-nav" role="toolbar">
<?php 
foreach($array_deals as $item){ 
		if(isset($item['Homestay']['id']) && $item['Homestay']['id']!="" ){		
		?>
		<div class="col-lg-4 col-md-6" style="margin-bottom:20px;">
			<a target="_blank" href="<?php echo  Configure::read('app_root_path')."plantation-stays/".$item['Homestay']['seo_unique_url']; ?>" class="listing-item-container compact" data-marker-id="3" style="margin-bottom:10px !important; text-decoration: none;   overflow: hidden;" >
					<?php if($item['Deal']['id']!=''){
						$offer_text="";
				
							if($item['Deal']['offer_type']=='percentage')
								$offer_text=$item['Deal']['offer_value']."% OFF till ".date('jS-M',strtotime($item['Deal']['end_date']));
							else
								$offer_text=$item['Deal']['offer_value']."Rs OFF till ".date('jS-M',strtotime($item['Deal']['end_date']));
						?>
                       <div class="listing-badge now-open" style="right:-42px;top:39px;font-weight: bold;"><?php echo $offer_text;?></div> 
                        
                      <?php }?>
						<div class="listing-item">
							<img src="<?php echo Configure::read('app_root_path').$item['HomestayFile']['0']['file_path']."/".$item['HomestayFile']['0']['file_name']; ?>" alt="" >


						</div>
					
					<figcaption title="<?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Homestay']['description']),0,100)), 0, 10))." ....";?>" class="fig-desc-strip">
						  <div class="caption-title"> 
							<span class="pull-right ng-binding">
							 <?php 
		  						$item_price=$item['Homestay']['base_price'];
		  						$offer_price=0;
		  						if($item['Deal']['id']!=''){
									if($item['Deal']['offer_type']=='percentage')
										$offer_price=($item_price-(($item_price*$item['Deal']['offer_value'])/100));
									else
										$offer_price=($item_price-$item['Deal']['offer_value']);
								 }	
		  						if($offer_price>0){
									echo "₹"."<strike>".number_format($item_price,2)."</strike> <br/>";
		  							echo "₹".number_format($offer_price,2);
								}
		  						else
									echo "₹".number_format($item_price,2);
							?>
						   </span>
							<span class="text-ellipsis ng-binding"><?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Homestay']['name']),0,20)), 0, 10))." ...";?></span>
						  </div>
						<div class="clearfix"></div>
						  <span class="text-muted sub-title">
							<span class="pull-right ng-binding "><?php echo $item['PropertyType']['name']; ?></span> 
							<span class="text-ellipsis ng-binding"> <i class="sl sl-icon-location"></i> &nbsp;<?php echo $item['Location']['name']; ?>, <?php echo $item['City']['name']; ?></span>	
						  </span>
						<span class="sub-title" > 
							<br/>
							  <?php for($k=1;$k<=$item['Homestay']['rating'];$k++){ ?>
										 <i class="fa fa-fw">&#xf005;</i>
							  <?php } ?>
					    </span>
                    </figcaption>
			</a>
		</div>
		<!-- Listing Item / End -->
<?php  
	}
	else if(isset($item['Experience']['id']) && $item['Experience']['id']!="" ){	
 ?>
	  <div class="col-lg-4 col-md-6" style="margin-bottom:20px;">
			 <a  target="_blank" href="<?php echo  Configure::read('app_root_path')."handpicked-experiences/".$item['Experience']['seo_unique_url']; ?>" class="listing-item-container compact" data-marker-id="3" style="margin-bottom:10px !important; text-decoration: none;overflow: hidden;" >
					<?php if($item['Deal']['id']!=''){
						$offer_text="";
				
							if($item['Deal']['offer_type']=='percentage')
								$offer_text=$item['Deal']['offer_value']."% OFF till ".date('jS-M',strtotime($item['Deal']['end_date']));
							else
								$offer_text=$item['Deal']['offer_value']."Rs OFF till ".date('jS-M',strtotime($item['Deal']['end_date']));
						?>
                       <div class="listing-badge now-open" style="right:-42px;top:39px;font-weight: bold;"><?php echo $offer_text;?></div> 
                        
                      <?php }?>	
						
						<div class="listing-item">
							<img src="<?php echo Configure::read('app_root_path').$item['ExperienceFile']['0']['file_path']."/".$item['ExperienceFile']['0']['file_name']; ?>" alt="" >


						</div>
					
					<figcaption title="<?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Experience']['title']),0,100)), 0, 10))." ....";?>" class="fig-desc-strip">
						  <span class="caption-title"> 
							<span class="pull-right ng-binding">
							<?php 
		  						$item_price=$item['ExperienceSlot'][0]['price'];
		  						$offer_price=0;
		  						if($item['Deal']['id']!=''){
									if($item['Deal']['offer_type']=='percentage')
										$offer_price=($item_price-(($item_price*$item['Deal']['offer_value'])/100));
									else
										$offer_price=($item_price-$item['Deal']['offer_value']);
								 }	
		  						if($offer_price>0){
									echo "₹"."<strike>".number_format($item_price,2)."</strike> <br/>";
		  							echo "₹".number_format($offer_price,2);
								}
		  						else
									echo "₹".number_format($item_price,2);
							?>
							 </span>
							<span class="text-ellipsis ng-binding"><?php echo implode(' ', array_slice(explode(' ', substr(strip_tags($item['Experience']['name']),0,20)), 0, 10))." ...";?></span>
						  </span>
						<div class="clearfix"></div>
						  <span class="text-muted sub-title">
							<span class="pull-right ng-binding"><?php echo $item['ExperienceType']['type']; ?></span> 
							<span class="text-ellipsis ng-binding"> <i class="sl sl-icon-location"></i> &nbsp;<?php echo $item['Location']['name']; ?>, <?php echo $item['City']['name']; ?></span>
						  </span>
						<span class="sub-title" > 
							<br/>
							 <?php for($k=1;$k<=$item['Experience']['rating'];$k++){ ?>
										 <i class="fa fa-fw">&#xf005;</i>
							  <?php } ?>
					    </span>
                    </figcaption>
			</a>
		</div>
		<!-- Listing Item / End -->
 <?php
	}
} ?>

</div>
	<!-- Carousel / End -->

</section>
<!-- Fullwidth Section /  Listing Deals  End -->


<!-- Recent Blog Posts -->
<section class="fullwidth border-top margin-top-10 padding-top-15 padding-bottom-15" data-background-color="#fff">
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<h3 class="headline left margin-bottom-45">
					KNOW YOUR COFFEE!
				</h3>
			</div>
		</div>

		<div class="row">
			<!-- Blog Post Item -->
			<?php foreach($blogPosts as $posts){ ?>
			<div class="col-md-4">
				<a href="<?php echo Configure::read('app_root_path')."blog/post/".$posts['BlogPost']['id']; ?>" class="blog-compact-item-container">
					<div class="blog-compact-item">
						<img src="<?php echo Configure::read('app_root_path').$posts['BlogPost']['file_path']."/".$posts['BlogPost']['file_name']; ?>" alt="">
						<span class="blog-item-tag"  style="font-weight: bold;">Posted By: <?php echo ucfirst($posts['BlogPost']['posted_by']); ?></span>
						<div class="blog-compact-item-content">
								<ul class="blog-post-tags">
									<li><?php echo date("F d, Y",strtotime($posts['BlogPost']['posted_date'])); ?></li>
								</ul>
								<h3><?php echo $posts['BlogPost']['title']; ?></h3>
								<p><?php
									
										$content = preg_replace("/<img[^>]+\>/i", "(image) ", $posts['BlogPost']['content']); 
										$content = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($content))))));
										echo substr ($content,0,200);
									
									?></p>
							</div>
					</div>
				</a>
			</div>
			<?php } ?>
			<!-- Blog post Item / End -->
		</div>

	</div>
</section>






<!-- Recent Blog Posts / End -->
<?php echo $this->Html->script('scripts/jquery.mockjax.js'); ?>
<?php echo $this->Html->script('scripts/jquery.autocomplete.js'); ?>
<?php //echo $this->Html->script('scripts/demo.js'); ?>

<?php echo $this->Html->script('scripts/wowslider.js'); ?>
<?php echo $this->Html->script('scripts/script-slider-wow.js'); ?>

<script>

$('#autocomplete').autocomplete({
    serviceUrl: "<?php echo Configure::read('app_root_path'); ?>pages/fetchSearchData",
    onSelect: function (suggestion) {
   //  console.log(suggestion.data.category);
		$('#search_type').val(suggestion.data.category);
		$('#search_field').val(suggestion.data.search_field);
		$('#search_field_value').val(suggestion.data.search_field_value);
    }
});
    
  function validateSearch(){
     // alert($('#autocomplete').val());
        if($('#autocomplete').val()!="")
            return true;
      else
          return false;
        
    }
</script>
