<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_location/save";
                 echo $this->Form->create('Location', array('id'=>'addLocation', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Location</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new Location .</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">City:</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                    <?php  
                                                    echo $this->Form->input('city_id', array('options'=>$city,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'city_id', 'empty'=>'--Select--')); 

                                                    ?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                      <div class="form-group">
                                        <label class="col-md-3 control-label">Location: </label>
                                          <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                   <?php echo $this->Form->input('name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                             </div>     
                                          </div>
                                    </div>
                                                                    
                                  <div class="form-group">
                                                <label class="col-md-3 control-label">In Search ?</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php 
                          $array_insearch_flag=array('1'=>'Yes','0'=>'No');
                          
                          echo $this->Form->input('in_search',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'in_search','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false, 'options'=>$array_insearch_flag ));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>

                              </div>   
                                                                                 
                            </div> <!-- END OF FIRST ROW -->
                           
                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>



