 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">List Locations</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                     <a href="<?php echo Configure::read('app_root_path'); ?>/manage_amenity/add_edit">&nbsp;List Location &nbsp;</a>
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>Location</th>
                                                <th>City</th>
                                                <th>In Search</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php foreach($locations as $details){
   // debug($room);
	
                                                ?>

                                            <tr>	
                                                <td><?php echo $details['Location']['name']; ?></td>
                                                 <td><?php echo $details['City']['name']; ?></td>
                                                  <td><?php if($details['Location']['in_search']==0)
                                                                echo "No";
                                                            else
                                                                echo "Yes";
                                                     ?></td>
												
                                                <td>   <a href="<?php echo Configure::read('app_root_path'); ?>/manage_location/add_edit/<?php echo $details['Location']['id']; ?>">&nbsp;Edit &nbsp;</a>
                                                <a href="<?php  echo Configure::read('app_root_path'); ?>manage_location/delete/<?php  echo $details['Location']['id']; ?>">&nbsp;Delete &nbsp;</a>                                       </td>
                                            </tr>
                                <?php } ?>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->