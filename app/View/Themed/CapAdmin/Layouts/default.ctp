<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>.:: Kaapitrip.com -Admin Panel ::.</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
		
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->  
		<?php echo $this->Html->css('theme-default.css'); ?>

        <!-- <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/> -->
        <!-- EOF CSS INCLUDE -->      
              <!-- START PLUGINS -->
		<?php echo $this->Html->script('plugins/jquery/jquery.min.js'); ?>
		<?php echo $this->Html->script('plugins/jquery/jquery-ui.min.js'); ?>
		<?php echo $this->Html->script('plugins/bootstrap/bootstrap.min.js'); ?>
              
                <?php echo $this->Html->script('scripts/jquery-validation/jquery.validate.js'); ?>
              
		<style>
		.pagination {
			display: inline-block;
			text-align: center;
			font-weight: bold;
		}
		.pagination ul{
				list-style: none;
		}
		.pagination ul li {
			padding: 19px;
			text-decoration: none;
			float:left;
			height: 25px;
			width:25px;
		}
		.pagination ul li a {
			color: black;
			float: left;			
			text-decoration: none;
			transition: background-color .3s;
		}

		.pagination ul li.current {
			background-color: #4CAF50;
			color: white;
		}

		.pagination a:hover:not(.active) {background-color: #ddd;}
</style>
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            	<?php echo $this->element('menu/left_sidebar'); ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                		<?php echo $this->element('menu/top_menu'); ?>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                  <?php echo $content_for_layout; ?>       
                
                
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo Configure::read('app_root_path'); ?>users/logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->         
        
        
    <!-- START SCRIPTS -->
  	
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->   
		<?php echo $this->Html->script('plugins/icheck/icheck.min.js'); ?>
		<?php echo $this->Html->script('plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'); ?>
		<?php echo $this->Html->script('plugins/scrolltotop/scrolltopcontrol.js'); ?>
        
    
        
        
 <?php if($this->params['controller']=='dashboard' &&  $this->params['action']=='view'){ ?>
        <?php echo $this->Html->script('plugins/morris/raphael-min.js'); ?>
		<?php echo $this->Html->script('plugins/morris/morris.min.js'); ?>
		<?php echo $this->Html->script('plugins/rickshaw/d3.v3.js'); ?>
		<?php echo $this->Html->script('plugins/rickshaw/rickshaw.min.js'); ?>
		<?php echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>
		<?php echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>
		<?php echo $this->Html->script('plugins/bootstrap/bootstrap-datepicker.js'); ?>
		<?php echo $this->Html->script('plugins/owl/owl.carousel.min.js'); ?>
		<?php echo $this->Html->script('plugins/moment.min.js'); ?>
		<?php echo $this->Html->script('plugins/daterangepicker/daterangepicker.js'); ?>	
  <?php   } ?>
		
<?php if(($this->params['controller']=='manage_homestays'|| $this->params['controller']=='manage_experiences' || $this->params['controller']=='manage_products' || $this->params['controller']=='manage_coffee_shops') &&  ($this->params['action']=='add'|| $this->params['action']=='list')){ ?>
        <?php echo $this->Html->script('plugins/bootstrap/bootstrap-file-input.js'); ?>	
        <?php echo $this->Html->script('plugins/bootstrap/bootstrap-select.js'); ?>	
        <?php echo $this->Html->script('plugins/tagsinput/jquery.tagsinput.min.js'); ?>	
        
        <?php echo $this->Html->script('plugins/validationengine/languages/jquery.validationEngine-en.js'); ?>
        <?php echo $this->Html->script('plugins/validationengine/jquery.validationEngine.js'); ?>
        <?php echo $this->Html->script('plugins/jquery-validation/jquery.validate.js'); ?>
        
        <?php echo $this->Html->script('plugins/dropzone/dropzone.min.js'); ?>
        <?php echo $this->Html->script('plugins/fileinput/fileinput.min.js'); ?>
        <?php echo $this->Html->script('plugins/filetree/jqueryFileTree.js'); ?>
        
        
<?php    } ?>

<?php if(($this->params['controller']=='manage_homestays' || $this->params['controller']=='manage_experiences' ) &&  ($this->params['action']=='add')){ ?>
        
        <?php echo $this->Html->script('plugins/icheck/icheck.min.js'); ?>
        <?php echo $this->Html->script('plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'); ?>
        <?php echo $this->Html->script('plugins/moment.min.js'); ?>
        <?php echo $this->Html->script('plugins/fullcalendar/fullcalendar.min.js'); ?>
        
<?php    } ?>

		
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <?php echo $this->Html->script('bootbox.min.js'); ?>
		<?php echo $this->Html->script('settings.js'); ?>
		<?php echo $this->Html->script('plugins.js'); ?>
		<?php echo $this->Html->script('actions.js'); ?>
        
        
    <?php if($this->params['controller']=='dashboard' &&  $this->params['action']=='view'){ ?>
		<?php echo $this->Html->script('demo_dashboard.js'); ?>
	 <?php
        } ?>

    <?php if($this->params['action']=='list' || $this->params['action']=='booking_list' || $this->params['action']=='enquiry_list'|| $this->params['action']=='bed_type_list' || $this->params['action']=='post_list' || $this->params['action']=='coupon_list' || $this->params['action']=='experience_list' || $this->params['action']=='experience_type_list' || $this->params['action']=='property_list' || $this->params['action']=='host_list' || $this->params['action']=='product_list'  || $this->params['action']=='property_type_list'  || $this->params['action']=='room_type_list' || $this->params['action']=='slider_list'  || $this->params['action']=='booking_invoice' || $this->params['action']=='event_list' || $this->params['action']=='agent_list'|| $this->params['action']=='deal_list' || $this->params['action']=='coffee_shop_list') { ?>
		  <?php
                  
                    echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
                    echo $this->Html->css('jquery.dataTables.min.css');
                  
        ?>

	<?php
        } ?>
		
	<?php if($this->params['controller']=='reports'){ ?>
        <?php echo $this->Html->script('plugins/bootstrap/bootstrap-select.js'); ?>	
  <?php } ?>
        
        <?php echo $this->Html->script('custom_coorgexpress.js'); ?>
        
        <!-- END TEMPLATE -->
        
<?php if(($this->params['controller']=='manage_homestays' || $this->params['controller']=='manage_experiences' || $this->params['controller']=='manage_products' || $this->params['controller']=='manage_coffee_shops') &&  $this->params['action']=='add'){        
        
        ?> 
<script >
    
    var ajaxUrl ="<?php echo Configure::read('app_root_path'); ?>manage_homestays/fileUpload/";
    <?php if($this->params['controller']=='manage_homestays' ){?>
             ajaxUrl ="<?php echo Configure::read('app_root_path'); ?>manage_homestays/fileUpload/";
       <?php  }elseif($this->params['controller']=='manage_homestays' ){?>
            ajaxUrl ="<?php echo Configure::read('app_root_path'); ?>manage_experiences/fileUpload/";
           
    <?php  }elseif($this->params['controller']=='manage_products' ){?>
            ajaxUrl ="<?php echo Configure::read('app_root_path'); ?>manage_products/fileUpload/";
            
     <?php  }elseif($this->params['controller']=='manage_coffee_shops' ){?>
            ajaxUrl ="<?php echo Configure::read('app_root_path'); ?>manage_coffee_shops/fileUpload/";
            <?php }?>           
                
  
        $(function() {
            
        
        Dropzone.autoDiscover = false;
	$("#dZUpload").dropzone({
		 addRemoveLinks: true,
		 url:ajaxUrl,
		 maxFiles: 2000,
		 uploadMultiple: true,
		 acceptedFiles: '.jpg, .jpeg, .png, .gif',
		 removedfile: function(file) {
			 console.log(file);
			 var name = file.xhr.response;
			 $.ajax({
					   type: 'POST',
					   url: ajaxUrl,
					   data: {name: name,request: 2},
					   sucess: function(data){
						console.log('success: ' + data);
					   }
			 });
			 var _ref;
			 
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
		 }
	});

        });
    
</script>
    <!-- END SCRIPTS -->    
<?php } ?>


   

    </body>
</html>






