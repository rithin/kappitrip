<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>.:: kaapitrip.com - Admin Panel Secured Login ::.</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
<!--        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>-->
        <?php echo $this->Html->css('theme-default.css'); ?>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        
            <?php echo $content_for_layout; ?>       
        
    </body>
</html>






