<?php echo $this->Html->docType('html5'); ?>
<html lang="en">
<head>
	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Highsite</title>

        <?php echo $this->Html->css('bootstrap.min.css'); ?>

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
        <?php echo $this->Html->css('jquery-ui/jquery.ui.1.10.2.ie.css'); ?>
	<![endif]-->

	<!-- Theme -->
        <?php echo $this->Html->css('main.css'); ?>
        <?php echo $this->Html->css('plugins.css'); ?>
        <?php echo $this->Html->css('responsive.css'); ?>
        <?php echo $this->Html->css('icons.css'); ?>
        <?php echo $this->Html->css('fontawesome/font-awesome.min.css'); ?>
            <!--[if IE 7]>
        <?php echo $this->Html->css('fontawesome/font-awesome-ie7.min.css'); ?>

            <![endif]-->

            <!--[if IE 8]>
        <?php echo $this->Html->css('fontawesome/ie8.css'); ?>
            <![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
            <?php echo $this->Html->css('editor.css'); ?>
</head>

<body>

	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner">
		<!-- Top Navigation Bar -->
		<?php echo $this->element('menu/top_menu'); ?>
		<!-- /top navigation bar -->


	</header> <!-- /.header -->

	<div id="container">
		<?php echo $this->element('menu/left_sidebar'); ?>
		<!-- /Sidebar -->

		<div id="content">
		    <?php echo $content_for_layout; ?>
		</div>

                <?php if(!$this->Session->read('Auth.User')){ ?>
                    <script>window.location='<?php echo Configure::read('nuwed_root_path'); ?>users';</script>
                <?php } ?>
	</div>

     </body>
     <!--=== JavaScript ===-->
     <?php echo $this->Html->script('libs/jquery-1.10.2.min.js'); ?>
     <?php echo $this->Html->script('plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js'); ?>
     <?php echo $this->Html->script('bootstrap.min.js'); ?>
     <?php echo $this->Html->script('libs/lodash.compat.min.js'); ?>
     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
     <!--[if lt IE 9]>
     <?php echo $this->Html->script('html5shiv.js'); ?>
     <![endif]-->
     <!-- Smartphone Touch Events -->
     <?php echo $this->Html->script('plugins/touchpunch/jquery.ui.touch-punch.min.js'); ?>
     <?php echo $this->Html->script('plugins/event.swipe/jquery.event.move.js'); ?>
     <?php echo $this->Html->script('plugins/event.swipe/jquery.event.swipe.js'); ?>
     <!-- General -->
     <?php echo $this->Html->script('libs/breakpoints.js'); ?>
     <?php echo $this->Html->script('plugins/respond/respond.min.js'); ?> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
     <?php echo $this->Html->script('plugins/cookie/jquery.cookie.min.js'); ?>
     <?php echo $this->Html->script('plugins/slimscroll/jquery.slimscroll.min.js'); ?>
     <?php echo $this->Html->script('plugins/slimscroll/jquery.slimscroll.horizontal.min.js'); ?>
     <?php echo $this->Html->script('plugins/blockui/jquery.blockUI.min.js'); ?>
     <!-- Page specific plugins -->

     <?php echo $this->Html->script('plugins/fullcalendar/fullcalendar.min.js'); ?>
     <?php echo $this->Html->script('plugins/sparkline/jquery.sparkline.min.js'); ?>
     <!-- Forms -->

     <?php echo $this->Html->script('plugins/uniform/jquery.uniform.min.js'); ?>
     <?php echo $this->Html->script('plugins/select2/select2.min.js'); ?>
     <!-- DataTables -->

     <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
     <?php echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js'); ?>
     <?php echo $this->Html->script('plugins/datatables/tabletools/TableTools.min.js'); ?>
     <?php echo $this->Html->script('plugins/datatables/colvis/ColVis.min.js'); ?>
     <?php echo $this->Html->script('plugins/datatables/DT_bootstrap.js'); ?>

      <!-- Pickers -->
     <?php echo $this->Html->script('plugins/daterangepicker/moment.min.js'); ?>
     <?php echo $this->Html->script('plugins/daterangepicker/daterangepicker.js'); ?>
     <?php echo $this->Html->script('plugins/pickadate/picker.js'); ?>
     <?php echo $this->Html->script('plugins/pickadate/picker.date.js'); ?>
     <?php echo $this->Html->script('plugins/pickadate/picker.time.js'); ?>
     <?php echo $this->Html->script('plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js'); ?>

     <!-- Form Validation -->
     <?php echo $this->Html->script('plugins/validation/jquery.validate.min.js'); ?>
     <?php echo $this->Html->script('plugins/validation/additional-methods.min.js'); ?>

     <!-- Noty -->
     <?php echo $this->Html->script('plugins/noty/jquery.noty.js'); ?>
     <?php echo $this->Html->script('plugins/noty/layouts/top.js'); ?>
     <?php echo $this->Html->script('plugins/noty/themes/default.js'); ?>
     <!-- Bootbox -->
     <?php echo $this->Html->script('plugins/bootbox/bootbox.js'); ?>

     <!-- App -->
     <?php echo $this->Html->script('app.js'); ?>
     <?php echo $this->Html->script('plugins.js'); ?>
     <?php echo $this->Html->script('plugins.form-components.js'); ?>

     <?php echo $this->Html->script('editor.js'); ?>
     <?php
     if(isset($restult_contents['AboutUsDetail']['description'])){ $descr=$restult_contents['AboutUsDetail']['description']; } else { $descr=""; }
     if(isset($restult_contents['HomeContent']['left_description'])){ $left_descr=$restult_contents['HomeContent']['left_description']; } else { $left_descr=""; }
     if(isset($restult_contents['HomeContent']['right_description'])){ $right_descr=$restult_contents['HomeContent']['right_description']; } else { $right_descr=""; }
     ?>
    <script>
    $(document).ready(function(){
        if($("#aboutEditor").length){
            $("#aboutEditor").Editor();
            $("#aboutEditor").Editor("setText",'<?php echo $descr; ?>');
        }
        if($("#leftEditor").length){
            $("#leftEditor").Editor();
            $("#leftEditor").Editor("setText",'<?php echo $left_descr; ?>');
        }
        if($("#rightEditor").length){
            $("#rightEditor").Editor();
            $("#rightEditor").Editor("setText",'<?php echo $right_descr; ?>');
        }
    });

    function imgSize(){
        var myImg = document.querySelector("#photo_location");
        var realWidth = myImg.naturalWidth;
        var realHeight = myImg.naturalHeight;
        //alert("Original width=" + realWidth + ", " + "Original height=" + realHeight);
    }
    </script>

	<script>
        $(document).ready(function(){
            "use strict";
            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins
        });
        </script>

        <?php
        if(isset($my_country_id)){
        if($my_country_id!=""){ ?>
        <script>
        jQuery.ajax({
            type: "POST",
            url: "<?php echo Configure::read('nuwed_root_path'); ?>users/get_cities/<?php echo $my_country_id; ?>/<?php echo $my_city_id; ?>",
            data:'',
            //contentType: "application/json; charset=utf-8",
            //dataType: "json",
            cache: false,

            success: function(data) {
              // console.log(data);
              $("#div_city_id").html(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              bootbox.alert("OPERATION FAILED: Try after sometime.");
              return false;
            }
         });
        </script>
        <?php } } ?>

	<!-- Demo JS -->
     <?php echo $this->Html->script('custom.js'); ?>
     <?php echo $this->Html->script('demo/ui_general.js'); ?>
     <?php echo $this->Html->script('demo/ui_buttons.js'); ?>
     <?php echo $this->Html->script('demo/form_validation.js'); ?>

        <script>
        $("#expiry_date").prop('disabled',true);
        $("#manufacture_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: 'D',
            onSelect: function (date) {
                var date2 = $('#manufacture_date').datepicker('getDate');
                date2.setDate(date2.getDate() + 1);
                //$('#expiry_date').datepicker('setDate', date2);
                //sets minDate to dt1 date + 1
                $('#expiry_date').datepicker('option', 'minDate', date2);
                $("#expiry_date").prop('disabled',false);
            }
        });


        $("#expiry_date").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: $('#manufacture_date').val()


        });
        </script>
        <script>
     





        </script>
</html>