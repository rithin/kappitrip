 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                     <a href="<?php echo Configure::read('app_root_path'); ?>/manage_blog/add_edit">&nbsp;Add Content &nbsp;</a>
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>Title</th>
                                                <th>Posted By</th>
												<th>Date</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php foreach($blog_post_lists as $details){
   // debug($room);
	
                                                ?>

                                            <tr>	
                                                <td><?php echo $details['BlogPost']['title']; ?></td>
                                                <td><?php echo $details['BlogPost']['posted_by']; ?></td>
												<td><?php echo $details['BlogPost']['posted_date']; ?></td>
                                                <td>       <a href="<?php echo Configure::read('app_root_path'); ?>/manage_blog/add_edit/<?php echo $details['BlogPost']['id']; ?>">&nbsp;Edit &nbsp;</a>
                                                <a href="<?php  echo Configure::read('app_root_path'); ?>manage_blog/delete/<?php  echo $details['BlogPost']['id']; ?>">&nbsp;Delete &nbsp;</a>                                            </td>
                                            </tr>
                                <?php } ?>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->