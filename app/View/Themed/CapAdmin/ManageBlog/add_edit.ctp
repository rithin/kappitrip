<?php echo $this->Html->css('editor.css'); ?>
<?php echo $this->Html->script('editor.js'); 
//echo $this->Html->script('/TinyMCE/js/tiny_mce/tiny_mce.js', array(	'inline' => false));
?>
 <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
<script>
      $(document).ready(function() {
//        $("#txtEditor").Editor();
//		  if($("#txtEditor").val()!=""){
//			  var postdata=$("#txtEditor").val();
//			  $( '.Editor-editor' ).text(postdata);
//		  }

//		   $('div#txtEditor').froalaEditor({
//			   heightMin: 100,
//      			heightMax: 600,			   
//			    toolbarButtons: ['undo', 'redo' , '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable', 'html'],
//      			toolbarButtonsXS: ['undo', 'redo' , '-', 'bold', 'italic', 'underline'],
//			  imageStyles: {
//					class1: 'Class 1',
//					class2: 'Class 2'
//				  },
//				  imageEditButtons: ['imageReplace', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize']
//				
//    
//		   })

		  
	tinymce.init({
    	selector: '#txtEditor',
			plugins: 'image code',
			  toolbar: 'undo redo | link image | code',
			  // enable title field in the Image dialog
			  image_title: true, 
			  // enable automatic uploads of images represented by blob or data URIs
			  automatic_uploads: true,
			  // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
			  // images_upload_url: 'postAcceptor.php',
			  // here we add custom filepicker only to Image dialog
			  file_picker_types: 'image', 
			  // and here's our custom image picker
			  file_picker_callback: function(cb, value, meta) {
				var input = document.createElement('input');
				input.setAttribute('type', 'file');
				input.setAttribute('accept', 'image/*');

				// Note: In modern browsers input[type="file"] is functional without 
				// even adding it to the DOM, but that might not be the case in some older
				// or quirky browsers like IE, so you might want to add it to the DOM
				// just in case, and visually hide it. And do not forget do remove it
				// once you do not need it anymore.

				input.onchange = function() {
				  var file = this.files[0];

				  var reader = new FileReader();
				  reader.onload = function () {
					// Note: Now we need to register the blob in TinyMCEs image blob
					// registry. In the next release this part hopefully won't be
					// necessary, as we are looking to handle it internally.
					var id = 'blobid' + (new Date()).getTime();
					var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
					var base64 = reader.result.split(',')[1];
					var blobInfo = blobCache.create(id, file, base64);
					blobCache.add(blobInfo);

					// call the callback and populate the Title field with the file name
					cb(blobInfo.blobUri(), { title: file.name });
				  };
				  reader.readAsDataURL(file);
				};

				input.click();
			  }
  });
		  
      });
    </script>
<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_blog/save";
                 echo $this->Form->create('BlogPost', array('id'=>'addContentType', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true,'enctype'=>'multipart/form-data','onsubmit'=>'javascript: return formvalidate()'));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Content</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new Contents.</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    
                                                        <?php echo $this->Form->input('title',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'title','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Posted By</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                       
                                                    
                                                        <?php echo $this->Form->input('posted_by',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'posted_by','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
											
											
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Content</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group col-md-12">
                                                        
                                                    
                                                        <?php //echo $this->Form->input('content',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'txtEditor','autofocus','type'=>'textarea','error'=>false));
														//$this->Froala->editor('.selector', array('toolbarInline' => false));
														
//													echo	$this->Html->css('/Froala/css/froala_editor.min.css');
//echo $this->Html->script('/Froala/js/froala_editor.min.js', array('toolbarInline' => false));
				
														
													//	$this->TinyMCE->editor(array('theme' => 'advanced', 'mode' => 'textareas'));
														
														?>
														<div id="txtEditor_a"><?php echo $this->Form->input('txtEditor',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'txtEditor','autofocus','data-rule-required'=>"true", 'type'=>'textarea','error'=>false));?></div>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Posted Image</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                       
                                                    
                                                        <?php echo $this->Form->input('file',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'file','autofocus','data-rule-required'=>"true",'type'=>'file','error'=>false));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
											
                                          
                                           
                                           
                                                                                 
                                    </div> <!-- END OF FIRST ROW -->
                           
                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right" onclick="javascript:formvalidate()" >Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>


<script>

function formvalidate(){
//	var str = $( '.Editor-editor' ).text();
//	console.log(str);
//   $('#txtEditor').val(str);
	return true;
}
</script>
