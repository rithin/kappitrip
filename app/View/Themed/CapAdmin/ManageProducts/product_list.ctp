 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">

                                    <table id="datadisplay" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Band Name</th>
                <th>Manufacturer Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        
    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->



<script>

$(document).ready(function() {

    $('#datadisplay').DataTable( {
        "aProcessing": true,
        "aServerSide": true,
        "ajax": '<?php echo Configure::read('app_root_path'); ?>manage_products/ajax_product_list',
        "aoColumns": [
            { "data": "product_name" },
            { "data": "brand_name" },
            { "data": "manufacturer_name" },
            { "data": "actions" },
           
        ],
        

    } );
} );

</script>