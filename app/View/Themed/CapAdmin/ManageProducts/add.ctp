<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_products/save";
                 echo $this->Form->create('Product', array('id'=>'addProduct', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Product</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new Products.</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of the Product</label>
                                                <div class="col-md-9 col-xs-12">   
                                                    
                                                        <?php echo $this->Form->input('product_name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'product_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">SEO Unique URL</label>
                                                <div class="col-md-9 col-xs-12">    
                                                    
                                                        <?php echo $this->Form->input('seo_unique_url',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'seo_unique_url','autofocus','data-rule-required'=>"true",'error'=>false));?>
                     
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">SKU</label>
                                                <div class="col-md-9 col-xs-12">    
                                                    
                                                        <?php echo $this->Form->input('sku',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'sku','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                   
                                                </div>
                                            </div>
                                                
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Product Location</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php 
                                                   //debug($array_experience_types);
                                                    echo $this->Form->input('product_location', array('options'=>$array_product_location,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'product_location',
                                                        'empty'=>'--Select--')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>   
                                                                                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Category</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php 
                                                   //debug($array_experience_types);
                                                    echo $this->Form->input('category_id', array('options'=>$array_categories,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'category_id',
                                                        'empty'=>'--Select--','onChange'=>'loadSubCategory(this);')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>                                            
                                            
                                             <!--<div class="form-group">
                                                <label class="col-md-3 control-label">Sub Category</label>
                                                <div class="col-md-9 col-xs-12" id="subCategory">                                            
                                                    <?php 
                                                   //debug($array_experience_types);
                                                    //echo $this->Form->input('sub_category_id', array('options'=>'', 'div'=>false, 'label'=>false, 'class'=>'form-control',  'type'=>'select', 'id' =>'sub_category_id', 'empty'=>'--Select--')); ?>
<!--                                                    
                                                </div>
                                            </div>-->
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Coffee Grind Size</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php 
                                                   //debug($array_experience_types);
                                                    echo $this->Form->input('coffee_type', array('options'=>$array_coffee_type,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'coffee_type',
                                                        'empty'=>'--Select--')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Product Desc</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                <?php echo $this->Form->input('product_desc',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'product_desc','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Brand Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('brand_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'brand_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Manufacturer Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                      <?php echo $this->Form->input('manufacturer_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'manufacturer_name', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>     
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Quantity</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('quantity',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'quantity','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Unit Prize</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('unit_price',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'unit_price','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Product Weight</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('product_weight',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'unit_price','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
                                             
                                          <div class="form-group">
                                                <label class="col-md-3 control-label">Shelf Life</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('shelf_life',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'shelf_life','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>    
                                            
                                          <div class="form-group">
                                                <label class="col-md-3 control-label">Coffee Flavour</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('coffee_flavour',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'coffee_flavour','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Coffee Ingredients</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('coffee_ingredients',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'coffee_ingredients','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Coffee Roast Level</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('coffee_roast',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'flavour','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Coffee Processing</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('coffee_processing',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'coffee_processing','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
                                                                                      
                                            
                                        </div>                                          
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                            <div class="row">
                                <div class="col-md-9" style="width:53%;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                         <h3 style="color:red;"><span class="fa fa-download"></span> Dropzone (Drop your property images here.)</h3>
                                            <p style="color:green;">Upload files one by one for better Performance. [Max file size:2MB, Supported Formats: JPEG, JPG, PNG GIF]</p>
                                            <div id="dZUpload" class="dropzone" style="min-height:150px;min-width:">
                                                <div class="dz-default dz-message"></div>
                                            </div>
                                        </div>
                                    </div>   
                                 </div> 
                                <?php if(count($productFiles)>0) { ?>
                                <div> <ul> <?php foreach($productFiles as $files) { 
                                    //debug($files);
                                    ?>
                                        <li><img src="<?php echo Configure::read('app_root_path').$files['ProductFile']['img_path'].'/'.$files['ProductFile']['resized_img_name']; ?>" width="100" height="100"/>&nbsp;&nbsp; <a href="<?php echo Configure::read('app_root_path'); ?>manage_products/deleteFile/<?php echo $files['ProductFile']['id']; ?>" alt="Delete image"><i class="fa fa-times fa-3x"></i></a></li>
                                    <?php } ?>
                                 </ul>
                                </div>
                                <?php } ?>
                            </div><!-- END OF Second ROW -->

                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>


<script>

    


function loadSubCategory(data){
	  var categoryId=data.value;
          
	  var jsonval= {categoryId:categoryId};
	  jQuery.ajax({
      type: "POST",
      url: "<?php echo Configure::read('app_root_path'); ?>manage_products/loadSubCategory",
      data:jsonval,
      //contentType: "application/json; charset=utf-8",
      dataType: "json",
      cache: false,
      success: function(data) {
		  //console.log(data);
          var $dropdown = $("#sub_category_id");
            $("#sub_category_id").empty().append('<option value="">Select</option>');
            $.each(data, function() {
                $dropdown.append($("<option />").val(this.SubCategory.id).text(this.SubCategory.sub_category_name));
            });
      },
      error: function(xhr, ajaxOptions, thrownError) {
       //bootbox.alert("OPERATION FAILED: Try after sometime.");
		return false;
      }
    });
	return false;
}



$("#addProduct").validate({
                ignore: [],
                rules: {                                            
                        'data[Product][product_name]': {
                                required: true,
                        },
                        'data[Product][seo_unique_url]': {
                                required: true,
                        },
                        'data[Product][sku]': {
                                required: true,
                        },
                        'data[Product][product_location]': {
                                required: true,
                                
                        },
                        'data[Product][category_id]': {
                                required: true,
                               
                                
                        },
                        'data[Product][coffee_type]': {
                                required: true,
                        },
                        'data[Product][product_desc]': {
                                required: true,
                        },
                        'data[Product][manufacturer_name]': {
                                required: true,
                        },
                        'data[Product][quantity]': {
                                required: true,
                        },
                        'data[Product][unit_price]': {
                                required: true,
                        },
                      
                        
             },
                
        
        
     });   

</script>
