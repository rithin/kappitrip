 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                     <a href="<?php echo Configure::read('app_root_path'); ?>/manage_property_type/add_edit">&nbsp;Add Room Type &nbsp;</a>
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>Room Type</th>
                                                
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php foreach($roomTypes as $details){
   // debug($room);
	
                                                ?>

                                            <tr>	
                                                <td><?php echo $details['RoomType']['type']; ?></td>
												
                                                <td>       <a href="<?php echo Configure::read('app_root_path'); ?>/manage_room_type/add_edit/<?php echo $details['RoomType']['id']; ?>">&nbsp;Edit &nbsp;</a>
                                                <a href="<?php  echo Configure::read('app_root_path'); ?>manage_room_type/delete/<?php  echo $details['RoomType']['id']; ?>">&nbsp;Delete &nbsp;</a>                                            </td>
                                            </tr>
                                <?php } ?>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->