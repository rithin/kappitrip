<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Invoice</title>
    <?php echo $this->Html->css('invoice.css'); ?>
</head> 

<body>

<!-- Print Button -->
<a href="javascript:window.print()" class="print-button">Print this invoice</a>

<!-- Invoice -->
<div id="invoice">

	<!-- Header -->
	<div class="row">
		<div class="col-md-6">
			<div id="logo"><img src="images/logo.png" alt=""></div>
		</div>

		<div class="col-md-6">	

			<p id="details">
				<strong>Order:</strong> #<?php echo $BookingOrderDetailArr[0]['BookingPaymentResponse']['order_id']; ?> <br>
				<strong>Issued:</strong> <?php echo date('d-m-Y'); ?> <br>
			<?php if($BookingOrderDetailArr[0]['BookingOrderDetail']['status']==8 || $BookingOrderDetailArr[0]['BookingOrderDetail']['status']==13){ ?>
				<span style="color:red;font-size:23px;"> CANCELLED</span>
				<?php } ?>
			</p>
		</div>
	</div>


	<!-- Client & Supplier -->
	<div class="row">
		<div class="col-md-12">
			<h1 style="
    text-align:  center;
">INVOICE</h1>
		</div>

		<div class="col-md-4">	
			<strong class="margin-bottom-5">Supplier</strong>
			<p>
				CoorgExpress. <br>
				#203, Sapthagiriri Springs <br>
				BTS Layout, Arekere <br>
                Banglore - 560076 <br>
			</p>
		</div>

		<div class="col-md-4">	
			<strong class="margin-bottom-5">Customer</strong>
			<p>
				<?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_name']); ?><br>
				<?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_address']); ?> <br>
				<?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_city']); ?>, 
                <?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_zip']); ?> <br>
                <?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_country']); ?> <br>
			</p>
		</div>
        
        <div class="col-md-4">	
			<strong class="margin-bottom-5">Accomadation</strong>
			<p>
				<?php echo ucfirst($HomestayDetailArr['Homestay']['name']); ?><br>
				<?php echo ucfirst($HomestayDetailArr['Homestay']['address']); ?> <br>
				<?php echo ucfirst($HomestayDetailArr['Homestay']['location']); ?> <br>
                <?php echo "Tel:   ". ucfirst($HomestayDetailArr['Homestay']['telephone']); ?> <br>
                <?php //echo ucfirst($HomestayDetailArr['Homestay']['address']); ?> <br>
			</p>
		</div>
	</div>


	<!-- Invoice -->
	<div class="row">
		<div class="col-md-12">
			<table class="margin-top-20">
				<tr>
					<th>Property</th>
					<th>Night(s)</th>
					<th>Unit Price</th>
					<th>Check In</th>
                    <th>Checkout</th>
					<th>Total</th>
				</tr>
        <?php  
            $grand_total=0;				
            foreach($BookingOrderDetailArr as $item) {
           		$grand_total +=($item['BookingOrderDetail']['no_of_days'] * $item['BookingOrderDetail']['total_price']);
          ?>
				<tr>
					<td><?php echo $HomestayDetailArr['Homestay']['name']; ?></td> 
					<td><?php echo $BookingOrderDetailArr[0]['BookingOrderDetail']['no_of_days'];?></td>
					<td><?php echo $BookingOrderDetailArr[0]['BookingOrderDetail']['total_price'];?></td>					
					<td><?php echo $OccupancyDetailArr['OccupancyDetail']['check_in_date']; ?></td>
                    <td><?php echo $OccupancyDetailArr['OccupancyDetail']['check_out_date']; ?></td>
					<td><i class="fa fa-fw">&#xf156;</i><?php echo number_format($item['BookingOrderDetail']['total_price']*$BookingOrderDetailArr[0]['BookingOrderDetail']['no_of_days']); ?></td>
				</tr>
        <?php } ?>         
        
			</table>
		</div>
		
		<div class="col-md-4 col-md-offset-8">	
			<table id="totals">
				<tr>
					<th>Total Amount Paid</th> 
					<th><span><i class="fa fa-fw">&#xf156;</i> <?php echo number_format($grand_total); ?></span></th>
				</tr>
			</table>
		</div>
	</div>


	<!-- Footer -->
	<div class="row">
		<div class="col-md-12">
			<ul id="footer">
				<li><span>www.coorgexpress.com</span></li>
				<li>info@coorgexpress.com</li>
<!--				<li>(123) 123-456</li>-->
			</ul>
		</div>
	</div>
		
</div>


</body>
</html>
