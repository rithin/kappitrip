 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <table id="datadisplay" class="table datatable">
                                        <thead>
                                            <tr>
						<th>Booking ID</th>
                                                <th>Customer Name</th>
                                                
                                                <th>Telephone</th>
                                                <th>Homestay</th>                                                
                                                <th>Homestay Contact</th>
						<th>Check In Date</th>
						<th>Check Out Date</th>
                                                <th>Coupon Discount</th>
						<th>Amount Paid</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                    
                                       
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

<script>

$(document).ready(function() {

    $('#datadisplay').DataTable( {
        "aProcessing": true,
        "aServerSide": true,
        "ajax": '<?php echo Configure::read('app_root_path'); ?>property_bookings/ajax_booking_list',
        "aoColumns": [
            { "data": "booking_id" },
            { "data": "billing_name" },
            
            { "data": "billing_tel" },
            { "data": "homestay_name" },
            { "data": "homestay_contact" },
            { "data": "checkin_date" },
            { "data": "checkout_date" },
            { "data": "coupon_discount" },
            { "data": "amount_paid" },
            { "data": "actions" },
           
        ],

    } );
} );

</script>