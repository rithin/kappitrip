<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

<style>
.redClass{background:#ef6f6f !important}

</style>
<!-- START DEFAULT DATATABLE -->
<div class="panel panel-default">
    <div class="panel-heading">                                
        <h3 class="panel-title">Booking Enquiries/ Custom Quotes</h3>
         <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
             <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
             <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
         </ul>                                
     </div>
                               
	<div class="col-md-1 col-xs-12" style="float:right; margin-right:150px;">    
		 <a title="Raise Custom Quote." href="<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/add_edit_custom_booking"> <button type="button" class="btn btn-danger" style="margin:10px;">Raise Custom Quote</button> </a>
	</div>
			<div class="panel-body">
                                    <table id="datadisplay" class="table datatable">
                                        <thead>
                                            <tr>
						<th>Type</th>
						<th>Name</th>
                                                <th>Homestay / Experience</th>
                                                <th>Email</th>
                                                <th>Telephone</th>
                                                <th>From</th>
                                                <th>To</th>
						<th>Adults</th>
                                                <th>Children</th>
												
                                            </tr>
                                        </thead>
                                    
                                       
                                    </table>
                                </div>
 </div>
                            <!-- END DEFAULT DATATABLE -->
<script>

function viewDetails(booking_enquiry_id){
	//event.preventDefault();
    //this.blur(); // Manually remove focus from clicked link.
 	console.log(booking_enquiry_id);
	$.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/view_details",
           data: 'booking_enquiry_id='+ booking_enquiry_id,
           type: "POST",
           success: function(response) {
			   $('#ajaxDetails').html(response);
               $('.enquiry_details').modal();               
           }
       });
}

$(document).ready(function() {

    $('#datadisplay').DataTable( {
        "aProcessing": true,
        "aServerSide": true,
        "ajax": '<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/ajax_enquiry_list',
        "aoColumns": [
            { "data": "display_type" },
            { "data": "customer_name" },
            { "data": "homestay_experience_name" },
            { "data": "contact_email" },
            { "data": "contact_no" },
            { "data": "start_date" },
            { "data": "end_date" },
            { "data": "no_of_adult" },
            { "data": "no_of_children" },
           
        ],
        "createdRow": function( row, data, dataIndex){
            
                
                if( data['type'] ==  'Quote'){
                    $(row).addClass('redClass');
                }
        },

    } );
} );

</script>
<div class="enquiry_details" style="display:none;">
  <div id="ajaxDetails"></div>
</div>