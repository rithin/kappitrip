<div class="row">
	<div class="col-md-12">
		<div class="form-group">
	 		<p class="col-xs-12 control-label" style="text-align: center;font-weight: bold; color: blue; text-transform:uppercase; padding:4px; ">Enquiry Details - <?php 
				if(@$enquiryDetails['Homestay']['name']!="")				
					echo $enquiryDetails['Homestay']['name']; 
				else if(@$enquiryDetails['Experience']['name']!="")				
					echo $enquiryDetails['Experience']['name'];
				?></p>	
		</div>
		<p style="color:red" id="errorMsg"></p>
		
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Customer Name:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo $enquiryDetails['BookingEnquiry']['full_name']; ?>
			</div>
        </div>
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Contact No:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo $enquiryDetails['BookingEnquiry']['contact_no']; ?>
			</div>
        </div>
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Customer Email:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo $enquiryDetails['BookingEnquiry']['contact_email']; ?>
			</div>
        </div>
		<?php if(@$enquiryDetails['Homestay']['name']!=""){		?>
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Homestay:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo $enquiryDetails['Homestay']['name']; ?>
			</div>
        </div>
		<?php } else if(@$enquiryDetails['Experience']['name']!=""){		?>
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Experience:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo $enquiryDetails['Experience']['name']; ?>
			</div>
        </div>
		<?php } ?>
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Date:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo $enquiryDetails['BookingEnquiry']['start_date']; ?> to <?php echo $enquiryDetails['BookingEnquiry']['end_date']; ?>
			</div>
        </div>
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Guest:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo "Adults: ". $enquiryDetails['BookingEnquiry']['no_of_adult']." and Children: ".$enquiryDetails['BookingEnquiry']['no_of_children']; ?>
			</div>
        </div>
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Price:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo $enquiryDetails['BookingEnquiry']['special_price']; ?>
			</div>
        </div>
		<div class="form-group col-md-12">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Note:</label>
       
			<div class="col-md-8 col-xs-12" style="color:green; font-weight:bold;"> 
				<?php echo $enquiryDetails['BookingEnquiry']['message_box']; ?>
			</div>
        </div>
		
	</div>
</div>

<style>

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}
.modal{
		max-width: 600px !important;
	}
.form-group{
	margin-bottom:6px;		
	}
.form-horizontal .control-label {
	text-align: left;
	}
</style>