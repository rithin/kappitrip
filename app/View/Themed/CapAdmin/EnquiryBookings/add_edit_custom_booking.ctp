<?php echo $this->Html->script('plugins/bootstrap/bootstrap-datepicker.js'); ?>

<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."enquiry_bookings/submit_cutom_enquiry";
                 echo $this->Form->create('BookingEnquiry', array('id'=>'BookingEnquiry', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Custom</strong> Quote</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Send a custom link to customer for making the payment and book.</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of the Person</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    
                                                        <?php echo $this->Form->input('guest_name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'guest_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            

                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('email',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'email','autofocus','data-rule-required'=>"true",'type'=>'email','error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Type of Request</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                       
                                                    <?php 
													$array_request_types=array('Property'=>'Property','Experience'=>'Experience');
													
													echo $this->Form->input('type_of_request',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'instant_book','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false, 'options'=>$array_request_types,'onclick'=>'javascript:checkRequestType(this.value)' ));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                                
                                                                                        
                                            <div class="form-group" id="Property" style="display:none;">
                                                <label class="col-md-3 control-label">Property</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php 
                                                   //debug($array_experience_types);
                                                    echo $this->Form->input('property_id', array('options'=>$array_properties,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'property_id',
                                                        'empty'=>'--Select--','onchange'=>'javascript:findrooms(this.value)')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>                                            
                                            
                                             <div class="form-group" id="experience" style="display:none;">
                                                <label class="col-md-3 control-label">Experience</label>
                                                <div class="col-md-9 col-xs-12" >                                            
                                                    <?php 
                                                    echo $this->Form->input('experience_id', array('options'=>$array_experiences, 'div'=>false, 'label'=>false, 'class'=>'form-control','type'=>'select', 'id' =>'experience_id','empty'=>'--Select--', 'onchange'=>'javascript:findSlots(this.value)')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											
											<div class="form-group" id="Property_rooms" style="display:none;">
                                                <label class="col-md-3 control-label">No of Rooms</label>
                                                <div class="col-md-9 col-xs-12" id="rooms_list">                                            
                                          
                                                </div>
                                            </div>
                                            
                                            <div class="form-group" id="experience_slot_div" style="display:none;">
                                                <label class="col-md-3 control-label">Experience Slots</label>
                                                <div class="col-md-9 col-xs-12" >                                            
                                                    <?php 
                                                    echo $this->Form->input('experience_slot', array('options'=>array(), 'div'=>false, 'label'=>false, 'class'=>'form-control',  'type'=>'select', 'id' =>'experience_slot', 'empty'=>'--Select--')); ?>
                                                </div>
                                            </div>
                                            
                                            
                                          <div class="form-group">
                                                <label class="col-md-3 control-label">Phone No</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('phone_no',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'phone_no','autofocus','data-rule-required'=>"true",'error'=>false));?>

                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">No of Guest</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('no_of_guest',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'no_of_guest','autofocus','data-rule-required'=>"true",'type'=>'number','error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											<div class="form-group" id="no_of_child">
                                                <label class="col-md-3 control-label">No of Children</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('no_of_child',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'no_of_child','autofocus','data-rule-required'=>"true",'type'=>'number','error'=>false)); ?>
                                              
                                                </div>
                                            </div>
										
											
                                        </div>
                                        <div class="col-md-6">
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Special Price</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('price',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'price','autofocus','data-rule-required'=>"true",'type'=>'number','error'=>false));?>

                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Start Date</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('checkin_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'checkin_date', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group" id="checkout_date">
                                                <label class="col-md-3 control-label">End Date</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('checkout_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'checkout_date', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Notes</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                      <?php echo $this->Form->input('notes',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'notes', 'autofocus','data-rule-required'=>"true",'error'=>false, 'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>     
                                                                           
                                            
                                        </div>                                          
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                       

                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right" type="submit">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>
<script>

function checkRequestType(request_type)
{
	//console.log(request_type);
	if(request_type=='Property'){
		$('#Property_rooms').show();
		$('#Property').show();
		$('#experience').hide();
        $('#no_of_child').show();
        $('#checkout_date').show();
        $('#experience_slot_div').hide();
	}else if(request_type=='Experience'){
		$('#experience').show();
        $('#experience_slot').show();        
		$('#Property').hide();
		$('#Property_rooms').hide();
        $('#no_of_child').hide();
        $('#checkout_date').hide();
        
	}
}
function findrooms(property_id){
	var roomdtls="";
	 $.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/findRooms",
           data: 'property_id='+ property_id,
           type: "POST",
           success: function(response) {
			   //console.log(response);
               var restultOutput=JSON.parse(response);
			 // console.log(restultOutput);
			   $.each(restultOutput, function(key, item) {
				//   console.log(item);
				   roomdtls+='<label><input type="checkbox"  id="room_'+key+'" onclick=""  name="BookingEnquiry.rooms[]" class="option-input checkbox" value="'+key+'" style="width:32px !important; height:20px; margin-bottom: 3px !important;"> '+ item +' </label>';
			   });
			   
			   $('#rooms_list').html(roomdtls);
		   }
       });
}
    
function findSlots(experience_id){
    $('#experience_slot_div').show();   
    $.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/findSlots",
           data: 'experience_id='+ experience_id,
           type: "POST",
           success: function(response) {
              var slotdtls = $.parseJSON(response);            
              $.each(slotdtls, function(id, itemValue) {             
                   $('#experience_slot').append('<option value="' + id + '">' + itemValue + '</option>');

               });
			   
            }
       });
}
</script>