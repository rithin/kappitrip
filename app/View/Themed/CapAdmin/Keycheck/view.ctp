  <div class="container">
            <!-- Breadcrumbs line -->
            <div class="crumbs">
                <ul id="breadcrumbs" class="breadcrumb">
<!--                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo Configure::read('nuwed_root_path') ?>pages/home">Dashboard</a>
                    </li>-->
                    <li class="current">
                        Key check in/out Management
                    </li>
                </ul>

                
            </div>
            <!-- /Breadcrumbs line -->

            <!--=== Page Header ===-->
            <div class="page-header">
                <div class="page-title">
                    <h3>Key check in/out</h3>
<!--                    <span>Good morning, John!</span>-->
                </div>

                <!-- Page Stats -->
                <!--<ul class="page-stats">
                    <li>
                        <div class="summary">
                            <span>New orders</span>
                            <h3>17,561</h3>
                        </div>
                        <div id="sparkline-bar" class="graph sparkline hidden-xs">20,15,8,50,20,40,20,30,20,15,30,20,25,20</div>
                        <!-- Use instead of sparkline e.g. this:
                        <div class="graph circular-chart" data-percent="73">73%</div>
                      
                    </li>
                    <li>
                        <div class="summary">
                            <span>My balance</span>
                            <h3>$21,561.21</h3>
                        </div>
                        <div id="sparkline-bar2" class="graph sparkline hidden-xs">20,15,8,50,20,40,20,30,20,15,30,20,25,20</div>
                    </li>
                </ul>-->
                <!-- /Page Stats -->
            </div>
            <!-- /Page Header -->

       <div class="row">
                <div class="col-md-12">
                    <div class="widget box">
                        <div class="widget-header">
                            <h4><i class="icon-reorder"></i>Key Check in/out</h4>
                            <div class="toolbar no-padding">
                                <div class="btn-group">
                                    <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content no-padding">
                        <div class="tabbable box-tabs">
                                <ul class="nav nav-tabs" style="top:-37px; margin-right:25px;">
                                    
                                    
                                    
                                    <li  <?php if($active_flag=="add_edit") echo "class='active'"; else " "; ?>><a href="#box_tab2" data-toggle="tab">Add/Edit Key check in/out </a></li>
                                    <li <?php  if($active_flag=="list") echo "class='active'"; else " "; ?>><a href="#box_tab1" data-toggle="tab">List Key check in/out</a></li>
                                </ul>
                               <div class="tab-content"> 
                               <div class="tab-pane <?php  if($active_flag=="list") echo "active"; else ' '; ?>" id="box_tab1" style="overflow-x: scroll">
                                  
                                   
                                   <table class="table table-striped table-bordered table-hover table-checkable  datatable" >
                                    <thead>
                                        <tr>
                                            
                                            <th class="checkbox-column"></th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Technician</th>
                                            
                                            <th>Site</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                 <?php 
                                 $slno=1;
                                    foreach($restult_keycheck as $results){
                                 ?>
                                    <tr>
                                        
                                        <td><?php echo $slno; ?></td>
                                       
                                        <td><?php echo $results['Keycheck']['keycheck_date'];?></td>
                                        <td><?php echo $results['Keycheck']['keycheck_time'];?></td>
                                        <td><?php echo $results['User']['first_name']." ".$results['User']['last_name'];?></td>
                                        <td><?php echo $results['Site']['site_name'];?></td>
                                        
                                        <td><a href="../keycheck/view/<?php echo $results['Keycheck']['id']; ?>" title="Edit" ><i class="icon-edit"></i></a>
                                        </td>
                                    </tr>
                                <?php $slno++; } ?>
                                </tbody>
                            </table>
                            </div>
                            <div class="tab-pane <?php if($active_flag=="add_edit") echo "active"; else " "; ?>" id="box_tab2">
                                
                              <?php			 
                              $action = Configure::read('nuwed_root_path')."keycheck/save";
                              echo $this->Form->create('Keycheck', array('id'=>'addEditUserForm', 'url'=>$action, 'type'=>'file','class'=>'form-horizontal row-border', 'novalidate' => true));
                              echo $this->Form->input('Keycheck.id',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'id', 'value'=>$user_id));
			      
                              echo $this->Form->input('Keycheck.status_id',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'status_id', 'value'=>1));
		              ?>
                                  <div class="form-group">
                                      
                                      <div class="col-md-12">
                                      <div class="row">
                                              
                                            <?php if(empty($user_id)){ ?>

                                                <div class="col-md-6">
                                               <?php echo $this->Form->input('keycheck_date',array('div'=>false, 'type'=>'text','label'=>false, 'class'=>'form-control required datepicker', 'id'=>'keycheck_date', 'error'=>false));?>
                                                  <span class="help-block">Date <span class="mandatory">*</span></span>
                                              </div>

                                              <div class="col-md-6">
                                               <?php echo $this->Form->input('keycheck_time',array('div'=>false, 'type'=>'text','label'=>false, 'class'=>'form-control required timepicker-fullscreen', 'id'=>'keycheck_time', 'error'=>false));?>
                                                  <span class="help-block">Time<span class="mandatory">*</span></span>
                                              </div>



                                              <?php } ?>

                                              <div class="col-md-6">

                                              <?php

                                          

                                          echo $this->Form->input('technician', array('options'=>$restult_technicians,
                                                        'selected'=>@$selected_technician,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control required', 'type'=>'select', 'id' =>'log_access',
                                                        'empty'=>'--Select--'));
                                          ?>
                                              <span class="help-block">Technician Name <span class="mandatory">*</span></span>
                                          </div>

                                          <?php if(empty($user_id)){ ?>

                                          <div class="col-md-6">

                                              <?php


                                          echo $this->Form->input('site', array('options'=>$result_sites,
                                                        'selected'=>@$selected_site,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control required', 'type'=>'select', 'id' =>'site',
                                                        'empty'=>'--Select--'));
                                          ?>
                                              <span class="help-block">Site Name <span class="mandatory">*</span></span>
                                          </div>



                                               <div class="col-md-6">
                                               <?php echo $this->Form->input('support_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'support_name', 'error'=>false,'value'=>$logged_user_name));?>
                                                  <span class="help-block">Support Name<span class="mandatory">*</span></span>
                                              </div>

                                             <?php } ?>
                                             

                                              <div class="col-md-6">
                                               <?php echo $this->Form->input('ticket_number',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'access_time', 'error'=>false));?>
                                                  <span class="help-block">Ticket Number<span class="mandatory">*</span></span>
                                              </div>

                                              <div class="col-md-6">

                                              <?php

                                              $reason_array =  array('Maintenance'=>'Maintenance','Install'=>'Install','Break down'=>'Break down');

                                          echo $this->Form->input('reason', array('options'=>$reason_array,
                                                        'selected'=>@$my_shop,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control required', 'type'=>'select', 'id' =>'reason',
                                                        'empty'=>'--Select--'));
                                          ?>
                                              <span class="help-block">Reason <span class="mandatory">*</span></span>
                                          </div>


                                              <div class="col-md-6">

                                              <?php

                                              $checked_array = array('Yes'=>'Yes','No'=>'No');

                                          echo $this->Form->input('checked', array('options'=>$checked_array,
                                                        'selected'=>@$my_shop,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control required', 'type'=>'select', 'id' =>'log_access',
                                                        'empty'=>false));
                                          ?>
                                              <span class="help-block">Checked <span class="mandatory">*</span></span>
                                          </div>

                                          
                                         
                                          
                                          
                                              
                                          </div>
                                               
                                              
                                          
                                          
                                          
                                      </div>
                                  </div>

                                  
                                  <div class="form-group">
                                      
                                      <div class="col-md-12">
                                      <div class="row">
                                              
                                             <div class="col-md-12"> 
                                              <p class="btn-toolbar btn-toolbar-demo" >
                                             <input type="submit" value="Save" id="save" class="btn btn-success"> 
                                             &nbsp;&nbsp;
                                             <a id="cancel" class="btn btn-danger" href="<?php echo Configure::read('nuwed_root_path'); ?>users/view">Cancel</a>
                                              </p>
                                             </div>
                                              
                                          </div>
                                      </div>
                                  </div>
                               </form>
                            </div>
                              
                       </div> 
                    </div> <!-- /.tabbable portlet-tabs -->
                </div>
            </div>
        </div>
    </div>
</div>             

<script language="javascript">


</script>