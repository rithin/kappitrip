 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h2 class="panel-title"><b>Coupon List(s)</b></h2>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
									
                                    <table id="datadisplay" class="table datatable">
                                        <thead>
                                            <tr>
												<th>Coupon Code</th>
                                                <th>Description</th>
                                                <th>Discount Type</th>
                                                <th>Value</th>
                                                <th>Expiry</th>
												<th>Status</th>
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->


<script>

$(document).ready(function() {

    $('#datadisplay').DataTable( {
        "aProcessing": true,
        "aServerSide": true,
        "ajax": '<?php echo Configure::read('app_root_path'); ?>manage_coupons/ajax_coupon_list',
        "aoColumns": [
            { "data": "coupon_code" },
            { "data": "description" },
            { "data": "discount_type" },
            { "data": "value" },
            { "data": "expiry_date" },
            { "data": "status" },
            { "data": "actions" },
           
        ],
        

    } );
} );

</script>