<?php echo $this->Html->script('plugins/bootstrap/bootstrap-datepicker.js'); ?>
<?php echo $this->Html->script('plugins/bootstrap/bootstrap-timepicker.min.js'); ?>
<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_coupons/save";
                 echo $this->Form->create('Product', array('id'=>'addCoupon', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
		 		 echo $this->Form->input('status',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false,'value'=>1));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Coupon</h3>
                                    <ul class="panel-controls">
                                        <li><a thref="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new coupons.</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
										<div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Coupon Code (Unique)</label>
                                                                                         
                                                    <div class="col-md-9 col-xs-12" > 
                                                    
                                                        <?php echo $this->Form->input('coupon_code',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'coupon_code','autofocus','data-rule-required'=>"true", 'error'=>false,'readonly'=>true,'style'=>'color: red;font-weight: bold;'));?>
                                                        
                                                                                                
													<button type="button" class="btn btn-warning" onclick="javascript:generateCoupon()">Generate Coupon</button>
                                                      </div>                                                  
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description</label>
                                                <div class="col-md-9 col-xs-12" >                                                        
                                                    
                                                        <?php echo $this->Form->input('description',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'description','autofocus','data-rule-required'=>"true", 'error'=>false,'type'=>'textarea'));?>
                                                        
                                                </div>
                                            </div>
                                                
                                                                                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Discount Type</label>
                                                <div class="col-md-9 col-xs-12">
                                                    
                                                    <input type="radio" name="data[Product][discount_type]" value="percentage" style="padding: 14px;  margin: 10px;padding-right: 40px"> Percentage
                                                    <input type="radio" name="data[Product][discount_type]" value="amount" style="padding: 14px;  margin: 10px;padding-right: 40px"> Amount
                                                    
                                                 <?php 
                                                   //$discount_type=array('percentage'=>'Percentage','amount'=>'Amount');
                                                    /*echo $this->Form->input('discount_type', array('options'=>$discount_type,'div'=>false, 'label'=>false, 
                                                        'class'=>'validate[required]','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'legend'=>false, 'id' =>'discount_type')); */
                                                 ?>
                                                  
                                                </div>
                                            </div>                                            
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Value</label>
                                                <div class="col-md-9 col-xs-12" >                                            
                                                   <?php echo $this->Form->input('value',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'value','autofocus','data-rule-required'=>"true",'type'=>'number','error'=>false));?>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Expiry</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                               <?php echo $this->Form->input('expiry_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'expiry_date','autofocus', 'error'=>false));?>
                                                </div>
                                            </div>
                                        </div>
                                                                            
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                          

                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button type="reset" class="btn btn-default">Cancel</button>                                    
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>


<script>

function generateCoupon(){

	  $.ajax({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>manage_coupons/couponGenerator",
          data:"",
          success: function(response)
          {	 
			  $('#coupon_code').val(response);
			 //console.log(response);
		  }
     
	 }); 
}


$("#addCoupon").validate({
                ignore: [],
                rules: {                                            
                        'data[Product][coupon_code]': {
                                required: true,
                        },
                        'data[Product][description]': {
                                required: true,
                        },
                        'data[Product][discount_type]': {
                                required: true,
                        },
                        'data[Product][product_location]': {
                                required: true,
                                
                        },
                        'data[Product][value]': {
                                required: true,
                        },
                        'data[Product][expiry_date]': {
                                required: true,
                        },
                        
         },
         errorPlacement: function(error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter(element);
            }
         },
                
        
        
     });   
     
</script>
