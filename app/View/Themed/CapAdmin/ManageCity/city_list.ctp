 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">List Cities</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                     <a href="<?php echo Configure::read('app_root_path'); ?>/manage_city/add_edit">&nbsp;List Cities &nbsp;</a>
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>City Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php foreach($city as $details){
   // debug($room);
	
                                                ?>

                                            <tr>	
                                                <td><?php echo $details['City']['name']; ?></td>
                                                 										
                                                <td>   <a href="<?php echo Configure::read('app_root_path'); ?>/manage_city/add_edit/<?php echo $details['City']['name']; ?>">&nbsp;Edit &nbsp;</a>
                                                <a href="<?php  echo Configure::read('app_root_path'); ?>manage_city/delete/<?php  echo $details['City']['name']; ?>">&nbsp;Delete &nbsp;</a>                                            </td>
                                            </tr>
                                <?php } ?>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->