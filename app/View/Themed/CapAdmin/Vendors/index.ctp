<div class="box">
<span class="red-mark text-center"> <?php echo $this->Session->flash(); ?></span>
		<div class="content">
			<!-- Login Formular -->
			<?php
                 $action = Configure::read('nuwed_root_path')."users/login";
                 echo $this->Form->create('User', array('id'=>'loginForm', 'url'=>$action, 'type'=>'file','class'=>'form-horizontal row-border', 'novalidate' => true));                                
                              
			 ?>
             <div class="alert fade in alert-danger" style=" <?php
			 $msg=$this->Session->flash('bad');
			  if($msg=="") echo "display: none;";  ?>">
					<i class="icon-remove close" data-dismiss="alert"></i>
					<?php  
                        echo $this->Session->flash('good');
                        echo $msg;
                    ?>
                </div>
				<!-- Title -->
				<h3 class="form-title">Sign In to your Account</h3>

				<!-- Error Message -->
				<div class="alert fade in alert-danger" style="display: none;">
					<i class="icon-remove close" data-dismiss="alert"></i>
					Enter any username and password.
				</div>

				<!-- Input Fields -->
				<div class="form-group">
					<!--<label for="username">Username:</label>-->
					<div class="input-icon">
						<i class="icon-user"></i>
                        <?php echo $this->Form->input('username',array('div'=>false, 'label'=>false, 'class'=>'form-control required email has-error', 'id'=>'username','autofocus','data-rule-required'=>"true",'data-msg-required'=>"Please enter your username.", 'error'=>false));?>

					</div>
				</div>
				<div class="form-group">
					<!--<label for="password">Password:</label>-->
					<div class="input-icon">
						<i class="icon-lock"></i>
 				<?php echo $this->Form->input('password',array('div'=>false, 'label'=>false, 'class'=>'form-control required has-error', 'id'=>'password','autofocus','data-rule-required'=>"true",'data-msg-required'=>"Please enter your password.", 'error'=>false));?>
					</div>
				</div>
				<!-- /Input Fields -->

				<!-- Form Actions -->
				<div class="form-actions">
					<label class="checkbox pull-left"><input type="checkbox" class="uniform" name="remember"> Remember me</label>
					<button type="submit" class="submit btn btn-primary pull-right">
						Sign In <i class="icon-angle-right"></i>
					</button>
				</div>
			</form>
			<!-- /Login Formular -->

			<!-- Register Formular (hidden by default) -->
			<form class="form-vertical register-form" action="index.html" method="post" style="display: none;">
				<!-- Title -->
				<h3 class="form-title">Sign Up for Free</h3>

				<!-- Input Fields -->
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-user"></i>
						<input type="text" name="username" class="form-control" placeholder="Username" autofocus data-rule-required="true" />
					</div>
				</div>
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-lock"></i>
						<input type="password" name="password" class="form-control" placeholder="Password" id="register_password" data-rule-required="true" />
					</div>
				</div>
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-ok"></i>
						<input type="password" name="password_confirm" class="form-control" placeholder="Confirm Password" data-rule-required="true" data-rule-equalTo="#register_password" />
					</div>
				</div>
				<div class="form-group">
					<div class="input-icon">
						<i class="icon-envelope"></i>
						<input type="text" name="Email" class="form-control" placeholder="Email address" data-rule-required="true" data-rule-email="true" />
					</div>
				</div>
				<div class="form-group spacing-top">
					<label class="checkbox"><input type="checkbox" class="uniform" name="remember" data-rule-required="true" data-msg-required="Please accept ToS first."> I agree to the <a href="javascript:void(0);">Terms of Service</a></label>
					<label for="remember" class="has-error help-block" generated="true" style="display:none;"></label>
				</div>
				<!-- /Input Fields -->

				<!-- Form Actions -->
				<div class="form-actions">
					<button type="button" class="back btn btn-default pull-left">
						<i class="icon-angle-left"></i> Back</i>
					</button>
					<button type="submit" class="submit btn btn-primary pull-right">
						Sign Up <i class="icon-angle-right"></i>
					</button>
				</div>
			</form>
			<!-- /Register Formular -->
		</div> <!-- /.content -->

		<!-- Forgot Password Form -->
		<div class="inner-box">
			<div class="content">
				<!-- Close Button -->
				<i class="icon-remove close hide-default"></i>

				<!-- Link as Toggle Button -->
				<a href="#" class="forgot-password-link">Forgot Password?</a>

				<!-- Forgot Password Formular -->
				<form class="form-vertical forgot-password-form hide-default" action="<?php echo Configure::read('nuwed_root_path').'users/forgot'; ?>" method="post">
					<!-- Input Fields -->
					<div class="form-group">
						<!--<label for="email">Email:</label>-->
						<div class="input-icon">
							<i class="icon-envelope"></i>
							<input type="text" name="email" id="forgot_email" class="form-control" placeholder="Enter email address" data-rule-required="true" data-rule-email="true" data-msg-required="Please enter your email." />
						</div>
					</div>
					<!-- /Input Fields -->

					<button type="submit" class="submit btn btn-default btn-block">
						Reset your Password
					</button>
				</form>
				<!-- /Forgot Password Formular -->

				<!-- Shows up if reset-button was clicked -->
				<div class="forgot-password-done hide-default">
					<i class="icon-ok success-icon"></i> <!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
					<span>Great. We have sent the link to reset your password to your email. Please check.</span>
				</div>
                                
                                <div class="forgot-password-error has-error hide-default">
					<i class="icon-remove danger-icon icon-2x has-error help-block"></i> <!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
					<span class="has-error help-block">Email-Id entered is not registered with us. Please enter a different Email-Id to proceed.</span>
				</div>
			</div> <!-- /.content -->
		</div>
		<!-- /Forgot Password Form -->
	</div>
    
    
    
	<!-- Single-Sign-On (SSO) 
	<div class="single-sign-on">
		<span>or</span>

		<button class="btn btn-facebook btn-block">
			<i class="icon-facebook"></i> Sign in with Facebook
		</button>

		<button class="btn btn-twitter btn-block">
			<i class="icon-twitter"></i> Sign in with Twitter
		</button>

		<button class="btn btn-google-plus btn-block">
			<i class="icon-google-plus"></i> Sign in with Google
		</button>
	</div>
	<!-- /Single-Sign-On (SSO) -->