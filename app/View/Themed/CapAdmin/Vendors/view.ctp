  <div class="container">
            <!-- Breadcrumbs line -->
            <div class="crumbs">
                <ul id="breadcrumbs" class="breadcrumb">
<!--                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo Configure::read('nuwed_root_path') ?>pages/home">Dashboard</a>
                    </li>-->
                    <li class="current">
                        Vendors Management
                    </li>
                </ul>

                
            </div>
            <!-- /Breadcrumbs line -->

            <!--=== Page Header ===-->
            <div class="page-header">
                <div class="page-title">
                    <h3>Vendors</h3>
<!--                    <span>Good morning, John!</span>-->
                </div>

                <!-- Page Stats -->
                <!--<ul class="page-stats">
                    <li>
                        <div class="summary">
                            <span>New orders</span>
                            <h3>17,561</h3>
                        </div>
                        <div id="sparkline-bar" class="graph sparkline hidden-xs">20,15,8,50,20,40,20,30,20,15,30,20,25,20</div>
                        <!-- Use instead of sparkline e.g. this:
                        <div class="graph circular-chart" data-percent="73">73%</div>
                      
                    </li>
                    <li>
                        <div class="summary">
                            <span>My balance</span>
                            <h3>$21,561.21</h3>
                        </div>
                        <div id="sparkline-bar2" class="graph sparkline hidden-xs">20,15,8,50,20,40,20,30,20,15,30,20,25,20</div>
                    </li>
                </ul>-->
                <!-- /Page Stats -->
            </div>
            <!-- /Page Header -->

       <div class="row">
                <div class="col-md-12">
                    <div class="widget box">
                        <div class="widget-header">
                            <h4><i class="icon-reorder"></i>Manage Vendors</h4>
                            <div class="toolbar no-padding">
                                <div class="btn-group">
                                    <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content no-padding">
                        <div class="tabbable box-tabs">
                                <ul class="nav nav-tabs" style="top:-37px; margin-right:25px;">
                                    <!--<li><a href="#box_tab3" data-toggle="tab">Section 3</a></li>
                                     <?php //if($active_flag=="add_edit") echo "class='active'"; else ''; ?>
                                    <?php // if($active_flag=="list") echo "class='active'"; else ''; ?>
                                    -->
                                    
                                    
                                    <li  <?php if($active_flag=="add_edit") echo "class='active'"; else " "; ?>><a href="#box_tab2" data-toggle="tab">Add/Edit Vendor </a></li>
                                    <li <?php  if($active_flag=="list") echo "class='active'"; else " "; ?>><a href="#box_tab1" data-toggle="tab">List Vendors</a></li>
                                </ul>
                               <div class="tab-content"> 
                               <div class="tab-pane <?php  if($active_flag=="list") echo "active"; else ' '; ?>" id="box_tab1" style="overflow-x: scroll">
                                  
                                   
                                   <table class="table table-striped table-bordered table-hover table-checkable  datatable" >
                                    <thead>
                                        <tr>
                                            <!--<th class="checkbox-column">
                                                <input type="checkbox" class="uniform">
                                            </th>-->
                                            <th class="checkbox-column"></th>
                                            <th>Vendor</th>
                                            <th>Contact</th>
                                            <th>Site Contact</th>
                                            
                                            <!--<th>Status</th>-->
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                 <?php 
                                 $slno=1;
                                    foreach($restult_vendors as $results){
                                 ?>
                                    <tr>
                                        
                                        <td><?php echo $slno; ?></td>
                                        <!--<td class="checkbox-column">
                                    <?php echo $this->Form->input('',array('type'=>'checkbox', 'value'=>$results['Vendor']['id'] )); ?>
                                        </td>-->
                                        <td><?php echo $results['Vendor']['vendor_name'];?></td>
                                        <td><?php echo $results['Vendor']['contact'];?></td>
                                        <td><?php echo $results['Vendor']['site_contact'];?></td>
                                        
                                        <!--<td><?php 
                                                $user_id_new=$results['Vendor']['id'];
                                                 echo $this->Form->input('status_update_id', array('options'=>$restult_status,
                                                        'selected'=>@$results['Vendor']['status_id'],'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'status_update_id',
                                                        'empty'=>'--Select--','onChange'=>'updateStatus('.$user_id_new.',this);'));

                                                //echo $results['Status']['status'];?></td>-->
                                        <td><?php if($results['Vendor']['status_id']=='1'){ ?><!--<a href="javascript:chnagePassword(<?php echo $results['Vendor']['id']; ?>);" title="Change Password" ><i class="icon-cogs"></i></a>--><?php } ?>
                                        &nbsp; &nbsp;<a href="../vendors/view/<?php echo $results['Vendor']['id']; ?>" title="Edit" ><i class="icon-edit"></i></a>
                                        </td>
                                    </tr>
                                <?php $slno++; } ?>
                                </tbody>
                            </table>
                            </div>
                            <div class="tab-pane <?php if($active_flag=="add_edit") echo "active"; else " "; ?>" id="box_tab2">
                                
                              <?php			 
                              $action = Configure::read('nuwed_root_path')."vendors/save";
                              echo $this->Form->create('Vendor', array('id'=>'addEditUserForm', 'url'=>$action, 'type'=>'file','class'=>'form-horizontal row-border', 'novalidate' => true));
                              echo $this->Form->input('Vendor.id',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'id', 'value'=>$user_id));
			      echo $this->Form->input('role_id',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'role_id', 'value'=>2));
                              echo $this->Form->input('Vendor.status_id',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'status_id', 'value'=>1));
		              ?>
                                  <div class="form-group">
                                      
                                      <div class="col-md-12">
                                      <div class="row">
                                              
                                                <div class="col-md-6">
                                               <?php echo $this->Form->input('vendor_name',array('div'=>false, 'label'=>false, 'class'=>'form-control required', 'id'=>'vendor_name', 'error'=>false));?>
                                                  <span class="help-block">Vendor Name <span class="mandatory">*</span></span>
                                              </div>

                                              <div class="col-md-6">
                                               <?php echo $this->Form->input('contact',array('div'=>false, 'label'=>false, 'class'=>'form-control required', 'id'=>'contact', 'error'=>false));?>
                                                  <span class="help-block">Contact<span class="mandatory">*</span></span>
                                              </div>

                                              

                                              <div class="col-md-6">
                                               <?php echo $this->Form->input('site_contact',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'site_contact', 'error'=>false));?>
                                                  <span class="help-block">Site Contact<span class="mandatory">*</span></span>
                                              </div>


                                              
                                              <div class="col-md-6">

                                              <?php

                                              $log_access_array = array('Yes'=>'Yes','No'=>'No');

                                          echo $this->Form->input('log_access', array('options'=>$log_access_array,
                                                        'selected'=>@$my_shop,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control required', 'type'=>'select', 'id' =>'log_access',
                                                        'empty'=>'--Select--'));
                                          ?>
                                              <span class="help-block">Log Access <span class="mandatory">*</span></span>
                                          </div>

                                            
                                              
                                          <div class="col-md-6">
					
                                            <select id="input18" class="select2-select-00 col-md-12 full-width-fix" multiple size="5" name="data[Vendor][vendor_sites][]">
                                            <?php 
                                            foreach($result_sites as $site_id=>$site_val){


                                            echo '<option value="'.$site_id.'">'.$site_val.'</option>';

                                            }

                                            ?>
                                         
                                          </select>
                                          
                                              
                                                               <span class="help-block">Site <span class="mandatory">*</span></span>

                                          </div>
                                               
                                              
                                          
                                          
                                          
                                      </div>
                                  </div>

                                  
                                  <div class="form-group">
                                      
                                      <div class="col-md-12">
                                      <div class="row">
                                              
                                             <div class="col-md-12"> 
                                              <p class="btn-toolbar btn-toolbar-demo" >
                                             <input type="submit" value="Save" id="save" class="btn btn-success"> 
                                             &nbsp;&nbsp;
                                             <a id="cancel" class="btn btn-danger" href="<?php echo Configure::read('nuwed_root_path'); ?>users/view">Cancel</a>
                                              </p>
                                             </div>
                                              
                                          </div>
                                      </div>
                                  </div>
                               </form>
                            </div>
                              
                       </div> 
                    </div> <!-- /.tabbable portlet-tabs -->
                </div>
            </div>
        </div>
    </div>
</div>             

<script language="javascript">
//javascript call for change password.
function chnagePassword(user_id){
    jQuery.ajax({
	  type: 'POST',
	  url: "<?php echo Configure::read('nuwed_root_path'); ?>/users/change_password/"+user_id,
	  success: function(data) {
		 bootbox.dialog({
		  message: data,
		  title: "Change Password",
		  buttons: {
			  success: {
				  label: "Save!",
				  className: "btn-success",
				  callback: function() {
					if ($("#changePassword").valid()) {
						if(savePasswordData()==true)
							return true;
						else
							return true;	
					}
					return false;
				  }
			  },
			  danger: {
				  label: "Cancel",
				  className: "btn-danger",
				  callback: function() {
					  console.log("uh oh, look out!");
				  }
			  },
		  }
	  }); 

	 }
   });
}
function savePasswordData(){
	var email = $('#email_change_pass').val();
    var password = $('#password_change_pass').val();
	var id = $('#id_change_pass').val();
    var confirmSend;
    var jsonval= {email:email,password:password,id:id};
	/* JSON.stringify({
            email: $("#email_change_pass").val(),
            password: $("#password_change_pass").val(),
            id: $("#id_change_pass").val()
        })*/
	//console.log(JSON.stringify(json_data))
	//JSON.stringify(json_data)
   jQuery.ajax({
      type: "POST",
      url: "../users/savePasswordData",
      data:jsonval,
      //contentType: "application/json; charset=utf-8",
      dataType: "json",
      cache: false,
      success: function(data) {
		  console.log(data);
        if (data == 1) {
          bootbox.alert("Successfully changed the password..!!");
		 	return true;
        } else {
          bootbox.alert("DATABASE OPERATION FAILED: Try after sometime.");
		  return false;
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        bootbox.alert("OPERATION FAILED: Try after sometime.");
		return false;
      }
    });
	return false
}


function updateStatus(user_id,data){
	
	//console.log(data.value);
	  var statusId=data.value;
	  var jsonval= {user_id:user_id,statusId:statusId};
	  jQuery.ajax({
      type: "POST",
      url: "../users/updatePassword",
      data:jsonval,
      //contentType: "application/json; charset=utf-8",
      dataType: "json",
      cache: false,
      success: function(data) {
		 // console.log(data);
        if (data == 1) {
          bootbox.alert("Successfully updated.",function(){
              window.location.reload(true);
          });
        } else {
          bootbox.alert("DATABASE OPERATION FAILED: Try after sometime.");
		  return false;
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        bootbox.alert("OPERATION FAILED: Try after sometime.");
		return false;
      }
    });
	return false;
}
</script>