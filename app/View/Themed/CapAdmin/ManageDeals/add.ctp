<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<?php echo $this->Html->script('plugins/bootstrap/bootstrap-datetimepicker.js'); ?>
<?php echo $this->Html->css('bootstrap/bootstrap-datetimepicker.css'); ?>
<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_deals/save";
                 echo $this->Form->create('Deal', array('id'=>'addDeal', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Deals</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new deals.</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
										<div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Deal title</label>
                                                
                                                <div class="col-md-9 col-xs-12">
                                                        <?php echo $this->Form->input('deal_title',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'coupon_code','autofocus','data-rule-required'=>"true", 'error'=>false));?>
                                                        
                                                                                              
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Type of Request</label>
                                                <div class="col-md-9 col-xs-12">
                                                       
                                                    
<!--
                                                    <input type="radio" name="data[Deal][deal_item]" value="Property" style="padding: 14px;  margin: 10px;padding-right: 40px" onclick="javascript:checkRequestType(this.value)"> Property
                                                    <input type="radio" name="data[Deal][deal_item]" value="Experience" style="padding: 14px;  margin: 10px;padding-right: 40px" onclick="javascript:checkRequestType(this.value)" > Experience
-->
<!--                                                    <input type="radio" name="data[Deal][deal_item]" value="Experience" style="padding: 14px;  margin: 10px;padding-right: 40px" onclick="javascript:checkRequestType(this.value)"> Product-->
                                                    
                                                    <?php 
													$array_request_types=array('Property'=>'Property','Experience'=>'Experience','Product'=>'Product');
													
													echo $this->Form->input('deal_item',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'deal_item','autofocus','error'=>false,'legend'=>false, 'options'=>$array_request_types,'onclick'=>'javascript:checkRequestType(this.value)' ));?>
                                                        
                                                    
                                                </div>
                                            </div>
                                                
                                                                                        
                                            <div class="form-group" id="property" style="<?php if(@$this->request->data['Deal']['homestay_id']!='') echo ""; else echo "display:none;"; ?>">
                                                <label class="col-md-3 control-label">Property</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php 
                                                   //debug($array_experience_types);
                                                    echo $this->Form->input('homestay_id', array('options'=>$array_properties,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'homestay_id',
                                                        'empty'=>'--Select--')); ?>
                                                </div>
                                            </div>                                            
                                            
                                             <div class="form-group" id="experience" style="<?php if(@$this->request->data['Deal']['experience_id']!='') echo ""; else echo "display:none;"; ?>">
                                                <label class="col-md-3 control-label">Experience</label>
                                                <div class="col-md-9 col-xs-12" >                                            
                                                    <?php 
                                                    echo $this->Form->input('experience_id', array('options'=>$array_experiences, 'div'=>false, 'label'=>false, 'class'=>'form-control','type'=>'select', 'id' =>'experience_id','empty'=>'--Select--')); ?>
                                                </div>
                                            </div>
											
											<div class="form-group" id="products" style="<?php if(@$this->request->data['Deal']['product_id']!='') echo ""; else echo "display:none;"; ?>">
                                                <label class="col-md-3 control-label">Product</label>
                                                <div class="col-md-9 col-xs-12" >                                            
                                                    <?php 
                                                    echo $this->Form->input('product_id', array('options'=>$array_products, 'div'=>false, 'label'=>false, 'class'=>'form-control','type'=>'select', 'id' =>'product_id','empty'=>'--Select--')); ?>
                                                </div>
                                            </div>
                                                                                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Discount Type</label>
                                                <div class="col-md-9 col-xs-12"> 
                                                    
<!--
                                                  <input type="radio" name="data[Deal][offer_type]" value="percentage" style="padding: 14px;  margin: 10px;padding-right: 40px"> Percentage
                                                  <input type="radio" name="data[Deal][offer_type]" value="amount" style="padding: 14px;  margin: 10px;padding-right: 40px"> Amount  
                                                    
-->
                                                 <?php 
                                                   $discount_type=array('percentage'=>'Percentage','amount'=>'Amount');
                                                    echo $this->Form->input('offer_type', array('options'=>$discount_type,'div'=>false, 'label'=>false, 
                                                        'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'legend'=>false, 'id' =>'offer_type')); 
                                                 ?>
                                                </div>
                                            </div>                                            
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Value</label>
                                                <div class="col-md-9 col-xs-12" >                                            
                                                   <?php echo $this->Form->input('offer_value',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'offer_value', 'autofocus','type'=>'number','error'=>false));?>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Start Date</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                               <?php echo $this->Form->input('start_date',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'start_date','type'=>'text','error'=>false));?>
                                                </div>
                                            </div>
											 <div class="form-group">
                                                <label class="col-md-3 control-label">End Date</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                               <?php echo $this->Form->input('end_date',array('div'=>false, 'label'=>false, 'class'=>'form-control','id'=>'end_date','type'=>'text', 'error'=>false));?>
                                                </div>
                                            </div>
                                        </div>
                                                                            
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                          

                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button type="reset" class="btn btn-default">Cancel</button>                                    
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>


<script>
function checkRequestType(request_type)
{
	//console.log(request_type);
	if(request_type=='Property'){
		$('#property').show();
		$('#experience').hide();
		$('#products').hide();
	}else if(request_type=='Experience'){
		$('#experience').show();   
		$('#property').hide();
        $('#products').hide();        
	}
	else if(request_type=='Product'){
		$('#experience').hide();   
		$('#property').hide();
        $('#products').show();
        
	}
}

	
 $(function () {
        $('#start_date').datetimepicker({
			format: "YYYY-MM-DD HH:mm:ss",
  		});
        $('#end_date').datetimepicker({
			format: "YYYY-MM-DD HH:mm:ss",
            useCurrent: false //Important! See issue #1075
        });
        $("#start_date").on("dp.change", function (e) {
            $('#end_date').data("DateTimePicker").minDate(e.date);
        });
        $("#end_date").on("dp.change", function (e) {
            $('#start_date').data("DateTimePicker").maxDate(e.date);
        });
    });	
    
    
    $("#addDeal").validate({
                ignore: [],
                rules: {                                            
                        'data[Deal][deal_title]': {
                                required: true,
                        },
                       
//                        'data[Deal][offer_type]': {
//                                required: true,
//                        },
                        'data[Deal][offer_value]': {
                                required: true,
                                
                        },
                        'data[Deal][start_date]': {
                                required: true,
                        },
                        'data[Deal][end_date]': {
                                required: true,
                        },
                        
         },
         errorPlacement: function(error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter(element);
            }
         },
                
        
        
     });  
    
</script>
