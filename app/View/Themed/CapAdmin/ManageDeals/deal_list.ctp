 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h2 class="panel-title"><b>Deals List</b></h2>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
									
                                    <table id="datadisplay" class="table datatable">
                                        <thead>
                                            <tr>
												<th>Deal Code</th>
                                                <th>Deal Item</th>
                                                <th>Offer Type</th>
                                                <th>Value</th>
                                                <th>Start Date</th>
												<th>End Date</th>
												<th>Status</th>
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                      
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->


<script>

$(document).ready(function() {

    $('#datadisplay').DataTable( {
        "aProcessing": true,
        "aServerSide": true,
        "ajax": '<?php echo Configure::read('app_root_path'); ?>manage_deals/ajax_deal_list',
        "aoColumns": [
            { "data": "deal_title" },
            { "data": "deal_item" },
            { "data": "offer_type" },
            { "data": "offer_value" },
            { "data": "start_date" },
            { "data": "end_date" },
            { "data": "status" },
            { "data": "actions" },
           
        ],
        

    } );
} );

</script>