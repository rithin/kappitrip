<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_experience_type/save";
                 echo $this->Form->create('ExperienceType', array('id'=>'addExperienceType', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Experience Type</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new Experience Types.</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Experience Type</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    
                                                        <?php echo $this->Form->input('type',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'type','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                            
                                          
                                           
                                           
                                                                                 
                                    </div> <!-- END OF FIRST ROW -->
                           
                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>


<script>

function loadSubCategory(data){
	  var categoryId=data.value;
	  var jsonval= {categoryId:categoryId};
	  jQuery.ajax({
      type: "POST",
      url: "../manage_products/loadSubCategory",
      data:jsonval,
      //contentType: "application/json; charset=utf-8",
      dataType: "json",
      cache: false,
      success: function(data) {
		  //console.log(data);
          var $dropdown = $("#sub_category_id");
            $.each(data, function() {
                $dropdown.append($("<option />").val(this.SubCategory.id).text(this.SubCategory.sub_category_name));
            });
      },
      error: function(xhr, ajaxOptions, thrownError) {
       //bootbox.alert("OPERATION FAILED: Try after sometime.");
		return false;
      }
    });
	return false;
}
</script>
