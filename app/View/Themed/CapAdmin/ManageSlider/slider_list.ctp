 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                     <a href="<?php echo Configure::read('app_root_path'); ?>/manage_slider/add_edit">&nbsp;Add Slider &nbsp;</a>
                                    <table id="datadisplay" class="table datatable">
                                        <thead>
                                            <tr>
												<th>Title</th>
												<th>Description</th>
												<th>Image</th>
												<th>Sort Order</th>
                                                
												<th>Action</th>
                                            </tr>
                                        </thead>
                                 
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->


<script>

$(document).ready(function() {

    $('#datadisplay').DataTable( {
        "aProcessing": true,
        "aServerSide": true,
        "ajax": '<?php echo Configure::read('app_root_path'); ?>manage_slider/ajax_slider_list',
        "aoColumns": [
            { "data": "title" },
            { "data": "description" },
            { "data": "slider_image",
                 render: function( data, type, full, meta ) {
                        return "<img src=\"" + data + "\" height=\"30\"/>";
                    }

             },
            { "data": "sort_order" },
            { "data": "actions" },
           
        ],
        

    } );
} );

</script>