<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_slider/save";
                 echo $this->Form->create('Slider', array('id'=>'addSlide', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Slide</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new Slide.</p>
                                </div>
                        <div class="panel-body">                                                                        
                          <div class="row">
                             <div class="col-md-8">
                                  <div class="form-group">
                                      <label class="col-md-3 control-label">Title</label>
                                       <div class="col-md-9">                                            
                                           <div class="input-group">
                                                <?php echo $this->Form->input('title',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'title','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                            </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                        </div>
                                    </div>
								 	<div class="form-group">
                                      <label class="col-md-3 control-label">Description</label>
                                       <div class="col-md-9">                                            
                                           <div class="input-group">
                                                <?php echo $this->Form->input('description',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'description', 'type'=>'textarea','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                            </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                        </div>
                                    </div>
								 <div class="form-group">
                                      <label class="col-md-3 control-label">Image</label>
                                       <div class="col-md-9">                                            
                                           <div class="input-group">
                                                <?php echo $this->Form->input('slide_image',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'slide_image', 'type'=>'file','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                            </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                        </div>
                                    </div>
                                            
                                   <div class="form-group">
                                      <label class="col-md-3 control-label">Sort</label>
                                       <div class="col-md-9">                                            
                                           <div class="input-group">
                                                <?php echo $this->Form->input('sort_order',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'sort_order', 'type'=>'number','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                            </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                        </div>
                                    </div>       
                                           
                                           
                                                                                 
                              </div> <!-- END OF FIRST ROW -->
                           
                        </div> <!-- END OF panel-body --> 
                          <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                            </div>
                     </div>
                 <?php echo $this->Form->end(); ?>
                            
            </div>
       </div>   
 </div>


<script>

$("#addSlide").validate({
                ignore: [],
                rules: {                                            
                        'data[Slider][title]': {
                                required: true,
                        },
                        'data[Slider][description]': {
                                required: true,
                        },
                        'data[Slider][slide_image]': {
                                required: true,
                        },
                        'data[Slider][sort_order]': {
                                required: true,
                                
                        },
                   
                        
         },
        
                
        
        
     });  
</script>
