
<?php echo $this->Html->script('plugins/bootstrap/bootstrap-datepicker.js'); ?>
<?php echo $this->Html->script('plugins/bootstrap/bootstrap-timepicker.min.js'); ?>
<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
           
          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><strong>Add</strong> Stay</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                    <div class="panel-body">
                        <p>Place where you can add new stay/Homestay/accomadation.</p>
                    </div>
                <div class="panel panel-default tabs"> 
                 <?php 
                        if(isset($this->params['pass'][1]) && $this->params['pass'][1]!="") 
                            $tab=$this->params['pass'][1];
                        else
                            $tab="listing";  
                    
                    ?>
                     <ul class="nav nav-tabs" role="tablist">
                       <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Listing Details</a></li> 
					<?php if(@$this->request->data['Homestay']['id']!=""){ ?>
                        <li><a href="#tab-second" role="tab" data-toggle="tab">Amenities</a></li>
                        <li><a href="#tab-third" role="tab" data-toggle="tab">Room Settings</a></li>
						<li><a href="#tab-fourth" role="tab" data-toggle="tab">Pricing Factors</a></li>
                        <li><a href="#tab-fifth" role="tab" data-toggle="tab">Availability</a></li>
					<?php } ?>
                    </ul>
                    <div class="panel-body tab-content">   
                        <div class="tab-pane active" id="tab-first">
                           <?php                         
                                 $action = Configure::read('app_root_path')."manage_homestays/save";
                                 echo $this->Form->create('Homestay', array('id'=>'listingDetail', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));  
                                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
                             echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'listing'));
                         ?>   
                                                     
                          <div class="row">
                                <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Property Type</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php  
                                                    echo $this->Form->input('property_type_id', array('options'=>$array_property_types,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'property_type_id', 'empty'=>'--Select--')); 

                                                    ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="col-md-3 control-label">Instant Book</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php 
													$array_instant_flag=array('Yes'=>'Yes','No'=>'No');
													
													echo $this->Form->input('instant_book',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'instant_book','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false, 'options'=>$array_instant_flag,'value'=>'Yes'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of the stay</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    
                                                        <?php echo $this->Form->input('name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'name','autofocus','data-rule-required'=>"true",'error'=>false));
														?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                    
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">SEO Unique URL</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                    
                                                        <?php 
                                                        if(@$this->request->data['Homestay']['seo_unique_url']==""){
                                                            $stay_name=trim(@$this->request->data['Homestay']['name']);
                                                            $stay_name=str_replace('-',' ',$stay_name);
                                                            $stay_name=str_replace('  ','-',$stay_name);
                                                            $stay_name=str_replace(',','-',$stay_name);
                                                            $stay_name=str_replace('&','-',$stay_name);
                                                            $seo_unique_url=str_replace(' ','-',$stay_name);
                                                        }else{
                                                             $seo_unique_url=$this->request->data['Homestay']['seo_unique_url'];
                                                        }
                                                        
                                                        echo $this->Form->input('seo_unique_url',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'seo_unique_url', 'autofocus','data-rule-required'=>"true",'error'=>false, 'value'=>$seo_unique_url));
                                                       
                                                        ?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">City</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php  
                                                    echo $this->Form->input('city_id', array('options'=>$array_cities,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'city_id', 'empty'=>'--Select--' , 'onchange'=>'javascript:findLocation(this.value)')); 

                                                    ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
									
											<div class="form-group">
                                                <label class="col-md-3 control-label">Location</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php   
                                                    echo $this->Form->input('location_id', array( 'options'=>$locations_arr,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'location_id', 'empty'=>'--Select--')); 

                                                    ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
									
									  		<div class="form-group">
                                                <label class="col-md-3 control-label">Rating</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('rating',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'rating','autofocus','data-rule-not-required'=>"true",'error'=>false,'min'=>1,'max'=>5));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">High Demand</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php 
													$array_high_demand_flag=array('Yes'=>'Yes','No'=>'No');
													
													echo $this->Form->input('high_demand',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'high_demand','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false, 'options'=>$array_high_demand_flag ));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('description',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'description','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">House Rules</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('house_rules',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'house_rules','autofocus','data-rule-not-required'=>"true",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
									
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Inclusions</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('inclusions',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'inclusions','autofocus','data-rule-not-required'=>"true",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cancellation Policy</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('cancellation_policy',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'cancellation_policy','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											 <div class="form-group">
                                                <label class="col-md-3 control-label">Activities Around</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('activities_around',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'activities_around', 'autofocus','data-rule-not-required'=>"true",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
									<fieldset>
										  <legend>Host Informations:</legend>
                                        	<div class="form-group">
                                                <label class="col-md-3 control-label">Host</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php  
                                                    echo $this->Form->input('host_id', array('options'=>$hosts_arr,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'host_id', 'empty'=>'--Select--')); 

                                                    ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Host Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('owner_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'owner_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Contact Person Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('contact_person_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'contact_person_name','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Mobile 1</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('mobile1',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'mobile1','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Mobile 2</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('mobile2',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'mobile2','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Telephone</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('telephone',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'telephone','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('email',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'email','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
									
											<div class="form-group">
                                                <label class="col-md-3 control-label">Coorgexpress Commission(%)</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('agent_commission',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'agent_commission', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
										
									</fieldset>
                                            

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Address</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('address',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'address','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Latitude</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('latitude',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'latitude','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Longitude</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('longitude',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'longitude','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Child  Charge</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('per_child_charge',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'per_child_charge', 'autofocus','data-rule-not-required'=>"true",'error'=>false,'min'=>0 ));?>
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">No of Bedrooms</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('no_of_bedrooms',array('div'=>false, 'label'=>false, 'class'=>'validate[required,custom[integer]] form-control', 'id'=>'no_of_bedrooms','autofocus','error'=>false));?>
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Max No of Guest</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('max_no_of_guest',array('div'=>false, 'label'=>false, 'class'=>'validate[required,custom[integer]]  form-control', 'id'=>'max_no_of_guest','autofocus','error'=>false));?>
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">No of Bathrooms</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('no_of_bathrooms',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'no_of_bathrooms','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">No of beds</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('no_of_beds',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'no_of_beds','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Check In Time</label>
                                                <div class="col-md-5">
                                                    <div class="input-group bootstrap-timepicker">
                                                     <?php echo $this->Form->input('check_in_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'check_in_time', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Check Out Time</label>
                                                <div class="col-md-5">
                                                    <div class="input-group bootstrap-timepicker">
                                                     <?php echo $this->Form->input('check_out_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'check_out_time', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                          <div class="form-group">
                                                <label class="col-md-3 control-label">Food Cusine</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('food_cuisine',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'food_cuisine','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                                </div>
                                            </div>    
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Other Details</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('other_details',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'other_details','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea'));?>
                                                </div>
                                            </div>  
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Cleaning Charges</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('cleaning_charges',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'cleaning_charges', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                </div>
                                            </div>  
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Title</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_title',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_title','autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
                                                </div>
                                            </div>  
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Keyword</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_keyword',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_keyword','autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
                                                </div>
                                            </div>  
											
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Description</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_description',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_description', 'autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
                                            
                                        </div>                                          
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                            <div class="row">
                                <div class="col-md-9" style="width:53%;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                          <h3 style="color:red;"><span class="fa fa-download"></span> Dropzone (Drop your property images here.)</h3>
                                            <p style="color:green;">Upload files one by one for better Performance. [Max file size:2MB, Supported Formats: JPEG, JPG, PNG GIF]</p>
                                            <div id="dZUpload" class="dropzone" style="min-height:150px;min-width:">
                                                <div class="dz-default dz-message"></div>
                                            </div>
                                        </div>
                                    </div>   
                                 </div> 
                                <div class="col-md-3"> 
									<ul style="list-style-type:none;">
								<?php
                                    if(count(@$homestayFiles)>0){
                                    
                                        foreach($homestayFiles as $files) { 
                                        //debug($files);
                                        ?>
                                        <li style="padding:3px; float:left; width:146px; height:108px; border:1px solid grey;  display: inline-block;;">
											<img src="<?php echo Configure::read('app_root_path').$files['HomestayFile']['file_path'].'/'.$files['HomestayFile']['file_name']; ?>" width="100" height="100"/>&nbsp;&nbsp; <a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/deleteFile/<?php echo $files['HomestayFile']['id']; ?>/<?php echo $files['HomestayFile']['homestay_id']; ?>" alt="Delete image"><i class="fa fa-times fa-3x"></i></a>
										</li>
                                    <?php }
                                        } ?>
                                 </ul>
                             </div>
                            </div><!-- END OF Second ROW -->
                         
                            <div class="panel-footer" >
<!--                                <button class="btn btn-default">Clear Form</button>                                    -->
                                <button class="btn btn-primary pull-right">Submit</button>
                           </div>
                            
                            <?php echo $this->Form->end(); ?>      
                        </div>  <!-- END OF First Tab Stay -->
                        
                        
                         <!-- START OF Second Tab Amentities  -->
                         <div class="tab-pane" id="tab-second">
                          <?php                         
                                 $action = Configure::read('app_root_path')."manage_homestays/saveAmenties";
                                 echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
                                echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'amenities'));
                             ?>
                           
                             <div class="col-md-12">
                                 
                                 <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" style="text-align:center; font-size: 16px;
    color: #1d908b;">What amenities do you offer? &nbsp;&nbsp;</label>                                    
                                 </div>
                                <div class="form-group">
                                 <?php   foreach($amenities_arr as $key=>$val){    ?>
                                   <div class="col-md-4">                                    
                                    <label class="check">
                                       <?php 
                                        $checkedFlag="";
                                        if(in_array($val['Amenity']['id'],$homestayAmenities_arr)) {
                                            $checkedFlag='checked';
                                        }                                  
                                        echo $this->Form->input("HomestayAmenity.amenity_id.[]", array(                    'type'=>'checkbox','name'=>'data[HomestayAmenity][amenity_id][]','value'=>$val['Amenity']['id'], 'checked'=>$checkedFlag, 'class'=>"icheckbox", 'label'=>false, 'id'=>'HomestayAmenity.amenity_id') ); 
                                        echo "<b class='amenity_name'>".$val['Amenity']['amenity']."</b>"; ?>       
                                    </label> 
                                            <?php  echo "<br/><span class='amenity_desc'>". $val['Amenity']['description']."</span>"; ?> 
                                   </div>
                                 
                                
                               <?php   } ?>
                                </div> 
                                 <br/><br/>
                            </div>
                             
                             
                           <div class="panel-footer" >
<!--                                <button class="btn btn-default">Clear Form</button>                                    -->
                                <button class="btn btn-primary pull-right">Submit</button>
                          </div>
                             
                             
                             <?php echo $this->Form->end(); ?>      
                         </div>
                         <!-- END OF Second Tab Amentities  -->
                        
                        
                        <!-- START OF Third Tab Pricing/Rooms -->
                        
                        <div class="tab-pane" id="tab-third">
                          <?php                         
                            $action = Configure::read('app_root_path')."manage_homestays/savePricingFactor";
                            echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                                          //  debug($this->request->data);
                            echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
                            echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'pricing'));
                             echo $this->Form->input('no_of_bedrooms',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'no_of_bedrooms','autofocus','error'=>false));
                          ?>
                           <?php 
                            $no_of_rooms=$this->request['data']['Homestay']['no_of_bedrooms'];
                            for($k=0; $k<$no_of_rooms;$k++){ ?>
                                   <div class="form-group">
                                     <label class="col-md-3 col-xs-12 control-label">
										<?php
											 if(@$this->request->data['RoomPriceDetail'][$k]['room_no_name']!=""){
												 @$room_no_name=$this->request->data['RoomPriceDetail'][$k]['room_no_name'];
											 }else{
											 	 $room_no_name= "BedRoom ".($k+1);
											 }
									   
									     echo $this->Form->input("RoomPriceDetail.$k.room_no_name",array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'RoomPriceDetail.room_no_name', 'autofocus','data-rule-not-required'=>"true",'error'=>false,'value'=>$room_no_name));?>
									   
									   </label>
                                        <div class="col-md-7 col-xs-12">                                                                 <?php  
                                                echo $this->Form->input("RoomPriceDetail.$k.id",array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>"RoomPriceDetail.$k.id",'autofocus','error'=>false));
                                                            
                                           // echo $this->form->input("MasterBedType.BedRoom_$k", array('label'=>false, 'type' => 'select', 'options' => $array_bed_types, 'multiple' =>'checkbox', 'id'=>'MasterBedType.id', 'data-rule-required'=>'true', 'style'=>'float:left;  display:inline;', 'class'=>"icheckbox", 'error'=>'false'));
                                                 //debug($array_bed_types);  
                                            $i=0;
                                            foreach($array_bed_types as $keyBed=>$bed){  
                                                     
                                                if(@in_array($keyBed,$this->request->data['MasterBedType']["BedRoom_$k"])){
                                                    //echo "ss".$keyBed;
                                                   $chekedFlag='checked="checked"';
                                                   @$no_of_bed=$this->request->data['MasterBedType']["BedCount_$k"][$keyBed];
                                                   $i++;
                                                   $no_of_bed_value="value='$no_of_bed'";
                                                }else{
                                                    $chekedFlag='';
                                                    $no_of_bed_value="";
                                                }
                                                '$no_of_bed'
;                                                 //if(in_array($keyBed,$this->request->data['MasterBedType']["BedCount_$k"]))
                                                
                                                    ?> 
                                            <div class="icheckbox">
                                            
                                            <input type="checkbox" name="data[MasterBedType][BedRoom_<?php echo $k;?>][]" id="MasterBedType.BedRoom_<?php echo $k;?>." data-rule-required="true" style="float: left; display: inline; position: absolute; opacity: 0;" <?php echo $chekedFlag; ?>  value="<?php echo $keyBed;?>">
                                            <input type="number" min="1" max="10" name="data[MasterBedCount][BedCounter_BedRoom_<?php echo $k;?>][<?php echo $keyBed;?>]" style="width:38px;" <?php echo $no_of_bed_value; ?> />
                                                    <label for="MasterBedType.id<?php echo $k;?>" class="selected hover"><?php echo $bed; ?></label>
                                                    
                                                  </div>

                                        <?php  }
                                         ?>
                                     
                                       </div>     
									   <div class="col-md-1 col-xs-12">    
										   <a title="Delete this room." href='<?php echo @Configure::read('app_root_path')."manage_homestays/deleteRoom/".$this->request['data']['Homestay']['id']."/".$this->request->data['RoomPriceDetail'][$k]['id']; ?>'> <button type="button" class="btn btn-danger">Delete Room</button> </a>
									   </div>
                                   </div>
                                   <div class="form-group" style="width:50%;float:left;">
                                     <label class="col-md-3 col-xs-12 control-label">Room Base Price</label>
                                       <div class="col-md-6 col-xs-12 ">                                                                   <?php echo $this->Form->input("RoomPriceDetail.$k.room_base_price",array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'RoomPriceDetail.room_base_price', 'autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                        </div>                                         
                                   </div>  
                                    <div class="form-group" style="width:50%;float:left;" >
                                     <label class="col-md-3 col-xs-12 control-label">Extra Bed Charge</label>
                                       <div class="col-md-6 col-xs-12 ">                                                                   <?php echo $this->Form->input("RoomPriceDetail.$k.extra_bed_charge",array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'RoomPriceDetail.extra_bed_charge', 'autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                        </div>                                         
                                   </div> 
                                 <div class="form-group" style="width:100%;float:left;">
                                     <label class="col-md-3 col-xs-12 control-label">Extra Bed Availablity &nbsp;&nbsp;</label>
                                       <div class="col-md-6 col-xs-12 "> 
                                        <?php echo $this->Form->input("RoomPriceDetail.$k.extra_bed", array(                    'type'=>'checkbox', 'label'=>false, 'id'=>'RoomPriceDetail.extra_bed'  ) ); ?>
                                           
                                           (Please check this flag if this room have an additional flag and select the bed type above.)
                                        </div>                                         
                                   </div>  
							<div class="form-group" style="width:50%;float:left;">
                                <label class="col-md-3 col-xs-6 control-label">Room Type &nbsp;&nbsp;</label>
                                       <div class="col-md-6 col-xs-6 "> 
                                        <?php echo $this->Form->input("RoomPriceDetail.$k.room_type_id", array('options'=>$array_room_types,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'RoomPriceDetail.room_type_id', 'empty'=>'--Select--')); 
										   	?>
                                        </div>                                         
                             </div> 
							
							<div class="form-group" style="width:50%;float:left;">
                                <label class="col-md-3 col-xs-6 control-label">Room Description &nbsp;&nbsp;</label>
                                       <div class="col-md-6 col-xs-6 "> 
                                        <?php echo $this->Form->input("RoomPriceDetail.$k.room_description",array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'RoomPriceDetail.room_description', 'autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));
										   	?>
                                        </div>                                         
                             </div> 
                          <div style="clear:both;"></div>
                            <hr style="font-weight:bold;"/>
                            <?php } ?>
                            
                             
                        
                        
                            <div class="panel-footer" >
<!--                                <button class="btn btn-default">Clear Form</button>                                    -->
                                <button class="btn btn-primary pull-right">Submit</button>
                           </div>
                             <?php echo $this->Form->end(); ?>            
                    </div> 						
						 <!-- END OF THird Tab rooms settings  -->
					<!-- START OF fourth Tab pricing factors  -->
					 <div class="tab-pane" id="tab-fourth">
							 <?php                         
                                 $action = Configure::read('app_root_path')."manage_homestays/savePricingStrategy";
                                 echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true )); 
							 	 echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
							 	echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'pricingfactors'));
						 		 echo $this->Form->input('PriceX.id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'price_x_id','autofocus','error'=>false));
						 		 if(count($this->request->data['PriceXSeasonal'])==0)
									 $countSeason=1;
						 		 else
						 		 	$countSeason=count($this->request->data['PriceXSeasonal']);
						 
						 		 if(count($this->request->data['PriceXHoliday'])==0)
									 $countHoliday=1;
						 		 else
								 	$countHoliday=count($this->request->data['PriceXHoliday']);
                             ?>
							 <div class="row">
							  
								<div class="form-group">
                                     <label class="col-md-4 control-label"  style="text-align:left;">Would you like to enable the variable pricing for this stay?</label>
                                       <div class="col-md-2">  
                                         <?php 
										   $array_smart_flag=array('Yes'=>'Yes','No'=>'No');
										   echo $this->Form->input('smart_price',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'smart_price','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false,'onclick'=>'manageSmartPricing(this.value);', 'options'=>$array_smart_flag ));
										   ?> 
                                        </div>
                                </div>
								 
								<section id="smart_price_section" style="<?php if(!empty($this->request->data['PriceX'])) echo ""; else echo "display:none;"; ?>">
								    <div class="form-group">
                                     <label class="col-md-4 control-label" style="text-align:left;">
										 Set Average Variable Price Percentage<span style="font-size:14px;">  (+ / -)</span></label>
                                       <div class="col-md-2 ">  
                                            <?php echo $this->Form->input('PriceX.avg_variable_price',array('div'=>false, 'label'=>false, 'class'=>'form-control ', 'id'=>'avg_variable_price','autofocus', 'error'=>false,'type'=>'number'));?> 
                                        </div>
                                     </div>
									<div class="row">
										<div class="col-md-6" > 
											<div class="form-group">
											<label class="col-md-12 control-label" style="color:red; text-align:center;"><h4>Set Season and Off Season
												<?php 
										   $array_smart_flag=array('Yes'=>'Enable','No'=>'Disable');
										   echo $this->Form->input('PriceX.seasonal_price',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'seasonal_price_setting','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false,'onclick'=>'manageSeasonalPricing(this.value);', 'options'=>$array_smart_flag ));
										   ?> 
												
												</h4>
											   (<small>Here you can set seasonal period you want to increment the price as well as decrement the price on off seasons period. Double check the prices and percentages before you submitting.</small>)
											   </label>
									 		</div>
										</div>
									    <div class="col-md-6" >
											<div class="form-group">
												<label class="col-md-12 control-label" style="color:red; text-align:center;"><h4>Set holidays(Local and Nationalized) <?php 
										   $array_smart_flag=array('Yes'=>'Enable','No'=>'Disable');
										   echo $this->Form->input('PriceX.holiday_price',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'holiday_price_setting','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false,'onclick'=>'manageHolidayPricing(this.value);', 'options'=>$array_smart_flag ));
										   ?> </h4> 
												   (<small>Here you can set holidays which you want to increment/ decrement the price. Seasonal price you have set for the same holiday, then no need to set again here.Seasonal pricing will be having more priority.</small>)
												   </label>
											 </div>
									    </div>
									</div>
									
								   <div class="row">	
									 <div class="col-md-6 " style="padding-left:0px !important;padding-right:0px !important;margin-right:10px !important; width:52%;"  > 
										 <div class="form-group" >
											<table id="seasonal_price_section"  style="<?php if(!empty($this->request->data['PriceXSeasonal'])) echo ""; else echo "display:none;"; ?>">
												<tr >
													<th>Start Date</th>
													<th>End Date</th>
													<th>Operator</th>
													<th>Percentage</th>
													<th>Action</th>
												</tr>
												<?php 
												$keySeason=0;												
												for($keySeason=0;$keySeason<$countSeason;$keySeason++){ ?>
												<tr id="seasonal_row">
													<td><?php echo $this->Form->input("PriceXSeasonal.$keySeason.season_start_date", array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'season_start_date','autofocus', 'error'=>false,'type'=>'text'));?> 
													</td>
													<td><?php echo $this->Form->input("PriceXSeasonal.$keySeason.season_end_date", array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'season_end_date', 'autofocus', 'error'=>false,'type'=>'text'));?> 
													</td>
													<td><?php $array_operators=array('+'=>'(+) Increment','-'=>'(-) Decerement');
													echo $this->Form->input("PriceXSeasonal.$keySeason.season_operator", array('options'=>$array_operators,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'season_operator', 'empty'=>'--Select--')); 
														
														?> 
													</td>
													<td><?php echo $this->Form->input ("PriceXSeasonal.$keySeason.season_price_percent", array('div'=>false, 'label'=>false, 'class'=>'form-control ', 'id'=>'season_price_percent','autofocus', 'error'=>false,'type'=>'number'));?> 
													</td>
													<td> <a href"" title="Add Row" onclick="repeatRow()">
														<span class="fa fa-plus fa-2x"></span></a>
														&nbsp;
														<a href"" title="Add Row" onclick="removeRow(this)">
														<span class="fa fa-minus fa-2x"></span></a>
														
													</td>
												</tr>
												<?php } ?>
											</table>
                                   		</div>
                               												 
								 	</div>
									<div class="col-md-6 "  style="padding-left::0px !important;padding-right:0px !important;margin-left:20px !important; width:44%;"> 									
                                	   <div class="form-group" >
											<table id="holiday_price_section" style="<?php if(!empty($this->request->data['PriceXHoliday'])) echo ""; else echo "display:none;"; ?>">
												<tr >
													<th>Start Date</th>
													<th>Operator</th>
													<th>Percentage</th>
													<th>Action</th>
												</tr>
												<?php 
												$keyHoliday=0;
												
												for($keyHoliday=0;$keyHoliday<$countHoliday;$keyHoliday++){ ?>
												<tr id="holiday_row">
													<td><?php echo $this->Form->input ("PriceXHoliday.$keyHoliday.holiday_start_date",  array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'season_start_date','autofocus', 'error'=>false,'type'=>'text'));?> 
													</td>													
													<td><?php $array_operators=array('+'=>'(+) Increment','-'=>'(-) Decerement');
													echo $this->Form->input("PriceXHoliday.$keyHoliday.holiday_operator", array('options'=>$array_operators,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'holiday_operator', 'empty'=>'--Select--')); 
														
														?> 
													</td>
													<td><?php echo $this->Form->input ("PriceXHoliday.$keyHoliday.holiday_price_percent", array('div'=>false, 'label'=>false, 'class'=>'form-control ', 'id'=>'holiday_price_percent','autofocus', 'error'=>false,'type'=>'number'));?> 
													</td>
													<td> <a href"" title="Add Row" onclick="repeatRowHoliday()">
														<span class="fa fa-plus fa-2x"></span></a>
													</td>
												</tr>
												<?php } ?>
											</table>
                                   		</div>
                               			
								 	</div>   
									   
							 	</div>
								<div class="row">
									<div class="col-md-12" > &nbsp; <br/> <br/></div>
								</div>
								<div class="row">
								   <div class="col-md-12" > 
									 <div class="form-group">
                                     <label class="col-md-5 control-label"  style="text-align:left;">Would you like to enable price hike on weekends.?</label>
                                       <div class="col-md-2">  
                                         <?php 
										   $array_smart_flag=array('Yes'=>'Yes','No'=>'No');
										   echo $this->Form->input('PriceX.weekend_price_hike',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'weekend_price_hike','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false, 'options'=>$array_smart_flag ));
										   ?> 
                                        </div>
									   </div>
									</div>									
							   </div>										
									
						    </section>							   	 
						</div>	
							<div class="panel-footer">
								<!--   <button class="btn btn-default">Clear Form</button>     -->
                                    <button class="btn btn-primary pull-right">Submit</button>
                             </div>
						<?php echo $this->Form->end(); ?>   
				 </div>
						
						<!-- END OF fourth Tab pricing factors  -->	
						 <!-- START OF fifth Tab Availability -->
                 <div class="tab-pane" id="tab-fifth">
                          <?php                         
                                 $action = Configure::read('app_root_path')."manage_homestays/saveAvailability";
                                 echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true,'onsubmit'=>'javascript: return saveAvailability()' ));   
                                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
                                echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'availability'));
							 
							 	echo $this->Form->input('bedroomChkFlag',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'bedroomChkFlag','autofocus','error'=>false,'value'=>0));
                             ?>
                            <div class="col-md-8">
                                <div class="form-group">
                                 <div class="content-frame-body padding-bottom-0" style="height:auto !important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="alert_holder"></div>
                                            <div class="calendar">                                
                                                <div id="calendar"></div>                            
                                            </div>
                                        </div>
                                    </div>
                                 </div>    
                               </div> 
                             </div>
                             <div class="col-md-4">
                                   <div class="form-group">
                                     <label class="col-md-3 col-xs-12 control-label">Check In Date</label>
                                       <div class="col-md-6 col-xs-12">  
                                            <?php echo $this->Form->input('OccupancyDetail.check_in_date',array('div'=>false, 'label'=>false, 'class'=>'form-control ', 'id'=>'dateBoxAvailability','autofocus', 'style'=>"font-weight:bold; font-size:16px; color:#7b214f;", 'data-rule-not-required'=>"true",'error'=>false,'type'=>'text','readonly'));?>
                                           
                                        </div>
                                     </div>
								 		
								 	  <div class="form-group">
                                     <label class="col-md-3 col-xs-12 control-label">Check Out Date</label>
                                       <div class="col-md-6 col-xs-12">  
                                            <?php echo $this->Form->input('OccupancyDetail.check_out_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'checkoutdate','autofocus', 'style'=>"font-weight:bold; font-size:16px; color:#7b214f;", 'data-rule-not-required'=>"true",'error'=>false,'type'=>'text'));?>
                                           
                                        </div>
                                     </div>
								 
                                    <div class="form-group">
                                       <label class="col-md-3 col-xs-12 control-label">Bed Rooms &nbsp;&nbsp;</label>
                                       <div class="col-md-6 col-xs-12" id="bedRoomsChkBox">                                            <?php    echo $this->form->input('OccupancyDetail.bedrooms', array('label'=>false,'type' => 'select', 'options' => $rooms, 'multiple' =>'checkbox', 'id'=>'OccupancyDetail.id', 'class'=>'bedroomsAvail','data-rule-required'=>'true', 'error'=>'false'));
                                               if(count($rooms)==0){
                                                        echo "(Please set the Pricing factors first..!!)";
                                                    }
                                              ?>
                                        </div>
                                    </div>
                                 
                                 <div class="form-group">
                                     
                                       <div class="col-md-6 col-xs-12"> 
                                            <button class="btn btn-info" onclick="javascript:saveAvailability()" >Save Non Availability</button>
                                        </div>
                                 </div>  
								 
								 
                              </div>
                             
                             
                             <?php echo $this->Form->end(); ?>      
                         </div>
                        <!-- END OF fifth Tab Availability -->
                      
                 </div>
               
              
         </div>
                            
      </div>
   </div>   
</div>

<style>
.radio, .checkbox {
    width: 110px !important; 
    }
    .fc-time{
        display: none !important;
    }
    .form-horizontal .radio, .form-horizontal .checkbox, .form-horizontal .radio-inline, .form-horizontal .checkbox-inline{
            padding-top: 2px !important;
    }
    .radio, .checkbox{
        width: 29px !important;
    }
    .amenity_desc{
        font-weight: 300 !important;
        cursor: pointer !important;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
        font-size: 15px !important;
        line-height: 18px !important;
        letter-spacing: 0.2px !important;
        padding-top: 4px !important;
        padding-bottom: 0px !important;
        color: #484848 !important;
        display: block !important;
        padding-left: 26px !important;
    }
    .amenity_name{
        font-weight: 300 !important;
        cursor: pointer !important;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
        font-size: 19px !important;
        line-height: 24px !important;
        letter-spacing: undefined !important;
        padding-top: 2px !important;
        padding-bottom: undefined !important;
        color: #484848 !important;
        display: inline-block !important;
        position: relative !important;
        top: -3px !important;
        vertical-align: top !important;
       
    }
    .icheckbox{
        float: left;
        padding: 3px;
        width: 190px !important;
    }

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}


</style>
    
    
<script>

var totat_bedrooms=""; var occupancyDetailJsonString="";
<?php if(@$occupancyDetailString!=""){ ?>
 occupancyDetailJsonString=<?php echo $occupancyDetailString; ?>   
<?php }else{ ?>
	occupancyDetailJsonString="";
<?php } ?>

<?php if(@$this->request->data['Homestay']['no_of_bedrooms']!=""){ ?>
totat_bedrooms=<?php echo $this->request->data['Homestay']['no_of_bedrooms']; ?>
<?php }else{ ?>
	totat_bedrooms=0;
<?php } ?>
	
$(document).ready(function() {

    $("input[name*='data[OccupancyDetail][bedrooms][]']").change(function() {
        if(this.checked) {
        }else{
			var returnVal = confirm("Are you sure it's available.?");
			//console.log(returnVal);
			if(returnVal==false){
				  $(this).prop("checked", true);
			}else{
				$('#bedroomChkFlag').val(1);
			}
				
		}
    });
	
	$('.datepicker').datepicker({
	  autoclose: true,
       format: 'yyyy-mm-dd' 
   });
});
function findLocation(city_id){
  //  $('#experience_slot_div').show();   
    $.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>manage_homestays/findLocation",
           data: 'city_id='+ city_id,
           type: "POST",
           success: function(response) {
              var locations = $.parseJSON(response);  
			   $('#location_id').empty().append('<option value="">--Select--</option>');
              $.each(locations, function(id, itemValue) { 
				 // console.log("itemValue  =="+itemValue);
                   $('#location_id').append('<option value="' + id + '">' + itemValue + '</option>');

               });
			   
			   $('#location_id').selectpicker('refresh');
            }
       });
}		
function repeatRow(){
	//alert('dfds');
	var rows = $('#seasonal_price_section tbody tr').length;
	var current=rows-1;
	 $("#seasonal_price_section").each(function () {
       
        var tds = '<tr><td><input name="data[PriceXSeasonal]['+current+'][season_start_date]" class="form-control datepicker" id="season_start_date_'+current+'" autofocus="autofocus" type="text"></td><td><input name="data[PriceXSeasonal]['+current+'][season_end_date]" class="form-control datepicker" id="season_end_date_'+current+'" autofocus="autofocus" type="text"> </td><td><select name="data[PriceXSeasonal]['+current+'][season_operator]" class="form-control select" id="season_operator_'+current+'" style="display: none;"><option value="">--Select--</option><option value="+">(+) Increment</option><option value="-" selected="selected">(-) Decerement</option></select></td><td><input name="data[PriceXSeasonal]['+current+'][season_price_percent]" class="form-control " id="season_price_percent_'+current+'" autofocus="autofocus" type="number"></td><td> <a href""="" title="Add Row" onclick="repeatRow()"><span class="fa fa-plus fa-2x"></span></a> &nbsp; <a href""="" title="Remove Row" onclick="removeRow(this)"><span class="fa fa-minus fa-2x"></span></a> </td></tr>';
        if ($('tbody', this).length > 0) {
            $('tbody', this).append(tds);
        } else {
            $(this).append(tds);
        }
		 
    });
		
	$('.datepicker').datepicker({
	  autoclose: true,
       format: 'yyyy-mm-dd' 
   });
	$('select').selectpicker();
}
function removeRow(currentRow){
		currentRow.closest('tr').remove();
	
}
function repeatRowHoliday(){
	var rows = $('#holiday_price_section tbody tr').length;
	var current=rows-1;
	 $("#holiday_price_section").each(function () {
       
        var tds = '<tr id="holiday_row"><td><input name="data[PriceXHoliday]['+current+'][holiday_start_date]" class="form-control datepicker" id="season_start_date_'+current+'" autofocus="autofocus" type="text"></td><td><select name="data[PriceXHoliday]['+current+'][holiday_operator]" class="form-control select" id="holiday_operator_'+current+'" style="display: none;"><option value="">--Select--</option><option value="+">(+) Increment</option><option value="-">(-) Decerement</option></select></td><td><input name="data[PriceXHoliday]['+current+'][holiday_price_percent]" class="form-control " id="holiday_price_percent_'+current+'" autofocus="autofocus" type="number"> </td><td> <a href""="" title="Add Row" onclick="repeatRowHoliday()"><span class="fa fa-plus fa-2x"></span></a>&nbsp; <a href""="" title="Remove Row" onclick="removeRowHoliday(this)"><span class="fa fa-minus fa-2x"></span></a></td></tr>';
        if ($('tbody', this).length > 0) {
            $('tbody', this).append(tds);
        } else {
            $(this).append(tds);
        }
		 
    });
	
	$('.datepicker').datepicker({
	   autoclose: true,
       format: 'yyyy-mm-dd' 
   });
	$('select').selectpicker();
}
function removeRowHoliday(currentRow){
		currentRow.closest('tr').remove();
	
}
function manageSmartPricing(value){
	//alert(value);
	if(value=="Yes")
		$('#smart_price_section').show();
	else
		$('#smart_price_section').hide();
}
function manageSeasonalPricing(value){
	//alert(value);
	if(value=="Yes")
		$('#seasonal_price_section').show();
	else
		$('#seasonal_price_section').hide();
}
function manageHolidayPricing(value){
	//alert(value);
	if(value=="Yes")
		$('#holiday_price_section').show();
	else
		$('#holiday_price_section').hide();
}
function saveAvailability(){
	var flag=true;
	if($('#dateBoxAvailability').val()==""){
		flag=false;
		$('#dateBoxAvailability').css("border" ,"2px solid red");
	}else{
		$('#dateBoxAvailability').css("border" ,"");
	}
	
	
	if ((($("input[name*='data[OccupancyDetail][bedrooms][]']:checked").length)<=0) && ($('#bedroomChkFlag').val()==0)) {
       flag=false;
		$('#bedRoomsChkBox').css("border" ,"2px solid red");
    }
	return flag
  
}
	
//  console.log('bed'+occupancyDetailJsonString);
 $(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();


  var calendar = $('#calendar').fullCalendar({
   editable: true,
   header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
   },


   events: "events.php",


   eventRender: function(event, element, view) {
    if (event.allDay === 'true') {
     event.allDay = true;
    } else {
     event.allDay = false;
    }
   },
   selectable: true,
   selectHelper: true,
   select: function(start, end, allDay) {
       var start_date = moment(start, 'DD.MM.YYYY').format('YYYY-MM-DD');
       var end_date = moment(start, 'DD.MM.YYYY').format('YYYY-MM-DD');
       var homestay_id = $('#homestay_id').val();
     
       var title = 'findAvailability';
	   var bedroomString="";
       $.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>manage_homestays/findAvailability",
           data: 'title='+ title+'&start_date='+ start_date +'&end_date='+ end_date +'&homestay_id='+ homestay_id,
           type: "POST",
           success: function(response) {
                $('#dateBoxAvailability').val(start_date); 
			  $('#bedroomChkFlag').val(0);
               var obj = JSON.parse(response);
			  // console.log(obj);
               $('.bedroomsAvail input[type="checkbox"]').each(function() {                 
                   if ($.inArray($(this).val(), obj) != -1)
                    {
                        $(this).prop('checked', true); 
						//bedroomString="<li>"+$(this).val()+ $("label[for='OccupancyDetail.id"+$(this).val()+"]")+"</li>";
						//$('#nonAvailableRooms').html(bedroomString);
                    }else{
                         $(this).prop('checked', false);         
                    }
                            
                });
               
           }
       });
   calendar.fullCalendar('unselect');
   },

      
  });
   
 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // TODO: check href of e.target to detect your tab
             //   $('#calendar').fullCalendar('render');
        });
     
//loading all the bookings/occupancies to calender. 
//Creating each booking as an event against the date.
     if(occupancyDetailJsonString!=""){         
        // console.log(occupancyDetailJsonString);
         $.each(occupancyDetailJsonString, function(key,value) {
			 //console.log(value);
			// console.log(value['OccupancyDetail']['check_in_date']);
			 	var check_in_date=value['OccupancyDetail']['check_in_date'];
			 	var check_out_date=value['OccupancyDetail']['check_out_date'];	
			 	var no_of_bookings=value['OccupancyDetail']['no_of_bookings'];	
                var freeRooms=totat_bedrooms-no_of_bookings;
                var titleMsg='Booked: '+no_of_bookings+'\n Free: '+freeRooms;
                start_date=check_in_date;
                end_date=check_out_date;
                allDay=false; //setting this to false to avoid displaying time.
                calendar.fullCalendar('renderEvent',
                   {
                       title: titleMsg,
                       start: start_date,
                       end: end_date,
                       allDay: false
                   },
                   true
                );
         }); 
     
     }
     
     
 });


$("#listingDetail").validate({
                ignore: [],
                rules: {                                            
                        'data[Homestay][property_type_id]': {
                                required: true,
                        },
                       // 'data[Homestay][instant_book]': {
                         //       required: true,
                        //},
                        'data[Homestay][name]': {
                                required: true,
                        },
                        'data[Homestay][seo_unique_url]': {
                                required: true,
                        },
                        'data[Homestay][city_id]': {
                                required: true,   
                        },
                        'data[Homestay][location_id]': {
                                required: true,
                        },
                        'data[Homestay][description]': {
                                required: true,
                        },
                        'data[Homestay][house_rules]': {
                                required: true,
                        },
                        'data[Homestay][inclusions]': {
                                required: true,
                        },
                        'data[Homestay][cancellation_policy]': {
                                required: true,
                        },
						'data[Homestay][activities_around]': {
                                required: true,
                        },
						'data[Homestay][per_child_charge]': {
                                required: true,
                        },
						'data[Homestay][no_of_bedrooms]': {
                                required: true,
                        },                     
                        
                  },
				errorPlacement: function(error, element) 
				{
					if ( element.is(":radio") ) 
					{
						error.appendTo( element.parents('.container') );
					}
					else 
					{ // This is the default behavior 
						error.insertAfter( element );
					}
				 }
                
        
        
     });    

</script>    
<?php if($tab=="availability") { ?>
    <script>
      $('.nav-tabs a[href="#tab-fifth"]').tab('show');
     </script>
 <?php } if($tab=="pricingfactors") { ?>
    <script>
      $('.nav-tabs a[href="#tab-fourth"]').tab('show');
     </script> 
 <?php }elseif($tab=="amenities") { ?>
    <script>
      $('.nav-tabs a[href="#tab-second"]').tab('show');
    </script>
<?php } elseif($tab=="pricing") { ?>
    <script>
      $('.nav-tabs a[href="#tab-third"]').tab('show');
    </script>
<?php }  elseif($tab=="listing") { ?>
    <script>
      $('.nav-tabs a[href="#tab-first"]').tab('show');
    </script>
<?php } ?>   
