 <!-- START DEFAULT DATATABLE -->
 <div class="panel panel-default">
        <div class="panel-heading">                                
                    <h3 class="panel-title">Property List</h3>
                        <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
            </div>
            <div class="panel-body">
                <div class="row">
                            <?php                         
                                 $action = Configure::read('app_root_path')."manage_homestays/property_list";
                                 echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));  
                         ?>   
                              <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="col-md-3 control-label">Property Name</label>
                                        <div class="col-md-9 col-xs-12" style="float:left; padding-left:10px;">    
                                            <?php echo $this->Form->input('property_name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'property_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                        </div>
                                   </div>
                              </div>
                            
                              <div class="col-md-6">
                                    <div class="form-group">
                                     
                                        <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                           <button class="btn btn-info" >Search</button>
                                          <a href="<?php echo Configure::read('app_root_path')."manage_homestays/property_list"; ?>" > <button class="btn btn-info" onclick="javascript:clearSearch();" >Reset</button></a>
                                        </div>
                                   </div>
                              </div>
                            
                            <?php echo $this->Form->end(); ?>  
                    <br/> <br/>
                </div>                       
                
                <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>StayName</th>
                                                <th>Owner</th>
                                                <th>Telephone</th>
                                                <th>Address</th>
                                                <th>Rooms</th>
                                                <th>Beds</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
									<?php 
      $this->Paginator->options(array(
			  'update'=>'#ajaxListing',
			  'before'=>'',
			  'complete'=>''
		));    
	 ?>
		<div id="ajaxListing">
                                   <?php foreach($homestays as $homestay){
   // debug($room);
                                                ?>

                                            <tr>	
                                                <td><?php echo $homestay['Homestay']['name']; ?></td>
                                                <td><?php echo $homestay['Homestay']['owner_name']; ?></td>
                                                <td><?php echo $homestay['Homestay']['telephone']; ?></td>
                                                <td><?php echo $homestay['Homestay']['address']; ?></td>
                                                <td><?php echo $homestay['Homestay']['no_of_bedrooms']; ?></td>
                                                <td><?php echo $homestay['Homestay']['no_of_beds']; ?></td>
                                                <td><a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/add/<?php echo $homestay['Homestay']['id']; ?>/listing">&nbsp;Edit &nbsp;</a>
												<?php if($homestay['Homestay']['status']==""){
														$status_code="Enable";
													}elseif($homestay['Homestay']['status']==1){
														$status_code="Disable";
													}elseif($homestay['Homestay']['status']==2){
														$status_code="Enable";
													}else{
														$status_code="Enable";
													}
												?>
                                                <a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/updateStatus/<?php echo $homestay['Homestay']['id']."/".$status_code; ?>">&nbsp; <?php echo $status_code; ?> &nbsp;</a>    
                                                </td>
                                            </tr>
                                <?php } ?>   
			
						</div>
					</tbody>				
						<tr>
							<td colspan="7">
										<?php echo $this->Js->writeBuffer(); ?>	
								
								  <nav class="pagination">
									<ul>

								<!--						<li><a href="#" class="current-page">1</a></li>-->
														<?php   echo $this->Paginator->prev(" < ", array(), null, array('class' => 'prev disabled')); ?>
														<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2,'separator' => '')); ?>
														<?php echo $this->Paginator->next(' > ', array(), null, array('class' => 'next disabled')); ?>
								<!--						<li><a href="#"><i class="sl sl-icon-arrow-right"></i></a></li>-->
									</ul>
								</nav>
						</td>			
				</tr>
											
                     
        </table>                                
	 </div>                            
</div>
                            <!-- END DEFAULT DATATABLE -->

<script>
function clearSearch(){
    $('#property_name').val('');
    return true;
    
}

</script>