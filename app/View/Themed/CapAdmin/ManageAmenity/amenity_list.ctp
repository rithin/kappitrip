 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">List Amenities</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                     <a href="<?php echo Configure::read('app_root_path'); ?>/manage_amenity/add_edit">&nbsp;List Amenity &nbsp;</a>
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>Amenity</th>
                                                <th>Amenity Cateogry</th>
                                                <th>Description</th>
                                                 <th>In Search</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php foreach($amenities as $details){
   // debug($room);
	
                                                ?>

                                            <tr>	
                                                <td><?php echo $details['Amenity']['amenity']; ?></td>
                                                 <td><?php echo $details['AmenityCategory']['name']; ?></td>
                                                 <td><?php echo $details['Amenity']['description']; ?></td>
                                                 <td><?php if($details['Amenity']['in_search']==0)
                                                                echo "No";
                                                            else
                                                                echo "Yes";
                                                     ?></td>
												
                                                <td>   <a href="<?php echo Configure::read('app_root_path'); ?>/manage_amenity/add_edit/<?php echo $details['Amenity']['id']; ?>">&nbsp;Edit &nbsp;</a>
                                                <a href="<?php  echo Configure::read('app_root_path'); ?>manage_amenity/delete/<?php  echo $details['Amenity']['id']; ?>">&nbsp;Delete &nbsp;</a>                                            </td>
                                            </tr>
                                <?php } ?>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->