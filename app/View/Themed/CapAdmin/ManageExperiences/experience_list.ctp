 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                      <div class="row">
                            <?php                         
                                 $action = Configure::read('app_root_path')."manage_experiences/experience_list";
                                 echo $this->Form->create('Experience', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));  
                         ?>   
                              <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="col-md-3 control-label">Property Name</label>
                                        <div class="col-md-9 col-xs-12" style="float:left; padding-left:10px;">    
                                            <?php echo $this->Form->input('experience_name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'experience_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                        </div>
                                   </div>
                              </div>
                            
                              <div class="col-md-6">
                                    <div class="form-group">
                                     
                                        <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                           <button class="btn btn-info" >Search</button>
                                          <a href="<?php echo Configure::read('app_root_path')."manage_experiences/property_list"; ?>" > <button class="btn btn-info" onclick="javascript:clearSearch();" >Reset</button></a>
                                        </div>
                                   </div>
                              </div>
                            
                            <?php echo $this->Form->end(); ?>  
                    <br/> <br/>
                </div>   
                                    
                                    
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>Experience Name</th>
                                                <th>Type</th>
                                                <th>Hosted By</th>
                                                <th>Hours</th>
                                                <th>Host Phone</th>
                                                <th>Host Email</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
													<?php 
      $this->Paginator->options(array(
			  'update'=>'#ajaxListing',
			  'before'=>'',
			  'complete'=>''
		));    
	 ?>
			<div id="ajaxListing">
                                   <?php foreach($experienceList as $item){
   										 $type='experience';
                                       ?>

                                            <tr>	
                                                <td><?php echo $item['Experience']['name']; ?></td>
                                                <td><?php echo $item['ExperienceType']['type']; ?></td>
                                                <td><?php echo $item['Experience']['hosted_by']; ?></td>
                                                <td><?php echo $item['Experience']['hours']; ?></td>
                                                <td><?php echo $item['Experience']['host_contact_no']; ?></td>
                                                <td><?php echo $item['Experience']['host_contact_email']; ?></td>
                                                <td><a href="<?php echo Configure::read('app_root_path'); ?>/manage_experiences/add/<?php echo $item['Experience']['id']."/?type=".$type; ?>">&nbsp;Edit &nbsp;</a>

													<?php if($item['Experience']['status']==""){
														$status_code="Enable";
													}elseif($item['Experience']['status']==1){
														$status_code="Disable";
													}elseif($item['Experience']['status']==2){
														$status_code="Enable";
													}else{
														$status_code="Enable";
													}
												?>
                                                <a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/updateStatus/<?php echo $item['Experience']['id']."/".$status_code; ?>">&nbsp; <?php echo $status_code; ?> &nbsp;</a>    
                                                </td>
                                            </tr>
                                <?php } ?>   
			
						</div>
						</tbody>					
							<tr>
							  <td colspan="7">
								<?php echo $this->Js->writeBuffer(); ?>	
								  		<nav class="pagination">
											<ul>
													<?php   echo $this->Paginator->prev(" < ", array(), null, array('class' => 'prev disabled')); ?>
														<?php echo $this->Paginator->numbers(array('first' => 2, 'last' => 2,'separator' => '')); ?>
														<?php echo $this->Paginator->next(' > ', array(), null, array('class' => 'next disabled')); ?>
											</ul>
										</nav>
										
										</td>		
											
								</tr>
             
        </table>
      </div>
</div>
                            <!-- END DEFAULT DATATABLE -->

<script>
function clearSearch(){
    $('#experience_name').val('');
    return true;
    
}

</script>