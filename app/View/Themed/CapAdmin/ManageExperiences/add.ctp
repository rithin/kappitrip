<?php echo $this->Html->script('plugins/bootstrap/bootstrap-datepicker.js'); ?>
<?php echo $this->Html->script('plugins/bootstrap/bootstrap-timepicker.min.js'); ?>
<?php
   @$type=$_REQUEST['type'];
?>
<div class="page-content-wrap">
<div class="row">
     <div class="col-md-12">
            
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Experiences</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new Experiences.</p>
                                </div>
                       <div class="panel panel-default tabs"> 
                         <?php 
						//debug($this->params['pass']);
						   		if(@$this->params['pass'][1]!="" && @$this->params['pass'][2]!="") 
									  @$tab=$this->params['pass'][2];
                                else if(isset($this->params['pass'][1]))
                                    $tab=$this->params['pass'][1];
                                else
                                    $tab="listing";  
						   
						   
                           ?>
                             <ul class="nav nav-tabs" role="tablist">
                               <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Experience Details</a></li>  
								 <li><a href="#tab-second" role="tab" data-toggle="tab">Availability</a></li>
							<?php if($type=="experience"){ ?> 
                                
							<?php } ?>
                            </ul>
                        <div class="panel-body  tab-content">                                                                        
                            <div class="tab-pane active" id="tab-first">   
                                 <?php                         
                                     $action = Configure::read('app_root_path')."manage_experiences/save";
                                     echo $this->Form->create('Experience', array('id'=>'addExperience', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                                              //  debug($this->request->data);
                                      echo $this->Form->input('id',array('div'=>false, 'typr'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
                                 ?>
								<input type="hidden" name="type" id="type" value="<?php echo $_REQUEST['type'];?>" />
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of the Experiences</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    
                                                        <?php echo $this->Form->input('name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label class="col-md-3 control-label">Instant Book</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php 
													$array_instant_flag=array('Yes'=>'Yes','No'=>'No');
													
													echo $this->Form->input('instant_book',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'instant_book','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false, 'options'=>$array_instant_flag ));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">SEO Unique URL</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                    
                                                        <?php 
                                                        if(@$this->request->data['Experience']['seo_unique_url']==""){
                                                            $stay_name=trim(@$this->request->data['Experience']['name']);
                                                            $stay_name=str_replace('-',' ',$stay_name);
                                                            $stay_name=str_replace('  ','-',$stay_name);
                                                            $stay_name=str_replace(',','-',$stay_name);
                                                            $stay_name=str_replace('&','-',$stay_name);
                                                            $seo_unique_url=str_replace(' ','-',$stay_name);
                                                        }else{
                                                             $seo_unique_url=$this->request->data['Experience']['seo_unique_url'];
                                                        }
                                                        
                                                        echo $this->Form->input('seo_unique_url',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'seo_unique_url','autofocus', 'data-rule-required'=>"true",'error'=>false , 'value'=>$seo_unique_url));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    
                                                        <?php echo $this->Form->input('title',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'title','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                            
                                           <div class="form-group">
                                                <label class="col-md-3 control-label">City</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                 <?php  
                                                    echo $this->Form->input('city_id', array('options'=>$array_cities,'div'=>false, 'label'=>false,  'class'=>'form-control select','type'=>'select','id' =>'experience_type','empty'=>'--Select--', 'onchange'=>'javascript:findLocation(this.value)' )); 

                                                  ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Location</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php  
                                                    echo $this->Form->input('location_id', array('options'=>$locations_arr,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'location_id', 'empty'=>'--Select--')); 
                                                    ?>
                                                </div>
                                            </div>
                                                                                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Experience Type</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php 
													if($type=="event"){
														$selected=5;
													}else{
														$selected="";
													}
                                                   //debug($array_experience_types);
                                                    echo $this->Form->input('experience_type', array('options'=>$array_experience_types,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select','default'=>$selected, 'id' =>'experience_type',
                                                        'empty'=>'--Select--')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Rating</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('rating',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'rating','autofocus','data-rule-not-required'=>"true",'error'=>false,'min'=>1,'max'=>5));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											
											
										 <?php 
											if($type=="event"){
 										 ?>
										
											<div class="form-group">
												<label class="col-md-3 control-label">Event Date</label>
												<div  class="col-md-9 col-xs-12">
													<?php echo $this->Form->input('event_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'event_date','autofocus', 'error'=>false));?>
												</div>
											</div>
											
										 <div class="form-group">
                                        	<label class="col-md-3 control-label">Event Time</label>
											 <div class="col-md-5">
                                            	<div class="input-group bootstrap-timepicker">
													<?php echo $this->Form->input('event_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'event_time','autofocus', 'error'=>false));?>
													<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
												</div>
											</div>
										 </div>
											<div class="form-group">
                                                <label class="col-md-3 control-label">Event Ticket Price (Rs)</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                 <?php echo $this->Form->input('event_price',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'event_price','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
										<?php } ?>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Trail Type</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php  
                                                    echo $this->Form->input('trail_type_id', array('options'=>$array_trail_types, 'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'trail_type_id', 'empty'=>'--Select--')); 

                                                    ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Duration</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                 <?php echo $this->Form->input('duration',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'duration','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'number'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Free Includes(like drinks,meal)</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('includes_in',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'includes_in','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Languages Offered</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">                                            
                                                   <?php                                                    
                                                    $list = $array_languages;
                                                        echo $this->form->input('ExperienceLanguage.language_id', array('label'=>false,'type' => 'select', 'options' => $list, 'multiple' =>'checkbox', 'id'=>'ExperienceLanguage.language_id','data-rule-required'=>'true', 'error'=>'false'));
                                                    ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											
											 <div class="form-group">
                                                <label class="col-md-3 control-label">About the host</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('about_host',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'about_host','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											
											 <div class="form-group">
                                                <label class="col-md-3 control-label">Things to Bring/Carry</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('things_to_bring',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'about_host','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea')); ?>
                                                </div>
                                            </div>
                                            
                                        <fieldset>
										  <legend>Host Informations:</legend>
                                        	<div class="form-group">
                                                <label class="col-md-3 control-label">Host</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php  
                                                    echo $this->Form->input('host_id', array('options'=>$hosts_arr,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'host_id', 'empty'=>'--Select--')); 
                                                    ?>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Host Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('hosted_by',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'hosted_by','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Contact Person Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('contact_person_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'contact_person_name','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Mobile 1</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('mobile1',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'mobile1','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Mobile 2</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('mobile2',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'mobile2','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Telephone</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('host_contact_no',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'host_contact_no', 'autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('host_contact_email',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'host_contact_email', 'autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
									
											<div class="form-group">
                                                <label class="col-md-3 control-label">Coorgexpress Commission(%)</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('agent_commission',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'agent_commission', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
										
									</fieldset>
                                           
                                        </div>
                                        <div class="col-md-6">
											
											
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">About the Experience</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('about_experience',array('div'=>false, 'label'=>false, 'class'=>'form-control','id'=>'about_experience','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Who can attend?</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                                           
                                                   <?php                                                    
                                                   
                                                        echo $this->form->input('ExperienceAttendee.attendee_type_id', array('label'=>false,'type' => 'select', 'options' => $who_can_attend, 'multiple' =>'checkbox', 'id'=>'ExperienceAttendee.attendee_type_id','data-rule-required'=>'true', 'error'=>'false'));
                                                    ?>
                                                </div>
                                            </div>
											
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Notes</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('notes',array('div'=>false, 'label'=>false, 'class'=>'form-control','id'=>'notes','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Where we can meet?</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('where_we_meet',array('div'=>false, 'label'=>false, 'class'=>'form-control','id'=>'where_we_meet','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>                                            
                                             
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Latitude</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('latitude',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'latitude','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Longitude</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('longitude',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'longitude','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                             
                                          <div class="form-group">
                                                <label class="col-md-3 control-label">Cancellation Policy</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('cancellation_policy',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'cancellation_policy','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>    
                                            
                                           <div class="form-group">
                                                <label class="col-md-3 control-label">Address</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('address',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'address','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Location</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('location',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'location','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Meta Title</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_title',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_title','autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Meta Keyword</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_keyword',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_keyword','autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Description</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_description',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_description', 'autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
											
                                                                                      
                                            
                                        </div>                                          
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                            <div class="row">
                                <div class="col-md-9" style="width:53%;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                          <h3 style="color:red;"><span class="fa fa-download"></span> Dropzone (Drop your property images here.)</h3>
                                            <p style="color:green;">Upload files one by one for better Performance. [Max file size:2MB, Supported Formats: JPEG, JPG, PNG GIF]</p>
                                            <div id="dZUpload" class="dropzone" style="min-height:150px;min-width:">
                                                <div class="dz-default dz-message"></div>
                                            </div>
                                        </div>
                                    </div>   
                                 </div> 
                                <?php if(count($experienceFiles)>0) { ?>
                                <div> <ul> <?php foreach($experienceFiles as $files) { 
                                   // debug($files);
                                    ?>
                                        <li><img src="<?php echo Configure::read('app_root_path').$files['ExperienceFile']['file_path'].'/'.$files['ExperienceFile']['file_name']; ?>" width="100" height="100"/>&nbsp;&nbsp; <a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/deleteFile/<?php echo $files['ExperienceFile']['id']; ?>/<?php echo $files['ExperienceFile']['experience_id']; ?>?type=<?php echo $_REQUEST['type'];?>" alt="Delete image"><i class="fa fa-times fa-3x"></i></a></li>
                                    <?php } ?>
                                 </ul>
                                </div>
                                <?php } ?>
                            </div><!-- END OF Second ROW -->
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                                <?php echo $this->Form->end(); ?>   
                        </div> 
                     <div class="tab-pane " id="tab-second">       
                       <div class="row">
                         <div class="col-md-6">
							 <div class="form-group">
								   <label class="col-xs-12 control-label" style="text-align: center;font-weight: bold; color: crimson;">Save Slot</label>
							   </div>

							 <?php                         
								  $action = Configure::read('app_root_path')."manage_experiences/saveAvailability";
								  echo $this->Form->create('ExperienceSlot', array('id'=>'addExperienceSlot', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ','onsubmit'=>'return validateSlotForm()',  'novalidate' => true,'onsubmit'=>'javascript: return saveSlot()'));  

								   echo $this->Form->input('experience_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'experience_id','autofocus','error'=>false,'value'=>@$this->data['Experience']['id']));

								  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false,'value'=>@$this->data['ExperienceSlot']['id']));

								   echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'availability'));
							 	
							 ?><input type="hidden" name="type" id="type" value="<?php echo $_REQUEST['type'];?>" />
                                  <div class="form-group">
                                          <label class="col-md-3 col-xs-12 control-label">Timings &nbsp;&nbsp;</label>
                                          <div class="col-md-6 col-xs-12">                                                                 From :
                                                  <div class="input-group bootstrap-timepicker">
                                                     <?php echo $this->Form->input('slot_start_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'slot_start_time', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                              To:
                                                <div class="input-group bootstrap-timepicker">
                                                     <?php echo $this->Form->input('slot_end_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'slot_end_time', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                          </div>
                                    </div>
                                 
                                    <div class="form-group">
                                       <label class="col-md-3 col-xs-12 control-label">Price</label>
                                       <div class="col-md-6 col-xs-12">                                            
                                         <?php echo $this->Form->input('price',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'slot_price','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                          <span class="help-block">Default textarea field</span>-->
                                        </div>
                                    </div>
                                     <div class="form-group">
                                       <label class="col-md-3 col-xs-12 control-label">Min no of Guest</label>
                                       <div class="col-md-6 col-xs-12">                                            
                                         <?php echo $this->Form->input('min_guest',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'min_guest','type'=>'number','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                          <span class="help-block">Default textarea field</span>-->
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                       <label class="col-md-3 col-xs-12 control-label">Max no of Guest</label>
                                       <div class="col-md-6 col-xs-12">                                            
                                         <?php echo $this->Form->input('no_of_people',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'no_of_people','type'=>'number','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                          <span class="help-block">Default textarea field</span>-->
                                        </div>
                                    </div>  
                                   
                                 
                                     <div class="form-group">

                                           <div class="col-md-6 col-xs-12" style="padding-left: 173px;padding-top: 20px;"> 
                                                <button class="btn btn-info" onclick="javascript:saveSlot()"  >Save Slot</button>
                                            </div>
                                    </div>   
							 			<br/>
						 	    <?php echo $this->Form->end(); ?>     
						    </div>
						   <div class="col-md-6">
							 <?php                         
								  $action = Configure::read('app_root_path')."manage_experiences/saveNonAvailability";
								  echo $this->Form->create('ExperienceBlocking', array('id'=>'addExperienceSlot', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true,'onsubmit'=>'javascript: return saveNonAvailability()'));  
							   
							   		echo $this->Form->input('experience_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'experience_id','autofocus','error'=>false,'value'=>@$this->data['Experience']['id']));
							   
							   		 echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false,'value'=>@$this->data['ExperienceBlocking']['id']));
							   ?>
							   <input type="hidden" name="type" id="type" value="<?php echo $_REQUEST['type'];?>" />
							   <div class="form-group">
								   <label class="col-xs-12 control-label" style="text-align: center;font-weight: bold; color: crimson;">Mark Non Availability</label>
							   </div>
							   <div class="form-group">
									<label class="col-md-3 col-xs-12 control-label">Start Date</label>
									<div class="col-md-6 col-xs-12">  
										<?php echo $this->Form->input('ExperienceBlocking.start_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'start_date','autofocus', 'style'=>"font-weight:bold; font-size:16px; color:#7b214f;", 'data-rule-not-required'=>"true",'error'=>false,'type'=>'text'));?>

									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 col-xs-12 control-label">End Date</label>
									<div class="col-md-6 col-xs-12">  
										<?php echo $this->Form->input('ExperienceBlocking.end_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'end_date','autofocus', 'style'=>"font-weight:bold; font-size:16px; color:#7b214f;", 'data-rule-not-required'=>"true",'error'=>false,'type'=>'text'));?>

									</div>
								 </div>
							   	 
							   <div class="form-group">
                                   <label class="col-md-3 control-label">Reason</label>
                                    <div class="col-md-9 col-xs-12">                                            
                                        <?php echo $this->Form->input('ExperienceBlocking.reason',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'reason','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                      </div>
                                 </div>

							   
							   	<div class="form-group">
									<div class="col-md-6 col-xs-12" style="padding-left: 173px;padding-top: 20px;"> 
										<button class="btn btn-info" onclick="javascript:saveNonAvailability()"   >Save Non Availability</button>
									</div>
								 </div>  
                             </div>							   
						   
						 </div> <!-- first row -->
                         <div class="row">
                            <div class="col-md-6">                                 
                                <div class="form-group" id="SlotDetails">
									<table>
										<tr>
												<th>Start Time</th>
												<th>End Time</th>
												<th>Min Guest</th>
												<th>Max Guest</th>
												<th>Price</th>
												<th>Action</th>
										  </tr>
													<?php foreach($ExperienceSlotsArr as $dtls){ ?>
													<tr>
														
														<td><?php echo $dtls['ExperienceSlot']['slot_start_time']; ?></td>
														<td><?php echo $dtls['ExperienceSlot']['slot_end_time']; ?></td>
														<td><?php echo $dtls['ExperienceSlot']['min_guest']; ?></td>
														<td><?php echo @$dtls['ExperienceSlot']['no_of_people']; ?></td>
														<td><i class="fa fa-fw fa-1x">&#xf156;</i><?php echo $dtls['ExperienceSlot']['price']; ?></td>
														<td> 
                                                            <a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/add/<?php echo $this->request->data['Experience']['id']."/".$dtls['ExperienceSlot']['id']."/availability/?type=".$type; ?>">&nbsp;Edit &nbsp;</a>
                                                            &nbsp;&nbsp;
                                                              <a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/deleteSlot/<?php echo $this->request->data['Experience']['id']."/".$dtls['ExperienceSlot']['id']."/availability/?type=".$type; ?>">&nbsp;Delete &nbsp;</a>  
                                                            
                                                        </td>
													</tr>

                                        
													<?php }?>


									  </table>
                                   </div>
                               </div>  
							 	
							 	<div class="col-md-6">                                 
                                <div class="form-group" id="SlotDetails">
									<table>
										<tr>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Reason</th>
												<th>Action</th>
										  </tr>
													<?php foreach($experienceBlockings as $dtls){ ?>
											<tr>
														
														<td><?php echo $dtls['ExperienceBlocking']['start_date']; ?></td>
														<td><?php echo $dtls['ExperienceBlocking']['end_date']; ?></td>
														<td><?php echo $dtls['ExperienceBlocking']['reason']; ?></td>
														<td> <a href="<?php echo Configure::read('app_root_path'); ?>/manage_experiences/add/<?php echo $this->request->data['Experience']['id']."/0/".$dtls['ExperienceBlocking']['id']."/availability/?type=".$type; ?>">&nbsp;Edit &nbsp;</a>                                                
                                                         &nbsp; &nbsp;
                                                        <a href="<?php echo Configure::read('app_root_path'); ?>/manage_experiences/deleteBlocking/<?php echo $this->request->data['Experience']['id']."/".$dtls['ExperienceBlocking']['id']."/availability/?type=".$type; ?>">&nbsp;Delete &nbsp;</a>
                                                            
                                                </td>
											</tr>

													<?php }?>


									  </table>
                                   </div>
                               </div> 
							 
							 
                           </div>
                     </div>
                       
                         
                         
                   
             </div> <!-- END OF panel-body --> 
          </div>   
        </div>               
    </div>
</div>   
</div>

<script type="text/javascript">
function findLocation(city_id){
  //  $('#experience_slot_div').show();   
    $.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>manage_homestays/findLocation",
           data: 'city_id='+ city_id,
           type: "POST",
           success: function(response) {
              var locations = $.parseJSON(response);  
			   $('#location_id').empty().append('<option value="">--Select--</option>');
              $.each(locations, function(id, itemValue) { 
				 // console.log("itemValue  =="+itemValue);
                   $('#location_id').append('<option value="' + id + '">' + itemValue + '</option>');

               });
			   
			   $('#location_id').selectpicker('refresh');
            }
       });
}	
function saveSlot(){
	 var flag=true;
	if($('#slot_price').val()==""){
		flag=false;
		$('#slot_price').css("border" ,"2px solid red");
	}else{
		$('#slot_price').css("border" ,"");
	}
	if($('#min_guest').val()==""){
		flag=false;
		$('#min_guest').css("border" ,"2px solid red");
	}else{
		$('#min_guest').css("border" ,"");
	}
	if($('#no_of_people').val()==""){
		flag=false;
		$('#no_of_people').css("border" ,"2px solid red");
	}else{
		$('#no_of_people').css("border" ,"");
	}
	
	return flag
}

function saveNonAvailability(){
	 var flag=true;
	if($('#start_date').val()==""){
		flag=false;
		$('#start_date').css("border" ,"2px solid red");
	}else{
		$('#start_date').css("border" ,"");
	}
	if($('#end_date').val()==""){
		flag=false;
		$('#end_date').css("border" ,"2px solid red");
	}else{
		$('#end_date').css("border" ,"");
	}
	
	
	return flag
}
function validateSlotForm(){
	var flag=true;
	var stringError=""
	if($('#slot_price').val()==""){
		flag=false;
		stringError+="Please enter a valid slot price. \n";
	}
	if($('#no_of_people').val()==""){
		flag=false;
		stringError+="Please enter max no of guest(s). \n";
	}
	if($('#min_guest').val()==""){
		flag=false;
		stringError+="Please enter min no of guest(s). \n";
	}
	
	if(stringError!=""){
		alert(stringError);
	}
 return flag;
}
	 
$('#timepicker_from').timepicker();
$('#timepicker_to').timepicker();

var ExperienceSlotJsonString=<?php echo $ExperienceSlotString; ?>  
 $(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();


  var calendar = $('#calendar').fullCalendar({
   editable: true,
   header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
   },


   events: "events.php",


   eventRender: function(event, element, view) {
    if (event.allDay === 'true') {
     event.allDay = true;
    } else {
     event.allDay = false;
    }
   },
   selectable: true,
   selectHelper: true,
   select: function(start, end, allDay) {
       var start_date = moment(start, 'DD.MM.YYYY').format('YYYY-MM-DD');
       var end_date = moment(end, 'DD.MM.YYYY').format('YYYY-MM-DD');
       var experience_id = $('#experience_id').val();
     
       var title = 'findAvailability';
       $.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>manage_experiences/findAvailability",
           data: 'title='+ title+'&start_date='+ start_date +'&end_date='+ end_date +'&experience_id='+ experience_id,
           type: "POST",
           success: function(response) {
                $('#dateBoxAvailabilityStart').val(start_date);        
			    $('#dateBoxAvailabilityEnd').val(end_date);        
                $('#SlotDetails').html(response);
               
           }
       });
   calendar.fullCalendar('unselect');
   },

      
  });
   
 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // TODO: check href of e.target to detect your tab
             //   $('#calendar').fullCalendar('render');
 });
     
//loading all the bookings/occupancies to calender. 
//Creating each booking as an event against the date.
     if(ExperienceSlotJsonString!=""){         
         //console.log(ExperienceSlotJsonString);
         $.each(ExperienceSlotJsonString, function(key,value) {
               var freeRooms=100-value;
                var titleMsg='Booked: '+value+'\n Free: '+freeRooms;
                start_date=key;
                end_date=key;
                allDay=false; //setting this to false to avoid displaying time.
                calendar.fullCalendar('renderEvent',
                   {
                       title: titleMsg,
                       start: start_date,
                       end: end_date,
                       allDay: false
                   },
                   true
                );
         }); 
     
     }
     
     
 });
    

</script>    

<?php if($tab=="availability") { ?>
    <script>
      $('.nav-tabs a[href="#tab-second"]').tab('show');
     </script>
    
 <?php }elseif($tab=="amenities") { ?>
    <script>
      $('.nav-tabs a[href="#tab-second"]').tab('show');
    </script>
<?php } ?>  


<style>

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}
</style>