<?php 



?>

            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="<?php echo Configure::read('app_root_path') ?>dashboard/view">
                       KAAPI TRIP</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?php echo Configure::read('app_root_path'); ?>/img/logo.jpg" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?php echo $user_name;?></div>
                                <div class="profile-data-title"><?php echo $user_role;?></div>
                            </div>
<!--
                            <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
-->
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="active">
                        <a href="<?php echo Configure::read('app_root_path'); ?>/dashboard/view"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>   
                    
                    <?php if($role_id==1 || $role_id==3 ){ ?>
                    
                    <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Properties</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/add"><span class="fa fa-edit"></span> Add Stay</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/property_list"><span class="fa fa-align-justify"></span> List Stay</a></li> 
                        </ul>
                    </li>
                    
                    
                    
                     <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Experiences</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/add?type=experience"><span class="fa fa-edit"></span>Add Experience</a></li>
							<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/add?type=event"><span class="fa fa-edit"></span>Add Event</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/experience_list"><span class="fa fa-align-justify"></span>List Experience</a></li> 
							<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/event_list"><span class="fa fa-align-justify"></span>List Event</a></li> 
                        </ul>
                    </li>
                            
                    <?php } ?>        
                    
                    <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Products</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_products/add"><span class="fa fa-edit"></span> Add Product</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_products/product_list"><span class="fa fa-align-justify"></span> List Products</a></li> 
                        </ul>
                    </li>
					
					 <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Bookings / Reports</span></a>
                        <ul>

                            <li><a href="<?php echo Configure::read('app_root_path'); ?>property_bookings/booking_list"><span class="fa fa-align-justify"></span> List Bookings</a></li> 
							 <li><a href="<?php echo Configure::read('app_root_path'); ?>reports/booking_list"><span class="fa fa-align-justify"></span> Reports</a></li> 
                        </ul>
                    </li>
				
					 <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Enquiries</span></a>
                        <ul>
							
							 <li><a href="<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/add_edit_custom_booking"><span class="fa fa-edit"></span> Raise Custom Quote</a></li> 
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/enquiry_list"><span class="fa fa-align-justify"></span> List Enquiries</a></li> 
                        </ul>
                    </li>
                    
                    <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Hosts</span></a>
                        <ul>
							
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_agent/add"><span class="fa fa-edit"></span>Add Host</a></li> 
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_agent/agent_list"><span class="fa fa-align-justify"></span> List Hosts</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Deal/Coupon</span></a>
                        <ul>					
							<li class="xn-openable">
								<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Coupons</span></a>
								<ul>
									<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_coupons/add"><span class="fa fa-edit"></span> Add Coupon</a></li>
									<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_coupons/coupon_list"><span class="fa fa-align-justify"></span> List Coupons</a></li> 
								</ul>
							</li>
							
							<li class="xn-openable">
								<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Deals</span></a>
								<ul>
									<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_deals/add"><span class="fa fa-edit"></span> Add Deal</a></li>
									<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_deals/deal_list"><span class="fa fa-align-justify"></span> List Deals</a></li> 
								</ul>
							</li>
						</ul>
					</li>
					
					<li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Blog</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_blog/add_edit"><span class="fa fa-edit"></span> Add Post</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_blog/post_list"><span class="fa fa-align-justify"></span> List Posts</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_coffee_shops/coffee_shop_list"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Coffee Shops</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_coffee_shops/add"><span class="fa fa-edit"></span> Add Coffee Shop</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_coffee_shops/coffee_shop_list"><span class="fa fa-align-justify"></span> List Coffee Shops</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Master Data</span></a>
                        <ul>

								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Property Type</span></a>
									<ul>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_property_type/add_edit"><span class="fa fa-edit"></span> Add Property Type</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_property_type/property_type_list"><span class="fa fa-align-justify"></span> List Property Type</a></li> 
									</ul>
								</li>
								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Room Type</span></a>
									<ul>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/add_edit"><span class="fa fa-edit"></span> Add Room Type</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/room_type_list"><span class="fa fa-align-justify"></span> List Room Type</a></li> 
									</ul>
								</li>
								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Bed Type</span></a>
									<ul>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_bed_type/add_edit"><span class="fa fa-edit"></span> Add Bed Type</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_bed_type/bed_type_list"><span class="fa fa-align-justify"></span> List Bed Type</a></li> 
									</ul>
								</li>
								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Experience Type</span></a>
									<ul>manage_bed_type
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experience_type/add_edit"><span class="fa fa-edit"></span> Add Experience Type</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experience_type/experience_type_list"><span class="fa fa-align-justify"></span> List Experience Type</a></li> 
									</ul>
								</li>

								
								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Aminities</span></a>
									<ul>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_amenity/add_edit"><span class="fa fa-edit"></span> Add Amenity</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_amenity/amenity_list"><span class="fa fa-align-justify"></span> List Amenities</a></li> 
									</ul>
								</li>
								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage City</span></a>
									<ul>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_city/add_edit"><span class="fa fa-edit"></span> Add City</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_city/city_list"><span class="fa fa-align-justify"></span> List City</a></li> 
									</ul>
								</li>
								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Location</span></a>
									<ul>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_location/add_edit"><span class="fa fa-edit"></span> Add Location</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_location/location_list"><span class="fa fa-align-justify"></span> List Location</a></li> 
									</ul>
								</li>
								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Category</span></a>
									<ul>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_category/add_edit"><span class="fa fa-edit"></span> Add Category</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_category/category_list"><span class="fa fa-align-justify"></span> List Category</a></li> 
									</ul>
								</li>
								<li class="xn-openable">
									<a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Sub Category</span></a>
									<ul>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_sub_category/add_edit"><span class="fa fa-edit"></span> Add Sub Category</a></li>
										<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_sub_category/sub_category_list"><span class="fa fa-align-justify"></span> List Sub Category</a></li> 
									</ul>
								</li>
								

						</ul>
					</li>

                      
					
					<li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Slider</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_slider/add_edit"><span class="fa fa-edit"></span> Add Slide</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_slider/slider_list"><span class="fa fa-align-justify"></span> List Slides</a></li> 
                        </ul>
                    </li>
					
					<li>
					<div style="min-height:150px;"></div>
					</li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>