
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="<?php echo Configure::read('app_root_path') ?>dashboard/view">
                       COORGEXPRESS</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?php echo Configure::read('app_root_path'); ?>/img/logo.jpg" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">Admin</div>
                                <div class="profile-data-title">Web Developer</div>
                            </div>
<!--
                            <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
-->
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="active">
                        <a href="<?php echo Configure::read('app_root_path'); ?>/dashboard/view"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>                    
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/add/listing"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Properties</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/add"><span class="fa fa-edit"></span> Add Stay</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/property_list"><span class="fa fa-align-justify"></span> List Stay</a></li> 
                        </ul>
                    </li>
                     <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/add"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Experiences</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/add?type=experience"><span class="fa fa-edit"></span>Add Experience</a></li>
							<li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/add?type=event"><span class="fa fa-edit"></span>Add Event</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experiences/experience_list"><span class="fa fa-align-justify"></span>List Experience</a></li> 
                        </ul>
                    </li>
                       <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_products/add"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Products</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_products/add"><span class="fa fa-edit"></span> Add Product</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_products/product_list"><span class="fa fa-align-justify"></span> List Products</a></li> 
                        </ul>
                    </li>
					
					 <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Bookings</span></a>
                        <ul>
<!--                            <li><a href="<?php echo Configure::read('app_root_path'); ?>property_bookings/add"><span class="fa fa-edit"></span> Add Stay</a></li>-->
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>property_bookings/booking_list"><span class="fa fa-align-justify"></span> List Bookings</a></li> 
                        </ul>
                    </li>
				
					 <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Enquiries</span></a>
                        <ul>
							
							 <li><a href="<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/add_edit_custom_booking"><span class="fa fa-edit"></span> Raise Custom Quote</a></li> 
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>enquiry_bookings/enquiry_list"><span class="fa fa-align-justify"></span> List Enquiries</a></li> 
                        </ul>
                    </li>
					
					 <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_coupons/add"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Coupons</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_coupons/add"><span class="fa fa-edit"></span> Add Coupon</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_coupons/coupon_list"><span class="fa fa-align-justify"></span> List Coupons</a></li> 
                        </ul>
                    </li>
					<li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_blog/add_edit"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Blog</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_blog/add_edit"><span class="fa fa-edit"></span> Add Post</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_blog/post_list"><span class="fa fa-align-justify"></span> List Posts</a></li> 
                        </ul>
                    </li>
					<li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_property_type/add_edit"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Property Type</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_property_type/add_edit"><span class="fa fa-edit"></span> Add Property Type</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_property_type/property_type_list"><span class="fa fa-align-justify"></span> List Property Type</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/add_edit"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Room Type</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/add_edit"><span class="fa fa-edit"></span> Add Room Type</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/room_type_list"><span class="fa fa-align-justify"></span> List Room Type</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_amenity/add_edit"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Aminities</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_amenity/add_edit"><span class="fa fa-edit"></span> Add Amenity</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_amenity/amenity_list"><span class="fa fa-align-justify"></span> List Amenities</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/add_edit"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Bed Type</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/add_edit"><span class="fa fa-edit"></span> Add Bed Type</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/room_type_list"><span class="fa fa-align-justify"></span> List Bed Type</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_city/add_edit"><span class="fa fa-files-o"></span> <span class="xn-text">Manage City</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_city/add_edit"><span class="fa fa-edit"></span> Add City</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_city/city_list"><span class="fa fa-align-justify"></span> List City</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_location/add_edit/"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Location</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_location/add_edit"><span class="fa fa-edit"></span> Add Location</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_location/location_list"><span class="fa fa-align-justify"></span> List Location</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_category/add_edit/"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Category</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_category/add_edit"><span class="fa fa-edit"></span> Add Category</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_category/category_list"><span class="fa fa-align-justify"></span> List Category</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_sub_category/add_edit/"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Sub Category</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_sub_category/add_edit"><span class="fa fa-edit"></span> Add Sub Category</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_sub_category/sub_category_list"><span class="fa fa-align-justify"></span> List Sub Category</a></li> 
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_room_type/add_edit"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Experience Type</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experience_type/add_edit"><span class="fa fa-edit"></span> Add Experience Type</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_experience_type/experience_type_list"><span class="fa fa-align-justify"></span> List Experience Type</a></li> 
                        </ul>
                    </li>
					
					<li class="xn-openable">
                        <a href="<?php echo Configure::read('app_root_path'); ?>manage_slider/add_edit"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Slider</span></a>
                        <ul>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_slider/add_edit"><span class="fa fa-edit"></span> Add Slide</a></li>
                            <li><a href="<?php echo Configure::read('app_root_path'); ?>manage_slider/slider_list"><span class="fa fa-align-justify"></span> List Slides</a></li> 
                        </ul>
                    </li>
					
					<br/> <br/>
					
<!--

                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Layouts</span></a>
                        <ul>
                            <li><a href="layout-boxed.html">Boxed</a></li>
                            <li><a href="layout-nav-toggled.html">Navigation Toggled</a></li>
                            <li><a href="layout-nav-top.html">Navigation Top</a></li>
                            <li><a href="layout-nav-right.html">Navigation Right</a></li>
                            <li><a href="layout-nav-top-fixed.html">Top Navigation Fixed</a></li>                            
                            <li><a href="layout-nav-custom.html">Custom Navigation</a></li>
                            <li><a href="layout-frame-left.html">Frame Left Column</a></li>
                            <li><a href="layout-frame-right.html">Frame Right Column</a></li>
                            <li><a href="layout-search-left.html">Search Left Side</a></li>
                            <li><a href="blank.html">Blank Page</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Components</li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">UI Kits</span></a>                        
                        <ul>
                            <li><a href="ui-widgets.html"><span class="fa fa-heart"></span> Widgets</a></li>                            
                            <li><a href="ui-elements.html"><span class="fa fa-cogs"></span> Elements</a></li>
                            <li><a href="ui-buttons.html"><span class="fa fa-square-o"></span> Buttons</a></li>                            
                            <li><a href="ui-panels.html"><span class="fa fa-pencil-square-o"></span> Panels</a></li>
                            <li><a href="ui-icons.html"><span class="fa fa-magic"></span> Icons</a><div class="informer informer-warning">+679</div></li>
                            <li><a href="ui-typography.html"><span class="fa fa-pencil"></span> Typography</a></li>
                            <li><a href="ui-portlet.html"><span class="fa fa-th"></span> Portlet</a></li>
                            <li><a href="ui-sliders.html"><span class="fa fa-arrows-h"></span> Sliders</a></li>
                            <li><a href="ui-alerts-popups.html"><span class="fa fa-warning"></span> Alerts & Popups</a></li>                            
                            <li><a href="ui-lists.html"><span class="fa fa-list-ul"></span> Lists</a></li>
                            <li><a href="ui-tour.html"><span class="fa fa-random"></span> Tour</a></li>
                        </ul>
                    </li>                    
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-pencil"></span> <span class="xn-text">Forms</span></a>
                        <ul>
                            <li>
                                <a href="form-layouts-two-column.html"><span class="fa fa-tasks"></span> Form Layouts</a>
                                <div class="informer informer-danger">New</div>
                                <ul>
                                    <li><a href="form-layouts-one-column.html"><span class="fa fa-align-justify"></span> One Column</a></li>
                                    <li><a href="form-layouts-two-column.html"><span class="fa fa-th-large"></span> Two Column</a></li>
                                    <li><a href="form-layouts-tabbed.html"><span class="fa fa-table"></span> Tabbed</a></li>
                                    <li><a href="form-layouts-separated.html"><span class="fa fa-th-list"></span> Separated Rows</a></li>
                                </ul> 
                            </li>
                            <li><a href="form-elements.html"><span class="fa fa-file-text-o"></span> Elements</a></li>
                            <li><a href="form-validation.html"><span class="fa fa-list-alt"></span> Validation</a></li>
                            <li><a href="form-wizards.html"><span class="fa fa-arrow-right"></span> Wizards</a></li>
                            <li><a href="form-editors.html"><span class="fa fa-text-width"></span> WYSIWYG Editors</a></li>
                            <li><a href="form-file-handling.html"><span class="fa fa-floppy-o"></span> File Handling</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="tables.html"><span class="fa fa-table"></span> <span class="xn-text">Tables</span></a>
                        <ul>                            
                            <li><a href="table-basic.html"><span class="fa fa-align-justify"></span> Basic</a></li>
                            <li><a href="table-datatables.html"><span class="fa fa-sort-alpha-desc"></span> Data Tables</a></li>
                            <li><a href="table-export.html"><span class="fa fa-download"></span> Export Tables</a></li>                            
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-bar-chart-o"></span> <span class="xn-text">Charts</span></a>
                        <ul>
                            <li><a href="charts-morris.html"><span class="xn-text">Morris</span></a></li>
                            <li><a href="charts-nvd3.html"><span class="xn-text">NVD3</span></a></li>
                            <li><a href="charts-rickshaw.html"><span class="xn-text">Rickshaw</span></a></li>
                            <li><a href="charts-other.html"><span class="xn-text">Other</span></a></li>
                        </ul>
                    </li>                    
                    <li>
                        <a href="maps.html"><span class="fa fa-map-marker"></span> <span class="xn-text">Maps</span></a>
                    </li>                    
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-sitemap"></span> <span class="xn-text">Navigation Levels</span></a>
                        <ul>                            
                            <li class="xn-openable">
                                <a href="#">Second Level</a>
                                <ul>
                                    <li class="xn-openable">
                                        <a href="#">Third Level</a>
                                        <ul>
                                            <li class="xn-openable">
                                                <a href="#">Fourth Level</a>
                                                <ul>
                                                    <li><a href="#">Fifth Level</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>                            
                        </ul>
                    </li>
-->
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>