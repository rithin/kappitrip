 <div class="login-container">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo"><h4 style="float:left;padding-top: 0px;font-size: 36px; color:white;">KAAPI<span style="color:#c7a17a">TRIP</span></h4></div>
                <div class="login-body">
                    <div class="login-title"><strong>Welcome</strong>, Please login</div>
                    <?php 
                   //debug($this->Session);die;
                    $flash_message=$this->Session->flash('login_bad');
                    if($flash_message!=""){ ?>
                    <div class="alert alert-danger" role="alert">
                     <button type="button" class="close" data-dismiss="alert">
                         <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <strong>Oh snap!</strong> <?php echo $flash_message; ?>
                    </div>
             <?php
                          }
                 $action = Configure::read('app_root_path')."users/login";
                 echo $this->Form->create('User', array('id'=>'loginForm', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal', 'novalidate' => true));                                
                              
			 ?>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?php echo $this->Form->input('username',array('div'=>false, 'label'=>false, 'class'=>'form-control required email has-error', 'id'=>'username','autofocus','data-rule-required'=>"true",'data-msg-required'=>"Please enter your username.", 'error'=>false));?>
<!--                            <input type="text" name="username" class="form-control" placeholder="Username"  autocomplete="off"/>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?php echo $this->Form->input('password',array('div'=>false, 'label'=>false, 'class'=>'form-control required has-error', 'id'=>'password','autofocus','data-rule-required'=>"true",'data-msg-required'=>"Please enter your password.", 'error'=>false));?>
<!--                           <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off"/>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Log In</button>
                        </div>
                    </div>
                   <?php echo $this->Form->end(); ?>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2014 - <?php echo date("Y"); ?> kaapitrip.com
                    </div>
<!--
                    <div class="pull-right">
                        <a href="#">About</a> |
                        <a href="#">Privacy</a> |
                        <a href="#">Contact Us</a>
                    </div>
-->
                </div>
            </div>
            
        </div>