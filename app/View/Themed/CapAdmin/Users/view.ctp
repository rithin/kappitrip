  <div class="container">
            <!-- Breadcrumbs line -->
            <div class="crumbs">
                <ul id="breadcrumbs" class="breadcrumb">
<!--                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo Configure::read('nuwed_root_path') ?>pages/home">Dashboard</a>
                    </li>-->
                    <li class="current">
                        Users Management
                    </li>
                </ul>

                
            </div>
            <!-- /Breadcrumbs line -->

            <!--=== Page Header ===-->
            <div class="page-header">
                <div class="page-title">
                    <h3>Users</h3>
<!--                    <span>Good morning, John!</span>-->
                </div>

                <!-- Page Stats -->
                <!--<ul class="page-stats">
                    <li>
                        <div class="summary">
                            <span>New orders</span>
                            <h3>17,561</h3>
                        </div>
                        <div id="sparkline-bar" class="graph sparkline hidden-xs">20,15,8,50,20,40,20,30,20,15,30,20,25,20</div>
                        <!-- Use instead of sparkline e.g. this:
                        <div class="graph circular-chart" data-percent="73">73%</div>
                      
                    </li>
                    <li>
                        <div class="summary">
                            <span>My balance</span>
                            <h3>$21,561.21</h3>
                        </div>
                        <div id="sparkline-bar2" class="graph sparkline hidden-xs">20,15,8,50,20,40,20,30,20,15,30,20,25,20</div>
                    </li>
                </ul>-->
                <!-- /Page Stats -->
            </div>
            <!-- /Page Header -->

       <div class="row">
                <div class="col-md-12">
                    <div class="widget box">
                        <div class="widget-header">
                            <h4><i class="icon-reorder"></i>Manage Users (<code>Admin Staffs</code>)</h4>
                            <div class="toolbar no-padding">
                                <div class="btn-group">
                                    <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content no-padding">
                        <div class="tabbable box-tabs">
                                <ul class="nav nav-tabs" style="top:-37px; margin-right:25px;">
                                    <!--<li><a href="#box_tab3" data-toggle="tab">Section 3</a></li>
                                     <?php //if($active_flag=="add_edit") echo "class='active'"; else ''; ?>
                                    <?php // if($active_flag=="list") echo "class='active'"; else ''; ?>
                                    -->
                                    
                                    
                                    <li  <?php if($active_flag=="add_edit") echo "class='active'"; else " "; ?>><a href="#box_tab2" data-toggle="tab">Add/Edit User </a></li>
                                    <li <?php  if($active_flag=="list") echo "class='active'"; else " "; ?>><a href="#box_tab1" data-toggle="tab">List User</a></li>
                                </ul>
                               <div class="tab-content"> 
                               <div class="tab-pane <?php  if($active_flag=="list") echo "active"; else ' '; ?>" id="box_tab1" style="overflow-x: scroll">
                                  
                                   
                                   <table class="table table-striped table-bordered table-hover table-checkable  datatable" >
                                    <thead>
                                        <tr>
                                            <!--<th class="checkbox-column">
                                                <input type="checkbox" class="uniform">
                                            </th>-->
                                            <th class="checkbox-column"></th>
                                            <th>Full Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Department</th>
                                            <!--<th>Status</th>-->
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                 <?php 
                                 $slno=1;
                                    foreach($restult_users as $results){
                                 ?>
                                    <tr>
                                        
                                        <td><?php echo $slno; ?></td>
                                        <!--<td class="checkbox-column">
                                    <?php echo $this->Form->input('',array('type'=>'checkbox', 'value'=>$results['User']['id'] )); ?>
                                        </td>-->
                                        <td><?php echo $results['User']['first_name']." ".$results['User']['last_name'];?></td>
                                        <td><?php echo $results['User']['username'];?></td>
                                        <td><?php echo $results['User']['email_id'];?></td>
                                        <td><?php echo $results['Department']['department_name'];?></td>
                                        <!--<td><?php 
                                                $user_id_new=$results['User']['id'];
                                                 echo $this->Form->input('status_update_id', array('options'=>$restult_status,
                                                        'selected'=>@$results['User']['status_id'],'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'status_update_id',
                                                        'empty'=>'--Select--','onChange'=>'updateStatus('.$user_id_new.',this);'));?>

                                        </td>-->
                                        <td><?php if($results['User']['status_id']=='1'){ ?><a href="javascript:chnagePassword(<?php echo $results['User']['id']; ?>);" title="Change Password" ><i class="icon-eye-open"></i></a><?php } ?>
                                        &nbsp; &nbsp;<a href="../users/view/<?php echo $results['User']['id']; ?>" title="Edit" ><i class="icon-edit"></i></a>
                                        </td>
                                    </tr>
                                <?php $slno++; } ?>
                                </tbody>
                            </table>
                            </div>
                            <div class="tab-pane <?php if($active_flag=="add_edit") echo "active"; else " "; ?>" id="box_tab2">
                                
                              <?php			 
                              $action = Configure::read('nuwed_root_path')."users/save";
                              echo $this->Form->create('User', array('id'=>'addEditUserForm', 'url'=>$action, 'type'=>'file','class'=>'form-horizontal row-border', 'novalidate' => true));
                              echo $this->Form->input('User.id',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'id', 'value'=>$user_id));
			      echo $this->Form->input('role_id',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'role_id', 'value'=>2));
                              echo $this->Form->input('User.status_id',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'status_id', 'value'=>1));
		              ?>
                                  <div class="form-group">
                                      
                                      <div class="col-md-12">
                                      <div class="row">
                                              <div class="col-md-6">
                                               <?php echo $this->Form->input('first_name',array('div'=>false, 'label'=>false, 'class'=>'form-control required', 'id'=>'last_name', 'error'=>false));?>
                                                  <span class="help-block">First Name <span class="mandatory">*</span></span>
                                              </div>

                                              <div class="col-md-6">
                                               <?php echo $this->Form->input('last_name',array('div'=>false, 'label'=>false, 'class'=>'form-control required', 'id'=>'last_name', 'error'=>false));?>
                                                  <span class="help-block">Last Name <span class="mandatory">*</span></span>
                                              </div>

                                              <div class="col-md-6">
                                              
                                          <?php

                                          echo $this->Form->input('department_id', array('options'=>$restult_departments,
                                                        'selected'=>@$my_shop,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control required', 'type'=>'select', 'id' =>'shop_id',
                                                        'empty'=>'--Select--'));
                                          ?>
                                              <span class="help-block">Select Department <span class="mandatory">*</span></span>
                                          </div>
                                      
                                          
                                          <div class="col-md-6">
                                               <?php echo $this->Form->input('email_id',array('type'=> 'text' , 'div'=>false, 'label'=>false, 'class'=>'form-control email', 'id'=>'email_id', 'error'=>false));?>
                                               <span class="help-block">Email</span>
                                              </div>
                                          
                                          
                                              
                                          </div>
                                               
                                              <div class="col-md-6">
                                               <?php echo $this->Form->input('username',array('div'=>false, 'label'=>false, 'class'=>'form-control required', 'id'=>'username', 'error'=>false));?>
                                               <span class="help-block">Username <span class="mandatory">*</span></span>
                                              </div>

                                              <?php if(empty($user_id)){ ?>

                                              <div class="col-md-6">
                                               <?php echo $this->Form->password('password',array('div'=>false, 'label'=>false, 'class'=>'form-control required', 'id'=>'password', 'error'=>false));?>
                                               <span class="help-block">Password <span class="mandatory">*</span></span>
                                              </div>
                                          
                                               <?php } ?>
                                          
                                      </div>
                                  </div>

                                  
                                  <div class="form-group">
                                      
                                      <div class="col-md-12">
                                      <div class="row">
                                              
                                             <div class="col-md-12"> 
                                              <p class="btn-toolbar btn-toolbar-demo" >
                                             <input type="submit" value="Save" id="save" class="btn btn-success"> 
                                             &nbsp;&nbsp;
                                             <a id="cancel" class="btn btn-danger" href="<?php echo Configure::read('nuwed_root_path'); ?>users/view">Cancel</a>
                                              </p>
                                             </div>
                                              
                                          </div>
                                      </div>
                                  </div>
                               </form>
                            </div>
                              
                       </div> 
                    </div> <!-- /.tabbable portlet-tabs -->
                </div>
            </div>
        </div>
    </div>
</div>             

<script language="javascript">
//javascript call for change password.
function chnagePassword(user_id){
    jQuery.ajax({
	  type: 'POST',
	  url: "<?php echo Configure::read('nuwed_root_path'); ?>/users/change_password/"+user_id,
	  success: function(data) {
		 bootbox.dialog({
		  message: data,
		  title: "Change Password",
		  buttons: {
			  success: {
				  label: "Save!",
				  className: "btn-success",
				  callback: function() {
					if ($("#changePassword").valid()) {
						if(savePasswordData()==true)
							return true;
						else
							return true;	
					}
					return false;
				  }
			  },
			  danger: {
				  label: "Cancel",
				  className: "btn-danger",
				  callback: function() {
					  console.log("uh oh, look out!");
				  }
			  },
		  }
	  }); 

	 }
   });
}
function savePasswordData(){
        var password = $('#password_change_pass').val();
	var id = $('#id_change_pass').val();
    var confirmSend;
    var jsonval= {password:password,id:id};
	/* JSON.stringify({
            email: $("#email_change_pass").val(),
            password: $("#password_change_pass").val(),
            id: $("#id_change_pass").val()
        })*/
	//console.log(JSON.stringify(json_data))
	//JSON.stringify(json_data)
   jQuery.ajax({
      type: "POST",
      url: "../users/savePasswordData",
      data:jsonval,
      //contentType: "application/json; charset=utf-8",
      dataType: "json",
      cache: false,
      success: function(data) {
		  console.log(data);
        if (data == 1) {
          bootbox.alert("Successfully changed the password..!!");
		 	return true;
        } else {
          bootbox.alert("DATABASE OPERATION FAILED: Try after sometime.");
		  return false;
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        bootbox.alert("OPERATION FAILED: Try after sometime.");
		return false;
      }
    });
	return false
}


function updateStatus(user_id,data){
	
	//console.log(data.value);
	  var statusId=data.value;
	  var jsonval= {user_id:user_id,statusId:statusId};
	  jQuery.ajax({
      type: "POST",
      url: "../users/updatePassword",
      data:jsonval,
      //contentType: "application/json; charset=utf-8",
      dataType: "json",
      cache: false,
      success: function(data) {
		 // console.log(data);
        if (data == 1) {
          bootbox.alert("Successfully updated.",function(){
              window.location.reload(true);
          });
        } else {
          bootbox.alert("DATABASE OPERATION FAILED: Try after sometime.");
		  return false;
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        bootbox.alert("OPERATION FAILED: Try after sometime.");
		return false;
      }
    });
	return false;
}
</script>