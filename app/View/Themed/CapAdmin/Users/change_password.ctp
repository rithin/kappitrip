<div class="row">
   <div class="col-md-12">
		<?php
            $action = Configure::read('nuwed_root_path')."users/change_password";
            echo $this->Form->create('User', array('id'=>'changePassword','onsubmit'=>'javascript:return false;', 'url'=>'#','type'=>'file','class'=>'form-horizontal row-border', 'novalidate' => true));
            echo $this->Form->input('id_change_pass',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'id'=>'id_change_pass', 'value'=>$user_id));

       ?>
       
       <div class="form-group">
           <label class="col-md-4 control-label" for="name">Password</label>
           <div class="col-md-4">
           <?php echo $this->Form->input('password_change_pass',array('div'=>false, 'label'=>false, 'class'=>'form-control required has-error', 'id'=>'password_change_pass','type'=>'password', 'error'=>false ,'rangelength'=>"5, 10"));?>
                     <span class="help-block">(5 to 10 characters)</span>
          </div>
       </div>
       <div class="form-group">
           <label class="col-md-4 control-label" for="name">Retype Password</label>
           <div class="col-md-4">
            <?php echo $this->Form->input('retype_password_change_pass',array('div'=>false, 'label'=>false, 'class'=>'form-control required  has-error','type'=>'password', 'id'=>'retype_password_change_pass','equalTo'=>'#password_change_pass','error'=>false,'rangelength'=>"5, 10"));?>
                 <span class="help-block">(5 to 10 characters)</span>
          </div>
       </div>

       </form>
   </div>
</div>