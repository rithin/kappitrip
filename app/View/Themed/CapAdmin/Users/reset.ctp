<div class="box">
<span class="red-mark text-center"> <?php //echo $this->Session->flash(); ?></span>
		<div class="content">
                <!-- Login Formular -->
	        
                <!-- Title -->
                <h3 class="form-title">Reset Password</h3>
                
                
                    <div class="reset-password-done hide-default has-success text-center">
                            <i class="icon-ok success-icon icon-2x has-success help-block"></i> <!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
                            <span class="has-success help-block">Your password is reset successfully. Please wait as we redirect to login page.</span>
                    </div>

                    <!-- Error Message -->
                    <div class="reset-password-error has-error hide-default text-center">
                        <i class="icon-remove danger-icon icon-2x has-error help-block"></i> <!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
                        <span class="has-error help-block"></span>
                    </div>
                    
                    <?php
                $action = Configure::read('nuwed_root_path')."users/reset_password";
                echo $this->Form->create('UserReset', array('id'=>'resetForm', 'url'=>$action, 'type'=>'file','class'=>'form-vertical row-border', 'novalidate' => true)); 
		?>
                                <?php
                                if(!empty($restult_status))
                                {
                                if($restult_status['UserReset']['status_id']==1)
                                {
                                    ?>
				<div class="form-group">
					<!--<label for="username">Username:</label>-->
					<div class="input-icon">
                                        <i class="icon-lock"></i>
                                         <?php echo $this->Form->input('password',array('div'=>false, 'placeholder'=>'New Password','data-rule-required='=>'true', 'data-msg-required'=>'Please enter the new password.', 'label'=>false,'type'=>'password', 'class'=>'form-control required has-error', 'id'=>'password','autofocus','data-rule-required'=>"true",'data-msg-required'=>"Please enter new password.", 'error'=>false));?>

					</div>
				</div>
				<div class="form-group">
					<!--<label for="password">Password:</label>-->
					<div class="input-icon">
						<i class="icon-lock"></i>
 				<?php echo $this->Form->input('confirm_password',array('type'=>'password','data-rule-required='=>'true','data-msg-required'=>'Please confirm the password.','placeholder'=>'Confirm New Password','div'=>false, 'label'=>false, 'class'=>'form-control required has-error', 'id'=>'confirm_password','autofocus','data-rule-required'=>"true",'data-msg-required'=>"Please confirm your password.", 'error'=>false));?>
					
                                        
                                <?php echo $this->Form->input('token',array('div'=>false, 'label'=>false,'type'=>'hidden','value'=>$restult_status['UserReset']['token']));?>
                                <?php echo $this->Form->input('userid',array('div'=>false, 'label'=>false,'type'=>'hidden','value'=>$restult_status['User']['id']));?>
                                <?php echo $this->Form->input('user_reset_id',array('div'=>false, 'label'=>false,'type'=>'hidden','value'=>$restult_status['UserReset']['id']));?> 
                                        
                                        
                                        
                                        </div>
				</div>
				<!-- /Input Fields -->

				<!-- Form Actions -->
				<div class="form-actions">
					
					<button type="submit" class="submit btn btn-primary pull-right">
						Submit <i class="icon-angle-right"></i>
					</button>
				</div>
			</form>
			<!-- /Login Formular -->
                                <?php } else { ?>
                        <div class="has-error text-center">
					<i class="icon-remove danger-icon icon-2x has-error help-block text-center"></i> <!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
					<span class="has-error help-block">Invalid link. Please proceed with the forgot password process again.</span>
				</div>
                        
                        
                        <div class="form-actions">
					
					<a href="<?php echo Configure::read('nuwed_root_path').'users/index'; ?>" class="submit btn btn-primary pull-right">
						Back to login <i class="icon-angle-right"></i>
					</a>
				</div>
                         
                        
                                <?php } } else { ?>
                        
                        <div class="has-error text-center">
					<i class="icon-remove danger-icon icon-2x has-error help-block text-center"></i> <!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
					<span class="has-error help-block">Invalid link. Please proceed with the forgot password process again.</span>
				</div>
                        
                        
                        <div class="form-actions">
					
					<a href="<?php echo Configure::read('nuwed_root_path').'users/index'; ?>" class="submit btn btn-primary pull-right">
						Back to login <i class="icon-angle-right"></i>
					</a>
				</div>
                                <?php } ?>
			
			<!-- /Register Formular -->
		</div> <!-- /.content -->
	</div>

<script>
$('form').submit(function(e){
    e.preventDefault();
    
    var action=$('#resetForm').attr('action');
    var formData = $("#resetForm").serializeArray();
    jQuery.ajax({
        type: "POST",
        url: action,
        data : formData,
        //contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
        // console.log(data);
            if (data == 1) {
                

                $('form').hide();
                $('.reset-password-error').hide();
                $('.reset-password-done').slideDown(350);

                setTimeout(function(){ window.location='<?php echo Configure::read('nuwed_root_path')."users/index"; ?>'; },5000);
                return true;
                
            } else if(data.password) {
                
                $('.reset-password-error').show();
                $('.reset-password-error span').html(data.password);
                $('#password').focus();
                return false;
            }
            else if(data.confirm_password) {
                
                $('.reset-password-error').show();
                $('.reset-password-error span').html(data.confirm_password);
                $('#confirm_password').focus();
                return false;
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {

            alert("OPERATION FAILED: Try after sometime.");
            return false;
          }
    });
});  
</script>