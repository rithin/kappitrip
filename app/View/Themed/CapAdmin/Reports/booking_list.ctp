<?php echo $this->Html->script('plugins/bootstrap/bootstrap-datepicker.js'); ?>

<?php echo $this->Html->script('plugins/bootstrap/bootstrap-timepicker.min.js'); ?>
<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" /><!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />


                            
<!-- START DEFAULT DATATABLE -->
<div class="panel panel-default">
    <h3 class="panel-title">Search</h3>
     <div class="panel-body" id="exportTable" >
     <?php                         
                 $action = Configure::read('app_root_path')."reports/booking_list";
                 echo $this->Form->create('Report', array('id'=>'addHost', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
		 ?>
                                    <div class="row" >
                                        <div class="col-md-2">
                                            <div class="list-group border-bottom">
                                                <h5 class="panel-title">Hosts:</h5>
                                              <?php  
                                                 echo $this->Form->input('host_id', array('options'=>$hosts_arr,'div'=>false, 'label'=>false,  'class'=>'form-control select', 'type'=>'select', 'id' =>'host_id', 'empty'=>'--Select--')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="list-group border-bottom">
                                                 <h5 class="panel-title">Homestay:</h5>
                                               <?php  
                                                 echo $this->Form->input('homestay_id', array('options'=>$homestay_arr,'div'=>false, 'label'=>false,  'class'=>'form-control select', 'type'=>'select', 'id' =>'homestay_id', 'empty'=>'--Select--','onchange'=>'managefilter("homestay")'));  ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="list-group border-bottom">
                                                <h5 class="panel-title">Experience:</h5>
                                                <?php  
                                                 echo $this->Form->input('experience_id', array('options'=>$experience_arr,'div'=>false, 'label'=>false,  'class'=>'form-control select', 'type'=>'select', 'id' =>'experience_id', 'empty'=>'--Select--' ,'onchange'=>'managefilter("experience")'));  ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="list-group border-bottom">
                                                <h5 class="panel-title">Start Date:</h5>
                                              <?php echo $this->Form->input('start_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'start_date','style'=>"font-weight:bold; font-size:16px; color:#7b214f;", 'data-rule-not-required'=>"true",'error'=>false,'type'=>'text'));?>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="list-group border-bottom">
                                                <h5 class="panel-title">End Date:</h5>
                                              <?php echo $this->Form->input('end_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'end_date','style'=>"font-weight:bold; font-size:16px; color:#7b214f;", 'data-rule-not-required'=>"true",'error'=>false,'type'=>'text'));?>
                                            </div>
                                        </div>
                                    </div>
                            <div class="row">
                               <div class="col-md-3">
                                    <div class="list-group border-bottom">
                                       <h5 class="panel-title">Type:</h5>
                                           <?php 
												$booking_type=array('homestay'=>'HomeStay','experience'=>'Experience');
                                                if(@$sltd_val=="") $sltd_val='homestay';
												echo $this->Form->input('book_type',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'book_type','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false, 'options'=>$booking_type ));?>
                                    </div>
                                 </div>
                            </div>
         
         
                            <div class="row" >
                                <div class="col-md-12">
                                <div class=" pull-right" style="margin-top:10px;">
                                                                  
                                    <button class="btn btn-primary pull-right">Search</button>
                                </div>
                                </div>
                            </div>
           
                     <?php echo $this->Form->end(); ?>
           
           </div>
       			  <div class="panel-heading">
                                    <h3 class="panel-title">DataTable Export</h3>
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='../images/icons/csv.png' width="24"/> CSV</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='../images/icons/xls.png' width="24"/> XLS</a></li>
                                            <li class="divider"></li>
<!--                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img src='../images/icons/pdf.png' width="24"/> PDF</a></li>-->
                                        </ul>
                                    </div>   
				</div>
       
                <div class="panel-heading">                                
                   <h3 class="panel-title">Data Listing</h3>
                    <ul class="panel-controls">
                      <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                         <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                         <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                     </ul>                                
                  </div>
                  <div class="panel-body">
                         <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
												<th>Invoice</th>
                                                <th>Customer Name</th>                                                
                                                <th>Homestay/ Experience</th>                                                
<!--                                                <th>Host</th>-->
												<th>Check-in</th>        
												<th>Check-out</th>        
												<th>Invoice Amount</th>
												<th>Comm: (%)</th>
												<th>Comm: Amount</th>
                                                <th>Total To Pay</th>
                                                <th>Paid</th>
												<th>Balance</th>
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
		
		<div id="ajaxListing">
                            <?php 
								$invoice_amount_total=0; $commission_total_amount=0; $amount_total_to_pay=0; $total_remitted_amount=0;$total_outstanding_balance=0;
							foreach($bookingDetails as $details){
									$agent_commission=0; $flag_type=""; 
									$booking_type_id=0; //basically homestay_id or experience_id
									$remitted_amount=0; $remit_btton_flag=false;
									if($details['BookingPaymentResponse']['homestay_id']!=""){
									  $agent_commission=$details['Homestay']['agent_commission'];
									  $flag_type="homestay";
									  $booking_type_id=$details['Homestay']['id'];
									}
									else if($details['BookingPaymentResponse']['experience_id']!=""){
									    $agent_commission=$details['Experience']['agent_commission'];
										$flag_type="experience";
										$booking_type_id=$details['Experience']['id'];
									}
	
									 if($agent_commission==0){
										$agent_commission= Configure::read('agent_commission_percentage');
									 }
								     if(count($details['InvoiceRemittance'])>0){
										 foreach($details['InvoiceRemittance'] as $remittances){
											$remitted_amount+=$remittances['remitted_amount'];
										 }
									 }							
									 $invoice_amount=$details['BookingPaymentResponse']['amount'];
									 $amount_for_coorgexpress=(($invoice_amount*$agent_commission)/100);	
									 $amount_to_host=($invoice_amount-$amount_for_coorgexpress);
									 $total_remitted_amount+=$remitted_amount;
									 $invoice_amount_total+=$invoice_amount;
									 $commission_total_amount+=$amount_for_coorgexpress;
									 $amount_total_to_pay+=$amount_to_host;
									 $balanceAmountToPay=($amount_to_host-$remitted_amount);
									 if($balanceAmountToPay>$remitted_amount){
										 $remit_btton_flag=true;
									 }
									 $total_outstanding_balance+=$balanceAmountToPay;
                           ?>

                               <tr style="<?php if($details['BookingOrderDetail']['status']==8 || $details['BookingOrderDetail']['status']==13 || $details['BookingOrderDetail']['status']==7) echo "color:red;"; ?>">	
                                                <td><?php echo $details['BookingPaymentResponse']['order_id']; ?></td>
                                                <td><?php echo $details['BookingPaymentResponse']['billing_name']; ?></td>
                                                <?php if($details['BookingPaymentResponse']['homestay_id']!="") { ?>
                                                   <td><?php echo $details['Homestay']['name']; ?></td>
<!--                                                   <td><?php //echo $details['Homestay']['owner_name']; ?></td>-->
                                                <?php }else if($details['BookingPaymentResponse']['experience_id']!="") {?>
                                                    <td><?php echo $details['Experience']['name']; ?></td>
<!--                                                    <td><?php //echo $details['Experience']['hosted_by']; ?></td>-->
                                                <?php } ?>
												
												<td><?php echo $details['OccupancyDetail']['check_in_date']; ?></td>
												<td><?php echo $details['OccupancyDetail']['check_out_date']; ?></td>
												
                                                <td><?php echo $details['BookingPaymentResponse']['amount']; ?></td>
												<td><?php echo $agent_commission; ?></td>	
												<td><?php echo $amount_for_coorgexpress; ?></td>
												<td><?php echo number_format($amount_to_host,2); ?></td>
								   				<td><?php echo number_format($remitted_amount,2); ?></td>
								   				<td><?php echo number_format($balanceAmountToPay,2); ?></td>
								   <td>
									   <?php if($remit_btton_flag==true){ ?>
							   
									   <button type="button" class="btn btn-info" onclick="javascript:remitNow(<?php echo $details['BookingPaymentResponse']['id']; ?>,'<?php echo $flag_type;?>',<?php echo $booking_type_id;?>)">Remit</button>
									   <?php } ?>
								   
								   </td>
                                               
                                </tr>
                                <?php } ?>   
			</div>
											
			 </tbody>
				        <tr>
								   <td colspan="1"></td>
                                   <td colspan="2" style="min-width: 150px;">Total Invoice Amount</td>                                                
								   <td colspan="1"></td>
								   <td colspan="2" style="min-width: 150px;">Total Commission Amount</td>
								   <td colspan="2" style="min-width: 150px;">Total Amount to Pay</td>
								   <td colspan="2" style="min-width: 150px;">Total Remitted Amount</td>
								   <td colspan="2" style="min-width: 150px;">Outstanding Balance</td>
                               </tr>
                             
								<tr style="font-weight:bold; font-size:large;">
									<td colspan="1"> &nbsp;</td>
									<td colspan="2" style="min-width: 150px;"> <?php echo number_format($invoice_amount_total,2); ?></td>
									<td colspan="1"></td>
									<td colspan="2" style="color:green; min-width: 150px;"><?php echo number_format($commission_total_amount,2); ?></td>
									<td colspan="2" style="color:red; min-width: 150px;"><?php echo number_format($amount_total_to_pay,2); ?></td>
									<td colspan="2" style="color:green; min-width: 150px;"><?php echo number_format($total_remitted_amount,2); ?></td>
									<td colspan="2" style="color:red; min-width: 150px;">
										<?php echo number_format($total_outstanding_balance,2); ?></td>
								 </tr>	
						
            </table>
     </div>
</div>
                            <!-- END DEFAULT DATATABLE -->

<?php echo $this->Html->script('plugins/tableexport/tableExport.js'); ?>
<?php echo $this->Html->script('plugins/tableexport/jquery.base64.js'); ?>
<?php echo $this->Html->script('plugins/tableexport/html2canvas.js'); ?>
<?php echo $this->Html->script('plugins/tableexport/jspdf/libs/sprintf.js'); ?>
<?php echo $this->Html->script('plugins/tableexport/jspdf/jspdf.js'); ?>
<?php echo $this->Html->script('plugins/tableexport/jspdf/libs/base64.js'); ?>


<script>

function managefilter(filterType){
    //console.log(filterType); experience
    if(filterType=='homestay'){
        $('#experience_id').prop('selectedIndex',0);
        $('#experience_id').prop('readonly', true);
        $("#book_typeHomestay").prop("checked", true);  
     }
    if(filterType=='experience'){
        $('#homestay_id').prop('selectedIndex',0);
        $('#homestay_id').prop('readonly', true);
        $("#book_typeExperience").prop("checked", true);
     }
}
	
function remitNow(payment_id,flag_type,booking_type_id){
	event.preventDefault();
    this.blur(); // Manually remove focus from clicked link.
 	
	$.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>reports/add_remittance",
           data: 'payment_id='+ payment_id+'&flag_type='+ flag_type +'&booking_type_id='+ booking_type_id,
           type: "POST",
           success: function(response) {
			   $('#ajaxRemittance').html(response);
               $('.remittance_form').modal();               
           }
       });
}

</script>


<style>
.row {
    margin: 10px;
    padding-bottom: 10px;
    border: 1px solid !important;
}
</style>
<div class="remittance_form" style="display:none;">
  <div id="ajaxRemittance"></div>
</div>