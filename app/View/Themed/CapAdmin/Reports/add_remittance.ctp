<div class="row">
	<div class="col-md-12">
		<div class="form-group">
	 		<p class="col-xs-12 control-label" style="text-align: center;font-weight: bold; color: blue; text-transform:uppercase; padding:4px; ">Remittance of invoice - <?php echo $remittance_dtls['order_id']; ?></p>	
		</div>
		<p style="color:red" id="errorMsg"></p>
		<?php                         
			 $action = Configure::read('app_root_path')."reports/saveRemittance";
			 echo $this->Form->create('InvoiceRemittance', array('id'=>'addRemittance', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ','novalidate' => true,'onsubmit'=>'javascript: return saveRemittance()')); 
			
			 echo $this->Form->input('experience_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'experience_id_remit','autofocus','error'=>false,'value'=>@$remittance_dtls['experience_id']));

			 echo $this->Form->input('homestay_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id_remit','autofocus','error'=>false,'value'=>@$remittance_dtls['homestay_id']));
		
			 echo $this->Form->input('booking_payment_response_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'booking_payment_response_id','autofocus','error'=>false,'value'=>@$remittance_dtls['booking_payment_response_id']));
		
			echo $this->Form->input('order_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'order_id','autofocus','error'=>false,'value'=>@$remittance_dtls['order_id']));
			
			echo $this->Form->input('outstanding_balance',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'outstanding_balance','autofocus','error'=>false,'value'=>@$remittance_dtls['outstanding_balance']));
		?> 
		<div class="form-group">
            <label class="col-md-4 col-xs-12 control-label" style="color:green; font-weight:bold;">Invoice Amount: <i class="fa fa-fw fa-1x">&#xf156;</i><?php echo number_format($remittance_dtls['invoice_amount'],2); ?> </label>
              <div class="col-md-4 col-xs-12" style="color:red; font-weight:bold;"> Agent Commission: 
				<i class="fa fa-fw fa-1x">&#xf156;</i> <?php echo $remittance_dtls['agent_commission']; ?>
             </div>
			<div class="col-md-4 col-xs-12" style="color:green; font-weight:bold;"> Remitted Amount: 
				<i class="fa fa-fw fa-1x">&#xf156;</i> <?php echo number_format($remittance_dtls['total_remitted_amount'],2); ?>
			</div>
        </div>
		<div class="form-group">
            <label class="col-md-4 col-xs-12 control-label">Remittance Amount:</label>
              <div class="col-md-4 col-xs-12">                                            
               <?php echo $this->Form->input('remitted_amount',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'remitted_amount','autofocus','data-rule-required'=>"true",'error'=>false,'min'=>1, 'max'=>$remittance_dtls['outstanding_balance']));?>
             </div>
			<div class="col-md-4 col-xs-12" style="color:green; font-weight:bold;"> Balance Amount: 
				<i class="fa fa-fw fa-1x">&#xf156;</i> <?php echo $remittance_dtls['outstanding_balance']; ?>
			</div>
        </div>
		<div class="form-group">
			<label class="col-md-4 col-xs-12 control-label">Remittance Date:</label>
			<div class="col-md-4 col-xs-12">  
				<?php echo $this->Form->input('remittance_date',array('div'=>false, 'label'=>false, 'class'=>'form-control datepicker', 'id'=>'remittance_date','autofocus', 'style'=>"font-weight:bold; font-size:16px; color:#7b214f;", 'data-rule-not-required'=>"true",'error'=>false,'type'=>'text'));?>

			</div>
			<div class="col-md-4 col-xs-12" style="color:green; font-weight:bold;">Invoice Date: 
				<i class="fa fa-fw fa-1x">&#xf156;</i> <?php echo $remittance_dtls['invoice_date']; ?>
			</div>
		 </div>
		<div class="form-group">
            <label class="col-md-3 col-xs-12 control-label">Mode of Payment:</label>
              <div class="col-md-12 col-xs-12">                                            
                <?php 
					$payment_mods=array('NEFT'=>'NEFT','CASH'=>'CASH','CHEQUE'=>'CHEQUE','A/C DEPOSIT'=>'A/C DEPOSIT','BALANCE-ADJUST'=>'BALANCE-ADJUSTMENT');
					echo $this->Form->input('mode_of_payment',array('div'=>false, 'label'=>false, 'class'=>'','style'=>'padding: 14px;  margin: 10px;padding-right: 40px;', 'type'=>'radio' ,'id'=>'mode_of_payment','autofocus','data-rule-required'=>"true",'error'=>false,'legend'=>false, 'name'=>'mode_of_payment','options'=>$payment_mods ));?>
             </div>
        </div>
		<div class="form-group">
            <label class="col-md-3 col-xs-12 control-label">Notes:</label>
              <div class="col-md-6 col-xs-12">                                            
               <?php echo $this->Form->input('note',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'note','autofocus','data-rule-required'=>"true",'error'=>false));?>
             </div>
        </div>
		<div class="form-group">
           <div class="col-md-6 col-xs-12" style="padding-left: 173px;padding-top: 20px;"> 
                <button class="btn btn-info"  >Remit</button>
            </div>
        </div>   
		 <?php echo $this->Form->end(); ?>   
		
	</div>
</div>

<div class="row">
	 <div class="col-md-12"> 
		 <div class="form-group" id="RemittanceDetails">
			<table>
				<tr>
					<th>Remitted Amount</th>
					<th>Remitted Date</th>
					<th>Mode of Payment</th>
				</tr>
					<?php foreach($remittance_dtls['old_remittance'] as $dtls){ ?>
					<tr>
						<td><?php echo $dtls['remitted_amount']; ?></td>
						<td><?php echo $dtls['remitted_date']; ?></td>
						<td><?php echo $dtls['mode_of_payment']; ?></td>
					</tr>

				<?php }?>


			</table>
          </div>		 
	</div>
</div>

<script>
$('#remittance_date').datepicker();
function saveRemittance(){
	var errorflag=false;
	$('#errorMsg').html('');
	var booking_payment_response_id= $('#booking_payment_response_id').val();
	var experience_id= $('#experience_id_remit').val();
	var homestay_id= $('#homestay_id_remit').val();
	var order_id= $('#order_id').val();
	var remitted_amount= $('#remitted_amount').val();
	var outstanding_balance= $('#outstanding_balance').val();	
	var remittance_date= $('#remittance_date').val();
	var mode_of_payment= $('input[name=mode_of_payment]:checked').val()
	var note= $('#note').val();
	//console.log("id"+$('#remitted_amount').val()+"ddd"+$('#outstanding_balance').val());
	if(remitted_amount==""){
		$('#errorMsg').html('* Remittance amount should be greater than 1.');
	}	
	else if(parseFloat(remitted_amount) > parseFloat(outstanding_balance) ){
		$('#errorMsg').html('* Entered remittance amount is greater than balance amount.');
	}
	else if(remittance_date=="" ){
		$('#errorMsg').html('* Please enter correct date for remittance.');
	}
	else if(mode_of_payment=="" ){
		$('#errorMsg').html('* Please choose correct mode of payment.');
	}
	if( typeof mode_of_payment=== "undefined"){
		$('#errorMsg').html('* Please choose correct mode of payment.');
	}
	else{
		$.ajax({
			   url: "<?php echo Configure::read('app_root_path'); ?>reports/saveRemittance",
			   data: 'booking_payment_response_id='+booking_payment_response_id+'&experience_id='+experience_id+'&homestay_id='+homestay_id+ '&order_id='+order_id+'&remitted_amount='+remitted_amount+'&remittance_date='+remittance_date +'&mode_of_payment='+mode_of_payment+'&note='+note,
			   type: "POST",
			   success: function(response) {
				   $('#ajaxRemittance').html(response);
				   $('.remittance_form').modal();               
			   }
		 });
	}
	return false;
	
}


</script>
<style>

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}
.modal{
		max-width: 800px !important;
	}
.form-group{
	margin-bottom:6px;		
	}
.form-horizontal .control-label {
	text-align: left;
	}
</style>
	