<?php echo $this->Html->script('plugins/bootstrap/bootstrap-timepicker.min.js'); ?>
<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
           
          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><strong>Add</strong> Stay</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                    <div class="panel-body">
                        <p>Place where you can add new stay/Homestay/accomadation.</p>
                    </div>
                <div class="panel panel-default tabs"> 
                 <?php 
                        if(isset($this->params['pass'][1]) && $this->params['pass'][1]!="") 
                            $tab=$this->params['pass'][1];
                        else
                            $tab="listing";  
                    
                    ?>
                     <ul class="nav nav-tabs" role="tablist">
                       <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Listing Details</a></li>  
                        <li><a href="#tab-second" role="tab" data-toggle="tab">Amenities</a></li>
                       <li><a href="#tab-third" role="tab" data-toggle="tab">Pricing Factors</a></li>
                       <li><a href="#tab-fourth" role="tab" data-toggle="tab">Availability</a></li>
                    </ul>
                    <div class="panel-body tab-content">   
                        <div class="tab-pane active" id="tab-first">
                           <?php                         
                                 $action = Configure::read('app_root_path')."manage_homestays/save";
                                 echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));  
                                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
                             echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'listing'));
                         ?>   
                            
                          <div class="row">
                                <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">What type of property you offering for the stay?</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php  
                                                    echo $this->Form->input('property_type_id', array('options'=>$array_property_types,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'property_type_id', 'empty'=>'--Select--')); 

                                                    ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of the stay</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    
                                                        <?php echo $this->Form->input('name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Which City you are offering the stay?</label>
                                                <div class="col-md-9 col-xs-12" style="float:left; padding-left:50px;">    
                                                   <?php  
                                                    echo $this->Form->input('city_id', array('options'=>$array_cities,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'city_id', 'empty'=>'--Select--')); 

                                                    ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                                                                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('description',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'description','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">House Rules</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('house_rules',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'house_rules','autofocus','data-rule-not-required'=>"true",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cancellation Policy</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('cancellation_policy',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'cancellation_policy','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
											 <div class="form-group">
                                                <label class="col-md-3 control-label">Activities Around</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('activities_around',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'activities_around', 'autofocus','data-rule-not-required'=>"true",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Owner Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('owner_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'owner_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Contact Person Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('contact_person_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'contact_person_name','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Mobile 1</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('mobile1',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'mobile1','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Mobile 2</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('mobile2',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'mobile2','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            
<!--
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">File</label>
                                                <div class="col-md-9">                                                                                                                                        
                                                    <input type="file" class="fileinput btn-primary" name="filename" id="filename" title="Browse file"/>
                                                    <span class="help-block">Input type file</span>
                                                </div>
                                            </div>
                                            
-->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Telephone</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('telephone',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'telephone','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('email',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'email','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Address</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('address',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'address','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Latitude</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('latitude',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'latitude','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Longitude</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('longitude',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'longitude','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">No of Bedrooms</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('no_of_bedrooms',array('div'=>false, 'label'=>false, 'class'=>'validate[required,custom[integer]] form-control', 'id'=>'no_of_bedrooms','autofocus','error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Max No of Guest</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('max_no_of_guest',array('div'=>false, 'label'=>false, 'class'=>'validate[required,custom[integer]]  form-control', 'id'=>'max_no_of_guest','autofocus','error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">No of Bathrooms</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('no_of_bathrooms',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'no_of_bathrooms','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">No of beds</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('no_of_beds',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'no_of_beds','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Check In Time</label>
                                                <div class="col-md-5">
                                                    <div class="input-group bootstrap-timepicker">
                                                     <?php echo $this->Form->input('check_in_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'check_in_time', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Check Out Time</label>
                                                <div class="col-md-5">
                                                    <div class="input-group bootstrap-timepicker">
                                                     <?php echo $this->Form->input('check_out_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'check_out_time', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                          <div class="form-group">
                                                <label class="col-md-3 control-label">Food Cusine</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('food_cuisine',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'food_cuisine','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>    
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Other Details</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('other_details',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'other_details','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Cleaning Charges</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('cleaning_charges',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'cleaning_charges', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Title</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_title',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_title','autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Keyword</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_keyword',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_keyword','autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
											
											  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Description</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('meta_description',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'meta_description', 'autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
											
                                            
                                        </div>                                          
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                            <div class="row">
                                <div class="col-md-9" style="width:53%;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                          <h3><span class="fa fa-download"></span> Dropzone</h3>
                                          <p>Add form with class <code>dropzone</code> to get dropzone box</p>
                                            <div id="dZUpload" class="dropzone" style="min-height:150px;min-width:">
                                                <div class="dz-default dz-message"></div>
                                            </div>
                                        </div>
                                    </div>   
                                 </div> 
                                <div> <ul> <?php
                                    if(count($homestayFiles)>0){
                                    
                                        foreach($homestayFiles as $files) { 
                                        //debug($files);
                                        ?>
                                        <li><img src="<?php echo Configure::read('app_root_path').$files['HomestayFile']['file_path'].'/'.$files['HomestayFile']['file_name']; ?>" width="100" height="100"/>&nbsp;&nbsp; <a href="<?php echo Configure::read('app_root_path'); ?>manage_homestays/deleteFile/<?php echo $files['HomestayFile']['id']; ?>" alt="Delete image"><i class="fa fa-times fa-3x"></i></a></li>
                                    <?php }
                                        } ?>
                                 </ul>
                                </div>
                            </div><!-- END OF Second ROW -->
                         
                            <div class="panel-footer" >
                                <button class="btn btn-default">Clear Form</button>                                    
                                <button class="btn btn-primary pull-middle">Submit</button>
                           </div>
                            
                            <?php echo $this->Form->end(); ?>      
                        </div>  <!-- END OF First Tab Stay -->
                        
                        
                         <!-- START OF Second Tab Amentities  -->
                         <div class="tab-pane" id="tab-second">
                          <?php                         
                                 $action = Configure::read('app_root_path')."manage_homestays/saveAmenties";
                                 echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
                                echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'amenities'));
                             ?>
                           
                             <div class="col-md-12">
                                 
                                 <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" style="text-align:center; font-size: 16px;
    color: #1d908b;">What amenities do you offer? &nbsp;&nbsp;</label>                                    
                                 </div>
                                <div class="form-group">
                                 <?php   foreach($amenities_arr as $key=>$val){    ?>
                                   <div class="col-md-4">                                    
                                    <label class="check">
                                       <?php 
                                        $checkedFlag="";
                                        if(in_array($val['Amenity']['id'],$homestayAmenities_arr)) {
                                            $checkedFlag='checked';
                                        }                                  
                                        echo $this->Form->input("HomestayAmenity.amenity_id.[]", array(                    'type'=>'checkbox','name'=>'data[HomestayAmenity][amenity_id][]','value'=>$val['Amenity']['id'], 'checked'=>$checkedFlag, 'class'=>"icheckbox", 'label'=>false, 'id'=>'HomestayAmenity.amenity_id') ); 
                                        echo "<b class='amenity_name'>".$val['Amenity']['amenity']."</b>"; ?>       
                                    </label> 
                                            <?php  echo "<br/><span class='amenity_desc'>". $val['Amenity']['description']."</span>"; ?> 
                                   </div>
                                 
                                
                               <?php   } ?>
                                </div> 
                                 <br/><br/>
                            </div>
                             
                             
                           <div class="panel-footer" >
                                <button class="btn btn-default">Clear Form</button>                                    
                                <button class="btn btn-primary pull-middle">Submit</button>
                          </div>
                             
                             
                             <?php echo $this->Form->end(); ?>      
                         </div>
                         <!-- END OF Second Tab Amentities  -->
                        
                        
                        <!-- START OF fourth Tab Availability -->
                         <div class="tab-pane" id="tab-fourth">
                          <?php                         
                                 $action = Configure::read('app_root_path')."manage_homestays/saveAvailability";
                                 echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
                                echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'availability'));
                             ?>
                            <div class="col-md-8">
                                <div class="form-group">
                                 <div class="content-frame-body padding-bottom-0" style="height:auto !important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="alert_holder"></div>
                                            <div class="calendar">                                
                                                <div id="calendar"></div>                            
                                            </div>
                                        </div>
                                    </div>
                                 </div>    
                               </div> 
                             </div>
                             <div class="col-md-4">
                                   <div class="form-group">
                                     <label class="col-md-3 col-xs-12 control-label">Date</label>
                                       <div class="col-md-6 col-xs-12">  
                                            <?php echo $this->Form->input('OccupancyDetail.date',array('div'=>false, 'label'=>false, 'class'=>'form-control ', 'id'=>'dateBoxAvailability','autofocus', 'style'=>"font-weight:bold; font-size:16px; color:#7b214f;", 'data-rule-not-required'=>"true",'error'=>false,'type'=>'text','readonly'));?>
                                           
                                        </div>
                                     </div>
                                    <div class="form-group">
                                          <label class="col-md-3 col-xs-12 control-label">Bed Rooms &nbsp;&nbsp;</label>
                                          <div class="col-md-6 col-xs-12">                                                                  <?php                                                    
                                                    echo $this->form->input('OccupancyDetail.bedrooms', array('label'=>false,'type' => 'select', 'options' => $rooms, 'multiple' =>'checkbox', 'id'=>'OccupancyDetail.id', 'class'=>'bedroomsAvail','data-rule-required'=>'true', 'error'=>'false'));
                                              
                                                    if(count($rooms)==0){
                                                        echo "(Please set the Pricing factors first..!!)";
                                                    }
                                              ?>
                                          </div>
                                    </div>
                                 
                                 <div class="form-group">
                                     
                                       <div class="col-md-6 col-xs-12"> 
                                            <button class="btn btn-info" >Save Availability</button>
                                        </div>
                                     </div>  
                              </div>
                             
                             
                             <?php echo $this->Form->end(); ?>      
                         </div>
                        <!-- END OF Second Tab Availability -->
                        
                        <div class="tab-pane" id="tab-third">
                          <?php                         
                            $action = Configure::read('app_root_path')."manage_homestays/save";
                            echo $this->Form->create('Homestay', array('id'=>'addRoom', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                                          //  debug($this->request->data);
                            echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false));
                            echo $this->Form->input('tabName',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tabName','autofocus','error'=>false,'value'=>'pricing'));
                             echo $this->Form->input('no_of_bedrooms',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'no_of_bedrooms','autofocus','error'=>false));
                          ?>
                           <?php 
                            $no_of_rooms=$this->request['data']['Homestay']['no_of_bedrooms'];
                            for($k=0; $k<$no_of_rooms;$k++){ ?>
                                   <div class="form-group">
                                     <label class="col-md-3 col-xs-12 control-label">BedRoom &nbsp;&nbsp;<?php echo $k+1; ?> &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        <div class="col-md-8 col-xs-12">                                                                 <?php  
                                                echo $this->Form->input("RoomPriceDetail.$k.id",array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>"RoomPriceDetail.$k.id",'autofocus','error'=>false));
                                                            
                                           // echo $this->form->input("MasterBedType.BedRoom_$k", array('label'=>false, 'type' => 'select', 'options' => $array_bed_types, 'multiple' =>'checkbox', 'id'=>'MasterBedType.id', 'data-rule-required'=>'true', 'style'=>'float:left;  display:inline;', 'class'=>"icheckbox", 'error'=>'false'));
                                                 //debug($array_bed_types);  
                                            $i=0;
                                            foreach($array_bed_types as $keyBed=>$bed){  
                                                     
                                                if(in_array($keyBed,$this->request->data['MasterBedType']["BedRoom_$k"])){
                                                    //echo "ss".$keyBed;
                                                   $chekedFlag='checked="checked"';
                                                   $no_of_bed=$this->request->data['MasterBedType']["BedCount_$k"][$keyBed];
                                                   $i++;
                                                   $no_of_bed_value="value='$no_of_bed'";
                                                }else{
                                                    $chekedFlag='';
                                                    $no_of_bed_value="";
                                                }
                                                '$no_of_bed'
;                                                 //if(in_array($keyBed,$this->request->data['MasterBedType']["BedCount_$k"]))
                                                
                                                    ?> 
                                            <div class="icheckbox">
                                            
                                            <input type="checkbox" name="data[MasterBedType][BedRoom_<?php echo $k;?>][]" id="MasterBedType.BedRoom_<?php echo $k;?>." data-rule-required="true" style="float: left; display: inline; position: absolute; opacity: 0;" <?php echo $chekedFlag; ?>  value="<?php echo $keyBed;?>">
                                            <input type="number" min="1" max="10" name="data[MasterBedCount][BedCounter_BedRoom_<?php echo $k;?>][<?php echo $keyBed;?>]" style="width:38px;" <?php echo $no_of_bed_value; ?> />
                                                    <label for="MasterBedType.id<?php echo $k;?>" class="selected hover"><?php echo $bed; ?></label>
                                                    
                                                  </div>

                                        <?php  }
                                         ?>
                                     
                                       </div>                            
                                   </div>
                                   <div class="form-group" style="width:50%;float:left;">
                                     <label class="col-md-3 col-xs-12 control-label">Room Base Price</label>
                                       <div class="col-md-6 col-xs-12 ">                                                                   <?php echo $this->Form->input("RoomPriceDetail.$k.room_base_price",array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'RoomPriceDetail.room_base_price', 'autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                        </div>                                         
                                   </div>  
                                    <div class="form-group" style="width:50%;float:left;" >
                                     <label class="col-md-3 col-xs-12 control-label">Extra Bed Charge</label>
                                       <div class="col-md-6 col-xs-12 ">                                                                   <?php echo $this->Form->input("RoomPriceDetail.$k.extra_bed_charge",array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'RoomPriceDetail.extra_bed_charge', 'autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                        </div>                                         
                                   </div> 
                                 <div class="form-group" style="width:100%;float:left;">
                                     <label class="col-md-3 col-xs-12 control-label">Extra Bed Availablity &nbsp;&nbsp;</label>
                                       <div class="col-md-6 col-xs-12 "> 
                                        <?php echo $this->Form->input("RoomPriceDetail.$k.extra_bed", array(                    'type'=>'checkbox', 'label'=>false, 'id'=>'RoomPriceDetail.extra_bed'  ) ); ?>
                                           
                                           (Please check this flag if this room have an additional flag and select the bed type above.)
                                        </div>                                         
                                   </div>  
							<div class="form-group" style="width:50%;float:left;">
                                <label class="col-md-3 col-xs-6 control-label">Room Type &nbsp;&nbsp;</label>
                                       <div class="col-md-6 col-xs-6 "> 
                                        <?php echo $this->Form->input("RoomPriceDetail.$k.room_type_id", array('options'=>$array_room_types,'div'=>false, 'label'=>false,  'class'=>'form-control select',  'type'=>'select', 'id' =>'RoomPriceDetail.room_type_id', 'empty'=>'--Select--')); 
										   	?>
                                        </div>                                         
                             </div> 
							
							<div class="form-group" style="width:50%;float:left;">
                                <label class="col-md-3 col-xs-6 control-label">Room Description &nbsp;&nbsp;</label>
                                       <div class="col-md-6 col-xs-6 "> 
                                        <?php echo $this->Form->input("RoomPriceDetail.$k.room_description",array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'RoomPriceDetail.room_description', 'autofocus','data-rule-required'=>"false",'error'=>false,'type'=>'textarea'));
										   	?>
                                        </div>                                         
                             </div> 
                          <div style="clear:both;"></div>
                            <hr style="font-weight:bold;"/>
                            <?php } ?>
                            
                             
                        
                        
                            <div class="panel-footer" >
                                <button class="btn btn-default">Clear Form</button>                                    
                                <button class="btn btn-primary pull-middle">Submit</button>
                           </div>
                             <?php echo $this->Form->end(); ?>            
                        </div> <!-- END OF panel-body --> 
                      
               </div>
               
              
         </div>
                            
      </div>
   </div>   
</div>

<style>
.radio, .checkbox {
    width: 110px !important; 
    }
    .fc-time{
        display: none !important;
    }
    .form-horizontal .radio, .form-horizontal .checkbox, .form-horizontal .radio-inline, .form-horizontal .checkbox-inline{
            padding-top: 2px !important;
    }
    .radio, .checkbox{
        width: 29px !important;
    }
    .amenity_desc{
        font-weight: 300 !important;
        cursor: pointer !important;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
        font-size: 15px !important;
        line-height: 18px !important;
        letter-spacing: 0.2px !important;
        padding-top: 4px !important;
        padding-bottom: 0px !important;
        color: #484848 !important;
        display: block !important;
        padding-left: 26px !important;
    }
    .amenity_name{
        font-weight: 300 !important;
        cursor: pointer !important;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
        font-size: 19px !important;
        line-height: 24px !important;
        letter-spacing: undefined !important;
        padding-top: 2px !important;
        padding-bottom: undefined !important;
        color: #484848 !important;
        display: inline-block !important;
        position: relative !important;
        top: -3px !important;
        vertical-align: top !important;
       
    }
    .icheckbox{
        float: left;
        padding: 3px;
        width: 190px !important;
    }
</style>
    
    
<script>
var occupancyDetailJsonString=<?php echo $occupancyDetailString; ?>   
var totat_bedrooms=<?php echo $this->request->data['Homestay']['no_of_bedrooms']; ?>
//  console.log('bed'+occupancyDetailJsonString);
 $(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();


  var calendar = $('#calendar').fullCalendar({
   editable: true,
   header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
   },


   events: "events.php",


   eventRender: function(event, element, view) {
    if (event.allDay === 'true') {
     event.allDay = true;
    } else {
     event.allDay = false;
    }
   },
   selectable: true,
   selectHelper: true,
   select: function(start, end, allDay) {
       var start_date = moment(start, 'DD.MM.YYYY').format('YYYY-MM-DD');
       var end_date = moment(start, 'DD.MM.YYYY').format('YYYY-MM-DD');
       var homestay_id = $('#homestay_id').val();
     
       var title = 'findAvailability';
       $.ajax({
           url: "<?php echo Configure::read('app_root_path'); ?>manage_homestays/findAvailability",
           data: 'title='+ title+'&start_date='+ start_date +'&end_date='+ end_date +'&homestay_id='+ homestay_id,
           type: "POST",
           success: function(response) {
                $('#dateBoxAvailability').val(start_date); 
			  
               var obj = JSON.parse(response);
			   console.log(obj);
               $('.bedroomsAvail input[type="checkbox"]').each(function() {                 
                   if ($.inArray($(this).val(), obj) != -1)
                    {
                        $(this).prop('checked', true);                           
                    }else{
                         $(this).prop('checked', false);         
                    }
                            
                });
               
           }
       });
       /*
           calendar.fullCalendar('renderEvent',
           {
               title: title,
               start: start_date,
               end: end_date,
               allDay: allDay
           },
           true
           );
       */
   calendar.fullCalendar('unselect');
   },

/*
   editable: true,
   eventDrop: function(event, delta) {
   var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
   var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
   $.ajax({
	   url: 'update_events.php',
	   data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.id ,
	   type: "POST",
	   success: function(json) {
	    alert("Updated Successfully");
	   }
   });
   },
   eventClick: function(event) {
	var decision = confirm("Do you really want to do that?"); 
	if (decision) {
	$.ajax({
		type: "POST",
		url: "delete_event.php",
		data: "&id=" + event.id,
		 success: function(json) {
			 $('#calendar').fullCalendar('removeEvents', event.id);
			  alert("Updated Successfully");}
	});
	}
  	},
   eventResize: function(event) {
	   var start = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss");
	   var end = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss");
	   $.ajax({
	    url: 'update_events.php',
	    data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.id ,
	    type: "POST",
	    success: function(json) {
	     alert("Updated Successfully");
	    }
	   });
	}
   
      */
      
  });
   
 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // TODO: check href of e.target to detect your tab
             //   $('#calendar').fullCalendar('render');
        });
     
//loading all the bookings/occupancies to calender. 
//Creating each booking as an event against the date.
     if(occupancyDetailJsonString!=""){         
        // console.log(occupancyDetailJsonString);
         $.each(occupancyDetailJsonString, function(key,value) {
			 //console.log(value);
			// console.log(value['OccupancyDetail']['check_in_date']);
			 	var check_in_date=value['OccupancyDetail']['check_in_date'];
			 	var check_out_date=value['OccupancyDetail']['check_out_date'];	
			 	var no_of_bookings=value['OccupancyDetail']['no_of_bookings'];	
                var freeRooms=totat_bedrooms-no_of_bookings;
                var titleMsg='Booked: '+no_of_bookings+'\n Free: '+freeRooms;
                start_date=check_in_date;
                end_date=check_out_date;
                allDay=false; //setting this to false to avoid displaying time.
                calendar.fullCalendar('renderEvent',
                   {
                       title: titleMsg,
                       start: start_date,
                       end: end_date,
                       allDay: false
                   },
                   true
                );
         }); 
     
     }
     
     
 });
    

</script>    
<?php if($tab=="availability") { ?>
    <script>
      $('.nav-tabs a[href="#tab-fourth"]').tab('show');
     </script>
    
 <?php }elseif($tab=="amenities") { ?>
    <script>
      $('.nav-tabs a[href="#tab-second"]').tab('show');
    </script>
<?php } ?>    
