 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">

                                    <table id="datadisplay" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Host Name</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th>Actions</th>
            </tr>
        </thead>
        
    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->



<script>

$(document).ready(function() {

    $('#datadisplay').DataTable( {
        "aProcessing": true,
        "aServerSide": true,
        "ajax": '<?php echo Configure::read('app_root_path'); ?>manage_agent/ajax_agent_list',
        "aoColumns": [
            { "data": "agent_name" },
            { "data": "email_id" },
            { "data": "contact_number" },
            { "data": "actions" },
           
        ],
        

    } );
} );

</script>