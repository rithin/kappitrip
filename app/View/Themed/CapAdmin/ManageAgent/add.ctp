<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_agent/save";
                 
                 echo $this->Form->create('User', array('id'=>'addAgent', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                         
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
                  
                  echo $this->Form->input('UserDetail.id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));

			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Host</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new Hosts.</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">First Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    
                                                       
                                                    
                                                        <?php echo $this->Form->input('first_name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'product_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Last Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    
                                                    
                                                        <?php echo $this->Form->input('last_name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'last_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Email Id</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    
                                                    
                                                        <?php echo $this->Form->input('agent_email',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'agent_email','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    
                                                </div>
                                            </div>
                                            
                                            <?php if($add_edit=='add'){ ?>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Password</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    
                                                    
                                                        <?php echo $this->Form->input('agent_password',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'agent_password','data-rule-required'=>"true",'autofocus','error'=>false));?>
                                                        
                                                    
                                                </div>
                                            </div>
                                            
                                            <?php } ?>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Contact Number</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    
                                                    
                                                        <?php echo $this->Form->input('contact_number',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'contact_number','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    
                                                </div>
                                            </div>
                                            
                                                
                                            
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Company Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('comapny_name',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'comapny_name','autofocus','error'=>false));?>
                                                 
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Address 1</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                      <?php echo $this->Form->input('address1',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'address1', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                              
                                                </div>
                                            </div>     
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Address 2</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('address2',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'address2','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">City</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('city',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'city','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                             
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">State</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php 
                                                  
                                                    echo $this->Form->input('state_id', array('options'=>$array_states,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'state_id',
                                                        'empty'=>'--Select--')); ?>
                                               
                                                </div>
                                            </div>
                                             
                                          <div class="form-group">
                                                <label class="col-md-3 control-label">Country</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('country',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'country','autofocus','data-rule-not-required'=>"true",'error'=>false));?>
                                                  
                                                </div>
                                            </div>    
                                            
                                          
                                            
                                                                                      
                                            
                                        </div>                                          
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                            

                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>


<script>

    $("#addAgent").validate({
                ignore: [],
                rules: {                                            
                        'data[User][first_name]': {
                                required: true,
                        },
                        'data[User][last_name]': {
                                required: true,
                        },
                        'data[User][agent_email]': {
                                required: true,
                                email: true
                        },
                        'data[User][agent_password]': {
                                required: true,
                                minlength: 6,
                                
                        },
                        'data[User][contact_number]': {
                                required: true,
                        },
                        'data[User][address1]': {
                                required: true,
                        },
                        'data[User][address2]': {
                                required: true,
                        },
                        'data[User][city]': {
                                required: true,
                        },
                        'data[User][state_id]': {
                                required: true,
                        },
                        'data[User][country]': {
                                required: true,
                        },
                        
             },
                
        
        
     });   

</script>
