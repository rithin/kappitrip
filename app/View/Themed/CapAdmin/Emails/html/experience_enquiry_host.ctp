<div bgcolor="#ededed" marginwidth="0" marginheight="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;margin:0px!important;padding:0px!important;background:#ededed!important;margin:0px;padding:0px;background:#ededed">
  <table style="font-family:'Open Sans',Arial,Helvetica,sans-serif;border-collapse:collapse!important;background:#ededed!important;border-collapse:collapse;background:#ededed" cellspacing="0" cellpadding="0" border="0" bgcolor="#ededed" width="100%">
    <tbody>
      <tr>
        <td align="center" bgcolor="#ededed" valign="top">
          <table style="border-collapse:collapse" align="center" cellspacing="0" cellpadding="0" border="0" width="700">
						<tbody>
							<tr>
								<td style="height:20px!important;line-height:10px!important;font-size:20px!important;color:#ededed!important;height:10px;line-height:20px;font-size:20px;color:#ededed" height="20" align="center" valign="top">.</td>
							</tr>
							<tr>
								<td align="center" valign="top">
									<table style="border-collapse:collapse;background:#ffffff;font-family:'Open Sans',Arial,Helvetica,sans-serif" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="700">
										<tbody>
											<tr>
												<td style="background:#ffffff" align="left" bgcolor="#ffffff" width="20" valign="top">&nbsp;</td>
												<td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;background:#ffffff" align="left" bgcolor="#ffffff" valign="top">
													<table style="border-collapse:collapse;background:#ffffff;font-family:'Open Sans',Arial,Helvetica,sans-serif" cellspacing="0" cellpadding="0" border="0" width="100%">
														<tbody>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="center" valign="top">
																	<table style="border-collapse:collapse;background:#ffffff;font-family:'Open Sans',Arial,Helvetica,sans-serif" cellspacing="0" cellpadding="0" border="0" width="660">
																		<tbody>
																			<tr>
																				<td align="center" valign="top">
																					<img src="https://ci3.googleusercontent.com/proxy/w8NAwD0z_8k8hwfHDQ4A6jlAgBuLJtMip--wh9V-5PwSwB92PMFEAdo53dcjbtcfwXJRUZo54skij-SySLbjNvlxGxiGxsxWpJ8kbTaCVTyplxXyC0iDvgldU0Y_ZcvyBxFUL_UjtXHwAE6-RFY=s0-d-e1-ft#https://res.cloudinary.com/wandertrails/image/upload/newdesign/mailer/emailer-header.png" alt="" class="CToWUd">
																					
																					<img src="<?php echo $head_image; ?>" alt="" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 792.5px; top: 586px;"><div id=":1q5" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>
																					
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="left" valign="top">
																	<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121;line-height:20px">
																	<span style="font-size:1.3em">Dear <span style="color:#eb344a">Ricky Monappa</span>,
																</span>
																<br><br>
																	<span style="font-size:1.2em;line-height:30px">Greetings from Wandertrails!</span>
                                <br><br>
                                
																	<span style="font-size:1.2em;line-height:30px">You have received a booking request at Sunset Drive to Mandalpatti for Standard Experience on <span class="aBn" data-term="goog_182670849" tabindex="0"><span class="aQJ">11/10/2018</span></span>.<br></span></font>
                                
                                </td>
															</tr>
															<tr>
																<td height="20" valign="top"></td>
															</tr>
															<tr>
																<td height="20" valign="top"></td>
															</tr>
															<tr>
																<td align="left" valign="top">
																	<table cellspacing="0" cellpadding="0" border="0" width="660px">
																		<tbody>
																			<tr>
																				<td colspan="2"><font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#353535">
																					</font>
																				</td>
																			</tr>
																			<tr>
																				<td style="padding:5px" bgcolor="#f3f3f3">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#353535"><b>Booking Summary:
																					</b></font>
																				</td>
																				<td style="padding:5px" bgcolor="#f3f3f3"></td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Experience Name: </font>
																				</td>
                                        <td align="left">
                                      	   <font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Sunset Drive to Mandalpatti</font>
                                        </td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Experience Type: </font>
																				</td>
																				<td align="left">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Standard Experience</font>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			
																			
																			
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Date: </font>
																				</td>
																				<td align="left">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121"><span class="aBn" data-term="goog_182670850" tabindex="0"><span class="aQJ">11/10/2018</span></span></font>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			
																			
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">No. of Adults: </font>
																				</td>
																				<td align="left">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">4</font>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">No. of Children: </font>
																				</td>
																				<td align="left">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">0</font>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
                                      
																			<tr>
																				<td style="border-top:1px solid light-gray"></td>
																				<td style="border-top:1px solid light-gray"></td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			<tr>
																				<td style="padding:5px" bgcolor="#f3f3f3">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#353535">
																					<b>Approximate booking value:
																					</b></font>
																				</td>
																				<td style="padding:5px" align="left" bgcolor="#f3f3f3">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.4em;color:#353535">
																					<b>2,222</b></font>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr><td width="60%" style="text-align:center">
                                <font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121"><b>Please mark your acceptance below:</b></font>
                              </td>
                              </tr><tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr align="start" valign="top">
                                <td>
                                  <a href="http://email.wandertrails.com/c/eJxdj8FuhSAURL8Gl4bLBbwsXNg2_gfopZJYJeCr7_Mf7aKLJpNJ5ixmMusIoIC7NCoJJK1CaTRK20P_PumJjHpTjmiWyggtb3-sXK7i01775fzqtpFlIGUHlAzGDEDWkw9DiM5pBYjU7eN2XbkKnISam-777v_3NOxzav4Nzeoj5z1x-cl-T6u_-A_hXLjm86gs8OMqDxbKbr5uLZGjGElqb9CiDW4A7VbnggkRFkbsypiOeLYby3mWT37mVvU7_wKx5lAT" style="margin-left:22%;float:left;background:#09a335;padding:12px 30px;border-radius:30px;color:white;text-decoration:none;font-weight:900;letter-spacing:2px" rel="noreferrer" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.wandertrails.com/c/eJxdj8FuhSAURL8Gl4bLBbwsXNg2_gfopZJYJeCr7_Mf7aKLJpNJ5ixmMusIoIC7NCoJJK1CaTRK20P_PumJjHpTjmiWyggtb3-sXK7i01775fzqtpFlIGUHlAzGDEDWkw9DiM5pBYjU7eN2XbkKnISam-777v_3NOxzav4Nzeoj5z1x-cl-T6u_-A_hXLjm86gs8OMqDxbKbr5uLZGjGElqb9CiDW4A7VbnggkRFkbsypiOeLYby3mWT37mVvU7_wKx5lAT&amp;source=gmail&amp;ust=1531421935747000&amp;usg=AFQjCNFE4PSTtoNkYFZKSkyn9sHLJkD89g">Accept</a>
                                  <a href="http://email.wandertrails.com/c/eJxdT0tuxSAQO02yjBgGKCyySFvlHnyGBokGBOnLO35pF11UsmzZC1sOKwAHmtPKGWimODIpkKkFlrdNbFryV2603hmXk2C3PQO1q9mU--LL53ysXHMirxFjcKCcBQXCk1AkbQDmwpzX47pqn3Cb-D5w3_fyv2fEtqbBDxjUv2rNidqPtzkFe9FfhHujXsvZacL3aPNQrg7bj2G10TFqJqxEhcqZFxAmGOOki-AJcW5rOmMZP3wp7YOedXT97n8D2ZJRlg" style="float:right;margin-right:22%;background:#eb344a;padding:12px 30px;border-radius:30px;color:white;text-decoration:none;font-weight:900;letter-spacing:2px" rel="noreferrer" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.wandertrails.com/c/eJxdT0tuxSAQO02yjBgGKCyySFvlHnyGBokGBOnLO35pF11UsmzZC1sOKwAHmtPKGWimODIpkKkFlrdNbFryV2603hmXk2C3PQO1q9mU--LL53ysXHMirxFjcKCcBQXCk1AkbQDmwpzX47pqn3Cb-D5w3_fyv2fEtqbBDxjUv2rNidqPtzkFe9FfhHujXsvZacL3aPNQrg7bj2G10TFqJqxEhcqZFxAmGOOki-AJcW5rOmMZP3wp7YOedXT97n8D2ZJRlg&amp;source=gmail&amp;ust=1531421935748000&amp;usg=AFQjCNF5pNAtscCsF4lua1d7h3auD_pZxw">Decline</a>
                                </td>
                              </tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="center" valign="top">
																	<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#3e6793;line-height:20px;text-align:center">
																	For any further clarifications please do not hesitate to reach out to us via <br> email <a href="mailto:connect@wandertrails.com" style="color:#eb344a" rel="noreferrer" target="_blank">connect@wandertrails.com</a> or call us on PH: <a href="tel:+91%209632563535" style="color:#eb344a" rel="noreferrer" target="_blank">+91 9632563535</a>
																	<br><br>
																	Best regards,<br>
																	Team Wandertrails<br><br>
																	<a href="http://email.wandertrails.com/c/eJxlzEEOwiAQQNHTyJIMAwywYFFNeg_CgJLUYqgJHr_Greuf9zkqhaqIFhGUB0IN1mggqeRtMYu3eMXg_QpoLwZm2rmM90htO2TuT_GIznmNlotGyNVUW8kxYQBOxCEYJ7Y455R_csS21_595t7HvXxeoxy_cgJtiCxE" style="border:none;outline:none;text-decoration:none;color:lightgrey" rel="noreferrer" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.wandertrails.com/c/eJxlzEEOwiAQQNHTyJIMAwywYFFNeg_CgJLUYqgJHr_Greuf9zkqhaqIFhGUB0IN1mggqeRtMYu3eMXg_QpoLwZm2rmM90htO2TuT_GIznmNlotGyNVUW8kxYQBOxCEYJ7Y455R_csS21_595t7HvXxeoxy_cgJtiCxE&amp;source=gmail&amp;ust=1531421935748000&amp;usg=AFQjCNE8Ickvf5IUsbOm0xNzZPHlhbkzNA">WWW.WANDERTRAILS.COM</a><br>
																	</font>
																</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="center" style="font-family:Arial,Helvetica,sans-serif">
																	<a href="http://email.wandertrails.com/c/eJxNjTsOhCAUAE8jJXk8BZ8FhbuJ9yB8lETFAAl7_NWtNplqihmnhUDhWdQIgkBhD3LoQXHB3_Mwk8QXTkQLoOwGaOZ0Ptds4l64TQfbtARrplH53iCQUmMg5yRIqciOAiyxXW-1XqXr5w6Xm9Yaj2epZs3meCKP--umEKKNZr81yzqeId1jm1Je_efKvvzGX1mMOEs" rel="noreferrer" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.wandertrails.com/c/eJxNjTsOhCAUAE8jJXk8BZ8FhbuJ9yB8lETFAAl7_NWtNplqihmnhUDhWdQIgkBhD3LoQXHB3_Mwk8QXTkQLoOwGaOZ0Ptds4l64TQfbtARrplH53iCQUmMg5yRIqciOAiyxXW-1XqXr5w6Xm9Yaj2epZs3meCKP--umEKKNZr81yzqeId1jm1Je_efKvvzGX1mMOEs&amp;source=gmail&amp;ust=1531421935748000&amp;usg=AFQjCNE9RdIo9iY3vf1T2xvPSI4iFiQZVw"><img style="margin:0 10px;width:30px;height:30px" src="https://ci3.googleusercontent.com/proxy/M2uRiXuyIt9P5Wg0lY-cSHNG-TzIdeCZ9GgNPaQ_gFdWzTOrCXcWZrzkhQf7N6affVeOxFVIx2nZDIoRfUUiEq9BksEJx57SlKZMnKeC5eBob-gf6MBlQg6OZ4zAYS2mZC4-BkBQDed2gnCkuLD5Sq8=s0-d-e1-ft#https://res.cloudinary.com/wandertrails/image/upload/v1479037799/newdesign/mailer/insta.png" class="CToWUd"></a>&nbsp;
																	<a href="http://email.wandertrails.com/c/eJxNjcEOgyAQBb9GjmTZRcQDB9vG_0CEamrFAAn9_GpPTd5p8jIzGyFQeLYaBKFBIUErCRQX_D7IQbd4w17rEbBtJFS7zz6VZNctcxffbDFygl5YN0-dCx6hI5RInSKyChS5nm1mKeXIDQ0NjudqrTxY56cYX5fjQn_a60RjSD409CiZJbPuIZ5tF2N6-s-RfP61v-GgOF0" rel="noreferrer" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.wandertrails.com/c/eJxNjcEOgyAQBb9GjmTZRcQDB9vG_0CEamrFAAn9_GpPTd5p8jIzGyFQeLYaBKFBIUErCRQX_D7IQbd4w17rEbBtJFS7zz6VZNctcxffbDFygl5YN0-dCx6hI5RInSKyChS5nm1mKeXIDQ0NjudqrTxY56cYX5fjQn_a60RjSD409CiZJbPuIZ5tF2N6-s-RfP61v-GgOF0&amp;source=gmail&amp;ust=1531421935748000&amp;usg=AFQjCNEOenW8B3fZ8HCCsj5aHpFaDOSeuA"><img style="margin:0 10px;width:30px;height:30px" src="https://ci5.googleusercontent.com/proxy/3pWj8uWZ9JuforfHYTd-_xR9RWHw7wQTuH6glTKXqVzHnJQjap70Bv-0X8Ssfj3tXP-qc_i4MU38ZQW4ycSmWMKiGuqzkbszsBVjnzlEfWMQeaf6xlEFKFP0V9y2HHaKge_5xF-y6iFQOavsyK4=s0-d-e1-ft#https://res.cloudinary.com/wandertrails/image/upload/v1479037800/newdesign/mailer/fb.png" class="CToWUd"></a>&nbsp;
																	<a href="http://email.wandertrails.com/c/eJwdjUEKgzAQRU9jlpKZJDousrAFT1DosoxJrAFrJAbs8WuFv3mL_563AAhBRIsSSDaopNFKNjXU9173ZPCGHdEg0VRaHrz6kEvmuOy1Sx8xW8c4amQam8COOw2dCkDe49S27MiIxc6lbHul-gqHc-WIpYT8v5_0vIyvx6UU2cZ1SmfIpZTf4bvlsF-hH1ZvM1w" rel="noreferrer" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.wandertrails.com/c/eJwdjUEKgzAQRU9jlpKZJDousrAFT1DosoxJrAFrJAbs8WuFv3mL_563AAhBRIsSSDaopNFKNjXU9173ZPCGHdEg0VRaHrz6kEvmuOy1Sx8xW8c4amQam8COOw2dCkDe49S27MiIxc6lbHul-gqHc-WIpYT8v5_0vIyvx6UU2cZ1SmfIpZTf4bvlsF-hH1ZvM1w&amp;source=gmail&amp;ust=1531421935748000&amp;usg=AFQjCNHj3VztbtK_4zg-cJ1XjfZZ3LRthA"><img style="margin:0 10px;width:30px;height:30px" src="https://ci6.googleusercontent.com/proxy/dr6402uvHePKbRgvKypi-cZ89tuGh0dZj8KS7Jj1vt3fAnuDQfiq2KTt-vxWh7PsFv8wVqcNlScsHDcMYN8kGs0r2choMpHve1-fiV5v9jirqcibdUiwlrLRFzxwjHABDJns_TTPVUr30zHkcczdWg=s0-d-e1-ft#https://res.cloudinary.com/wandertrails/image/upload/v1479037799/newdesign/mailer/twit.png" class="CToWUd"></a>&nbsp;
																	<a href="http://email.wandertrails.com/c/eJwdjrFugzAURb_GbEHPzzYxgweSClXK1qpz5OAHWHExMlC3f1-30h2uznB0nOEcOVXeIHANDQpQUkBT8_rayU4rvGCrdQ-omIRsF0dpT9aHrR7iZzUbJci22lpwTTvCGSXIsRCFD1RDq6AKZt73dWOiY9iX5Zzrn3jsx4P-FIUMs10WCuV9XMPk8HY5vX69-1Oj3868f95Cx0Q_Hd6Ru88U1vsYYmbiRVTJ-GWMJWyIMU30vSba_sN-Aal7QaI" rel="noreferrer" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.wandertrails.com/c/eJwdjrFugzAURb_GbEHPzzYxgweSClXK1qpz5OAHWHExMlC3f1-30h2uznB0nOEcOVXeIHANDQpQUkBT8_rayU4rvGCrdQ-omIRsF0dpT9aHrR7iZzUbJci22lpwTTvCGSXIsRCFD1RDq6AKZt73dWOiY9iX5Zzrn3jsx4P-FIUMs10WCuV9XMPk8HY5vX69-1Oj3868f95Cx0Q_Hd6Ru88U1vsYYmbiRVTJ-GWMJWyIMU30vSba_sN-Aal7QaI&amp;source=gmail&amp;ust=1531421935748000&amp;usg=AFQjCNH_rhBhNfvBdxp-0dICbcmtoM-31A"><img style="margin:0 10px;width:30px;height:30px" src="https://ci4.googleusercontent.com/proxy/QtSN-yuTy6ySqTqjiowGjb7l3N2-70y7bjhuYNSDRyfesZNCMTSWI0CKFrBvYbyQxZxcbcbCeqysuvH3ncGGWz8TPoWeeE5Q0J6rcyOwT-s8P6MRhpkwtWay8XAFBTPNBzmwSkhIMJNITQkdp7iM=s0-d-e1-ft#https://res.cloudinary.com/wandertrails/image/upload/v1479037799/newdesign/mailer/you.png" class="CToWUd"></a>
																</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td style="background:#ffffff" align="left" bgcolor="#ffffff" width="20" valign="top">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="height:20px!important;line-height:20px!important;font-size:20px!important;color:#ededed!important;height:20px;line-height:20px;font-size:20px;color:#ededed" height="20" align="center" valign="top">.</td>
							</tr>
						</tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>