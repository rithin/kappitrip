<?php echo $this->Html->script('plugins/bootstrap/bootstrap-timepicker.min.js'); ?>
<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_coffee_shops/save";
                 echo $this->Form->create('CoffeeShops', array('id'=>'addCoffeeShop', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Coffee Shop</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new Coffee Shops.</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Coffee Shop Name</label>
                                                
                                                <div class="col-md-9 col-xs-12">   
                                                        <!--<span class="input-group-addon"><span class="fa fa-pencil"></span></span>-->
                                                    
                                                        <?php echo $this->Form->input('shop_name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'shop_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                 </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Brand Name</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php echo $this->Form->input('brand_name',array('type'=>'text','div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'brand_name','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            
                                                
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Opening Time</label>
                                                <div class="col-md-5">
                                                    <div class="input-group bootstrap-timepicker">
                                                     <?php echo $this->Form->input('opening_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'opening_time', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cloging Time</label>
                                                <div class="col-md-5">
                                                    <div class="input-group bootstrap-timepicker">
                                                     <?php echo $this->Form->input('closing_time',array('div'=>false, 'label'=>false, 'class'=>'form-control timepicker', 'id'=>'closing_time', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Contact Number</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('contact_number',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'contact_number','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div> 
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Contact Email</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('contact_email',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'contact_email','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cuisines</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('cuisines',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'cuisines','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div> 
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Average Cost</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('average_cost',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'average_cost','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div> 
                                           
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Address1</label>
                                                <div class="col-md-9 col-xs-12">   
                                                       
                                                    
                                                        <?php echo $this->Form->input('address1',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'address1','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                                                             
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                            
                                            
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Address 2</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                      <?php echo $this->Form->input('address2',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'address2', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>     

                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Location</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                      <?php echo $this->Form->input('location',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'location', 'autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>   
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">City</label>
                                                <div class="col-md-9 col-xs-12">   
                                                       
                                                    
                                                        <?php echo $this->Form->input('city',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'sku','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                                                               
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">State</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                    <?php 
                                                   
                                                    echo $this->Form->input('state_id', array('options'=>$array_states,'div'=>false, 'label'=>false, 
                                                        'class'=>'form-control', 'type'=>'select', 'id' =>'state_id',
                                                        'empty'=>'--Select--')); ?>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Latitude</label>
                                                <div class="col-md-9 col-xs-12">   
                                                       
                                                    
                                                        <?php echo $this->Form->input('latitude',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'latitude','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                                                             
<!--                                                    <span class="help-block">This is sample of text field</span>-->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Longitude</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                   <?php echo $this->Form->input('longitude',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'longitude','autofocus','data-rule-required'=>"true",'error'=>false));?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>  
                                          
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description</label>
                                                <div class="col-md-9 col-xs-12">                                            
                                                <?php echo $this->Form->input('description',array('div'=>false, 'label'=>false, 'class'=>'form-control', 'id'=>'description','autofocus','data-rule-required'=>"true",'error'=>false,'type'=>'textarea')); ?>
<!--                                                    <span class="help-block">Default textarea field</span>-->
                                                </div>
                                            </div>
                                          
                                                                                      
                                            
                                        </div>                                          
                                    </div> <!-- END OF FIRST ROW -->
                            <br/>
                            <div class="row">
                                <div class="col-md-9" style="width:53%;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                         <h3 style="color:red;"><span class="fa fa-download"></span> Dropzone (Drop your property images here.)</h3>
                                            <p style="color:green;">Upload files one by one for better Performance. [Max file size:2MB, Supported Formats: JPEG, JPG, PNG GIF]</p>
                                            <div id="dZUpload" class="dropzone" style="min-height:150px;min-width:">
                                                <div class="dz-default dz-message"></div>
                                            </div>
                                        </div>
                                    </div>   
                                 </div> 
                                <?php if(count($coffeeshopImages)>0) { ?>
                                <div> <ul> <?php foreach($coffeeshopImages as $files) { 
                                    //debug($files);
                                    ?>
                                        <li><img src="<?php echo Configure::read('app_root_path').$files['CoffeeShopImage']['img_path'].'/'.$files['CoffeeShopImage']['resized_img_name']; ?>" width="100" height="100"/>&nbsp;&nbsp; <a href="<?php echo Configure::read('app_root_path'); ?>manage_coffee_shops/deleteFile/<?php echo $files['CoffeeShopImage']['id']; ?>" alt="Delete image"><i class="fa fa-times fa-3x"></i></a></li>
                                    <?php } ?>
                                 </ul>
                                </div>
                                <?php } ?>
                            </div><!-- END OF Second ROW -->

                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>


<script>


$("#addCoffeeShop").validate({
                ignore: [],
                rules: {                                            
                        'data[CoffeeShops][shop_name]': {
                                required: true,
                        },
                        'data[CoffeeShops][opening_time]': {
                                required: true,
                        },
                        'data[CoffeeShops][closing_time]': {
                                required: true,
                        },
                        'data[CoffeeShops][contact_email]': {
                            email: true
                        },
                        'data[CoffeeShops][contact_number]': {
                                required: true,
                        },
                        'data[CoffeeShops][address1]': {
                                required: true,
                        },
                        'data[CoffeeShops][address2]': {
                                required: true,
                        },
                        'data[CoffeeShops][location]': {
                                required: true,
                        },
                        'data[CoffeeShops][city]': {
                                required: true,
                        },
                        'data[CoffeeShops][latitude]': {
                                required: true,
                        },
                        'data[CoffeeShops][longitude]': {
                                required: true,
                        },
                        'data[CoffeeShops][description]': {
                                required: true,
                        },
                        
             },
                
        
        
     });  

</script>
