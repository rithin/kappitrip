 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <table id="datadisplay" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>Coffee Shop Name</th>
                                                <th>Brand</th>
                                                <th>Location</th>
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->


<script>

$(document).ready(function() {

    $('#datadisplay').DataTable( {
        "aProcessing": true,
        "aServerSide": true,
        "ajax": '<?php echo Configure::read('app_root_path'); ?>manage_coffee_shops/ajax_coffee_shop_list',
        "aoColumns": [
            { "data": "coffeeshop_name" },
            { "data": "brand_name" },
            { "data": "city" },
            { "data": "actions" },
           
        ],
        

    } );
} );

</script>