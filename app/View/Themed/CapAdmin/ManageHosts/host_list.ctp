 <!-- START DEFAULT DATATABLE -->
   <div class="panel panel-default">
                <div class="panel-heading">                                
                                    <h3 class="panel-title">Default</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                     <a href="<?php echo Configure::read('app_root_path'); ?>
manage_hosts/add_edit">&nbsp;Add Hosts &nbsp;</a>
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
												<th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php foreach($hosts as $details){
   // debug($room);
	
                                                ?>

                                            <tr>	
                                                <td><?php echo $details['Host']['name']; ?></td>
                                                <td><?php echo $details['Host']['email']; ?></td>
                                                <td><?php echo $details['Host']['phone']; ?></td>   
												
                                                <td>       <a href="<?php echo Configure::read('app_root_path'); ?>/manage_hosts/add_edit/<?php echo $details['Host']['id']; ?>">&nbsp;Edit &nbsp;</a>
                                                <a href="<?php  echo Configure::read('app_root_path'); ?>
manage_hosts/delete/<?php  echo $details['Host']['id']; ?>">&nbsp;Delete &nbsp;</a>                                            </td>
                                            </tr>
                                <?php } ?>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->