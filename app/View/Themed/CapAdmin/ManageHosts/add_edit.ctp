<div class="page-content-wrap">
 <div class="row">
     <div class="col-md-12">
             <?php                         
                 $action = Configure::read('app_root_path')."manage_hosts/save";
                 echo $this->Form->create('Host', array('id'=>'addHost', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'file','class'=>'form-horizontal ', 'novalidate' => true));   
                          //  debug($this->request->data);
                  echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
			 ?>
                   <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Host</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p>Place where you can add new hosts .</p>
                                </div>
                        <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                    
                                                        <?php echo $this->Form->input('name',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'name','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Email id</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                    
                                                        <?php echo $this->Form->input('email',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'email','autofocus','data-rule-required'=>"true",'error'=>false));?>
                                                        
                                                    </div>                                            
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Phone</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                    
                                                        <?php echo $this->Form->input('phone',array('div'=>false, 'label'=>false, 'class'=>'validate[required] form-control', 'id'=>'phone','autofocus','data-rule-required'=>"true",'error'=>false, 'type'=>'number'));?>
                                                        
                                                    </div>                                            
                                                </div>
                                            </div>
                                            
                                          
                                           
                                           
                                                                                 
                                    </div> <!-- END OF FIRST ROW -->
                           
                        </div> <!-- END OF panel-body --> 
                                <div class="panel-footer pull-right">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                     </div>
                             <?php echo $this->Form->end(); ?>
                            
                        </div>
       </div>   
 </div>


<script>


</script>
