<?php
App::import('Model','ProductCategory');
App::import('Model','ProductSubCategory');
App::import('Model','EventProfilePages');
App::import('Model','CartProduct');
App::uses('SessionHelper', 'View/Helper');
App::uses('CookieHelper', 'View/Helper');

class CustomHelper extends AppHelper {
    var $helpers = array('Html','Form','Session');
    public function view_cart($my_cookie){
        $cart_obj=new CartProduct();
        if(isset($my_cookie) && !empty($my_cookie))
        {
            $cookie_id=$my_cookie;
        }
        else
        {
            $cookie_id="";
        }
        if($this->Session->read('Auth.User'))
        {
            $session_data=$this->Session->read('Auth.User');
            $my_user_id=$session_data['id'];
            $my_cond='CartProduct.user_id='.$my_user_id;
        }
        else
        {
            $my_cond='';
        }
        $cart_obj->recursive =2;
        $res_cart=$cart_obj->find('all', array(
            'joins' => array(
                array(
                    'table' => 'products',
                    'alias' => 'Product',
                    'type' => 'INNER',
                    'conditions' => array(
                        'CartProduct.product_id = Product.id'
                    )
                ),
                array(
                    'table' => 'product_images',
                    'alias' => 'Image',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'CartProduct.product_id=Image.product_id'
                    ),
                ),
                array(
                    'table' => 'wish_lists',
                    'alias' => 'WishList',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'CartProduct.wishlist_id=WishList.id'
                    ),
                )
            ),
            'conditions' => array('OR'=>array('CartProduct.browser_id'=>$cookie_id,$my_cond)),
            'fields' => array('CartProduct.*','Product.*','Image.image_name','Image.image_location','WishList.*'),
            'group'=>'Image.product_id'
        ));
        return count($res_cart);
    }
    
    public function get_productcategory()
    {
        $prod_catobj=new ProductCategory();
        return $prod_catobj->find('all',array('conditions'=>array('status_id'=>'1')));
    }
    
    public function get_sub_productcategory($cat)
    {
        $prod_subcatobj=new ProductSubCategory();
        return $prod_subcatobj->find('all',array('conditions'=>array('product_category_id'=>$cat,'ProductSubCategory.status_id'=>'1')));
    }
    public function get_profile_pic(){
        $event_profobj=new EventProfilePages();
        $session_data=$this->Session->read('Auth.User');
        return $event_profobj->find('first',array('fields'=>array('EventProfilePages.profile_pic_name','EventProfilePages.profile_pic_location'),'conditions'=>array('user_id'=>$session_data['id'])));
    }
    
    function encrypt_e($input, $ky) {
	$key = $ky;
	$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
	$input = $this->pkcs5_pad_e($input, $size);
	$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
	$iv = "@@@@&&&&####$$$$";
	mcrypt_generic_init($td, $key, $iv);
	$data = mcrypt_generic($td, $input);
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);
	$data = base64_encode($data);
	return $data;
}

function decrypt_e($crypt, $ky) {

	$crypt = base64_decode($crypt);
	$key = $ky;
	$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
	$iv = "@@@@&&&&####$$$$";
	mcrypt_generic_init($td, $key, $iv);
	$decrypted_data = mdecrypt_generic($td, $crypt);
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);
	$decrypted_data = $this->pkcs5_unpad_e($decrypted_data);
	$decrypted_data = rtrim($decrypted_data);
	return $decrypted_data;
}

function pkcs5_pad_e($text, $blocksize) {
	$pad = $blocksize - (strlen($text) % $blocksize);
	return $text . str_repeat(chr($pad), $pad);
}

function pkcs5_unpad_e($text) {
	$pad = ord($text{strlen($text) - 1});
	if ($pad > strlen($text))
		return false;
	return substr($text, 0, -1 * $pad);
}

function generateSalt_e($length) {
	$random = "";
	srand((double) microtime() * 1000000);

	$data = "AbcDE123IJKLMN67QRSTUVWXYZ";
	$data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
	$data .= "0FGH45OP89";

	for ($i = 0; $i < $length; $i++) {
		$random .= substr($data, (rand() % (strlen($data))), 1);
	}

	return $random;
}

function checkString_e($value) {
	$myvalue = ltrim($value);
	$myvalue = rtrim($myvalue);
	if ($myvalue == 'null')
		$myvalue = '';
	return $myvalue;
}

function getChecksumFromArray($arrayList, $key, $sort=1) {
	if ($sort != 0) {
		ksort($arrayList);
	}
	$str = $this->getArray2Str($arrayList);
	$salt = $this->generateSalt_e(4);
	$finalString = $str . "|" . $salt;
	$hash = hash("sha256", $finalString);
	$hashString = $hash . $salt;
	$checksum = $this->encrypt_e($hashString, $key);
	return $checksum;
}

function verifychecksum_e($arrayList, $key, $checksumvalue) {
	$arrayList = $this->removeCheckSumParam($arrayList);
	ksort($arrayList);
	$str = $this->getArray2Str($arrayList);
	$paytm_hash = $this->decrypt_e($checksumvalue, $key);
	$salt = substr($paytm_hash, -4);

	$finalString = $str . "|" . $salt;

	$website_hash = hash("sha256", $finalString);
	$website_hash .= $salt;

	$validFlag = "FALSE";
	if ($website_hash == $paytm_hash) {
		$validFlag = "TRUE";
	} else {
		$validFlag = "FALSE";
	}
	return $validFlag;
}

function getArray2Str($arrayList) {
	$paramStr = "";
	$flag = 1;
	foreach ($arrayList as $key => $value) {
		if ($flag) {
			$paramStr .= $this->checkString_e($value);
			$flag = 0;
		} else {
			$paramStr .= "|" . $this->checkString_e($value);
		}
	}
	return $paramStr;
}

function redirect2PG($paramList, $key) {
	$hashString = $this->getchecksumFromArray($paramList);
	$checksum = $this->encrypt_e($hashString, $key);
}

function removeCheckSumParam($arrayList) {
	if (isset($arrayList["CHECKSUMHASH"])) {
		unset($arrayList["CHECKSUMHASH"]);
	}
	return $arrayList;
}

function getTxnStatus($requestParamList) {
	return $this->callAPI(PAYTM_STATUS_QUERY_URL, $requestParamList);
}

function initiateTxnRefund($requestParamList) {
	$CHECKSUM = $this->getChecksumFromArray($requestParamList,PAYTM_MERCHANT_KEY,0);
	$requestParamList["CHECKSUM"] = $CHECKSUM;
	return $this->callAPI(PAYTM_REFUND_URL, $requestParamList);
}

function callAPI($apiURL, $requestParamList) {
	$jsonResponse = "";
	$responseParamList = array();
	$JsonData =json_encode($requestParamList);
	$postData = 'JsonData='.urlencode($JsonData);
	$ch = curl_init($apiURL);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                         
	'Content-Type: application/json', 
	'Content-Length: ' . strlen($postData))                                                                       
	);  
	$jsonResponse = curl_exec($ch);   
	$responseParamList = json_decode($jsonResponse,true);
	return $responseParamList;
}
}
?>