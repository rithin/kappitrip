
<!-- Dashboard -->
<div id="dashboard" style="padding-top:0px !important;"	>

	<!-- Navigation
	================================================== -->
	<?php echo $this->element('menu/dashboard_menu'); ?>

	<!-- Navigation / End -->


	<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<!-- Titlebar -->
		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>My Profile</h2>
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Dashboard</a></li>
							<li>My Profile</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>

		<div class="row">
			
			<!-- Profile -->
			<div class="col-lg-6 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					<h4 class="gray">Change Password</h4>
					
					
					<div class="dashboard-list-box-static" style="padding: 1px 30px 30px 30px !important;">
						<!-- Details -->
						<div class="my-profile">
							<?php if($msg=='success')  echo '<label style="color:#c7a17a;">Password changed successfully.</label>'; ?>
							<?php   
							$action = Configure::read('app_root_path')."my_accounts/saveSettings";            
							echo $this->Form->create('User', array('id'=>'change_password', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'POST','class'=>'form-horizontal', 'novalidate' => true,'type'=>'file')); 									
							echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
				
							?>
							<label>Password</label>
							<?php 
								echo $this->Form->input('password',array('div'=>false, 'type'=>'password', 'label'=>false, 'id'=>'password','autofocus','error'=>false,'placeholder'=>'Min 5 charcters and 1 number','mypassword'=>'mypassword'));
                     		?>
							
							<label>Confirm Password</label>
							<?php 
								echo $this->Form->input('confirm_password',array('div'=>false, 'type'=>'password', 'label'=>false, 'id'=>'confirm_password','autofocus','error'=>false,'placeholder'=>'Min 5 charcters and 1 number'));
                     		?>

							<label>Username/Email</label>
							<?php 
								echo $this->Form->input('username',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'username','autofocus','error'=>false,'readonly'=>'readonly','style'=>'background-color: #c7a17a;color: white;font-weight: bold;'));
                     		?>

							
							<button class="button margin-top-50">Save Changes</button>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
			</div>

			<!-- Change Password -->
			<div class="col-lg-6 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					<h4 class="gray">Accounts</h4>
					<div class="dashboard-list-box-static">

						<!-- Change Password -->
						<div class="my-profile">
							<button class="button margin-top-50">Delete Account</button>
						
						</div>

					</div>
				</div>
			</div>
			
			
			
			
			<div class="col-md-12">
				<div class="copyrights"></div>
			</div>
			
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

<script>
	
   $("#change_password").validate({
                ignore: [],
                rules: {                                            
                        'data[User][password]': {
                             required: true,
							 minlength: 6,
							 maxlength:10,
							// mypassword: true
                        },
                        'data[User][confirm_password]': {
                            required: true,
							equalTo : "#password"
                        },
                        'data[User][user_name]': {
                                required: true,
                                email: true
                        },     
             	},
	   		 messages: {
                'data[User][password]': " Please enter password(Min 5 characters and 1 number).",
               'data[User][confirm_password]': " Pasword and confirm passed are not same."
            }
                
    
        
     });   
$.validator.addMethod('mypassword', function(value, element) {
        return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
    },
    'Password must contain at least one numeric and one alphabetic character.');

</script>