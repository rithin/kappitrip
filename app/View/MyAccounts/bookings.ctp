<!-- Dashboard -->
<div id="dashboard">
	<?php echo $this->element('menu/dashboard_menu'); ?>


	<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<!-- Titlebar -->
		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Bookings</h2>
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Dashboard</a></li>
							<li>Bookings</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		
		<div class="row">
			
			<!-- Listings -->
			<div class="col-lg-12 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					
					<!-- Booking Requests Filters  -->
					<div class="booking-requests-filter">
					</div>

					
					<h4>Booking Requests</h4>
					<ul>

						<?php foreach($bookingDetails as $details){ 
						   $headcount_string=""; $host_name=""; $contact_host_no="";$booking_status=""; $show_cancel_button='No'; $today=date('Y-m-d');$cancellation_policy="";
						   $booking_date_details="From ". $details['OccupancyDetail']['check_in_date']." at ". $details['Homestay']['check_in_time']." to ". $details['OccupancyDetail']['check_out_date']." at ". $details['Homestay']['check_out_time'];
							if($details['BookingPaymentResponse']['homestay_id']!="") {
								$headcount_string=$details['BookingOrderDetail']['no_of_adults']." Adult(s) ";
								if($details['BookingOrderDetail']['no_of_children']>0)
									$headcount_string=$headcount_string.", ".$details['BookingOrderDetail']['no_of_children']." Children ";
								if($details['BookingOrderDetail']['no_of_infants']>0)
									$headcount_string=$headcount_string.", ".$details['BookingOrderDetail']['no_of_infants']." Infant(s) ";
								
								$host_name=$details['Homestay']['owner_name'];
								$latitude=$details['Homestay']['latitude'];
		 						$longitude=$details['Homestay']['longitude'];
								$contact_host_no=$details['Homestay']['telephone'];	
								if($today < $details['OccupancyDetail']['check_in_date'] ){
									$show_cancel_button='Yes';
								}
								$cancellation_policy=$details['Homestay']['cancellation_policy'];
								$action = Configure::read('app_root_path')."my_accounts/cancel_stay_booking";       
		 	
							}else if($details['BookingPaymentResponse']['experience_id']!="") {
								$host_name=$details['Experience']['hosted_by'];
								$latitude=$details['Experience']['latitude'];
		 						$longitude=$details['Experience']['longitude'];
								$contact_host_no=$details['Experience']['host_contact_no'];
								$booking_date_details=$details['ExperienceBookingDetail']['slot_date'] ." From ".$details['ExperienceSlot']['slot_start_time']." to ".$details['ExperienceSlot']['slot_end_time'];
								
								if($today < $details['ExperienceBookingDetail']['slot_date'] ){
									$show_cancel_button='Yes';
								}
								$cancellation_policy=$details['Experience']['cancellation_policy'];
								$action = Configure::read('app_root_path')."my_accounts/cancel_experience_booking";       
						   }
						  if($details['BookingPaymentResponse']['status']==14) {
							$booking_status="approved-booking";
						  }else{
							$booking_status="canceled-booking";  
						  }
						 $google_maps="https://www.maps.google.com/?&q=$latitude,$longitude";
						?>
						<?php //echo $details['BookingPaymentResponse']['order_id']?>
						<div id="small-dialog_<?php echo $details['BookingPaymentResponse']['order_id'];?>" class="zoom-anim-dialog mfp-hide cancel_dialogue">
							<div class="small-dialog-header">
								<h3>Booking Cancellation</h3>
							</div>
						<?php	
	
							     
							echo $this->Form->create('BookingPaymentResponse', array('id'=>'cancel_booking', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'POST','class'=>'form-horizontal', 'novalidate' => true)); 
							echo $this->Form->input('order_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false,'value'=>$details['BookingPaymentResponse']['order_id']));
						?>
							<div class="message-reply margin-top-0">
								<h4>Cancellation Policy:</h4>
								<div><?php echo $cancellation_policy; ?></div>
								<input type="checkbox" name="cancel_agreement" class="cancel_agree_checkbox" onchange="termsAgree(<?php echo $details['BookingPaymentResponse']['order_id'];?>)" id="term_checkbox_<?php echo $details['BookingPaymentResponse']['order_id'];?>"> Please check the agreement to continue the cancellation.
								<button class="button" disabled='disabled' id="btn_cancel_<?php echo $details['BookingPaymentResponse']['order_id'];?>" >Cancel Booking</button>
							</div>
						<?php echo $this->Form->end(); ?>
					   </div>
						<li class="<?php echo $booking_status; ?>">
							<div class="list-box-listing bookings">
								<div class="list-box-listing-img"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&s=120" alt=""></div>
								<div class="list-box-listing-content">
									<div class="inner">
										<h3> <?php if($details['BookingPaymentResponse']['homestay_id']!="") { ?>
                                                   <?php echo $details['Homestay']['name']; ?>
                                                <?php }else if($details['BookingPaymentResponse']['experience_id']!="") {?>
                                                   <?php echo $details['Experience']['name']; ?>
                                                <?php } ?>
											
										<span class="booking-status"><?php if($details['BookingPaymentResponse']['status']==14) echo "Confirmed"; else echo "Cancelled"; ?></span>
										</h3>

										<div class="inner-booking-list">
											<h5>Booking Date:</h5>
											<ul class="booking-list">
												<li class="highlighted"><?php echo $booking_date_details; ?></li>
											</ul>
										</div>
													
										<div class="inner-booking-list">
											<h5>Booking Details:</h5>
											<ul class="booking-list">
												<li class="highlighted"><?php echo $headcount_string; ?></li>
											</ul>
										</div>		
										<div class="inner-booking-list">
											<h5>Amount Paid:</h5>
											<ul class="booking-list">
												<li class="highlighted"> ₹ <?php echo $details['BookingPaymentResponse']['amount']; ?></li>
											</ul>
										</div>		
										<div class="inner-booking-list">
											<h5>Other Details:</h5>
											<ul class="booking-list">
												<li>Hosted By <?php echo $host_name; ?></li>
												<?php if($details['BookingPaymentResponse']['status']==14) { ?>
												<li><a href="<?php echo $google_maps; ?>" class="button gray reject" target="_blank" style="margin-bottom: -10px;"><i class="im im-icon-Map-Marker2"></i> View Map</a></li>
												<li>Contact:</li>
												<?php } ?>
											</ul>
										</div>


									</div>
								</div>
							</div>
							<?php if($details['BookingPaymentResponse']['status']==14  && $show_cancel_button=='Yes') { ?>
								<div class="buttons-to-right">
									<a href="#small-dialog_<?php echo $details['BookingPaymentResponse']['order_id']?>" class="button gray reject popup-with-zoom-anim"><i class="sl sl-icon-close"></i> Cancel</a>
								</div>
							<?php } ?>
						</li>
						<?php } ?>   
					
					</ul>
				</div>
			</div>


			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights"></div>
			</div>
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->
<style>
	.cancel_dialogue{
		    background: #fff;
    padding: 40px;
    padding-top: 0;
    text-align: left;
    max-width: 610px;
    margin: 40px auto;
    position: relative;
    box-sizing: border-box;
    border-radius: 4px;
	}
	.small-dialog-header {
		margin-bottom: 1px !important; 
	}
	.cancel_agree_checkbox{
		background: #af6161 !important;
    border: 1px solid #af6161 !important;
    border-radius: 2px !important;
    height: 18px !important;
    width: 18px !important;
    display: inline-block !important;
    text-align: center !important;
    overflow: hidden !important;
	}
</style>
<script>
function termsAgree(order_id){
	var checkbox_name='term_checkbox_'+order_id;
	if($('#'+checkbox_name).prop('checked')){
		$('#btn_cancel_'+order_id).prop('disabled',false);
	}else
		$('#btn_cancel_'+order_id).prop('disabled',true);
		
}

</script>