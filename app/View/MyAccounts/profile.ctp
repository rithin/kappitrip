
<!-- Dashboard -->
<div id="dashboard" style="padding-top:0px !important;"	>

	<!-- Navigation
	================================================== -->
	<?php echo $this->element('menu/dashboard_menu'); ?>

	<!-- Navigation / End -->


	<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<!-- Titlebar -->
		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>My Profile</h2>
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Dashboard</a></li>
							<li>My Profile</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>

		<div class="row">
			<?php   
             	$action = Configure::read('app_root_path')."my_accounts/saveProfile";            
				echo $this->Form->create('User', array('id'=>'user_profile', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'POST','class'=>'form-horizontal', 'novalidate' => true,'type'=>'file')); 
						//, 'onsubmit'=>'return doValidate()'
					echo $this->Form->input('id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'id','autofocus','error'=>false));
					echo $this->Form->input('UserDetail.id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'UserDetail_id','autofocus','error'=>false));
				?>
			<!-- Profile -->
			<div class="col-lg-6 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					<h4 class="gray">Personal Details</h4>
					<div class="dashboard-list-box-static">
			
						<!-- Avatar -->
						<div class="edit-profile-photo">
							<img src="<?php echo Configure::read('app_root_path').$this->request->data['UserDetail']['profile_pic_path']."/".$this->request->data['UserDetail']['profile_pic_name'] ?>" alt="Profile Picture">
							<div class="change-photo-btn">
								<div class="photoUpload">
								    <span id="profile_pic_span"><i class="fa fa-upload"></i> Upload Photo</span>
<!--								    <input type="file" class="upload" />-->
									<?php 
										echo $this->Form->input('profile_pic',array('div'=>false, 'type'=>'file', 'label'=>false,'class'=>'upload', 'id'=>'file-upload','autofocus','error'=>false));
                     			?>
								</div>
							</div>
						</div>
	
						<!-- Details -->
						<div class="my-profile">

							<label>First Name</label>
							<?php 
								echo $this->Form->input('first_name',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'first_name','autofocus','error'=>false));
                     		?>
							
							<label>Last Name</label>
							<?php 
								echo $this->Form->input('last_name',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'last_name','autofocus','error'=>false));
                     		?>

							<label>Phone</label>
							<?php 
								echo $this->Form->input('contact_number',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'contact_number','autofocus','error'=>false));
                     		?>

							<label>Email</label>
							<?php 
								echo $this->Form->input('email_id',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'email_id','autofocus','error'=>false));
                     		?>

							<label>Date of Birth</label>
							<?php 
								echo $this->Form->input('dob',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'dob','autofocus','error'=>false));
                     		?>
						</div>
	
						

					</div>
				</div>
			</div>

			<!-- Change Password -->
			<div class="col-lg-6 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					<h4 class="gray">Billing Address</h4>
					<div class="dashboard-list-box-static">

						<!-- Change Password -->
						<div class="my-profile">

							<label>Address Line 1</label>
							<?php 
								echo $this->Form->input('UserDetail.address1',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'address1','autofocus','error'=>false));
                     		?>
							
							<label>Address Line 2</label>
							<?php 
								echo $this->Form->input('UserDetail.address2',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'address2','autofocus','error'=>false));
                     		?>

							<label>City</label>
							<?php 
								echo $this->Form->input('UserDetail.city',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'city','autofocus','error'=>false));
                     		?>

							<label>State</label>
							<?php 
								 echo $this->Form->input('UserDetail.state_id', array('options'=>$array_states,'div'=>false, 'label'=>false, 'type'=>'select', 'id' =>'state_id','empty'=>'--Select--')); 
                     		?>

							<label>Country</label>
							<?php 
								echo $this->Form->input('UserDetail.country',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'country','autofocus','error'=>false));
                     		?>
						</div>

					</div>
				</div>
			</div>
			
			<button class="button margin-top-50">Save Changes</button>
			<?php echo $this->Form->end(); ?>
			
			
			<div class="col-md-12">
				<div class="copyrights"></div>
			</div>
			
		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->

<script>
	
   $("#user_profile").validate({
                ignore: [],
                rules: {                                            
                        'data[User][first_name]': {
                                required: true,
                        },
                        'data[User][last_name]': {
                                required: true,
                        },
                        'data[User][email_id]': {
                                required: true,
                                email: true
                        },                       
                        'data[User][contact_number]': {
                                required: true,
                        },
                        'data[User][address1]': {
                                required: true,
                        },
                        'data[UserDetail][address2]': {
                                required: true,
                        },
                        'data[UserDetail][city]': {
                                required: true,
                        },
                        'data[UserDetail][state_id]': {
                                required: true,
                        },
                        'data[UserDetail][country]': {
                                required: true,
                        },
                        
             },
                
        
        
     });   
$('#file-upload').change(function() {
  var i = $(this).prev('label').clone();
  var file = $('#file-upload')[0].files[0].name;
  $('#profile_pic_span').html(file);
});

</script>