<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Invoice</title>
    <?php echo $this->Html->css('invoice.css'); ?>
</head> 

<body>

<!-- Print Button -->
<a href="javascript:window.print()" class="print-button">Print this invoice</a>

<!-- Invoice -->
<div id="invoice">

	<!-- Header -->
	<div class="row">
		<div class="col-md-6">
			<div id="logo"><img src="images/logo.png" alt=""></div>
		</div>

		<div class="col-md-6">	

			<p id="details">
				<strong>Order:</strong> #<?php echo $BookingOrderDetailArr[0]['BookingPaymentResponse']['order_id']; ?> <br>
				<strong>Issued:</strong> <?php echo date('d-m-Y'); ?> <br>
			
			</p>
		</div>
	</div>


	<!-- Client & Supplier -->
	<div class="row">
		<div class="col-md-12">
			<h2>Invoice</h2>
		</div>

		<div class="col-md-4">	
			<strong class="margin-bottom-5">Supplier</strong>
			<p>
				CoorgExpress. <br>
				#203, Sapthagiriri Springs <br>
				BTS Layout, Arekere <br>
                Banglore - 560076 <br>
			</p>
		</div>

		<div class="col-md-4">	
			<strong class="margin-bottom-5">Customer</strong>
			<p>
				<?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_name']); ?><br>
				<?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_address']); ?> <br>
				<?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_city']); ?>, 
                <?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_zip']); ?> <br>
                <?php echo ucfirst($BookingOrderDetailArr[0]['BookingPaymentResponse']['billing_country']); ?> <br>
			</p>
		</div>
        
        <div class="col-md-4">	
			<strong class="margin-bottom-5">Accomadation</strong>
			<p>
				<?php echo ucfirst($HomestayDetailArr['Homestay']['name']); ?><br>
				<?php echo ucfirst($HomestayDetailArr['Homestay']['address']); ?> <br>
				<?php echo ucfirst($HomestayDetailArr['Location']['name']); ?> <br>
                <?php echo "Tel:   ". ucfirst($HomestayDetailArr['Homestay']['telephone']); ?> <br>
                <?php //echo ucfirst($HomestayDetailArr['Homestay']['address']); ?> <br>
			</p>
		</div>
	</div>


	<!-- Invoice -->
	<div class="row">
		<div class="col-md-12">
			<table class="margin-top-20">
				<tr>
					<th>Homestay Name</th>
					<th>No of Days</th>
					<th>Unit Price</th>
					<th>Start Date</th>
                    <th>End Date</th>
					<th>Total</th>
				</tr>
        <?php  
            $grand_total=0;	$total_amount_paid=0;		
            foreach($BookingOrderDetailArr as $item) {
           		$grand_total +=($item['BookingOrderDetail']['no_of_days'] * $item['BookingOrderDetail']['unit_price']);
				$total_amount_paid+=($item['BookingOrderDetail']['total_room_charge'] - $item['BookingOrderDetail']['discount_amount']);
				$discount_amount=$item['BookingOrderDetail']['discount_amount'];
				$applied_coupon=$item['BookingOrderDetail']['applied_coupon'];
          ?>
				<tr>
					<td><?php echo $HomestayDetailArr['Homestay']['name']; ?></td> 
					<td><?php echo $BookingOrderDetailArr[0]['BookingOrderDetail']['no_of_days'];?></td>
					<td>₹ <?php echo number_format($item['BookingOrderDetail']['unit_price']); ?></td>
					<td><?php echo $OccupancyDetailArr['OccupancyDetail']['check_in_date']; ?></td>
                    <td><?php echo $OccupancyDetailArr['OccupancyDetail']['check_out_date']; ?></td>
					<td>₹ <?php echo number_format($item['BookingOrderDetail']['total_room_charge']); ?></td>
				</tr>
        <?php } ?>         
        
			</table>
		</div>
		
		<div class="col-md-5 col-md-offset-7">	
			<table id="totals">
				<tr>
					<th>Grand Total </th> 
					<th><span>₹ <?php echo number_format($grand_total); ?></span></th>
				</tr>
				
				<?php if($discount_amount!=""){ ?>					
				<tr>
					<th>Discount <small style="color:green;">Coupon(<?php echo  $applied_coupon; ?>)</small> </th> 
					<th><span>- ₹ <?php echo number_format($discount_amount); ?></span></th>
				</tr>
				<?php } ?>
				<tr>
					<th>Total Amount Paid</th> 
					<th><span>₹ <?php echo number_format($total_amount_paid); ?></span></th>
				</tr>
			</table>
		</div>
	</div>


	<!-- Footer -->
	<div class="row">
		<div class="col-md-12">
			<ul id="footer">
				<li><span>www.coorgexpress.com</span></li>
				<li>info@coorgexpress.com</li>
<!--				<li>(123) 123-456</li>-->
			</ul>
		</div>
	</div>
		
</div>


</body>
</html>
