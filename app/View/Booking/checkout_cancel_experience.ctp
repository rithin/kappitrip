

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Purchase Processed</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>/">Home</a></li>
						<li>Purchase Processed</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<div class="booking-confirmation-page">
				<i class="fa fa-exclamation-circle"></i>
				<h2 class="margin-top-30">Sorry, Payment has been failed!</h2>
				<p>We are deeply regret for the payment failure. We are extremely happy to serve you better. Please reach us on 
				<?php echo Configure::read('enquiry_phone'); ?>
				</p>
<!--				<a href="<?php //echo Configure::read('app_root_path'); ?>booking/booking_invoice/<?php // echo base64_encode($order_id)."/".base64_encode($experience_id); ?>" class="button margin-top-30">View Invoice</a>-->
			</div>

		</div>
	</div>
</div>
<!-- Container / End -->
