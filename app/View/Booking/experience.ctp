

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Booking</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>listing/experience_list">Listing Experience</a></li>
						<li>Booking</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">
	<div class="row">

		<!-- Content
		================================================== -->
		<div class="col-lg-8 col-md-8 padding-right-30">
         <?php   
               $action = Configure::read('app_root_path')."booking/confirm_experience_booking";
            
               echo $this->Form->create('ExperienceBookingDetail', array('id'=>'dobook', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'POST','class'=>'form-horizontal ', 'novalidate' => true, 'onsubmit'=>'return doConfirmPayment()'));              
               
               echo $this->Form->input('tid',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'tid','autofocus','error'=>false,'value'=>strtotime(date('Y-m-d H:i:s'))));
            
               echo $this->Form->input('merchant_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'merchant_id','autofocus','error'=>false,'value'=>4538));
            
               echo $this->Form->input('order_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'order_id','autofocus','error'=>false,'value'=>strtotime(date('Y-m-d H:i:s'))));  
                
               echo $this->Form->input('currency',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'currency','autofocus','error'=>false,'value'=>'INR')); 
            
               echo $this->Form->input('amount',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'amount','autofocus','error'=>false,'value'=>$grand_total));
            
               echo $this->Form->input('language',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'language','autofocus','error'=>false,'value'=>'EN'));
            
               echo $this->Form->input('redirect_url',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'redirect_url','autofocus','error'=>false,'value'=>Configure::read('app_root_path').'booking/checkout_response_experience'));
            
               echo $this->Form->input('cancel_url',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'cancel_url','autofocus','error'=>false,'value'=>Configure::read('app_root_path').'booking/checkout_cancel_experience'));
            
               echo $this->Form->input('ExperienceBookingDetail.date',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'ExperienceBookingDetail.date','autofocus','error'=>false,'value'=>$date));               
            
               echo $this->Form->input('ExperienceBookingDetail.unit_price',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'ExperienceBookingDetail.unit_price','autofocus','error'=>false,'value'=>$unit_price));
                
               echo $this->Form->input('ExperienceBookingDetail.no_of_guest',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'ExperienceBookingDetail.no_of_guest','autofocus','error'=>false,'value'=>$no_of_guest));  
            
               echo $this->Form->input('ExperienceBookingDetail.grand_total',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'ExperienceBookingDetail.grand_total','autofocus','error'=>false,'value'=>$grand_total));
            
               echo $this->Form->input('ExperienceBookingDetail.slot_start_time',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'ExperienceBookingDetail.slot_start_time','autofocus','error'=>false,'value'=>$slot_start_time));  
            
               echo $this->Form->input('ExperienceBookingDetail.slot_end_time',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'ExperienceBookingDetail.slot_end_time','autofocus','error'=>false,'value'=>$slot_end_time));  
            
               echo $this->Form->input('ExperienceBookingDetail.experience_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'ExperienceBookingDetail.experience_id','autofocus','error'=>false,'value'=>$experience_id)); 
            
               echo $this->Form->input('ExperienceBookingDetail.experience_slot_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'ExperienceBookingDetail.experience_slot_id','autofocus','error'=>false, 'value'=>$experience_slot_id)); 
                
        ?>

			<h3 class="margin-top-0 margin-bottom-30">Billing Details</h3>

			<div class="row">

				<div class="col-md-6">
					<label>Billing Name</label>
                    <?php 
					 echo $this->Form->input('billing_name',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_name','autofocus','error'=>false));
                     ?>
				</div>
				

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>E-Mail Address</label>
						<?php 
					 echo $this->Form->input('billing_email',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_email','autofocus','error'=>false));
                     ?>
						<i class="im im-icon-Mail"></i>
					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Phone</label>
						<?php 
					 echo $this->Form->input('billing_tel',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_tel','autofocus','error'=>false));
                     ?>
						<i class="im im-icon-Phone-2"></i>
					</div>
				</div>
                
                
                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Address </label>
						<?php 
					 echo $this->Form->input('billing_address',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_address','autofocus','error'=>false));
                     ?>
<!--						<i class="im im-icon-Mail"></i>-->
					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Postcode </label>
						<?php 
					 echo $this->Form->input('billing_zip',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_zip','autofocus','error'=>false));
                     ?>
<!--						<i class="im im-icon-Phone-2"></i>-->
					</div>
				</div>
                
                   
                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>City</label>
						<?php 
					 	echo $this->Form->input('billing_city',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_city','autofocus','error'=>false));
                     ?>
<!--						<i class="im im-icon-Mail"></i>-->
					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>State</label>
						<?php 
					 echo $this->Form->input('billing_state',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_state','autofocus','error'=>false));
                     ?>
<!--						<i class="im im-icon-Phone-2"></i>-->
					</div>
				</div>
                
                <div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Country</label>
						<?php 
					 echo $this->Form->input('billing_country',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'billing_country','autofocus','error'=>false));
                     ?>
<!--						<i class="im im-icon-Phone-2"></i>-->
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="input-with-icon medium-icons">
						<label>Special Request </label>
						<?php 
					 echo $this->Form->input('special_request',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'special_request','type'=>'textarea','autofocus','error'=>false));
                     ?>
<!--						<i class="im im-icon-Mail"></i>-->
					</div>
				</div>

			</div>


			<h3 class="margin-top-55 margin-bottom-30">Payment Method</h3>

			<!-- Payment Methods Accordion -->
			<div class="payment">

<!--
				<div class="payment-tab payment-tab-active">
					<div class="payment-tab-trigger">
						<input checked id="paypal" name="cardType" type="radio" value="paypal">
						<label for="paypal">PayPal</label>
						<img class="payment-logo paypal" src="https://i.imgur.com/ApBxkXU.png" alt="">
					</div>

					<div class="payment-tab-content">
						<p>You will be redirected to PayPal to complete payment.</p>
					</div>
				</div>
-->


				<div class="payment-tab">
					<div class="payment-tab-trigger">
						<input type="radio" name="cardType" id="creditCart" checked="checked" value="creditCard">
						<label for="creditCart">Credit / Debit Card / Net Banking (Powered By CCAvenue)</label>
						<img class="payment-logo" src="https://i.imgur.com/IHEKLgm.png" alt="">
					</div>
<!--

					<div class="payment-tab-content">
						<div class="row">

							<div class="col-md-6">
								<div class="card-label">
									<label for="nameOnCard">Name on Card</label>
									<input id="nameOnCard" name="nameOnCard" required type="text">
								</div>
							</div>

							<div class="col-md-6">
								<div class="card-label">
									<label for="cardNumber">Card Number</label>
									<input id="cardNumber" name="cardNumber" placeholder="1234  5678  9876  5432" required type="text">
								</div>
							</div>

							<div class="col-md-4">
								<div class="card-label">
									<label for="expirynDate">Expiry Month</label>
									<input id="expiryDate" placeholder="MM" required type="text">
								</div>
							</div>

							<div class="col-md-4">
								<div class="card-label">
									<label for="expiryDate">Expiry Year</label>
									<input id="expirynDate" placeholder="YY" required type="text">
								</div>
							</div>

							<div class="col-md-4">
								<div class="card-label">
									<label for="cvv">CVV</label>
									<input id="cvv" required type="text">
								</div>
							</div>

						</div>
					</div>
                    
-->
				</div>

			</div>
			<!-- Payment Methods Accordion / End -->
	
<!--			<a href="pages-booking-confirmation.html" class="button booking-confirmation-btn margin-top-40 margin-bottom-65">Confirm and Pay</a>-->
            <input type="submit" class="button booking-confirmation-btn margin-top-40 margin-bottom-65" name="btn_booking" value="Confirm and Pay" />
            <?php echo $this->Form->end(); ?> 
		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-0 margin-bottom-60">

			<!-- Booking Summary -->
			<div class="listing-item-container compact order-summary-widget">
				<div class="listing-item">
					<div class="listing-slider mfp-gallery-container margin-bottom-0">
                           <?php 
                            if(count($experienceDetails['ExperienceFile'] )>0){
                                foreach($experienceDetails['ExperienceFile'] as $files) { 
                               // debug($files);
                                if(is_array($files)){
                                    $imageUrl = Configure::read('app_root_path').$files['file_path'].'/'.$files['file_name'];
                                ?>
                                <a href="<?php echo $imageUrl;?>" data-background-image="<?php echo $imageUrl;?>" class="item mfp-gallery" title="<?php echo  $files['file_name']; ?>"></a>
                                <?php
                                        }
                                  } 
                            }
                           ?>
                    </div>

					<div class="listing-item-content">
<!--						<div class="numerical-rating" data-rating="<?php //echo $experienceDetails['Experience']['rating'];?>"></div>-->
						<h3><?php echo $experienceDetails['Experience']['name'];?></h3>
					</div>
				</div>
			</div>
			<div class="boxed-widget opening-hours summary margin-top-0">
				<h3><i class="fa fa-calendar-check-o"></i> Booking Summary</h3>
				<ul>  
					<li>Date <span><?php echo $date ."  <i class='fa fa-fw'>&#xf061;</i> ". $date; ?></span></li>
					<li>Start Time <span><?php echo $slot_start_time; ?></span></li>
                    <li>End Time <span><?php echo $slot_end_time; ?></span></li>
					<li>Guests <span><?php echo $no_of_guest; ?></span></li>
                    <li class="total-costs"><i class="fa fa-fw fa-1x">&#xf156;</i><?php echo number_format((float)$unit_price, 2, '.', ',')." X ".$no_of_guest." guest(s)"; ?><span><i class="fa fa-fw fa-1x">&#xf156;</i><?php echo number_format((float)$grand_total, 2, '.', ','); ?></span> </li>
                  
					<li class="total-costs">Grand Total(INR)<span class="finalAmount"><i class="fa fa-fw fa-1x">&#xf156;</i><?php echo number_format((float)($grand_total), 2, '.', ','); ?></span> </li>
				</ul>

			</div>
			<!-- Booking Summary / End -->

		</div>

	</div>
</div>
<!-- Container / End -->
<style>

    .summary{
        word-wrap: break-word !important;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    }
    .finalAmount{
            font-size: 20px;
        color: black !important;
    }
</style>


<script>
$(document).ready(function () {

	if($('#logged_in_user_id').val()==0){
		 $.magnificPopup.open({
				  items: {
					src: '#sign-in-dialog'
				  	},
				    type: 'inline',
			 		closeOnBgClick:false,
    				enableEscapeKey: false
				});	
		console.log('here not logged');
	}
	
	$("#sign-in-dialog").mouseleave(function(){
		if($('#logged_in_user_id').val()==0){
			 $.magnificPopup.open({
					  items: {
						src: '#sign-in-dialog'
					  },
					  type: 'inline',
			 		closeOnBgClick:false,
    				enableEscapeKey: false
			 });	  
			console.log('here mouseup');
		}
    
    
	}); 
});
	
function redeemCoupon(){
	var coupon_code_customer=$('#coupon_code_customer').val();
	var grand_total_for_room=$('#grand_total_for_room').val();
	var cleaning_charges=$('#cleaning_charges').val();
	var homestay_id=$('#homestay_id').val();
	var fltrString={"coupon_code_customer": coupon_code_customer,"grand_total_for_room":grand_total_for_room, "cleaning_charges":cleaning_charges,"homestay_id":homestay_id};    
	 
    var couponDataJson=JSON.stringify(fltrString);
	$.ajax
     ({
         type: "POST",
         url: "<?php echo Configure::read('app_root_path'); ?>booking/applyCoupon",
         data: "couponDataJson="+couponDataJson,
         success: function(response)
          {
			  if(response!='error'){
				//  alert(response);
				  var objResponse = JSON.parse(response);
				  $('#discount_display').show();
				  $('#discount_display_amount').html(' - '+' <i class="fa fa-fw fa-1x">&#xf156;</i>'+ objResponse.discount_amount);
				  $('#applied_coupon').val(objResponse.coupon_code);
				  $('#discount_amount').val(objResponse.discount_amount);
				  var finalamount= objResponse.final_discounted_amount;				  
				  $('#finalamount').html('<i class="fa fa-fw fa-1x">&#xf156;</i>'+finalamount);
				  
				  $('#coupon_code_customer').attr('readonly','true');
				  $('#successCouponMsg').css('color','green');
				  $('#successCouponMsg').css('font-style','oblique');				  
				  $('#successCouponMsg').html('  Coupon applied successfully..! ');
				  $('#couponRedeemBtn').hide();
			  }else{
				  $('#successCouponMsg').css('color','red');
				  $('#successCouponMsg').css('font-style','oblique');				  
				  $('#successCouponMsg').html('   Invalid coupon code. Please try a valid coupon. ');
			  }
			 // console.log(response);
		  }
	 });
	 
 }
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}
function doConfirmPayment(){	
	var flag =true;  
	$('#billing_name').css('border','1px solid #e6e6e6');
	$('#billing_email').css('border','1px solid #e6e6e6');
	$('#billing_tel').css('border','1px solid #e6e6e6');
	$('#billing_address').css('border','1px solid #e6e6e6');
	$('#billing_city').css('border','1px solid #e6e6e6');
	$('#billing_state').css('border','1px solid #e6e6e6'); 
	var contact_email=$('#billing_email').val();
	if($('#billing_name').val()==""){
		flag =false;
		$('#billing_name').css('border','1px solid red');		
	}
	else if($('#billing_email').val()=="" || validateEmail(contact_email)==false ){
		flag =false;
		$('#billing_email').css('border','1px solid red');
	}
	else if($('#billing_tel').val()==""){
		flag =false;
		$('#billing_tel').css('border','1px solid red');
	}
	else if($('#billing_address').val()==""){
		flag =false;
		$('#billing_address').css('border','1px solid red');
	}
	else if($('#billing_city').val()==""){
		flag =false;
		$('#billing_city').css('border','1px solid red');
	}
	else if($('#billing_state').val()==""){
		flag =false;
		$('#billing_state').css('border','1px solid red');
	}else if($('#logged_in_user_id').val()==0){
		flag =false;
		$.magnificPopup.open({
			items: {
				src: '#sign-in-dialog'
			},
			type: 'inline',
			closeOnBgClick:false,
			enableEscapeKey: false
		});	  
			
	}
	else{
		flag=true;
	}
	console.log(flag);
	return flag;
}
</script>

