
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

<?php echo $this->Html->css('daterangepicker.min.css'); ?>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js" type="text/javascript"></script>
<?php echo $this->Html->script('longbill_calender/jquery.daterangepicker.js'); ?>

<!-- Slider
================================================== -->
<div class="listing-slider mfp-gallery-container margin-bottom-0">
   <?php 
	//debug($experienceDetails['ExperienceFile']);
    if(count($cofeeShopImages)>0){
        foreach($cofeeShopImages as $files) { 
        //debug($files);
        if(is_array($files)){
            $imageUrl = Configure::read('app_root_path').$files['CoffeeShopImage']['img_path'].'/'.$files['CoffeeShopImage']['resized_img_name'];
        ?>
        <a href="<?php echo $imageUrl;?>" data-background-image="<?php echo $imageUrl;?>" class="item mfp-gallery" title="coffe shop images"></a>
        <?php
                }
          } 
    }
   ?>
	
</div>

<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
		<div class="col-lg-8 col-md-8 padding-right-30">

			<!-- Titlebar -->
			<div id="titlebar" class="listing-titlebar">
				<div class="listing-titlebar-title">
                        <h2><?php echo $coffee_shop_details['CoffeeShop']['shop_name'];?></b></span></h2>
					<span>
						<a href="#listing-location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							<?php echo $coffee_shop_details['CoffeeShop']['location'];?>
						</a>
					</span>
					<div class="star-rating" data-rating="<?php echo $coffee_shop_details['CoffeeShop']['rating'];?>">
<!--						<div class="rating-counter"><a href="#listing-reviews">(31 reviews)</a></div>-->
					</div>
                    
                    
                    
				</div>
			</div>

			<!-- Listing Nav -->
			<div id="listing-nav" class="listing-nav-container">
				<ul class="listing-nav">
					<li><a href="#listing-overview" class="active">Overview</a></li>
					
					<li><a href="#listing-location">Location</a></li>
					
				</ul>
			</div>
			
			<!-- Overview -->
			<div id="listing-overview" class="listing-section">
                            <p>
                                <?php echo $coffee_shop_details['CoffeeShop']['description']; ?>
                            </p>
                            
                            <p>
                                Cuisines : <?php echo $coffee_shop_details['CoffeeShop']['cuisines']; ?>
                            </p>
                            
                            <p>
                                Average Cost : <?php echo $coffee_shop_details['CoffeeShop']['average_cost'].' INR'; ?>
                            </p>
				
			</div>


			
			
		
			<!-- Location -->
			<div id="listing-location" class="listing-section">
				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Location</h3>

				<div id="singleListingMap-container">
					<div id="singleListingMap" data-latitude="<?php echo $coffee_shop_details['CoffeeShop']['latitude'];?>" data-longitude="<?php echo $coffee_shop_details['CoffeeShop']['longitude'];?>" data-map-icon="im im-icon-Running-Shoes"></div>
					<a href="#" id="streetView">Street View</a>
				</div>
			</div>
			




	</div>

        

		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-75 sticky">
			
                    
                    
                    
                        <!-- Opening Hours -->
			<div class="boxed-widget opening-hours margin-top-35">
                            
                                <?php 
                                $store_opening_time =  date('H:i', strtotime($coffee_shop_details['CoffeeShop']['opening_time']));
                                $store_closing_time =  date('H:i', strtotime($coffee_shop_details['CoffeeShop']['closing_time']));
                                                                
                                $current_time = date('H:i');
                                                        
                                                                
                                if($store_opening_time<$current_time && $store_closing_time>$current_time){
                                ?> 
                            
				<div class="listing-badge now-open">Now Open</div>
                                
                                <?php } ?>
                                
				<h3><i class="sl sl-icon-clock"></i> Opening Hours</h3>
				<ul>
					<li>Monday <span> <?php echo $coffee_shop_details['CoffeeShop']['opening_time'].' - '.$coffee_shop_details['CoffeeShop']['closing_time']; ?></span></li>
					<li>Tuesday <span> <?php echo $coffee_shop_details['CoffeeShop']['opening_time'].' - '.$coffee_shop_details['CoffeeShop']['closing_time']; ?></span></li>
					<li>Wednesday <span><?php echo $coffee_shop_details['CoffeeShop']['opening_time'].' - '.$coffee_shop_details['CoffeeShop']['closing_time']; ?></span></li>
					<li>Thursday <span><?php echo $coffee_shop_details['CoffeeShop']['opening_time'].' - '.$coffee_shop_details['CoffeeShop']['closing_time']; ?></span></li>
					<li>Friday <span><?php echo $coffee_shop_details['CoffeeShop']['opening_time'].' - '.$coffee_shop_details['CoffeeShop']['closing_time']; ?></span></li>
					<li>Saturday <span><?php echo $coffee_shop_details['CoffeeShop']['opening_time'].' - '.$coffee_shop_details['CoffeeShop']['closing_time']; ?></span></li>
					<li>Sunday <span><?php echo $coffee_shop_details['CoffeeShop']['opening_time'].' - '.$coffee_shop_details['CoffeeShop']['closing_time']; ?></span></li>
				</ul>
			</div>
			<!-- Opening Hours / End -->
                    
			<!-- Contact -->
			<div class="boxed-widget margin-top-35">
				
					<h3><span>Address & Contact Details</span></h3>
				
				<ul class="listing-details-sidebar">
                                        <li><i class="sl sl-icon-location"></i><?php echo $coffee_shop_details['CoffeeShop']['address1']; ?></li>
                                        <?php if(!empty($coffee_shop_details['CoffeeShop']['address2'])){ ?>
					<li> <?php echo $coffee_shop_details['CoffeeShop']['address2'];?></li>
                                        <?php } ?>
                                        <li><?php echo $coffee_shop_details['CoffeeShop']['location']; ?></li>
                                        <li><?php echo $coffee_shop_details['CoffeeShop']['city']; ?></li>
					<li><i class="sl sl-icon-phone"></i><?php echo $coffee_shop_details['CoffeeShop']['contact_number']; ?></li>
					<?php if(!empty($coffee_shop_details['CoffeeShop']['contact_email'])){ ?>
					<li><i class="fa fa-envelope-o"></i> <a href="mailto:<?php $coffee_shop_details['CoffeeShop']['contact_email'];?>"><?php echo $coffee_shop_details['CoffeeShop']['contact_email'];?></a></li>
                                        <?php } ?>
				</ul>

                
                
			</div>
			<!-- Contact / End-->
                        
                        
                        

            	<div class="listing-share margin-top-40 margin-bottom-40 no-border">
<!--
				<button class="like-button"><span class="like-icon"></span> Bookmark this listing</button> 
				<span>159 people bookmarked this place</span>
-->
                <?php 
                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                ?>
					<!-- Share Buttons -->
					<ul class="share-buttons margin-top-40 margin-bottom-0">
						<li>
                             <div class="fb-share-button fb-share" data-href="<?php echo $actual_link; ?>" data-layout="button_count" data-size="large"> </div>
                            </li>
<!--
                            
                             <a class="fb-share" href="#"><i class="fa fa-facebook"></i> Share</a>
						<li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
						<li><a class="gplus-share" href="#"><i class="fa fa-google-plus"></i> Share</a></li>
						<li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li> 
-->
					</ul>
					<div class="clearfix"></div>
			</div>
			

		</div>
		<!-- Sidebar / End -->

	</div>
</div>


<!-- Maps -->


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7AcWtZ7YewGNJfCNQYLDgwYiLzgdNQlE&libraries=geometry"></script>
<?php echo $this->Html->script('scripts/infobox.min.js'); ?>
<?php echo $this->Html->script('scripts/markerclusterer.js'); ?>
<?php echo $this->Html->script('scripts/maps.js'); ?>





<style >
	@media (max-width: 1600px) {
		.date-picker-wrapper{
			   top: 831.984px;
		}
	}
	@media (max-width: 767px) {
		.date-picker-wrapper{
			top: 4178px;
		}
	}
    .date-picker-wrapper{
        z-index: 999 !important;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    }
    .checkInbtn{
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    }
    .priceDisplay{
        margin: 0px !important;
    word-wrap: break-word !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 22px !important;
    line-height: 28px !important;
    letter-spacing: -0.2px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #484848 !important;
    font-weight: 700 !important;
    display: inline !important;
    }
    .extra_room_checkbox{
        background: #af6161 !important;
    border: 1px solid #af6161 !important;
    border-radius: 2px !important;
    height: 18px !important;
    width: 18px !important;
    display: inline-block !important;
    text-align: center !important;
    overflow: hidden !important;
    }
    .book-now.button{
        width: 100%;
        text-align: center;
    }
    
    
#see_dates_exprnce {
    max-width: 500px;
}
#see_dates_exprnce, #small-dialog {
    background: #fff;
    padding: 40px;
    padding-top: 0;
    text-align: left;
    max-width: 610px;
    margin: 40px auto;
    position: relative;
    box-sizing: border-box;
    border-radius: 4px;
}

#small-dialog .mfp-close, #see_dates_exprnce, .mfp-close, .mfp-close:hover {
    color: #fff;
}
._1f9rmq80 {
    border-bottom: 1px solid #DBDBDB !important;
    padding-top: 24px !important;
    padding-bottom: 24px !important;
    }
._e296pg {
    position: relative !important;
}
._rotqmn2 {
    margin: 0px !important;
    word-wrap: break-word !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 19px !important;
    line-height: 24px !important;
    letter-spacing: undefined !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #484848 !important;
    font-weight: 300 !important;
}
    
._150a3jym {
    margin: 0px !important;
    word-wrap: break-word !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 15px !important;
    line-height: 18px !important;
    letter-spacing: 0.2px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #484848 !important;
    font-weight: 300 !important;
}
    
._13k6a2k {
    position: absolute !important;
    right: 0px !important;
    top: 0px !important;
}

._annhcu9 {
    cursor: pointer !important;
    -webkit-transition: background 0.3s, border-color 0.3s !important;
    -moz-transition: background 0.3s, border-color 0.3s !important;
    transition: background 0.3s, border-color 0.3s !important;
    position: relative !important;
    display: inline-block !important;
    text-align: center !important;
    text-decoration: none !important;
    border-radius: 4px !important;
    width: auto !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 15px !important;
    line-height: 18px !important;
    letter-spacing: undefined !important;
    padding-top: 8px !important;
    padding-bottom: 8px !important;
    color: #484848 !important;
    font-weight: normal !important;
    border: 1px solid transparent !important;
    padding-right: 16px !important;
    padding-left: 16px !important;
    min-width: 51.77708763999664px !important;
    border-color: #008489 !important;
    background: transparent !important;
}

._16c9pko4 {
    -webkit-transition: color 0.3s !important;
    -moz-transition: color 0.3s !important;
    transition: color 0.3s !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 15px !important;
    line-height: 18px !important;
    letter-spacing: undefined !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #008489 !important;
}
    
.share-buttons li i {
   font-size: 20px;       
    font-weight: bold;
    }
</style>
    



 <div id="see_dates_exprnce" class="zoom-anim-dialog mfp-hide">

    <div class="small-dialog-header" style="margin-bottom:0px;">
		<h3>Upcoming Availability</h3>
    </div>
    <div id="listing-pricing-list" class="listing-section">
        <div class="show-more">
				<div class="pricing-list-container">
                    <?php foreach($experienceSlots as $slots) { ?> 
                                <div class="_1f9rmq80">
                                    <div class="_e296pg">
                                        <div>
                                            <div class="_rotqmn2"> <?php echo date('D, dS F Y ' ,strtotime($slots['ExperienceSlot']['checkindate'])); ?></div>
                                            <div>
                                                <div style="margin-top: 8px;">
                                                    <div class="_150a3jym"><?php echo $slots['ExperienceSlot']['slot_start_time']." − ". $slots['ExperienceSlot']['slot_end_time']; ?> <i class="fa fa-fw fa-1x">&#xf156;</i><?php echo number_format($slots['ExperienceSlot']['price']); ?> per person</div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="_13k6a2k">
                                            <button type="button" class="_annhcu9" aria-busy="false">Choose</button>
                                        </div>
                                    </div>
                                </div>
                        
                        <?php } ?>
				</div>
            </div>
        <a href="#" class="show-more-button" data-more-title="Show More" data-less-title="Show Less"><i class="fa fa-angle-down"></i></a>
     </div>             
             
</div>

<?php echo $this->Html->script('scripts/quantityButtonsExperience.js'); ?>