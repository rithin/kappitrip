<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>

<!-- Titlebar
================================================== -->
<!--
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				
				 
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Coffee Shops</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>
-->




<!-- Content  ================================================== -->

<div class="container  ">
	<div class="row margin-bottom-25" style="border-bottom: #F0F0F0 1px solid;">

    </div>
    
    <div class="row">
        
        
		<!-- Sidebar		================================================== -->
		<div class="col-lg-3 col-md-4">
			<div class="sidebar">


                                <!-- Widget -->
				<div class="widget margin-bottom-40">
					<h3 class="margin-top-0 margin-bottom-30">Filters</h3>
					<div class="col-md-12 margin-bottom-20">
						<button class="button " onclick="javascript:filterListing('basicSearch')" > Search</button>
						<button class="button " onclick="javascript:ClearAll('clear')" > Clear All</button>
					</div>
					

					<!-- Row -->
					<div class="row with-forms">
						<!-- Cities -->
						<div class="col-md-12">
						
							<?php echo $this->Form->input('search_text',array('div'=>false, 'label'=>false, 'id'=>'search_text','autofocus','placeholder'=>'What are you looking for?','error'=>false));?>
						</div>
					</div>
					<!-- Row / End -->


                                       


					

                                        <!-- Row -->
					<div class="row with-forms">
						<!-- Type -->
						<div class="col-md-12">
						
                                        <?php  
                                            echo $this->Form->input('state_id', array('options'=> $array_states,'div'=>false, 'label'=>false,  'class'=>'chosen-select',  'type'=>'select', 'id' =>'state_id', 'data-placeholder'=>'State','empty'=>'State')); 

                                        ?>
						</div>
					</div>
					<!-- Row / End -->

                                        
             

					<!-- Area Range -->
					<!--<div class="range-slider">
						<input class="distance-radius" type="range" min="10" max="2000" step="10" value="10" data-title="Products Price Range" onclick="filterListing('priceSlider')">
					</div>-->


					

				<button class="button fullwidth margin-top-25" onclick="javascript:filterListing('basicSearch')" > Search</button>

				</div>
				<!-- Widget / End -->

				

			</div>
		</div>
		<!-- Sidebar / End -->
        

		<div class="col-lg-9 col-md-8">
                <section>
			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-25 ">
                 <section class="sortby">
                    <div class="col-md-6 col-xs-6">
                        <!-- Layout Switcher -->
                        <div class="sort-by">
                            <nav id="breadcrumbs" class="breadcrumbs">
                            <ul>
                                <li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
                                <li>Coffee Shops</li>
                            </ul>
                            </nav>
                        </div>
                        
                    </div>

                    <div class="col-md-6 col-xs-6">
                        <!-- Sort by -->
                        <!--<div class="sort-by">
                            <div class="sort-by-select">
                                <select data-placeholder="Sort By" class="chosen-select-no-single">
                                    <option>Default Order</option>	
                                    <option>Highest Rated</option>
                                    <option>Most Reviewed</option>
                                    <option>Newest Listings</option>
                                    <option>Oldest Listings</option>
                                </select>
                            </div>
                        </div>-->
                    </div>
                </section>
			</div>
               
			<!-- Sorting / Layout Switcher / End -->

			<div class="row ">
                 <div id="ajaxListing">	
			         <input type="hidden" name="rowcount" id="rowcount" />
		        </div> <!-- Ajax Div Ends here -->

			</div>
       </section>
                
    </div>

        
        
        
    </div>
    
</div>


<script >
$( document ).ready(function() {
	if($('#property_type').val()!=0){
			filterListing('basicSearch'); 
		}
	if($('#search_text').val()!=0){
			filterListing('basicSearch'); 
		}
});


function ClearAll(action){
    if(action=='clear'){
        
        $('#search_text').val('');
        $('#state_id').val('').trigger('chosen:updated');
        
        filterListing(''); 

    }
}
   
	

function slectedAmenities(){
    var selectedAmenities = [];
    var selectedAmenitiesJson="";
     $('#amenityChkbox input:checked').each(function() {
        selectedAmenities.push($(this).val());
     });
    if(selectedAmenities.length>0){
     selectedAmenitiesJson=JSON.stringify(selectedAmenities);
    }
    return selectedAmenitiesJson;
}
function selectedPriceSlider(){
    var maxprice="";
     maxprice= $('.range-output').html();
    return maxprice;
}
function filterListing(){
	
    var data='';  
   
 
    data= data+'&search_text='+$('#search_text').val();
    data= data+'&state_id='+$('#state_id').val();
    
    
    /*if( selectedPriceSlider()>60){        
        data= data+'&selectedPriceRange='+selectedPriceSlider();
    }*/
    //data= data+'&selectedAmenitiesJson='+slectedAmenities();    
    
    //console.log(data);
    $.ajax({
        type: 'POST',
        url: "<?php echo Configure::read('app_root_path'); ?>coffee_shop/ajaxCoffeeShopFilter",
        data: data,
        timeout: 30000,
        error: function()
         {
             return true;
        },
        beforeSend: function(){
              $('div#dim').fadeIn(400);
        },  
        complete : function(){
         $('div#dim').fadeOut(200);
        },
        success:function(html){
            $('#ajaxListing').html(html);
        }
    });
    
    
}
    
 
function searchProperty(){
		return true;
 }
	
function getresult(url) {
   
	$.ajax({
		url: url,
		type: "GET",
		data:  {
			rowcount:$("#rowcount").val(),"pagination_setting":$("#pagination-setting").val()
		},
        timeout: 30000,
        error: function()
         {
             return true;
        },
        beforeSend: function(){
              $('div#dim').fadeIn(400);
        },  
        complete : function(){
         $('div#dim').fadeOut(200);
        },
		success: function(data){
			$("#ajaxListing").html(data);;
		},
		error: function() 
		{ } 	        
   });
}
function changePagination(option) {
	if(option!= "") {
		getresult("<?php echo Configure::read('app_root_path'); ?>coffee_shop/ajaxCoffeeShopFilter");
	}
}	

getresult("<?php echo Configure::read('app_root_path'); ?>coffee_shop/ajaxCoffeeShopFilter");




     function check_user_login(){

            $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/check_user_login",
                
                success:function(response){
                    if(response==0){
                        $.magnificPopup.open({
                        items: {
                            src: '#sign-in-dialog',
                        },
                            type: 'inline',
                            closeOnBgClick:false,
                            enableEscapeKey: false,
                            closeOnContentClick: false,
                            showCloseBtn : false,
                        });
                        
                  
                    }
                }
            });



        }   

</script>


