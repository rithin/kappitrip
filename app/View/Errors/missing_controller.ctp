<!-- Container -->
<div id="container">
    <section class="error-section">
        <div class="error-content" style="top: 40%">
            <div class="text-center">
                <img src="<?php echo Configure::read('nuwed_root_path'); ?>img/logo.png" />
            </div>
            <div class="container">
                    <span>404</span>
                    <h1>oops! Page not found</h1>
                    <p>We are sorry, but the page you're looking for doesn't exist.</p>
                    <a href="<?php echo Configure::read('nuwed_root_path'); ?>" class="button-one">Return To Website</a>
            </div>
        </div>
    </section>
</div>
<!-- End Container -->