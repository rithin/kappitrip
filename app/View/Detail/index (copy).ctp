<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<?php echo $this->Html->css('daterangepicker.min.css'); ?>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js" type="text/javascript"></script>
<?php  echo $this->Html->script('longbill_calender/jquery.daterangepicker.js'); ?>
<!-- Slider
================================================== -->
<div class="listing-slider mfp-gallery-container margin-bottom-0">
   <?php 
    if(count($homestayDetails['HomestayFile'] )>0){
        foreach($homestayDetails['HomestayFile'] as $files) { 
       // debug($files);
        if(is_array($files)){
            $imageUrl = Configure::read('app_root_path').$files['file_path'].'/'.$files['file_name'];
        ?>
        <a href="<?php echo $imageUrl;?>" data-background-image="<?php echo $imageUrl;?>" class="item mfp-gallery" title="<?php echo  $files['file_name']; ?>"></a>
        <?php
                }
          } 
    }
   ?>
	
</div>

<!-- ========================================= Content ================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
		<div class="col-lg-8 col-md-8 padding-right-30">

			<!-- Titlebar -->
			<div id="titlebar" class="listing-titlebar">
				<div class="listing-titlebar-title">
                    <h2><?php echo $homestayDetails['Homestay']['name'];?><span class="listing-tag"><b>Starting from  INR <?php echo $homestayDetails['Homestay']['base_price'];?> </b></span></h2>
					<span>
						<a href="#listing-location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							<?php echo $homestayDetails['Location']['name'];?>
						</a>
					</span> 
					<div class="star-rating" data-rating="<?php echo $homestayDetails['Homestay']['rating'];?>">
<!--						<div class="rating-counter"><a href="#listing-reviews">(31 reviews)</a></div>-->
					</div>
					<ul class="share-buttons margin-top-40 margin-bottom-0" style="font-family: sans-serif;
    font-size: 18px;">
						<li><i class="fa fa-users"></i>  &nbsp;<?php echo $no_of_guest;?> Guests &nbsp;</li>	
						<li><span class="im im-icon-Green-House"></span>  &nbsp;<?php echo $no_of_rooms;?> Bedrooms &nbsp;</li>	
						<li><i class="fa fa-bed" style="font-size:18px"></i> &nbsp;<?php echo $no_of_bed;?> Beds &nbsp;</li>	
						<li><i class="fa fa-bath" style="font-size:18px"></i> &nbsp;<?php echo $homestayDetails['Homestay']['no_of_bathrooms'];?> Baths</li>
					</ul>
					
				</div>
			</div>

			<!-- Listing Nav -->
			<div id="listing-nav" class="listing-nav-container">
				<ul class="listing-nav">
					<li><a href="#listing-overview" class="active">Overview</a></li>
					<li><a href="#listing-pricing-list">Rooms</a></li>
					<li><a href="#other-details">Other Details</a></li> 
					<li><a href="#listing-location">Location</a></li>
<!--					<li><a href="#listing-reviews">Reviews</a></li>
					<li><a href="#add-review">Add Review</a></li> -->
				</ul>
			</div>
			
			<!-- Overview -->
			<div id="listing-overview" class="listing-section">

				<!-- Description -->

				<p>
                    <?php echo $homestayDetails['Homestay']['description'];?>
				</p>

				<!-- Amenities -->
				<h3 class="listing-desc-headline">Amenities</h3>
				<ul class="listing-features checkboxes margin-top-0">
                    <?php foreach($homestayDetails['HomestayAmenity'] as $amenities) { ?>
					   <li><?php echo $amenities['Amenity']['amenity'];?></li>
					
                    <?php } ?>
				</ul>
				<?php if($homestayDetails['Homestay']['inclusions']!=""){ ?>
					<!-- Amenities -->
					<h3 class="listing-desc-headline">Inclusions</h3>
					<ul class="listing-features checkboxes margin-top-0">

						<?php echo  $homestayDetails['Homestay']['inclusions'];  ?>

					</ul>
				<?php } ?>
				
			</div>


			<!-- Food Menu -->
			<div id="listing-pricing-list" class="listing-section">
				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Rooms</h3>

				<div class="show-more">
					<div class="pricing-list-container">
						
						<!-- Food List -->
						<h4>Bed Room Details</h4>
						<ul>
                            <?php foreach($homestayDetails['RoomPriceDetail'] as $rooms) { if($rooms['status']==1){ ?>
							<li> 
								<h5><?php echo @$rooms['room_no_name']." - ".$rooms['RoomType']['type'];?></h5>
								<p>This room have <?php echo $rooms['no_of_bed'];?> bed(s). <?php if($rooms['extra_bed']>0) echo "Extra bed available @<mark> INR ".$rooms['extra_bed_charge'] ."</mark>"; ?></p>
								<span><i class="fa fa-fw fa-1x">&#xf156;</i>  <?php echo $rooms['room_base_price'];?></span>
								<p> <b>&quot;</b>  <i><?php echo $rooms['room_description'];?></i>  <b>&quot;</b> </p>
							</li>
                            <?php } } ?>
							
						</ul>

					</div>
				</div>
				<a href="#" class="show-more-button" data-more-title="Show More" data-less-title="Show Less"><i class="fa fa-angle-down"></i></a>
			</div>
			<!-- Food Menu / End -->
<!-- Other details -->
	 <div id="other-details" class="listing-section" style="">
		<h3 class="listing-desc-headline margin-top-60 margin-bottom-30" >More Information</h3>
		 <div style="background-color: #f9f9f9; padding: 5px;padding-left: 20px; padding-right: 20px;">
		 <!-- Amenities -->
			 <?php  if($homestayDetails['Homestay']['activities_around']!=""){ ?>		 
				<h3 class="listing-desc-headline">Activities</h3>		 
				<ul class="list-4 color">
					<?php echo  $homestayDetails['Homestay']['activities_around'];  ?>
				</ul>

			 <?php } if($homestayDetails['Homestay']['house_rules']!=""){ ?>
				<h3 class="listing-desc-headline">House Rules</h3>
				<ul class="list-4 color">
				  <?php echo $homestayDetails['Homestay']['house_rules'];?>
				</ul>
			 <?php } if($homestayDetails['Homestay']['food_cuisine']!=""){ ?>
				<h3 class="listing-desc-headline">Cusine</h3>
				<ul class="list-4 color">
				  <?php echo $homestayDetails['Homestay']['food_cuisine'];?>
				</ul>
				<?php }  if($homestayDetails['Homestay']['cancellation_policy']!=""){ ?>
				<h3 class="listing-desc-headline">Cancellation Policy</h3>
				<ul class="list-4 color">
				  <?php echo $homestayDetails['Homestay']['cancellation_policy'];?>
				</ul>
			 <?php }  if($homestayDetails['Homestay']['other_details']!=""){ ?>
				<h3 class="listing-desc-headline">Other Details</h3>
				<ul class="list-4 color">
				  <?php echo $homestayDetails['Homestay']['other_details'];?>
				</ul>
			 <?php } ?>
		 </div>
	 </div>
			
			
			<!-- Location -->
	<div id="listing-location" class="listing-section">
		<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Location</h3>

		<div id="singleListingMap-container">
			<div id="singleListingMap" data-latitude="<?php echo $homestayDetails['Location']['latitude'];?>" data-longitude="<?php echo $homestayDetails['Location']['longitude'];?>" data-map-icon="im im-icon-Home-2"></div>
			<a href="#" id="streetView">Street View</a>
		</div>
		</div>
	

		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-75 booking-section" id="booking-section" >
			<!-- Verified Badge -->
			<div class="verified-badge with-tip" data-tip-content="Listing has been verified and belongs the business owner or manager.">
				<?php 
					$offer_text="";
					if($homestayDetails['Deal']['id']!=''){
						if($homestayDetails['Deal']['offer_type']=='percentage')
								$offer_text=$homestayDetails['Deal']['offer_value']."% OFF from ".date('jS-M',strtotime($homestayDetails['Deal']['start_date']))." to ".date('jS-M',strtotime($homestayDetails['Deal']['end_date']));
						else
								$offer_text=$homestayDetails['Deal']['offer_value']."Rs OFF from ".date('jS-M',strtotime($homestayDetails['Deal']['start_date']))." to ".date('jS-M',strtotime($homestayDetails['Deal']['end_date']));
					}else{
						$offer_text="Verified Listing";
					}
				?>
				<i class="sl sl-icon-check"></i><?php echo $offer_text; ?>
			</div>
			
			<!-- Book Now 

style="position:fixed !important;
    bottom: 20px !important; width:417px; display:none;" 
-->
            <?php                         
               $action = Configure::read('app_root_path')."booking/index";
               echo $this->Form->create('Booking', array('id'=>'bookingDetail', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'get','class'=>'form-horizontal ', 'novalidate' => true, 'onsubmit'=>'return doBook()'));   
               echo $this->Form->input('room_id_selected',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'room_id_selected','autofocus','error'=>false));
               echo $this->Form->input('total_amount',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'total_amount','autofocus','error'=>false,'value'=>''));
               echo $this->Form->input('homestay_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'homestay_id','autofocus','error'=>false,'value'=>$homestayDetails['Homestay']['id'])); 
				echo $this->Form->input('error_flag',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'error_flag','autofocus','error'=>false,'value'=>'')); 
				echo $this->Form->input('Deal_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'deal_id','autofocus','error'=>false,'value'=>$homestayDetails['Deal']['id'])); 
            ?>
			<div class="boxed-widget booking-widget margin-top-35 " id="booking_section" >
			
				<?php if($homestayDetails['Homestay']['instant_book']=="Yes"){ ?>
<!--				<div class="listing-badge now-open">Instant Book</div> -->
                <?php } ?>
                <h3><span class="priceDisplay" id="priceDisplay"><i class="fa fa-fw fa-1x">&#xf156;</i>
                    <span id="PriceActual" style="font-size: 23px;">
						<?php 
		  					$item_price=$homestayDetails['Homestay']['base_price'];
		  					$offer_price=0;
		  					if($homestayDetails['Deal']['id']!=''){
									if($homestayDetails['Deal']['offer_type']=='percentage')
										$offer_price=($item_price-(($item_price*$homestayDetails['Deal']['offer_value'])/100));
									else
										$offer_price=($item_price-$homestayDetails['Deal']['offer_value']);
								 }	
		  						if($offer_price>0){
								//	echo '<strike><i class="fa fa-fw fa-1x">&#xf156;</i>'.number_format($item_price,2)."</strike> ";
		  							echo number_format($offer_price,2);
								}
		  						else
									echo number_format($item_price,2);
						?>
                    </span> 
					<?php if($homestayDetails['Homestay']['property_type_id']==6){ ?>
						 <small>per tour</small> <br/>
						 <small> (<i><?php echo $homestayDetails['RoomPriceDetail'][0]['room_description']; ?></i>)</small>
				
					<?php	}else{?>
						<small>per night</small>
						<?php } ?>
					</span>
					
				  <span style="display:none;" id="priceLoader"><img src="<?php echo Configure::read('app_root_path');?>images/progress_bar_price.gif" height="80"/></span>
				</h3>
				
			<?php if($homestayDetails['Homestay']['instant_book']=="Yes" || $enquiry_accept_reject=="accepted") { ?>
				
				<div class="row with-forms  margin-top-0">
                     
					<!-- Date Picker - docs: http://www.vasterad.com/docs/listeo/#!/date_picker -->
					
                    <div class="col-lg-12">

                    <div class="panel-dropdown">
                        <input id="checkInbtn" class="checkInbtn" size="60" value="Check in – Check out"> 
                        <input type="hidden" name="startDate" id="startDate"/>
                        <input type="hidden" name="endDate" id="endDate"/>
                    </div>
                    
					<!-- Time Picker - docs: http://www.vasterad.com/docs/listeo/#!/time_picker -->
<!--
					<div class="col-lg-6 col-md-12">
                        Ex. Check-In
						<input type="text" id="booking-time" value="9:00 am">
					</div>
-->
					
                    </div>
                    
                    
					<!-- Panel Dropdown -->
					<div class="col-lg-12">
                        <?php 
                            $max_no_of_guest=$homestayDetails['Homestay']['max_no_of_guest'];
                            $max_no_of_bedrooms=$homestayDetails['Homestay']['no_of_bedrooms'];
                        
                        
                        ?>
						<div class="panel-dropdown" id="guestQuanityDropDown">
							<a href="#">Guests <span class="qtyTotal" name="qtyTotal">1</span></a>
							<div class="panel-dropdown-content">
                                <input type="hidden" id="maxAllowedCount" name="maxAllowedCount" value="0" >
                                <input type="hidden" id="room_ids" name="room_ids" value="" >
                                <input type="hidden" id="maxAvailRooms" name="maxAvailRooms" value="0" >
                                <input type="hidden" id="flagSameRoomAvail" name="flagSameRoomAvail" value="" >
								<!-- Quantity Buttons -->
								<div class="qtyButtons">
									<div class="qtyTitle">Adults</div>
									<input type="text" id="adultCount" name="qtyInputAdult"  class="qtyInput" value="1" min="0" max="1">
								</div>
                                <div class="qtyButtons">
									<div class="qtyTitle">Infants (0 - 5 years)</div>
									<input type="number" id="infantCount" name="qtyInputInfant" class="qtyInput" value="0" min="0" max="0">
								</div>
								<div class="qtyButtons">
									<div class="qtyTitle">Children (6 - 12 years)</div>
									<input type="number" id="childCount" name="qtyInputChild" class="qtyInput" value="0" min="0" max="2">
								</div>
                                
                                

							</div>
						</div>
						<div id="error_message_display"></div>
					</div>
					
					<!-- Panel Dropdown / End -->
                    
                    <div class="col-lg-12" id="room_id">
                      <div class="panel-dropdown " id="room_choice">
                            <a href="#">Rooms <span class="room_type_chosen" id="room_type_chosen" style="color: #f91942;font-size: 13px;"></span></a>
                            <div class="panel-dropdown-content">
								<div style="  color: #ff0047;" id="room_error_msg"> </div>
								
                                <div class="rooms" id="room_list">
								</div>
                                
                                
                            </div>
                      </div>                             
                    </div>
                    
					
				<div class="col-lg-6">
					<input type="submit" class="button book-now fullwidth margin-top-5" name="btn_booking" value="Book Now" />
				 </div>
				<div class="col-lg-6">
					<span class="qtyTotal" name="qtyTotal" style="display:none;">0</span>
					<input type="button" class="button book-now fullwidth margin-top-5 sign-in popup-with-zoom-anim" name="btn_enquiry" value="Enquiry" onclick="javascript:raiseEnquiry()" style="background-color:green;" />
				</div>	
               

			  </div>
				
				
				
		<?php }else{ ?>	 
				<input id="checkInbtn" class="checkInbtn" size="60" value="Choose Dates" type="hidden"> 
				<span class="qtyTotal" name="qtyTotal" style="display:none;">0</span>
			<input type="button" class="button book-now fullwidth margin-top-5 sign-in popup-with-zoom-anim" name="btn_enquiry" value="Enquiry" onclick="javascript:raiseEnquiry()" />	
		<?php } ?>
<!--				<a href="pages-booking.html" class="button book-now fullwidth margin-top-5">Book Now</a>-->
				</div>
		<?php echo $this->Form->end(); ?>  
		
			
			<!-- Book Now / End -->
		

			<!-- Opening Hours -->
			<div class="boxed-widget opening-hours margin-top-35">
                <?php if($homestayDetails['Homestay']['instant_book']=="Yes"){ ?>
				<div class="listing-badge now-open">Instant Book</div> 
                <?php } ?>
				<h3><i class="sl sl-icon-clock"></i> Opening Hours</h3>
				<ul>
					<li>Check In Time <span><?php echo $homestayDetails['Homestay']['check_in_time'];?></span></li>
					<li>Check Out Time <span><?php echo $homestayDetails['Homestay']['check_out_time'];?></span></li>
					
				</ul>
			</div>
			<!-- Opening Hours / End -->


			<!-- Contact -->
			<div class="boxed-widget margin-top-35">
				<div class="hosted-by-title">
					<h4><span>Hosted by</span> <a href="#"><?php echo "Coorgexpress.com";?></a></h4>
					<a href="pages-user-profile.html" class="hosted-by-avatar"><img src="images/dashboard-avatar.jpg" alt=""></a>
				</div>
				<ul class="listing-details-sidebar">
					<li><i class="sl sl-icon-phone"></i><?php echo Configure::read('enquiry_phone');?></li>
					<!-- <li><i class="sl sl-icon-globe"></i> <a href="#">http://example.com</a></li> -->
					<li><i class="fa fa-envelope-o"></i> <a href="#"><?php echo Configure::read('enquiry_email');?></a></li>
				</ul>


				<!-- Reply to review popup -->
				<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
					<div class="small-dialog-header">
						<h3>Send Message</h3>
					</div>
					<div class="message-reply margin-top-0">
						<textarea cols="40" rows="3" placeholder="Your message to <?php echo $homestayDetails['Homestay']['owner_name'];?>"></textarea>
					</div>
				</div>

			</div>
			<!-- Contact / End-->


			<!-- Share / Like -->
			<div class="listing-share margin-top-40 margin-bottom-40 no-border">

                <?php 
                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                ?>
					<!-- Share Buttons -->
					<ul class="share-buttons margin-top-40 margin-bottom-0">
						<li>
                             <div class="fb-share-button fb-share" data-href="<?php echo $actual_link; ?>" data-layout="button_count" data-size="large"> </div>
							 <div id="like_button_container"></div>
                            </li>
                            
					</ul>
					<div class="clearfix"></div>
			</div>

		</div>
		<!-- Sidebar / End -->

	</div>
</div>

<div id="book_now_footer" style="    position: fixed !important;
    bottom: 0px !important;
    width: 100% !important;
    background-color: #ffffff !important;
    box-shadow: 0 3px 9px 3px rgba(0, 0, 0, 0.05) !important;
    border-top: 1px solid #EBEBEB !important;
    z-index: 500 !important;
    padding: 10px 0 !important;">
	<div class="col-lg-9"></div>
	<div class="col-lg-3">
		<div class="col-lg-6">
			<input type="submit" class="button book-now fullwidth margin-top-5" id="btn_booking" name="btn_booking" value="Book Now" />
		  </div>

		<div class="col-lg-6">
			<span class="qtyTotal" name="qtyTotal" style="display:none;">0</span>
			<input type="button" class="button book-now fullwidth margin-top-5 sign-in popup-with-zoom-anim" name="btn_enquiry" value="Enquiry" onclick="javascript:raiseEnquiry()" style="background-color:green;" />
		</div>
	</div>
		
</div>

<div id="enquiryForm" class="zoom-anim-dialog mfp-hide"></div>

<div id="fb-root"></div>


<script>
    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
    
</script>

  <!-- Your share button code -->
 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7AcWtZ7YewGNJfCNQYLDgwYiLzgdNQlE&libraries=geometry"></script>
<?php echo $this->Html->script('scripts/infobox.min.js'); ?>
<?php echo $this->Html->script('scripts/markerclusterer.js'); ?>
<?php echo $this->Html->script('scripts/maps.js'); ?>

<!-- Date Picker - docs: http://www.vasterad.com/docs/listeo/#!/date_picker -->
<?php echo $this->Html->css('plugins/datedropper.css'); ?>
<?php echo $this->Html->script('scripts/datedropper.js'); ?>
<script>$('#booking-date').dateDropper();</script> 

<!-- Time Picker - docs: http://www.vasterad.com/docs/listeo/#!/time_picker -->

<?php echo $this->Html->script('scripts/timedropper.js'); ?>
<?php echo $this->Html->css('plugins/timedropper.css'); ?>
<?php $current_base_url= Configure::read('app_root_path')."handpicked-stays/".$homestayDetails['Homestay']['seo_unique_url']."?searchId=".$homestayDetails['Homestay']['id']."&name=".$homestayDetails['Homestay']['name']; ?>
<script>
	
$("#btn_booking").click(function () {
    //getting the next element
    $content = $('#booking_section');
    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
    $content.slideToggle(500, function () {
        //execute this after slideToggle is done
		$('#book_now_footer').hide();
        
    });

});
var url_rewrite = "<?php echo $current_base_url;?>";
this.$('#booking-time').timeDropper({
	setCurrentTime: false,
	meridians: true,
	primaryColor: "#f91942",
	borderColor: "#f91942",
	minutesInterval: '15'
});

var $clocks = $('.td-input');
var disabled_dates_arr = [<?php  foreach ($disableDays as $disableDate) { echo "'".$disableDate."',"; } ?> ];
var monthly_occupied_dates_arr = [<?php  foreach ($monthlyOccupancys as $occupiedDate) { echo "'".$occupiedDate."',"; } ?> ];    
var homestay_id="<?php echo $homestayDetails['Homestay']['id'];?>";
var no_of_bedrooms="<?php echo $homestayDetails['Homestay']['no_of_bedrooms'];?>";    
    

$(function() { 
	if (!window['console'])
	{
		window.console = {};
		window.console.log = function(){};
	}
		
	/*
	define a new language named "custom"
	*/

	$.dateRangePickerLanguages['custom'] = 
	{
		'selected': 'Choosed:',
		'days': 'Days',
		'apply': 'Close',
		'week-1' : 'Mon',
		'week-2' : 'Tue',
		'week-3' : 'Wed',
		'week-4' : 'Thu',
		'week-5' : 'Fri',
		'week-6' : 'Sat',
		'week-7' : 'Sun',
		'month-name': ['January','February','March','April','May','June','July','August','September','October','November','December'],
		'shortcuts' : 'Shortcuts',
		'past': 'Past',
		'7days' : '7days',
		'14days' : '14days',
		'30days' : '30days',
		'previous' : 'Previous',
		'prev-week' : 'Week',
		'prev-month' : 'Month',
		'prev-quarter' : 'Quarter',
		'prev-year' : 'Year',
		'less-than' : 'Date range should longer than %d days',
		'more-than' : 'Date range should less than %d days',
		'default-more' : 'Please select a date range longer than %d days',
		'default-less' : 'Please select a date range less than %d days',
		'default-range' : 'Please select a date range between %d and %d days',
		'default-default': 'This is costom language'
       
	};
 // console.log(disabled_dates_arr);
$('.checkInbtn').dateRangePicker(
 {
		
        startDate: new Date(),
		selectForward: true,
        format: 'DD-MM-YYYY',       
	  // alwaysOpen:true,
		beforeShowDay: function(t)
		{
            current_date= moment(t).format('DD-MM-YYYY');
           //var valid = !(t.getDay() == 0 || t.getDay() == 6);  //disable saturday and sunday
            var valid = "";
                if(disabled_dates_arr.indexOf(current_date) == -1)
                    valid=true;
                else
                    valid=false;
			var _class = '';
			var _tooltip = valid ? '' : 'sold out';           
			return [valid,_class,_tooltip];     
		},
        onSelect: function(date, instance) {
           // alert(date);
            $.ajax
            ({
                  type: "Post",
                  url: "www.example.com",
                  data: "date="+date,
                  success: function(result)
                  {
                      //do something
                  }
             });  
        },
        
        
    })
    .bind('datepicker-first-date-selected', function(event, obj)
    {
                //event trigger on check in date selection
    
    })
   .bind('datepicker-change',function(event,obj)
     {
            /* This event will be triggered when second date is selected */
            var check_in_date=moment(obj.date1).format('YYYY-MM-DD'); //converting to sql format.
            var check_out_date=moment(obj.date2).format('YYYY-MM-DD');
            var selectedDate= moment(obj.date1).format('DD MMM YYYY')+' -> '+moment(obj.date2).format('DD MMM YYYY');
            $('#startDate').val(check_in_date);
            $('#endDate').val(check_out_date);
            $('.checkInbtn').html(selectedDate);            
    
            var fltrString={"check_in_date": check_in_date,"check_out_date":check_out_date, "homestay_id":homestay_id, "no_of_bedrooms":no_of_bedrooms};        
            var selectedDateJson=JSON.stringify(fltrString);
            if(url_rewrite.indexOf("?") > -1) {
                url_rewrite = url_rewrite+"&check_in_date="+check_in_date+"&check_out_date="+check_out_date;
            }
            window.history.pushState({path:url_rewrite},'',url_rewrite);
            //console.log(check_in_date +"<<<<<===>>>"+check_out_date);
            $.ajax
                ({
                      type: "POST",
                      url: "<?php echo Configure::read('app_root_path'); ?>detail/findAvailableRooms",
                      data: "selectedDates="+selectedDateJson,
                      success: function(result)
                      {
                          var restultOutput=JSON.parse(result);
                          var max_no_of_beds=restultOutput.max_no_of_beds;
                          var max_available_rooms=restultOutput.max_available_rooms;
                          var room_ids=restultOutput.room_ids;
                              
						  var room_idJsonString = JSON.stringify(room_ids);
                          $('#maxAllowedCount').val(max_no_of_beds); 
                          $('#maxAvailRooms').val(max_available_rooms); 
                          $('#room_ids').val(room_idJsonString);   
                          $("#adultCount").attr({ "max" : max_no_of_beds,"min" : 1 });
                          $("#infantCount").attr({ "max" : (max_available_rooms*2),"min" : 0 });
                          $("#childCount").attr({ "max" : 2,"min" : 0 });
                         // console.log(result);
						  $('.date-picker-wrapper').hide();
						  $("#guestQuanityDropDown").attr('class','panel-dropdown  active');
						  $('#adultCount').val(1);
						  $('#childCount').val(0);
				 		  $('#infantCount').val(0);
						  $('.qtyTotal').html('1');
						  // console.log(room_idJsonString);   
                      }
                }); 
     });



});   
    
$('#guestQuanityDropDown').click(function(evt)
{
     if($('#startDate').val()=="" || $('#endDate').val()==""){
         $("#guestQuanityDropDown").attr('class','panel-dropdown  ');
         evt.stopPropagation();
	     $('#checkInbtn').data('dateRangePicker').open();
    }
	
});
    
// managing the guests drop dowm close and open.
var mouse_is_inside = false;
$('#guestQuanityDropDown').hover(function(){
     mouse_is_inside=true;
 }, function(){
    mouse_is_inside=false;      
});
    
$("body").mouseup(function(){
    if(! mouse_is_inside && $("#guestQuanityDropDown").is(".active")) 
        findRoomPriceDetails(); 
}); 

// enquiry form ajax

function raiseEnquiry(){
	
	 $.ajax
    ({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>detail/stay_enquiry",
          data: "home_stay_id="+homestay_id,
          success: function(enquiryForm)
          {	  
			  $('#errorMsgEnquiry').hide();
			  $('#tabMsgBox').hide();
			  $('#tabForm').show();			  
			  $("#enquiryForm").html(enquiryForm);
			 // $("#enquiryForm").dialog("open");
			  $.magnificPopup.open({
				  items: {
					src: '#enquiryForm'
				  },
				  type: 'inline'
				});
			  
			 	if($('#startDate').val()!=""){
						var start_date=$('#startDate').val();
						$('#start_date').val(start_date);
				}
				if($('#endDate').val()!=""){
						var end_date=$('#endDate').val();
						$('#end_date').val(end_date);
				}
				if($('#adultCount').val()!=""){
						var no_of_adult=$('#adultCount').val();
						$('#no_of_adult').val(no_of_adult);
				}
				if($('#childCount').val()!=""){
						var no_of_children=$('#childCount').val();
						$('#no_of_children').val(no_of_children);
				}
			  
		  }
     
	 }); 
}
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}
// enquiry form
function doEnquire(){    
		var start_date=$('#start_date').val();
		var end_date=$('#end_date').val();
		var no_of_adult=$('#no_of_adult').val();
		var no_of_children=$('#no_of_children').val();
		var full_name=$('#full_name').val();
		var contact_no=$('#contact_no').val();
		var contact_email=$('#contact_email').val();
		var message_box=$('#message_box').val();
		var home_stay_id=$('#home_stay_id').val();
        var room_id_selected=$('#room_id_selected').val();
        var final_price=$('#final_price').val();
        var selectedRoomsVal=new Array();
	    $('#room_list_enq input:checked').each(function() {		
		  selectedRoomsVal.push($(this).val());	 	
	   });
		if(start_date=="" || end_date=="" || no_of_adult=="" ||  full_name=="" || contact_no=="" || contact_email=="" || message_box==""  ){
			$('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('  Please fill all the fields with relevant data.  ');		
		}
        if(selectedRooms.length>0) {
            enquiryFindRoomPriceDetails();
            $('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('  Please select the room(s).  ');
            e.preventDefault();
        }
        if($("#error_flag_enq").val()==1){   
            enquiryFindRoomPriceDetails();
			$('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('  Please select enough rooms to accommodate all the guests.  ');
            e.preventDefault();
		}
		if ($.trim(contact_email).length == 0) {
			$('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('  Please fill a valid email address.  ');
            e.preventDefault();
        }
        if (validateEmail(contact_email)==false) {
			$('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('   Please fill a valid email address.  ');
            e.preventDefault();
        }
		else{
            
			$('#errorMsgEnquiry').hide();
			var formData={"start_date": start_date,"end_date":end_date, "no_of_adult":no_of_adult,"no_of_children":no_of_children, "full_name":full_name, "contact_no":contact_no,"contact_email":contact_email, "message_box":message_box, "home_stay_id":home_stay_id,"room_id_selected":room_id_selected,"final_price":final_price };        
			var formDataJson=JSON.stringify(formData);

			 $.ajax
			({
				  type: "POST",
				  url: "<?php echo Configure::read('app_root_path'); ?>detail/doEnquire",
				  data: "formDataJson="+formDataJson,
                  timeout: 30000,
                  error: function() {
                        return true;
                   },
                  beforeSend: function(){
                        $('div#dim').fadeIn(400);
                  },  
                  complete : function(){
                        $('div#dim').fadeOut(200);

                  },
				  success: function(msgData)
				  {
					  if(msgData==1){ 
						  $('#errorMsgEnquiry').hide();
						  $('#tabForm').hide();
						  $("#enquiryForm").css('max-width','800px');
						  $("#not-found i").css('font-size','98px');
						  $("#not-found").css('margin-top','-42px');
						  $('#tabMsgBox').show();
					  } 
					  $('#tabForm').hide();
				  }
			});

		}
	
}
    //finding price based on guests.
function findRoomPriceDetails(){
    if($('#startDate').val()=="" || $('#endDate').val()==""){
        
    }else{
    var currentAdultCount=$('#adultCount').val();
    var currentChildCount=$('#childCount').val();
    var currentInfantCount=$('#infantCount').val();
    var room_ids=$('#room_ids').val();
    var max_available_rooms=$('#maxAvailRooms').val();
    var extraRoom=0;
//    if($('#extra_room').is(':checked')){
//        extraRoom=1;
//    }
    var fltrString={"adultCount": currentAdultCount,"childCount":currentChildCount, "infantCount":currentInfantCount, "room_ids":room_ids,"homestay_id":homestay_id,"extraRoom":extraRoom,"max_available_rooms":max_available_rooms};        
    var priceDetailJson=JSON.stringify(fltrString);
     if(url_rewrite.indexOf("?") > -1) {
            url_rewrite = url_rewrite+"&adultCount="+currentAdultCount+"&childCount="+currentChildCount+"&infantCount="+currentInfantCount+"&room_ids="+room_ids+"&extraRoom="+extraRoom+"&max_available_rooms="+max_available_rooms;
     }
     window.history.pushState({path:url_rewrite},'',url_rewrite);
    
    $.ajax
    ({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>detail/findPriceDetails",
          data: "priceDetail="+priceDetailJson,
          success: function(priceDetails)
          {
			  //console.log(priceDetails);
			if(priceDetails=='error_same_rooms_not_available'){
				 	$("#error_message_display").html('<span style="color:red;">No rooms available.Please try different dates. </span>');
					$("#room_id").hide();				
					$("#error_flag").val("error_same_rooms_not_available");
					$('#adultCount').val(1);
					$('#childCount').val(0);
				 	$('#infantCount').val(0);
					$('.qtyTotal').html('1');
		     }else{
				 $("#error_message_display").html('');
				//console.log(priceDetails);
                var restultOutput=JSON.parse(priceDetails);
                var room_id_selected=restultOutput.roomsSelected;
                var total_amount=restultOutput.final_price;
				var flagSameRoomAvail=restultOutput.flagSameRoomAvail;
				var extraBedFlag=restultOutput.extraBedFlag;
             //   $('#room_id_selected').val(room_id_selected);
                //$('#total_amount').val(total_amount);
				$('#flagSameRoomAvail').val(flagSameRoomAvail);
				var checkedflag=null;
				var roomdtls="";
				 
				if(restultOutput.room_dtls !="" ){ //&& restultOutput.multipleRoomAvail=="Yes"
					$("#room_id").show();
					$("#room_choice").attr('class','panel-dropdown  active');
					roomdtls='<div class="qtyTitle">Available Room Choices:</div>';		
					if(extraBedFlag>0){
						roomdtls+='<div id="ExtraRoom" class="checkboxes" style=""><small style="font-weight: bold;color: #af6161;font-size: 90%;">Do you need an extra bed?</small>	<input type="checkbox" id="extra_bed" name="extra_bed" class="extra_bed_checkbox" onClick="javascript:updateSelection(0)" ></div>';
					}
					
					if(restultOutput.flagSameRoomAvail=="no"){
						roomdtls+='<span style="color:red;">Same room(s) are not available for selected dates. The available choices are below: </span> <br/>';
					}
					$.each(restultOutput.room_dtls, function(keydate, itemArr) {
						if(restultOutput.flagSameRoomAvail=="no"){	
								roomdtls+='<span style="color:#5dadf1;font-weight:bold;">'+keydate+'</span>';
								checkedflag='checked="checked"';
							}
						$.each(itemArr, function(i, item) {
						if(restultOutput.flagSameRoomAvail=="no"){	
							roomdtls+='<label><input type="checkbox"  id="room_'+item.room_price_detail_id+'" onclick="javascript:updateRoom('+item.room_price_detail_id+','+item.final_price+','+"'"+item.room_type+"'"+ ','+item.room_base_price+','+item.extra_bed_charge+','+item.no_of_bed+',' +item.extra_bed+','+"'"+keydate+"'"+'   )"  name="rooms[]" class="option-input checkbox" value="'+item.room_price_detail_id+'" style="width:32px !important; height:20px; margin-bottom: 3px !important;"> '+ item.room_type +' <small>('+item.no_of_bed+': pax  '+item.extra_bed+': extra pax) </small> </label>';
							//' @ INR '+item.room_base_price+
							
						}else{							
							roomdtls+='<label><input type="checkbox" '+checkedflag+' id="room_'+item.room_price_detail_id+'" onclick="javascript:updateRoom('+item.room_price_detail_id+','+item.final_price+','+"'"+item.room_type+"'"+ ','+item.room_base_price+','+item.extra_bed_charge+','+item.no_of_bed+','+item.extra_bed +','+"'"+keydate+"'"+' )"  name="rooms[]" class="option-input checkbox" value="'+item.room_price_detail_id+'" style="width:32px !important; height:20px; margin-bottom: 3px !important;"> '+ item.room_type +' <small>('+item.no_of_bed+': pax  '+item.extra_bed+': extra pax)</small> </label>';
						}
							//' @ INR '+item.room_base_price+
							 //$('#total_amount').val(item.final_price);
							// $('#PriceActual').html(item.final_price);
						});
					});
					if(restultOutput.flagSameRoomAvail=="no" || restultOutput.multipleRoomAvail=="Yes"){	
						var valok="ok";var valcancel="cancel";
						roomdtls+='<span style="color:red;font-weight:bold;"><input type="button" class="button book-now fullwidth margin-top-5" name="btn_cancel" value="Cancel" style="width:44% !important;float:left;" onClick="javascript:updateSelection('+"'"+valcancel+"'"+ ');"> &nbsp; &nbsp; <input type="button" class="button book-now fullwidth margin-top-5" name="btn_ok" value="OK" style="width:44% !important;float:left;background-color:#54ba1d;" onClick="javascript:updateSelection('+"'"+valok+"'"+ ');"></span>';
						checkedflag='checked="checked"';
					}
					
					$('#room_list').html(roomdtls);
				}else{
					$("#room_id").hide();
					$("#error_flag").val("");
				}
				
			}
				
         }
     }); 
        
    }
} 

function updateSelection(selectItem){
//	if(selectItem=="ok")
//		$("#room_choice").attr('class','panel-dropdown');
//	else
//		 location.reload(true);
	
	$('#room_type_chosen').html('');
	var selectedRooms=new Array();
	$('#room_list input:checked').each(function() {		
		selectedRooms.push($(this).val());	 
	
	});	
	var selectedRoomsString = JSON.stringify(selectedRooms);
	var currentAdultCount=$('#adultCount').val();
    var currentChildCount=$('#childCount').val();
    var currentInfantCount=$('#infantCount').val();
    var max_available_rooms=$('#maxAvailRooms').val();
	var flagSameRoomAvail=$('#flagSameRoomAvail').val();
    var extraBed=0;
	var check_in_date=$('#startDate').val();
    var check_out_date=$('#endDate').val();
	
	//console.log(selectedRoomsString);
	if($('#extra_bed').is(':checked')){
        extraBed=1;
   }
	if(selectedRooms.length>0)
	{
    var fltrString={"adultCount": currentAdultCount,"childCount":currentChildCount, "infantCount":currentInfantCount, "selectedRoomsString":selectedRoomsString,"homestay_id":homestay_id, "extraBed":extraBed, "max_available_rooms":max_available_rooms,"flagSameRoomAvail":flagSameRoomAvail };        
    var priceDetailJson=JSON.stringify(fltrString);
	
	$.ajax
        ({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>detail/findFinalPrice",
          data: "details="+priceDetailJson,
          success: function(result)
            {
							
				var restultOutput=JSON.parse(result);
				var error_msg=restultOutput.error_msg;
				if(error_msg=="not_enough_bed_count"){
					$("#room_choice").attr('class','panel-dropdown active');
					$('#room_error_msg').html("**Choose more rooms to accomadate all guests.");
                    $('#error_flag').val(1);
				}else{
					var room_id_selected=restultOutput.roomsSelected;
					var final_booking_price=restultOutput.final_booking_price;
					var room_chosen_string=restultOutput.room_chosen_string;
					//finding the best price for the selected dates.
					priceX(homestay_id,check_in_date,check_out_date,final_booking_price,room_id_selected,room_chosen_string);
					
				}
           }
       }); 
	}else{
		$('#room_error_msg').html("Please select the room(s).");
        $('#error_flag').val(1);
		
	}
}

function priceX(homestay_id,check_in_date,check_out_date,final_booking_price,room_id_selected,room_chosen_string){
	var deal_id=$('#deal_id').val();
	var priceXString={"homestay_id":homestay_id,"check_in_date":check_in_date,"check_out_date":check_out_date, "base_price_per_day": final_booking_price,"deal_id":deal_id};
	var priceXJson=JSON.stringify(priceXString);
	var response=0;
	
	$('#priceDisplay').hide();
	$('#priceLoader').show();
	$.ajax
        ({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>pricing_x/bestPrice",
          data: "priceXJson="+priceXJson,
          success: function(resultPriceX)
            {	if(resultPriceX==0){
					resultPriceX=final_booking_price;
				}			
			 var resultPriceX_WithoutComma=(resultPriceX + "").replace(',', '')
				$('#room_id_selected').val(room_id_selected);
				$('#total_amount').val(resultPriceX_WithoutComma);
				$('#PriceActual').html(resultPriceX);
				$('#room_type_chosen').html(room_chosen_string);
				$("#room_choice").attr('class','panel-dropdown');
				$('#room_error_msg').html("");
                $('#error_flag').val(0);		
			 },
			 complete: function(){
				$('#priceLoader').hide();
				 $('#priceDisplay').show();
			  }
       }); 
	//return response;
}
	
var selectedRooms=new Array();
var room_beds=0; var room_final_price=0; var room_chosen_string="";
var selected_room_ids='';
function updateRoom(room_id,final_price,room_type,room_base_price,extra_bed_charge,no_of_bed,extra_bed){	
	//var returnflag=false;
	
	//$('#room_id_selected').val(selectedRoomsString);
	if($('#extra_bed').prop('checked')==true){
		room_beds=(parseInt(no_of_bed) + parseInt(extra_bed));	
	}else{
		room_beds=(parseInt(no_of_bed));	
	}
	
	selected_room_ids+=selected_room_ids+"_"+room_id;
	var currentAdultCount=$('#adultCount').val();
    var currentChildCount=$('#childCount').val();
    var currentInfantCount=$('#infantCount').val();
	var no_of_guest=(parseInt(currentAdultCount));
	/*
	selectedRooms.push(room_id);	 
	var selectedRoomsString = JSON.stringify(selectedRooms);		
	if(extra_bed_charge==null){
		extra_bed_charge=0;
	}
	if($('#extra_bed').prop('checked')==true){
		room_final_price=(parseInt(room_final_price) +parseInt(room_base_price) + parseInt(extra_bed_charge));
	}else{
		room_final_price=(parseInt(room_final_price) +parseInt(room_base_price) );
	}
	
	room_chosen_string= room_chosen_string+"("+room_type+" @INR "+room_final_price+")";
	$('#total_amount').val(room_final_price);
    $('#PriceActual').html(room_final_price);
	$('#room_type_chosen').html(room_chosen_string);
	*/
	if(room_beds >= no_of_guest){
		//$("#room_choice").attr('class','panel-dropdown');
	}
	//$('#room_id_selected').val(selected_room_ids);
	// alert(room_type+" @INR"+room_base_price);
	 	//$("#room_choice").attr('class','panel-dropdown');
	 updateSelection('ok');
 }   
function doBook(){
		var flag =true;
		var room_id_raw=$('#room_id_selected').val();
		if (($("#room_list").text().length) > 0) {
			if ($('input:checkbox').filter(':checked').length >= 1 )		{
				updateSelection('ok');
				flag=true;
			}else{
				$("#room_choice").attr('class','panel-dropdown active');
				$('#room_type_chosen').html("(Please choose a room.)");
				flag=false;
			}	
		}
		//console.log(flag);
		if($("#error_flag").val()==1){           
			flag=false;
		}
		if(flag==false){
			$("#room_choice").attr('class','panel-dropdown active');
		}
	return flag;

}

</script> 


<style >
	@media (max-width: 1600px) {
		.date-picker-wrapper{
			   top: 831.984px;
		}
	}
	@media (max-width: 767px) {
		.date-picker-wrapper{
			top: 4178px;
		}
	}
    .date-picker-wrapper{
        z-index: 999 !important;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    }
    .checkInbtn{
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    }
    .priceDisplay{
        margin: 0px !important;
    word-wrap: break-word !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 22px !important;
    line-height: 28px !important;
    letter-spacing: -0.2px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #484848 !important;
    font-weight: 700 !important;
    display: inline !important;
    }
    .extra_bed_checkbox{
        background: #af6161 !important;
    border: 1px solid #af6161 !important;
    border-radius: 2px !important;
    height: 18px !important;
    width: 18px !important;
    display: inline-block !important;
    text-align: center !important;
    overflow: hidden !important;
    }
    .book-now.button{
        width: 100%;
        text-align: center;
    }

@keyframes click-wave {
  0% {
    height: 40px !important;
    width: 40px !important;
    opacity: 0.35 !important;
    position: relative !important;
  }
  100% {
    height: 200px !important;
    width: 200px !important;
    margin-left: -80px !important;
    margin-top: -80px !important;
    opacity: 0 !important;
  }
}

.option-input {
  -webkit-appearance: none !important;
  -moz-appearance: none !important;
  -ms-appearance: none !important;
  -o-appearance: none !important;
  appearance: none !important;
  position: relative !important;
  top: 13.33333px !important;
  right: 0 !important;
  bottom: 0 !important;
  left: 0 !important;
  height: 20px !important;
  width: 20px;
  transition: all 0.15s ease-out 0s !important;
  background: #cbd1d8 !important;
  border: none !important;
  color: #fff !important;
  cursor: pointer !important;
  display: inline-block !important;
  margin-right: 0.5rem !important;
  outline: none !important;
  position: relative !important;
  z-index: 1000 !important;
}
.option-input:hover {
  background: #9faab7 !important;
}
.option-input:checked {
  background: #40e0d0 !important;
}
.option-input:checked::before {
  height: 20px !important;
  width: 20px !important;
  position: absolute !important;
  content: '✔' !important;
  display: inline-block !important;
  font-size: 26.66667px !important;
  text-align: center !important;
  line-height: 20px !important;
}
.option-input:checked::after {
  -webkit-animation: click-wave 0.65s !important;
  -moz-animation: click-wave 0.65s !important;
  animation: click-wave 0.65s !important;
  background: #40e0d0 !important;
  content: '' !important;
  display: block !important;
  position: relative !important;
  z-index: 100 !important;
}
.option-input.radio {
  border-radius: 50% !important;
}
.option-input.radio::after {
  border-radius: 50% !important;
}



body label {
  display: block;
  line-height: 20px !important;
}
.share-buttons li span {
   font-size: 20px;    
    }
</style>
<!-- Booking Widget - Quantity Buttons -->
<?php echo $this->Html->script('scripts/quantityButtons.js'); ?>


