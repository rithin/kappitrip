
	<!-- breadcrumb -->
	<div class="bread-crumb bgwhite flex-w p-l-52 p-r-15 p-t-30 p-l-15-sm">
		<a href="index.html" class="s-text16">
			Home
			<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
		</a>

		<a href="product.html" class="s-text16">
			Women
			<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
		</a>

		<a href="#" class="s-text16">
			T-Shirt
			<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
		</a>

		<span class="s-text17">
			<?php echo $product_details['Product']['product_name']; ?>
		</span>
	</div>

	<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
		<div class="flex-w flex-sb">
			<div class="w-size13 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
					<div class="wrap-slick3-dots"></div>

					<div class="slick3">
                        
                        <?php foreach($product_details['ProductFile'] as $key=>$dtls){ 
                       // debug($dtls);
                        ?>
                        
						<div class="item-slick3" data-thumb="<?php echo Configure::read('app_root_path').$dtls['file_path']."/".$dtls['file_name']; ?>" alt="<?php echo $product_details['Product']['product_name']; ?>">
							<div class="wrap-pic-w">
								<img src="<?php echo Configure::read('app_root_path').$dtls['file_path']."/".$dtls['file_name']; ?>" alt="<?php echo $product_details['Product']['product_name']; ?>">
							</div>
						</div>
                        <?php } ?>
						
					</div>
				</div>
			</div>

			<div class="w-size14 p-t-30 respon5">
				<h4 class="product-detail-name m-text16 p-b-13">
					<?php echo $product_details['Product']['product_name']; ?>
				</h4>

				<span class="m-text17">
					<i class="fa fa-fw fa-1x">&#xf156;</i> <?php echo $product_details['Product']['unit_price']; ?>
				</span>

				

				<!--  -->
				<div class="p-t-33 p-b-60">
<!--
					<div class="flex-m flex-w p-b-10">
						<div class="s-text15 w-size15 t-center">
							Size
						</div>

						<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
							<select class="selection-2" name="size">
								<option>Choose an option</option>
								<option>Size S</option>
								<option>Size M</option>
								<option>Size L</option>
								<option>Size XL</option>
							</select>
						</div>
					</div>

					<div class="flex-m flex-w">
						<div class="s-text15 w-size15 t-center">
							Color
						</div>

						<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
							<select class="selection-2" name="color">
								<option>Choose an option</option>
								<option>Gray</option>
								<option>Red</option>
								<option>Black</option>
								<option>Blue</option>
							</select>
						</div>
					</div>
-->

					<div class="flex-r-m flex-w p-t-10">
						<div class="w-size16 flex-m flex-w">
							<div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
								<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
								</button>

								<input class="size8 m-text18 t-center num-product" type="number" id="numOfQty" name="num-product" value="1">

								<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>

							<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
								<!-- Button -->
								<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" onclick="javascript:addToCart(<?php echo $product_details['Product']['id']; ?>)">
									Add to Cart
								</button>
							</div>
						</div>
					</div>
				</div>

				<div class="p-b-45">
					<span class="s-text8 m-r-35">SKU: <?php echo $product_details['Product']['sku']; ?></span>
					<span class="s-text8">Categories: <?php echo $product_details['Category']['category_name']; ?>, <?php echo $product_details['SubCategory']['sub_category_name']; ?></span>
				</div>

				<!--  -->
				<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Description
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
							<?php echo $product_details['Product']['product_desc']; ?>
						</p>
					</div>
				</div>

				<div class="wrap-dropdown-content bo7 p-t-15 p-b-14">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Additional information
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
						Brand Name: <?php echo $product_details['Product']['brand_name']; ?> <br>
                        Manufacturer Name: <?php echo $product_details['Product']['manufacturer_name']; ?>  <br>
                        Shelf Life: <?php echo $product_details['Product']['shelf_life']; ?>
						</p>
					</div> 
				</div>

<!--
				<div class="wrap-dropdown-content bo7 p-t-15 p-b-14">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Reviews (0)
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
							Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat
						</p>
					</div>
				</div>
-->
			</div>
		</div>
	</div>


	<!-- Relate Product -->
	<section class="relateproduct bgwhite p-t-45 p-b-138">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 class="m-text5 t-center">
					Related Products
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">
                <?php          
   
                foreach($related_product_details as $item){ ?>
					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
								<img src="<?php echo Configure::read('app_root_path').$item['ProductFile']['0']['file_path']."/".$item['ProductFile']['0']['file_name']; ?>" alt="<?php echo $item['Product']['product_name']; ?>" alt="<?php echo $item['Product']['product_name']; ?>">

								<div class="block2-overlay trans-0-4">
									<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
										<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
										<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
									</a>

									<div class="block2-btn-addcart w-size1 trans-0-4">
										<!-- Button -->
										<?php echo $this->Form->button('Add to cart',array('class'=>'flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4','onClick'=>"javascript:addToCartRelated(".$item['Product']['id'].")"));?>
                                <?php //echo $this->Form->end();?>
                                            
                                       <a href="<?php echo  Configure::read('app_root_path')."detail/productDetail?searchId=".base64_encode($item['Product']['id'])."&name=".$item['Product']['product_name']; ?>" class="listing-item-container compact">   

                                           <button class='flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4' style="margin-top: 5px;">View Details</button>
                                        </a>
                                            
									</div>
								</div>
							</div>

							<div class="block2-txt p-t-20">
								<a href="<?php echo  Configure::read('app_root_path')."detail/productDetail?searchId=".base64_encode($item['Product']['id'])."&name=".$item['Product']['product_name']; ?>" class="block2-name dis-block s-text3 p-b-5">
									<?php echo $item['Product']['product_name']; ?>
								</a>

								<span class="block2-price m-text6 p-r-5">
									₹ <?php echo $item['Product']['unit_price']; ?>
								</span>
							</div>
						</div>
					</div>

                    <?php } ?>
					
                    
                    
				</div>
			</div>

		</div>
	</section>

<script>

 function addToCart(product_id){
        var numOfQty=$('#numOfQty').val();
        data={'product_id':product_id,numOfQty:numOfQty};
         $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/add_product_to_cart",
                data: data,
                success:function(dataCount){
                    $('#cartCounter').html(dataCount);
                }
        });
        
        var dataInput;
         $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/ajax_mini_cart",
                data: dataInput,
                beforeSend:function(html){
                    $('.loading-overlay').show();
                },
                success:function(cartDetails){
                    $('.loading-overlay').hide();
                    $('#ajaxMiniCart').html(cartDetails);
                }
        });
    }
    
 function addToCartRelated(product_id){

        data={'product_id':product_id};
         $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/add_product_to_cart",
                data: data,
                success:function(dataCount){
                    $('#cartCounter').html(dataCount);
                }
        });
        
        var dataInput;
         $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/ajax_mini_cart",
                data: dataInput,
                beforeSend:function(html){
                    $('.loading-overlay').show();
                },
                success:function(cartDetails){
                    $('.loading-overlay').hide();
                    $('#ajaxMiniCart').html(cartDetails);
                }
        });
    }    
</script>
