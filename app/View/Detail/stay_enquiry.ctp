<div class="small-dialog-header"><h3>Enquiry </h3></div>

<?php

 $action = Configure::read('app_root_path')."detail/enquiry";            
  echo $this->Form->create('Booking', array('id'=>'dobook', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'POST','class'=>'form-horizontal ', 'novalidate' => true, 'onsubmit'=>'return doEnquire()'));     
                            
?>
<input type="hidden" id="maxAllowedCount" name="maxAllowedCount" value="0" >
<input type="hidden" id="room_ids" name="room_ids" value="" >
<input type="hidden" id="maxAvailRooms" name="maxAvailRooms" value="0" >
<input type="hidden" id="flagSameRoomAvail" name="flagSameRoomAvail" value="" >
<input type="hidden" class="input-text" name="data[Booking][home_stay_id]" id="home_stay_id" value="<?php echo $home_stay_id; ?>" />
<input type="hidden" id="room_id_selected" name="room_id_selected" value="0" >
<input type="hidden" id="error_flag_enq" name="error_flag_enq" value="1" >
<!--Tabs -->
<div class="sign-in-form style-1" id="tabForm">
		<div class="tabs-container alt">

						<!-- Register -->
			<div class="tab-content" id="tab2" >

				<form method="post" class="register">
					<div class="row">
						<div class="col-md-6">			
							<p class="form-row form-row-wide">
								<label for="username2">Start Date:  		
									<i class="im im-icon-Calendar-3"></i>
									<input type="text" class="input-text datepicker" name="data[Booking][start_date]" id="start_date" data-large-mode="true" data-format="d-m-Y" value="" />
								</label>
							</p>
						</div>
						<div class="col-md-6">		
							<p class="form-row form-row-wide">
								<label for="email2">End Date:
									<i class="im im-icon-Calendar-3"></i>
									<input type="text" class="input-text datepicker" name="data[Booking][end_date]" id="end_date" data-large-mode="true" data-format="d-m-Y" data-default-date="<?php echo date("m-d-Y", time() + 86400); ?>"  />
								</label>
							</p>
						</div>
					</div>	
					<div class="row">
						<div class="col-md-6">
							<p class="form-row form-row-wide">
								<label for="password1">Adults:(age above 12)
									<i class="im im-icon-Administrator"></i>
									<input type="number" class="input-text" name="data[Booking][no_of_adult]" id="no_of_adult" value="1" min="1" onchange="javascript:enquiryFindRoomPriceDetails();" />
								</label>
							</p>
						</div>
						<div class="col-md-6">
							<p class="form-row form-row-wide">
								<label for="password1">Children:(age below 12)
									<i class="im im-icon-Aim"></i>
									<input type="number" class="input-text" name="data[Booking][no_of_children]" id="no_of_children" value=""  min="0"  onchange="javascript:enquiryFindRoomPriceDetails();"  />
								</label>
							</p>
						</div>
					</div>
                     <div class="row" >
				     <div class="col-md-12">
                         <div style="  color: #ff0047;" id="room_error_msg"> </div>
                         <small style="font-weight: bold;color: #af6161;font-size: 90%;">Do you need an extra bed?</small>
                         <input type="checkbox" id="extra_bed" name="extra_bed" class="extra_bed_checkbox" onClick="javascript:enquiryUpdateSelection(0)" >
                          <div id="room_list_enq" style=" overflow-y: scroll;max-height: 200px;"></div>
                       </div>  
                         
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                          <p class="form-row form-row-wide">
								<label for="password1">Final Price: (INR) <small>Per Night</small>
									<i class="im im-icon-Dollar"></i>
									<input type="text" class="input-text" name="data[Booking][final_price]" id="final_price" value=""  style="font-weight:bold;color:red;font-size: x-large;background-color: #e4e4e4;" readonly="readonly"/>
								</label>
							</p>
                         </div>                        
                    </div>
					
					<div class="row">
						<div class="col-md-6">
							<p class="form-row form-row-wide">
								<label for="password1">Name:
									<i class="im im-icon-Remove-User"></i>
									<input type="text" class="input-text" name="data[Booking][full_name]" id="full_name" value="" />
								</label>
							</p>
						</div>
						<div class="col-md-6">
							<p class="form-row form-row-wide">
								<label for="password1">Contact No:
									<i class="im im-icon-Old-Telephone"></i>
								<input type="number" class="input-text" name="data[Booking][contact_no]" id="contact_no" value="" min="0" />
								</label>
							</p>
						</div>
					</div>
					<p class="form-row form-row-wide">
						<label for="password1">Email:
						<i class="im im-icon-Email"></i>
						<input type="text" class="input-text" name="data[Booking][contact_email]" id="contact_email" value="" />
						</label>
					  </p>
						<p class="form-row form-row-wide">
						  <label for="password2">Message:
							<i class="im im-icon-Mail"></i>
							<textarea class="input-text" name="data[Booking][message]" id="message_box"></textarea>
						 </label>
					   </p>
					<p id="errorMsgEnquiry" class="im im-icon-Information" style="display:none; color:red;"></p>
							<input type="button" class="button border fw margin-top-10" name="register" value="Submit" onclick="javascript:doEnquire()" />
	
				</form>
		</div>

	</div>
</div>

<div class="sign-in-form style-1" id="tabMsgBox" style="display:none;" >
		<div class="tabs-container alt">
						<!-- Register -->
			<div class="tab-content" id="not-found" >
				<p style="font"> Received your enquiry. We will get back to you at the earliest. <i class="im im-icon-Thumbs-UpSmiley"></i></p>
			</div>
	</div>
</div>


<script>

$("#start_date").dateDropper();
$('#end_date').dateDropper(<?php date("d-m-Y", time() + 86400); ?>);
$( document ).ready(function() {
	if($('#start_date').val()!="" && $('#end_date').val()!=""){
		 enquiryAvailableRooms();	
		 enquiryFindRoomPriceDetails();
	}
});
	 
function formatDate(date) {
     var d = new Date(date),
         month = '' + (d.getMonth() + 1),
         day = '' + d.getDate(),
         year = d.getFullYear();

     if (month.length < 2) month = '0' + month;
     if (day.length < 2) day = '0' + day;

     return [year, month, day].join('-');
 }    
jQuery('#end_date').change(function(){
   enquiryAvailableRooms();
});    
function enquiryAvailableRooms() {
    var start_date=$('#start_date').val()
    var end_date=$('#end_date').val();
     /* This event will be triggered when second date is selected */
    var check_in_date=moment(start_date,'DD-MM-YYYY').format('YYYY-MM-DD'); //converting to sql format.
    var check_out_date=moment(end_date,'DD-MM-YYYY').format('YYYY-MM-DD');
    var fltrString={"check_in_date": check_in_date,"check_out_date":check_out_date, "homestay_id":homestay_id, "no_of_bedrooms":no_of_bedrooms};        
    var selectedDateJson=JSON.stringify(fltrString);
      $.ajax
          ({
               type: "POST",
               url: "<?php echo Configure::read('app_root_path'); ?>detail/findAvailableRooms",
               data: "selectedDates="+selectedDateJson,
               success: function(result)
                      {
                          var restultOutput=JSON.parse(result);
                          var max_no_of_beds=restultOutput.max_no_of_beds;
                          var max_available_rooms=restultOutput.max_available_rooms;
                          var room_ids=restultOutput.room_ids;
						  var room_idJsonString = JSON.stringify(room_ids);
                          $('#maxAllowedCount').val(max_no_of_beds); 
                          $('#maxAvailRooms').val(max_available_rooms); 
                          $('#room_ids').val(room_idJsonString);   
                          $("#no_of_adult").attr({ "max" : max_no_of_beds,"min" : 1 });
                      //    $("#infantCount").attr({ "max" : (max_available_rooms*2),"min" : 0 });
                          $("#no_of_children").attr({ "max" : 2,"min" : 0 });
                       return 1;
                      }
        }); 
} 
    
  //finding price based on guests.
function enquiryFindRoomPriceDetails(){
    if($('#maxAvailRooms').val()==0){
        enquiryAvailableRooms();
    }
    if($('#start_date').val()=="" || $('#end_date').val()==""){
        
    }else{
    var currentAdultCount=$('#no_of_adult').val();
    var currentChildCount=$('#no_of_children').val();
    var currentInfantCount=0;
    var room_ids=$('#room_ids').val();
    var max_available_rooms=$('#maxAvailRooms').val();
    var extraRoom=0;
    var fltrString={"adultCount": currentAdultCount,"childCount":currentChildCount, "infantCount":currentInfantCount, "room_ids":room_ids,"homestay_id":homestay_id,"extraRoom":extraRoom,"max_available_rooms":max_available_rooms};        
    var priceDetailJson=JSON.stringify(fltrString);
    
    $.ajax
    ({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>detail/findPriceDetails",
          data: "priceDetail="+priceDetailJson,
          success: function(priceDetails)
          {
			if(priceDetails=='error_same_rooms_not_available'){
				 	$("#error_message_display").html('<span style="color:red;">No rooms available.Please try different dates. </span>');			
					$("#error_flag_enq").val("error_same_rooms_not_available");
					
		     }else{
				 $("#error_message_display").html('');
				//console.log(priceDetails);
                var restultOutput=JSON.parse(priceDetails);
                var room_id_selected=restultOutput.roomsSelected;
                var total_amount=restultOutput.final_price;
				var flagSameRoomAvail=restultOutput.flagSameRoomAvail;             
               // $('#total_amount').val(total_amount);
                //$('#PriceActual').html(total_amount);
				$('#flagSameRoomAvail').val(flagSameRoomAvail);
				var checkedflag=null;
				var roomdtls="";
				if(restultOutput.room_dtls !="" ){ ////&& restultOutput.multipleRoomAvail=="Yes"
					$("#room_id").show();
					$("#room_choice").attr('class','panel-dropdown  active');
					roomdtls='<div class="qtyTitle" style="color:black;">Available Room Choices:</div>';
					if(restultOutput.flagSameRoomAvail=="no"){
						roomdtls+='<span style="color:red;">Same room(s) are not available for selected dates. The available choices are below: </span> <br/>';
					}
					$.each(restultOutput.room_dtls, function(keydate, itemArr) {
						if(restultOutput.flagSameRoomAvail=="no"){	
								roomdtls+='<span style="color:#5dadf1;font-weight:bold;">'+keydate+'</span>';
								checkedflag='checked="checked"';
							}
						$.each(itemArr, function(i, item) {
						if(restultOutput.flagSameRoomAvail=="no"){	
							roomdtls+='<label><input type="checkbox"  id="room_'+item.room_price_detail_id+'" onclick="javascript:enquiryUpdateRoom('+item.room_price_detail_id+','+item.final_price+','+"'"+item.room_type+"'"+ ','+item.room_base_price+','+item.extra_bed_charge+','+item.no_of_bed+',' +item.extra_bed+','+"'"+keydate+"'"+'   )"  name="rooms[]" class="option-input checkbox" value="'+item.room_price_detail_id+'" style="width:32px !important; height:20px; margin-bottom: 3px !important;"> '+ item.room_type +' <small>('+item.no_of_bed+': pax  '+item.extra_bed+': extra pax) </small> </label>';
							//' @ INR '+item.room_base_price+
							
						}else{							
							roomdtls+='<label><input type="checkbox" '+checkedflag+' id="room_'+item.room_price_detail_id+'" onclick="javascript:enquiryUpdateRoom('+item.room_price_detail_id+','+item.final_price+','+"'"+item.room_type+"'"+ ','+item.room_base_price+','+item.extra_bed_charge+','+item.no_of_bed+','+item.extra_bed +','+"'"+keydate+"'"+' )"  name="rooms[]" class="option-input checkbox" value="'+item.room_price_detail_id+'" style="width:32px !important; height:20px; margin-bottom: 3px !important;"> '+ item.room_type +' <small>('+item.no_of_bed+': pax  '+item.extra_bed+': extra pax)</small> </label>';
							//' @ INR '+item.room_base_price+
						}
							 //$('#total_amount').val(item.final_price);
							// $('#PriceActual').html(item.final_price);
						});
					});
					if(restultOutput.flagSameRoomAvail=="no" || restultOutput.multipleRoomAvail=="Yes"){	
						var valok="ok";var valcancel="cancel";
						//roomdtls+='<span style="color:red;font-weight:bold;"><input type="button" class="button book-now fullwidth margin-top-5" name="btn_cancel" value="Cancel" style="width:44% !important;float:left;" onClick="javascript:updateSelection('+"'"+valcancel+"'"+ ');"> &nbsp; &nbsp; <input type="button" class="button book-now fullwidth margin-top-5" name="btn_ok" value="OK" style="width:44% !important;float:left;background-color:#54ba1d;" onClick="javascript:updateSelection('+"'"+valok+"'"+ ');"></span>';
                        roomdtls+="<br/>"
						checkedflag='checked="checked"';
					}
					
					$('#room_list_enq').html(roomdtls);
				}else{
					$("#room_id").hide();
					$("#error_flag_enq").val(1);
				}
				
			}
				
         }
     }); 
        
    }
} 
    

function enquiryUpdateSelection(selectItem){

	var selectedRooms=new Array();
	$('#room_list_enq input:checked').each(function() {		
		selectedRooms.push($(this).val());	 	
	});	
	var selectedRoomsString = JSON.stringify(selectedRooms);
	var currentAdultCount=$('#no_of_adult').val();
    var currentChildCount=$('#no_of_children').val();
    var currentInfantCount=0;
    var max_available_rooms=$('#maxAvailRooms').val();
	var flagSameRoomAvail=$('#flagSameRoomAvail').val();
    var extraBed=0;
	var check_in_date=$('#start_date').val();
    var check_out_date=$('#end_date').val();
	if($('#extra_bed').is(':checked')){
        extraBed=1;
   }
	if(selectedRooms.length>0)
	{
    var fltrString={"adultCount": currentAdultCount,"childCount":currentChildCount, "infantCount":currentInfantCount, "selectedRoomsString":selectedRoomsString,"homestay_id":homestay_id, "extraBed":extraBed, "max_available_rooms":max_available_rooms,"flagSameRoomAvail":flagSameRoomAvail };        
    var priceDetailJson=JSON.stringify(fltrString);
	
	$.ajax
        ({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>detail/findFinalPrice",
          data: "details="+priceDetailJson,
          success: function(result)
            {
				var restultOutput=JSON.parse(result);
				var error_msg=restultOutput.error_msg;
				if(error_msg=="not_enough_bed_count"){
					$('#room_error_msg').html("**Choose more rooms to accomadate all guests.");
                    $('#error_flag_enq').val(1);
				}else{
					var room_id_selected=restultOutput.roomsSelected;
					var final_booking_price=restultOutput.final_booking_price;
					var room_chosen_string=restultOutput.room_chosen_string;
					priceXEnquiry(homestay_id,check_in_date,check_out_date,final_booking_price,room_id_selected,room_chosen_string);
				}
           }
       }); 
	}else{
		$('#room_error_msg').html("Please select the room(s).");
        $('#error_flag_enq').val(1);		
	}
}
	
function priceXEnquiry(homestay_id,check_in_date,check_out_date,final_booking_price,room_id_selected,room_chosen_string){
	var priceXString={"homestay_id":homestay_id,"check_in_date":check_in_date,"check_out_date":check_out_date,"base_price_per_day": final_booking_price};
	var priceXJson=JSON.stringify(priceXString);
	var response=0;
	
	$.ajax
        ({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>pricing_x/bestPrice",
          data: "priceXJson="+priceXJson,
          success: function(resultPriceX)
            {	if(resultPriceX==0){
					resultPriceX=final_booking_price;
				}			
				$('#room_id_selected').val(room_id_selected);
				$('#final_price').val(resultPriceX);					
				$('#room_error_msg').html("");
                $('#error_flag_enq').val(0);	
			 },
			 complete: function(){
				//$('#priceLoader').hide();
				 //$('#priceDisplay').show();
			  }
       }); 
	//return response;
}
var selectedRooms=new Array();
var room_beds=0; var room_final_price=0; var room_chosen_string="";
var selected_room_ids='';
function enquiryUpdateRoom(room_id,final_price,room_type,room_base_price,extra_bed_charge,no_of_bed,extra_bed){		
	if($('#extra_bed').prop('checked')==true){
		room_beds=(parseInt(no_of_bed) + parseInt(extra_bed));	
	}else{
		room_beds=(parseInt(no_of_bed));	
	}	
	selected_room_ids+=selected_room_ids+"_"+room_id;
	 var currentAdultCount=$('#no_of_adult').val();
    var currentChildCount=$('#no_of_children').val();
    var currentInfantCount=0;
	var no_of_guest=(parseInt(currentAdultCount));
	if(room_beds >= no_of_guest){
		//$("#room_choice").attr('class','panel-dropdown');
	}
	 enquiryUpdateSelection('ok');
 } 

</script>
