 <div class="small-dialog-header" style="margin-bottom:0px; color:black;">
		<h3>Upcoming Availability</h3>
    </div>
    <div id="listing-pricing-list" class="listing-section">
        <div class="show-more">
				<div class="pricing-list-container">
                    <?php foreach($experienceSlots as $slots) {
                    
                        if($slots['ExperienceSlot']['no_of_required_slots'] <= $slots['ExperienceSlot']['freeSlot']){
                   
                            $action = Configure::read('app_root_path')."booking/experience";            
                            echo $this->Form->create('Booking', array('id'=>'dobook', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'get','class'=>'form-horizontal ', 'novalidate' => true));     
                            
                            echo $this->Form->input('experience_slot_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'experience_slot_id','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['id']));
                            
                            echo $this->Form->input('experience_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'experience_id','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['experience_id']));
                            
                            echo $this->Form->input('price',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'price','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['price']));
							
							echo $this->Form->input('final_price',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'price','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['final_price']));
                            
                            echo $this->Form->input('date',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'date','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['checkindate']));
                            
                            echo $this->Form->input('slot_start_time',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'slot_start_time','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['slot_start_time']));
                            
                            echo $this->Form->input('slot_end_time',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'slot_end_time','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['slot_end_time']));
                            
                            echo $this->Form->input('no_of_people',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'no_of_people','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['no_of_people']));
							
							echo $this->Form->input('min_guest',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'min_guest','autofocus','error'=>false,'value'=>$slots['ExperienceSlot']['min_guest']));
                    ?> 
                                <div class="_1f9rmq80">
                                    <div class="_e296pg">
                                        <div>
                                            <div class="_rotqmn2"> 
                                                <?php echo date('D, dS F Y ' ,strtotime($slots['ExperienceSlot']['checkindate'])) ."  "; ?>
                                                <span style="float:right; marging-right:200px !important; color:#008489; right: 70px !important;">Guest: &nbsp;&nbsp;
                                                 <input name="data[Booking][no_of_guest]" type="number" min="1" max="<?php echo $slots['ExperienceSlot']['freeSlot'];?>" id="no_of_guest"  value="<?php echo $slots['ExperienceSlot']['no_of_required_slots'];?>" style="max-width:50px; width:50px; height:34px; padding:2px !important; float: n; margin-right:100px;" ></span>
                                            </div>
                                            <div>
                                                <div style="margin-top: 8px;">
                                                    <div class="_150a3jym">
                                                        <?php echo $slots['ExperienceSlot']['slot_start_time']." − ". $slots['ExperienceSlot']['slot_end_time']; ?> <i class="fa fa-fw fa-1x">&#xf156;</i><?php echo number_format($slots['ExperienceSlot']['final_price']); ?> per person
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div> 
                                        
                                        <div class="_13k6a2k">                                      
                                            
                                            <button type="submit" class="_annhcu9" aria-busy="false">Choose</button>
                                        </div>
                                    </div>
                                </div>
                         <?php echo $this->Form->end(); ?> 
                        <?php }
                        } ?>
				</div>
            </div>
        <a href="#" class="show-more-button" data-more-title="Show More" data-less-title="Show Less"><i class="fa fa-angle-down"></i></a>
     </div>  