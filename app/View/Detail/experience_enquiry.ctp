<div class="small-dialog-header"><h3>Enquiry </h3></div>

<?php

 $action = Configure::read('app_root_path')."detail/enquiry";            
  echo $this->Form->create('Booking', array('id'=>'dobook', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'POST','class'=>'form-horizontal ', 'novalidate' => true, 'onsubmit'=>'return doExperienceEnquiry()'));     
                            
?>
<input type="hidden" class="input-text" name="data[Booking][experience_id]" id="experience_id" value="<?php echo $experience_id; ?>" />
<input type="hidden" id="experience_slot_id" name="experience_slot_id" value="0" >
<!--Tabs -->
<div class="sign-in-form style-1" id="tabForm">
		<div class="tabs-container alt">

						<!-- Register -->
			<div class="tab-content" id="tab2" >

				<form method="post" class="register">
					<div class="row">
						<div class="col-md-6">			
							<p class="form-row form-row-wide">
								<label for="username2"> Date:  		
									<i class="im im-icon-Calendar-3"></i>
									<?php if($exprnce_dtls['Experience']['experience_type']==5) { 
									 		$idFlag='event_date';
									?>
									<input type="text" class="input-text" name="data[Booking][start_date]" id="<?php echo $idFlag;?>" data-large-mode="true" data-format="d-m-Y" value="" readonly="readonly"  />
									<?php } else {
											  $idFlag="start_date";
									?>
									<input type="text" class="input-text datepicker" name="data[Booking][start_date]" id="<?php echo $idFlag;?>" data-large-mode="true" data-format="d-m-Y" value=""  />
									<?php } ?>
								
								</label>
							</p>
						</div>
						<div class="col-md-6">
							<p class="form-row form-row-wide">
								<label for="password1">Adults:(age above 12)
									<i class="im im-icon-Administrator"></i>
									<input type="number" class="input-text" name="data[Booking][no_of_adult]" id="no_of_adult" value="1" min="1" onchange="javascript:findAvailableSlots();" />
								</label>
							</p>
						</div>
					</div>	
                   <div class="row" >
				     <div class="col-md-12">
                         <div style="  color: #ff0047;" id="room_error_msg"> </div>
                          <div id="slot_list" style=" overflow-y: scroll;max-height: 200px;"></div>
                       </div>  
                         
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                          <p class="form-row form-row-wide">
								<label for="password1">Final Price: (INR) <small>Per Night</small>
									<i class="im im-icon-Dollar"></i>
									<input type="text" class="input-text" name="data[Booking][final_price]" id="final_price" value=""  style="font-weight:bold;color:red;font-size: x-large; background-color: #e4e4e4;" readonly="readonly"/>
								</label>
							</p>
                         </div>                        
                    </div>
					
					<div class="row">
						<div class="col-md-6">
							<p class="form-row form-row-wide">
								<label for="password1">Name:
									<i class="im im-icon-Remove-User"></i>
									<input type="text" class="input-text" name="data[Booking][full_name]" id="full_name" value="" />
								</label>
							</p>
						</div>
						<div class="col-md-6">
							<p class="form-row form-row-wide">
								<label for="password1">Contact No:
									<i class="im im-icon-Old-Telephone"></i>
								<input type="number" class="input-text" name="data[Booking][contact_no]" id="contact_no" value="" min="0" />
								</label>
							</p>
						</div>
					</div>
					<p class="form-row form-row-wide">
						<label for="password1">Email:
						<i class="im im-icon-Email"></i>
						<input type="text" class="input-text" name="data[Booking][contact_email]" id="contact_email" value="" />
						</label>
					  </p>
						<p class="form-row form-row-wide">
						  <label for="password2">Message:
							<i class="im im-icon-Mail"></i>
							<textarea class="input-text" name="data[Booking][message]" id="message_box"></textarea>
						 </label>
					   </p>
					<p id="errorMsgEnquiry" class="im im-icon-Information" style="display:none; color:red;"></p>
							<input type="button" class="button border fw margin-top-10" name="register" value="Submit" onclick="javascript:doExperienceEnquiry()" />
	
				</form>
		</div>

	</div>
</div>

<div class="sign-in-form style-1" id="tabMsgBox" style="display:none;" >
		<div class="tabs-container alt">
						<!-- Register -->
			<div class="tab-content" id="not-found" >
				<p style="font"> Received your enquiry. We will get back to you at the earliest. <i class="im im-icon-Thumbs-UpSmiley"></i></p>
			</div>
	</div>
</div>
<?php echo $this->Html->css('plugins/datedropper.css'); ?>
<?php echo $this->Html->script('scripts/datedropper.js'); ?>

<script>

$("#start_date").dateDropper();
$( document ).ready(function() {
	if(($('#start_date').val()!="" || $('#event_date').val()!="" ) && $('#adultCount').val()){
		findAvailableSlots();
		
	}
});

function findAvailableSlots(){
	 var currentAdultCount=$('#adultCount').val();  
	var checkindate=$('#checkInbtn').val();  
    if(currentAdultCount<1){
        alert("Please choose no of people?.");
    }
    var fltrString={"checkindate": checkindate,"adultCount": currentAdultCount,"experience_id":experience_id};        
    var detailJson=JSON.stringify(fltrString);
	
	$.ajax
		({
			type: "POST",
			url: "<?php echo Configure::read('app_root_path'); ?>detail/slot_availability",
			data: "detailJson="+detailJson,
			success: function(responseAvailability)
			{
					$('#slot_list').html(responseAvailability);
			}
		  }); 
}

function updatePrice(slot_id,stat_time,end_time,price){
	$('#experience_slot_id').val(slot_id);
	$('#final_price').val(price);
}
function formatDate(date) {
     var d = new Date(date),
         month = '' + (d.getMonth() + 1),
         day = '' + d.getDate(),
         year = d.getFullYear();

     if (month.length < 2) month = '0' + month;
     if (day.length < 2) day = '0' + day;

     return [year, month, day].join('-');
 }    

</script>
