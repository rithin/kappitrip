
<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient" style="box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.12);  padding: 25px 0 !important; margin-bottom: 25px !important;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Enquiry Response</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Enquiry Response</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">

	   <div class="row">

		<!-- Contact Details -->
		<div class="col-md-5">

			<h4 class="headline margin-bottom-30">Find Us There</h4>

			<!-- Contact Details -->
			<div class="sidebar-textbox">
				<p>If you are facing any trouble to mark your enquiry acceptance/rejection, please contact our customer care. Coorgexpress.com will be happy to help you 24/7.</p>

				<ul class="contact-details">
					<li><i class="im im-icon-Phone-2"></i> <strong>Phone:</strong> <span>+91 96323 38111 / +91 97418 30222 </span></li>
					<li><i class="im im-icon-Globe"></i> <strong>Web:</strong> <span><a href="https://www.coorgexpress.com">www.coorgexpress.com</a></span></li>
					<li><i class="im im-icon-Envelope"></i> <strong>E-Mail:</strong> <span><a href="mailto:info@coorgexpress.com"> info@coorgexpress.com  </a></span></li>
				</ul>
			</div>

		</div>

		<!-- Contact Form -->
		<div class="col-md-7">
			<section id="contact">
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-9">
                    <span style="color:red; font-size:x-large;" id="displayMessageFrm"><?php echo $success_msg; ?></span>
                    </div>
                </div>				 
			</section>
		</div>
		<!-- Contact Form / End -->

	</div>
</div>


<style>
.widget-tabs li {
	padding: 0 0 12px 0 !important;
    margin: 4px 0 0 0 !important;
	
}
.list_team {
    list-style-type: square;
    color: red;
}
.widget h3 {
	color:red;
}
.blog-post{
	text-align: justify;
	}
.post-meta li{
		float:left !important;
	}
.col-lg-3, .col-md-4{
		padding-right: 0px !important; 
	}
</style>
