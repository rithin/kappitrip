
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

<?php echo $this->Html->css('daterangepicker.min.css'); ?>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js" type="text/javascript"></script>
<?php echo $this->Html->script('longbill_calender/jquery.daterangepicker.js'); ?>

<!-- Slider
================================================== -->
<div class="listing-slider mfp-gallery-container margin-bottom-0">
   <?php 
	//debug($experienceDetails['ExperienceFile']);
    if(count($experienceDetails['ExperienceFile'] )>0){
        foreach($experienceDetails['ExperienceFile'] as $files) { 
       // debug($files);
        if(is_array($files)){
            $imageUrl = Configure::read('app_root_path').$files['file_path'].'/'.$files['file_name'];
        ?>
        <a href="<?php echo $imageUrl;?>" data-background-image="<?php echo $imageUrl;?>" class="item mfp-gallery" title="<?php echo  $files['file_name']; ?>"></a>
        <?php
                }
          } 
    }
   ?>
	
</div>

<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
		<div class="col-lg-8 col-md-8 padding-right-30">

			<!-- Titlebar -->
			<div id="titlebar" class="listing-titlebar">
				<div class="listing-titlebar-title">
                    <h2><?php echo $experienceDetails['Experience']['name'];?><span class="listing-tag"><b>Starting from  INR <?php echo @$experienceSlots[0]['ExperienceSlot']['price'];?> </b></span></h2>
					<span>
						<a href="#listing-location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							<?php echo $experienceDetails['Experience']['location'];?>
						</a>
					</span>
					<div class="star-rating" data-rating="<?php echo $experienceDetails['Experience']['rating'];?>">
<!--						<div class="rating-counter"><a href="#listing-reviews">(31 reviews)</a></div>-->
					</div>
                    
                    <ul class="share-buttons margin-top-40 margin-bottom-0" style="font-family: sans-serif;
    font-size: 18px;">
						<li><i class="im im-icon-Turtle"></i>  &nbsp;<?php echo $experienceDetails['TrailType']['name'];?>  &nbsp;</li>	
						<li><i class="im im-icon-Hour"></i>  &nbsp;<?php echo $experienceDetails['Experience']['duration'];?> Hours &nbsp;</li>	
<!--
						<li><i class="fa fa-bed" style="font-size:18px"></i> &nbsp;<?php // echo $no_of_bed;?> Beds &nbsp;</li>	
						<li><i class="fa fa-bath" style="font-size:18px"></i> &nbsp;<?php// echo $homestayDetails['Homestay']['no_of_bathrooms'];?> Baths</li>
-->
					</ul>
					
                    
				</div>
			</div>

			<!-- Listing Nav -->
			<div id="listing-nav" class="listing-nav-container">
				<ul class="listing-nav">
					<li><a href="#listing-overview" class="active">Overview</a></li>
					<li><a href="#listing-pricing-list">Pricing</a></li>
					<li><a href="#listing-more">More Details</a></li>
					<li><a href="#listing-location">Location</a></li>
					
				</ul>
			</div>
			
			<!-- Overview -->
			<div id="listing-overview" class="listing-section">

				<!-- Description -->

				<p>
                    <?php echo $experienceDetails['Experience']['about_experience'];?>
				</p>

				<!-- Amenities -->
				<h3 class="listing-desc-headline">Inclusion</h3>
				<ul class="listing-features checkboxes margin-top-0">
                    <?php 
                        $includes_in=explode(',',$experienceDetails['Experience']['includes_in']);
                        foreach($includes_in as $include){
                            
                            echo "<i class='sl sl-icon-drop'></i> ".$include." </br>";
                        }
                    
                    
                    ?>
				</ul>
				<h3 class="listing-desc-headline">Who can attend</h3>
					<ul class="listing-features checkboxes margin-top-0">
						  <?php echo $experienceDetails['Experience']['who_can_attend'];?>
					</ul>
				
			</div>


			<!-- Food Menu -->
			<div id="listing-pricing-list" class="listing-section">
				<h3 class="listing-desc-headline margin-top-10 margin-bottom-05">Timings</h3>

				<div class="show-more" style="height:auto !important;">
					<div class="pricing-list-container">
						
						<!-- Food List -->
						<h4>Upcoming Availability</h4>
						<ul>
                            <?php foreach($experienceSlots as $slots) { ?>
							<li> 
								<p>  <?php echo $slots['ExperienceSlot']['slot_start_time']." − ". $slots['ExperienceSlot']['slot_end_time']; ?></p>
								<span><i class="fa fa-fw fa-1x">&#xf156;</i>  <?php echo $slots['ExperienceSlot']['price'];?></span>
							</li>
                            <?php } ?>
							
						</ul>

						
					</div>
				</div>
				<a href="#" class="show-more-button" data-more-title="Show More" data-less-title="Show Less"><i class="fa fa-angle-down"></i></a>
			</div>
			<!-- Food Menu / End -->

			
			<!-- More Details -->
			<div id="listing-more" class="listing-section" style="padding: 5px;background-color: #f9f9f9;padding-left: 20px;
    padding-right: 20px;">
				<h3 class="listing-desc-headline">Tour Notes</h3>
				<p>
                    <?php echo $experienceDetails['Experience']['notes'];?>
				</p>
				<h3 class="listing-desc-headline">Things to Bring/Carry</h3>
				<ul class="list-4 color">
                    <?php echo $experienceDetails['Experience']['things_to_bring'];?>
				</ul>
				
				<h3 class="listing-desc-headline">Cancellation Policy</h3>
				<ul class="list-4 color">
                    <?php echo $experienceDetails['Experience']['cancellation_policy'];?>
				</ul>
				
			</div>
			
			
		
			<!-- Location -->
			<div id="listing-location" class="listing-section">
				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Location</h3>

				<div id="singleListingMap-container">
					<div id="singleListingMap" data-latitude="<?php echo $experienceDetails['Location']['latitude'];?>" data-longitude="<?php echo $experienceDetails['Location']['longitude'];?>" data-map-icon="im im-icon-Running-Shoes"></div>
					<a href="#" id="streetView">Street View</a>
				</div>
			</div>
			
	</div>

        

		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-75 sticky">
			<!-- Verified Badge -->
			<div class="verified-badge with-tip" data-tip-content="Listing has been verified and belongs the business owner or manager.">
				<?php 
					$offer_text="";
					if($experienceDetails['Deal']['id']!=''){
						if($experienceDetails['Deal']['offer_type']=='percentage')
								$offer_text=$experienceDetails['Deal']['offer_value']."% OFF from ".date('jS-M',strtotime($experienceDetails['Deal']['start_date']))." to ".date('jS-M',strtotime($experienceDetails['Deal']['end_date']));
						else
								$offer_text=$experienceDetails['Deal']['offer_value']."Rs OFF from ".date('jS-M',strtotime($experienceDetails['Deal']['start_date']))." to ".date('jS-M',strtotime($experienceDetails['Deal']['end_date']));
					}else{
						$offer_text="Verified Listing";
					}
				?>
				<i class="sl sl-icon-check"></i><?php echo $offer_text; ?>
			</div>

			<!-- Book Now -->
          <?php                         
               $action = Configure::read('app_root_path')."booking/experience";
               echo $this->Form->create('Booking', array('id'=>'bookingDetail', 'url'=>$action, 'autocomplete'=>'off', 'type'=>'get','class'=>'form-horizontal ', 'novalidate' => true )); 
			
                echo $this->Form->input('experience_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'experience_id','autofocus','error'=>false,'value'=>$experienceDetails['Experience']['id']));    
				
				echo $this->Form->input('experience_type',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'experience_type','autofocus','error'=>false,'value'=>$experienceDetails['Experience']['experience_type']));    
            ?>
	
			<div class="boxed-widget booking-widget margin-top-35">
					
                <h3><span class="priceDisplay"><i class="fa fa-fw fa-1x">&#xf156;</i>
                    <span id="PriceActual" style="font-size: 23px;">
                    <?php 
						if($experienceDetails['Experience']['experience_type']==5){
							echo $experienceDetails['Experience']['event_price'];
							
							echo $this->Form->input('event_price',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'event_price','autofocus','error'=>false,'value'=>$experienceDetails['Experience']['event_price'])); 
							
							echo $this->Form->input('slot_start_time',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'slot_start_time','autofocus','error'=>false,'value'=>$experienceDetails['Experience']['event_time'])); 
							
							echo @$this->Form->input('experience_slot_id',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'experience_slot_id','autofocus','error'=>false,'value'=>$experienceSlots[0]['ExperienceSlot']['id'])); 
							
							echo @$this->Form->input('maxAllowedCount',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'maxAllowedCount','autofocus','error'=>false,'value'=>$experienceSlots[0]['ExperienceSlot']['no_of_people'])); 
							
						}else{						
							echo @$experienceSlots[0]['ExperienceSlot']['price'];
							
							echo $this->Form->input('maxAllowedCount',array('div'=>false, 'type'=>'hidden', 'label'=>false, 'id'=>'maxAllowedCount','autofocus','error'=>false,'value'=>$experienceSlots[0]['ExperienceSlot']['no_of_people'])); 
							
						
						}  ?>
                    </span>
                    <small>per person</small></span>
                </h3>
			<?php if($experienceDetails['Experience']['instant_book']=="Yes") { ?>
            <div class="row with-forms  margin-top-0">
                <!-- Panel Dropdown -->
				
				<div class="col-lg-12">

                    <div class="panel-dropdown">
<!--				        <a href="#" class="checkInbtn">Check in – Check out</a>-->
						<?php if($experienceDetails['Experience']['experience_type']==5) {  
						
						echo $this->Form->input('eventdate',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'eventdate','autofocus','error'=>false,'readonly', 'value'=>$experienceDetails['Experience']['event_date'],'style'=>"background-color: #f9f9f9;font-weight: bold;border: none;box-shadow: none;
    font-size: x-large; padding-left: 3px;"));   
							?> 
						
						  <input id="checkInbtn" class="checkInbtn" size="60" placeholder="Choose Dates" value="" type="hidden"> 
						<?php
						
						 }else{ ?>
						  <input id="checkInbtn" class="checkInbtn" placeholder="Choose Dates" size="60" value=""> 
						
						<?php } ?>
                    </div>
                    
					<!-- Time Picker - docs: http://www.vasterad.com/docs/listeo/#!/time_picker -->
<!--
					<div class="col-lg-6 col-md-12">
                        Ex. Check-In
						<input type="text" id="booking-time" value="9:00 am">
					</div>
-->
                    </div>
				
				
				
				
				<div class="col-lg-12">                        
						<div class="panel-dropdown" id="guestQuanityDropDown">
							<a href="#">Guests <span class="qtyTotal" name="qtyTotal">1</span></a>
							<div class="panel-dropdown-content">
                                <input type="hidden" id="maxAllowedCount" name="maxAllowedCount" value="" >  
								<!-- Quantity Buttons -->
								<div class="qtyButtons">
									<div class="qtyTitle">No Of Persons</div>
									<?php if($experienceDetails['Experience']['experience_type']==5) {  
						
									 echo $this->Form->input('adultCount',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'adultCount','autofocus','min'=>1,'max'=>$experienceSlots[0]['ExperienceSlot']['no_of_people'],'error'=>false,'value'=>1,'class'=>'qtyInput'));   

									 }else{
									echo $this->Form->input('adultCount',array('div'=>false, 'type'=>'text', 'label'=>false, 'id'=>'adultCount','autofocus','min'=>$experienceSlots[0]['ExperienceSlot']['min_guest'],'max'=>$experienceSlots[0]['ExperienceSlot']['no_of_people'],'error'=>false,'value'=>$experienceSlots[0]['ExperienceSlot']['min_guest'],'class'=>'qtyInput'));   

									} ?>
								</div>                                

							</div>
				    </div>
				</div>
					<!-- Panel Dropdown / End -->
					<div class="col-lg-6">
						<?php if($experienceDetails['Experience']['experience_type']==5) { ?> 
								<input type="submit" class="button book-now fullwidth margin-top-5" name="btn_booking" value="Book Now" />
						<?php }else{ ?>

							<button type="button" data-mfp-src="#see_dates_exprnce" class="button book-now fullwidth margin-top-5 button-popup-link" id="seeDates">See Dates</button>
						<?php } ?>
					</div>
					<div class="col-lg-6">
						<input type="button" class="button book-now fullwidth margin-top-5 sign-in popup-with-zoom-anim" name="btn_enquiry" value="Enquiry" onclick="javascript:raiseEnquiry()" style="background-color:green;" />
					</div>
            </div>
			
			<?php }else{ ?>	
			
				<input type="button" class="button book-now fullwidth margin-top-5 sign-in popup-with-zoom-anim" name="btn_enquiry" value="Enquiry" onclick="javascript:raiseEnquiry()"  />
			
			
			<?php } ?>

		</div>
			
		
            <?php echo $this->Form->end(); ?>    
			<!-- Book Now / End -->
		


			<!-- Contact -->
			<div class="boxed-widget margin-top-35">
				<div class="hosted-by-title">
					<h4><span>Hosted by</span><?php echo ucfirst($experienceDetails['Experience']['hosted_by']);?></h4>
					
				</div>
				<ul class="listing-details-sidebar">
					<li><i class="sl sl-icon-phone"></i><?php echo Configure::read('enquiry_phone');?></li>
					<!-- <li><i class="sl sl-icon-globe"></i> <a href="#">http://example.com</a></li> -->
					<li><i class="fa fa-envelope-o"></i> <a href="mailto:<?php echo Configure::read('enquiry_email');?>"><?php echo Configure::read('enquiry_email');?></a></li>
				</ul>

                
                
			</div>
			<!-- Contact / End-->

            	<div class="listing-share margin-top-40 margin-bottom-40 no-border">
<!--
				<button class="like-button"><span class="like-icon"></span> Bookmark this listing</button> 
				<span>159 people bookmarked this place</span>
-->
                <?php 
                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                ?>
					<!-- Share Buttons -->
					<ul class="share-buttons margin-top-40 margin-bottom-0">
						<li>
                             <div class="fb-share-button fb-share" data-href="<?php echo $actual_link; ?>" data-layout="button_count" data-size="large"> </div>
                            </li>
<!--
                            
                             <a class="fb-share" href="#"><i class="fa fa-facebook"></i> Share</a>
						<li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
						<li><a class="gplus-share" href="#"><i class="fa fa-google-plus"></i> Share</a></li>
						<li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li> 
-->
					</ul>
					<div class="clearfix"></div>
			</div>
			

		</div>
		<!-- Sidebar / End -->

	</div>
</div>
<div id="enquiryForm" class="zoom-anim-dialog mfp-hide"></div>

<!-- Maps -->


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7AcWtZ7YewGNJfCNQYLDgwYiLzgdNQlE&libraries=geometry"></script>
<?php echo $this->Html->script('scripts/infobox.min.js'); ?>
<?php echo $this->Html->script('scripts/markerclusterer.js'); ?>
<?php echo $this->Html->script('scripts/maps.js'); ?>



<?php $current_base_url= Configure::read('app_root_path')."handpicked-experiences/".$experienceDetails['Experience']['seo_unique_url']."?searchId=".$experienceDetails['Experience']['id']."&name=".$experienceDetails['Experience']['name']; ?>
<script>
    
//var url_rewrite = document.location.href;
var url_rewrite = "<?php echo $current_base_url;?>";
var experience_id="<?php echo $experienceDetails['Experience']['id'];?>";    
// managing the guests drop dowm close and open.
var mouse_is_inside = false;
$('#guestQuanityDropDown').hover(function(){
     mouse_is_inside=true;
}, function(){
  mouse_is_inside=false;  
});
var disabled_dates_arr = [<?php  foreach ($disabledDates as $disableDate) { echo "'".$disableDate."',"; } ?> ];
	

$(function() { 
	
	if (!window['console'])
	{
		window.console = {};
		window.console.log = function(){};
	}
	<?php if($experienceDetails['Experience']['instant_book']=="Yes") { ?>	// intializing calender only if instant book is enabled.
	$('.checkInbtn').dateRangePicker(
   {
		autoClose: true,
		singleDate : true,
		showShortcuts: false,
        startDate: new Date(),
		selectForward: true,
        format: 'DD-MM-YYYY',
		beforeShowDay: function(t)
		{
            current_date= moment(t).format('DD-MM-YYYY');
           //var valid = !(t.getDay() == 0 || t.getDay() == 6);  //disable saturday and sunday
            var valid = "";
                if(disabled_dates_arr.indexOf(current_date) == -1)
                    valid=true;
                else
                    valid=false;
			var _class = '';
			var _tooltip = valid ? '' : 'Not Available';           
			return [valid,_class,_tooltip];     
		},
        onSelect: function(date, instance) {
             
        },
    })
	.bind('datepicker-change',function(event,obj)
     {
		  var check_in_date=moment(obj.date1).format('YYYY-MM-DD'); //converting to sql format.

		 $("#guestQuanityDropDown").attr('class','panel-dropdown active');
	 });
	
	<?php } ?>
	
	
});   	
	
	
$( document ).ready(function() {		
		$('#seeDates').click(function(evt){			
			 if($('#checkInbtn').val()==""){
				evt.stopPropagation();
				$('#checkInbtn').data('dateRangePicker').open();
			 }else{	
				$('.button-popup-link').magnificPopup({
				  type:'inline',
				  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
					});	
				}
	   });	
});  
	
    
$("body").mouseup(function(){
   if(! mouse_is_inside && $("#guestQuanityDropDown").is(".active")) {
        findExperienceAvailability();
   }
    
});    
	
$('#guestQuanityDropDown').click(function(evt){	
	if($('#experience_type').val()!=5){
		if($('#checkInbtn').val()==""){
			$("#guestQuanityDropDown").attr('class','panel-dropdown  ');
			evt.stopPropagation();
			$('#checkInbtn').data('dateRangePicker').open();
		}	
	}
});
	
// enquiry form ajax

function raiseEnquiry(){
	
	 $.ajax
    ({
          type: "POST",
          url: "<?php echo Configure::read('app_root_path'); ?>detail/experience_enquiry",
          data: "experience_id="+experience_id,
          success: function(enquiryForm)
          {	  
			  $('#errorMsgEnquiry').hide();
			  $('#tabMsgBox').hide();
			  $('#tabForm').show();			  
			  $("#enquiryForm").html(enquiryForm);
			 // $("#enquiryForm").dialog("open");
			  $.magnificPopup.open({
				  items: {
					src: '#enquiryForm'
				  },
				  type: 'inline'
				});
			  
			 	if($('#checkInbtn').val()!=""){
						var start_date=$('#checkInbtn').val();
						$('#start_date').val(start_date);
				}
			  if($('#eventdate').val()!=""){
						var start_date=$('#eventdate').val();
						$('#event_date').val(start_date);
				}
				
				if($('#adultCount').val()!=""){
						var no_of_adult=$('#adultCount').val();
						$('#no_of_adult').val(no_of_adult);
				}
				
			  
		  }
     
	 }); 
}


function doExperienceEnquiry(){
		var start_date=""; 
		if($('#start_date').val()!="" && $('#start_date').val()!=undefined)
		 start_date=$('#start_date').val();
		else
		 start_date=$('#event_date').val();	
	
		var no_of_adult=$('#no_of_adult').val();
		var no_of_children=0;
		var full_name=$('#full_name').val();
		var contact_no=$('#contact_no').val();
		var contact_email=$('#contact_email').val();
		var message_box=$('#message_box').val();
		var experience_id=$('#experience_id').val();
        var experience_slot_id=$('#experience_slot_id').val();
        var final_price=$('#final_price').val();
       // console.log(start_date);
		if(start_date=="" || no_of_adult=="" ||  full_name=="" || contact_no=="" || contact_email=="" || message_box==""  ){
			$('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('  Please fill all the fields with relevant data.  ');		
		}
        else if(experience_slot_id==0 || experience_slot_id=="") {
            findAvailableSlots();
            $('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('  Please select the slot.  ');
            e.preventDefault();
        }
        
		else if ($.trim(contact_email).length == 0) {
			$('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('  Please fill a valid email address.  ');
            e.preventDefault();
        }
        else if (validateEmail(contact_email)==false) {
			$('#errorMsgEnquiry').show();
			$('#errorMsgEnquiry').html('   Please fill a valid email address.  ');
            e.preventDefault();
        }
		else{
            
			$('#errorMsgEnquiry').hide();
			var formData={"start_date": start_date, "no_of_adult":no_of_adult,"no_of_children":no_of_children, "full_name":full_name, "contact_no":contact_no,"contact_email":contact_email, "message_box":message_box, "experience_id":experience_id,"experience_slot_id":experience_slot_id,"final_price":final_price };        
			var formDataJson=JSON.stringify(formData);

			 $.ajax
			({
				  type: "POST",
				  url: "<?php echo Configure::read('app_root_path'); ?>detail/doExperienceEnquiry",
				  data: "formDataJson="+formDataJson,
                  timeout: 30000,
                  error: function() {
                        return true;
                   },
                  beforeSend: function(){
                        $('div#dim').fadeIn(400);
                  },  
                  complete : function(){
                        $('div#dim').fadeOut(200);

                  },
				  success: function(msgData)
				  {
					  if(msgData==1){ 
						  $('#errorMsgEnquiry').hide();
						  $('#tabForm').hide();
						  $("#enquiryForm").css('max-width','800px');
						  $("#not-found i").css('font-size','98px');
						  $("#not-found").css('margin-top','-42px');
						  $('#tabMsgBox').show();
					  } 
					  $('#tabForm').hide();
				  }
			});

		}
	
}
    //finding price based on guests.
function findExperienceAvailability(){ 
	
    var currentAdultCount=$('#adultCount').val();  
	var checkindate=$('#checkInbtn').val();  
    if(currentAdultCount<1){
        alert("Please choose no of people?.");
    }
    var fltrString={"checkindate": checkindate,"adultCount": currentAdultCount,"experience_id":experience_id};        
    var detailJson=JSON.stringify(fltrString);
     if(url_rewrite.indexOf("?") > -1) {
            url_rewrite = url_rewrite+"&adultCount="+currentAdultCount+"&checkindate="+checkindate+"&adultCount="+ currentAdultCount+"&experience_id="+experience_id;
     }
     window.history.pushState({path:url_rewrite},'',url_rewrite);
   
		$.ajax
		({
			type: "POST",
			url: "<?php echo Configure::read('app_root_path'); ?>detail/experience_availability",
			data: "detailJson="+detailJson,
			success: function(responseAvailability)
			{
					$('#see_dates_exprnce').html(responseAvailability);
			}
		  }); 
	 
} 
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

</script> 

<style >
	@media (max-width: 1600px) {
		.date-picker-wrapper{
			   top: 831.984px;
		}
	}
	@media (max-width: 767px) {
		.date-picker-wrapper{
			top: 4178px;
		}
	}
    .date-picker-wrapper{
        z-index: 999 !important;
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    }
    .checkInbtn{
        font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    }
    .priceDisplay{
        margin: 0px !important;
    word-wrap: break-word !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 22px !important;
    line-height: 28px !important;
    letter-spacing: -0.2px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #484848 !important;
    font-weight: 700 !important;
    display: inline !important;
    }
    .extra_room_checkbox{
        background: #af6161 !important;
    border: 1px solid #af6161 !important;
    border-radius: 2px !important;
    height: 18px !important;
    width: 18px !important;
    display: inline-block !important;
    text-align: center !important;
    overflow: hidden !important;
    }
    .book-now.button{
        width: 100%;
        text-align: center;
    }
    
    
#see_dates_exprnce {
    max-width: 500px;
}
#see_dates_exprnce, #small-dialog {
    background: #fff;
    padding: 40px;
    padding-top: 0;
    text-align: left;
    max-width: 610px;
    margin: 40px auto;
    position: relative;
    box-sizing: border-box;
    border-radius: 4px;
}

#small-dialog .mfp-close, #see_dates_exprnce, .mfp-close, .mfp-close:hover {
    color: #fff;
}
._1f9rmq80 {
    border-bottom: 1px solid #DBDBDB !important;
    padding-top: 24px !important;
    padding-bottom: 24px !important;
    }
._e296pg {
    position: relative !important;
}
._rotqmn2 {
    margin: 0px !important;
    word-wrap: break-word !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 19px !important;
    line-height: 24px !important;
    letter-spacing: undefined !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #484848 !important;
    font-weight: 300 !important;
}
    
._150a3jym {
    margin: 0px !important;
    word-wrap: break-word !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 15px !important;
    line-height: 18px !important;
    letter-spacing: 0.2px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #484848 !important;
    font-weight: 300 !important;
}
    
._13k6a2k {
    position: absolute !important;
    right: 0px !important;
    top: 0px !important;
}

._annhcu9 {
    cursor: pointer !important;
    -webkit-transition: background 0.3s, border-color 0.3s !important;
    -moz-transition: background 0.3s, border-color 0.3s !important;
    transition: background 0.3s, border-color 0.3s !important;
    position: relative !important;
    display: inline-block !important;
    text-align: center !important;
    text-decoration: none !important;
    border-radius: 4px !important;
    width: auto !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 15px !important;
    line-height: 18px !important;
    letter-spacing: undefined !important;
    padding-top: 8px !important;
    padding-bottom: 8px !important;
    color: #484848 !important;
    font-weight: normal !important;
    border: 1px solid transparent !important;
    padding-right: 16px !important;
    padding-left: 16px !important;
    min-width: 51.77708763999664px !important;
    border-color: #008489 !important;
    background: transparent !important;
}

._16c9pko4 {
    -webkit-transition: color 0.3s !important;
    -moz-transition: color 0.3s !important;
    transition: color 0.3s !important;
    font-family: Circular,-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif !important;
    font-size: 15px !important;
    line-height: 18px !important;
    letter-spacing: undefined !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    color: #008489 !important;
}
    
.share-buttons li i {
   font-size: 20px;       
    font-weight: bold;
    }
</style>
    



 <div id="see_dates_exprnce" class="zoom-anim-dialog mfp-hide">

    <div class="small-dialog-header" style="margin-bottom:0px;">
		<h3>Upcoming Availability</h3>
    </div>
    <div id="listing-pricing-list" class="listing-section">
        <div class="show-more">
				<div class="pricing-list-container">
                    <?php foreach($experienceSlots as $slots) { ?> 
                                <div class="_1f9rmq80">
                                    <div class="_e296pg">
                                        <div>
                                            <div class="_rotqmn2"> <?php echo date('D, dS F Y ' ,strtotime($slots['ExperienceSlot']['checkindate'])); ?></div>
                                            <div>
                                                <div style="margin-top: 8px;">
                                                    <div class="_150a3jym"><?php echo $slots['ExperienceSlot']['slot_start_time']." − ". $slots['ExperienceSlot']['slot_end_time']; ?> <i class="fa fa-fw fa-1x">&#xf156;</i><?php echo number_format($slots['ExperienceSlot']['price']); ?> per person</div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="_13k6a2k">
                                            <button type="button" class="_annhcu9" aria-busy="false">Choose</button>
                                        </div>
                                    </div>
                                </div>
                        
                        <?php } ?>
				</div>
            </div>
        <a href="#" class="show-more-button" data-more-title="Show More" data-less-title="Show Less"><i class="fa fa-angle-down"></i></a>
     </div>             
             
</div>
<?php if($experienceDetails['Experience']['instant_book']=="Yes") {
	// intializing calender only if instant book is enabled.
	echo $this->Html->script('scripts/quantityButtonsExperience.js'); 

}
?>