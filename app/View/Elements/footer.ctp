<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
<!--				<img class="footer-logo" src="images/logo.png" alt="">-->
				<div id="logo" class="footer-logo">
<!--
					<a href="<?php //echo Configure::read('app_root_path'); ?>">
                        <img src="<?php //echo Configure::read('app_root_path'); ?>/images/coorgexpress.jpg" style="float:left; max-height: 80px;" alt="CoorgExpress.com">
                    </a>
-->
					 <h4 style="float:left;padding-top: 0px;font-size: 36px;">KAAPI<span style="color:#c7a17a">TRIP</span></h4>
				</div>
				<br><br>
                <div class="clearfix"></div>
				<p style=" text-align: justify; font-size:16px; line-height:25px;">
                   
 					Kaapi Trip is one stop portal on Indian Coffee estates, Cafes, Specialities, Stories and it's Culture!
					 <br>
					We Introduce you some of the scenic coffee plantation stays part of the western ghats, Tours that showcases the great flavor of Indian Coffee, Guide to explore the emerging Cafes and roastery around the city and an e-Shop to buy the best Indian specialty Coffee.		
				
				
				</p>
			</div>

			<div class="col-md-4 col-sm-6 ">
				<h4>Helpful Links</h4>
				<ul class="footer-links" style="padding-top:15px;">
					<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>plantation-stays">Plantation Stay</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>handpicked-experiences">Experiences</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>events">Coffee Events</a></li>
                    <li><a href="<?php echo Configure::read('app_root_path'); ?>blog/">Blog</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>shop">Products</a></li>
				</ul>


				<div class="clearfix"></div>
			</div>		

			<div class="col-md-3  col-sm-12">
				<h4>Contact Us</h4>  
				<div class="text-widget" style="padding-top:15px;">
					<span>KaapiTrip,</span> <br>
					<span>#120P,Santosh Towers</span> <br>
					<span>EPIP, Whitefield </span> <br>
					<span>Bangalore,Karnataka- 66</span> <br>
					Phone: <span>+91 (963) 233-8111 </span><br>
					E-Mail:<span> <a href="mailto:info@kaapitrip.com"> info@kaapitrip.com</a> </span><br>
				</div>

				<ul class="social-icons margin-top-20">
<!--
					<li><a class="facebook" href="https://www.facebook.com/coorgexpress/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/coorgexpress" target="_blank"><i class="icon-twitter"></i></a></li>
					<li><a class="instagram" href="https://www.instagram.com/coorgexpress/" target="_blank"><i class="icon-instagram"></i></a></li>
-->
				</ul>

			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">© 2014 - <?php echo date('Y');?> KaapiTrip.com. All Rights Reserved.</div>
			</div>
		</div>

	</div>

</div>