<nav id="navigation" class="style-1" >
	<ul id="responsive">
		<li><a href="#">Visit</a>
			<ul>
				<li><a href="<?php echo Configure::read('app_root_path'); ?>plantation-stays">Estate Stays</a></li>
				<li><a href="<?php echo Configure::read('app_root_path'); ?>handpicked-experiences">Tours and Experiences</a></li>
				<li><a href="<?php echo Configure::read('app_root_path'); ?>experience/Coffee Roasters">Coffee Roaster</a></li>
				<li><a href="<?php echo Configure::read('app_root_path'); ?>events">Events</a></li>
			</ul>
		</li>
        <li><a href="#">Learn</a>		
				<ul>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>experience/Work in Coffee farm">Work in Coffee Farm</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>experience/Internship">Internship</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>experience/Workshop">Work shops</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>experience/Project Kaapi">Project kaapi</a></li>
					<li><a href="<?php echo Configure::read('app_root_path'); ?>value-added-services/wholesale">Wholesale</a></li>
				</ul>
		</li>
		<li><a href="<?php echo Configure::read('app_root_path'); ?>shop">Local Shop</a>		
<!--
			<ul>
				<li><a href="<?php echo Configure::read('app_root_path'); ?>plantation-stays">Subscribe</a></li>
				<li><a href="<?php echo Configure::read('app_root_path'); ?>handpicked-experiences">Wholesale</a></li>
			</ul>
-->
		</li>
		<li><a href="#">Coffee Table</a>
			<ul>
				<li><a href="<?php echo Configure::read('app_root_path'); ?>blog">Blogs and Stories</a></li>
			</ul>
		</li>
		
	</ul>
</nav>


<div class="clearfix"></div>