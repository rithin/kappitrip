	<!-- Responsive Navigation Trigger -->
	<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
	
	<div class="dashboard-nav">
		<div class="dashboard-nav-inner">

			<ul data-submenu-title="Main">
				
				<li <?php if($this->request->action=='bookings') echo 'class="active"'; ?>><a href="<?php echo Configure::read('app_root_path'); ?>user/bookings"><i class="fa fa-calendar-check-o"></i> Bookings</a></li>
				<li <?php if($this->request->action=='orders') echo 'class="active"'; ?>><a href="<?php echo Configure::read('app_root_path'); ?>user/orders"><i class="sl sl-icon-layers"></i> Orders</a></li>
			</ul>			
			
			<ul data-submenu-title="Account">
				<li <?php if($this->request->action=='profile') echo 'class="active"'; ?>><a href="<?php echo Configure::read('app_root_path'); ?>user/profile"><i class="sl sl-icon-user"></i> My Profile</a></li>
				<li <?php if($this->request->action=='settings') echo 'class="active"'; ?>><a href="<?php echo Configure::read('app_root_path'); ?>user/settings"><i class="sl sl-icon-settings"></i> Settings</a></li>
				<li><a href="#" onclick="logout_url_redirect();"><i class="sl sl-icon-power"></i> Logout</a></li>
			</ul>

	</div>
	</div>