  <ul class="post-meta" style="margin-bottom: 1px; margin-top:1px; ">
	<?php if($this->Session->check('Auth.User')) { ?>
	    <li>
          <a href="<?php echo Configure::read('app_root_path'); ?>user/profile" class="sign-in popup-with-zoom-anim" style="text-decoration:none;" > My Profile</a> 
        </li>
	   <li>
          <a href="<?php echo Configure::read('app_root_path'); ?>user/bookings" class="sign-in popup-with-zoom-anim" style="text-decoration:none;">My Bookings</a> 
	   </li>
	   <li>
          <a href="<?php echo Configure::read('app_root_path'); ?>user/settings" class="sign-in popup-with-zoom-anim" style="text-decoration:none;">My Orders</a> 
       </li>
	   <li>
          <a href="<?php echo Configure::read('app_root_path'); ?>user/orders" class="sign-in popup-with-zoom-anim" style="text-decoration:none;" > Settings</a> 
       </li>
       <li>
	      <a href="#" onclick="logout_url_redirect();" class="sign-in " style="text-decoration:none;" >
		  <i class="im im-icon-Geek-2" ></i> <?php echo ucfirst($this->Session->read('Auth.User.first_name'))." ".ucfirst($this->Session->read('Auth.User.last_name')); ?> | Logout</a>
       </li>
	  <?php } else { ?>
     
           <li>
                <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim" style="text-decoration:none;" onclick="javascript:inititePopup()" >
            Sign In</a> 
           </li>
           <li>
                <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim" style="text-decoration:none;" onclick="javascript:inititePopup()" >
                Register</a>
             </li>
   
	  
        <?php } ?>
   </ul>



