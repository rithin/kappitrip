<div bgcolor="#ededed" marginwidth="0" marginheight="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;margin:0px!important;padding:0px!important;background:#ededed!important;margin:0px;padding:0px;background:#ededed">
  <table style="font-family:'Open Sans',Arial,Helvetica,sans-serif;border-collapse:collapse!important;background:#ededed!important;border-collapse:collapse;background:#ededed" cellspacing="0" cellpadding="0" border="0" bgcolor="#ededed" width="100%">
    <tbody>
      <tr>
        <td align="center" bgcolor="#ededed" valign="top">
          <table style="border-collapse:collapse" align="center" cellspacing="0" cellpadding="0" border="0" width="700">
						<tbody>
							<tr>
								<td style="height:20px!important;line-height:10px!important;font-size:20px!important;color:#ededed!important;height:10px;line-height:20px;font-size:20px;color:#ededed" height="20" align="center" valign="top">.</td>
							</tr>
							<tr>
								<td align="center" valign="top">
									<table style="border-collapse:collapse;background:#ffffff;font-family:'Open Sans',Arial,Helvetica,sans-serif" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="700">
										<tbody>
											<tr>
												<td style="background:#ffffff" align="left" bgcolor="#ffffff" width="20" valign="top">&nbsp;</td>
												<td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;background:#ffffff" align="left" bgcolor="#ffffff" valign="top">
													<table style="border-collapse:collapse;background:#ffffff;font-family:'Open Sans',Arial,Helvetica,sans-serif" cellspacing="0" cellpadding="0" border="0" width="100%">
														<tbody>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="center" valign="top">
																	<table style="border-collapse:collapse;background:#ffffff;font-family:'Open Sans',Arial,Helvetica,sans-serif" cellspacing="0" cellpadding="0" border="0" width="660">
																		<tbody>
																			<tr>
																				<td align="center" valign="top">
																					
																					
																					<img src="<?php echo $head_image; ?>" alt="" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 792.5px; top: 586px;"><div id=":1q5" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>
																					
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="left" valign="top">
																	<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121;line-height:20px">
																	<span style="font-size:1.3em">Dear <span style="color:#eb344a">Ricky Monappa</span>,
																</span>
																<br><br>
																	<span style="font-size:1.2em;line-height:30px">Greetings from Coorgexpress!</span>
                                <br><br>
                                
																	<span style="font-size:1.2em;line-height:30px">You have received a booking request at Sunset Drive to Mandalpatti for Standard Experience on <span class="aBn" data-term="goog_182670849" tabindex="0"><span class="aQJ">11/10/2018</span></span>.<br></span></font>
                                
                                </td>
															</tr>
															<tr>
																<td height="20" valign="top"></td>
															</tr>
															<tr>
																<td height="20" valign="top"></td>
															</tr>
															<tr>
																<td align="left" valign="top">
																	<table cellspacing="0" cellpadding="0" border="0" width="660px">
																		<tbody>
																			<tr>
																				<td colspan="2"><font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#353535">
																					</font>
																				</td>
																			</tr>
																			<tr>
																				<td style="padding:5px" bgcolor="#f3f3f3">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#353535"><b>Booking Summary:
																					</b></font>
																				</td>
																				<td style="padding:5px" bgcolor="#f3f3f3"></td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Experience Name: </font>
																				</td>
                                        <td align="left">
                                      	   <font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Sunset Drive to Mandalpatti</font>
                                        </td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Experience Type: </font>
																				</td>
																				<td align="left">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Standard Experience</font>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			
																			
																			
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">Date: </font>
																				</td>
																				<td align="left">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121"><span class="aBn" data-term="goog_182670850" tabindex="0"><span class="aQJ">11/10/2018</span></span></font>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			
																			
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">No. of Adults: </font>
																				</td>
																				<td align="left">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">4</font>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			<tr>
																				<td width="60%">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">No. of Children: </font>
																				</td>
																				<td align="left">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121">0</font>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
                                      
																			<tr>
																				<td style="border-top:1px solid light-gray"></td>
																				<td style="border-top:1px solid light-gray"></td>
																			</tr>
																			<tr>
																				<td colspan="2">&nbsp;</td>
																			</tr>
																			<tr>
																				<td style="padding:5px" bgcolor="#f3f3f3">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#353535">
																					<b>Approximate booking value:
																					</b></font>
																				</td>
																				<td style="padding:5px" align="left" bgcolor="#f3f3f3">
																					<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.4em;color:#353535">
																					<b>2,222</b></font>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr><td width="60%" style="text-align:center">
                                <font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#212121"><b>Please mark your acceptance below:</b></font>
                              </td>
                              </tr><tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr align="start" valign="top">
                                <td>
                                  <a href="#" style="margin-left:22%;float:left;background:#09a335;padding:12px 30px;border-radius:30px;color:white;text-decoration:none;font-weight:900;letter-spacing:2px" rel="noreferrer" target="_blank" data-saferedirecturl="#">Accept</a>
                                  <a href="#" style="float:right;margin-right:22%;background:#eb344a;padding:12px 30px;border-radius:30px;color:white;text-decoration:none;font-weight:900;letter-spacing:2px" rel="noreferrer" target="_blank" data-saferedirecturl="#">Decline</a>
                                </td>
                              </tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
                              <tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td align="center" valign="top">
																	<font style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:1.3em;color:#3e6793;line-height:20px;text-align:center">
																	For any further clarifications please do not hesitate to reach out to us via <br> email <a href="mailto:info@coorgexpress.com" style="color:#eb344a" rel="noreferrer" target="_blank">info@coorgexpress.com</a> or call us on PH: <a href="tel:+91%20<?php echo $host_telphone; ?>" style="color:#eb344a" rel="noreferrer" target="_blank">+91 <?php echo $host_telphone; ?></a>
																	<br><br>
																	Best regards,<br>
																	Team Coorgexpress<br><br>
																	<br>
																	</font>
																</td>
															</tr>
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
															
															<tr>
																<td align="left" valign="top">&nbsp;</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td style="background:#ffffff" align="left" bgcolor="#ffffff" width="20" valign="top">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="height:20px!important;line-height:20px!important;font-size:20px!important;color:#ededed!important;height:20px;line-height:20px;font-size:20px;color:#ededed" height="20" align="center" valign="top">.</td>
							</tr>
						</tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>