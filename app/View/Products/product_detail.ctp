<?php echo $this->Html->css('product.css'); ?>
<?php echo $this->Html->css('vendor/slick/slick.css'); ?>
<?php echo $this->Html->script('scripts/bootstrap.min.js'); ?>
<?php //echo $this->Html->script('scripts/common.js'); ?>

        <section class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="bread-crumbs">
						<li><a href="<?php echo Configure::read('app_root_path'); ?>">Home</a></li>
						<li>Shop</li>
					</ul>
				</div>
			</div>
	</section>

        
        <main class="container product-content">
			<section class="row">
				<div class="col-sm-12 col-md-6">
					<div class="product-slider">
						<div class="big-slider">
                                                   
                                                            <?php if(!empty($productFiles)){
                                                                
                                                                foreach($productFiles as $img_key=>$img_data){
                                                                
                                                                
                                                            ?>
                                                    
                                                            <div class="slick-slide">
                                                                <div>
                                                                    <img src="<?php echo Configure::read('app_root_path').$img_data['ProductFile']['img_path'].'/'.$img_data['ProductFile']['resized_img_name']; ?>" alt="product" >
                                                                </div>
                                                            </div>
                                                            
                                                            <?php }} ?>
                                                     
                                                </div>
						<div class="small-slider">
                                                    
                                                                    <?php if(!empty($productFiles)){
                                                                
                                                                foreach($productFiles as $img_key=>$img_data){
                                                                
                                                                
                                                                    ?>
                                                    
                                                    
                                                                    <div class="slick-slide">
                                                                        <div class="small-slider-iner" >
                                                                        <img src="<?php echo Configure::read('app_root_path').$img_data['ProductFile']['img_path'].'/'.$img_data['ProductFile']['thumbnail_img_name']; ?>" alt="product">
                                                                        </div>
                                                                    </div>    
                                                    
                                                                    <?php }} ?>
                                                    
                                                       
                                                </div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="product-info">

                                                <input type="hidden" id="product_id_val" value="<?php echo $product_details['Product']['id']; ?>"/>                                  
						<h2><?php echo $product_details['Product']['product_name'];?></h2>
						<!--<div class="product-rating">
							<span><i class="fa fa-star" aria-hidden="true"></i></span>
							<span><i class="fa fa-star" aria-hidden="true"></i></span>
							<span><i class="fa fa-star" aria-hidden="true"></i></span>
							<span><i class="fa fa-star" aria-hidden="true"></i></span>
							<span><i class="fa fa-star-o" aria-hidden="true"></i></span>
							<p>(2 customer reviews)</p>
						</div>-->
						<p><?php echo $product_details['Product']['product_desc'];?></p>
						<p class="availability"><i class="fa fa-check" aria-hidden="true"></i> In Stock</p>
						<p class="price"> ₹ <?php echo $product_details['Product']['unit_price'];?></p>
						<!--<form action="#" class="product-page-form">-->
                                                <div>
							
							<div class="quantity">
								<div class="inner-quantity">
									<p>Quantity</p>
									<div class="counters">
										<i class="fa fa-caret-left decrement" id="decrement" aria-hidden="true"></i>
										<input id="quantitycount" class="count" value="1">
										<i class="fa fa-caret-right increment" id="increment" aria-hidden="true"></i>
									</div>

								</div>
								<button type="submit" id="add_cart_button" >add to cart</button>
							</div>
							<!--<div class="add-buttons">
								<a href="#" class="to-wish"><i class="fa fa-heart-o" aria-hidden="true"></i><i class="fa fa-heart" aria-hidden="true"></i> Add to Wishlist</a>
								<a href="#" class="to-compare"><i class="fa fa-exchange" aria-hidden="true"></i> Add to Compare</a>
							</div>-->
							<div class="secondary-info">
								<p>Package Weight: <span><?php echo $product_details['Product']['product_weight'];?></span></p>
                                                                <p>Origin: <span><?php echo $product_details['City']['name'];?></span></p>
                                                                <p>Grind Size: <span><?php echo $product_details['CoffeeType']['coffee_type_name'];?></span></p>
                                                                <p>Flavour: <span><?php echo $product_details['Product']['coffee_flavour'];?></span></p>
                                                                <p>Ingredients: <span><?php echo $product_details['Product']['coffee_ingredients'];?></span></p>
                                                                <p>Roast: <span><?php echo $product_details['Product']['coffee_roast'];?></span></p>
                                                                <p>Processing: <span><?php echo $product_details['Product']['coffee_processing'];?></span></p>
								
							</div>
							<div class="share-product">
								<p>share:</p>
								<ul class="share">
									<li><a href="#" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#" title="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
									<li><a href="#" title="vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
								</ul>
							</div>
                                                </div>
						<!--</form>-->
					</div>
				</div>		
			</section>
			<section class="row">
				<div class="col-md-12">
					<div class="tabs-box">
					    <ul class="tabs">
					        <li class="active"><a href="#tab1"><p class="h3">details</p></a></li>
					        <li><a href="#tab2"><p class="h3">additional information</p></a></li>
					        <!--<li><a href="#tab3"><p class="h3">reviews (2)</p></a></li>-->
					    </ul>
					    <div class="tab_container">
					        <div id="tab1" class="tab_content" >
					            <p><?php echo $product_details['Product']['product_desc'];?> </p>
					        </div>
					        <!-- end #tab1 -->
					        <div id="tab2" class="tab_content" >
					            <p> Brand Name : <?php echo $product_details['Product']['brand_name'];?></p>
                                                    <p> Manufacturer Name : <?php echo $product_details['Product']['manufacturer_name'];?></p>
                                                    <p> Shelf Life : <?php echo $product_details['Product']['shelf_life'];?></p>
					        </div>
					        <!-- end #tab2 -->
					        <!--<div id="tab3" class="tab_content" >
					            <p>Reviews - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
					        </div>-->
					        <!-- end #tab3 -->
					    </div>
					</div>
				</div>
			</section>
            
                       
                        <?php if(!empty($related_product_details)){ ?>
            
			<section class="related-slider">
				<div class="row">
					<div class="col-md-12 related-slider-header">
						<p class="h2">related products</p>
						<div class="prev-slider slick-arrow" style=""><i class="fa fa-angle-left" aria-hidden="true"></i></div>
						<div class="next-slider slick-arrow" style=""><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="related-product-slider">
                                                    
                                                   
                                                            <?php foreach($related_product_details as $related_product_data){ ?>
                                                         
                                                            <div class="card">
								<div class="product">
									<div class="product-prev">
                                                                                <input type="hidden" class="related_product_id" value="<?php echo $related_product_data['Product']['id']; ?>"/>
										<a href="<?php echo $related_product_data['Product']['seo_unique_url'];?>" tabindex="-1">
											<img src="<?php echo Configure::read('app_root_path').$related_product_data['ProductFile']['img_path']."/".$related_product_data['ProductFile']['listing_img_name']; ?>" alt="product">
										</a>
										<div class="hide-product-buttons">
                                                                                    
											<!--<a href="javascript:addToCartRelated(3)" class="h5" tabindex="-1"><i class="fa fa-shopping-basket" aria-hidden="true"></i> add to cart</a>-->
										
                                                                                        <a href="javascript:void(0)" class="h5 add_to_cart_related" tabindex="-1"><i class="fa fa-shopping-basket" aria-hidden="true"></i> add to cart</a>
                                                                                    
                                                                                        
                                                                                        
                                                                                        <!--<a href="#" class="follow" tabindex="-1"><i class="fa fa-heart-o" aria-hidden="true"></i> <i class="fa fa-heart" aria-hidden="true"></i></a>-->
										</div>
									</div>
									
									<div class="product-name">
										<a href="<?php echo $related_product_data['Product']['seo_unique_url'];?>" class="h4" tabindex="-1"><?php echo $related_product_data['Product']['product_name'];?></a>
										<p class="price">
                                                                                    <!--<span>$ 100</span>--> 
                                                                                    ₹ <?php echo $related_product_data['Product']['unit_price'];?></p>
									</div>
									<div class="product-desc">
										<p><?php echo $related_product_data['Product']['brand_name'];?></p>
										<p class="product-rating">
											<span><i class="fa fa-star" aria-hidden="true"></i></span>
											<span><i class="fa fa-star" aria-hidden="true"></i></span>
											<span><i class="fa fa-star" aria-hidden="true"></i></span>
											<span><i class="fa fa-star" aria-hidden="true"></i></span>
											<span><i class="fa fa-star-o" aria-hidden="true"></i></span>
										</p>
									</div>
								</div>
							</div>
                                                       
                                                    
                                                        <?php } ?>
                                                
                                     
                                                 
                                                            
                                                
                                                </div>
					</div>
				</div>
			</section>
                        <?php } ?>
            
		</main>

	

<script>

$(document).on('ready', function() {
  
    $(".product-slider .big-slider").slick({arrows:!1,asNavFor:".product-slider .small-slider",infinite:!1,draggable:!1,swipe:!1,speed:1e3,fade:!0});
    $(".product-slider .small-slider").slick({slidesToShow:3,arrows:!1,asNavFor:".product-slider .big-slider",focusOnSelect:!0,centerPadding:"0px",infinite:!1});
  
    tabs();

    increase();

    $(".related-slider .related-product-slider").slick({prevArrow:".related-slider .prev-slider",nextArrow:".related-slider .next-slider",slidesToShow:3,slidesToScroll:3,responsive:[{breakpoint:992,settings:{slidesToShow:2,slidesToScroll:2}},{breakpoint:769,settings:{slidesToShow:1,slidesToScroll:1}}]});


});

function increase(){
    
        $(".decrement").on("click",function(){
            var e=$(this).closest(".counters").find(".count"),o=e.val();o>1?e.val(o=+o-1):e.val(o=1)
        }),
        $(".increment").on("click",function(){
            var e=$(this).closest(".counters").find(".count"),o=e.val();e.val(o=+o+1)
        })
}

 function tabs(){
       
        var tabsNav    = $('.tabs'),
	tabsNavLis = tabsNav.children('li');

	tabsNav.each(function() {
		 var $this = $(this);

		 $this.next().children('.tab_content').stop(true,true).hide()
		 .first().show();

		 $this.children('li').first().addClass('active').stop(true,true).show();
	});

	tabsNavLis.on('click', function(e) {
            
            
		 var $this = $(this);

		 $this.siblings().removeClass('active').end()
		 .addClass('active');

		 $this.parent().next().children('.tab_content').stop(true,true).hide()
		 .siblings( $this.find('a').attr('href') ).fadeIn();

		 e.preventDefault();
	});
	var hash = window.location.hash;
	var anchor = $('.tabs a[href="' + hash + '"]');
  
	if (anchor.length === 0) {
		 $(".tabs li:first").addClass("active").show(); //Activate first tab
		 $(".tab_content:first").show(); //Show first tab content
	} else {
		 anchor.parent('li').click();
	}
    
    
    
    
   }

 function addToCart(product_id){
        var numOfQty=$('#quantitycount').val();
        
        console.log(numOfQty);
        
        data={'product_id':product_id,numOfQty:numOfQty};
         $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/add_product_to_cart",
                data: data,
                success:function(dataCount){
                    $('#cartCounter').html(dataCount);
                }
        });
        
        var dataInput;
         $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/ajax_mini_cart",
                data: dataInput,
                beforeSend:function(html){
                    $('.loading-overlay').show();
                },
                success:function(cartDetails){
                    $('.loading-overlay').hide();
                    $('#ajaxMiniCart').html(cartDetails);
                }
        });
        
        
        check_user_login();
    }
    
 function addToCartRelated(product_id){

        data={'product_id':product_id};
         $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/add_product_to_cart",
                data: data,
                success:function(dataCount){
                    $('#cartCounter').html(dataCount);
                }
        });
        
        var dataInput;
         $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/ajax_mini_cart",
                data: dataInput,
                beforeSend:function(html){
                    $('.loading-overlay').show();
                },
                success:function(cartDetails){
                    $('.loading-overlay').hide();
                    $('#ajaxMiniCart').html(cartDetails);
                }
        });
        
        check_user_login();
        
    }    
    

    $('.add_to_cart_related').on('click', function () {
	var product_id=$(this).closest('.related_product_id').find("input").val();
	//console.log(product_id);
	var cart = $('.carticon');
        var imgtodrag = $(this).closest('.product-prev').find("img").eq(0);
	//console.log(imgtodrag);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                $(this).detach()
            });
        }
	addToCartRelated(product_id);
 });
 
 

 
 $('#add_cart_button').on('click', function () {
     
        
        
     
	var product_id=$('#product_id_val').val();
	//console.log(product_id);
	var cart = $('.carticon');
        var imgtodrag = $('.big-slider').find("img").eq(0);
	//console.log(imgtodrag);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                $(this).detach()
            });
        }
	addToCart(product_id);
 });


 function check_user_login(){

            $.ajax({
                type: 'POST',
                url: "<?php echo Configure::read('app_root_path'); ?>carts/check_user_login",
                
                success:function(response){
                    if(response==0){
                        $.magnificPopup.open({
                        items: {
                            src: '#sign-in-dialog',
                        },
                            type: 'inline',
                            closeOnBgClick:false,
                            enableEscapeKey: false,
                            closeOnContentClick: false,
                            showCloseBtn : false,
                        });
                        
                  
                    }
                }
            });



            

}   
 
</script>
