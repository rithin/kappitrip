  <?php          
   if(count($productList)>0){
	   ?>
<!-- Sorting / Layout Switcher -->
<!--
<div class="row fs-switcher">
	<div class="col-md-6">
		 Showing Results 
		<p class="showing-results"><?php //echo count($productList); ?> Results Found </p>
	</div>
</div>
-->
 <?php 
	$keyItem=0;
	foreach($productList as $item){ 
        if($item['Product']['seo_unique_url']!=""){            
			$keyItem++;$keyItem=0;
?>
				<!-- Listing Item -->
                <div class="col-md-6" style="height: 436.484px;">
					<div class="product">
							<div class="product-prev">
								<a target="_blank" href="<?php echo  Configure::read('app_root_path')."shop/".$item['Product']['seo_unique_url']; ?>" class="product_img">
										<img src="<?php echo Configure::read('app_root_path').$item['ProductFile']['0']['img_path']."/".$item['ProductFile']['0']['listing_img_name']; ?>" alt="<?php echo $item['Product']['product_name']; ?>"  >
									<input type="hidden" class="product_id" value="<?php echo $item['Product']['id']; ?>"/>
										<!--<span class="product-label">sale</span>-->
									</a>
											<div class="hide-product-buttons">
<!--												javascript:addToCart(<?php echo $item['Product']['id']; ?> )-->
												<a href="javascript:void(0);" class="h5 add_to_cart_test" ><i class="fa fa-shopping-basket" aria-hidden="true"></i> ADD TO CART</a>
												 
												
<!--												<a href="#" class="follow"><i class="fa fa-heart-o" aria-hidden="true"></i> <i class="fa fa-heart" aria-hidden="true"></i></a>-->
											</div>
				                </div>
										
										<div class="product-name">
											<a href="<?php echo  Configure::read('app_root_path')."product/".$item['Product']['seo_unique_url']; ?>" class="h4"><?php echo $item['Product']['product_name']; ?></a>
											<p class="price"> ₹ <?php echo number_format( $item['Product']['unit_price'],2); ?></p>
										</div>
										<div class="product-desc">
											<p><?php echo $item['Product']['product_weight'];?></p>
											<p class="product-rating">
												<span><i class="fa fa-star" aria-hidden="true"></i></span>
												<span><i class="fa fa-star" aria-hidden="true"></i></span>
												<span><i class="fa fa-star" aria-hidden="true"></i></span>
												<span><i class="fa fa-star" aria-hidden="true"></i></span>
												<span><i class="fa fa-star-o" aria-hidden="true"></i></span>
											</p>
										</div>
                        </div>
		          </div>				
					<?php if($keyItem==3 || $keyItem==6 || $keyItem==9) {
	
					?>   
<!--                        <div class="clearfix"></div>-->
                        <?php  } ?>

                
    <?php } 
        }
    ?>        
				<!-- Listing Item / End -->

<?php }else{ ?>    
	
	<div style="
    text-align: center;
    height: 250px;
    padding-top: 100px;
    font-stretch: extra-expanded;
    font-family: monospace;
    color: #a4692d;
    font-size: larger;
">
			No items found. Please try different combination of search.
</div>

<?php }?>


	<!-- Pagination -->
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12">
						<!-- Pagination -->
				<div id="pagination" class="pagination-container margin-top-20 margin-bottom-40" style=" text-align: center;">
					<?php echo $perpageresult; ?>
				</div>
			</div>
		</div>
				<!-- Pagination / End -->	
<script>


$('.add_to_cart_test').on('click', function () {
	var product_id=$(this).closest('.product-prev').find("input").val();
	//console.log(product_id);
	var cart = $('.carticon');
        var imgtodrag = $(this).closest('.product-prev').find("img").eq(0);
	//console.log(imgtodrag);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                $(this).detach()
            });
        }
	addToCart(product_id);
 });
</script>