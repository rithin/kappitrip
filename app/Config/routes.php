<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'index', 'action' =>  'index'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	//Router::connect('/listing/ajaxFilter', array('controller' => 'listing', 'action' => 'index'));


##########################################NEW URL CHANGES ##################################

// in the routes.php
Router::connect('/plantation-stays/:slug', array('controller' => 'detail', 'action' => 'index')
, array('pass'=>array('slug')));

Router::connect('/handpicked-experiences/:slug', array('controller' => 'detail', 'action' => 'experience')
, array('pass'=>array('slug')));

Router::connect('/shop/:slug', array('controller' => 'products', 'action' => 'productDetail')
, array('pass'=>array('slug')));

Router::connect('/shop', array('controller' => 'products', 'action' => 'listing'));


Router::connect('/coffee-shops/:slug', array('controller' => 'coffee_shop', 'action' => 'coffeeShopDetail')
, array('pass'=>array('slug')));

Router::connect('/coffee-shops', array('controller' => 'coffee_shop', 'action' => 'listing'));

Router::connect('/plantation-stays', array('controller' => 'listing', 'action' => 'index'));

Router::connect('/handpicked-experiences', array('controller' => 'listing', 'action' => 'experience_list'));

Router::connect('/experience/:slug', array('controller' => 'listing', 'action' => 'experience_list')
, array('pass'=>array('slug')));

//Router::connect('/activities/:slug', array('controller' => 'listing', 'action' => 'experience_list'), array('pass'=>array('slug')));

Router::connect('/user/profile', array('controller' => 'my_accounts', 'action' => 'profile'));
Router::connect('/user/bookings', array('controller' => 'my_accounts', 'action' => 'bookings'));
Router::connect('/user/orders', array('controller' => 'my_accounts', 'action' => 'orders'));
Router::connect('/user/settings', array('controller' => 'my_accounts', 'action' => 'settings'));



Router::connect('/blog', array('controller' => 'blog', 'action' => 'index'));


Router::connect('/events', array('controller' => 'listing', 'action' => 'event_list'));

##########################################NEW URL CHANGES ##################################

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
