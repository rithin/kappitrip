<?php
ini_set('memory_limit','1024M');
ini_set('max_execution_time',3000000);
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $view   = 'Theme';
    public $theme = "CapAdmin";
    public $helpers = array('Session','Html','Custom');
    public $components = array(
        'Session',
        'Cookie',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'home', 'action' => 'index'),
            'loginAction'	=> array('controller' => 'index', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'index', 'action' => 'index'),
            'authError' => 'You must be logged in to view this page.',
            'loginError' => 'Invalid Username or Password entered, please try again.'
        )
    );

    public $uses = array('OccupancyDetail','Role');

    function beforeFilter() {
        $this->__setTheme();
        parent::beforeFilter();
        $this->Auth->allow('index','searchDirection', 'updatePassword','reset_password','forgot','ajaxFilter','fetchSearchData');
        $this->loadModel('Cart');		
		$this->set('count',$this->Cart->getCount());
       // $this->set('cookieHelper', $this->Cookie);
        $this->set('logged_in_user_id',$this->Auth->user('id'));
		if (session_status() == PHP_SESSION_NONE) {
				session_start();
		}
		$this->Cookie->name ='KaapiTrip';
		$this->Cookie->time =2592000;  // or '30 days'
		$this->Cookie->path = '/';
		$this->Cookie->domain =  $_SERVER['HTTP_HOST'];
		//$this->Cookie->secure = true;  // i.e. only sent if using secure HTTPS
		$this->Cookie->key = Configure::read('cookie_enc_key');
		//$this->Cookie->httpOnly = true;
		$this->Cookie->type('aes');
        
        $rowqa= $this->OccupancyDetail->query('DELETE FROM occupancy_details WHERE  `created` < ADDDATE(NOW(), INTERVAL -1 MINUTE) AND `status`=4'); // deleteing all the rooms which gone to hold.
        // debug($rowqa);
       
       
       
       
        $user_name = '';
        $user_role = '';
        $role_id = '';
                
        $user_data = $this->Auth->user();
        
        if(!empty($user_data['id'])){
        
        $user_name = $user_data['first_name'].' '.$user_data['last_name'];
        
        $role_id = $user_data['role_id'];
        
        $role_data = $this->Role->query('select role_name from roles where id='.$role_id);
       
        
        $user_role = $role_data['0']['roles']['role_name'];
                
        }
        
        $this->set('user_name',$user_name);
        
        $this->set('user_role',$user_role);
        
        $this->set('role_id',$role_id);
        
    }

    public function isAuthorized($user) {
        // Here is where we should verify the role and give access based on role
        return true;
    }
    function __setSettings() {
            $Setting  = ClassRegistry::init('Setting');
            $settings = $Setting->find('all');
            foreach($settings as $_setting) {
                    if ($_setting['Setting']['value']!==null) {
                            Configure::write('Settings.'.$_setting['Setting']['category'].'.'.$_setting['Setting']['setting'], $_setting['Setting']['value']);
                    }
            }
            // you can now use the following anywhere in your app
            // $settings = Configure::read('Settings.app'); debug($settings)
    }

    function __setTheme() {
        // check if the admin is being requested

        $admin_controllers =array('users','vendors','keycheck','dashboard','manage_homestays', 'manage_experiences', 'manage_products','property_bookings','enquiry_bookings','manage_coupons','manage_property_type','manage_room_type','manage_bed_type','manage_experience_type','manage_slider','manage_blog','manage_amenity','manage_city','manage_location','manage_category' , 'manage_sub_category', 'manage_hosts','reports','manage_coffee_shops','manage_agent','manage_deals');

        if(in_array($this->params->controller,$admin_controllers)) {
                // set admin theme
                $this->theme = "CapAdmin";
        }
        else {
                // set front end theme
                $this->theme = "";
        }
    }

    function _setErrorLayout() {
        if ($this->name == 'CakeError') {
            $this->layout = 'error404_layout';
        }
    }
    function beforeRender () {
        $this->_setErrorLayout();
    }
}