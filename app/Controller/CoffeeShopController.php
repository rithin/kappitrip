<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class CoffeeShopController extends AppController {

    //var $layout ="product_layout";
    public $components = array('Paginator','PerPage');
    public $uses = array('User','CoffeeShop','CoffeeShopImage','State');
    
    public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
		
	}
    
    public function listing(){  
        
        
        $seoMetaDtls = array();	
        
        //$seoMetaDtls=$this->CoffeeShop->query("SELECT meta_title,meta_keyword,meta_description FROM `products` WHERE rand() limit 0,1");
        //debug($seoMetaDtls);
        $meta_title = '';
        $meta_keyword = '';
        $meta_description = '';
        
        if(!empty($seoMetaDtls)){
            $meta_title = $seoMetaDtls[0]['products']['meta_title'];
            $meta_keyword = $seoMetaDtls[0]['products']['meta_keyword'];
            $meta_description = $seoMetaDtls[0]['products']['meta_description'];
        }
        
        
        $array_states = array(); 
        $array_states=$this->State->find('list',array('fields'=>array('id','state_name')));        
        $this->set('array_states',$array_states);
        
        $this->set('meta_title',$meta_title);
	$this->set('meta_keyword',$meta_keyword);
	$this->set('meta_description',$meta_description);    
        
  
    }
   
    
    public function ajaxCoffeeShopFilter(){
       
        
       $this->layout='ajax';
       $conditions =array(); 
       $join_condition='';  
       $page = 1;
        
       if(!empty($_GET["page"])) {
			$page = $_GET["page"];
		}
		
		$start = ($page-1)*1;
		if($start < 0) $start = 0;
                
                
        
        //debug($_REQUEST);

	$paginationlink = "coffee_shop/ajaxCoffeeShopFilter/?page=";	
	$pagination_setting = 'all-links';
        
       
        
        if(isset($_REQUEST) && isset($_REQUEST['state_id']) && $_REQUEST['state_id']!=''){   
          $shop_location=$_REQUEST['state_id'];
          $conditions_data=array("CoffeeShop.state_id"=> $shop_location);
          array_push($conditions,$conditions_data);
        }
        
        /*
      
        if(isset($_REQUEST) && isset($_REQUEST['search_text']) && $_REQUEST['search_text']!='undefined'){
            $searchTerm=$_REQUEST['search_text'];
            $conditions_data=array("(Product.product_name LIKE '%$searchTerm%' OR Product.product_desc LIKE '%$searchTerm%')");
            array_push($conditions,$conditions_data);
        }
	if(@$_REQUEST['selectedPriceRange']>10){
            $base_price=$_REQUEST['selectedPriceRange'];
            array_push($conditions,array("Product.unit_price < $base_price"));
	}
          
       
         */
        
        array_push($conditions,array('CoffeeShop.status'=>1));
        //debug($conditions);
        $joins=array(
                  array(
                    'table' => 'coffee_shop_images',
                    'alias' => 'CoffeeShopImage',
                    'type' => 'LEFT',
                    'conditions' => array(
                            'CoffeeShop.id = CoffeeShopImage.coffee_shop_id'
                          )
                    ),    
		  );
         $coffeeShopList=$this->CoffeeShop->find('all', array(
			'joins' =>$joins,
            'conditions' => $conditions,
                        'limit' => 10,
			'page'=>$page,
			'offset'=>$start,
			'fields' => array('CoffeeShop.*','CoffeeShopImage.thumbnail_img_name','CoffeeShopImage.img_path'),
                        'group' => 'CoffeeShop.id',
			'order' => 'CoffeeShop.created DESC'
		));
        
        $coffeeShopListTotal=$this->CoffeeShop->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,
                        'fields' => array('CoffeeShop.*','CoffeeShopImage.resized_img_name','CoffeeShopImage.img_path'),
                        'group' => 'CoffeeShop.id',
			'order' => 'CoffeeShop.created DESC'
		));
		//debug($_GET);
		if(empty($_GET["rowcount"])) {
			$_GET["rowcount"] = count($coffeeShopListTotal);
		}

		if($pagination_setting == "prev-next") {
			$perpageresult = $this->perPage->getPrevNext($_GET["rowcount"], $paginationlink,$pagination_setting);	
		} else {
			$perpageresult = $this->getAllPageLinks($_GET["rowcount"], $paginationlink,$pagination_setting);	
		}
		//debug($coffeeShopList);
                 $this->set('coffeeShopList',$coffeeShopList);	
		 $this->set('perpageresult',$perpageresult);	
                 
                //exit;
        
    }
    

    public function coffeeShopDetail($unique_url=null){
        
        
        
        
        $conditions=array();
        
        if($unique_url !=""){
            
            
            $this->CoffeeShop->recursive=-1;
            $coffeeShopDetail=$this->CoffeeShop->find('first',array('conditions'=>array("seo_unique_url LIKE '%$unique_url%' "),'fields'=>array('CoffeeShop.*')));
            
            if(count($coffeeShopDetail)==0){ // redirecting back to listing page if someone types a wrong url.
                $this->redirect('/coffee-shops');
            }
            
            $searchId=$coffeeShopDetail['CoffeeShop']['id'];
            
        
   
            $conditions=array("CoffeeShop.id "=> $searchId);
            
            
            
        }    
            
        $coffee_shop_details= $this->CoffeeShop->find('first', array(
					'joins' => array(

					   array(
							'table' => 'coffee_shop_images',
							'alias' => 'CoffeeShopImage',
							'type' => 'LEFT',
							'conditions' => array(
								'CoffeeShop.id = CoffeeShopImage.coffee_shop_id'
							)
						),
						
					
                                        

					),
					'conditions' => $conditions,
					'fields' => array('CoffeeShop.*','CoffeeShopImage.resized_img_name','CoffeeShopImage.img_path'),
					'order' => 'CoffeeShop.created DESC'
		    ));
            
            
                    
           //debug($product_details); 
            
           //$array_sub_categories=$this->SubCategory->find('list',array('fields'=>array('id','sub_category_name'))); 
           $this->CoffeeShopImage->unbindModel(array('belongsTo' => array('CoffeeShop')));
           $cofeeShopImages=$this->CoffeeShopImage->find('all',array('conditions'=>array('CoffeeShopImage.coffee_shop_id'=>$searchId)));  
            
           
           
          
       
        //debug($related_product_details);
        
        unset($coffee_shop_details['CoffeeShopImage']['resized_img_name']);
        unset($coffee_shop_details['CoffeeShopImage']['img_path']);
        
        //debug($cofeeShopImages);
        
        $this->set('cofeeShopImages',$cofeeShopImages); 
        
        $this->set('coffee_shop_details',$coffee_shop_details);
        
        $this->set('meta_keyword',''); 
        $this->set('meta_description',''); 
    }

    public function getAllPageLinks($count,$href) {
		$output = '';
		if(!isset($_GET["page"])) $_GET["page"] = 1;
		$this->perpage=10;
		if($this->perpage != 0)
			$pages  = ceil($count/$this->perpage);
		
		
		//echo "pages>>". $pages." count ".$count;
		if($pages>1) {
			if($_GET["page"] == 1) 
				$output = $output . '<span class="link first disabled">&#8810;</span><span class="link disabled">&#60;</span>';
			else	
				$output = $output . '<a class="link first" onclick="getresult(\'' . $href . (1) . '\')" >&#8810;</a><a class="link" onclick="getresult(\'' . $href . ($_GET["page"]-1) . '\')" >&#60;</a>';
			
			
			if(($_GET["page"]-3)>0) {
				if($_GET["page"] == 1)
					$output = $output . '<span id=1 class="link current">1</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href . '1\')" >1</a>';
			}
			if(($_GET["page"]-3)>1) {
					$output = $output . '<span class="dot">...</span>';
			}
			
			for($i=($_GET["page"]-2); $i<=($_GET["page"]+2); $i++)	{
				if($i<1) continue;
				if($i>$pages) break;
				if($_GET["page"] == $i)
					$output = $output . '<span id='.$i.' class="link current">'.$i.'</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href . $i . '\')" >'.$i.'</a>';
			}
			
			if(($pages-($_GET["page"]+2))>1) {
				$output = $output . '<span class="dot">...</span>';
			}
			if(($pages-($_GET["page"]+2))>0) {
				if($_GET["page"] == $pages)
					$output = $output . '<span id=' . ($pages) .' class="link current">' . ($pages) .'</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href .  ($pages) .'\')" >' . ($pages) .'</a>';
			}
			
			if($_GET["page"] < $pages)
				$output = $output . '<a  class="link" onclick="getresult(\'' . $href . ($_GET["page"]+1) . '\')" >></a><a  class="link" onclick="getresult(\'' . $href . ($pages) . '\')" >&#8811;</a>';
			else				
				$output = $output . '<span class="link disabled">></span><span class="link disabled">&#8811;</span>';
			
			
		}
		return $output;
	}

    
}