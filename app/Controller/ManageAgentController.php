<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageAgentController extends AppController {

	var $layout ="default";
	public $uses = array('User','Role','UserDetail','State');

	public function beforeFilter()
	{
            parent::beforeFilter();
            $this->Auth->allow();
	}
        
        public function agent_list(){
            
        		
        
        }
    

        function ajax_agent_list(){
        
            $this->layout = false;
        
                $agentList=$this->User->find('all', array(
			
                        'conditions'=>array('User.role_id'=>'3'),
			'fields' => array('User.*'),
                        'group'=>'User.id',
			'order' => 'User.created DESC'
		));
                
                
                //debug($productList);
                
                $list_view_array = array();
                
                foreach($agentList as $key=>$agent_data){
                    
                    if($agent_data['User']['status_id']==""){
                        $status_code="Enable";
                    }elseif($agent_data['User']['status_id']==1){
                        $status_code="Disable";
                    }elseif($agent_data['User']['status_id']==2){
                        $status_code="Enable";
                    }else{
                        $status_code="Enable";
                    }
                    
                    $list_view_array['data'][$key]['agent_name'] = $agent_data['User']['first_name'].' '.$agent_data['User']['last_name'];
                    $list_view_array['data'][$key]['email_id'] = $agent_data['User']['email_id'];
                    $list_view_array['data'][$key]['contact_number'] = $agent_data['User']['contact_number'];
                    $list_view_array['data'][$key]['actions'] = "<a href='".Configure::read('app_root_path')."manage_agent/add/".$agent_data['User']['id']."'>Edit</a>".
                                                                "&nbsp;&nbsp;<a href='".Configure::read('app_root_path')."manage_agent/updateStatus/".$agent_data['User']['id']."/".$status_code."'>".$status_code."</a>";                }
                
                echo json_encode($list_view_array);
                
                exit;
        
    }
    
    
    public function add($id=''){
       
        
        $array_states = array(); 
        $array_states=$this->State->find('list',array('fields'=>array('id','state_name')));        
        $this->set('array_states',$array_states);
        
        $add_edit = 'add';
        
        if($id!=""){	
            
            $user_data = $this->User->find('first', array(
			'joins' => array(

			   array(
					'table' => 'user_details',
					'alias' => 'UserDetail',
					'type' => 'LEFT',
					'conditions' => array(
						'UserDetail.user_id = User.id'
					)
				),
                

			),
                        'conditions' => array('User.id'=>$id),
			'fields' => array('User.*','UserDetail.*'),
			'order' => 'User.created DESC'
		    ));
            
            
            
            $this->request->data = $user_data;
            
            $this->request->data['User']['agent_email'] = $user_data['User']['email_id'];
            $this->request->data['User']['contact_number'] = $user_data['User']['contact_number'];
            
            $this->request->data['User']['comapny_name'] = $user_data['UserDetail']['company_name'];
            $this->request->data['User']['address1'] = $user_data['UserDetail']['address1'];
            $this->request->data['User']['address2'] = $user_data['UserDetail']['address2'];
            $this->request->data['User']['city'] = $user_data['UserDetail']['city'];
            $this->request->data['User']['state_id'] = $user_data['UserDetail']['state_id'];
            $this->request->data['User']['country'] = $user_data['UserDetail']['country'];
           
            $add_edit = 'edit';
            
        }
       
        $this->set('add_edit',$add_edit);
        
    }
   
    public function save(){      
        
        if(!empty($_POST['data']['User']['first_name'])){
            
            if(!empty($_POST['data']['User']['id'])){
                $user_data['id'] = $_POST['data']['User']['id'];
            }else{
                $user_data['status_id'] =  1;
            }
            
            
            $user_data['role_id'] =  3;
            $user_data['first_name'] = $_POST['data']['User']['first_name'];
            $user_data['last_name'] = $_POST['data']['User']['last_name'];
            $user_data['email_id'] = $_POST['data']['User']['agent_email'];
            $user_data['username'] = $_POST['data']['User']['agent_email'];
            if(!empty($_POST['data']['User']['agent_password'])){
                $user_data['password'] = $_POST['data']['User']['agent_password'];
            }
            $user_data['contact_number'] = $_POST['data']['User']['contact_number'];
            
      
            
            $response=$this->User->SaveAll($user_data);
       
            if($_POST['data']['User']['id']!="")
                $LastInsertID=$_POST['data']['User']['id'];
            else
               $LastInsertID=$this->User->getLastInsertID();
            
            if(!empty($LastInsertID)){
           
            if(!empty($_POST['data']['UserDetail']['id'])){
                $userDetails['id'] = $_POST['data']['UserDetail']['id'];
            }    
                
            $userDetails['user_id']=$LastInsertID;
           
            $userDetails['company_name']=$_POST['data']['User']['comapny_name'];
            $userDetails['address1']=$_POST['data']['User']['address1'];
            $userDetails['address2']=$_POST['data']['User']['address2'];
            $userDetails['city']=$_POST['data']['User']['city'];
            $userDetails['state_id']=$_POST['data']['User']['state_id'];
            $userDetails['country']=$_POST['data']['User']['country'];
            
           
                        
            $this->UserDetail->SaveAll($userDetails);
            
            }
         
           
         
           $this->redirect(array('controller'=>'manage_agent','action'=>'agent_list'));
        }

      }
      
      public function updateStatus($product_id=null,$status_code=null){
        $this->autoRender=false;  
            if($status_code=='Enable'){
		$status_id=1;
            }else{
		$status_id=2;
            }
	
        $userStatusArr['User']['id'] =$product_id;
        $userStatusArr['User']['status_id'] =$status_id;                
        $this->User->saveAll($userStatusArr);
            $this->redirect(array('controller'=>'manage_agent','action'=>'agent_list'));
	}
 
      
}
?>