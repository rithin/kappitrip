<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PropertyBookingsController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry');

	public function beforeFilter()
	{
        parent::beforeFilter();
        //$this->Auth->allow('booking_invoice');
	}
    
        public function booking_list(){ 
         
           
        }
    
    
        public function ajax_booking_list(){
        
         $this->layout = false;
         
         $conditions=array();
         $this->BookingOrderDetail->recursive=2;
         $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
         $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
         $this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
        
		$fields = array('OccupancyDetail.*','Homestay.*','RoomPriceDetail.*','BookingOrderDetail.*','BookingPaymentResponse.*');
		$joins= array(
				array(
					'table' => 'occupancy_details',
					'alias' => 'OccupancyDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						'OccupancyDetail.order_id = BookingOrderDetail.order_id'
					)
				),
				array(
					'table' => 'homestays',
					'alias' => 'Homestay',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = BookingOrderDetail.homestay_id'
					)
				),
                array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						'OccupancyDetail.room_price_detail_id = RoomPriceDetail.id'
					)
				),				
				array(
					'table' => 'booking_payment_responses',
					'alias' => 'BookingPaymentResponse',
					'type' => 'LEFT',
					'conditions' => array(
						'BookingOrderDetail.id = BookingPaymentResponse.booking_order_detail_id'
					)
				),   
			);
		
		
                
                
                
                $bookingDetails=$this->BookingOrderDetail->find('all', array(
			'joins' => $joins,
                        'conditions' => $conditions,
			'fields' => $fields,
                        'group' => '`OccupancyDetail`.`order_id`',
			'order' => 'BookingOrderDetail.created DESC'
		));         
                
                
                
                
                $list_view_array = array();
                
                foreach($bookingDetails as $key=>$booking_data){
                  
                    
                    $list_view_array['data'][$key]['booking_id'] = $booking_data['BookingOrderDetail']['order_id'];
                    $list_view_array['data'][$key]['billing_name'] = $booking_data['BookingPaymentResponse']['billing_name'];
                    $list_view_array['data'][$key]['billing_address'] = $booking_data['BookingPaymentResponse']['billing_address'];
                    $list_view_array['data'][$key]['billing_tel'] = $booking_data['BookingPaymentResponse']['billing_tel'];
                    $list_view_array['data'][$key]['homestay_name'] = $booking_data['Homestay']['name'];
                    $list_view_array['data'][$key]['homestay_contact'] = $booking_data['Homestay']['telephone'];
                    $list_view_array['data'][$key]['checkin_date'] = $booking_data['OccupancyDetail']['check_in_date'];
                    $list_view_array['data'][$key]['checkout_date'] = $booking_data['OccupancyDetail']['check_out_date'];
                    $list_view_array['data'][$key]['coupon_discount'] = $booking_data['BookingOrderDetail']['discount_amount'];
                    $list_view_array['data'][$key]['amount_paid'] = $booking_data['BookingPaymentResponse']['amount'];
                    
                    
                    $list_view_array['data'][$key]['actions'] = "<a  target='_blank' href='".Configure::read('app_root_path')."invoice/".base64_encode($booking_data['BookingOrderDetail']['order_id'])."/".base64_encode($booking_data['Homestay']['id'])."'>Invoice</a>";
                    
                    if($booking_data['BookingOrderDetail']['status']!=8 && $booking_data['BookingOrderDetail']['status']!=13){
                            $list_view_array['data'][$key]['actions'] .= "&nbsp;&nbsp; <a  target='_blank' href='".Configure::read('app_root_path')."property_bookings/cancel_booking/".base64_encode($booking_data['BookingOrderDetail']['order_id'])."/".base64_encode($booking_data['Homestay']['id'])."'>Cancel</a>";

                    }
                                                                           
                    
                    }
                
             
                    
                echo json_encode($list_view_array);
                
                exit;
                
        
    }
    
    public function delete($id=""){      
        $this->Homestay->delete($id);
        $this->HomestayFile->deleteAll(array('HomestayFile.homestay_id' => $id), false);
        $this->redirect(array('controller'=>'manage_homestays','action'=>'booking_list'));
     }
     public function booking_invoice($order_id_enc=null,$homestay_id_enc=null){
         $this->layout=false; 
         $order_id=base64_decode($order_id_enc);
         $homestay_id=base64_decode($homestay_id_enc);
        $OccupancyDetailArr=$this->OccupancyDetail->find('first',array('conditions'=> array('OccupancyDetail.order_id'=>$order_id), 'fields'=>array('OccupancyDetail.*')));
        $HomestayDetailArr=$this->Homestay->find('first',array(
            'joins' => array(
				array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type' => 'LEFT',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),               

			),
            'conditions'=> array('Homestay.id'=>$homestay_id),
            'fields'=>array('Homestay.*','RoomPriceDetail.*')));
       // debug($productOrderDetailArr);
      //  debug($paymentResponseDetailArr);
        $BookingOrderDetailArr=$this->BookingOrderDetail->find('all',array(
            'joins' => array(
				array(
					'table' => 'booking_payment_responses',
					'alias' => 'BookingPaymentResponse',
					'type' => 'LEFT',
					'conditions' => array(
						'BookingOrderDetail.id = BookingPaymentResponse.booking_order_detail_id'
					)
				),               

			),
            'conditions'=> array('BookingOrderDetail.order_id'=>$order_id),
            'fields'=>array('BookingOrderDetail.*','BookingPaymentResponse.*')));
        
        $this->set('OccupancyDetailArr',$OccupancyDetailArr);
        $this->set('HomestayDetailArr',$HomestayDetailArr);
        $this->set('BookingOrderDetailArr',$BookingOrderDetailArr);
    }
	
	public function cancel_booking($order_id_enc,$homestay_id_enc){
		$this->layout=false; 
		$order_id=base64_decode($order_id_enc);
        $homestay_id=base64_decode($homestay_id_enc);
        $OccupancyDetailArr=$this->OccupancyDetail->find('first',array('conditions'=> array('OccupancyDetail.order_id'=>$order_id), 'fields'=>array('OccupancyDetail.*')));
		
		$this->OccupancyDetail->updateAll(
							array( 'OccupancyDetail.status' => 8 ),   //fields to update
							array( 'OccupancyDetail.order_id' => $order_id )  //condition
						);
		
		$this->BookingOrderDetail->updateAll(
							array( 'BookingOrderDetail.status' => 8 ),   //fields to update
							array( 'BookingOrderDetail.order_id' => $order_id )  //condition
						);
		
		$this->redirect(array('controller'=>'property_bookings','action'=>'booking_list'));
		
	}
	
    public function add($id=''){ 
        $array_cities=$this->City->find('list');
        $this->set('array_cities',$array_cities);
		$array_property_types=$this->PropertyType->find('list');
        $this->set('array_property_types',$array_property_types);
		$array_room_types=$this->RoomType->find('list',array('fields'=>array('id','type')));
        $this->set('array_room_types',$array_room_types);
		
        $array_bed_types=$this->MasterBedType->find('list');
        $this->set('array_bed_types',$array_bed_types);
        if($id!="")	{
            $OccupanyDataArray=array();
            $this->request->data= $this->Homestay->find('first',array('conditions'=>array('Homestay.id'=>$id))); 
            $homestayFiles=$this->HomestayFile->find('all',array('conditions'=>array('HomestayFile.homestay_id'=>$id)));             
            //fetching rooms to list the at the availability tab..	
            $rooms=$this->RoomPriceDetail->find('list',array('conditions'=>array('RoomPriceDetail.homestay_id'=>$id), 'fields'=>array('id','room_no_name'))); 
            $this->set('homestayFiles',$homestayFiles);	
            $this->set('rooms',$rooms);	
            //finding General Amenities for the home stay from master table.
            $amenities_arr=$this->Amenity->find('all',array('fields'=>array('id','amenity','description')));
            $this->set('amenities_arr',$amenities_arr);	
            
            $homestayAmenities_arr=$this->HomestayAmenity->find('list',array('fields'=>array('amenity_id')));
            $this->set('homestayAmenities_arr',$homestayAmenities_arr);	
           //homestayAmenities_arr debug($homestayAmenities_arr);
            
            $roomPriceDetails=$this->RoomPriceDetail->find('all',array('conditions'=>array('RoomPriceDetail.homestay_id'=>$id))); 
             
            foreach($roomPriceDetails as $keyDtl=>$detail){
                $room_price_detail_id=$detail['RoomPriceDetail']['id'];
                $bedroomDetails = $this->BedroomDetail->find('list', array('conditions'=> array('BedroomDetail.homestay_id'=>$id,'BedroomDetail.room_price_detail_id'=>$room_price_detail_id), 'fields'=>array('id','master_bed_type_id')));
                
                $bedcountDetails = $this->BedroomDetail->find('list', array('conditions'=> array('BedroomDetail.homestay_id'=>$id,'BedroomDetail.room_price_detail_id'=>$room_price_detail_id), 'fields'=>array('master_bed_type_id','no_of_bed')));
                
                $this->request->data['MasterBedType']["BedRoom_$keyDtl"]=$bedroomDetails;
                $this->request->data['MasterBedType']["BedCount_$keyDtl"]=$bedcountDetails;
               // $this->request->data['MasterBedType']["BedRoom_$keyDtl"]['id']=
                $this->request->data['RoomPriceDetail'][$keyDtl]["id"]=$detail['RoomPriceDetail']['id'];
                $this->request->data['RoomPriceDetail'][$keyDtl]["room_base_price"]=$detail['RoomPriceDetail']['room_base_price'];
                $this->request->data['RoomPriceDetail'][$keyDtl]["extra_bed_charge"]=$detail['RoomPriceDetail']['extra_bed_charge'];
                $this->request->data['RoomPriceDetail'][$keyDtl]["extra_bed"]=$detail['RoomPriceDetail']['extra_bed'];
            }
           // debug($this->request->data);
            //finding availabilty on this month of current homestay.
            //here finding count by using virtual field(no_of_bookings) mentioned in the corresponding model.
            //doing the above coz, cakephp not give aggreagte results on 'list' method
            $OccupanyDataArray=$this->OccupancyDetail->find('all', array('conditions'=>array( 'OccupancyDetail.homestay_id'=>$id, 
                        array('OccupancyDetail.check_in_date BETWEEN ? AND ?' => array(date('Y-m-01'), date('Y-m-t')))  ),'fields'=>array('check_in_date' ,'check_out_date' ,'OccupancyDetail.no_of_bookings'), 'group'=>'check_in_date')); 
            // $log = $this->Homestay->getDataSource()->getLog(false, false);  
			
			//debug($OccupanyDataArray);
            $occupancyDetailString=json_encode($OccupanyDataArray);
            $this->set('occupancyDetailString',$occupancyDetailString);
            
            
        }
    }
    
    public function findAvailability(){
        $this->autoRender=false;  
        $data=array();
        $homestay_id=$_REQUEST['homestay_id'];
        $start_date=$_REQUEST['start_date'];
       // $OccupancyDetail=$this->OccupancyDetail->find('list',array('conditions'=> array('OccupancyDetail.homestay_id'=>$homestay_id, 'check_in_date'=>$start_date),'fields'=>array('room_price_detail_id'))); 
		 $condition=" ( check_out_date > '$start_date' AND check_in_date < '$start_date') ";
        $OccupancyDetail=$this->OccupancyDetail->find('list', array(            
         'conditions'=>array( 'OccupancyDetail.homestay_id'=>$homestay_id,'OccupancyDetail.room_price_detail_id IS NOT NULL', $condition ),'fields'=>array('OccupancyDetail.room_price_detail_id'))); 
		
		if(count($OccupancyDetail)==0){			
			$OccupancyDetail=$this->OccupancyDetail->find('list',array('conditions'=> array('OccupancyDetail.homestay_id'=> $homestay_id,  'check_in_date'=>$start_date),'fields'=>array('room_price_detail_id'))); 
		}
		//debug($OccupancyDetail);
        if(count($OccupancyDetail)>0){
            foreach($OccupancyDetail as $val){
                $data[]=$val;
            }
            echo  json_encode($data);
        }else{
            echo  json_encode($data);
        }
        exit;
    }
    

    
}
?>