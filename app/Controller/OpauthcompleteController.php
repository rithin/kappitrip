<?php

App::uses('AppController', 'Controller');


/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class OpauthcompleteController extends AppController {
    var $layout ="";
    public $uses = array('User','UserSocialNetwork');
    public function beforeFilter()
    {
        parent::beforeFilter();		
        $this->Auth->allow();		
    }
    public function index()
    {
        
        
        $this->autoRender=false;

        
        $social_unqid=$this->request->data['auth']['uid'];
        $social_provider=$this->request->data['auth']['provider'];
        $social_name=$this->request->data['auth']['info']['name'];
        $social_email=$this->request->data['auth']['info']['email'];
        
      

        
        $user_first_name = '';
        $user_last_name = '';
        
        if($social_provider=="Google"){
            $user_first_name = $this->request->data['auth']['info']['first_name'];
            $user_last_name = $this->request->data['auth']['info']['last_name'];
        }
       
        if($social_provider=="Facebook"){
            $pieces = explode(" ", $social_name);
            
            $user_first_name = $pieces[0];
            $user_last_name = $pieces[1];
            if(!empty($pieces[2])){
                $user_last_name .= " ".$pieces[2];
            }

        }
        
       
        $restult_users=$this->User->find('first',array('conditions'=>array('username'=>$social_email)));
        if(empty($restult_users))
        {
            if($social_email!="")
            {
                $insert_user['User']['username']=$social_email;
                $insert_user['User']['email_id']=$social_email;
                $insert_user['User']['role_id']=4;
                $insert_user['User']['status_id']=1;
                $insert_user['User']['first_name']=$user_first_name;
                $insert_user['User']['last_name']=$user_last_name;
                $this->User->saveAll($insert_user);
               
                
                $insert_social['UserSocialNetwork']['social_unique_id']=$social_unqid;
                $insert_social['UserSocialNetwork']['social_network']=$social_provider;
                $insert_social['UserSocialNetwork']['user_id']=$this->User->getLastInsertID();
                $this->UserSocialNetwork->saveAll($insert_social);
                
            }
        }
      
        $getuser_data=$this->User->find('first',array('conditions'=>array('username'=>$social_email)));
        if ($this->Auth->login($getuser_data['User'])) {
            //$this->redirect(Configure::read('app_root_path').'index');
            
            
            
            
            
            
            
            $redirect_url = $this->Session->read('social_redirect_url');
            
            
            $this->redirect($redirect_url);
        }
    }
}
?>
