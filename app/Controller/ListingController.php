<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ListingController extends AppController {
	var $layout ="default";
	public $components = array('Paginator','PerPage');
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail', 'OccupancyDetail','Amenity','HomestayAmenity','Product','ProductFile','Category','SubCategory','Experience','ExperienceFile','ExperienceLanguage','ExperienceType','ExperienceSlot','ExperienceBookingDetail','PropertyType','PropertyType','Location','AttendeeType','ExperienceAttendee','TrailType');
	//public $helpers = array('PerPage');
	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
		
	}
	public function index(){
		$property_type_id=0; $text_search='';
		
		if(isset($_REQUEST['type_id']))
			$property_type_id=base64_decode($_REQUEST['type_id']);
		
		if(isset($_REQUEST['text_search']))
			$text_search=$_REQUEST['text_search'];
        
        $amenities_arr=$this->Amenity->find('all',array('conditions'=>array('Amenity.in_search'=>1), 'fields'=>array('id','amenity','description')));		
		$this->set('amenities_arr',$amenities_arr);	
		
		$locations_arr=$this->Location->find('list',array('conditions'=>array('Location.in_search'=>1), 'fields'=>array('id','name')));
		$this->set('locations_arr',$locations_arr);	
		
		$cities_arr=$this->City->find('list',array( 'fields'=>array('id','name')));
		$this->set('cities_arr',$cities_arr);	
		
		$property_type_arr=$this->PropertyType->find('list',array(	'fields'=>array('id','name')));		
        $this->set('property_type_arr',$property_type_arr);	        
        
       
		
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		$this->set('text_search',$text_search);
		$this->set('property_type_id',$property_type_id);
		$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
      //  debug($seoMetaDtls);
    }
     public function experience_list($requestPagetype=null){       
		$experience_type_id=0;		
		if(isset($_REQUEST['type_id']))
			$experience_type_id=base64_decode($_REQUEST['type_id']);
		 
		if($requestPagetype!="") {
			$experience_type= $this->ExperienceType->find('all',array('fields'=>array('id','type'),'conditions'=>array('ExperienceType.type'=>$requestPagetype)));
			$experience_type_id=$experience_type[0]['ExperienceType']['id'];
	 	}
		$this->set('requestPagetype',$requestPagetype);
		$locations_arr=$this->Location->find('list',array('conditions'=>array('Location.in_search'=>1), 'fields'=>array('id','name')));
		$this->set('locations_arr',$locations_arr);	
		 
		$cities_arr=$this->City->find('list',array( 'fields'=>array('id','name')));
		$this->set('cities_arr',$cities_arr);	
		
       
        $experience_type_arr=$this->ExperienceType->find('list',array('fields'=>array('id','type')));
        $this->set('experience_type_arr',$experience_type_arr);	
        
		$attendeeTypes=$this->AttendeeType->find('list',array('fields'=>array('id','attendee')));
        $this->set('attendeeTypes',$attendeeTypes);	
        
        $trailTypes=$this->TrailType->find('list',array('fields'=>array('id','name')));
        $this->set('trailTypes',$trailTypes);	
        
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `experiences` WHERE rand() limit 0,1");
		$this->set('experience_type_id',$experience_type_id);
		$this->set('meta_title',$seoMetaDtls[0]['experiences']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['experiences']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['experiences']['meta_description']);
		
    }
	public function event_list($requestPagetype=null){       
		$experience_type_id=0;		
		if(isset($_REQUEST['type_id']))
			$experience_type_id=base64_decode($_REQUEST['type_id']);
       
		$this->set('requestPagetype',$requestPagetype);			
		$locations_arr=$this->Location->find('list',array('conditions'=>array('Location.in_search'=>1), 'fields'=>array('id','name')));
		$this->set('locations_arr',$locations_arr);	
		
		$cities_arr=$this->City->find('list',array( 'fields'=>array('id','name')));
		$this->set('cities_arr',$cities_arr);	
       
        $experience_type_arr=$this->ExperienceType->find('list',array('conditions'=>array('id'=>5),'fields'=>array('id','type')));
        $this->set('experience_type_arr',$experience_type_arr);	
        
		$attendeeTypes=$this->AttendeeType->find('list',array('fields'=>array('id','attendee')));
        $this->set('attendeeTypes',$attendeeTypes);	
        
        $trailTypes=$this->TrailType->find('list',array('fields'=>array('id','name')));
        $this->set('trailTypes',$trailTypes);	
        
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `experiences` WHERE rand() limit 0,1");
		$this->set('experience_type_id',$experience_type_id);
		$this->set('meta_title',$seoMetaDtls[0]['experiences']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['experiences']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['experiences']['meta_description']);
		
    }
	public function search_list(){
		$property_type_id=0; $text_search='';$city_id=0;
		
		if(isset($_REQUEST['type_id']))
			$property_type_id=base64_decode($_REQUEST['type_id']);
		
		if(isset($_REQUEST['text_search']))
			$text_search=$_REQUEST['text_search'];
		
		if(isset($_REQUEST['city_id']))
			$city_id=base64_decode($_REQUEST['city_id']);
		
		$stays=$this->Homestay->query("SELECT count(*) active_stay FROM `homestays` WHERE status=1");
		
		$experiences=$this->Homestay->query("SELECT count(*) active_experience FROM `experiences` WHERE status=1 and experience_type!=5");
		$events=$this->Homestay->query("SELECT count(*) active_event FROM `experiences` WHERE status=1 and experience_type=5");
		
		$this->set('active_stay',$stays[0][0]['active_stay']);
		$this->set('active_experience',$experiences[0][0]['active_experience']);
		$this->set('active_event',$events[0][0]['active_event']);
		
        $amenities_arr=$this->Amenity->find('all',array('conditions'=>array('Amenity.in_search'=>1), 'fields'=>array('id','amenity','description')));		
		$this->set('amenities_arr',$amenities_arr);	
		
		$locations_arr=$this->Location->find('list',array('conditions'=>array('Location.in_search'=>1), 'fields'=>array('id','name')));
		$this->set('locations_arr',$locations_arr);	
		
		$cities_arr=$this->City->find('list',array( 'fields'=>array('id','name')));
		$this->set('cities_arr',$cities_arr);	
       
		//debug($locations_arr);
		$property_type_arr=$this->PropertyType->find('list',array(	'fields'=>array('id','name')));		
        $this->set('property_type_arr',$property_type_arr);	        
        
       
		
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		$this->set('text_search',$text_search);
		$this->set('property_type_id',$property_type_id);
		$this->set('city_id',$city_id);
		$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
	}
	
	public function our_program(){
		
		$experience_type_arr=$this->ExperienceType->query("SELECT * FROM `experience_types` WHERE status=1");
		$property_type_arr=$this->PropertyType->query("SELECT * FROM `property_types` WHERE status=1");
		//debug();
		
	}
    
    public function ajaxFilter(){
        $this->layout=false;
		$conditions=array();
		$page = 1;
		if(!empty($_GET["page"])) {
			$page = $_GET["page"];
		}
		
		$start = ($page-1)*1;
		if($start < 0) $start = 0;

		$paginationlink = "listing/ajaxFilter/?page=";	
		$pagination_setting = 'all-links';
		//debug($_REQUEST);
		
		
		if(isset($_REQUEST) && isset($_REQUEST['filterType'])){
			$filterType=$_REQUEST['filterType'];
			@$selectedAmenities=json_decode($_REQUEST['selectedAmenitiesJson']);
            @$base_price=$_REQUEST['selectedPriceRange'];
            
			$conditions =array(); $join_condition='';
         //   debug($selectedAmenities);
			if(count($selectedAmenities)>0){            
				array_push($conditions,array("HomestayAmenity.amenity_id IN (".implode(",",$selectedAmenities).")"));
			}
			if($filterType=='checkInCheckOut'){         

				@$conditions=array("Homestay.id IN(SELECT room_price_details.homestay_id from room_price_details as room_price_details where room_price_details.id NOT IN ( select room_price_detail_id from occupancy_details where check_out_date > '$checkInCheckOut->startDate' AND check_in_date < '$checkInCheckOut->endDate') GROUP BY   `room_price_details`.`homestay_id` )");

			}
			if($base_price>800){
				 $base_price=$_REQUEST['selectedPriceRange'];
				 array_push($conditions,array("Homestay.base_price < $base_price"));
			}
			if($filterType=='instantBook' || $_REQUEST['instantBook']!=""){
				 $instantBook=$_REQUEST['instantBook'];
				if($instantBook=="Yes"){
					array_push($conditions,array("Homestay.instant_book = '$instantBook'"));
				}
			}
			
			if($filterType=='basicSearch' || ($_REQUEST['city']!="" || $_REQUEST['search_text']
!=""||$_REQUEST['property_type']!="")){
				if(@$_REQUEST['search_text']!=""){
					$searchText=$_REQUEST['search_text'];
					array_push($conditions,array('OR' => array(
								"Homestay.name LIKE '%$searchText%' ",
								"Homestay.description LIKE '%$searchText%' ",
										)						
									 ));
				}
				if(@$_REQUEST['city']!=""){				
					array_push($conditions,array('Homestay.city_id'=>$_REQUEST['city']));
				}
				if(@$_REQUEST['property_type']!=""){				
					array_push($conditions,array('Homestay.property_type_id'=>$_REQUEST['property_type']));
				}
			}		
		}
		array_push($conditions,array('Homestay.status'=>1,'Homestay.seo_unique_url IS NOT NULL'));
		//debug($conditions);
      	$joins= array(
				array(
					'table' => 'homestay_files',
					'alias' => 'HomestayFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayFile.homestay_id'
					)
				),
                array(
					'table' => 'homestay_amenities',
					'alias' => 'HomestayAmenity',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayAmenity.homestay_id'
					)
				),
                array(
					'table' => 'amenities',
					'alias' => 'Amenity',
					'type'  => 'LEFT',
					'conditions' => array(
						'Amenity.id = HomestayAmenity.amenity_id' 
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Homestay.location_id'
					)
				),
				array(
					'table' => 'property_types',
					'alias' => 'PropertyType',
					'type' => 'INNER',
					'conditions' => array(
						'PropertyType.id = Homestay.property_type_id'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Homestay.city_id'
					)
				),
				array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'INNER',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),				
				array(
					'table' => 'deals',
					'alias' => 'Deal',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = Deal.homestay_id','Deal.status=1 ','CURDATE() between `Deal.start_date` and `Deal.end_date`'
					)
				),
			);
         $homestayList=$this->Homestay->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,
			'limit' => 9,
			'page'=>$page,
			'offset'=>$start,
			'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path','Location.name','Location.latitude', 					'Location.longitude','PropertyType.name','City.name','Deal.*'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.rating DESC'
		));
     //	$log = $this->Homestay->getDataSource()->getLog(false, false); 
//debug($homestayList);
		$homestayListTotal=$this->Homestay->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,
			
			'fields' => array('Homestay.*'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.created DESC'
		));
		//debug($_GET);
		if(empty($_GET["rowcount"])) {
			$_GET["rowcount"] = count($homestayListTotal);
		}

		if($pagination_setting == "prev-next") {
			$perpageresult = $this->perPage->getPrevNext($_GET["rowcount"], $paginationlink,$pagination_setting);	
		} else {
			$perpageresult = $this->getAllPageLinks($_GET["rowcount"], $paginationlink,$pagination_setting);	
		}
		//echo $perpageresult;
		 $this->set('perpageresult',$perpageresult);	
		
    	// we are using the 'Homestay' model
    	//$homestayList = $this->paginate('Homestay');        
        $this->set('homestayList',$homestayList);
    }
    
    public function ajaxExperienceFilter(){
		
        $this->layout=false;
		$conditions=array(); $join_condition="";
		$page = 1;
		if(!empty($_GET["page"])) {
			$page = $_GET["page"];
		}
		
		$start = ($page-1)*1;
		if($start < 0) $start = 0;
//debug($_REQUEST);
		$paginationlink = "listing/ajaxExperienceFilter/?page=";	
		$pagination_setting = 'all-links';
		
		if(isset($_REQUEST) && isset($_REQUEST['filterType'])){
			$filterType=$_REQUEST['filterType'];
            @$selectedAttendeeTypes=json_decode($_REQUEST['selectedExperienceTypeJson']);
			//debug($_REQUEST);
			if($filterType=='AttendeeType'){
				$selectedAttendeeTypes=json_decode($_REQUEST['selectedExperienceTypeJson']);    
			}
			
			$conditions =array(); $join_condition='';
			if(count($selectedAttendeeTypes)>0){            
				$attendee_type=implode(",",$selectedAttendeeTypes);
				array_push($conditions,array("ExperienceAttendee.attendee_type_id IN ($attendee_type) "));
			}
//			if($filterType=='checkInCheckOut'){         
//
//				$conditions=array("Experience.id IN(SELECT experience_slots.experience_id from experience_slots as experience_slots where experience_slots.id NOT IN ( select experience_slot_id from experience_booking_details where date >  '$checkInCheckOut->startDate' AND date < '$checkInCheckOut->endDate') GROUP BY `experience_slots`.`experience_id )");
//
//			}
			if(@$_REQUEST['selectedPriceRange']>800){
				 $base_price=$_REQUEST['selectedPriceRange'];
				 array_push($conditions,array("ExperienceSlot.price < $base_price"));
			 }
			
			if($filterType=='basicSearch'  || ($_REQUEST['city']!="" || $_REQUEST['search_text']
!=""||$_REQUEST['experience_type']!="" ||$_REQUEST['trail_type_id']!="")){
				if(@$_REQUEST['search_text']!=""){
					$searchText=$_REQUEST['search_text'];
					array_push($conditions,array('OR' => array(
								"Experience.name LIKE '%$searchText%' ",
								"Experience.about_experience LIKE '%$searchText%' ",
										)						
									 ));
				}
//				if(@$_REQUEST['location']!=""){				
//					array_push($conditions,array('Experience.location_id'=>$_REQUEST['location']));
//				}
				if(@$_REQUEST['city']!=""){				
					array_push($conditions,array('Experience.city_id'=>$_REQUEST['city']));
				}
				if(@$_REQUEST['experience_type']!=""){				
					array_push($conditions,array('Experience.experience_type'=>$_REQUEST['experience_type']));
				}
				if(@$_REQUEST['trail_type_id']!=""){				
					array_push($conditions,array('Experience.trail_type_id'=>$_REQUEST['trail_type_id']));
				}
			}	
			
		}
		if(@$_REQUEST['type']=="experience_event"){
				array_push($conditions,array('Experience.experience_type'=>5));
		}else{
			array_push($conditions,array('Experience.experience_type !=5'));
		}
		//debug($conditions);
		$joins= array(

				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),  
                array(
					'table' => 'experience_slots',
                    'alias' => 'ExperienceSlot',
                    'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceSlot.experience_id' .$join_condition
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Experience.location_id'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Experience.city_id'
					)
				),
                array(
					'table' => 'experience_attendees',
					'alias' => 'ExperienceAttendee',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceAttendee.experience_id'
					)
				),
				array(
					'table' => 'deals',
					'alias' => 'Deal',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = Deal.experience_id','Deal.status=1 ','CURDATE() between `Deal.start_date` and `Deal.end_date`'
					)
				),
			);
       
		
		array_push($conditions,array('Experience.status'=>1,'Experience.seo_unique_url IS NOT NULL'));
		
		$experienceList=$this->Experience->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,
			'limit' => 9,
			'page'=>$page,
			'offset'=>$start,
			'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path','Location.name','Location.latitude','Location.longitude','ExperienceType.type','City.name','ExperienceSlot.*','Deal.*'),
            'group' => '`Experience`.`id`',
			'order' => 'Experience.rating DESC'
		));
		foreach($experienceList as $key=>$listItem){
			$experience_id=$listItem['Experience']['id'];
			$this->ExperienceSlot->recursive=-1;
			$this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));  
			unset($this->ExperienceSlot->virtualFields['no_of_slots']);
			$experienceSlots=$this->ExperienceSlot->find('all',array('conditions'=>array('ExperienceSlot.experience_id'=> $experience_id, 'ExperienceSlot.status'=>1),'order'=>'ExperienceSlot.price ASC'));
				//("select ExperienceSlot.* from  experience_slots ExperienceSlot WHERE ExperienceSlot.experience_id=$experience_id and ExperienceSlot.status=1 ORDER BY ExperienceSlot.price ASC"); 
			//debug($experienceSlots);
			unset($experienceList[$key]['ExperienceSlot']);
			foreach($experienceSlots as $slots){
				$experienceList[$key]['ExperienceSlot'][]=$slots['ExperienceSlot'];
			}
		}
		
		$experienceListTotal=$this->Experience->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,			
			'fields' => array('Experience.*'),
            'group' => '`Experience`.`id`',
			'order' => 'Experience.created DESC'
		));
		if(empty($_GET["rowcount"])) {
			$_GET["rowcount"] = count($experienceListTotal);
		}

		if($pagination_setting == "prev-next") {
			$perpageresult = $this->perPage->getPrevNext($_GET["rowcount"], $paginationlink,$pagination_setting);	
		} else {
			$perpageresult = $this->getAllPageLinks($_GET["rowcount"], $paginationlink,$pagination_setting);	
		}
		//echo $perpageresult;
		 $this->set('perpageresult',$perpageresult);	
     
       $this->set('experienceList',$experienceList);
    }
	
	
	 public function ajaxEventFilter(){
        $this->layout=false;
		$conditions=array(); $join_condition="";
		$page = 1;
		if(!empty($_GET["page"])) {
			$page = $_GET["page"];
		}
		
		$start = ($page-1)*1;
		if($start < 0) $start = 0;
//debug($_REQUEST);
		$paginationlink = "listing/ajaxEventFilter/?page=";	
		$pagination_setting = 'all-links';
		
		if(isset($_REQUEST) && isset($_REQUEST['filterType'])){
			$filterType=$_REQUEST['filterType'];
            @$selectedAttendeeTypes=json_decode($_REQUEST['selectedExperienceTypeJson']);
			//debug($_REQUEST);
			if($filterType=='AttendeeType'){
				$selectedAttendeeTypes=json_decode($_REQUEST['selectedExperienceTypeJson']);    
			}
			
			$conditions =array(); $join_condition='';
			if(count($selectedAttendeeTypes)>0){            
				$attendee_type=implode(",",$selectedAttendeeTypes);
				array_push($conditions,array("ExperienceAttendee.attendee_type_id IN ($attendee_type) "));
			}
//			if($filterType=='checkInCheckOut'){         
//
//				$conditions=array("Experience.id IN(SELECT experience_slots.experience_id from experience_slots as experience_slots where experience_slots.id NOT IN ( select experience_slot_id from experience_booking_details where date >  '$checkInCheckOut->startDate' AND date < '$checkInCheckOut->endDate') GROUP BY `experience_slots`.`experience_id )");
//
//			}
			if(@$_REQUEST['selectedPriceRange']>800){
				 $base_price=$_REQUEST['selectedPriceRange'];
				 array_push($conditions,array("ExperienceSlot.price < $base_price"));
			 }
			
			if($filterType=='basicSearch'  || ($_REQUEST['city']!="" || $_REQUEST['search_text']
!=""||$_REQUEST['experience_type']!="" || $_REQUEST['trail_type_id']!="")){
				if(@$_REQUEST['search_text']!=""){
					$searchText=$_REQUEST['search_text'];
					array_push($conditions,array('OR' => array(
								"Experience.name LIKE '%$searchText%' ",
								"Experience.about_experience LIKE '%$searchText%' ",
										)						
									 ));
				}
				
				if(@$_REQUEST['city']!=""){				
					array_push($conditions,array('Experience.city_id'=>$_REQUEST['city']));
				}
				if(@$_REQUEST['experience_type']!=""){				
					array_push($conditions,array('Experience.experience_type'=>$_REQUEST['experience_type']));
				}
				if(@$_REQUEST['trail_type_id']!=""){				
					array_push($conditions,array('Experience.trail_type_id'=>$_REQUEST['trail_type_id']));
				}
			}	
			
		}
		//debug($conditions);
		array_push($conditions,array('Experience.experience_type'=>5));
		$joins= array(

				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),  
                array(
					'table' => 'experience_slots',
                    'alias' => 'ExperienceSlot',
                    'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceSlot.experience_id' .$join_condition
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Experience.location_id'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Experience.city_id'
					)
				),
                array(
					'table' => 'experience_attendees',
					'alias' => 'ExperienceAttendee',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceAttendee.experience_id'
					)
				),
			);
       
		
		array_push($conditions,array('Experience.status'=>1,'Experience.seo_unique_url IS NOT NULL'));
		
		$experienceList=$this->Experience->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,
			'limit' => 9,
			'page'=>$page,
			'offset'=>$start,
			'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path','Location.name','Location.latitude','Location.longitude','ExperienceType.type','City.name','ExperienceSlot.*'),
            'group' => '`Experience`.`id`',
			'order' => 'Experience.rating DESC'
		));
		foreach($experienceList as $key=>$listItem){
			$experience_id=$listItem['Experience']['id'];
			$this->ExperienceSlot->recursive=-1;
			$this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));  
			unset($this->ExperienceSlot->virtualFields['no_of_slots']);
			$experienceSlots=$this->ExperienceSlot->find('all',array('conditions'=>array('ExperienceSlot.experience_id'=> $experience_id, 'ExperienceSlot.status'=>1),'order'=>'ExperienceSlot.price ASC'));
				//("select ExperienceSlot.* from  experience_slots ExperienceSlot WHERE ExperienceSlot.experience_id=$experience_id and ExperienceSlot.status=1 ORDER BY ExperienceSlot.price ASC"); 
			//debug($experienceSlots);
			unset($experienceList[$key]['ExperienceSlot']);
			foreach($experienceSlots as $slots){
				$experienceList[$key]['ExperienceSlot'][]=$slots['ExperienceSlot'];
			}
		}
		
		$experienceListTotal=$this->Experience->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,			
			'fields' => array('Experience.*'),
            'group' => '`Experience`.`id`',
			'order' => 'Experience.created DESC'
		));
		if(empty($_GET["rowcount"])) {
			$_GET["rowcount"] = count($experienceListTotal);
		}

		if($pagination_setting == "prev-next") {
			$perpageresult = $this->perPage->getPrevNext($_GET["rowcount"], $paginationlink,$pagination_setting);	
		} else {
			$perpageresult = $this->getAllPageLinks($_GET["rowcount"], $paginationlink,$pagination_setting);	
		}
		//echo $perpageresult;
		 $this->set('perpageresult',$perpageresult);	
     //  $log = $this->Experience->getDataSource()->getLog(false, false);
      //debug($log);
      
      // debug($experienceList);
       $this->set('experienceList',$experienceList);
        
    }
    
	
	public function searchAjaxFilter(){
        $this->layout=false;
		$conditions=array();
		$page = 1;
		if(!empty($_GET["page"])) {
			$page = $_GET["page"];
		}
		
		$start = ($page-1)*1;
		if($start < 0) $start = 0;

		$paginationlink = "listing/searchAjaxFilter/?page=";	
		$pagination_setting = 'all-links';
		//debug($_REQUEST);
		
		
		if(isset($_REQUEST) && isset($_REQUEST['filterType'])){
			$filterType=$_REQUEST['filterType'];
			@$selectedAmenities=json_decode($_REQUEST['selectedAmenitiesJson']);
            @$base_price=$_REQUEST['selectedPriceRange'];
            
			$conditions =array(); $join_condition='';
         //   debug($selectedAmenities);
			if(count($selectedAmenities)>0){            
				array_push($conditions,array("HomestayAmenity.amenity_id IN (".implode(",",$selectedAmenities).")"));
			}
			if($filterType=='checkInCheckOut'){         

				@$conditions=array("Homestay.id IN(SELECT room_price_details.homestay_id from room_price_details as room_price_details where room_price_details.id NOT IN ( select room_price_detail_id from occupancy_details where check_out_date > '$checkInCheckOut->startDate' AND check_in_date < '$checkInCheckOut->endDate') GROUP BY   `room_price_details`.`homestay_id` )");

			}
			if($base_price>800){
				 $base_price=$_REQUEST['selectedPriceRange'];
				 array_push($conditions,array("Homestay.base_price < $base_price"));
			}
			if($filterType=='instantBook' || $_REQUEST['instantBook']!=""){
				 $instantBook=$_REQUEST['instantBook'];
				if($instantBook=="Yes"){
					array_push($conditions,array("Homestay.instant_book = '$instantBook'"));
				}
			}
			
			if($filterType=='basicSearch' || ($_REQUEST['city']!="" || $_REQUEST['search_text']
!=""||$_REQUEST['property_type']!="")){
				if(@$_REQUEST['search_text']!=""){
					$searchText=$_REQUEST['search_text'];
					array_push($conditions,array('OR' => array(
								"Homestay.name LIKE '%$searchText%' ",
								"Homestay.description LIKE '%$searchText%' ",
										)						
									 ));
				}
				if(@$_REQUEST['city']!=""){				
					array_push($conditions,array('Homestay.city_id'=>$_REQUEST['city']));
				}
				if(@$_REQUEST['property_type']!=""){				
					array_push($conditions,array('Homestay.property_type_id'=>$_REQUEST['property_type']));
				}
			}		
		}
		array_push($conditions,array('Homestay.status'=>1,'Homestay.seo_unique_url IS NOT NULL'));
		//debug($conditions);
      	$joins= array(
				array(
					'table' => 'homestay_files',
					'alias' => 'HomestayFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayFile.homestay_id'
					)
				),
                array(
					'table' => 'homestay_amenities',
					'alias' => 'HomestayAmenity',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayAmenity.homestay_id'
					)
				),
                array(
					'table' => 'amenities',
					'alias' => 'Amenity',
					'type'  => 'LEFT',
					'conditions' => array(
						'Amenity.id = HomestayAmenity.amenity_id' 
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Homestay.location_id'
					)
				),
				array(
					'table' => 'property_types',
					'alias' => 'PropertyType',
					'type' => 'INNER',
					'conditions' => array(
						'PropertyType.id = Homestay.property_type_id'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Homestay.city_id'
					)
				),
				array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'INNER',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),
			);
         $homestayList=$this->Homestay->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,
			'limit' => 9,
			'page'=>$page,
			'offset'=>$start,
			'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path','Location.name','Location.latitude', 					'Location.longitude','PropertyType.name','City.name'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.rating DESC'
		));
     //	$log = $this->Homestay->getDataSource()->getLog(false, false); 
//debug($homestayList);
		$homestayListTotal=$this->Homestay->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,
			
			'fields' => array('Homestay.*'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.created DESC'
		));
		//debug($_GET);
		if(empty($_GET["rowcount"])) {
			$_GET["rowcount"] = count($homestayListTotal);
		}

		if($pagination_setting == "prev-next") {
			$perpageresult = $this->perPage->getPrevNext($_GET["rowcount"], $paginationlink,$pagination_setting);	
		} else {
			$perpageresult = $this->getAllPageLinks($_GET["rowcount"], $paginationlink,$pagination_setting);	
		}
		//echo $perpageresult;
		 $this->set('perpageresult',$perpageresult);	
		
    	// we are using the 'Homestay' model
    	//$homestayList = $this->paginate('Homestay');        
        $this->set('homestayList',$homestayList);
    }
    
	
	public function getAllPageLinks($count,$href) {
		$output = '';
		if(!isset($_GET["page"])) $_GET["page"] = 1;
		$this->perpage=9;
		if($this->perpage != 0)
			$pages  = ceil($count/$this->perpage);
		
		
		//echo "pages>>". $pages." count ".$count;
		if($pages>1) {
			if($_GET["page"] == 1) 
				$output = $output . '<span class="link first disabled">&#8810;</span><span class="link disabled">&#60;</span>';
			else	
				$output = $output . '<a class="link first" onclick="getresult(\'' . $href . (1) . '\')" >&#8810;</a><a class="link" onclick="getresult(\'' . $href . ($_GET["page"]-1) . '\')" >&#60;</a>';
			
			
			if(($_GET["page"]-3)>0) {
				if($_GET["page"] == 1)
					$output = $output . '<span id=1 class="link current">1</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href . '1\')" >1</a>';
			}
			if(($_GET["page"]-3)>1) {
					$output = $output . '<span class="dot">...</span>';
			}
			
			for($i=($_GET["page"]-2); $i<=($_GET["page"]+2); $i++)	{
				if($i<1) continue;
				if($i>$pages) break;
				if($_GET["page"] == $i)
					$output = $output . '<span id='.$i.' class="link current">'.$i.'</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href . $i . '\')" >'.$i.'</a>';
			}
			
			if(($pages-($_GET["page"]+2))>1) {
				$output = $output . '<span class="dot">...</span>';
			}
			if(($pages-($_GET["page"]+2))>0) {
				if($_GET["page"] == $pages)
					$output = $output . '<span id=' . ($pages) .' class="link current">' . ($pages) .'</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href .  ($pages) .'\')" >' . ($pages) .'</a>';
			}
			
			if($_GET["page"] < $pages)
				$output = $output . '<a  class="link" onclick="getresult(\'' . $href . ($_GET["page"]+1) . '\')" >></a><a  class="link" onclick="getresult(\'' . $href . ($pages) . '\')" >&#8811;</a>';
			else				
				$output = $output . '<span class="link disabled">></span><span class="link disabled">&#8811;</span>';
			
			
		}
		return $output;
	}
    
}
?>