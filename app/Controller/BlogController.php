<?php
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class BlogController extends AppController {
	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','Experience','ExperienceFile','ExperienceType','Slider','BlogPost');
	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
    
    public function index(){
        $conditions=array('BlogPost.status'=>1);
        $blogPosts=$this->BlogPost->find('all', array(				
	            'conditions'=>$conditions,
				'fields' => array('BlogPost.*'),           
				'order' => 'BlogPost.created DESC'
			));
        
        
        $conditions_side=array('BlogPost.status'=>1);
	        $sideWidgetPosts=$this->BlogPost->find('all', array(				
	            'conditions'=>$conditions_side,
				'fields' => array('BlogPost.*'), 
				'limit'=>4,
				'order' => 'BlogPost.created DESC'
				
			));
			
			$conditions_related=array('BlogPost.status'=>1);
	        $realatedPosts=$this->BlogPost->find('all', array(				
	            'conditions'=>$conditions_related,
				'fields' => array('BlogPost.*'), 
				'limit'=>2,
				'order' => 'BlogPost.created DESC'
				
			));
			
			
		//	$log = $this->BlogPost->getDataSource()->getLog(false, false);
//debug($log);
			
      
		$this->set('blogPosts',$blogPosts);
		$this->set('sideWidgetPosts',$sideWidgetPosts);
		$this->set('realatedPosts',$realatedPosts);
		//debug($sideWidgetPosts);
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		@$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		@$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		@$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
    }
    
	public function post($id){
		
		if($id!=""){
			//base64_decode($id);
			$post_id=$id;
	    	$conditions=array('BlogPost.id'=>$post_id);
	        $postData=$this->BlogPost->find('first', array(				
	            'conditions'=>$conditions,
				'fields' => array('BlogPost.*'),           
				'order' => 'BlogPost.created DESC'
			));
			
			$conditions_side=array("BlogPost.id !=$post_id",'status'=>1);
	        $sideWidgetPosts=$this->BlogPost->find('all', array(				
	            'conditions'=>$conditions_side,
				'fields' => array('BlogPost.*'), 
				'limit'=>4,
				'order' => 'BlogPost.created DESC'
				
			));
			
			$conditions_related=array("BlogPost.id !=$post_id",'status'=>1);
	        $realatedPosts=$this->BlogPost->find('all', array(				
	            'conditions'=>$conditions_related,
				'fields' => array('BlogPost.*'), 
				'limit'=>2,
				'order' => 'BlogPost.created DESC'
				
			));
			
			
		//	$log = $this->BlogPost->getDataSource()->getLog(false, false);
//debug($log);
			
        }
		$this->set('postData',$postData);
		$this->set('sideWidgetPosts',$sideWidgetPosts);
		$this->set('realatedPosts',$realatedPosts);
		//debug($sideWidgetPosts);
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		@$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		@$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		@$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
    }
    
    
	
	
}
?>