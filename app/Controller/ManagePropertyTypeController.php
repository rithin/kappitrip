<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManagePropertyTypeController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry');

	public function beforeFilter()
	{
        parent::beforeFilter();
       // $this->Auth->allow();
	}
    public function property_type_list(){ 
		 $conditions=array('PropertyType.status'=>1);
         $propertyTypes=$this->PropertyType->find('all', array(
			
            'conditions'=>$conditions,
			'fields' => array('PropertyType.*'),           
			'order' => 'PropertyType.created DESC'
		));
		$log = $this->PropertyType->getDataSource()->getLog(false, false);
//debug($log);
        $this->set('propertyTypes',$propertyTypes);	
      //  debug($propertyTypes);
    }

    public function add_edit($id=null){
    	if($id!=""){
	    	 $conditions=array('PropertyType.id'=>$id);
	         $this->request->data=$this->PropertyType->find('first', array(
				
	            'conditions'=>$conditions,
				'fields' => array('PropertyType.*'),           
				'order' => 'PropertyType.created DESC'
			));
        }

    }
   public function delete($id=""){      
        //$this->PropertyType->delete($id);
   		$data['PropertyType']['id']=$id;
   		 $data['PropertyType']['status']=0;
         $response=$this->PropertyType->SaveAll($data);
         $this->redirect(array('controller'=>'manage_property_type','action'=>'property_type_list'));
     }
  public function save(){        
        if($_POST!=''){
           
          // debug($_POST['data']); 
        	$_POST['data']['PropertyType']['status']=1;
            $response=$this->PropertyType->SaveAll($_POST['data']['PropertyType']);
            //die;
           $this->redirect(array('controller'=>'manage_property_type','action'=>'property_type_list'));
        }

      }  

    
}
?>