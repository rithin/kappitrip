<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageHomestaysController extends AppController {

	var $layout ="default";
	public $components = array('Paginator');
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail', 'OccupancyDetail', 'Amenity', 'HomestayAmenity','MasterBedType','PropertyType','RoomType','Location','Host', 'PriceX','PriceXHoliday','PriceXSeasonal');

	public function beforeFilter()
	{
        parent::beforeFilter();
       // $this->Auth->allow();
	}
    public function property_list(){
        
        //debug($this->request->data);
        //$homestays=$this->Homestay->find('all');
	
        $joins=array(); $conditions=array();
        if(@$this->request->data['Homestay']['property_name']!=""){
            @$property_name=$this->request->data['Homestay']['property_name'];
            @array_push($conditions,array("Homestay.name LIKE '%$property_name%' "));
        }
        
        $role_id = $this->Session->read('Auth.User.role_id');
        $user_id = $this->Session->read('Auth.User.id');
        
        
        
        if($role_id!=1){
            @array_push($conditions,array("Homestay.host_id ='$user_id' "));
        }
        
        //debug($conditions);
        
		$this->paginate = array(
			'joins' =>$joins,
			'conditions' => $conditions,
			'limit' => 10,
			'order' => array('Homestay.created' => 'DESC'),
			'fields' => array('Homestay.*'),
                        'group' => '`Homestay`.`id`',
		);
	
        $homestays = $this->paginate('Homestay');
        $this->set('homestays',$homestays);		
        //debug($rooms);
    }
    public function delete($id="",$homestay_id=''){      
        $this->Homestay->delete($id);
        $this->HomestayFile->deleteAll(array('HomestayFile.homestay_id' => $id), false);
        $this->redirect(array('controller'=>'manage_homestays','action'=>'property_list'));
     }
    public function deleteFile($id="",$homestay_id=''){         
         $data= $this->HomestayFile->find('first',array('conditions'=>array('HomestayFile.id'=>$id)));  
	//	debug($data);
		if(count($data)>0){
			 $targetFile = WWW_ROOT.$data['HomestayFile']['file_path'].'/'.$data['HomestayFile']['file_name'];
			 @unlink(@$targetFile);
			 $this->HomestayFile->delete($id);
		 }
		 $homestay_id=$data['HomestayFile']['homestay_id'];
		 $action_url='add/'.$homestay_id.'/listing';
         $this->redirect(array('controller'=>'manage_homestays','action'=>$action_url));
    }
	 public function deleteRoom($homestay_id="",$room_price_detail_id=""){   
		
        $status_id=2; //disable		
		$RoomPriceDetailArr['RoomPriceDetail']['homestay_id'] =$homestay_id;
		$RoomPriceDetailArr['RoomPriceDetail']['id'] =$room_price_detail_id;
        $RoomPriceDetailArr['RoomPriceDetail']['status'] =$status_id;                
        $this->RoomPriceDetail->saveAll($RoomPriceDetailArr);
		$dataRoom=$this->RoomPriceDetail->query("SELECT count(id) as room_count FROM room_price_details WHERE homestay_id=$homestay_id and status=1;");
		$no_of_bedrooms=$dataRoom[0][0]['room_count'];
		$dataHomestay['Homestay']['id']=$homestay_id;
		$dataHomestay['Homestay']['no_of_bedrooms']=$no_of_bedrooms;
		$this->Homestay->saveAll($dataHomestay);
		// die;
		$action_url='add/'.$homestay_id.'/pricing';		
        $this->redirect(array('controller'=>'manage_homestays','action'=>$action_url));
     }
    public function add($id=''){ 
        $array_cities=$this->City->find('list');
        $this->set('array_cities',$array_cities);
		$array_property_types=$this->PropertyType->find('list');
        $this->set('array_property_types',$array_property_types);
		$array_room_types=$this->RoomType->find('list',array('fields'=>array('id','type')));
        $this->set('array_room_types',$array_room_types);
		
		$locations_arr=$this->Location->find('list',array('conditions'=>array('Location.in_search'=>1), 'fields'=>array('id','name')));
		$this->set('locations_arr',$locations_arr);	
        
                $hosts_arr = array();
                $hosts_arr_list=$this->User->find('all',array('conditions'=>array('User.role_id'=>3), 'fields'=>array('id','first_name','last_name')));
		if(!empty($hosts_arr_list)){
                    foreach($hosts_arr_list as $host_data){
                        $hosts_arr[$host_data['User']['id']] = $host_data['User']['first_name'].' '.$host_data['User']['last_name'];
                    }
                }
                $this->set('hosts_arr',$hosts_arr);	
		
        $array_bed_types=$this->MasterBedType->find('list');
        $this->set('array_bed_types',$array_bed_types);
        if($id!="")	{
            $OccupanyDataArray=array();
            $this->request->data= $this->Homestay->find('first',array(				
				'conditions'=>array('Homestay.id'=>$id))); 
			
			$this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
			$this->RoomPriceDetail->unbindModel(array('belongsTo' => array('RoomType')));
			$roomDtsls=$this->RoomPriceDetail->find('all',array('conditions'=>array('RoomPriceDetail.homestay_id'=>$id,'RoomPriceDetail.status'=>1)));
			if(count($roomDtsls)>0){
				unset($this->request->data['RoomPriceDetail']);
				foreach($roomDtsls as $key_room=>$dtl_room){
					$this->request->data['RoomPriceDetail'][$key_room]=$dtl_room['RoomPriceDetail'];
				}
			}
	
			//debug($this->request->data);
			
			$this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));	
            $homestayFiles=$this->HomestayFile->find('all',array('conditions'=>array('HomestayFile.homestay_id'=>$id)));  
            $this->set('homestayFiles',$homestayFiles);	
			
			//fetching rooms to list the at the availability tab..	
            $rooms=$this->RoomPriceDetail->find('list',array('conditions'=>array('RoomPriceDetail.homestay_id'=>$id,'RoomPriceDetail.status'=>1), 'fields'=>array('id','room_no_name'))); 	
            $this->set('rooms',$rooms);	
			
            //finding General Amenities for the home stay from master table.
            $amenities_arr=$this->Amenity->find('all',array('fields'=>array('id','amenity','description')));
            $this->set('amenities_arr',$amenities_arr);	
            
            $homestayAmenities_arr=$this->HomestayAmenity->find('list',array('fields'=>array('amenity_id'), 'conditions'=>array('HomestayAmenity.homestay_id'=>$id)));
            $this->set('homestayAmenities_arr',$homestayAmenities_arr);	
           //homestayAmenities_arr debug($homestayAmenities_arr);
            
            $roomPriceDetails=$this->RoomPriceDetail->find('all', array('conditions'=>array ('RoomPriceDetail.homestay_id'=>$id, 'RoomPriceDetail.status'=>1))); 
             
            foreach($roomPriceDetails as $keyDtl=>$detail){
                $room_price_detail_id=$detail['RoomPriceDetail']['id'];
                $bedroomDetails = $this->BedroomDetail->find('list', array('conditions'=> array('BedroomDetail.homestay_id'=>$id,'BedroomDetail.room_price_detail_id'=>$room_price_detail_id), 'fields'=>array('id','master_bed_type_id')));
                
                $bedcountDetails = $this->BedroomDetail->find('list', array('conditions'=> array('BedroomDetail.homestay_id'=>$id,'BedroomDetail.room_price_detail_id'=>$room_price_detail_id), 'fields'=>array('master_bed_type_id','no_of_bed')));
                
                $this->request->data['MasterBedType']["BedRoom_$keyDtl"]=$bedroomDetails;
                $this->request->data['MasterBedType']["BedCount_$keyDtl"]=$bedcountDetails;
               // $this->request->data['MasterBedType']["BedRoom_$keyDtl"]['id']=
                $this->request->data['RoomPriceDetail'][$keyDtl]["id"]=$detail['RoomPriceDetail']['id'];
                $this->request->data['RoomPriceDetail'][$keyDtl]["room_base_price"]=$detail['RoomPriceDetail']['room_base_price'];
                $this->request->data['RoomPriceDetail'][$keyDtl]["extra_bed_charge"]=$detail['RoomPriceDetail']['extra_bed_charge'];
                $this->request->data['RoomPriceDetail'][$keyDtl]["extra_bed"]=$detail['RoomPriceDetail']['extra_bed'];
            }
            //finding availabilty on this month of current homestay.
            //here finding count by using virtual field(no_of_bookings) mentioned in the corresponding model.
            //doing the above coz, cakephp not give aggreagte results on 'list' method
            $OccupanyDataArray=$this->OccupancyDetail->find('all', array('conditions'=>array( 'OccupancyDetail.homestay_id'=>$id, 
                        array('OccupancyDetail.check_in_date BETWEEN ? AND ?' => array(date('Y-m-01'), date('Y-m-t'))), 'OccupancyDetail.status IN (14,18)' ),'fields'=>array('check_in_date' ,'check_out_date' ,'OccupancyDetail.no_of_bookings'), 'group'=>'check_in_date')); 
            
            $occupancyDetailString=json_encode($OccupanyDataArray);
            $this->set('occupancyDetailString',$occupancyDetailString);
            //$this->PriceX->recursive=2;
            $dataPriceX=$this->PriceX->find('first',array('conditions'=>array('PriceX.homestay_id'=>$id)));
			@$this->request->data['PriceX']=$dataPriceX['PriceX'];
			@$this->request->data['PriceXSeasonal']=$dataPriceX['PriceXSeasonal'];
			@$this->request->data['PriceXHoliday']=$dataPriceX['PriceXHoliday'];
			//debug($this->request->data);
        }
    }
    public function fileUpload(){
        @session_start();
        $this->autoRender=false;        
        $ds          = DIRECTORY_SEPARATOR;  
        $storeFolder = 'uploads/temp_files'; 
		//unset($_SESSION['room_images']);
		//debug($_REQUEST);
		//debug($_FILES);
		$request=1;
		if(isset($_POST['request'])){ 
			  $request = $_POST['request'];
		}
        if ((!empty($_FILES))&& $request==1) {
			
			for($i=0; $i< count($_FILES['file']['name']); $i++){             
				$tempFile = $_FILES['file']['tmp_name'][$i];         
				$targetPath = WWW_ROOT.$storeFolder . $ds;  
				$fileName=""; $time=time();
				if($_FILES['file']['type'][$i]=="image/jpeg"){
					 $targetFile =  $targetPath. $time.".jpg"; 
					 $fileName=$time.".jpg";
				}else if($_FILES['file']['type'][$i]=="image/png"){
					 $targetFile =  $targetPath. $time.".png";  
					 $fileName=$time.".png";
				}else if($_FILES['file']['type'][$i]=="image/gif"){
					 $targetFile =  $targetPath. $time.".gif";  
					 $fileName=$time.".gif";
				}else if($_FILES['file']['type'][$i]=="image/webp"){
					 $targetFile =  $targetPath. $time.".webp";  
					 $fileName=$time.".webp";
				}else{
					$targetFile=$targetPath. $_FILES['file']['name'][$i]; 
					$fileName= $time.$_FILES['file']['name'][$i];
					//echo "Error. Invalid file type..!";
				}
				 $_FILES["file"]["name"][$i]=$fileName;
				move_uploaded_file($tempFile,$targetFile);            
				chmod($targetFile, 0777);
				$arrayImg['file']=$targetFile;
				$arrayImg['name']=$fileName;
				$_SESSION['room_images'][$time]=$arrayImg;
			   // array_push($_SESSION['room_images'],$arrayImg);
			}
        }else if($request==2){
			$nameArr=explode(".",$_POST['name']);
			$key=$nameArr[0];
			$targetDeletedFile= $_SESSION['room_images'][$key]['file'];
			unlink(@$targetDeletedFile);
			unset($_SESSION['room_images'][$key]);
			//debug($_SESSION);
		}
		debug($_SESSION);
      }
    public function save(){    
		
        if(@$_POST['data']!="" && $_POST['data']['Homestay']['name']!=""){
            $bedroomDetails=array(); $roomPriceDetails=array();
            $baseUploadPath = WWW_ROOT."uploads/room_pics/";
            $this->Homestay->unbindModel(array('hasMany' => array('RoomPriceDetail')));		
            $_POST['data']['Homestay']['created_by'] = $this->Session->read('Auth.User.id');
            $response=$this->Homestay->SaveAll($_POST['data']);  
            $folderPath="uploads/room_pics/";
            if($_POST['data']['Homestay']['id']!="")
                $LastInsertID=$_POST['data']['Homestay']['id'];
           else
               $LastInsertID=$this->Homestay->getLastInsertID();
            
            $targetPath = $baseUploadPath.$LastInsertID;   
            $dbPath=$folderPath.$LastInsertID;   
            if($response==true){               
                if (file_exists($targetPath)=="") {
                    mkdir($targetPath, 0777, true); 
                    chmod($targetPath, 0777);
                }
				//debug($_SESSION);
				//die;
                if(isset($_SESSION['room_images']) && count($_SESSION['room_images'])>0){
                    //Moving Files to orginal directory.
                    foreach($_SESSION['room_images'] as $key=>$item){
                        $targetFile="";
                        $roomfiles['homestay_id']=$LastInsertID;
                        $roomfiles['status']=1;
                        $roomfiles['file_path']=$dbPath;
                        $roomfiles['file_name']=$item['name'];
                        $this->HomestayFile->SaveAll($roomfiles);
                        @chmod($targetPath, 0777);
                        $targetFile=$targetPath."/".$item['name'];  
                        $sourceFile=$item['file'];
                        //debug($roomfiles);                  
                        system("mv $sourceFile $targetPath");                   
                    }
                    
                 unset( $_SESSION['room_images']);     
               }
            }
          
          $action_url='add/'.$LastInsertID.'/amenities';
          $this->redirect(array('controller'=>'manage_homestays','action'=>$action_url));
        }else{
		
		  $action_url='add/';
          $this->redirect(array('controller'=>'manage_homestays','action'=>$action_url));
		}

      }
	
	public function updateStatus($homestay_id=null,$status_code=null){
		$this->autoRender=false;  
		if($status_code=='Enable'){
			$status_id=1;
		}else{
			$status_id=2;
		}
		$homestayStatusArr['Homestay']['id'] =$homestay_id;
        $homestayStatusArr['Homestay']['status'] =$status_id;                
        $this->Homestay->saveAll($homestayStatusArr);
		$this->redirect(array('controller'=>'manage_homestays','action'=>'property_list'));
	}
    public function findAvailability(){
        $this->autoRender=false;  
        $data=array();
        $homestay_id=$_REQUEST['homestay_id'];
        $start_date=$_REQUEST['start_date'];
		 $condition=" ( check_out_date > '$start_date' AND check_in_date <= '$start_date') ";
        $OccupancyDetail=$this->OccupancyDetail->find('list', array(            
         'conditions'=>array( 'OccupancyDetail.homestay_id'=>$homestay_id,'OccupancyDetail.room_price_detail_id IS NOT NULL', $condition,'OccupancyDetail.status IN (14,18)'),'fields'=>array('OccupancyDetail.room_price_detail_id'))); 
		 

		$log = $this->OccupancyDetail->getDataSource()->getLog(false, false);
//debug($log);
		//debug($OccupancyDetail);
        if(count($OccupancyDetail)>0){
            foreach($OccupancyDetail as $val){
                $data[]=$val;
            }
            echo  json_encode($data);
        }else{
            echo  json_encode($data);
        }
        exit;
    }
    
    public function saveAvailability(){
        $this->autoRender=false;   
        $homestay_id=$_POST['data']['Homestay']['id'];
        $check_in_date=$_POST['data']['OccupancyDetail']['check_in_date'];
		$check_out_date=$_POST['data']['OccupancyDetail']['check_out_date'];
		debug($_REQUEST);
		//die;
		if($check_out_date==""){
			$check_out_date= date('Y-m-d', strtotime("$check_in_date" . ' +1 day'));
		} 
		
        $this->OccupancyDetail->deleteAll(array('OccupancyDetail.homestay_id' => $homestay_id,'OccupancyDetail.check_in_date'=>$check_in_date), false);
        $dataOccupancy=array();
		if($_POST['data']['OccupancyDetail']['bedrooms']!='' && count($_POST['data']['OccupancyDetail']['bedrooms'])>0){
			foreach($_POST['data']['OccupancyDetail']['bedrooms'] as $key=>$value){
				$dataOccupancy[$key]['OccupancyDetail']['room_price_detail_id']=$value;
				$dataOccupancy[$key]['OccupancyDetail']['homestay_id']=$homestay_id;
				$dataOccupancy[$key]['OccupancyDetail']['check_in_date']=$check_in_date;
				$dataOccupancy[$key]['OccupancyDetail']['check_out_date']=$check_out_date;
				$dataOccupancy[$key]['OccupancyDetail']['status']=18;
			}
			$this->OccupancyDetail->saveAll($dataOccupancy);
		}
        $action_url='add/'.$homestay_id.'/availability';		
        $this->redirect(array('controller'=>'manage_homestays','action'=>$action_url));
    }
    
    public function saveAmenties(){
        $this->autoRender=false; 
        //debug($_POST);
        $amenities_arr=array();
        $homestay_id=$_POST['data']['Homestay']['id'];
        foreach($_POST['data']['HomestayAmenity']['amenity_id'] as $key=>$value){
            if($value!=0){
                $amenities_arr[$key]['HomestayAmenity']['amenity_id']=$value;
                $amenities_arr[$key]['HomestayAmenity']['homestay_id']=$homestay_id;
            }
        }
        $this->HomestayAmenity->deleteAll(array('HomestayAmenity.homestay_id' => $homestay_id), false);
        $this->HomestayAmenity->saveAll($amenities_arr);
        $action_url='add/'.$homestay_id.'/pricing';
        $this->redirect(array('controller'=>'manage_homestays','action'=>$action_url));
       // debug($amenities_arr);
      //  die;
    }
	
   public function savePricingFactor(){
	  $this->autoRender=false; 
	   $homestay_id=$_POST['data']['Homestay']['id'];
	  //debug($_POST['data']);
	 // die;
		if(isset($_POST['data']['RoomPriceDetail']) && ($_POST['data']['Homestay']['tabName']=="pricing")){
            foreach($_POST['data']['RoomPriceDetail'] as $key=>$roomPriceDetail){
                    $roomPriceDetails['RoomPriceDetail']['id']=$roomPriceDetail['id'];
                    $roomPriceDetails['RoomPriceDetail']['room_base_price']=$roomPriceDetail['room_base_price'];
                    $roomPriceDetails['RoomPriceDetail']['extra_bed_charge']=$roomPriceDetail['extra_bed_charge'];
                    $roomPriceDetails['RoomPriceDetail']['extra_bed']=$roomPriceDetail['extra_bed'];
				 	$roomPriceDetails['RoomPriceDetail']['room_type_id']=$roomPriceDetail['room_type_id'];
					$roomPriceDetails['RoomPriceDetail']['room_description']=$roomPriceDetail['room_description'];
                    $roomPriceDetails['RoomPriceDetail']['status']=1;
                    $roomPriceDetails['RoomPriceDetail']['room_no_name']=$roomPriceDetail['room_no_name'];
                    $roomPriceDetails['RoomPriceDetail']['homestay_id']=$_POST['data']['Homestay']['id'];
                    /// finding the no of max beds in each room
                    // it's not considering the extra bed.
                    $no_of_adult_stay="";
					//debug($_POST['data']); 
					$no_of_adult_stay="";
					if(isset($_POST['data']['MasterBedType']['BedRoom_'.$key])){
						foreach($_POST['data']['MasterBedType']['BedRoom_'.$key]  as $key_bed=>$bedDetail){
							$no_of_people_dtls = $this->MasterBedType->find('first', array('conditions'=> array('MasterBedType.id'=> $bedDetail) , 'fields'=>array('no_of_adult')));
							$no_of_bed=$_POST['data']['MasterBedCount']["BedCounter_BedRoom_$key"][$bedDetail];
							$no_of_adult_stay+=($no_of_bed*$no_of_people_dtls['MasterBedType']['no_of_adult']);
						}

					}
					
					$roomPriceDetails['RoomPriceDetail']['no_of_bed']=$no_of_adult_stay;
					//    debug($roomPriceDetails);

					$this->RoomPriceDetail->saveAll($roomPriceDetails);  
					if($roomPriceDetail['id']!="")
						$lastInsertRoomDetailID=$roomPriceDetail['id'];
					else
						$lastInsertRoomDetailID = $this->RoomPriceDetail->getLastInsertID();    
						
					

                  /*       //saving bed arrangement details.
                    iterating thorugh seletected bed types
                    formatting the array with bed type values and roompricedetails last insert value and saving to db.
                  */
                    //debug($_POST['data']);
                    foreach($_POST['data']['MasterBedType'] as $key_room=>$bedRooms){
                        foreach($bedRooms as $key_bed=>$bedDetail){
                            @$no_of_bed=$_POST['data']['MasterBedCount']["BedCounter_$key_room"][$bedDetail];
                            $bedroomDetails[$key_bed]['BedroomDetail']['master_bed_type_id']=$bedDetail;
                            $bedroomDetails[$key_bed]['BedroomDetail']['room_price_detail_id']=$lastInsertRoomDetailID;
                            $bedroomDetails[$key_bed]['BedroomDetail']['homestay_id']=$_POST['data']['Homestay']['id'];
                            $bedroomDetails[$key_bed]['BedroomDetail']['no_of_bed']=$no_of_bed;
                        }
                       $this->BedroomDetail->deleteAll(array('BedroomDetail.room_price_detail_id' => $lastInsertRoomDetailID), false);
                     //debug($bedroomDetails);
                        $this->BedroomDetail->saveAll($bedroomDetails);
                        unset($bedroomDetails);
                        unset($_POST['data']['MasterBedType'][$key_room]); //unsetting the saved value to avoid multiple iteration.
                        break;
                    }
                    unset($roomPriceDetails);
              }
              // die;
                $homestayBasePriceArr=array();
                //saving the min price to main table. // it will help to display at front end.
                $roomBasePriceDetails=$this->RoomPriceDetail->find('first',array('conditions'=> array('RoomPriceDetail.homestay_id'=> $_POST['data']['Homestay']['id'],'RoomPriceDetail.status'=>1),'fields'=>array('MIN(room_base_price) as min_base_price'))); 
                $homestayBasePriceArr['Homestay']['id'] =$_POST['data']['Homestay']['id'];
                $homestayBasePriceArr['Homestay']['base_price'] =$roomBasePriceDetails[0]['min_base_price'];                
                $this->Homestay->saveAll($homestayBasePriceArr);
        }
		
		$action_url='add/'.$homestay_id.'/pricingfactors';
        $this->redirect(array('controller'=>'manage_homestays','action'=>$action_url));
	}
	public function savePricingStrategy(){
		//models which are using for PriceX are:-
		//, 'PriceX','PriceXHoliday','PriceXSeasonal'
		debug($this->data);
		//die;
		if($_POST['data']['Homestay']['smart_price']=="Yes"){
			$this->Homestay->saveAll($_POST['data']['Homestay']);
			$homestay_id=$_POST['data']['Homestay']['id'];
			$_POST['data']['PriceX']['homestay_id']=$_POST['data']['Homestay']['id'];
			
			$this->PriceX->saveAll($_POST['data']['PriceX']);
			if($_POST['data']['PriceX']['id']!="")
					$lastInsertPriceXID=$_POST['data']['PriceX']['id'];
			else
					$lastInsertPriceXID = $this->PriceX->getLastInsertID();    
			//$dataSeasonal=array();$dataHoliday=array();
			if($_POST['data']['PriceX']['seasonal_price']=='Yes'){
				$this->PriceXSeasonal->deleteAll(array('PriceXSeasonal.price_x_id' => $lastInsertPriceXID), false);
				if(isset($_POST['data']['PriceXSeasonal']) && count($_POST['data']['PriceXSeasonal'])>0){
					foreach($_POST['data']['PriceXSeasonal'] as $key=>$PriceXSeasonal){
						$dataSeasonal['PriceXSeasonal'][$key]=$PriceXSeasonal;
						$dataSeasonal['PriceXSeasonal'][$key]['price_x_id']=$lastInsertPriceXID;
					}
					$this->PriceXSeasonal->saveAll($dataSeasonal['PriceXSeasonal']);
				}
				if(!isset($_POST['data']['PriceXSeasonal']) || count($_POST['data']['PriceXSeasonal'])==0){
					$_POST['data']['PriceX']['seasonal_price']='No';
					$_POST['data']['PriceX']['id']=$lastInsertPriceXID;
					$this->PriceX->saveAll($_POST['data']['PriceX']);
				}
			}
			//delting all the seasonal pricing
			if($_POST['data']['PriceX']['seasonal_price']=='No'){
				$this->PriceXSeasonal->deleteAll(array('PriceXSeasonal.price_x_id' => $lastInsertPriceXID), false);
			}
			if($_POST['data']['PriceX']['holiday_price']=='Yes'){
				$this->PriceXHoliday->deleteAll(array('PriceXHoliday.price_x_id' => $lastInsertPriceXID), false);
				if(isset($_POST['data']['PriceXHoliday']) && count($_POST['data']['PriceXHoliday'])>0){
					foreach($_POST['data']['PriceXHoliday'] as $keyHoliday=>$PriceXHoliday){
						$dataHoliday['PriceXHoliday'][$keyHoliday]=$PriceXHoliday;
						$dataHoliday['PriceXHoliday'][$keyHoliday]['price_x_id']=$lastInsertPriceXID;
					}
					//debug($dataHoliday['PriceXHoliday']);
					$this->PriceXHoliday->saveAll($dataHoliday['PriceXHoliday']);
				}
				if(!isset($_POST['data']['PriceXHoliday']) || count($_POST['data']['PriceXHoliday'])==0){
					$_POST['data']['PriceX']['holiday_price']='No';
					$_POST['data']['PriceX']['id']=$lastInsertPriceXID;
					$this->PriceX->saveAll($_POST['data']['PriceX']);
				}
			}
			if($_POST['data']['PriceX']['holiday_price']=='No'){
				$this->PriceXHoliday->deleteAll(array('PriceXHoliday.price_x_id' => $lastInsertPriceXID), false);
			}
			
		}else{
			$this->Homestay->saveAll($_POST['data']['Homestay']);
		}
		//debug($this->data);
		//die;
		$action_url='add/'.$homestay_id.'/availability';
        $this->redirect(array('controller'=>'manage_homestays','action'=>$action_url));
	}
	
	public function findLocation(){
		$this->autoRender=false;// ajax method for finding the rooms of property.       
		$city_id=$_REQUEST['city_id'];
		$locationsDtls=$this->Location->query("SELECT id,name from locations as Location  WHERE  `Location`.`city_id`=$city_id  ");
		//debug($freeRoomDetail);
		$locations=array();
		foreach($locationsDtls as $dtl){
			$locations[$dtl['Location']['id']]=$dtl['Location']['name'];
		}
		echo json_encode($locations);
		
	}
	
}
?>