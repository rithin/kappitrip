<?php

App::uses('AppController', 'Controller');


class IndexController extends AppController {
	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','Experience','ExperienceFile','ExperienceType','Slider','BlogPost');
	public function beforeFilter()
	{
            parent::beforeFilter();
            $this->Auth->allow();
	}
	public function index(){	
        $userId = $this->Auth->user('id');
		$conditions=array('Homestay.status'=>1);
        $homestayList=$this->Homestay->find('all', array(
			'joins' => array(
				array(
					'table' => 'homestay_files',
					'alias' => 'HomestayFile',
					'type' => 'INNER',
					'conditions' => array(
						'Homestay.id = HomestayFile.homestay_id'
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Homestay.location_id'
					)
				),
				array(
					'table' => 'property_types',
					'alias' => 'PropertyType',
					'type' => 'INNER',
					'conditions' => array(
						'PropertyType.id = Homestay.property_type_id'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Homestay.city_id'
					)
				),
				array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'INNER',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),
			),
			'conditions'=>$conditions,
			'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path','Location.name','Location.latitude', 					'Location.longitude','PropertyType.name','City.name'),
            'group' => '`Homestay`.`id`',
			'order' => array('Homestay.high_demand DESC','Homestay.rating DESC'),
			'limit'=>4
		));
        $conditions_expr=array('Experience.status'=>1,'Experience.experience_type!=5');
        $experienceList=$this->Experience->find('all', array(
			'joins' => array(

				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type' => 'INNER',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Experience.location_id'
					)
				),
				array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type' => 'INNER',
					'conditions' => array(
						'ExperienceType.id = Experience.experience_type'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Experience.city_id'
					)
				),

			),
			'conditions'=>$conditions_expr,
			'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path','ExperienceType.type', 'Location.name','Location.latitude','Location.longitude','City.name'),
            'group' => '`Experience`.`id`',
			'limit'=>4,
			'order' => 'Experience.created DESC'
		));
		
		
		
		$conditions_event=array('Experience.status'=>1,'Experience.experience_type'=>5);
        $eventList=$this->Experience->find('all', array(
			'joins' => array(

				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type' => 'INNER',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Experience.location_id'
					)
				),
				array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type' => 'INNER',
					'conditions' => array(
						'ExperienceType.id = Experience.experience_type'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Experience.city_id'
					)
				),
                

			),
			'conditions'=>$conditions_event,
			'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path','ExperienceType.type', 'Location.name','Location.latitude','Location.longitude','City.name'),
            'group' => '`Experience`.`id`',
			'limit'=>4,
			'order' => 'Experience.created DESC'
		));
      // debug($eventList);
       // $log = $this->Homestay->getDataSource()->getLog(false, false);
		
		 $condition_slider=array('Slider.status'=>1);
         $sliders=$this->Slider->find('all', array(			
						'conditions'=>$condition_slider,
						'fields' => array('Slider.*'),           
						'order' => 'Slider.sort_order ASC'
					));
		
		$this->set('sliders',$sliders);	
        $this->set('homestayList',$homestayList);	
        $this->set('experienceList',$experienceList);	
		$this->set('eventList',$eventList);
		
		$blogPosts=$this->BlogPost->find('all', array(				
	            'conditions'=>array('status'=>1),
				'fields' => array('BlogPost.*'), 
				'limit'=>3,
				'order' => 'RAND()'
				
			));
		
		$this->set('blogPosts',$blogPosts);	
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
		
		//finding the latest deals 
		$array_deals=$this->findDeals();	
		$this->set('array_deals',$array_deals);
        
    }
	
	public function findDeals(){
		$array_deals=array();
		$conditions=array('Homestay.status'=>1);
        $stayDeals=$this->Homestay->find('all', array(
			'joins' => array(
				array(
					'table' => 'homestay_files',
					'alias' => 'HomestayFile',
					'type' => 'INNER',
					'conditions' => array(
						'Homestay.id = HomestayFile.homestay_id'
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Homestay.location_id'
					)
				),
				array(
					'table' => 'property_types',
					'alias' => 'PropertyType',
					'type' => 'INNER',
					'conditions' => array(
						'PropertyType.id = Homestay.property_type_id'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Homestay.city_id'
					)
				),
				array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'INNER',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),
				array(
					'table' => 'deals',
					'alias' => 'Deal',
					'type'  => 'INNER',
					'conditions' => array(
						'Homestay.id = Deal.homestay_id','Deal.status=1 ','CURDATE() between `Deal.start_date` and `Deal.end_date`'
					)
				),
			),
			'conditions'=>$conditions,
			'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path','Location.name', 'Location.latitude', 'Location.longitude','PropertyType.name','City.name','Deal.*'),
            'group' => '`Homestay`.`id`',
			'order' => array('Homestay.high_demand DESC','Homestay.rating DESC'),
			'limit'=>4
		));
		$conditions_expr=array('Experience.status'=>1,'Experience.experience_type!=5');
        $experienceDeals=$this->Experience->find('all', array(
			'joins' => array(

				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type' => 'INNER',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Experience.location_id'
					)
				),
				array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type' => 'INNER',
					'conditions' => array(
						'ExperienceType.id = Experience.experience_type'
					)
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'INNER',
					'conditions' => array(
						'City.id = Experience.city_id'
					)
				),
				array(
					'table' => 'deals',
					'alias' => 'Deal',
					'type'  => 'INNER',
					'conditions' => array(
						'Experience.id = Deal.experience_id','Deal.status=1 ','CURDATE() between `Deal.start_date` and `Deal.end_date`'
					)
				),

			),
			'conditions'=>$conditions_expr,
			'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path','ExperienceType.type', 'Location.name','Location.latitude','Location.longitude','City.name','Deal.*'),
            'group' => '`Experience`.`id`',
			'limit'=>4,
			'order' => 'Experience.created DESC'
		));
		
		
		foreach($stayDeals as $stays){
			array_push($array_deals,$stays);
		}
		foreach($experienceDeals as $experiences){
			array_push($array_deals,$experiences);
		}
		//debug($array_deals);
		return $array_deals;
	}
	
	
}
?>
