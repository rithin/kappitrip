<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageProductsController extends AppController {

	var $layout ="default";
	public $uses = array('User','Language','Product','Category','SubCategory','ProductFile','City','CoffeeType');

	public function beforeFilter()
	{
        parent::beforeFilter();
        //$this->Auth->allow();
	}
    public function product_list(){
        
    }
    
    function ajax_product_list(){
        
            $this->layout = false;
        
                $productList=$this->Product->find('all', array(
			'joins' => array(

				array(
					'table' => 'product_files',
					'alias' => 'ProductFile',
					'type' => 'LEFT',
					'conditions' => array(
						'Product.id = ProductFile.product_id'
					)
				),
                                array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'LEFT',
					'conditions' => array(
						'Category.id = Product.category_id'
					)
				),
                                array(
					'table' => 'sub_categories',
					'alias' => 'SubCategory',
					'type' => 'LEFT',
					'conditions' => array(
						'SubCategory.id = Product.sub_category_id'
					)
				),
                                

			),
			'fields' => array('Product.*','ProductFile.uploaded_img_name','ProductFile.img_path','Category.category_name','SubCategory.sub_category_name'),
            'group'=>'Product.id',
			'order' => 'Product.created DESC'
		));
                
                
                //debug($productList);
                
                $list_view_array = array();
                
                foreach($productList as $key=>$product_data){
                    
                    if($product_data['Product']['status']==""){
                        $status_code="Enable";
                    }elseif($product_data['Product']['status']==1){
                        $status_code="Disable";
                    }elseif($product_data['Product']['status']==2){
                        $status_code="Enable";
                    }else{
                        $status_code="Enable";
                    }
                    
                    $list_view_array['data'][$key]['product_name'] = $product_data['Product']['product_name'];
                    $list_view_array['data'][$key]['brand_name'] = $product_data['Product']['brand_name'];
                    $list_view_array['data'][$key]['manufacturer_name'] = $product_data['Product']['manufacturer_name'];
                    $list_view_array['data'][$key]['actions'] = "<a href='".Configure::read('app_root_path')."manage_products/add/".$product_data['Product']['id']."'>Edit</a>".
                                                                "&nbsp;&nbsp;<a href='".Configure::read('app_root_path')."manage_products/updateStatus/".$product_data['Product']['id']."/".$status_code."'>".$status_code."</a>";                
                    
                    }
                
                echo json_encode($list_view_array);
                
                exit;
        
    }
    
    public function delete($id=""){      
        $this->Product->delete($id);
        $this->ProductFile->deleteAll(array('ProductFile.product_id' => $id), false);
        $this->redirect(array('controller'=>'manage_products','action'=>'product_list'));
     }
    public function deleteFile($id=""){
         
         $data= $this->ProductFile->find('first',array('conditions'=>array('ProductFile.id'=>$id))); 
         $uploadedFile = WWW_ROOT.$data['ProductFile']['img_path'].'/'.$data['ProductFile']['uploaded_img_name'];
         unlink($uploadedFile);
         $resizedFile = WWW_ROOT.$data['ProductFile']['img_path'].'/'.$data['ProductFile']['resized_img_name'];
         unlink($resizedFile);
         $listingFile = WWW_ROOT.$data['ProductFile']['img_path'].'/'.$data['ProductFile']['listing_img_name'];
         unlink($listingFile);
         $thumbnailFile = WWW_ROOT.$data['ProductFile']['img_path'].'/'.$data['ProductFile']['thumbnail_img_name'];
         unlink($thumbnailFile);
         $this->ProductFile->delete($id);
         $this->redirect(array('controller'=>'manage_products','action'=>'product_list'));
    }
    public function add($id=''){
        
        $array_sub_categories=array(); 
        $productFiles=array();
        $array_categories=$this->Category->find('list',array('fields'=>array('id','category_name')));        
        $this->set('array_categories',$array_categories);
        
        
        $array_product_location = array(); 
        $array_product_location=$this->City->find('list',array('fields'=>array('id','name')));        
        $this->set('array_product_location',$array_product_location);
        
        $array_coffee_type = array();
        $array_coffee_type=$this->CoffeeType->find('list',array('fields'=>array('id','coffee_type_name')));        
        $this->set('array_coffee_type',$array_coffee_type);
        
       
        
        
        if($id!="")	{	
            $this->request->data= $this->Product->find('first', array(
			'joins' => array(

			   array(
					'table' => 'product_files',
					'alias' => 'ProductFile',
					'type' => 'LEFT',
					'conditions' => array(
						'Product.id = ProductFile.product_id'
					)
				),
                array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'LEFT',
					'conditions' => array(
						'Category.id = Product.category_id'
					)
				),
                array(
					'table' => 'sub_categories',
					'alias' => 'SubCategory',
					'type' => 'LEFT',
					'conditions' => array(
						'SubCategory.id = Product.sub_category_id'
					)
				)

			),
            'conditions' => array('Product.id'=>$id),
			'fields' => array('Product.*','ProductFile.uploaded_img_name','ProductFile.img_path','Category.category_name', 'SubCategory.sub_category_name'),
			'order' => 'Product.created DESC'
		    ));
           $array_sub_categories=$this->SubCategory->find('list',array('fields'=>array('id','sub_category_name'))); 
           $this->ProductFile->unbindModel(array('belongsTo' => array('Product')));
           $productFiles=$this->ProductFile->find('all',array('conditions'=>array('ProductFile.product_id'=>$id)));  
        }
        $this->set('productFiles',$productFiles);
        $this->set('array_sub_categories',$array_sub_categories);
    }
    public function loadSubCategory(){ 
        $this->layout = false;
        $categoryId = $_REQUEST['categoryId'];
        $array_sub_categories=$this->SubCategory->find('all',array('conditions' => array('SubCategory.category_id'=>$categoryId),'fields'=>array('id','sub_category_name'))); 
        echo json_encode($array_sub_categories);
        exit;
      // $this->set('array_sub_categories',$array_sub_categories);
    }
    public function fileUpload(){
        //session_start();
        
        if(!empty($_SESSION['product_images'])){
            unset($_SESSION['product_images']);
        }
        
        $this->autoRender=false;        
        $ds = DIRECTORY_SEPARATOR;  
        $storeFolder = 'uploads/temp_files';
        if (!file_exists($storeFolder)) {
            mkdir($storeFolder, 0777, true);
            chmod($storeFolder, 0777);
        }
        $request=1;
        if(isset($_POST['request'])){ 
            $request = $_POST['request'];
	}
        if ((!empty($_FILES))&& $request==1) {            
             
            for($i=0; $i< count($_FILES['file']['name']); $i++){             
				$tempFile = $_FILES['file']['tmp_name'][$i];         
				$targetPath = WWW_ROOT.$storeFolder . $ds;  
				$fileName=""; 
                                $time=time();
                                
				if($_FILES['file']['type'][$i]=="image/jpeg"){
					 $targetFile =  $targetPath. $time."_".$i.".jpg"; 
					 $fileName=$time."_".$i.".jpg";
				}else if($_FILES['file']['type'][$i]=="image/png"){
					 $targetFile =  $targetPath. $time."_".$i.".png";  
					 $fileName=$time."_".$i.".png";
				}else if($_FILES['file']['type'][$i]=="image/gif"){
					 $targetFile =  $targetPath. $time."_".$i.".gif";  
					 $fileName=$time."_".$i.".gif";
				}else if($_FILES['file']['type'][$i]=="image/webp"){
					 $targetFile =  $targetPath. $time."_".$i.".webp";  
					 $fileName=$time."_".$i.".webp";
				}else{
					$targetFile=$targetPath. $_FILES['file']['name'][$i]; 
					$fileName= $time."_".$i.$_FILES['file']['name'][$i];
					//echo "Error. Invalid file type..!";
				}
				
                                //$_FILES["file"]["name"][$i]=$fileName;
                            
                                
				move_uploaded_file($tempFile,$targetFile);            
				chmod($targetFile, 0777);
				$arrayImg['file']=$targetFile;
				$arrayImg['name']=$fileName;
                                $_SESSION['product_images'][$time][$i]=$arrayImg;
                                
                                //debug($_SESSION['product_images']);
			   // array_push($_SESSION['room_images'],$arrayImg);
			}
            
        }else if($request==2){
			$nameArr=explode(".",$_POST['name']);
			$key=$nameArr[0];
			$targetDeletedFile= $_SESSION['product_images'][$key]['file'];
			unlink(@$targetDeletedFile);
			unset($_SESSION['product_images'][$key]);
			//debug($_SESSION);
		}

      }
    public function save(){        
        if($_POST!=''){
            $baseUploadPath = WWW_ROOT."uploads/product_pics/";
           // debug($_POST['data']); 
            
            $_POST['data']['Product']['status'] = 1;
            $response=$this->Product->SaveAll($_POST['data']['Product']);
            
            $folderPath="uploads/product_pics/";
            if($_POST['data']['Product']['id']!="")
                $LastInsertID=$_POST['data']['Product']['id'];
           else
               $LastInsertID=$this->Product->getLastInsertID();
            
         if(count($_SESSION['product_images'])>0){
            $targetPath = $baseUploadPath.$LastInsertID;   
            $dbPath=$folderPath.$LastInsertID; 
            
            //debug($dbPath);
            if(!(file_exists($baseUploadPath))){
                mkdir($baseUploadPath, 0777, true);
            }
            
            if($response==true){               
                if (file_exists($targetPath)=="") {
                    mkdir($targetPath, 0777, true); 
                    chmod($targetPath, 0777);
                }
				//debug($_SESSION);
				//die;
                
                
                if(isset($_SESSION['product_images']) && count($_SESSION['product_images'])>0){
                    //Moving Files to orginal directory.
                    foreach($_SESSION['product_images'] as $key=>$image_item){
                        
                        foreach($image_item as $k=>$item){
                        
                        $targetFile="";
                        
                        
                        //chmod($targetPath, 0777);
                        $targetFile=$targetPath."/".$item['name'];  
                        $sourceFile=$item['file'];
                        
                  
                                        
                        system("mv $sourceFile $targetPath");
                        
                        
                        $exploding = explode(".",$sourceFile);
                        $ext = end($exploding);
                        
                        $filename = $targetFile;
                        $resizedFile = $dbPath."/"."resized_".$item['name'];
                        $resizedFileName = "resized_".$item['name'];
                        
                        $imgData = $this->resize_image($filename, 570, 625);
                        $this->convert_image($imgData,$resizedFile,$ext);
                        
                        //imagejpeg($imgData, $resizedFile);
                        chmod($resizedFile, 0777);
                        
                        $litingImgFile = $dbPath."/"."listing_".$item['name'];
                        $litingImgFileName = "listing_".$item['name'];
                        
                        $listingImgData = $this->resize_image($filename, 370,315,true);
                        $this->convert_image($listingImgData,$litingImgFile,$ext);
                        
                        //imagejpeg($imgData, $resizedFile);
                        chmod($litingImgFile, 0777);
                        
                    
                        $thumbnailFile = $dbPath."/"."thumbnail_".$item['name'];
                        $thumbnailFilename = "thumbnail_".$item['name'];
                        $thumbimgData = $this->resize_image($resizedFile, 170, 146);
                        $this->convert_image($thumbimgData,$thumbnailFile,$ext);
                        //imagejpeg($thumbimgData, $thumbnailFile);
                        chmod($thumbnailFile, 0777);
                        
                        $productfiles['product_id']=$LastInsertID;
                        $productfiles['status']=1;
                        $productfiles['img_path']=$dbPath;
                        $productfiles['uploaded_img_name']=$item['name'];
                        $productfiles['resized_img_name']=$resizedFileName;
                        $productfiles['listing_img_name']=$litingImgFileName;
                        $productfiles['thumbnail_img_name']= $thumbnailFilename;
                        
                        $this->ProductFile->SaveAll($productfiles);
                        
                    }
                                        
                    }
                    
                 unset( $_SESSION['product_images']);     
               }
            }
            
             
          }
          
         
           $this->redirect(array('controller'=>'manage_products','action'=>'product_list'));
        }

      }
      
      
    function resize_image($file, $w, $h, $crop=false) {
          
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }
    
        //Get file extension
        $exploding = explode(".",$file);
        $ext = end($exploding);
    
        switch($ext){
            case "png":
                $src = imagecreatefrompng($file);
                break;
            case "jpeg":
            case "jpg":
                $src = imagecreatefromjpeg($file);
                break;
            case "gif":
                $src = imagecreatefromgif($file);
                break;
            default:
                $src = imagecreatefromjpeg($file);
                break;
        }
    
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }
    
    
    function convert_image($src_img,$convert_img,$ext){
        
        switch($ext){
            case "png":
                imagepng($src_img,$convert_img);
                break;
            case "jpeg":
            case "jpg":
                imagejpeg($src_img,$convert_img);
                break;
            case "gif":
                imagegif($src_img,$convert_img);
                break;
            default:
                imagejpeg($src_img,$convert_img);
                break;
        }
        
        
    }
    
    public function updateStatus($product_id=null,$status_code=null){
        $this->autoRender=false;  
            if($status_code=='Enable'){
		$status_id=1;
            }else{
		$status_id=2;
            }
	
        $productStatusArr['Product']['id'] =$product_id;
        $productStatusArr['Product']['status'] =$status_id;                
        $this->Product->saveAll($productStatusArr);
            $this->redirect(array('controller'=>'manage_products','action'=>'product_list'));
	}
    
      
}
?>