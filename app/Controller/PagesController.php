<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','Product','Category','SubCategory','ProductFile','Experience','ExperienceFile','ExperienceLanguage','ExperienceType','ExperienceSlot','ExperienceBookingDetail','RoomType','PropertyType','UserReset');
    public function beforeFilter()
    {
        parent::beforeFilter();		
        $this->Auth->allow();
    }
/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact( 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	public function home(){
		
		//debug($this->Session->read('User')); echo "line 81 pages controller";
	}
	public function searchDirection(){
		//debug($_REQUEST);
		//die;
		if($_REQUEST['text_search']!=""){
			if($_POST['search_type']=='Homestay' && $_POST['search_field_value']!=""){
				$id=$_POST['search_field_value'];
				$name=$_POST['text_search'];                
                $result_home=$this->Homestay->find('first',array('conditions'=>array('Homestay.id'=>$id)));
                $seo_unique_url=$result_home['Homestay']['seo_unique_url'];
				//$this->redirect(array('controller'=>'detail','action'=>'index/?searchId='.base64_encode($id).'&name='.$name));
                $this->redirect('/handpicked-stays/'.$seo_unique_url);  
			}
			elseif($_POST['search_type']=='Experience' && $_POST['search_field_value']!=""){
				$id=$_POST['search_field_value'];
				$name=$_POST['text_search'];
                $result_exprnce=$this->Experience->find('first',array('conditions'=>array('Experience.id'=>$id)));
                $seo_unique_url=$result_home['Experience']['seo_unique_url'];                
				//$this->redirect(array('controller'=>'detail','action'=>'experience/?searchId='.base64_encode($id).'&name='.$name));
                $this->redirect('/handpicked-experiences/'.$seo_unique_url);  
			}
			elseif($_POST['search_type']=='ExperienceType' && $_POST['search_field_value']!=""){
				$id=$_POST['search_field_value'];
				$name=$_POST['text_search'];
                //$this->redirect('/handpicked-experiences/'.$name);  
			  $this->redirect(array('controller'=>'listing','action'=>'experience_list/?type_id='.base64_encode($id).'&type='.$name));
			}
			elseif($_POST['search_type']=='PropertyType' && $_POST['search_field_value']!=""){
				$id=$_POST['search_field_value'];
				$name=$_POST['text_search'];
			$this->redirect(array('controller'=>'listing','action'=>'index/?type_id='.base64_encode($id).'&type='.$name));
			}
			elseif($_POST['search_type']=='City' && $_POST['search_field_value']!=""){
				$id=$_POST['search_field_value'];
				$name=$_POST['text_search'];
				$this->redirect(array('controller'=>'listing','action'=>'search_list/?city_id='.base64_encode($id).'&type='.$name));
			}
			else{
				$id=$_POST['search_field_value'];
				$textsearch=$_POST['text_search'];
				$this->redirect(array('controller'=>'listing','action'=>'index/?text_search='.$textsearch));
			}
		}
	}
	public function fetchSearchData(){
		$this->layout=false;//inside call      
		$this->render=false;//inside call    
		
		$resultSearchArray=array();
		$query=$_REQUEST['query'];
		$condition_homestay =array(); $condition_experience=array();$condition_experience_type=array();
		$condition_property_type=array();$condition_city=array();
		if($query!=''){
//			$condition_homestay =array(
//				'OR'=>array("Homestay.name LIKE '%$query%'","Homestay.description LIKE '%$query%'")
//			);	
//			$condition_experience =array(
//				'OR'=>array("Experience.name LIKE '%$query%'","Experience.title LIKE '%$query%'")
//			);	
			$condition_homestay =array("Homestay.name LIKE '%$query%'");
			$condition_experience =array("Experience.name LIKE '%$query%'");
			$condition_experience_type =array("ExperienceType.type LIKE '%$query%'");
			$condition_property_type =array("PropertyType.name LIKE '%$query%'");
			$condition_city =array("City.name LIKE '%$query%'");
		}
		
		$this->Homestay->recursive=0;		
		$homestayList=$this->Homestay->find('all', array(			
            'conditions'=>$condition_homestay,
			'fields' => array('Homestay.id','Homestay.name')
		));
		//$log = $this->Homestay->getDataSource()->getLog(false, false); 
//debug($log);
		$this->Experience->recursive=0;		
		$experienceList=$this->Experience->find('all', array(			
            'conditions'=>$condition_experience,
			'fields' => array('Experience.id','Experience.name')
		));
		$this->ExperienceType->recursive=0;		
		$experienceTypeList=$this->ExperienceType->find('all', array(			
            'conditions'=>$condition_experience_type,
			'fields' => array('ExperienceType.id','ExperienceType.type')
		));
		
		$this->PropertyType->recursive=0;		
		$propertyTypeList=$this->PropertyType->find('all', array(			
            'conditions'=>$condition_property_type,
			'fields' => array('PropertyType.id','PropertyType.name')
		));
		
		$this->City->recursive=0;		
		$cityList=$this->City->find('all', array(			
            'conditions'=>$condition_city,
			'fields' => array('City.id','City.name')
		));
		
		//debug($experienceTypeList);
		$resultSearchArray['query']=$query;
		$suggestions=array();
		if(count($homestayList)>0){
			foreach($homestayList as $listHome){
				$data['value']=$listHome['Homestay']['name'];
				$data['data']=array('category'=>'Homestay','search_field'=>'homestay_id','search_field_value'=>$listHome['Homestay']['id']);
				array_push($suggestions,$data);
			}
			$resultSearchArray['suggestions']=$suggestions;
		}
		if(count($experienceList)>0){
			foreach($experienceList as $listEx){
				$data['value']=$listEx['Experience']['name'];				$data['data']=array('category'=>'Experience','search_field'=>'experience_id','search_field_value'=>$listEx['Experience']['id']);
				array_push($suggestions,$data);
			}
			$resultSearchArray['suggestions']=$suggestions;
		}
		
		if(count($experienceTypeList)>0){
			foreach($experienceTypeList as $listExType){
				$data['value']=$listExType['ExperienceType']['type'];				$data['data']=array('category'=>'ExperienceType','search_field'=>'experience_type_id','search_field_value'=>$listExType['ExperienceType']['id']);
				array_push($suggestions,$data);
			}
			$resultSearchArray['suggestions']=$suggestions;
		}
		if(count($propertyTypeList)>0){
			foreach($propertyTypeList as $listPropType){
				$data['value']=$listPropType['PropertyType']['name'];				$data['data']=array('category'=>'PropertyType','search_field'=>'property_type_id','search_field_value'=>$listPropType['PropertyType']['id']);
				array_push($suggestions,$data);
			}
			$resultSearchArray['suggestions']=$suggestions;
		}
		
		if(count($cityList)>0){
			foreach($cityList as $listCity){
				$data['value']=$listCity['City']['name'];				$data['data']=array('category'=>'City','search_field'=>'city_id','search_field_value'=>$listCity['City']['id']);
				array_push($suggestions,$data);
			}
			$resultSearchArray['suggestions']=$suggestions;
		}
		
		//debug($resultSearchArray);
		echo json_encode($resultSearchArray);
		exit;
	}
    
    public function reset_password(){
        $error_flag="";$user_id="";$email_id="";$token="";$user_reset_id="";
        if($_REQUEST['token']!=""){
            $token=$_REQUEST['token'];
            $userreset_data=$this->UserReset->find('first',array('conditions'=>array('token'=>$token)));            
            if(!empty($userreset_data)){
                $user_id=base64_decode($_REQUEST['user_id']);
                $email_id=base64_decode($_REQUEST['email_id']);
                $token=$_REQUEST['token'];
                $user_reset_id=$userreset_data['UserReset']['id'];
            }else{
                 $error_flag='invalid_token';
            }
            
        }else{
            $error_flag='invalid_token';
        }
        
        $this->set('user_id',$user_id);
        $this->set('token',$token);
        $this->set('email_id',$email_id);
        $this->set('user_reset_id',$user_reset_id); 
        $this->set('error_flag',$error_flag);
        $seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		@$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		@$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		@$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
    }
	
	public function about_coorg(){
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		@$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		@$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		@$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
	}
	public function our_story(){
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		@$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		@$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		@$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
	}
	
	public function why_coorgexpress(){
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		@$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		@$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		@$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
	}
	
	public function responsible_tourism(){
		$seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		@$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		@$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		@$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
	}
}
