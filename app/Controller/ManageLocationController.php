<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageLocationController extends AppController {

	var $layout ="default";
	public $uses = array('User','Amenity','HomestayAmenity','AmenityCategory','Location','City');

	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
public function location_list(){ 
		 $locations= $this->Location->find('all',
      array(
        'fields'=>array('*'),
        'joins'=>array(
            array(
              'table' => 'cities',
              'alias' => 'City',
              'type'  => 'INNER',
              'conditions' => array(
                'Location.city_id = City.id'
              )
            ),
        ),
        'conditions'=>array('Location.status'=>1),

    ));
     //debug( $amenities);
     //die;
     $this->set('locations',$locations);
}

public function add_edit($id=null){
  $city= $this->City->find('list',array('fields'=>array('id','name')));

  if($id!=""){
	    	 $conditions=array('Location.id'=>$id);
	         $this->request->data=$this->Location->find('first', array(				
	            'conditions'=>$conditions,
				'fields' => array('Location.*'),           
				'order' => 'Location.created DESC'
			));
    }
    $this->set('city',$city);
}
   public function delete($id=""){      
        //$this->MasterBedType->delete($id);
   		$data['Location']['id']=$id;
   		$data['Location']['status']=3;
         $response=$this->Location->SaveAll($data);
         $this->redirect(array('controller'=>'manage_location','action'=>'location_list'));
     }
  
 
  public function save(){    

 //debug($_POST['data']); 
  //die;    
        if($_POST!=''){
           
          // debug($_POST['data']); 
        	$_POST['data']['Location']['status']=1;
         $response=$this->Location->SaveAll($_POST['data']['Location']);
            //die;
           $this->redirect(array('controller'=>'manage_location','action'=>'location_list'));
        }

      }  

    
}
?>