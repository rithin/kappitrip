<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageHostsController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry','Host');

	public function beforeFilter()
	{
        parent::beforeFilter();
       // $this->Auth->allow();
	}
    public function host_list(){ 
		 $conditions=array('Host.status'=>1);
         $hosts=$this->Host->find('all', array(
			
            'conditions'=>$conditions,
			'fields' => array('Host.*'),           
			'order' => 'Host.created DESC'
		));
		$log = $this->Host->getDataSource()->getLog(false, false);
//debug($log);
        $this->set('hosts',$hosts);	
      //  debug($propertyTypes);
    }

    public function add_edit($id=null){
    	if($id!=""){
	    	 $conditions=array('Host.id'=>$id);
	         $this->request->data=$this->Host->find('first', array(
				
	            'conditions'=>$conditions,
				'fields' => array('Host.*'),           
				'order' => 'Host.created DESC'
			));
        }

    }
   public function delete($id=""){      
        //$this->PropertyType->delete($id);
   		$data['Host']['id']=$id;
   		 $data['Host']['status']=0;
         $response=$this->Host->SaveAll($data);
         $this->redirect(array('controller'=>'manage_hosts','action'=>'host_list'));
     }
  public function save(){        
        if($_POST!=''){
           
         //  debug($_POST['data']); 
          //  die;
        	$_POST['data']['Host']['status']=1;
            $response=$this->Host->SaveAll($_POST['data']['Host']);
            //die;
           $this->redirect(array('controller'=>'manage_hosts','action'=>'host_list'));
        }

      }  

    
}
?>