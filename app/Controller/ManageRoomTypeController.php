<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageRoomTypeController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry');

	public function beforeFilter()
	{
        parent::beforeFilter();
        //$this->Auth->allow();
	}
    public function room_type_list(){ 
		 $conditions=array('RoomType.status'=>1);
         $roomTypes=$this->RoomType->find('all', array(
			
            'conditions'=>$conditions,
			'fields' => array('RoomType.*'),           
			'order' => 'RoomType.created DESC'
		));
		$log = $this->RoomType->getDataSource()->getLog(false, false);
//debug($log);
        $this->set('roomTypes',$roomTypes);	
      //  debug($propertyTypes);
    }

    public function add_edit($id=null){
    	if($id!=""){
	    	 $conditions=array('RoomType.id'=>$id);
	         $this->request->data=$this->RoomType->find('first', array(
				
	            'conditions'=>$conditions,
				'fields' => array('RoomType.*'),           
				'order' => 'RoomType.created DESC'
			));
        }

    }
   public function delete($id=""){      
        //$this->PropertyType->delete($id);
   		$data['RoomType']['id']=$id;
   		 $data['RoomType']['status']=0;
         $response=$this->RoomType->SaveAll($data);
         $this->redirect(array('controller'=>'manage_room_type','action'=>'room_type_list'));
     }
  public function save(){        
        if($_POST!=''){
           
          // debug($_POST['data']); 
        	$_POST['data']['RoomType']['status']=1;
            $response=$this->RoomType->SaveAll($_POST['data']['RoomType']);
            //die;
           $this->redirect(array('controller'=>'manage_room_type','action'=>'room_type_list'));
        }

      }  

    
}
?>