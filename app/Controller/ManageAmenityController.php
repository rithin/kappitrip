<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageAmenityController extends AppController {

	var $layout ="default";
	public $uses = array('User','Amenity','HomestayAmenity','AmenityCategory');

	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
public function amenity_list(){ 
		 $amenities= $this->Amenity->find('all',
      array(
        'fields'=>array('*'),
        'joins'=>array(
            array(
              'table' => 'amenity_categories',
              'alias' => 'AmenityCategory',
              'type'  => 'INNER',
              'conditions' => array(
                'Amenity.amenity_category_id = AmenityCategory.id'
              )
            ),
        ),
        'conditions'=>array('Amenity.status'=>1),

    ));
     //debug( $amenities);
     //die;
     $this->set('amenities',$amenities);
}

public function add_edit($id=null){
  $amenity_category= $this->AmenityCategory->find('list',array('fields'=>array('id','name')));

  if($id!=""){
	    	 $conditions=array('Amenity.id'=>$id);
	         $this->request->data=$this->Amenity->find('first', array(				
	            'conditions'=>$conditions,
				'fields' => array('Amenity.*'),           
				'order' => 'Amenity.created DESC'
			));
    }
    $this->set('amenity_category',$amenity_category);
}
   public function delete($id=""){      
        //$this->MasterBedType->delete($id);
   		$data['Amenity']['id']=$id;
   		 $data['Amenity']['status']=3;
         $response=$this->Amenity->SaveAll($data);
         $this->redirect(array('controller'=>'manage_amenity','action'=>'amenity_list'));
     }
  
 
  public function save(){    

 // debug($_POST['data']); 
  //die;    
        if($_POST!=''){
           
          // debug($_POST['data']); 
        	$_POST['data']['Amenity']['status']=1;
         $response=$this->Amenity->SaveAll($_POST['data']['Amenity']);
            //die;
           $this->redirect(array('controller'=>'manage_amenity','action'=>'amenity_list'));
        }

      }  

    
}
?>