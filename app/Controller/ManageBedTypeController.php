<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageBedTypeController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry');

	public function beforeFilter()
	{
        parent::beforeFilter();
        //$this->Auth->allow();
	}
    public function bed_type_list(){ 
		 $conditions=array('MasterBedType.status'=>1);
         $bedTypes=$this->MasterBedType->find('all', array(
			
            'conditions'=>$conditions,
			'fields' => array('MasterBedType.*'),           
			'order' => 'MasterBedType.created DESC'
		));
		$log = $this->MasterBedType->getDataSource()->getLog(false, false);
//debug($log);
        $this->set('bedTypes',$bedTypes);	
      //  debug($propertyTypes);
    }

    public function add_edit($id=null){
    	if($id!=""){
	    	 $conditions=array('MasterBedType.id'=>$id);
	         $this->request->data=$this->MasterBedType->find('first', array(
				
	            'conditions'=>$conditions,
				'fields' => array('MasterBedType.*'),           
				'order' => 'MasterBedType.created DESC'
			));
        }

    }
   public function delete($id=""){      
        //$this->MasterBedType->delete($id);
   		$data['MasterBedType']['id']=$id;
   		 $data['MasterBedType']['status']=0;
         $response=$this->MasterBedType->SaveAll($data);
         $this->redirect(array('controller'=>'manage_bed_type','action'=>'bed_type_list'));
     }
  public function save(){        
        if($_POST!=''){
           
          // debug($_POST['data']); 
        	$_POST['data']['MasterBedType']['status']=1;
            $response=$this->MasterBedType->SaveAll($_POST['data']['MasterBedType']);
            //die;
           $this->redirect(array('controller'=>'manage_bed_type','action'=>'bed_type_list'));
        }

      }  

    
}
?>