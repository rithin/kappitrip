<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageExperienceTypeController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry','ExperienceType');

	public function beforeFilter()
	{
        parent::beforeFilter();
        //$this->Auth->allow();
	}
    public function experience_type_list(){ 
		 $conditions=array('ExperienceType.status'=>1);
         $experienceTypes=$this->ExperienceType->find('all', array(
			
            'conditions'=>$conditions,
			'fields' => array('ExperienceType.*'),           
			'order' => 'ExperienceType.created DESC'
		));
		$log = $this->ExperienceType->getDataSource()->getLog(false, false);
//debug($log);
        $this->set('experienceTypes',$experienceTypes);	
      //  debug($propertyTypes);
    }

    public function add_edit($id=null){
    	if($id!=""){
	    	 $conditions=array('ExperienceType.id'=>$id);
	         $this->request->data=$this->ExperienceType->find('first', array(
				
	            'conditions'=>$conditions,
				'fields' => array('ExperienceType.*'),           
				'order' => 'ExperienceType.created DESC'
			));
        }

    }
   public function delete($id=""){      
        //$this->MasterBedType->delete($id);
   		$data['ExperienceType']['id']=$id;
   		 $data['ExperienceType']['status']=0;
         $response=$this->ExperienceType->SaveAll($data);
         $this->redirect(array('controller'=>'manage_experience_type','action'=>'experience_type_list'));
     }
  
 
  public function save(){        
        if($_POST!=''){
           
          // debug($_POST['data']); 
        	$_POST['data']['ExperienceType']['status']=1;
            $response=$this->ExperienceType->SaveAll($_POST['data']['ExperienceType']);
            //die;
           $this->redirect(array('controller'=>'manage_experience_type','action'=>'experience_type_list'));
        }

      }  

    
}
?>