<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageCityController extends AppController {

  var $layout ="default";
  public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry');

  public function beforeFilter()
  {
        parent::beforeFilter();
        $this->Auth->allow();
  }
    public function city_list(){ 
          $conditions=array();
         $city=$this->City->find('all', array(
      
            'conditions'=>$conditions,
      'fields' => array('City.*'),           
      'order' => 'City.created DESC'
    ));
   
        $this->set('city',$city); 
      //  debug($propertyTypes);
    }

    public function add_edit($id=null){
      if($id!=""){
         $conditions=array('City.id'=>$id);
           $this->request->data=$this->City->find('first', array(
        
              'conditions'=>$conditions,
        'fields' => array('City.*'),           
        'order' => 'City.created DESC'
      ));
        }

    }
   public function delete($id=""){      
      $data['City']['name']=$id;
        $response=$this->City->SaveAll($data);
         $this->redirect(array('controller'=>'manage_city','action'=>'city_list'));
     }
  public function save(){        
        if($_POST!=''){
           
          // debug($_POST['data']); 
          $_POST['data']['City'];
            $response=$this->City->SaveAll($_POST['data']['City']);
            //die;
           $this->redirect(array('controller'=>'manage_city','action'=>'city_list'));
        }

      }  

    
}
?>