<?php
// app/Controller/Component/ExistingComponent.php
App::uses('Component', 'Controller');
class GeneralComponent extends Component{
public $components = array('Cookie');
    public function generateInvoiceNo() {
	   	//invoice no generation..	
		$invoiceNo="CE".date('Y-m-d').rand(0000,9999);   
       	while($this->invoiceNoCheck($invoiceNo)!=""){
			return $invoiceNo;
		}
		return rand('10111121','32432434324');
		
    }
	
	public function invoiceNoCheck($invoiceNo=null){		
		$data = ClassRegistry::init('BookingPaymentResponse')->query("SELECT * FROM `booking_payment_responses` WHERE `invoice_no`='$invoiceNo'");
		return $data;

	}
	
	public function handleCookie($action=null,$key=null,$value=null){
		$this->Cookie->name =null;
		$this->Cookie->time =2592000;  // or '30 days'
		$this->Cookie->path = '/';
		$this->Cookie->domain =  $_SERVER['HTTP_HOST'];
		//$this->Cookie->secure = true;  // i.e. only sent if using secure HTTPS
		$this->Cookie->key = md5(uniqid(rand(), true));
		//$this->Cookie->httpOnly = true;
		$this->Cookie->type('aes');
		
		if($action=='write'){
			$this->Cookie->write($key,$value,false,2592000);
		}
		return true;
	}
	
	
	public function getBrowser() 
	{ 
			$u_agent = $_SERVER['HTTP_USER_AGENT']; 
			$bname = 'Unknown';
			$platform = 'Unknown';
			$version= "";

			//First get the platform?
			if (preg_match('/linux/i', $u_agent)) {
				$platform = 'linux';
			}
			elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
				$platform = 'mac';
			}
			elseif (preg_match('/windows|win32/i', $u_agent)) {
				$platform = 'windows';
			}

			// Next get the name of the useragent yes seperately and for good reason
			if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
			{ 
				$bname = 'Explorer'; 
				$ub = "MSIE"; 
			} 
			elseif(preg_match('/Firefox/i',$u_agent)) 
			{ 
				$bname = 'Firefox'; 
				$ub = "Firefox"; 
			}
			elseif(preg_match('/OPR/i',$u_agent)) 
			{ 
				$bname = 'Opera'; 
				$ub = "Opera"; 
			} 
			elseif(preg_match('/Chrome/i',$u_agent)) 
			{ 
				$bname = 'Chrome'; 
				$ub = "Chrome"; 
			} 
			elseif(preg_match('/Safari/i',$u_agent)) 
			{ 
				$bname = 'Apple Safari'; 
				$ub = "Safari"; 
			} 
			elseif(preg_match('/Netscape/i',$u_agent)) 
			{ 
				$bname = 'Netscape'; 
				$ub = "Netscape"; 
			} 

			// finally get the correct version number
			$known = array('Version', $ub, 'other');
			$pattern = '#(?<browser>' . join('|', $known) .
			')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
			if (!preg_match_all($pattern, $u_agent, $matches)) {
				// we have no matching number just continue
			}

			// see how many we have
			$i = count($matches['browser']);
			if ($i != 1) {
				//we will have two since we are not using 'other' argument yet
				//see if version is before or after the name
				if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
					$version= $matches['version'][0];
				}
				else {
					$version= $matches['version'][1];
				}
			}
			else {
				$version= $matches['version'][0];
			}

			// check if we have a number
			if ($version==null || $version=="") {$version="?";}

			return array(
				'userAgent' => $u_agent,
				'name'      => $bname,
				'version'   => $version,
				'platform'  => $platform,
				'pattern'    => $pattern
			);
		} 

	
}
?>