<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageExperiencesController extends AppController {

	var $layout ="default";
	public $uses = array('User','Room','RoomFile','Language','ExperienceType','Experience','ExperienceLanguage','ExperienceFile', 'ExperienceSlot','City','TrailType','Location','ExperienceBlocking','AttendeeType','ExperienceAttendee','Host');

	public function beforeFilter()
	{
        parent::beforeFilter();
       // $this->Auth->allow();
	}
    public function experience_list(){
		$joins = array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type' => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type' => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),
        );
		$fields = array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path','ExperienceType.type');
       	$conditions=array('Experience.experience_type!=5');
        if(@$this->request->data['Experience']['experience_name']!=""){
            @$experience_name=$this->request->data['Experience']['experience_name'];
            @array_push($conditions,array("Experience.name LIKE '%$experience_name%' "));
        }
        
        $role_id = $this->Session->read('Auth.User.role_id');
        $user_id = $this->Session->read('Auth.User.id');
        
        
        
        if($role_id==3){
            @array_push($conditions,array("Experience.host_id ='$user_id' "));
        }
        
		$this->paginate = array(
			'joins' =>$joins,
			'conditions' => $conditions,
			'limit' => 10,
			'order' => array('Experience.created' => 'DESC'),
			'fields' => $fields,
                        'group' => '`Experience`.`id`',
		);
		$experienceList = $this->paginate('Experience');        
        $this->set('experienceList',$experienceList);	
    }
	public function event_list(){
		$joins = array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type' => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type' => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),
        );
		$fields = array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path','ExperienceType.type');
       	$conditions=array('Experience.experience_type'=>5);
        if(@$this->request->data['Experience']['experience_name']!=""){
            @$experience_name=$this->request->data['Experience']['experience_name'];
            @array_push($conditions,array("Experience.name LIKE '%$experience_name%' "));
        }
		$this->paginate = array(
			'joins' =>$joins,
			'conditions' => $conditions,
			'limit' => 10,
			'order' => array('Experience.created' => 'DESC'),
			'fields' => $fields,
            'group' => '`Experience`.`id`',
		);
		$experienceList = $this->paginate('Experience');        
        $this->set('experienceList',$experienceList);	
    }
    public function delete($id="",$type){      
        $this->Experience->delete($id);
        $this->ExperienceFile->deleteAll(array('ExperienceFile.room_id' => $id), false);
		if($type=='event')
			$this->redirect(array('controller'=>'manage_rooms','action'=>'event_list'));
		else
        	$this->redirect(array('controller'=>'manage_rooms','action'=>'experience_list'));
     }
    public function deleteFile($id="",$experience_id='',$type){
        $type= $_REQUEST['type'];
         $data= $this->ExperienceFile->find('first',array('conditions'=>array('ExperienceFile.id'=>$id)));  
        //debug($data);
		if(count($data)>0){
			 $targetFile = WWW_ROOT.$data['ExperienceFile']['file_path'].'/'.$data['ExperienceFile']['file_name'];
			 @unlink($targetFile);
			 $this->ExperienceFile->delete($id);
		 }
		 //$experience_id=$data['ExperienceFile']['experience_id'];
		 $action_url='add/'.$experience_id.'/?type='.$type;
         $this->redirect(array('controller'=>'manage_experiences','action'=>$action_url));
    }
  
    
  public function add($id='',$experience_slot_detail_id=null,$experience_blocking_id=null){ 
	  	$type=$_REQUEST['type'];
        $experienceFiles=array(); $ExperienceSlotsArr="";
        $ExperienceSlotString="";        
        $array_cities=$this->City->find('list');
        $this->set('array_cities',$array_cities);
				   
        $array_languages=$this->Language->find('list');
        $this->set('array_languages',$array_languages);	
		
		$array_trail_types=$this->TrailType->find('list');
        $this->set('array_trail_types',$array_trail_types);	
        
        $who_can_attend=$this->AttendeeType->find('list',array('fields'=>array('id','attendee')));
        $this->set('who_can_attend',$who_can_attend);	
		
		$locations_arr=$this->Location->find('list',array('conditions'=>array('Location.in_search'=>1),'fields'=>array('id','name')));
		$this->set('locations_arr',$locations_arr);	
		
        $array_experience_types=$this->ExperienceType->find('list',array('fields' => array('ExperienceType.id', 'ExperienceType.type'),));
        $this->set('array_experience_types',$array_experience_types);      
      
        $hosts_arr = array();
        $hosts_arr_list=$this->User->find('all',array('conditions'=>array('User.role_id'=>3), 'fields'=>array('id','first_name','last_name')));
	if(!empty($hosts_arr_list)){
            foreach($hosts_arr_list as $host_data){
                        $hosts_arr[$host_data['User']['id']] = $host_data['User']['first_name'].' '.$host_data['User']['last_name'];
            }
        }
        $this->set('hosts_arr',$hosts_arr);	      
      
        if($id!="")	{	
            $this->request->data= $this->Experience->find('first', array(
			'joins' => array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type' => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type' => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type' => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_attendees',
					'alias' => 'ExperienceAttendee',
					'type' => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceAttendee.experience_id'
					)
				),
			),
            'conditions' => array('Experience.id'=>$id),
			'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path','ExperienceType.type', 'ExperienceLanguage.language_id'),	'order' => 'Experience.created DESC'));
           
            $experienceLanguages = $this->ExperienceLanguage->find('all',array('conditions'=>array ('ExperienceLanguage.experience_id'=>$id)));             
            $language=array();
            foreach($experienceLanguages as $item){
                array_push($language,$item['ExperienceLanguage']['language_id']);
            }
            $this->request->data['ExperienceLanguage']['language_id']=$language;   
            
            $experienceAttendees = $this->ExperienceAttendee->find('all',array('conditions'=>array ('ExperienceAttendee.experience_id'=>$id)));             
            $attendeeTypes=array();
            foreach($experienceAttendees as $item){
                array_push($attendeeTypes,$item['ExperienceAttendee']['attendee_type_id']);
            }
            $this->request->data['ExperienceAttendee']['attendee_type_id']=$attendeeTypes; 
            
            
            $experienceFiles=$this->ExperienceFile->find('all',array('conditions'=>array('ExperienceFile.experience_id'=>$id))); 
             //debug($experienceFiles);
                        
           $ExperienceSlotArray=$this->ExperienceSlot->find('list', array('conditions'=>array( 'ExperienceSlot.experience_id'=>$id, 
                        array('ExperienceSlot.start_date BETWEEN ? AND ?' => array(date('Y-m-01'), date('Y-m-t')) ), 
                        'ExperienceSlot.status'=>1 ),'fields'=>array('start_date' ,'end_date','ExperienceSlot.no_of_slots'), 'group'=>'start_date')); 
            // $log = $this->Homestay->getDataSource()->getLog(false, false);          
            $ExperienceSlotString=json_encode($ExperienceSlotArray);  
			
			$ExperienceSlotsArr=$this->findAvailability($id);
			if($experience_slot_detail_id!=null && $experience_slot_detail_id!=0){
				$ExperienceSlotDtl=$this->ExperienceSlot->find('all', array('conditions'=> array('ExperienceSlot.id'=>$experience_slot_detail_id), 'fields'=>array('ExperienceSlot.*')));
				$this->request->data['ExperienceSlot']=$ExperienceSlotDtl[0]['ExperienceSlot'];
			}
			//debug($ExperienceSlotDtl);
			//debug($this->request->data);
			if($experience_blocking_id!=null && $experience_blocking_id!=0){
				$ExperienceBlockingDtl=$this->ExperienceBlocking->find('all', array('conditions'=> array('ExperienceBlocking.id'=>$experience_blocking_id), 'fields'=>array('ExperienceBlocking.*')));
				$this->request->data['ExperienceBlocking']=$ExperienceBlockingDtl[0]['ExperienceBlocking'];
			}
			
        }
		$experienceBlockings=$this->ExperienceBlocking->find('all',array('conditions'=> array('ExperienceBlocking.experience_id' =>$id )));
		
		$this->set('experienceBlockings',$experienceBlockings);
		$this->set('ExperienceSlotsArr',$ExperienceSlotsArr);
        $this->set('experienceFiles',$experienceFiles);		
        $this->set('ExperienceSlotString',$ExperienceSlotString);
    }
    public function fileUpload(){
        @session_start();
        $this->autoRender=false;        
        $ds          = DIRECTORY_SEPARATOR;  
        $storeFolder = 'uploads/temp_files';   
       $request=1;
		if(isset($_POST['request'])){ 
			  $request = $_POST['request'];
		}
        if ((!empty($_FILES))&& $request==1) {
			
			for($i=0; $i< count($_FILES['file']['name']); $i++){             
				$tempFile = $_FILES['file']['tmp_name'][$i];         
				$targetPath = WWW_ROOT.$storeFolder . $ds;  
				$fileName=""; $time=time();
				if($_FILES['file']['type'][$i]=="image/jpeg"){
					 $targetFile =  $targetPath. $time.".jpg"; 
					 $fileName=$time.".jpg";
				}else if($_FILES['file']['type'][$i]=="image/png"){
					 $targetFile =  $targetPath. $time.".png";  
					 $fileName=$time.".png";
				}else if($_FILES['file']['type'][$i]=="image/gif"){
					 $targetFile =  $targetPath. $time.".gif";  
					 $fileName=$time.".gif";
				}else if($_FILES['file']['type'][$i]=="image/webp"){
					 $targetFile =  $targetPath. $time.".webp";  
					 $fileName=$time.".webp";
				}else{
					$targetFile=$targetPath. $_FILES['file']['name'][$i]; 
					$fileName= $time.$_FILES['file']['name'][$i];
					//echo "Error. Invalid file type..!";
				}
				 $_FILES["file"]["name"][$i]=$fileName;
				move_uploaded_file($tempFile,$targetFile);            
				chmod($targetFile, 0777);
				$arrayImg['file']=$targetFile;
				$arrayImg['name']=$fileName;
				$_SESSION['room_images'][$time]=$arrayImg;
			   // array_push($_SESSION['room_images'],$arrayImg);
			}
        }else if($request==2){
			$nameArr=explode(".",$_POST['name']);
			$key=$nameArr[0];
			$targetDeletedFile= $_SESSION['room_images'][$key]['file'];
			unlink(@$targetDeletedFile);
			unset($_SESSION['room_images'][$key]);
			//debug($_SESSION);
		}
      }
    public function save(){  
		$type=$_REQUEST['type'];
        if($_POST!=''){
            $baseUploadPath = WWW_ROOT."uploads/experience_pics/";			
            $_POST['data']['Experience']['created_by'] = $this->Session->read('Auth.User.id');            
            $response=$this->Experience->SaveAll($_POST['data']['Experience']);				
            $folderPath="uploads/experience_pics/";
			
            if($_POST['data']['Experience']['id']!="")
                $LastInsertID=$_POST['data']['Experience']['id'];
           else
               $LastInsertID=$this->Experience->getLastInsertID();			
		
            $this->ExperienceLanguage->deleteAll(array('ExperienceLanguage.experience_id' => $LastInsertID), false);
            $this->ExperienceAttendee->deleteAll(array('ExperienceAttendee.experience_id' => $LastInsertID), false);
           // debug($_POST['data']);
            if(is_array($_POST['data']['ExperienceLanguage']['language_id']) && count($_POST['data']['ExperienceLanguage']['language_id'] )>0){
                foreach($_POST['data']['ExperienceLanguage']['language_id'] as $key=>$item){
                    $expLang[$key]['experience_id']=$LastInsertID;
                    $expLang[$key]['language_id']=$item;
                }
                $this->ExperienceLanguage->SaveAll($expLang);
            }
            if(is_array($_POST['data']['ExperienceAttendee']['attendee_type_id']) && count($_POST['data']['ExperienceAttendee']['attendee_type_id'] )>0){
                foreach($_POST['data']['ExperienceAttendee']['attendee_type_id'] as $key=>$item){
                    $expAttendees[$key]['experience_id']=$LastInsertID;
                    $expAttendees[$key]['attendee_type_id']=$item;
                }
                $this->ExperienceAttendee->SaveAll($expAttendees);
            }
            //debug($_SESSION['room_images']);
			$targetPath = $baseUploadPath.$LastInsertID;   
            $dbPath=$folderPath.$LastInsertID;   
           
            if($response==true){               
                if (file_exists($targetPath)=="") {
                   @mkdir($targetPath, 0777, true); 
                    chmod($targetPath, 0777);
                }
			  if(isset($_SESSION['room_images']) && count($_SESSION['room_images'])>0){
                //debug($_SESSION['room_images']);
                //Moving Files to orginal directory.
                foreach($_SESSION['room_images'] as $key=>$item){
                    $targetFile="";
                    $roomfiles['experience_id']=$LastInsertID;
                    $roomfiles['status']=1;
                    $roomfiles['file_path']=$dbPath;
                    $roomfiles['file_name']=$item['name'];
                    $this->ExperienceFile->SaveAll($roomfiles);
                    chmod($targetPath, 0777);
                    $targetFile=$targetPath."/".$item['name'];  
                    $sourceFile=$item['file'];
                    //debug($roomfiles);                  
                    system("mv $sourceFile $targetPath");
                     
                   // move_uploaded_file($item['file'],$targetFile);                     
                }
             }
              unset( $_SESSION['room_images']); 
          }
            $action_url='add/'.$LastInsertID.'/availability/?type='.$type;
            $this->redirect(array('controller'=>'manage_experiences','action'=>$action_url));
       }

   }
    public function updateStatus($experience_id=null,$status_code=null){		
		$this->autoRender=false;  
		$type=$_REQUEST['type'];
		if($status_code=='Enable'){
			$status_id=1;
		}else{
			$status_id=2;
		}
		$experienceStatusArr['Experience']['id'] =$experience_id;
        $experienceStatusArr['Experience']['status'] =$status_id;                
        $this->Experience->saveAll($experienceStatusArr);
		if($type=='event')
			$this->redirect(array('controller'=>'manage_experiences','action'=>'event_list'));
		else				
			$this->redirect(array('controller'=>'manage_experiences','action'=>'experience_list'));
	}
    public function findAvailability($experience_id){
        $this->render=false;  
        $data=array();
		 $this->Experience->unbindModel(array('hasMany' => array('ExperienceSlot')));
         $this->ExperienceFile->unbindModel(array('belongsTo' => array('Experience')));
         $this->ExperienceLanguage->unbindModel(array('belongsTo' => array('Experience')));
         $this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));        
         unset($this->ExperienceSlot->virtualFields['no_of_slots']);
		
        $ExperienceSlotDtl=$this->ExperienceSlot->find('all', array('conditions'=> array('ExperienceSlot.experience_id'=>$experience_id,'ExperienceSlot.status'=>1), 'fields'=>array('ExperienceSlot.*')));
   //$log = $this->ExperienceSlot->getDataSource()->getLog(false, false);  
	//	debug($log);
	//	debug($ExperienceSlotDtl);
		return $ExperienceSlotDtl;
       
    }
    
    public function saveAvailability(){
		$type=$_REQUEST['type'];
		//  $this->autoRender=false;   	
        $experince_id=$_POST['data']['ExperienceSlot']['experience_id'];
		$experince_slot_id=$_POST['data']['ExperienceSlot']['id'];
        $this->ExperienceSlot->saveAll($_POST['data']);
        $action_url='add/'.$experince_id.'/availability/?type='.$type;
        $this->redirect(array('controller'=>'manage_experiences','action'=>$action_url));
        
    }
	
	public function saveNonAvailability(){
		$type=$_REQUEST['type'];
        $experince_id=$_POST['data']['ExperienceBlocking']['experience_id'];
		$experince_blocking_id=$_POST['data']['ExperienceBlocking']['id'];		
        $this->ExperienceBlocking->saveAll($_POST['data']);
        $action_url='add/'.$experince_id.'/availability/?type='.$type;
        $this->redirect(array('controller'=>'manage_experiences','action'=>$action_url));
        
    }
    
    public function deleteSlot($experience_id=null,$experience_slot_id=null){
		$type=$_REQUEST['type'];
        $dataSlot['ExperienceSlot']['id']=$experience_slot_id;
        $dataSlot['ExperienceSlot']['status']=2;
        $this->ExperienceSlot->saveALl($dataSlot);
        $action_url='add/'.$experience_id.'/availability?type='.$type;
        $this->redirect(array('controller'=>'manage_experiences','action'=>$action_url));
    }
    public function deleteBlocking($experience_id=null,$experience_blocking_id=null){
		$type=$_REQUEST['type'];
        $this->ExperienceBlocking->deleteAll(array('ExperienceBlocking.id' => $experience_blocking_id), false);
        $action_url='add/'.$experience_id.'/availability?type='.$type;
        $this->redirect(array('controller'=>'manage_experiences','action'=>$action_url));
    }
    
	
	public function splitDates($start_date, $end_date) {
		 date_default_timezone_set('UTC');
		 $aDays[] = $start_date; 		
		 while (strtotime($start_date) < strtotime($end_date)) {  
		    $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
			$aDays[] = $start_date;  
		 }		 
		 return $aDays;         
    }
    public function findLocation(){
		$this->autoRender=false;// ajax method for finding the rooms of property.       
		$city_id=$_REQUEST['city_id'];
		$locationsDtls=$this->Location->query("SELECT id,name from locations as Location  WHERE  `Location`.`city_id`=$city_id  ");
		//debug($freeRoomDetail);
		$locations=array();
		foreach($locationsDtls as $dtl){
			$locations[$dtl['Location']['id']]=$dtl['Location']['name'];
		}
		echo json_encode($locations);
		
	}
    
}
?>