<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageCategoryController extends AppController {

  var $layout ="default";
  public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry','Category');

  public function beforeFilter()
  {
        parent::beforeFilter();
        $this->Auth->allow();
  }
    public function category_list(){ 
          $conditions=array();
         $category=$this->Category->find('all', array(
      
            'conditions'=>$conditions,
      'fields' => array('Category.*'),           
      'order' => 'Category.created DESC'
    ));
   
        $this->set('category',$category); 
      //  debug($propertyTypes);
    }

    public function add_edit($id=null){
      if($id!=""){
         $conditions=array('Category.id'=>$id);
           $this->request->data=$this->Category->find('first', array(
        
              'conditions'=>$conditions,
        'fields' => array('Category.*'),           
        'order' => 'Category.created DESC'
      ));
        }

    }
   public function delete($id=""){      
      $data['Category']['id']=$id;
      $data['Category']['status']=3;
        $response=$this->Category->SaveAll($data);
         $this->redirect(array('controller'=>'manage_category','action'=>'category_list'));
     }
  public function save(){        
        if($_POST!=''){
           
          // debug($_POST['data']); 
          $_POST['data']['Category']['status']=1;
            $response=$this->Category->SaveAll($_POST['data']['Category']);
            //die;
           $this->redirect(array('controller'=>'manage_category','action'=>'category_list'));
        }

      }  

    
}
?>