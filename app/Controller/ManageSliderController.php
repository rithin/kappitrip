<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageSliderController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail', 'OccupancyDetail','Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry','Slider');

	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
    public function slider_list(){ 
		
    }
    
    public function ajax_slider_list(){ 
         
        $conditions=array('Slider.status'=>1);
        $sliders=$this->Slider->find('all', array(
			
            'conditions'=>$conditions,
			'fields' => array('Slider.*'),           
			'order' => 'Slider.sort_order ASC'
        ));
        
        $list_view_array = array();
                
                foreach($sliders as $key=>$slider_data){
                    
                    
                    
                    $list_view_array['data'][$key]['title'] = $slider_data['Slider']['title'];
                    $list_view_array['data'][$key]['description'] = $slider_data['Slider']['description'];
                    $list_view_array['data'][$key]['slider_image'] = Configure::read('app_root_path').$slider_data['Slider']['file_path']."/".$slider_data['Slider']['file_name'];
                    $list_view_array['data'][$key]['sort_order'] = $slider_data['Slider']['sort_order'];
                    $list_view_array['data'][$key]['actions'] = "<a href='".Configure::read('app_root_path')."manage_slider/add_edit/".$slider_data['Slider']['id']."'>Edit</a>"
                            . "&nbsp; &nbsp; <a href='".Configure::read('app_root_path')."manage_slider/delete/".$slider_data['Slider']['id']."'>Delete</a>";                
                    
                    }
                
                echo json_encode($list_view_array);
                
                exit; 
		

    }

    public function add_edit($id=null){
    	if($id!=""){
	    	 $conditions=array('Slider.id'=>$id);
	         $this->request->data=$this->Slider->find('first', array(
				
	            'conditions'=>$conditions,
				'fields' => array('Slider.*'),           
				'order' => 'Slider.created DESC'
			));
        }

    }
   public function delete($id=""){      
        //$this->PropertyType->delete($id);
   		$data['Slider']['id']=$id;
   		 $data['Slider']['status']=0;
         $response=$this->Slider->SaveAll($data);
         $this->redirect(array('controller'=>'manage_slider','action'=>'slider_list'));
     }
  public function save(){        
        if($_POST!=''){
           
          
        	$_POST['data']['Slider']['status']=1;
			
		if($_FILES['data']['name']["Slider"]["slide_image"]!=""){
			$target_dir = "uploads/slider_pics/";
			if (!file_exists(WWW_ROOT.$target_dir)) {
				mkdir(WWW_ROOT.$target_dir);
					chmod(WWW_ROOT.$target_dir, 0777);
			}
			
			$file_name= time().$_FILES['data']['name']["Slider"]["slide_image"];
			$target_file = $target_dir . basename($file_name);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])) {
				$check = getimagesize($_FILES['data']['tmp_name']["Slider"]["slide_image"]);
				if($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
				} else {
					echo "File is not an image.";
					$uploadOk = 0;
				}
			}
			// Check if file already exists
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES['data']['size']["Slider"]["slide_image"]> 500000000) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				echo "Sorry, your file was not uploaded.";
				die;
			// if everything is ok, try to upload file
			} else {
				if (move_uploaded_file($_FILES['data']['tmp_name']["Slider"]["slide_image"], $target_file)) {
					echo "The file ". basename( $file_name). " has been uploaded.";
				} else {
					echo "Sorry, there was an error uploading your file.";
				}
			}
			chmod(WWW_ROOT.$target_file, 0777);
			$_POST['data']['Slider']['file_path']=$target_dir;
			$_POST['data']['Slider']['file_name']=$file_name;
		}
			
           $response=$this->Slider->SaveAll($_POST['data']['Slider']);
            //die;
           $this->redirect(array('controller'=>'manage_slider','action'=>'slider_list'));
       }

   }  

    
}
?>