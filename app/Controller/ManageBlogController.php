<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageBlogController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry','BlogPost');
	//public $helpers = array('Froala.Froala');
	public function beforeFilter()
	{
        parent::beforeFilter();
        //$this->Auth->allow();
	}
    public function post_list(){ 
		 $conditions=array('BlogPost.status'=>1);
         $blog_post_lists=$this->BlogPost->find('all', array(
			
            'conditions'=>$conditions,
			'fields' => array('BlogPost.*'),           
			'order' => 'BlogPost.created DESC'
		));
        $this->set('blog_post_lists',$blog_post_lists);	
      //  debug($content);
    }

    public function add_edit($id=null){
	//	$this->helpers = array('Froala.Froala');
		$this->helpers = array('TinyMCE.TinyMCE');
    	if($id!=""){
	    	 $conditions=array('BlogPost.id'=>$id);
	         $this->request->data=$this->BlogPost->find('first', array(
				
	            'conditions'=>$conditions,
				'fields' => array('BlogPost.*'),           
				'order' => 'BlogPost.created DESC'
			));
			//
			
				$this->request->data['BlogPost']['txtEditor']=$this->request->data['BlogPost']['content'];
			//debug($this->request->data);
        }

    }
   public function delete($id=""){      
        //$this->Content->delete($id);
   		$data['BlogPost']['id']=$id;
   		 $data['BlogPost']['status']=0;
         $response=$this->BlogPost->SaveAll($data);
         $this->redirect(array('controller'=>'manage_blog','action'=>'post_list'));
     }
  public function save(){        
        if($_POST!=''){
           //   debug($_REQUEST);
       // debug($_FILES);die; 
        	$_POST['data']['BlogPost']['status']=1;	
			$_POST['data']['BlogPost']['posted_date']=date('Y-m-d');	
			$_POST['data']['BlogPost']['content']=$_POST['data']['BlogPost']['txtEditor'];
            $response=$this->BlogPost->SaveAll($_POST['data']['BlogPost']);
			
			if($_POST['data']['BlogPost']['id']!=""){
				$blogPostID = $_POST['data']['BlogPost']['id'];
			}else{
				$blogPostID = $this->BlogPost->getLastInsertID(); 
			}
			
			if($_FILES['data']['name']['BlogPost']['file']!=""){
				$ds          = DIRECTORY_SEPARATOR;  
				$storeFolder = 'uploads/blog_pics/'; 
				$tempFile =$_FILES['data']['tmp_name']['BlogPost']['file'];         
				$targetPath = WWW_ROOT.$storeFolder.$blogPostID.$ds; 
				$baseUploadPath = WWW_ROOT."uploads/blog_pics/".$blogPostID;

				 if (file_exists($baseUploadPath)=="") {
						mkdir($baseUploadPath, 0777, true); 
						chmod($baseUploadPath, 0777);
				  }
				$fileName=""; $time=time();
					if($_FILES['data']['type']['BlogPost']['file']=="image/jpeg"){
						 $targetFile =  $targetPath. $time.".jpg"; 
						 $fileName=$time.".jpg";
					}else if($_FILES['data']['type']['BlogPost']['file']=="image/png"){
						 $targetFile =  $targetPath. $time.".png";  
						 $fileName=$time.".png";
					}else if($_FILES['data']['type']['BlogPost']['file']=="image/gif"){
						 $targetFile =  $targetPath. $time.".gif";  
						 $fileName=$time.".gif";
					}else if($_FILES['data']['type']['BlogPost']['file']=="image/webp"){
						 $targetFile =  $targetPath. $time.".webp";  
						 $fileName=$time.".webp";
					}else{
						$targetFile=$targetPath.$_FILES['data']['type']['BlogPost']['file']; 
						$fileName= $time.$_FILES['data']['name']['BlogPost']['file'];
						//echo "Error. Invalid file type..!";
					}
				move_uploaded_file($tempFile,$targetFile); 
				chmod($targetFile, 0777);
				$dataUpdate['BlogPost']['id']=$blogPostID;
				$dataUpdate['BlogPost']['file_name']=$fileName;
				$dataUpdate['BlogPost']['file_path']=$storeFolder.$blogPostID;
				$this->BlogPost->SaveAll($dataUpdate);
			}
           // die;
           $this->redirect(array('controller'=>'manage_blog','action'=>'post_list'));
        }

      }  

    
}
?>