<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ProductsController extends AppController {

    var $layout ="product_layout";
    public $components = array('Paginator','PerPage');
    public $uses = array('User','Product','ProductFile','Category','SubCategory','City','CoffeeType');
    
    public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
		
	}
    
    public function listing(){  
        $array_categories=$this->Category->find('list',array('fields'=>array('id','category_name')));
        $this->set('array_categories',$array_categories);	
        $this->set('array_sub_categories',array());	
        $seoMetaDtls=$this->Product->query("SELECT meta_title,meta_keyword,meta_description FROM `products` WHERE rand() limit 0,1");
        //debug($seoMetaDtls);
        $meta_title = '';
        $meta_keyword = '';
        $meta_description = '';
        
        if(!empty($seoMetaDtls)){
            $meta_title = $seoMetaDtls[0]['products']['meta_title'];
            $meta_keyword = $seoMetaDtls[0]['products']['meta_keyword'];
            $meta_description = $seoMetaDtls[0]['products']['meta_description'];
        }
        
        $array_product_location = array(); 
        $array_product_location=$this->City->find('list',array('fields'=>array('id','name')));        
        $this->set('array_product_location',$array_product_location);
        
        $array_coffee_type = array();
        $array_coffee_type=$this->CoffeeType->find('list',array('fields'=>array('id','coffee_type_name')));        
        $this->set('array_coffee_type',$array_coffee_type);
        

        
        $this->set('meta_title',$meta_title);
	$this->set('meta_keyword',$meta_keyword);
	$this->set('meta_description',$meta_description);        
    }
   public function findSubCategory(){        
       $this->autoRender=false;
      // debug($_REQUEST);
       $category_id=$_REQUEST['category_id'];
       $array_sub_categories=$this->SubCategory->find('list',array('conditions'=>array('category_id'=>$category_id), 'fields'=>array('id','sub_category_name')));
       echo json_encode($array_sub_categories);
       // $this->set('array_sub_categories',$array_sub_categories);	
       exit;
    }
    
    public function ajaxProductFilter(){        
       $this->layout=false;
       $conditions =array(); $join_condition='';  
       $page = 1;
		if(!empty($_GET["page"])) {
			$page = $_GET["page"];
		}
		
		$start = ($page-1)*1;
		if($start < 0) $start = 0;
                
                
        
        //debug($_REQUEST);

	$paginationlink = "products/ajaxProductFilter/?page=";	
	$pagination_setting = 'all-links';
        
        if(isset($_REQUEST) && isset($_REQUEST['category_id']) && $_REQUEST['category_id']!=''){   
          $category_id=$_REQUEST['category_id'];
          $conditions_data =array("Product.category_id"=> $category_id);
          array_push($conditions,$conditions_data);
        }
        
        if(isset($_REQUEST) && isset($_REQUEST['location']) && $_REQUEST['location']!=''){   
          $product_location=$_REQUEST['location'];
          $conditions_data=array("Product.product_location"=> $product_location);
          array_push($conditions,$conditions_data);
        }
        if(isset($_REQUEST) && isset($_REQUEST['coffee_type']) && $_REQUEST['coffee_type']!=''){   
          $coffee_type=$_REQUEST['coffee_type'];
          $conditions_data=array("Product.coffee_type"=> $coffee_type);
          array_push($conditions,$conditions_data);
        }
      
        if(isset($_REQUEST) && isset($_REQUEST['search_text']) && $_REQUEST['search_text']!='undefined'){
            $searchTerm=$_REQUEST['search_text'];
            $conditions_data=array("(Product.product_name LIKE '%$searchTerm%' OR Product.product_desc LIKE '%$searchTerm%')");
            array_push($conditions,$conditions_data);
        }
	if(@$_REQUEST['selectedPriceRange']>10){
            $base_price=$_REQUEST['selectedPriceRange'];
            array_push($conditions,array("Product.unit_price < $base_price"));
	}
        
        array_push($conditions,array('Product.status'=>1));
        //debug($conditions);
        $joins=array(
                  array(
                    'table' => 'product_files',
                    'alias' => 'ProductFile',
                    'type' => 'LEFT',
                    'conditions' => array(
                            'Product.id = ProductFile.product_id'
                          )
                    ),    
		  );
         $productList=$this->Product->find('all', array(
			'joins' =>$joins,
            'conditions' => $conditions,
            'limit' => 10,
			'page'=>$page,
			'offset'=>$start,
			'fields' => array('Product.*','ProductFile.resized_img_name','ProductFile.img_path'),
            'group' => '`Product`.`id`',
			'order' => 'Product.created DESC'
		));
        
        $productListTotal=$this->Product->find('all',array(
			'joins' =>$joins,
			'conditions' => $conditions,
            'fields' => array('Product.*','ProductFile.resized_img_name','ProductFile.img_path'),
            'group' => '`Product`.`id`',
			'order' => 'Product.created DESC'
		));
		//debug($_GET);
		if(empty($_GET["rowcount"])) {
			$_GET["rowcount"] = count($productListTotal);
		}

		if($pagination_setting == "prev-next") {
			$perpageresult = $this->perPage->getPrevNext($_GET["rowcount"], $paginationlink,$pagination_setting);	
		} else {
			$perpageresult = $this->getAllPageLinks($_GET["rowcount"], $paginationlink,$pagination_setting);	
		}
		//echo $perpageresult;
                 $this->set('productList',$productList);	
		 $this->set('perpageresult',$perpageresult);	
        
    }
    

    public function productDetail($unique_url=null){
        
        
        
        
        $conditions=array();
        
        if($unique_url !=""){
            
            
            $this->Product->recursive=-1;
            $productDetail=$this->Product->find('first',array('conditions'=>array("seo_unique_url LIKE '%$unique_url%' "),'fields'=>array('Product.*')));
            
            if(count($productDetail)==0){ // redirecting back to listing page if someone types a wrong url.
                $this->redirect('/product');
            }
            
            $searchId=$productDetail['Product']['id'];
            
        
   
            $conditions=array("Product.id "=> $searchId);
            
            
            
        }    
            
        $product_details= $this->Product->find('first', array(
					'joins' => array(

					   array(
							'table' => 'product_files',
							'alias' => 'ProductFile',
							'type' => 'LEFT',
							'conditions' => array(
								'Product.id = ProductFile.product_id'
							)
						),
						array(
							'table' => 'categories',
							'alias' => 'Category',
							'type' => 'LEFT',
							'conditions' => array(
								'Category.id = Product.category_id'
							)
						),
						array(
							'table' => 'sub_categories',
							'alias' => 'SubCategory',
							'type' => 'LEFT',
							'conditions' => array(
								'SubCategory.id = Product.sub_category_id'
							)
						),
                                                array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'LEFT',
					'conditions' => array(
						'City.id = Product.product_location'
					)
                                                ),
                                        array(
					'table' => 'coffee_types',
					'alias' => 'CoffeeType',
					'type' => 'LEFT',
					'conditions' => array(
						'CoffeeType.id = Product.coffee_type'
					)
                                        ),
                                        

					),
					'conditions' => $conditions,
					'fields' => array('Product.*','ProductFile.resized_img_name','ProductFile.img_path','Category.category_name', 'SubCategory.sub_category_name','City.name','CoffeeType.coffee_type_name'),
					'order' => 'Product.created DESC'
		    ));
            
            
                    
           //debug($product_details); 
            
           $array_sub_categories=$this->SubCategory->find('list',array('fields'=>array('id','sub_category_name'))); 
           $this->ProductFile->unbindModel(array('belongsTo' => array('Product')));
           $productFiles=$this->ProductFile->find('all',array('conditions'=>array('ProductFile.product_id'=>$searchId)));  
            
           
           
           
           $related_product_details= $this->Product->find('all', array(
					'joins' => array(

					   array(
							'table' => 'product_files',
							'alias' => 'ProductFile',
							'type' => 'LEFT',
							'conditions' => array(
								'Product.id = ProductFile.product_id'
							)
						),
						array(
							'table' => 'categories',
							'alias' => 'Category',
							'type' => 'LEFT',
							'conditions' => array(
								'Category.id = Product.category_id'
							)
						),
						array(
							'table' => 'sub_categories',
							'alias' => 'SubCategory',
							'type' => 'LEFT',
							'conditions' => array(
								'SubCategory.id = Product.sub_category_id'
							)
						)

					),
					'conditions' => array('Product.category_id'=>$product_details['Product']['category_id'],"Product.id!=$searchId"),
					'group' => '`Product`.`id`',
                                        'fields' => array('Product.*','ProductFile.*'),
					'order' => 'Product.created DESC'
		    ));
            
        //}
       
        //debug($related_product_details);
        
        unset($product_details['ProductFile']['resized_img_name']);
        unset($product_details['ProductFile']['img_path']);
        
        //debug($productFiles);
        
        $this->set('productFiles',$productFiles); 
        $this->set('array_sub_categories',$array_sub_categories);
        $this->set('product_details',$product_details);
        $this->set('related_product_details',$related_product_details);
        $this->set('meta_keyword',''); 
        $this->set('meta_description',''); 
    }

    public function getAllPageLinks($count,$href) {
		$output = '';
		if(!isset($_GET["page"])) $_GET["page"] = 1;
		$this->perpage=10;
		if($this->perpage != 0)
			$pages  = ceil($count/$this->perpage);
		
		
		//echo "pages>>". $pages." count ".$count;
		if($pages>1) {
			if($_GET["page"] == 1) 
				$output = $output . '<span class="link first disabled">&#8810;</span><span class="link disabled">&#60;</span>';
			else	
				$output = $output . '<a class="link first" onclick="getresult(\'' . $href . (1) . '\')" >&#8810;</a><a class="link" onclick="getresult(\'' . $href . ($_GET["page"]-1) . '\')" >&#60;</a>';
			
			
			if(($_GET["page"]-3)>0) {
				if($_GET["page"] == 1)
					$output = $output . '<span id=1 class="link current">1</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href . '1\')" >1</a>';
			}
			if(($_GET["page"]-3)>1) {
					$output = $output . '<span class="dot">...</span>';
			}
			
			for($i=($_GET["page"]-2); $i<=($_GET["page"]+2); $i++)	{
				if($i<1) continue;
				if($i>$pages) break;
				if($_GET["page"] == $i)
					$output = $output . '<span id='.$i.' class="link current">'.$i.'</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href . $i . '\')" >'.$i.'</a>';
			}
			
			if(($pages-($_GET["page"]+2))>1) {
				$output = $output . '<span class="dot">...</span>';
			}
			if(($pages-($_GET["page"]+2))>0) {
				if($_GET["page"] == $pages)
					$output = $output . '<span id=' . ($pages) .' class="link current">' . ($pages) .'</span>';
				else				
					$output = $output . '<a class="link" onclick="getresult(\'' . $href .  ($pages) .'\')" >' . ($pages) .'</a>';
			}
			
			if($_GET["page"] < $pages)
				$output = $output . '<a  class="link" onclick="getresult(\'' . $href . ($_GET["page"]+1) . '\')" >></a><a  class="link" onclick="getresult(\'' . $href . ($pages) . '\')" >&#8811;</a>';
			else				
				$output = $output . '<span class="link disabled">></span><span class="link disabled">&#8811;</span>';
			
			
		}
		return $output;
	}

    
}