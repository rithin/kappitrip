<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageDealsController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','Experience','Product','DiscountCouponCode','Deal');

	public function beforeFilter()
	{
        parent::beforeFilter();
        //$this->Auth->allow();
	}
    public function deal_list(){ 
		
    }
    
    
    public function ajax_deal_list(){        
         $conditions=array();
         $dealsDetails=$this->Deal->find('all', array(			
            'conditions'=>$conditions,
			 'joins'=>array(
			 		array(
					'table' => 'homestays',
					'alias' => 'Homestay',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = Deal.homestay_id'
					)
				),
                array(
					'table' => 'experiences',
					'alias' => 'Experience',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = Deal.experience_id'
					)
				),
                array(
					'table' => 'products',
					'alias' => 'Product',
					'type'  => 'LEFT',
					'conditions' => array(
						'Product.id = Deal.product_id' 
					)
				),
			 
			 ),
			'fields' => array('Deal.*','Homestay.name','Experience.name','Product.product_name'),           
			'order' => 'Deal.created DESC'
		));
        
		$list_view_array = array();
        foreach($dealsDetails as $key=>$product_data){
                    
                    if($product_data['Deal']['status']==""){
                        $status_code="Enable";
                        $status = 'Disabled';
                    }elseif($product_data['Deal']['status']==1){
                        $status_code="Disable";
                        $status = 'Active';
                    }elseif($product_data['Deal']['status']==2){
                        $status_code="Enable";
                        $status = 'Disabled';
                    }else{
                        $status_code="Enable";
                        $status = 'Disabled';
                    }
                    
                    if($product_data['Deal']['homestay_id']!=""){
                        $deal_item = $product_data['Homestay']['name'];
                    }else if($product_data['Deal']['experience_id']!=""){ 
			$deal_item = $product_data['Experience']['name'];
                    }else if($product_data['Deal']['product_id']!=""){ 
			$deal_item = $product_data['Product']['product_name'];
                    }
                    
                    
                    $list_view_array['data'][$key]['deal_title'] = $product_data['Deal']['deal_title'];
                    $list_view_array['data'][$key]['deal_item'] = $deal_item;
                    $list_view_array['data'][$key]['offer_type'] = $product_data['Deal']['offer_type'];
                    $list_view_array['data'][$key]['offer_value'] = $product_data['Deal']['offer_value'];
                    $list_view_array['data'][$key]['start_date'] = $product_data['Deal']['start_date'];
                    $list_view_array['data'][$key]['end_date'] = $product_data['Deal']['end_date'];
                    $list_view_array['data'][$key]['status'] = $status;
                    $list_view_array['data'][$key]['actions'] = "<a href='".Configure::read('app_root_path')."manage_deals/updateStatus/".$product_data['Deal']['id']."/".$status_code."'>".$status_code."</a> &nbsp; <a href='".Configure::read('app_root_path')."manage_deals/add/".$product_data['Deal']['id']."'> Edit</a>";                
                    
         }
         // debug($dealsDetails);
		 //debug($list_view_array);
		
          echo json_encode($list_view_array);
          exit; 
         
         
    }
    
    
	public function add($id=null){
		
		if($id!=null){
			$this->request->data=$this->Deal->find('first', array(			
            'conditions'=>array('Deal.id'=>$id),
			 'joins'=>array(
			 		array(
					'table' => 'homestays',
					'alias' => 'Homestay',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = Deal.homestay_id'
					)
				),
                array(
					'table' => 'experiences',
					'alias' => 'Experience',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = Deal.experience_id'
					)
				),
                array(
					'table' => 'products',
					'alias' => 'Product',
					'type'  => 'LEFT',
					'conditions' => array(
						'Product.id = Deal.product_id' 
					)
				),
			 
			 ),
			'fields' => array('Deal.*','Homestay.name','Experience.name','Product.product_name'),           
			'order' => 'Deal.created DESC'
			));
			//debug($this->request->data);
		}
				
		$array_properties=$this->Homestay->find('list',array('conditions'=>array('Homestay.status'=>1), 'fields'=>array('id','name'))); 
		$array_experiences=$this->Experience->find('list',array('conditions'=>array('Experience.status'=>1,'Experience.experience_type!=5'),'fields'=>array('id','name'))); 
		$array_products=$this->Product->find('list',array('conditions'=>array('Product.status'=>1), 'fields'=>array('id','product_name'))); 
        $this->set('array_properties',$array_properties);	
        $this->set('array_experiences',$array_experiences);	
		$this->set('array_products',$array_products);	
	}
	public function save(){
		//debug($_REQUEST);
		$data=$_REQUEST['data']['Deal'];
		$this->Deal->saveAll($data);
		$this->redirect(array('controller'=>'manage_deals','action'=>'deal_list'));
	}
	public function updateStatus($id=null,$status_code=null){
		$this->autoRender=false;  
		if($status_code=='Enable'){
			$status_id=1;
		}else{
			$status_id=2;
		}
		$couponStatusArr['Deal']['id'] =$id;
        $couponStatusArr['Deal']['status'] =$status_id;                
        $this->Deal->saveAll($couponStatusArr);
		$this->redirect(array('controller'=>'manage_deals','action'=>'deal_list'));
	}
   
  
	
}
?>