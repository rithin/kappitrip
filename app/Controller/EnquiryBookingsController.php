<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class EnquiryBookingsController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','Experience','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry','ExperienceSlot');
	
	var $components = array('Encryption','General');
	public function beforeFilter()
	{
        parent::beforeFilter();
       // $this->Auth->allow();
	}
	public function add_edit_custom_booking(){
		$array_properties=$this->Homestay->find('list',array('fields'=>array('id','name'))); 
		$array_experiences=$this->Experience->find('list',array('fields'=>array('id','name'))); 
        $this->set('array_properties',$array_properties);	
        $this->set('array_experiences',$array_experiences);	
		
	}
	public function submit_cutom_enquiry(){		
		$dataEnquiry=$this->request->data;
		$guest_name=$dataEnquiry['BookingEnquiry']['guest_name'];
		$guest_email=$dataEnquiry['BookingEnquiry']['email'];
		$type_of_request=$dataEnquiry['BookingEnquiry']['type_of_request'];
		@$property_id=$dataEnquiry['BookingEnquiry']['property_id'];
		@$experience_id=$dataEnquiry['BookingEnquiry']['experience_id'];
        @$experience_slot_id=$dataEnquiry['BookingEnquiry']['experience_slot'];
		@$no_of_rooms=count($dataEnquiry['BookingEnquiry_rooms']);
		$phone_no=$dataEnquiry['BookingEnquiry']['phone_no'];
		$no_of_guest=$dataEnquiry['BookingEnquiry']['no_of_guest'];
		$no_of_children=$dataEnquiry['BookingEnquiry']['no_of_child'];
		$price=$dataEnquiry['BookingEnquiry']['price'];
		$checkin_date=$dataEnquiry['BookingEnquiry']['checkin_date'];
		$checkout_date=$dataEnquiry['BookingEnquiry']['checkout_date'];
		$notes=$dataEnquiry['BookingEnquiry']['notes'];
		@$room_id_selected=implode("_",$dataEnquiry['BookingEnquiry_rooms']);
		$display_prop="none"; 
		
		$dataArray['BookingEnquiry']['type']="custom_package";
		$dataArray['BookingEnquiry']['special_price']=$price;
		$dataArray['BookingEnquiry']['start_date']=$checkin_date;
		$dataArray['BookingEnquiry']['end_date']=$checkout_date;
		$dataArray['BookingEnquiry']['no_of_adult']=$no_of_guest;
		$dataArray['BookingEnquiry']['no_of_children']=$no_of_children;
		$dataArray['BookingEnquiry']['full_name']=$guest_name;
		$dataArray['BookingEnquiry']['contact_no']=$phone_no;
		$dataArray['BookingEnquiry']['contact_email']=$guest_email;
		$dataArray['BookingEnquiry']['message_box']=$notes;
		@$dataArray['BookingEnquiry']['home_stay_id']=$property_id;
		@$dataArray['BookingEnquiry']['experience_id']=$experience_id;	
        @$dataArray['BookingEnquiry']['room_id_selected']=$room_id_selected;	
		$response=$this->BookingEnquiry->saveAll($dataArray);  
	 	$lastInsertBookingEngquiryID = $this->BookingEnquiry->getLastInsertID();  
		
		//debug($dataEnquiry);
		
		if($dataEnquiry['BookingEnquiry']['type_of_request']=='Property'){
			$display_prop="block";
			$homestay_id=$dataEnquiry['BookingEnquiry']['property_id'];
			$conditions=array('Homestay.id'=>$homestay_id);
						
			$this->Homestay->recursive=2;
			$this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
			$this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
			$this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
			
			$homestayDetails=$this->Homestay->find('first', array(
				'joins' => array(
					array(
						'table' => 'homestay_files',
						'alias' => 'HomestayFile',
						'type'  => 'LEFT',
						'conditions' => array(
							'Homestay.id = HomestayFile.homestay_id'
						)
					),
					array(
						'table' => 'homestay_amenities',
						'alias' => 'HomestayAmenity',
						'type'  => 'LEFT',
						'conditions' => array(
							'Homestay.id = HomestayAmenity.homestay_id'
						)
					),
					array(
						'table' => 'room_price_details',
						'alias' => 'RoomPriceDetail',
						'type'  => 'LEFT',
						'conditions' => array(
							'Homestay.id = RoomPriceDetail.homestay_id'
						)
					),
					

				),
            'conditions'=>$conditions,
			'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.created DESC'
		   ));
		//	debug($homestayDetails);
			
			$homestay_head_image="";	
			if(@is_array($homestayDetails['HomestayFile'][0])){
	 			@$homestay_head_image=Configure::read('app_root_path').$homestayDetails['Homestay']['HomestayFile'][0]['file_path']['file_name'];
	    	}
			$subject="Customised booking plan for   ".$homestayDetails['Homestay']['name']." from ".date('Y-m-d' ,strtotime($checkin_date))." to ".date('Y-m-d',strtotime($checkout_date));
			$room_ids=json_encode($dataEnquiry['BookingEnquiry_rooms']);
			$unique_email_code=$this->generateRandomString();
			$parameters="?homestay_id=".$homestay_id."&unique_email_code=".$unique_email_code." &total_amount=".$price."&booking_enquiry_id=".$lastInsertBookingEngquiryID."&startDate=".date('Y-m-d',strtotime($checkin_date))."&endDate=".date('Y-m-d',strtotime($checkout_date)). "&room_id_selected=".$room_id_selected."&flagSameRoomAvail=no&qtyInputAdult=".$no_of_guest."&qtyInputChild=".$no_of_children."&qtyInputInfant=0"."&room_ids=".$room_ids;
			
			$accept_url=Configure::read('app_root_path')."booking/index".$parameters."&enquiry_accept_reject=accepted";		$reject_url=Configure::read('app_root_path')."booking/acceptRejectEnquiryResponse".$parameters."&enquiry_accept_reject=rejected";	
			
			$emailVars=array(
								'request_type'=>'Homestay',
			 					'request_type_name' => $homestayDetails['Homestay']['name'],
								'request_type_head_image'=>$homestay_head_image,
							   	'guest_name' =>$guest_name,
								'guest_email'=>$guest_email,
								'no_of_rooms' =>$no_of_rooms,
							   	'no_of_adult' => ($no_of_guest),
							 	'no_of_children' =>$no_of_children,
								'check_in_date'=>date('Y-m-d',strtotime($checkin_date)),
								'check_out_date'=>date('Y-m-d',strtotime($checkout_date)),
								'contact_no'=>$phone_no,
								'message_box'=>$notes,								
			   					'display_prop'=>$display_prop,
								'accept_url'=>$accept_url,
			 					'reject_url'=>$reject_url
								
								
							 );
			$EmailStay = new CakeEmail();	
		// debug($EmailHostAn);
			$EmailStay->config('gmail');
			$EmailStay->config(array('from' => 'coorgexpressbooking@gmail.com'));
			$EmailStay->viewVars($emailVars); 
			$EmailStay->template('custom_enquiry_customer',null)
									->emailFormat('html')
									->to($guest_email) 
									->cc(Configure::read('booking_email_cc'))
									->from(Configure::read('smtp_from_email'))
									->subject($subject)
									->send();
			
		
		}
		if($dataEnquiry['BookingEnquiry']['type_of_request']=='Experience'){
		     $display_prop="none";
			 $conditions=array("Experience.id "=> $experience_id,'ExperienceSlot.id'=>$experience_slot_id); 
			 $today=date('Y-m-d');
			 $this->Experience->recursive=2;
			 $this->Experience->unbindModel(array('hasMany' => array('ExperienceSlot')));
			 $this->ExperienceFile->unbindModel(array('belongsTo' => array('Experience')));
			 $this->ExperienceLanguage->unbindModel(array('belongsTo' => array('Experience')));
			 $this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));        
			 unset($this->ExperienceSlot->virtualFields['no_of_slots']);
			 $experienceDetails=$this->Experience->find('first', array(
				'joins' => array(
					array(
						'table' => 'experience_files',
						'alias' => 'ExperienceFile',
						'type'  => 'LEFT',
						'conditions' => array(
							'Experience.id = ExperienceFile.experience_id'
						)
					),
					array(
						'table' => 'experience_languages',
						'alias' => 'ExperienceLanguage',
						'type'  => 'LEFT',
						'conditions' => array(
							'Experience.id = ExperienceLanguage.experience_id'
						)
					),
					array(
						'table' => 'experience_types',
						'alias' => 'ExperienceType',
						'type'  => 'LEFT',
						'conditions' => array(
							'Experience.experience_type = ExperienceType.id'
						)
					),                    
					array(
						'table' => 'experience_slots',
						'alias' => 'ExperienceSlot',
						'type'  => 'LEFT',
						'conditions' => array(
							'Experience.id = ExperienceSlot.experience_id'
						)
					),

				),
				'conditions'=>$conditions,
				'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path', 'ExperienceLanguage.*', 'ExperienceType.*','ExperienceSlot.*'),
				 //,'ExperienceFile.file_name','ExperienceFile.file_path', 'ExperienceLanguage.*', 'ExperienceType.*','ExperienceSlot.*'
				'group' => '`Experience`.`id`',
				'order' => 'Experience.created DESC'
			));
			//debug($experienceDetails); die;
		   $experience_head_image="";	
		   if(@is_array($experienceDetails['ExperienceFile'][0])){
	 			@$experience_head_image=Configure::read('app_root_path').$experienceDetails['Experience']['ExperienceFile'][0]['file_path']['file_name'];
	    	}
            $slot_start_time=$experienceDetails['ExperienceSlot']['slot_start_time'];
            $slot_end_time=$experienceDetails['ExperienceSlot']['slot_end_time'];
            
            $unique_email_code=$this->generateRandomString();
			$parameters="?experience_id=".experience_id."&unique_email_code=".$unique_email_code." &total_amount=".$price."&experience_slot_id=".experience_slot_id."&startDate=".date('Y-m-d',strtotime($checkin_date)). "&slot_start_time=".$slot_start_time."&slot_end_time".$slot_end_time."&no_of_people=".$no_of_guest."&min_guest=".$no_of_guest."&no_of_guest=".$no_of_guest;
            
            $accept_url=Configure::read('app_root_path')."booking/experience".$parameters."&enquiry_accept_reject=accepted";		$reject_url=Configure::read('app_root_path')."booking/acceptRejectEnquiryResponse".$parameters."&enquiry_accept_reject=rejected";
			$subject="Customised booking plan for   ".$experienceDetails['Experience']['name']." on ".date('Y-m-d' ,strtotime($checkin_date))." from ".$slot_start_time." to ".$slot_end_time;
           $emailVars=array(
			 					'request_type'=>'Experience',
			 					'request_type_name' => $experienceDetails['Experience']['name'],
								'request_type_head_image'=>$experience_head_image,
							   	'guest_name' =>$guest_name,
								'guest_email'=>$guest_email,
								'slot_date' =>date('Y-m-d',strtotime($checkin_date)),
							   	'no_of_adult' => ($no_of_guest),
							 	'slot_timing' =>$slot_start_time." to ".$slot_end_time,
								'contact_no'=>$phone_no,
								'message_box'=>$notes,
			   					'display_prop'=>$display_prop,
								'accept_url'=>$accept_url,
			 					'reject_url'=>$reject_url
							);
			
			
			$EmailEnquiry = new CakeEmail();	
		// debug($EmailHostAn);
			$EmailEnquiry->config('gmail');
			$EmailEnquiry->config(array('from' => 'coorgexpressbooking@gmail.com'));
			$EmailEnquiry->viewVars($emailVars); 
			$EmailEnquiry->template('custom_experience_enquiry_customer',null)
									->emailFormat('html')
									->to($guest_email) 
									->cc(Configure::read('booking_email_cc'))
									->from(Configure::read('smtp_from_email'))
									->subject($subject)
									->send();
		
		}
		$dataArrayUpdate['BookingEnquiry']['id']=$lastInsertBookingEngquiryID;
		$dataArrayUpdate['BookingEnquiry']['pay_url']=$accept_url;
		$response=$this->BookingEnquiry->saveAll($dataArrayUpdate); 
		$this->set('display_prop',$display_prop);
		
	 	
		//die;
		 $action_url='enquiry_list';
         $this->redirect(array('controller'=>'enquiry_bookings','action'=>$action_url));
		
}
	
public function findRooms(){
		$this->autoRender=false;// ajax method for finding the rooms of property.       
		$homestay_id=$_REQUEST['property_id'];
		$freeRoomDetail=$this->RoomPriceDetail->query("SELECT id,room_no_name from room_price_details as RoomPriceDetail  WHERE  `RoomPriceDetail`.`homestay_id`=$homestay_id  AND `RoomPriceDetail`.`status`=1 ");
		//debug($freeRoomDetail);
		$room_ids=array();
		foreach($freeRoomDetail as $dtl){
			$room_ids[$dtl['RoomPriceDetail']['id']]=$dtl['RoomPriceDetail']['room_no_name'];
		}
		echo json_encode($room_ids);
	}
    public function findSlots(){
        $this->autoRender=false;// ajax method for finding the rooms of property.       
		$experience_id=$_REQUEST['experience_id'];
		$slotDetail=$this->ExperienceSlot->query("SELECT id,slot_start_time,slot_end_time from experience_slots as ExperienceSlot  WHERE  `ExperienceSlot`.`experience_id`=$experience_id  AND `ExperienceSlot`.`status`=1 ");
		//debug($freeRoomDetail);
		$slots=array();
		foreach($slotDetail as $dtl){
			$slots[$dtl['ExperienceSlot']['id']]=$dtl['ExperienceSlot']['slot_start_time']." - ".$dtl['ExperienceSlot']['slot_end_time'];
		}
		echo json_encode($slots);
    }
    public function enquiry_list(){ 
		 
    }
    
    
    public function ajax_enquiry_list(){ 
        
        $conditions=array();
        // $this->BookingOrderDetail->recursive=1;
         $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
         $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
        
		$joins = array(				
				array(
					'table' => 'homestays',
					'alias' => 'Homestay',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = BookingEnquiry.home_stay_id'
					)
				),
				array(
					'table' => 'experiences',
					'alias' => 'Experience',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = BookingEnquiry.experience_id'
					)
				)
			);
		$fields=array('BookingEnquiry.*','Homestay.*');
		
                
                $enquiryDetails=$this->BookingEnquiry->find('all', array(
			'joins' => $joins,
                        'conditions' => $conditions,
			'fields' => $fields,
                        'group' => '`BookingEnquiry`.`id`',
			'order' => 'BookingEnquiry.created DESC'
		)); 
                
               
                
                
                $list_view_array = array();
                
                foreach($enquiryDetails as $key=>$details){
                    
			$quote_enquiry_flag="";
			if($details['BookingEnquiry']['type']=="custom_package") 
                            $quote_enquiry_flag= "Quote"; 
			else 
                            $quote_enquiry_flag="Enquiry";
                        
                        $homestay_experience_name = '';
                        
                        if(@$details['BookingEnquiry']['home_stay_id']!="") {
                            $homestay_experience_name = @$details['Homestay']['name'];
			}else if(@$details['BookingEnquiry']['experience_id']!=""){
                            $homestay_experience_name = @$details['Experience']['name']; 
			} 
                        
                        if(!empty($details['BookingEnquiry']['no_of_adult'])){
                            $no_of_adult= $details['BookingEnquiry']['no_of_adult'];
                        }else{
                            $no_of_adult = 0;
                        }
                        
                        if(!empty($details['BookingEnquiry']['no_of_children'])){
                            $no_of_children = $details['BookingEnquiry']['no_of_children'];
                        }else{
                            $no_of_children = 0;
                        }
                        
                        $list_view_array['data'][$key]['type'] = $quote_enquiry_flag;
                        $list_view_array['data'][$key]['display_type'] = "<a href='javascript:viewDetails(".$details['BookingEnquiry']['id'].")'>".$quote_enquiry_flag."</a>"; 
			$list_view_array['data'][$key]['customer_name'] = $details['BookingEnquiry']['full_name'];			
                        $list_view_array['data'][$key]['homestay_experience_name'] = $homestay_experience_name;
                        $list_view_array['data'][$key]['contact_email'] = $details['BookingEnquiry']['contact_email'];
			$list_view_array['data'][$key]['contact_no'] = $details['BookingEnquiry']['contact_no'];
                        $list_view_array['data'][$key]['start_date'] = $details['BookingEnquiry']['start_date'];
                        $list_view_array['data'][$key]['end_date'] = $details['BookingEnquiry']['end_date'];
                        $list_view_array['data'][$key]['no_of_adult'] = $no_of_adult;
                        $list_view_array['data'][$key]['no_of_children'] = $no_of_children;
                        
                        
                 }
	
                 
                 echo json_encode($list_view_array);
                
                 exit;
        
                
                
                
    }
   
    public function generateRandomString($length = 10) {
		$this->autoRender=false;//inside call     
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
    }
	
	public function view_details(){ 
		$this->layout=false; 
		$booking_enquiry_id=$_REQUEST['booking_enquiry_id'];
		$conditions=array('BookingEnquiry.id'=>$booking_enquiry_id);
        // $this->BookingOrderDetail->recursive=1;
        $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
        $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
        
		$joins = array(				
				array(
					'table' => 'homestays',
					'alias' => 'Homestay',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = BookingEnquiry.home_stay_id'
					)
				),
				array(
					'table' => 'experiences',
					'alias' => 'Experience',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = BookingEnquiry.experience_id'
					)
				)
			
			);
		$fields=array('BookingEnquiry.*','Homestay.*','Experience.*');
		$enquiryDetails=$this->BookingEnquiry->find('first', array(
			'joins' =>$joins,
			'conditions' => $conditions,
			'limit' => 1,
			'order' => array('BookingEnquiry.created' => 'DESC'),
			'fields' => $fields,
            'group' => '`BookingEnquiry`.`id`',
		));
		//$enquiryDetails = $this->paginate('BookingEnquiry');    
        $this->set('enquiryDetails',$enquiryDetails);	
       // debug($enquiryDetails);
		//die;
    }
    
}
?>