<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class CartsController extends AppController {

    var $layout ="checkout_layout";
	public $uses = array('Product','Cart','ProductFile','Category','SubCategory','ProductPaymentResponse','ProductOrderDetail', 'DiscountCouponCode','State','User','UserDetail');
    var $components = array('Encryption','General','Cookie');
	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
	public function add_product_to_cart() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
              $numOfQty=1;
              if(@$_REQUEST['numOfQty']){
                 @$numOfQty=$_REQUEST['numOfQty'];
             }
              $this->Cart->addProduct($_REQUEST['product_id'],$numOfQty);
		}
		$count=$this->Cart->getCount();
		$cartTotal=$this->getCartTotal();
		echo json_encode(array('itemCount'=>$count,'cartTotal'=>$cartTotal));
	}
        
    public function delete_product_from_cart(){
        $this->autoRender = false;
		if ($this->request->is('post')) {
             $this->Cart->deleteProduct($_REQUEST['product_id'],$numOfQty);
		}
		$count=$this->Cart->getCount();                
		$cartTotal=$this->getCartTotal();
		echo json_encode(array('itemCount'=>$count,'cartTotal'=>$cartTotal));
            
     }
        
    public function clearcart(){        
            $this->Cart->clearCart();
            
     }
	public function getCartCount() {
        $this->autoRender = false;
		$count=$this->Cart->getCount();
        $cartTotal=$this->getCartTotal();
		echo json_encode(array('itemCount'=>$count,'cartTotal'=>$cartTotal));
     }
	public function getCartTotal(){
		$cartTotal=0;
        $carts = $this->Cart->readProduct();
        $products = array();
        if (null!=$carts) {
           foreach ($carts as $productId => $count) {
                $product = $this->Product->read(null,$productId);
				$cartTotal+=($count*$product['Product']['unit_price']);                   
             }				
        } 
		return $cartTotal;
		
	}
	public function view() {
        $this->layout="list_checkout";
		$carts = $this->Cart->readProduct();
		$products = array();
		if (null!=$carts) {
			foreach ($carts as $productId => $count) {
				$product = $this->Product->read(null,$productId);
				$product['Product']['count'] = $count;
				$products[]=$product;
			}
		} 
		$this->set(compact('products'));
	}
	
	public function single_page_checkout(){
		$cart_count=$this->Cart->getCount();
         if($cart_count==0){ 
                    //$this->redirect('/shop');
         }
        $states_arr=$this->State->find('list',array('fields'=>array('id','state_name'),'order'=>array('state_name ASC')));
		$carts = $this->Cart->readProduct();
		$products = array();
		if (null!=$carts) {
			foreach ($carts as $productId => $count) {
				$product = $this->Product->read(null,$productId);
				$product['Product']['count'] = $count;
				$products[]=$product;
			}
		} 
		if($this->Auth->user('id')!=null){
		    $user_id =$this->Auth->user('id');
            $result_user=$this->User->find('first',array( 
			 'joins' => array(
						array(
							'table' => 'user_details',
							'alias' => 'UserDetail',
							'type' => 'LEFT',
							'conditions' => array(
								'User.id = UserDetail.user_id'
							)
						),
			  ),
			'conditions'=>array('User.id'=>$user_id),'fields'=>array('User.*','UserDetail.*')));
		//	debug($result_user);
			$this->request->data['Billing']['billing_name']=$result_user['User']['first_name']." ".$result_user['User']['last_name'];
			$this->request->data['Billing']['billing_email']=$result_user['User']['email_id'];
			$this->request->data['Billing']['billing_tel']=$result_user['User']['contact_number'];
			$this->request->data['Billing']['billing_address']=$result_user['UserDetail']['address1']." ".$result_user['UserDetail']['address2'];
			$this->request->data['Billing']['billing_city']=$result_user['UserDetail']['city'];
			$this->request->data['Billing']['billing_state']=$result_user['UserDetail']['state_id'];
			$this->request->data['Billing']['billing_country']=$result_user['UserDetail']['country'];
        }
		$this->set('shipping_charge',30);
		$this->set('states_arr',$states_arr);
		$this->set(compact('products'));
        $this->set('cart_count',$cart_count);
	}
		
	public function update() {
		if ($this->request->is('post')) {
			if (!empty($this->request->data)) {
				$cart = array();
				foreach ($this->request->data['Cart']['count'] as $index=>$count) {
					if ($count>0) {
						$productId = $this->request->data['Cart']['product_id'][$index];
						$cart[$productId] = $count;
					}
				}
				$this->Cart->saveProduct($cart);
			}
		}
		$this->redirect(array('action'=>'single_page_checkout'));
	}
    
    public function ajax_mini_cart(){
        $this->layout = false;
        if ($this->request->is('post')) {           
            $carts = $this->Cart->readProduct();
            $products = array();
            if (null!=$carts) {
                foreach ($carts as $productId => $count) {
                    $product = $this->Product->read(null,$productId);
					//$cartTotal+=($count*$product['Product']['unit_price']);
                    $product['Product']['count'] = $count;					
                    $products[]=$product;
                }
            } 
            $this->set('products',$products);
        }
    }
    
    public function make_payment(){
        $cart_data=$_POST['data']['Cart'];
        $billing_data=$_POST['data']['Billing'];
        $billing_state_id = $billing_data['billing_state'];
        $billing_state_data = $this->State->find('first',array('fields'=>array('id','state_name'), 'conditions'=>array('id'=>$billing_state_id)));
        $billing_data['billing_state'] = $billing_state_data['State']['state_name'];
        $billing_data['billing_country'] = 'India';
        $shipping_data = array();
        $shipping_data=$_POST['data']['Shipping'];
        if(!empty($shipping_data['delivery_state'])){        
			$shipping_state_id = $shipping_data['delivery_state'];        
			$shipping_state_data = $this->State->find('first',array('fields'=>array('id','state_name'),
																	'conditions'=>array('id'=>$shipping_state_id)));        
			$shipping_data['delivery_state'] = $shipping_state_data['State']['state_name'];        
        }
        
        if($cart_data['same_shipping_billing']==1){            
            $shipping_data['delivery_name'] = $billing_data['billing_name'];
            $shipping_data['delivery_email'] = $billing_data['billing_email'];
            $shipping_data['delivery_tel'] = $billing_data['billing_tel'];
            $shipping_data['delivery_address'] = $billing_data['billing_address'];
            $shipping_data['delivery_zip'] = $billing_data['billing_zip'];
            $shipping_data['delivery_city'] = $billing_data['billing_city'];
            $shipping_data['delivery_state'] = $billing_data['billing_state'];
            $shipping_data['delivery_country'] = $billing_data['billing_country'];
            
        }
        
        
        $shipping_data['delivery_country'] = 'India';        
        $merchant_data='4538';
	    $working_key='8B7C0D424D6ECF825FEA92094F6027B6';//Shared by CCAVENUES
	    $access_code='AVBT01FB53BW15TBWB';//Shared by CCAVENUES
	
        foreach ($cart_data as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
        foreach ($billing_data as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
        
        if(!empty($shipping_data)){
			foreach ($shipping_data as $key => $value){
				$merchant_data.=$key.'='.$value.'&';
			}
        }
        
        $encrypted_data=$this->Encryption->encrypt($merchant_data,$working_key); // Method for encrypting the data.
        $this->set('encrypted_data',$encrypted_data);
        $this->set('access_code',$access_code);
        if($cart_data['payment_method']=='cod'){      
            if($this->Auth->user('id')!=null){
				$user_id =$this->Auth->user('id');
                $restult_user=$this->User->find('first',array('conditions'=>array('id'=>$user_id)));
            }
            $carts = $this->Cart->readProduct();
            $products = array();
			if (null!=$carts) {
				foreach ($carts as $productId => $count) {
					$product = $this->Product->read(null,$productId);
					$product['Product']['count'] = $count;
					$products[]=$product;
				}
			 }
          $order_id=$cart_data['order_id'];
          $productDetailarr=$this->ProductOrderDetail->find('first',array('conditions'=> array('ProductOrderDetail.order_id'=>$order_id))); 
          if(count($productDetailarr)==0){        
            $ProductOrderDetail=array();
            foreach($products as $key=>$item){
                $ProductOrderDetail[$key]['ProductOrderDetail']['order_id']=$order_id;
                $ProductOrderDetail[$key]['ProductOrderDetail']['order_date']=date('Y-m-d H:i:s');
                $ProductOrderDetail[$key]['ProductOrderDetail']['product_id']=$item['Product']['id'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['unit_price']=$item['Product']['unit_price'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['quantity']=$item['Product']['count'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['tax']='';
                $ProductOrderDetail[$key]['ProductOrderDetail']['total_price']=($item['Product']['count'] * $item['Product']['unit_price']);
            }            
            $this->ProductOrderDetail->saveAll($ProductOrderDetail);
            
            $responseDetailArray['order_id']= $order_id;
            $responseDetailArray['order_status']= 'Success';
            $responseDetailArray['payment_mode']= 'cod';
            $responseDetailArray['amount']= $cart_data['amount'];
            $responseDetailArray['trans_date']= date('Y-m-d H:i:s');            
            $responseDetailArray['billing_name']= $billing_data['billing_name'];
            $responseDetailArray['billing_address']= $billing_data['billing_address'];
            $responseDetailArray['billing_city']= $billing_data['billing_city'];
            $responseDetailArray['billing_state']= $billing_data['billing_state'];
            $responseDetailArray['billing_zip']= $billing_data['billing_zip'];
            $responseDetailArray['billing_country']= $billing_data['billing_country'];
            $responseDetailArray['billing_tel']= $billing_data['billing_tel'];
            $responseDetailArray['billing_email']= $billing_data['billing_email'];            
            $responseDetailArray['delivery_name']= $shipping_data['delivery_name'];
            $responseDetailArray['delivery_address']= $shipping_data['delivery_address'];
            $responseDetailArray['delivery_city']= $shipping_data['delivery_city'];
            $responseDetailArray['delivery_zip']= $shipping_data['delivery_zip'];
            $responseDetailArray['delivery_country']= $shipping_data['delivery_country'];
            $responseDetailArray['delivery_tel']= $shipping_data['delivery_tel'];            
            $responseDetailArray['mer_amount']= $cart_data['amount'];            
            $lastInsertOrderDetailID = $this->ProductOrderDetail->getLastInsertID();  
            $ProductPaymentResponseArr['ProductPaymentResponse']= $responseDetailArray;
			$ProductPaymentResponseArr['ProductPaymentResponse']['status']=
            $ProductPaymentResponseArr['ProductPaymentResponse']['product_order_detail_id']= $lastInsertOrderDetailID;  
            $this->ProductPaymentResponse->saveAll($ProductPaymentResponseArr);           
          }
          $this->redirect(array('controller' => 'carts', 'action' => 'purchase_response','cod',$order_id));
        }
        
    }
    public function confirm_purchase(){
		$merchant_data='4538';	
        $working_key=Configure::read('working_key');
        $access_code=Configure::read('access_code');
		$purchase_data=array();$cart_grand_total=0; $shipping_charges=0;$cart_sub_total=0;$discounted_amount=0;
		$discount_type='';$discount_value='';
		
		
		$carts = $this->Cart->readProduct();
		$products = array(); $cartDetail=array();
		if (null!=$carts) {
			$k=0; 
			foreach ($carts as $productId => $count) {
				$product = $this->Product->read(null,$productId);
				$product['Product']['count'] = $count;
				$products[$k]['id']=$productId;
				$products[$k]['product_name']=$product['Product']['product_name'];
				$products[$k]['quantity']=$count;
				$products[$k]['unit_price']=$product['Product']['unit_price'];
				$products[$k]['unit_total']=($product['Product']['unit_price']*$count);
				$cart_grand_total+=($product['Product']['unit_price']*$count);
				$k++;
			}			
		}
		$cartDetail['grand_total']=$this->request->data['Cart']['amount'];
		$cartDetail['shipping_charges']=$shipping_charges;
		$cartDetail['cart_sub_total']=$cart_grand_total;
		$cartDetail['discounted_amount']=$discounted_amount;
		$cartDetail['discount_type']=$discount_type;
		$cartDetail['discount_value']=$discount_value;		
		$order_id=$_POST['data']['Cart']['order_id'];
		$purchase_data[$order_id]['CartProductDetail']=$products;
		$purchase_data[$order_id]['cartDetail']=$cartDetail;
		
		foreach ($this->request->data['Cart'] as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
		foreach ($this->request->data['Billing'] as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
		foreach ($this->request->data['Shipping'] as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
        CakeSession::write('purchase_data',$purchase_data);
        $sessionPurchaseData=CakeSession::read('purchase_data');
        $encrypted_data=$this->Encryption->encrypt($merchant_data,$working_key); // Method for encrypting the data.
		//die;
    
        $this->set('encrypted_data',$encrypted_data);
        $this->set('access_code',$access_code);
	}
    public function purchase_response($payment_type=null,$order_id=null){
		$working_key=Configure::read('working_key');
        $access_code=Configure::read('access_code');
        $encResponse=$_POST["encResp"];		//This is the response given by the CCAvenue Server
        $rcvdString=$this->Encryption->decrypt($encResponse,$working_key);// Decryption used as per the specified working key.
        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $responseDetailArray=array();   
		$invoice_no=$this->General->generateInvoiceNo();	  
        foreach($decryptValues as $key=>$value){
            $information=explode('=',$value);
            $responseDetailArray[$information[0]]=$information[1];
        }
        if($this->Auth->user('id')!=null){
		$user_id =$this->Auth->user('id');
        	$restult_user=$this->User->find('first',array('conditions'=>array('id'=>$user_id)));
		}
		$sessionPurchaseData=CakeSession::read('purchase_data');
		//debug($sessionPurchaseData);
		
		$order_status=$responseDetailArray['order_status'];
		$order_id=$responseDetailArray['order_id'];  
        $billing_email=$responseDetailArray['billing_email'];
        $productDetailarr=$this->ProductOrderDetail->find('first', array('conditions'=> array('ProductOrderDetail.order_id'=>$order_id), 'fields'=>array('order_id'))); 
       
		$item_details_string="";$grand_total_amount=0;
        if(count($productDetailarr)==0){        
            $ProductOrderDetail=array();
            foreach($sessionPurchaseData[$order_id]['CartProductDetail'] as $key=>$item){
                $ProductOrderDetail[$key]['ProductOrderDetail']['order_id']=$responseDetailArray['order_id'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['order_date']=date('Y-m-d H:i:s');
                $ProductOrderDetail[$key]['ProductOrderDetail']['product_id']=$item['id'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['unit_price']=$item['unit_price'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['quantity']=$item['quantity'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['tax']='';
                $ProductOrderDetail[$key]['ProductOrderDetail']['total_price']=$item['unit_total'];
				$item_details_string.="<tr><td>".$item['product_name']."</td> <td>".$item['unit_price']."</td><td>".$item['quantity']."</td><td>".$item['unit_total']."</td></tr>";
				$grand_total_amount+=$item['unit_total'];
           }            
            $this->ProductOrderDetail->saveAll($ProductOrderDetail);
            $lastInsertOrderDetailID = $this->ProductOrderDetail->getLastInsertID();   
            $ProductPaymentResponseArr['ProductPaymentResponse']= $responseDetailArray;
			$ProductPaymentResponseArr['ProductPaymentResponse']['invoice_no']= $invoice_no;
            $ProductPaymentResponseArr['ProductPaymentResponse']['product_order_detail_id']= $lastInsertOrderDetailID;           
            $this->ProductPaymentResponse->saveAll($ProductPaymentResponseArr);  
        }
       // debug($responseDetailArray);
		################################### CUSTOMER ORDER EMAIL ############################################
		 $subject="You have been successfully placed an order on Kaapitrip.com.";		  
		 $customer_billing_email=$responseDetailArray['billing_email'];
		 $customer_name=$responseDetailArray['billing_name'];
		 $order_date=$responseDetailArray['trans_date'];
		 $billing_address=$responseDetailArray['billing_name']."<br>";
		 $billing_address.=$responseDetailArray['billing_address']."<br>";
 		 $billing_address.=$responseDetailArray['billing_city'].",";
		 $billing_address.=$responseDetailArray['billing_state']."<br>";
		 $billing_address.=$responseDetailArray['billing_country'].",";
		 $billing_address.=$responseDetailArray['billing_zip'];
		
		 $shipping_address=$responseDetailArray['delivery_name']."<br>";
		 $shipping_address.=$responseDetailArray['delivery_address']."<br>";
 		 $shipping_address.=$responseDetailArray['delivery_city'].",";
		 $shipping_address.=$responseDetailArray['delivery_state']."<br>";
		 $shipping_address.=$responseDetailArray['delivery_country'].",";
		 $shipping_address.=$responseDetailArray['delivery_zip']."<br>";
		 $shipping_address.="Phone:-".$responseDetailArray['delivery_tel'];
			 
		 $EmailHost = new CakeEmail();	
		 $EmailHost->config('gmail');
		 $EmailHost->config(array('from' => Configure::read('smtp_from_email')));
		 $EmailHost->viewVars(array(
			 					'customer_name' =>$customer_name,
								'invoice_no'=>$invoice_no,
							   	'order_id' =>$order_id,
							   	'order_date' => $order_date,
							 	'billing_address' =>$billing_address,
								'shipping_address'=>$shipping_address,
								'item_details_string'=>$item_details_string,
								'grand_total_amount'=>$grand_total_amount						
								
							   )); 
		 $EmailHost->template('purchase_invoice_customer',null)
								->emailFormat('html')
								->to($customer_billing_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();
		################################### CUSTOMER ORDER EMAIL ############################################
		$this->set('responseDetailArray',$responseDetailArray);
        $this->set('order_id',$responseDetailArray['order_id']);
        $this->set('billing_email',$billing_email);       
        $this->clearcart();
        $this->Session->delete("purchase_data"); // deleting the session data of current booking
		
  }
  public function purchase_cancel(){
        //$this->autoRender=false;// response after successfull payment. 
     //   $this->layout="list_checkout";
         //Get Response
        $working_key=Configure::read('working_key');
        $access_code=Configure::read('access_code');
        $encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
        $rcvdString=$this->Encryption->decrypt($encResponse,$working_key);//Crypto Decryption used as per the specified working key.
        $decryptValues=explode('&', $rcvdString);
       // debug($decryptValues); 
        $dataSize=sizeof($decryptValues);
        $responseDetailArray=array();
        
        foreach($decryptValues as $key=>$value){
            $information=explode('=',$value);
            $responseDetailArray[$information[0]]=$information[1];
        }
        $order_status=$responseDetailArray['order_status'];
        $carts = $this->Cart->readProduct();
        $products = array();
		if (null!=$carts) {
			foreach ($carts as $productId => $count) {
				$product = $this->Product->read(null,$productId);
				$product['Product']['count'] = $count;
				$products[]=$product;
			}
		} 
        $order_id=$responseDetailArray['order_id'];
        $billing_email=$responseDetailArray['billing_email'];
        $productDetailarr=$this->ProductOrderDetail->find('first',array('conditions'=> array('ProductOrderDetail.order_id'=>$order_id) , 'fields'=>array('order_id'))); 
        
        if(count($productDetailarr)==0){        
            $ProductOrderDetail=array();
            foreach($products as $key=>$item){
                $ProductOrderDetail[$key]['ProductOrderDetail']['order_id']=$responseDetailArray['order_id'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['order_date']=date('Y-m-d H:i:s');
                $ProductOrderDetail[$key]['ProductOrderDetail']['product_id']=$item['Product']['id'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['unit_price']=$item['Product']['unit_price'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['quantity']=$item['Product']['count'];
                $ProductOrderDetail[$key]['ProductOrderDetail']['tax']='';
                $ProductOrderDetail[$key]['ProductOrderDetail']['total_price']=($item['Product']['count'] * $item['Product']['unit_price']);
            }            
            $this->ProductOrderDetail->saveAll($ProductOrderDetail);
            $lastInsertOrderDetailID = $this->ProductOrderDetail->getLastInsertID();   
            $ProductPaymentResponseArr['ProductPaymentResponse']= $responseDetailArray;
            $ProductPaymentResponseArr['ProductPaymentResponse']['product_order_detail_id']= $lastInsertOrderDetailID;            
            $this->ProductPaymentResponse->saveAll($ProductPaymentResponseArr);  
        }
       // debug($responseDetailArray);
        $this->set('responseDetailArray',$responseDetailArray);
        $this->set('order_id',$responseDetailArray['order_id']);
        $this->set('billing_email',$billing_email);
        $this->Session->delete('cart');
    }
    
    public function purchase_invoice($order_id=null){
         $this->layout=false;
       
        $paymentResponseDetailArr=$this->ProductPaymentResponse->find('first',array('conditions'=> array('ProductPaymentResponse.order_id'=>$order_id),'fields'=>array('ProductPaymentResponse.*')));
        $productOrderDetailArr=$this->Product->find('all',array(
            'joins' => array(

				array(
					'table' => 'product_order_details',
					'alias' => 'ProductOrderDetail',
					'type' => 'LEFT',
					'conditions' => array(
						'Product.id = ProductOrderDetail.product_id'
					)
				),               

			),
            'conditions'=> array('ProductOrderDetail.order_id'=>$order_id),
            'fields'=>array('ProductOrderDetail.*','Product.*')));
           // debug($productOrderDetailArr);
      //  debug($paymentResponseDetailArr);
        
        $this->set('productOrderDetailArr',$productOrderDetailArr);
        $this->set('paymentResponseDetailArr',$paymentResponseDetailArr);
        
    }
    
    public function check_user_login(){
        
        $response = 0 ;
        $user_id=$this->Session->read('Auth.User.id');
        if(!empty($user_id)){
            $response = 1;
        }
        
        echo $response;
        exit;
        
    }
    
}