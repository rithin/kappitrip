<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ProductsController extends AppController {
	var $layout ="innerpage_layout";
	public $uses = array('Shop','Status','Product');
	//var $name="ProductCategories";
	// listing the product_categories here.
	public function beforeFilter()
	{
            parent::beforeFilter();
            //$this->Auth->allow();
	}
	public function view($id=""){
            //$this->autoRender=false;
            $restult_product_categories=$this->Product->find('all', array(
                'conditions' => array('status_id IN(1,2)'),
                'fields' => array('Product.*'),
                'order' => 'Product.created DESC'
            ));
            //debug($restult_product_categories);
            $active_flag="";$content_id="";
            if($id!=""){
                $restult_product_categories_arr=$this->edit($id);	
                if(count($restult_product_categories_arr)==0 || empty($restult_product_categories_arr)){
                    $this->set('active_flag',$active_flag="list");
                }else{
                    $content_id=$id;
                    $this->request->data = $restult_product_categories_arr[0];
                    $this->set('product_category_id',$this->request->data['Product']['id']);
                    $this->set('active_flag',$active_flag="add_edit");
                }
            }else{
                $this->set('active_flag',$active_flag="list");
            }
            $this->set('content_id',$content_id);		
            $this->set('restult_contents',$restult_product_categories);		
            $this->set('restult_status',$this->Status->find('list',array('fields'=>array('id','status'))));
	}
	
	public function save(){
		//debug($this->request->data);
		
		//die;
		if(!empty($this->request->data)){
			 $this->Product->SaveAll($this->request->data['Product']);
//			 $last_insert_id= $this->Content->getLastInsertID();
//			 if($last_insert_id==""){
//				$last_insert_id=$this->request->data['Content']['id'];
//			 }
			 /*
			 if($this->request->data['ProductCategory']['file']['name'] !="" ){
				 $this->request->data['ProductCategory']['id']=$last_insert_id;
				 $this->request->data['ProductCategory']['category_pic_name']=$this->request->data['ProductCategory']['file']['name'];
				 $this->request->data['ProductCategory']['category_pic_location']=DS.'uploads'.DS.'ProductCategories'.DS.$date_seperator.DS.$last_insert_id;
				 $this->ProductCategory->SaveAll($this->request->data['ProductCategory']);
			 }*/
		}
		$this->redirect(array('controller'=>'products','action'=>'view'));
	}
	
	public function edit($id=""){
            if($id!="")		
                    $restult_product_categories=$this->Product->find('all',array('conditions'=>array('Product.id'=>$id)));
            return $restult_product_categories;
	}
	
	public function editSubCategory($id=""){
		if($id!="")		
			$restult_product_sub_categories=$this->ProductSubCategory->find('all',array('conditions'=>array('ProductSubCategory.id'=>$id)));
			
		return $restult_product_sub_categories;
	}
	
	
	public function saveBrand(){
		//debug($this->request->data);
		
		//die;
		if(!empty($this->request->data)){
			 $this->ProductBrand->SaveAll($this->request->data['ProductBrand']);
			 $last_insert_id= $this->ProductBrand->getLastInsertID();
			 if($last_insert_id==""){
				$last_insert_id=$this->request->data['ProductBrand']['id'];
			 }
			 $date_seperator=date('Y-m');
			 /* copy uploaded file */
			 $dir = WWW_ROOT.DS.'uploads'.DS.'ProductBrands'.DS.$date_seperator.DS.$last_insert_id.DS;
			 if(!file_exists(WWW_ROOT.DS. 'uploads'.DS.'ProductBrands'.DS.$date_seperator)){
			   mkdir(WWW_ROOT.DS. 'uploads'.DS.'ProductBrands'.DS.$date_seperator,0777);
			 }
			  // Strip path information
			 $filename = $dir.basename($this->request->data['ProductBrand']['file']['name']); 
			 if(file_exists($dir) && is_dir($dir))
			 {
				move_uploaded_file($this->data['ProductBrand']['file']['tmp_name'],$filename);  
			 }
			 elseif(mkdir($dir,0777))
			 {
			 	move_uploaded_file($this->data['ProductBrand']['file']['tmp_name'],$filename);    
			 }
			 if($this->request->data['ProductBrand']['file']['name'] !="" ){
				 $this->request->data['ProductBrand']['id']=$last_insert_id;
				 $this->request->data['ProductBrand']['brand_logo']=$this->request->data['ProductBrand']['file']['name'];
				 $this->request->data['ProductBrand']['brand_logo_location']=DS.'uploads'.DS.'ProductBrands'.DS.$date_seperator.DS.$last_insert_id;
				//debug($this->request->data['ProductBrand']);			
				 $this->ProductBrand->SaveAll($this->request->data['ProductBrand']);
			 }
		}
		//die;
		$this->redirect(array('controller'=>'product_categories','action'=>'view_brands'));
	}
	public function editBrand($id=""){
		if($id!="")		
			$restult_product_brands=$this->ProductBrand->find('all',array('conditions'=>array('ProductBrand.id'=>$id)));
			
		return $restult_product_brands;
	}
        
        public function updatePassword(){
		$this->autoRender = false;		
		if(!empty($this->request->data)){
		$data['ProductCategory']['status_id']=$this->request->data['statusId'];
		$data['ProductCategory']['id']=$this->request->data['ProductCategory_id'];		
		$this->ProductCategory->SaveAll($data);
			return true;
		}else
		 	return false;
	}
        
        public function updateStatus(){
		$this->autoRender = false;		
		if(!empty($this->request->data)){
		$data['Product']['status_id']=$this->request->data['statusId'];
		$data['Product']['id']=$this->request->data['id'];		
		$this->Product->SaveAll($data);
			return true;
		}else
		 	return false;
	}
        
        public function updateBrandStatus(){
		$this->autoRender = false;		
		if(!empty($this->request->data)){
		$data['ProductBrand']['status_id']=$this->request->data['statusId'];
		$data['ProductBrand']['id']=$this->request->data['ProductCategory_id'];		
		$this->ProductBrand->SaveAll($data);
			return true;
		}else
		 	return false;
	}
}
?>