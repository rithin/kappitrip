<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageSubCategoryController extends AppController {

	var $layout ="default";
	public $uses = array('User','Amenity','HomestayAmenity','AmenityCategory','Location','City','SubCategory','Category');

	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
public function sub_category_list(){ 
		 $subcategory= $this->SubCategory->find('all',
      array(
        'fields'=>array('SubCategory.*','Category.*'),
        'joins'=>array(
            array(
              'table' => 'categories',
              'alias' => 'Category',
              'type'  => 'INNER',
              'conditions' => array(
                'SubCategory.category_id = Category.id'
              )
            ),
        ),
        'conditions'=>array('SubCategory.status'=>1),

    ));
     //debug( $amenities);
     //die;
     $this->set('subcategory',$subcategory);
}

public function add_edit($id=null){
  $category= $this->Category->find('list',array('fields'=>array('id','category_name')));

  if($id!=""){
	    	 $conditions=array('SubCategory.id'=>$id);
	         $this->request->data=$this->SubCategory->find('first', array(				
	            'conditions'=>$conditions,
				'fields' => array('SubCategory.*'),           
				'order' => 'SubCategory.created DESC'
			));
    }
    $this->set('category',$category);
}
   public function delete($id=""){      
        //$this->MasterBedType->delete($id);
   		$data['SubCategory']['id']=$id;
   		$data['SubCategory']['status']=3;
         $response=$this->SubCategory->SaveAll($data);
         $this->redirect(array('controller'=>'manage_sub_category','action'=>'sub_category_list'));
     }
  
 
  public function save(){    

 //debug($_POST['data']); 
  //die;    
        if($_POST!=''){
           
          // debug($_POST['data']); 
        	$_POST['data']['SubCategory']['status']=1;
         $response=$this->SubCategory->SaveAll($_POST['data']['SubCategory']);
            //die;
           $this->redirect(array('controller'=>'manage_sub_category','action'=>'sub_category_list'));
        }

      }  

    
}
?>