<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ReportsController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry','Host','Experience','InvoiceRemittance');

	public function beforeFilter()
	{
        parent::beforeFilter();
       // $this->Auth->allow();
	}
    public function booking_list(){ 
		$conditions=array();
        $filter_type="";
        if(!empty($_POST)){
            
           if($_POST['data']['Report']['host_id']!=""){	
               array_push($conditions,array('Homestay.host_id'=>$_POST['data']['Report']['host_id']));
           }
          if($_POST['data']['Report']['homestay_id']!=""){	
              array_push($conditions,array('Homestay.id'=>$_POST['data']['Report']['homestay_id']));
              $filter_type="homestay";
           }
          if($_POST['data']['Report']['experience_id']!=""){	
              array_push($conditions,array('Experience.id'=>$_POST['data']['Report']['experience_id']));
              $filter_type="experience";
           }
          if($_POST['data']['Report']['start_date']!="" && $_POST['data']['Report']['end_date']!=""){	
              $start_date=$_POST['data']['Report']['start_date'];
              $end_date=  $_POST['data']['Report']['end_date'] ;
              array_push($conditions,array(" ( BookingPaymentResponse.created > '$start_date' AND BookingPaymentResponse.created < '$end_date')"));
           }    
        }
        
        $this->BookingPaymentResponse->recursive=1;
        $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
        $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
        $this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
		$this->BookingPaymentResponse->bindModel(
	   							array('hasMany' => array(
												'InvoiceRemittance' => array(
													'className' => 'InvoiceRemittance'
												)
											)
									)
	   						);
	   $this->InvoiceRemittance->bindModel(
										array('belongsTo' => array(
												'BookingPaymentResponse' => array(
													'className' => 'BookingPaymentResponse'
												)
											)
										)
	   								);
        
		 $fields = array('OccupancyDetail.*','Homestay.*','BookingOrderDetail.*','BookingPaymentResponse.*','Experience.*', 'ExperienceBookingDetail.*');
		 $joins= array(
                    array(
                        'table' => 'homestays',
                        'alias' => 'Homestay',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'Homestay.id = BookingPaymentResponse.homestay_id'
                        )
                    ),
                    array(
                        'table' => 'experiences',
                        'alias' => 'Experience',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'Experience.id = BookingPaymentResponse.experience_id'
                        )
                    ),                    
                    array(
                        'table' => 'booking_order_details',
                        'alias' => 'BookingOrderDetail',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'BookingOrderDetail.order_id = BookingPaymentResponse.order_id'
                        )
				    ),
                     array(
                        'table' => 'occupancy_details',
                        'alias' => 'OccupancyDetail',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'OccupancyDetail.order_id = BookingOrderDetail.order_id'
                        )
				    ),
                     array(
                        'table' => 'experience_booking_details',
                        'alias' => 'ExperienceBookingDetail',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'ExperienceBookingDetail.order_id = BookingPaymentResponse.order_id'
                        )
				    ),
			 		array(
						'table' => 'invoice_remittances',
						'alias' => 'InvoiceRemittance',
						'type' => 'LEFT',
						'conditions' => array(
							'BookingPaymentResponse.id = InvoiceRemittance.booking_payment_response_id'
								)
						),
                
			);
		
		
		
//		$this->paginate = array(
//			'joins' =>$joins,
//			'conditions' => $conditions,
//			'limit' => 10,
//			'fields' =>$fields,
//            'group' => '`OccupancyDetail`.`order_id`',
//			'order' => 'BookingOrderDetail.created DESC'
//		);
        
         $bookingDetails= $this->BookingPaymentResponse->find('all', array(
                 'joins' =>$joins,
			     'conditions' => $conditions,
			     'fields' =>$fields,
                 'group' => '`BookingPaymentResponse`.`order_id`',
			     'order' => 'BookingPaymentResponse.created DESC'
            ));
        
     // debug($bookingDetails);
        $this->set('bookingDetails',$bookingDetails);	
        $this->set('filter_type',$filter_type);
        
        
        $hosts_arr=$this->Host->find('list',array('conditions'=>array('Host.status'=>1), 'fields'=>array('id','name')));
		$this->set('hosts_arr',$hosts_arr);	
        
        $homestay_arr=$this->Homestay->find('list',array('conditions'=>array('Homestay.status'=>1), 'fields'=>array('id','name')));
		$this->set('homestay_arr',$homestay_arr);	
        
        $experience_arr=$this->Experience->find('list',array('conditions'=>array('Experience.status'=>1), 'fields'=>array('id','name')));
		$this->set('experience_arr',$experience_arr);	
        
    }
   public function add_remittance(){
	   $this->layout=false; 
	   $payment_id=$_REQUEST['payment_id'];
	   $flag_type=$_REQUEST['flag_type'];
	   $booking_type_id=$_REQUEST['booking_type_id'];
	   $this->BookingPaymentResponse->recursive=1;
	   $this->BookingPaymentResponse->bindModel(
	   							array('hasMany' => array(
												'InvoiceRemittance' => array(
													'className' => 'InvoiceRemittance'
												)
											)
									)
	   						);
	   $this->InvoiceRemittance->bindModel(
										array('belongsTo' => array(
												'BookingPaymentResponse' => array(
													'className' => 'BookingPaymentResponse'
												)
											)
										)
	   								);
	   $invoice_details=$this->BookingPaymentResponse->find('all',array(
									'joins' => array(
										array(
											'table' => 'invoice_remittances',
											'alias' => 'InvoiceRemittance',
											'type' => 'LEFT',
											'conditions' => array(
												'BookingPaymentResponse.id = InvoiceRemittance.booking_payment_response_id'
												)
											),  
										),
		   							'conditions'=>array('BookingPaymentResponse.id'=>$payment_id),
									'group'=>array('BookingPaymentResponse.id')			 
									 )
								);
	  //debug($invoice_details);
	  $invoice_amount=$invoice_details[0]['BookingPaymentResponse']['amount'];
	  $old_remittance=array(); $total_remitted_amount=0; $outstanding_balance=0;
	   $agent_commission=$invoice_details[0]['BookingPaymentResponse']['agent_commission'];
	  if($agent_commission==0){
			$agent_commission= Configure::read('agent_commission_percentage');
	   }
	  
	  if(is_array($invoice_details[0]['InvoiceRemittance'])){		  
		  foreach($invoice_details[0]['InvoiceRemittance'] as $key=>$remittance){
			  $old_remittance[$key]['remitted_amount']=$remittance['remitted_amount'];
			  $old_remittance[$key]['remitted_date']=$remittance['remitted_date'];
			  $old_remittance[$key]['mode_of_payment']=$remittance['mode_of_payment'];
			  $total_remitted_amount+=$remittance['remitted_amount'];
		  }
	  }
	  $amount_for_coorgexpress=(($invoice_amount*$agent_commission)/100);
	  $amount_to_host=($invoice_amount-$amount_for_coorgexpress);
	  $outstanding_balance=($amount_to_host-$total_remitted_amount);
	   
	  $remittance_dtls['homestay_id']=$invoice_details[0]['BookingPaymentResponse']['homestay_id'];
	  $remittance_dtls['experience_id']=$invoice_details[0]['BookingPaymentResponse']['experience_id'];
	  $remittance_dtls['booking_payment_response_id']=$invoice_details[0]['BookingPaymentResponse']['id'];
	  $remittance_dtls['order_id']=$invoice_details[0]['BookingPaymentResponse']['order_id'];
	  $remittance_dtls['invoice_date']=date('Y-m-d', strtotime($invoice_details[0]['BookingPaymentResponse']['created']));
	   
	  $remittance_dtls['invoice_amount']=$invoice_amount;
	  $remittance_dtls['old_remittance']=$old_remittance;
	  $remittance_dtls['total_remitted_amount']=$total_remitted_amount;
	  $remittance_dtls['outstanding_balance']=$outstanding_balance;
	  $remittance_dtls['agent_commission']=number_format($amount_for_coorgexpress,2)." @ ".$agent_commission."%";
	 // debug($remittance_dtls);
	  $this->set('remittance_dtls',$remittance_dtls);  
   }
  public function saveRemittance(){
	  $this->autoRender=false;	
	  $data=$_REQUEST;	
	  $data['remitted_date']=date('Y-m-d',strtotime($_REQUEST['remittance_date']));
	  unset($_REQUEST['remittance_date']); unset($data['remittance_date']);
	 // debug($data);
	  //die;
	  if($this->InvoiceRemittance->saveAll($data)){
		  echo "Successfully submitted the remittance for the invoice ".$_REQUEST['order_id'];
	  }else{
		  echo "Remittance failed for the invoice ".$_REQUEST['order_id']." Please try after sometime.";
	  }
	  exit;
  }
   public function booking_invoice($order_id_enc=null,$homestay_id_enc=null){
        $this->layout=false; 
        $order_id=base64_decode($order_id_enc);
        $homestay_id=base64_decode($homestay_id_enc);
        $OccupancyDetailArr=$this->OccupancyDetail->find('first',array('conditions'=> array('OccupancyDetail.order_id'=>$order_id), 'fields'=>array('OccupancyDetail.*')));
        $HomestayDetailArr=$this->Homestay->find('first',array(
            'joins' => array(
				array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type' => 'LEFT',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),               

			),
            'conditions'=> array('Homestay.id'=>$homestay_id),
            'fields'=>array('Homestay.*','RoomPriceDetail.*')));
	   
        $BookingOrderDetailArr=$this->BookingOrderDetail->find('all',array(
            'joins' => array(
				array(
					'table' => 'booking_payment_responses',
					'alias' => 'BookingPaymentResponse',
					'type' => 'LEFT',
					'conditions' => array(
						'BookingOrderDetail.id = BookingPaymentResponse.booking_order_detail_id'
					)
				),               

			),
            'conditions'=> array('BookingOrderDetail.order_id'=>$order_id),
            'fields'=>array('BookingOrderDetail.*','BookingPaymentResponse.*')));
        
        $this->set('OccupancyDetailArr',$OccupancyDetailArr);
        $this->set('HomestayDetailArr',$HomestayDetailArr);
        $this->set('BookingOrderDetailArr',$BookingOrderDetailArr);
    }
	
	public function cancel_booking($order_id_enc,$homestay_id_enc){
		$this->layout=false; 
		$order_id=base64_decode($order_id_enc);
        $homestay_id=base64_decode($homestay_id_enc);
        $OccupancyDetailArr=$this->OccupancyDetail->find('first',array('conditions'=> array('OccupancyDetail.order_id'=>$order_id), 'fields'=>array('OccupancyDetail.*')));
		
		$this->OccupancyDetail->updateAll(
							array( 'OccupancyDetail.status' => 8 ),   //fields to update
							array( 'OccupancyDetail.order_id' => $order_id )  //condition
						);
		
		$this->BookingOrderDetail->updateAll(
							array( 'BookingOrderDetail.status' => 8 ),   //fields to update
							array( 'BookingOrderDetail.order_id' => $order_id )  //condition
						);
		
		$this->redirect(array('controller'=>'property_bookings','action'=>'booking_list'));
		
	}
	
   
   
    

    
}
?>