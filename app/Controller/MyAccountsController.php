<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class MyAccountsController extends AppController {
	var $layout ="default";//user_dashboard_layout
	public $uses = array('User','Role','Status','State','UserReset','UserDetail','BookingPaymentResponse','HomestayAmenity','HomestayFile', 'RoomPriceDetail','InvoiceRemittance','BookingOrderDetail','OccupancyDetail','ExperienceBookingDetail');
	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
	public function profile(){
		if(($this->Session->read('Auth.User.id')!="") && ($this->Session->read('Auth.User.id')>0)){
        	$logged_in_user_id=$this->Session->read('Auth.User.id');
        	$this->request->data=$this->User->find('first',array(
				'joins' => array(
							array(
								'table' => 'user_details',
								'alias' => 'UserDetail',
								'type'  => 'LEFT',
								'conditions' => array(
									'User.id = UserDetail.user_id'
								)
							),
					),				
				'conditions'=>array('User.id'=>$logged_in_user_id),
			'fields' => array('User.*','UserDetail.*')));			
		}
		//debug($this->request->data);
		$array_states = array(); 
        $array_states=$this->State->find('list',array('fields'=>array('id','state_name')));        
        $this->set('array_states',$array_states);
	}
	public function saveProfile(){    
        if(!empty($_POST['data']['User']['first_name'])){
      		$this->request->data['User']['role_id']=4;            
            $response=$this->User->SaveAll($this->request->data['User']);
       
            if($_POST['data']['User']['id']!="")
                $LastInsertID=$_POST['data']['User']['id'];
            else
               $LastInsertID=$this->User->getLastInsertID();
			
           	$this->request->data['UserDetail']['user_id']=$LastInsertID;  
			$this->request->data['UserDetail']['company_name']='';
			//debug($_FILES['data']);
			if(!empty($_FILES['data']['tmp_name']['User']['profile_pic'])){
				
				$tempFile = $_FILES['data']['tmp_name']['User']['profile_pic'];  
				$baseUploadPath = WWW_ROOT."uploads/profile_pics/";
				$fileName=""; $time=time();$targetFile="";
				$dbPath="uploads/profile_pics/".$LastInsertID; 
				$targetPath = WWW_ROOT."uploads/profile_pics/".$LastInsertID."/";
            	if(!(file_exists($baseUploadPath))){
					mkdir($baseUploadPath, 0777, true);
            	}  
				$this->deleteFolder($targetPath);
                if (file_exists($targetPath)=="") {
                    mkdir($targetPath, 0777, true); 
                    chmod($targetPath, 0777);
                }
                                
				if($_FILES['data']['type']['User']['profile_pic']=="image/jpeg"){
					 $targetFile =  $targetPath. $time.".jpg"; 
					 $fileName=$time.".jpg";
				}else if($_FILES['data']['type']['User']['profile_pic']=="image/png"){
					 $targetFile =  $targetPath. $time.".png";  
					 $fileName=$time.".png";
				}else if($_FILES['data']['type']['User']['profile_pic']=="image/gif"){
					 $targetFile =  $targetPath. $time.".gif";  
					 $fileName=$time.".gif";
				}else if($_FILES['data']['type']['User']['profile_pic']=="image/webp"){
					 $targetFile =  $targetPath. $time.".webp";  
					 $fileName=$time.".webp";
				}     
				move_uploaded_file($tempFile,$targetFile);            
				chmod($targetFile, 0777);
				$this->request->data['UserDetail']['profile_pic_name']=$fileName;
				$this->request->data['UserDetail']['profile_pic_path']=$dbPath;
			}
			 //debug($this->request->data['UserDetail']);
           $this->UserDetail->SaveAll($this->request->data['UserDetail']);
        //  debug($_FILES);
			//die;
        }
		 $this->redirect(array('controller'=>'my_accounts','action'=>'profile'));
    }
    public function deleteFolder($rootPath)
	{   
		foreach(new DirectoryIterator($rootPath) as $fileToDelete)
		{
			if($fileToDelete->isDot()) continue;
			if ($fileToDelete->isFile())
				unlink($fileToDelete->getPathName());
			if ($fileToDelete->isDir())
				deleteFolder($fileToDelete->getPathName());
		}
    	rmdir($rootPath);
	}
    public function settings($msg=null){
		if(($this->Session->read('Auth.User.id')!="") && ($this->Session->read('Auth.User.id')>0)){
        	$logged_in_user_id=$this->Session->read('Auth.User.id');
        	$this->request->data=$this->User->find('first',array(			
				'conditions'=>array('User.id'=>$logged_in_user_id),
			'fields' => array('User.id','User.username')));			
		}
	 	$this->set('msg',$msg);	
    }
	
	public function saveSettings(){
		if(!empty($this->request->data)){
			$this->User->SaveAll($this->request->data);			
		}
		 $this->redirect(array('controller'=>'my_accounts','action'=>'settings/success'));
	}
	
	public function bookings(){
		$conditions=array();
		array_push($conditions,array('BookingPaymentResponse.user_id'=>$this->Session->read('Auth.User.id')));
		
		$this->BookingPaymentResponse->recursive=1;
        $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
        $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
        $this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
		$this->BookingPaymentResponse->bindModel(
	   							array('hasMany' => array(
												'InvoiceRemittance' => array(
													'className' => 'InvoiceRemittance'
												)
											)
									)
	   						);
	   $this->InvoiceRemittance->bindModel(
										array('belongsTo' => array(
												'BookingPaymentResponse' => array(
													'className' => 'BookingPaymentResponse'
												)
											)
										)
	   								);
        
		 $fields = array('OccupancyDetail.*','Homestay.*','BookingOrderDetail.*','BookingPaymentResponse.*','Experience.*', 'ExperienceBookingDetail.*','ExperienceSlot.*');
		 $joins= array(
                    array(
                        'table' => 'homestays',
                        'alias' => 'Homestay',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'Homestay.id = BookingPaymentResponse.homestay_id'
                        )
                    ),
                    array(
                        'table' => 'experiences',
                        'alias' => 'Experience',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'Experience.id = BookingPaymentResponse.experience_id'
                        )
                    ),                    
                    array(
                        'table' => 'booking_order_details',
                        'alias' => 'BookingOrderDetail',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'BookingOrderDetail.order_id = BookingPaymentResponse.order_id'
                        )
				    ),
                     array(
                        'table' => 'occupancy_details',
                        'alias' => 'OccupancyDetail',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'OccupancyDetail.order_id = BookingOrderDetail.order_id'
                        )
				    ),
                     array(
                        'table' => 'experience_booking_details',
                        'alias' => 'ExperienceBookingDetail',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'ExperienceBookingDetail.order_id = BookingPaymentResponse.order_id'
                        )
				    ),
			 		array(
                        'table' => 'experience_slots',
                        'alias' => 'ExperienceSlot',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'ExperienceBookingDetail.experience_slot_id = ExperienceSlot.id'
                        )
				    ),
			 		array(
						'table' => 'invoice_remittances',
						'alias' => 'InvoiceRemittance',
						'type' => 'LEFT',
						'conditions' => array(
							'BookingPaymentResponse.id = InvoiceRemittance.booking_payment_response_id'
								)
						),
                
			);
		
		
         $bookingDetails= $this->BookingPaymentResponse->find('all', array(
                 'joins' =>$joins,
			     'conditions' => $conditions,
			     'fields' =>$fields,
                 'group' => '`BookingPaymentResponse`.`order_id`',
			     'order' => 'BookingPaymentResponse.created DESC'
            ));
        
     // debug($bookingDetails);
        $this->set('bookingDetails',$bookingDetails);	
	}
	
	public function cancel_stay_booking(){
		//debug($this->request->data);
		$order_id=$this->request->data['BookingPaymentResponse']['order_id'];
		$this->BookingPaymentResponse->updateAll(array('BookingPaymentResponse.status'=>13, 'BookingPaymentResponse.canceled_date'=>'CURRENT_TIMESTAMP'),array('BookingPaymentResponse.order_id'=>$order_id));
		$this->BookingOrderDetail->updateAll(array('BookingOrderDetail.status'=>13), array('BookingOrderDetail.order_id'=>$order_id));
		$this->OccupancyDetail->updateAll(array('OccupancyDetail.status'=>13), array('OccupancyDetail.order_id'=>$order_id));
		 $this->redirect(array('controller'=>'my_accounts','action'=>'bookings'));
	}
	
	public function cancel_experience_booking(){
		$order_id=$this->request->data['BookingPaymentResponse']['order_id'];
		$this->BookingPaymentResponse->updateAll(array('BookingPaymentResponse.status'=>13, 'BookingPaymentResponse.canceled_date'=>'CURRENT_TIMESTAMP'),array('BookingPaymentResponse.order_id'=>$order_id));
		$this->ExperienceBookingDetail->updateAll(array('ExperienceBookingDetail.status'=>13), array('ExperienceBookingDetail.order_id'=>$order_id));
		 $this->redirect(array('controller'=>'my_accounts','action'=>'bookings'));
	}
	
}