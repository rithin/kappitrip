<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageCoffeeShopsController extends AppController {

	var $layout ="default";
	public $uses = array('User','CoffeeShops','CoffeeShopImage','State','CoffeeType');

	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
        
        public function coffee_shop_list(){
            
        	
        
        }
    
    public function ajax_coffee_shop_list(){
            
        $this->layout = false;
        
        $coffeeshopList=$this->CoffeeShops->find('all', array(
			'joins' => array(

				array(
					'table' => 'coffee_shop_images',
					'alias' => 'CoffeeShopImage',
					'type' => 'LEFT',
					'conditions' => array(
						'CoffeeShops.id = CoffeeShopImage.coffee_shop_id'
					)
				),
                                
                                

			),
			'fields' => array('CoffeeShops.*','CoffeeShopImage.uploaded_img_name','CoffeeShopImage.img_path'),
            'group'=>'CoffeeShops.id',
			'order' => 'CoffeeShops.created DESC'
		));
        
        
                $list_view_array = array();
                
                foreach($coffeeshopList as $key=>$coffeeshop_data){
                    
                   
                    
                    $list_view_array['data'][$key]['coffeeshop_name'] = $coffeeshop_data['CoffeeShops']['shop_name'];
                    $list_view_array['data'][$key]['brand_name'] = $coffeeshop_data['CoffeeShops']['brand_name'];
                    $list_view_array['data'][$key]['city'] = $coffeeshop_data['CoffeeShops']['city'];
                    $list_view_array['data'][$key]['actions'] = "<a href='".Configure::read('app_root_path')."manage_coffee_shops/add/".$coffeeshop_data['CoffeeShops']['id']."'>Edit</a>".
                                                                "&nbsp;&nbsp;<a href='".Configure::read('app_root_path')."manage_coffee_shops/delete/".$coffeeshop_data['CoffeeShops']['id'].">Delete</a>";                
                    
                    }
                
                echo json_encode($list_view_array);
                
                exit;
        	
        
    }
    
    public function delete($id=""){      
        $this->Product->delete($id);
        $this->ProductFile->deleteAll(array('CoffeeShopImage.	coffee_shop_id' => $id), false);
        $this->redirect(array('controller'=>'manage_coffee_shops','action'=>'coffee_shop_list'));
    }
     
    public function deleteFile($id=""){
         
         $data= $this->CoffeeShopImage->find('first',array('conditions'=>array('CoffeeShopImage.id'=>$id)));
         $this->CoffeeShopImage->delete($id);
         
         $uploadedFile = WWW_ROOT.$data['CoffeeShopImage']['img_path'].'/'.$data['CoffeeShopImage']['uploaded_img_name'];
         unlink($uploadedFile);
         $resizedFile = WWW_ROOT.$data['CoffeeShopImage']['img_path'].'/'.$data['CoffeeShopImage']['resized_img_name'];
         unlink($resizedFile);
        
         $thumbnailFile = WWW_ROOT.$data['CoffeeShopImage']['img_path'].'/'.$data['CoffeeShopImage']['thumbnail_img_name'];
         unlink($thumbnailFile);
         
         $this->redirect(array('controller'=>'manage_coffee_shops','action'=>'coffee_shop_list'));
    }
    
    public function add($id=''){
       
        
       
        
        $coffeeshopImages = array();
        
        $array_states = array(); 
        $array_states=$this->State->find('list',array('fields'=>array('id','state_name')));        
        $this->set('array_states',$array_states);
        
        
       
        if($id!="")	{	
            $this->request->data= $this->CoffeeShops->find('first', array(
			'joins' => array(

			   array(
					'table' => 'coffee_shop_images',
					'alias' => 'CoffeeShopImage',
					'type' => 'LEFT',
					'conditions' => array(
						'CoffeeShops.id = CoffeeShopImage.coffee_shop_id'
					)
				),
                

			),
            'conditions' => array('CoffeeShops.id'=>$id),
			'fields' => array('CoffeeShops.*','CoffeeShopImage.uploaded_img_name','CoffeeShopImage.img_path'),
			'order' => 'CoffeeShops.created DESC'
		    ));
            
           $this->CoffeeShopImage->unbindModel(array('belongsTo' => array('CoffeeShops')));
           $coffeeshopImages = $this->CoffeeShopImage->find('all',array('conditions'=>array('CoffeeShopImage.coffee_shop_id'=>$id)));  
        }
        $this->set('coffeeshopImages',$coffeeshopImages);
        
        
    }
   
    public function fileUpload(){
        //session_start();
        
        if(!empty($_SESSION['coffe_shop_images'])){
            unset($_SESSION['coffe_shop_images']);
        }
        
        $this->autoRender=false;        
        $ds = DIRECTORY_SEPARATOR;  
        $storeFolder = 'uploads/temp_files';
        if (!file_exists($storeFolder)) {
            mkdir($storeFolder, 0777, true);
            chmod($storeFolder, 0777);
        }
        $storeFolder = 'uploads/temp_files/shop_images';
        if (!file_exists($storeFolder)) {
            mkdir($storeFolder, 0777, true);
            chmod($storeFolder, 0777);
        }
        $request=1;
        if(isset($_POST['request'])){ 
            $request = $_POST['request'];
	}
        if ((!empty($_FILES))&& $request==1) {            
             
            for($i=0; $i< count($_FILES['file']['name']); $i++){             
				$tempFile = $_FILES['file']['tmp_name'][$i];         
				$targetPath = WWW_ROOT.$storeFolder . $ds;  
				$fileName=""; 
                                $time=time();
                                
				if($_FILES['file']['type'][$i]=="image/jpeg"){
					 $targetFile =  $targetPath. $time."_".$i.".jpg"; 
					 $fileName=$time."_".$i.".jpg";
				}else if($_FILES['file']['type'][$i]=="image/png"){
					 $targetFile =  $targetPath. $time."_".$i.".png";  
					 $fileName=$time."_".$i.".png";
				}else if($_FILES['file']['type'][$i]=="image/gif"){
					 $targetFile =  $targetPath. $time."_".$i.".gif";  
					 $fileName=$time."_".$i.".gif";
				}else if($_FILES['file']['type'][$i]=="image/webp"){
					 $targetFile =  $targetPath. $time."_".$i.".webp";  
					 $fileName=$time."_".$i.".webp";
				}else{
					$targetFile=$targetPath. $_FILES['file']['name'][$i]; 
					$fileName= $time."_".$i.$_FILES['file']['name'][$i];
					//echo "Error. Invalid file type..!";
				}
				
                                //$_FILES["file"]["name"][$i]=$fileName;
                            
                                
				move_uploaded_file($tempFile,$targetFile);            
				chmod($targetFile, 0777);
				$arrayImg['file']=$targetFile;
				$arrayImg['name']=$fileName;
                                $_SESSION['coffe_shop_images'][$time][$i]=$arrayImg;
                                
                                //debug($_SESSION['product_images']);
			   // array_push($_SESSION['room_images'],$arrayImg);
			}
            
        }else if($request==2){
			$nameArr=explode(".",$_POST['name']);
			$key=$nameArr[0];
			$targetDeletedFile= $_SESSION['coffe_shop_images'][$key]['file'];
			unlink(@$targetDeletedFile);
			unset($_SESSION['coffe_shop_images'][$key]);
			//debug($_SESSION);
		}

      }
    public function save(){        
        if($_POST!=''){
            $baseUploadPath = WWW_ROOT."uploads/coffee_shop_pics/";
            //debug($_POST['data']);
            //exit;
            
            $_POST['data']['CoffeeShops']['status'] = 1;
            
            
            
            $response=$this->CoffeeShops->SaveAll($_POST['data']['CoffeeShops']);
            
            $folderPath="uploads/coffee_shop_pics/";
            if($_POST['data']['CoffeeShops']['id']!="")
                $LastInsertID=$_POST['data']['CoffeeShops']['id'];
           else
               $LastInsertID=$this->CoffeeShops->getLastInsertID();
            
         if(count($_SESSION['coffe_shop_images'])>0){
            $targetPath = $baseUploadPath.$LastInsertID;   
            $dbPath=$folderPath.$LastInsertID; 
            
            //debug($dbPath);
            if(!(file_exists($baseUploadPath))){
                mkdir($baseUploadPath, 0777, true);
            }
            
            if($response==true){               
                if (file_exists($targetPath)=="") {
                    mkdir($targetPath, 0777, true); 
                    chmod($targetPath, 0777);
                }
				//debug($_SESSION);
				//die;
                
                
                if(isset($_SESSION['coffe_shop_images']) && count($_SESSION['coffe_shop_images'])>0){
                    //Moving Files to orginal directory.
                    foreach($_SESSION['coffe_shop_images'] as $key=>$image_item){
                        
                        foreach($image_item as $k=>$item){
                        
                        $targetFile="";
                        
                        
                        //chmod($targetPath, 0777);
                        $targetFile=$targetPath."/".$item['name'];  
                        $sourceFile=$item['file'];
                        
                  
                                        
                        system("mv $sourceFile $targetPath");
                        
                        
                        $exploding = explode(".",$sourceFile);
                        $ext = end($exploding);
                        
                        $filename = $targetFile;
                        $resizedFile = $dbPath."/"."resized_".$item['name'];
                        $resizedFileName = "resized_".$item['name'];
                        
                        $imgData = $this->resize_image($filename, 570, 625);
                        $this->convert_image($imgData,$resizedFile,$ext);
                        
                        //imagejpeg($imgData, $resizedFile);
                        chmod($resizedFile, 0777);
                        
                        /*$litingImgFile = $dbPath."/"."listing_".$item['name'];
                        $litingImgFileName = "listing_".$item['name'];
                        
                        $listingImgData = $this->resize_image($filename, 370,315,true);
                        $this->convert_image($listingImgData,$litingImgFile,$ext);
                        
                        //imagejpeg($imgData, $resizedFile);
                        chmod($litingImgFile, 0777);*/
                        
                    
                        $thumbnailFile = $dbPath."/"."thumbnail_".$item['name'];
                        $thumbnailFilename = "thumbnail_".$item['name'];
                        $thumbimgData = $this->resize_image($resizedFile, 170, 146);
                        $this->convert_image($thumbimgData,$thumbnailFile,$ext);
                        //imagejpeg($thumbimgData, $thumbnailFile);
                        chmod($thumbnailFile, 0777);
                        
                        $coffeeshopfiles['coffee_shop_id']=$LastInsertID;
                        $coffeeshopfiles['status']=1;
                        $coffeeshopfiles['img_path']=$dbPath;
                        $coffeeshopfiles['uploaded_img_name']=$item['name'];
                        $coffeeshopfiles['resized_img_name']=$resizedFileName;
                        //$productfiles['listing_img_name']=$litingImgFileName;
                        $coffeeshopfiles['thumbnail_img_name']= $thumbnailFilename;
                        
                        $this->CoffeeShopImage->SaveAll($coffeeshopfiles);
                        
                    }
                                        
                    }
                    
                 unset( $_SESSION['coffe_shop_images']);     
               }
            }
            
             
          }
          
         
           $this->redirect(array('controller'=>'manage_coffee_shops','action'=>'coffee_shop_list'));
        }

      }
      
      
    function resize_image($file, $w, $h, $crop=false) {
          
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }
    
        //Get file extension
        $exploding = explode(".",$file);
        $ext = end($exploding);
    
        switch($ext){
            case "png":
                $src = imagecreatefrompng($file);
                break;
            case "jpeg":
            case "jpg":
                $src = imagecreatefromjpeg($file);
                break;
            case "gif":
                $src = imagecreatefromgif($file);
                break;
            default:
                $src = imagecreatefromjpeg($file);
                break;
        }
    
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }
    
    
    function convert_image($src_img,$convert_img,$ext){
        
        switch($ext){
            case "png":
                imagepng($src_img,$convert_img);
                break;
            case "jpeg":
            case "jpg":
                imagejpeg($src_img,$convert_img);
                break;
            case "gif":
                imagegif($src_img,$convert_img);
                break;
            default:
                imagejpeg($src_img,$convert_img);
                break;
        }
        
        
    }
    
      
}
?>