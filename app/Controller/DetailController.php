<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class DetailController extends AppController {
	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','Product','Category','SubCategory','ProductFile','Experience','ExperienceFile','ExperienceLanguage','ExperienceType','ExperienceSlot','ExperienceBookingDetail','RoomType','BookingEnquiry','EnquiryHostResponse','ExperienceBlocking','TrailType');
	
	var $components = array('Encryption','General','Cookie');
	
	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();	
		
		
	}
	public function index($unique_url=null){	
		$browserInfo=$this->General->getBrowser();
		$browserName=$browserInfo['name'];
		$cookie_value=md5(uniqid(rand(), true))."_".$browserName;
		$array_cookie_data=array();
		
		$identity= $this->Cookie->read('0.know_the_bae.identity');
		if($identity==""){	
			$identity=array('know_the_bae'=>array('identity'=>$cookie_value));
			array_push($array_cookie_data,$identity);
			//$array_cookie_data['know_the_bae']['identity']=$cookie_value;
			//$this->Cookie->write('',$array_cookie_data);
			$this->Cookie->write('.know_the_bae.identity',$cookie_value);
		}else{
			if($this->Session->read('Auth.User.id')){
				//$array_cookie_data['know_the_bae']['user']=array('name'=>$this->Session->read('Auth.User.username'),'id'=>$this->Session->read('Auth.User.id'),'role'=>$this->Session->read('Auth.User.role'));
				$this->Cookie->write('.know_the_bae.identity',$identity);
				$this->Cookie->write('.userInfo.user.name',$this->Session->read('Auth.User.username'));
				$this->Cookie->write('.userInfo.user.id',$this->Session->read('Auth.User.id'));
				$this->Cookie->write('.userInfo.user.role',$this->Session->read('Auth.User.role'));
			
			}else{
				//$userData=array('user'=> array('name'=>'UNKN_USR','id'=>0,'role'=>'GUEST'));
				//array_push($array_cookie_data,$userData);
							
				$this->Cookie->write('.know_the_bae.identity',$identity);
				$this->Cookie->write('.userInfo.user.name','UNKN_USR');
				$this->Cookie->write('.userInfo.user.id',0);
				$this->Cookie->write('.userInfo.user.id','GUEST');
			}
			//$this->Cookie->write('',$array_cookie_data);
		}

        $conditions=array();
        if($unique_url !=""){ 
          // $stay_name= str_replace('-'," ",$stay_name);
           $this->Homestay->recursive=-1;
           $stayDetail=$this->Homestay->find('first',array('conditions'=>array("seo_unique_url LIKE '%$unique_url%' "),'fields'=>array('Homestay.*')));
            
            if(count($stayDetail)==0){ // redirecting back to listing page if someone types a wrong url.
                $this->redirect('/handpicked-stays');
            }
            
            $searchId=$stayDetail['Homestay']['id'];
            $conditions=array("Homestay.id "=> $searchId,'RoomPriceDetail.status'=>1);
			
			@$homestay_id=base64_decode($_REQUEST['searchId']);
		 	@$name=$stayDetail['Homestay']['name'];
		 	@$unique_email_code=$_REQUEST['unique_email_code'];
		 	@$enquiry_host_response_id=$_REQUEST['enquiry_host_response_id'];
		 	@$booking_enquiry_id=$_REQUEST['booking_enquiry_id'];
		 	@$enquiry_accept_reject=$_REQUEST['enquiry_accept_reject'];
			$enqryDtls=array();
			if($booking_enquiry_id!=""){
				
				$enqryDtls=$this->BookingEnquiry->query("SELECT * FROM `booking_enquiries` as BookingEnquiry WHERE BookingEnquiry.`id`=$booking_enquiry_id");				
			//	debug($enqryDtls);
			}
			if(count(@$enqryDtls)>0){
				@$this->set('enquiry_accept_reject',"accepted");
			}else{
				@$this->set('enquiry_accept_reject',"");
			}
			
         }
		
		 
		
//		 $parameters="?searchId=".base64_encode($item['Homestay']['id'])."&name=".$homestayDetails['Homestay']['name']."&unique_email_code=".$unique_email_code."&enquiry_host_response_id=".$lastInsertEnquiryResponseID."&booking_enquiry_id=".$lastInsertBookingEngquiryID."&enquiry_accept_reject=accepted";
//		
		
         $this->Homestay->recursive=2;
         $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
         $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
         $this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
         $homestayDetails=$this->Homestay->find('first', array(
			'joins' => array(
				array(
					'table' => 'homestay_files',
					'alias' => 'HomestayFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayFile.homestay_id'
					)
				),
                array(
					'table' => 'homestay_amenities',
					'alias' => 'HomestayAmenity',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayAmenity.homestay_id'
					)
				),
                array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id','RoomPriceDetail.status'=>1
					)
				),				 
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Homestay.location_id'
					)
				),
				array(
					'table' => 'deals',
					'alias' => 'Deal',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = Deal.homestay_id','Deal.status=1 ','CURDATE() between `Deal.start_date` and `Deal.end_date`',"Deal.homestay_id=$searchId"
					)
				),

			),
            'conditions'=>$conditions,
			'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path','Location.name', 'Location.latitude', 'Location.longitude','Deal.*'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.created DESC'
		));
		$bedroomDetails=$this->BedroomDetail->find('all',array(
			'joins'=>array(
				array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						'BedroomDetail.room_price_detail_id = RoomPriceDetail.id'
					)
				),
			),
			'conditions'=>array('BedroomDetail.homestay_id'=>$searchId,'RoomPriceDetail.status'=>1),
			'fields' => array('BedroomDetail.*','RoomPriceDetail.*'),
		
		));
		//$log1 = $this->BedroomDetail->getDataSource()->getLog(false, false); 
//debug($log1);
		//debug($conditions);		
	//debug($homestayDetails);		
		$no_of_bed=0;
		foreach($bedroomDetails as $dtlRoom){
			$no_of_bed+=($dtlRoom['BedroomDetail']['no_of_bed']+$dtlRoom['RoomPriceDetail']['extra_bed']);			
		}
		
		
        $this->set('homestayDetails',$homestayDetails);	
		$no_of_guest=0;
		foreach($homestayDetails['RoomPriceDetail'] as $dtls){
			$no_of_guest+=($dtls['no_of_bed']+$dtls['extra_bed']);
		}
		
		$roomPriceDetails=$this->RoomPriceDetail->find('all',array(				
				'conditions'=>array("RoomPriceDetail.homestay_id"=>$searchId, "RoomPriceDetail.status"=>1 ),
				'fields'=>array('RoomPriceDetail.*')));
		
		
		$this->set('no_of_rooms',count($roomPriceDetails));
		$this->set('no_of_guest',$no_of_guest);
		$this->set('no_of_bed',$no_of_bed);

		
        unset($homestayDetails['HomestayFile']['file_name']);
        unset($homestayDetails['HomestayFile']['file_path']);
      //  debug($homestayDetails);  
        //finding the current month availability details.
		$disableDays=$this->findAvailability('disableDays',$searchId,$homestayDetails['Homestay']['no_of_bedrooms']);
 
        $this->set('disableDays',$disableDays);
        $this->set('monthlyOccupancys',$this->findAvailability('monthly',$searchId,$homestayDetails['Homestay']['no_of_bedrooms']));
		$offer_text="";
        if($homestayDetails['Deal']['id']!=''){
			if($homestayDetails['Deal']['offer_type']=='percentage')
				$offer_text="-".$homestayDetails['Deal']['offer_value']."% OFF from ".date('jS-M',strtotime($homestayDetails['Deal']['start_date']))." to ".date('jS-M',strtotime($homestayDetails['Deal']['end_date']));
			else
				$offer_text="-".$homestayDetails['Deal']['offer_value']."Rs OFF from ".date('jS-M',strtotime($homestayDetails['Deal']['start_date']))." to ".date('jS-M',strtotime($homestayDetails['Deal']['end_date']));
		}
		$page_title=$homestayDetails['Homestay']['name']." ".$offer_text;
        $this->set('page_title',$page_title);
		$this->set('meta_title',$homestayDetails['Homestay']['meta_title']);
		$this->set('meta_keyword',$homestayDetails['Homestay']['meta_keyword']);
		$this->set('meta_description',$homestayDetails['Homestay']['meta_description']);
	}
    
  public function experience($unique_url=null){
       //debug($_SESSION);
        $conditions=array();
        if($unique_url!=""){    
           $this->Experience->recursive=-1;
           $exprDetail=$this->Experience->find('first',array('conditions'=>array("seo_unique_url LIKE '%$unique_url%' "),'fields'=>array('Experience.*')));
            
            if(count($exprDetail)==0){ // redirecting back to listing page if someone types a wrong url.
                $this->redirect('/handpicked-experiences');
            }
            
            $searchId=$exprDetail['Experience']['id'];           
            $conditions=array("Experience.id "=> $searchId);            
            //$conditions=array("ExperienceSlot.date "=> $today);            
         }
         $today=date('Y-m-d');
         $this->Experience->recursive=2;
         $this->Experience->unbindModel(array('hasMany' => array('ExperienceSlot')));
         $this->ExperienceFile->unbindModel(array('belongsTo' => array('Experience')));
         $this->ExperienceLanguage->unbindModel(array('belongsTo' => array('Experience')));
         $this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));        
         unset($this->ExperienceSlot->virtualFields['no_of_slots']);
         $experienceDetails=$this->Experience->find('first', array(
			'joins' => array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),
                array(
					'table' => 'trail_types',
					'alias' => 'TrailType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.trail_type_id = TrailType.id'
					)
				),
                array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Experience.location_id'
					)
				),
				array(
					'table' => 'deals',
					'alias' => 'Deal',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = Deal.experience_id','Deal.status=1 ','CURDATE() between `Deal.start_date` and `Deal.end_date`',"Deal.experience_id=$searchId"
					)
				),

			),
            'conditions'=>$conditions,
			'fields' => array('Experience.*','TrailType.*','Location.name','Location.latitude', 'Location.longitude','Deal.*' ),
             //,'ExperienceFile.file_name','ExperienceFile.file_path', 'ExperienceLanguage.*', 'ExperienceType.*','ExperienceSlot.*'
            'group' => '`Experience`.`id`',
			'order' => 'Experience.created DESC'
		));
        
        
		
        $this->set('experienceDetails',$experienceDetails);	
        unset($experienceDetails['ExperienceFile']['file_name']);
        unset($experienceDetails['ExperienceFile']['file_path']);
    //  debug($experienceDetails);  
        
        $experienceSlots=$this->ExperienceSlot->query("select ExperienceSlot.* from  experience_slots ExperienceSlot WHERE ExperienceSlot.experience_id=$searchId and ExperienceSlot.status=1");
        
        
        foreach($experienceSlots as $keyID=>$slot){
            $experience_id=$slot['ExperienceSlot']['experience_id'];
            $experience_slot_id=$slot['ExperienceSlot']['id'];
            $no_of_slots=$slot['ExperienceSlot']['no_of_people'];
           // $date=$slot['ExperienceSlot']['date'];           
            $experienceBookingDtls=$this->ExperienceBookingDetail->query("SELECT SUM(no_of_person) as no_of_bookings FROM `experience_booking_details` WHERE `experience_slot_id`=$experience_slot_id");
          //  debug($experienceBookingDtls);
            $no_of_bookings=$experienceBookingDtls[0][0]['no_of_bookings'];            
            $experienceSlots[$keyID]['ExperienceSlot']['slotCount']= $no_of_slots;
            $experienceSlots[$keyID]['ExperienceSlot']['freeSlot']=($no_of_slots-$no_of_bookings);
            
        }
		
		
		$datesArr = $this->experienceBlockDates($searchId);
		//debug($datesArr);
		$disabledDates=array();
		foreach($datesArr as $dateSub){
			foreach($dateSub as $date){
				$disabledDates[]=date('d-m-Y',strtotime($date));
			}
		}
		//debug($disabledDates); 
		$this->set('disabledDates', $disabledDates);
        $this->set('experienceSlots',$experienceSlots);	
	    $offer_text="";	
      	if($experienceDetails['Deal']['id']!=''){
			if($experienceDetails['Deal']['offer_type']=='percentage')
				$offer_text="-".$experienceDetails['Deal']['offer_value']."% OFF from ".date('jS-M',strtotime($experienceDetails['Deal']['start_date']))." to ".date('jS-M',strtotime($experienceDetails['Deal']['end_date']));
			else
				$offer_text="-".$experienceDetails['Deal']['offer_value']."Rs OFF from ".date('jS-M',strtotime($experienceDetails['Deal']['start_date']))." to ".date('jS-M',strtotime($experienceDetails['Deal']['end_date']));
		}
		$page_title=$experienceDetails['Experience']['name']." ".$offer_text;
        $this->set('page_title',$page_title);
		$this->set('meta_title',$experienceDetails['Experience']['meta_title']);
		$this->set('meta_keyword',$experienceDetails['Experience']['meta_keyword']);
		$this->set('meta_description',$experienceDetails['Experience']['meta_description']);
    }
	
	public function experienceBlockDates($experience_id=null){
		
		$disableDates=array();
        $experienceBlockingData=$this->ExperienceBlocking->find('all', array('conditions'=>array( 'ExperienceBlocking.experience_id'=>$experience_id),'fields'=>array('start_date' ,'end_date'), 'group'=>'start_date')); 
      //debug($experienceBlockingData);
		//die;
        foreach($experienceBlockingData as $key=>$data){
            $start_date=$data['ExperienceBlocking']['start_date'];
			$end_date=$data['ExperienceBlocking']['end_date'];	
			$datearr=$this->splitDates($start_date, $end_date);
			array_push($disableDates,$datearr);
        }
		//debug($disableDates);
		//die;
		return $disableDates;
	}
    
    public function experience_availability(){
        $this->layout=false;        
        $today=date('Y-m-d'); 
        $detailJson=json_decode($_REQUEST['detailJson']);    
		$checkindate=$detailJson->checkindate;
        $adultCount=$detailJson->adultCount;
        $experience_id=$detailJson->experience_id;
        $experienceSlots=$this->ExperienceSlot->query("select ExperienceSlot.* from  experience_slots ExperienceSlot WHERE ExperienceSlot.experience_id=$experience_id and ExperienceSlot.status=1 ");
        foreach($experienceSlots as $keyID=>$slot){
            $experience_id=$slot['ExperienceSlot']['experience_id'];
            $experience_slot_id=$slot['ExperienceSlot']['id'];
			
			$min_guest=$slot['ExperienceSlot']['min_guest'];
			$no_of_people=$slot['ExperienceSlot']['no_of_people'];
			$price=$slot['ExperienceSlot']['price'];
			
			//if no of requested guest are less than min guest the price will automatically increase
			$final_price=$this->findFinalExperiencePrice($adultCount,$min_guest,$price);
			
            $no_of_slots=$slot['ExperienceSlot']['no_of_people'];                  
            $experienceBookingDtls=$this->ExperienceBookingDetail->query("SELECT SUM(no_of_person) as no_of_bookings FROM `experience_booking_details` WHERE `experience_slot_id`=$experience_slot_id");			
            //debug($experienceBookingDtls);
            $no_of_bookings=$experienceBookingDtls[0][0]['no_of_bookings'];       
			$experienceSlots[$keyID]['ExperienceSlot']['final_price']= $final_price;// final per head charges
			$experienceSlots[$keyID]['ExperienceSlot']['no_of_people']= $no_of_people;
			$experienceSlots[$keyID]['ExperienceSlot']['min_guest']= $min_guest;
            $experienceSlots[$keyID]['ExperienceSlot']['slotCount']= $no_of_slots;
            $experienceSlots[$keyID]['ExperienceSlot']['freeSlot']=($no_of_slots-$no_of_bookings);
            $experienceSlots[$keyID]['ExperienceSlot']['no_of_required_slots']= $adultCount;
			$experienceSlots[$keyID]['ExperienceSlot']['checkindate']= $checkindate;
            
        }
	
     //  debug($experienceSlots);
        $this->set('experienceSlots',$experienceSlots);	
    }

	public function slot_availability(){
        $this->layout=false;        
        $today=date('Y-m-d'); 
        $detailJson=json_decode($_REQUEST['detailJson']);    
		$checkindate=$detailJson->checkindate;
        $adultCount=$detailJson->adultCount;
        $experience_id=$detailJson->experience_id;
        $experienceSlots=$this->ExperienceSlot->query("select ExperienceSlot.* from  experience_slots ExperienceSlot WHERE ExperienceSlot.experience_id=$experience_id and ExperienceSlot.status=1 ");
        foreach($experienceSlots as $keyID=>$slot){
            $experience_id=$slot['ExperienceSlot']['experience_id'];
            $experience_slot_id=$slot['ExperienceSlot']['id'];
			
			$min_guest=$slot['ExperienceSlot']['min_guest'];
			$no_of_people=$slot['ExperienceSlot']['no_of_people'];
			$price=$slot['ExperienceSlot']['price'];
			
			//if no of requested guest are less than min guest the price will automatically increase
			$final_price=$this->findFinalExperiencePrice($adultCount,$min_guest,$price);
			
            $no_of_slots=$slot['ExperienceSlot']['no_of_people'];                  
            $experienceBookingDtls=$this->ExperienceBookingDetail->query("SELECT SUM(no_of_person) as no_of_bookings FROM `experience_booking_details` WHERE `experience_slot_id`=$experience_slot_id");			
            //debug($experienceBookingDtls);
            $no_of_bookings=$experienceBookingDtls[0][0]['no_of_bookings'];       
			$experienceSlots[$keyID]['ExperienceSlot']['final_price']= $final_price;// final per head charges
			$experienceSlots[$keyID]['ExperienceSlot']['no_of_people']= $no_of_people;
			$experienceSlots[$keyID]['ExperienceSlot']['min_guest']= $min_guest;
            $experienceSlots[$keyID]['ExperienceSlot']['slotCount']= $no_of_slots;
            $experienceSlots[$keyID]['ExperienceSlot']['freeSlot']=($no_of_slots-$no_of_bookings);
            $experienceSlots[$keyID]['ExperienceSlot']['no_of_required_slots']= $adultCount;
			$experienceSlots[$keyID]['ExperienceSlot']['checkindate']= $checkindate;
            
        }
	
     //  debug($experienceSlots);
        $this->set('experienceSlots',$experienceSlots);	
    }
    
   public function findFinalExperiencePrice($adultCount=null,$min_guest=null,$price=null){
		$this->render=false;
		$final_price=0; $min_price=0;
			//if no of requested guest are less than min guest the price will automatically increase
			if($adultCount < $min_guest){
				$min_price=($price*$min_guest);
				$final_price=$min_price/$adultCount; // new per head price
				
			}else{
				$final_price=$price;
			}
		return $final_price;
	}
	
    
    public function findAvailability($flag=null,$home_stay_id=null,$no_of_bedrooms=null,$month=null,$year=null){
        $this->render=false;
        if($month==null){
            //current month as selected months
            $selected_month_start=date('Y-m-01');
            $selected_month_end=date('Y-m-t');
        }
        $disableDates=array(); $monthlyOccupancies=array();
        $OccupanyDataArray=$this->OccupancyDetail->find('list', array('conditions'=>array( 'OccupancyDetail.homestay_id'=>$home_stay_id, array('OccupancyDetail.check_in_date BETWEEN ? AND ?' => array($selected_month_start, $selected_month_end)) ,'OccupancyDetail.status IN (14,18)' ),'fields'=>array('check_in_date' ,'OccupancyDetail.no_of_bookings'), 'group'=>'check_in_date')); 
      // debug($OccupanyDataArray);
        foreach($OccupanyDataArray as $date=>$cnt){
            
            if($no_of_bedrooms==$cnt){
                $disableDates[]=date("d-m-Y",strtotime($date));
            }
            $monthlyOccupancies[]=date("d-m-Y",strtotime($date));
        }
		
		 // debug($disableDates); debug($monthlyOccupancies);
        if($flag=='disableDays')
            return $disableDates;
        elseif($flag=='monthly')
            return $monthlyOccupancies;
        
       // debug($resultArr);
    }
    public function findAvailableRooms(){
		$this->autoRender=false;// ajax method works on selecting date rage from calender. 
        $freeBedroomsArr=array(); $available_rooms_details=array();
        $checkInCheckOut=json_decode($_REQUEST['selectedDates']);    
        //debug($checkInCheckOut);
        $check_in_date=$checkInCheckOut->check_in_date;
        $check_out_date=$checkInCheckOut->check_out_date;
        $homestay_id=$checkInCheckOut->homestay_id;
        $no_of_bedrooms=$checkInCheckOut->no_of_bedrooms;
        unset($this->OccupancyDetail->virtualFields['no_of_bookings']); 
        //find least available rooms
        $condition=" ( check_out_date > '$check_in_date' AND check_in_date < '$check_out_date') ";
        $OccupanyDataArray=$this->OccupancyDetail->find('all', array(            
         'conditions'=>array( 'OccupancyDetail.homestay_id'=>$homestay_id,'OccupancyDetail.room_price_detail_id IS NOT NULL', $condition,'OccupancyDetail.status IN (14,18,4)' ),'fields'=>array('OccupancyDetail.*'))); 
		
	
		//find least available rooms      
		$dataArrayOccupany=array();
		foreach($OccupanyDataArray as $keyDate=>$detail){
			  $check_in_date=$detail['OccupancyDetail']['check_in_date'];
			  $dataArrayOccupany[$check_in_date][]=$detail['OccupancyDetail']['room_price_detail_id'];
		  }
		$cntElement=9999; $slctdRooms=array();$max_no_of_beds=0;$max_available_no_beds=0;
	//	debug($dataArrayOccupany);
		if(count($dataArrayOccupany)>0){
			foreach($dataArrayOccupany as $key=>$item){				
				//finding free room details
				$freeRoomDetail=$this->RoomPriceDetail->query("SELECT * from room_price_details as RoomPriceDetail LEFT JOIN `occupancy_details` AS `OccupancyDetail` ON (`RoomPriceDetail`.`id` = `OccupancyDetail`.`room_price_detail_id` AND check_in_date = '$key' AND OccupancyDetail.status IN (14,18,4) ) WHERE  `RoomPriceDetail`.`homestay_id`=$homestay_id AND OccupancyDetail.room_price_detail_id IS NULL AND `RoomPriceDetail`.`status`=1  ");
			
				$cntRoom=count($freeRoomDetail);		
				$no_of_bed=0; $room_ids=array();
				foreach($freeRoomDetail as $roomDetails){
					 $max_no_of_beds +=($roomDetails['RoomPriceDetail']['no_of_bed'] + $roomDetails['RoomPriceDetail']['extra_bed']);
					 $room_id=$roomDetails['RoomPriceDetail']['id']; 
					 $room_ids[$key][]=$room_id;
					 $no_of_bed +=($roomDetails['RoomPriceDetail']['no_of_bed'] + $roomDetails['RoomPriceDetail']['extra_bed']);

				 }
				//echo "no_of_bed >>".$no_of_bed." cntElement >>".$cntElement;
				if($no_of_bed <= $cntElement){
					$cntElement=$no_of_bed;
					$max_available_no_beds=$no_of_bed;
					$slctdRooms[]=$room_ids;
					$least_possible_date=$key;	
					$leastFreeRooms=$cntRoom;
				}else{
					$slctdRooms[]=$room_ids;
				}
			}
		}else{
		
			 if(count($OccupanyDataArray)==0){
				$leastFreeRooms=$no_of_bedrooms; 
				$least_possible_date=$check_in_date;
			}
			else{
				$leastFreeRooms=min($freeBedroomsArr); 
				$least_possible_date=$least_item['OccupancyDetail']['check_in_date'];
			}

			$available_rooms_details['max_available_rooms']=$leastFreeRooms;
			$available_rooms_details['least_possible_date']=$least_possible_date;
			//finding free room details
			date_default_timezone_set('UTC');
			//$dates[] = $check_in_date; 		
			 while (strtotime($check_in_date) < strtotime($check_out_date)) { 
				 $check_in_date_need=$check_in_date;
				$check_in_date = date ("Y-m-d", strtotime("+1 days", strtotime($check_in_date)));
				// $check_in_date = date_format($check_in_date,"Y-m-d");
				$dates[] = $check_in_date_need;  
			 }	
			//debug($dates);
			 $room_ids=null;
			foreach ($dates as $value) {
				//$curdate=$value->format('Y-m-d');
				$freeRoomDetail=$this->RoomPriceDetail->query("SELECT * from room_price_details as RoomPriceDetail LEFT JOIN `occupancy_details` AS `OccupancyDetail` ON (`RoomPriceDetail`.`id` = `OccupancyDetail`.`room_price_detail_id` AND check_in_date = '$value'  AND OccupancyDetail.status IN (14,18,4) ) WHERE  `RoomPriceDetail`.`homestay_id`=$homestay_id AND OccupancyDetail.room_price_detail_id IS NULL  AND `RoomPriceDetail`.`status`=1  " );
		//			$log = $this->RoomPriceDetail->getDataSource()->getLog(false, false); 
//debug($log);
				$room_ids_other=array();$max_no_of_beds=0;
				foreach($freeRoomDetail as $roomDetails){
					$max_no_of_beds +=($roomDetails['RoomPriceDetail']['no_of_bed'] + $roomDetails['RoomPriceDetail']['extra_bed']);
					$room_id=$roomDetails['RoomPriceDetail']['id']; 
					$room_ids_other[$value][]=$room_id;
				}	
				//debug($room_ids_other);
				$slctdRooms[]=$room_ids_other;
			}
			
			$max_available_no_beds=$max_no_of_beds;
		}
		
		$available_rooms_details['max_available_rooms']=$leastFreeRooms;
        $available_rooms_details['least_possible_date']=$least_possible_date;
		$available_rooms_details['max_no_of_beds']=$max_available_no_beds;
        $available_rooms_details['room_ids']=$slctdRooms;
        //debug($slctdRooms);
        echo  json_encode($available_rooms_details);
}
    
public function isOddOrEven($number){
    $this->autoRender=false;//inside call
    $odd=1; $even=0;
    if( $number % 2 ==1)
        return $odd;
    else
        return $even;
        
}
    
public function findPriceDetails(){
        $this->autoRender=false;// ajax method for finding the right price for customer based on no of guests.         
        $priceDetail=json_decode($_REQUEST['priceDetail']); 
        $room_ids=$priceDetail->room_ids;
        $homestay_id=$priceDetail->homestay_id;
        $adultCount=$priceDetail->adultCount;
        $childCount=$priceDetail->childCount;
        $infantCount=$priceDetail->infantCount;
        $extraRoom=$priceDetail->extraRoom;
        $max_available_rooms=$priceDetail->max_available_rooms;
        $room_dtls=array();
        if($max_available_rooms<2){
            $extraRoom=0; // because only one room is available so cannot able to allocate an extra room
        }
        //debug(json_decode($room_ids));
		$room_id_objct_arr=json_decode($room_ids);
	//	debug($room_id_objct_arr);
		$room_ids=null;$room_id_arr=array(); $oldkeydate=0;
		//this flag use when same room is not available for multiple dates choosing by customer.
		$flagSameRoomAvail="yes";
		$oldRoom=array();
		foreach($room_id_objct_arr as $room){
			
			foreach($room as $keydate=>$itemsArr){	
			//	debug($room->$keydate);
				if(count($oldRoom)==0){
					$oldRoom=$room->$keydate;
				}elseif($oldRoom===$room->$keydate){
					$flagSameRoomAvail="yes";
				}else{
					$flagSameRoomAvail="no";		
				}				
				/*
				if($oldkeydate==0){
						$oldkeydate=$keydate;
					}elseif($oldkeydate==$keydate){
						$flagSameRoomAvail="yes";
					}elseif($oldkeydate!=$keydate){
						$flagSameRoomAvail="no";						
					}
					*/
				foreach($itemsArr as $key=>$item){					
					//echo "item---->>".$item;
					$freeroom_ids[$keydate][]=$item;
					$room_ids=$room_ids."_".$item;
					array_push($room_id_arr,$item);
				}
			}
		}
		
		$detailsForBooking=array();
		//avoiding the duplicates
		$freeroom_ids=array_map("unserialize", array_unique(array_map("serialize", $freeroom_ids)));
		$room_id_arr=array_map("unserialize", array_unique(array_map("serialize", $room_id_arr)));
		$detailsForBooking['roomsAvail']=$room_id_arr;        
		$final_price=0;$slctdRooms=array(); $multipleRoomAvail=0; $room_dtls=array();
		$extra_bed_flag=0;
		//debug($freeroom_ids);
	 	foreach($freeroom_ids as $keydate=>$roomids){
			$room_id_arr=$roomids;
			//$total_no_of_guest=($adultCount+$childCount);   //commented this because of change in considering child charges.
            $total_no_of_guest=($adultCount); 
			$room_id_string=implode(",",$room_id_arr);
			$this->RoomPriceDetail->unbindModel(array('belongsTo' => array('RoomType')));
			$roomPriceDetails=$this->RoomPriceDetail->find('all',array(
				'joins' => array(
						array(
							'table' => 'room_types',
							'alias' => 'RoomType',
							'type' => 'LEFT',
							'conditions' => array(
								'RoomType.id = RoomPriceDetail.room_type_id'
							)
						),     
				 ),
				'conditions'=>array("RoomPriceDetail.id IN($room_id_string)", "RoomPriceDetail.status"=>1 ),
				'fields'=>array('RoomPriceDetail.*','RoomType.*'),
				'order' => 'RoomPriceDetail.room_base_price ASC'));
			if($flagSameRoomAvail=='no'&& (count($roomPriceDetails)>1) ){
				$extraRoom=1;
				/// closing the special case booking of same room is not available in coming days.
				return   "error_same_rooms_not_available";
				exit;
				
			}
		    //debug($roomPriceDetails); 			
			// ################### START OF ROOM DETAILS ITERATION FOREACH #####################
			foreach($roomPriceDetails as $keyDtl=>$detail){
				$room_base_price=$detail['RoomPriceDetail']['room_base_price'];//base price for the room with normal beds
				$no_of_bed=$detail['RoomPriceDetail']['no_of_bed']; // normal beds
				$extra_bed=$detail['RoomPriceDetail']['extra_bed'];// extra bed available at this room it can be chargeable
				$extra_bed_charge=$detail['RoomPriceDetail']['extra_bed_charge']; //extra bed charge
				if($detail['RoomPriceDetail']['extra_bed']>0){
					$extra_bed_flag++;
				}
			// debug($detail);
//echo "total_no_of_guest==>".$total_no_of_guest."  no_of_bed==>".$no_of_bed."  extraRoom==>".$extraRoom." <br>";
				//condition 1 : checking total no of guest can fit in the current itreating room:
				//and extra room is 0            
				if($total_no_of_guest<=$no_of_bed && ($extraRoom==0)){
					$multipleRoomAvail++;
					$final_price+=$room_base_price;
					array_push($slctdRooms,$detail['RoomPriceDetail']['id']);  
					$room_id=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_price_detail_id']=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_type']=$detail['RoomType']['type'];
					$room_dtls[$keydate][$room_id]['room_description']=$detail['RoomPriceDetail']['room_description'];
					$room_dtls[$keydate][$room_id]['room_base_price']=$detail['RoomPriceDetail']['room_base_price'];
					$room_dtls[$keydate][$room_id]['extra_bed_charge']=$detail['RoomPriceDetail']['extra_bed_charge'];
					$room_dtls[$keydate][$room_id]['no_of_bed']=$detail['RoomPriceDetail']['no_of_bed'];
					$room_dtls[$keydate][$room_id]['extra_bed']=$detail['RoomPriceDetail']['extra_bed'];
					$room_dtls[$keydate][$room_id]['final_price']=$room_base_price;
					 continue;
				}
				else if($total_no_of_guest<=$no_of_bed && ($extraRoom==1)){
					$multipleRoomAvail++;
					$final_price+=$room_base_price;
					array_push($slctdRooms,$detail['RoomPriceDetail']['id']); 
					$room_id=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_price_detail_id']=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_type']=$detail['RoomType']['type'];
					$room_dtls[$keydate][$room_id]['room_description']=$detail['RoomPriceDetail']['room_description'];
					$room_dtls[$keydate][$room_id]['room_base_price']=$detail['RoomPriceDetail']['room_base_price'];
					$room_dtls[$keydate][$room_id]['extra_bed_charge']=$detail['RoomPriceDetail']['extra_bed_charge'];
					$room_dtls[$keydate][$room_id]['no_of_bed']=$detail['RoomPriceDetail']['no_of_bed'];
					$room_dtls[$keydate][$room_id]['extra_bed']=$detail['RoomPriceDetail']['extra_bed'];
					$room_dtls[$keydate][$room_id]['final_price']=$room_base_price;
					 continue;
				}

				// condition 2: checking total no of guests can fit in the current iterating room: considering extra bed.
				// and extra room is 0
				else if($total_no_of_guest<=($no_of_bed+$extra_bed) && ($extraRoom==0) ){
					
					$multipleRoomAvail++; 
					$final_price+=$room_base_price+$extra_bed_charge; // extra bed charges are applied to final price.
					array_push($slctdRooms,$detail['RoomPriceDetail']['id']);             
					$room_id=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_price_detail_id']=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_type']=$detail['RoomType']['type'];
					$room_dtls[$keydate][$room_id]['room_description']=$detail['RoomPriceDetail']['room_description'];
					$room_dtls[$keydate][$room_id]['room_base_price']=$detail['RoomPriceDetail']['room_base_price'];
					$room_dtls[$keydate][$room_id]['extra_bed_charge']=$detail['RoomPriceDetail']['extra_bed_charge'];
					$room_dtls[$keydate][$room_id]['no_of_bed']=$detail['RoomPriceDetail']['no_of_bed'];
					$room_dtls[$keydate][$room_id]['extra_bed']=$detail['RoomPriceDetail']['extra_bed'];
					$room_dtls[$keydate][$room_id]['final_price']=$room_base_price+$extra_bed_charge;
					continue;
				}
				// condition 2: checking total no of guests can fit in the current iterating room: considering extra bed.
				// and extra room is 1

				else if($total_no_of_guest<=($no_of_bed+$extra_bed) && ($extraRoom==1) ){ 
					$multipleRoomAvail++; 
					$final_price+=$room_base_price; // extra bed charges are applied to final price.
					array_push($slctdRooms,$detail['RoomPriceDetail']['id']);              
					$room_id=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_price_detail_id']=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_type']=$detail['RoomType']['type'];
					$room_dtls[$keydate][$room_id]['room_description']=$detail['RoomPriceDetail']['room_description'];
					$room_dtls[$keydate][$room_id]['room_base_price']=$detail['RoomPriceDetail']['room_base_price'];
					$room_dtls[$keydate][$room_id]['extra_bed_charge']=$detail['RoomPriceDetail']['extra_bed_charge'];
					$room_dtls[$keydate][$room_id]['no_of_bed']=$detail['RoomPriceDetail']['no_of_bed'];
					$room_dtls[$keydate][$room_id]['extra_bed']=$detail['RoomPriceDetail']['extra_bed'];
					$room_dtls[$keydate][$room_id]['final_price']=$room_base_price;
					continue;
				}

				// condition 2: checking total no of guests is greater than no of beds in current iterating room: considering extra bed too.
				// we are keeping the price of room and extra bed and add up with next room prices
				// and extra room is 0 or 1(because need an extra room to accomadate this many guests)
				//if above conditions are failing. We should go for the next room iteration(s) to satisfy the requirement.
			   else if($total_no_of_guest>($no_of_bed+$extra_bed) && ($extraRoom==0||$extraRoom==1) ){
				  if($multipleRoomAvail==0)
				   		$multipleRoomAvail++;  
				   else
					   $multipleRoomAvail++;
					//here we considering the fact as even no of adults are travelling. 
					//There will be a great chance for couples 4 guests ->2 couples. so extra bed charge is not relevant.             array_push($slctdRooms,$detail['RoomPriceDetail']['id']);
					$room_id=$detail['RoomPriceDetail']['id'];   
					if($this->isOddOrEven($adultCount)==0){
						 $final_price+=$room_base_price;
						$room_dtls[$keydate][$room_id]['final_price']=$room_base_price;
					}elseif($flagSameRoomAvail=='no'&& (count($roomPriceDetails)>1)){
						 $final_price+=$room_base_price;
						 $room_dtls[$keydate][$room_id]['final_price']=$room_base_price;
					}else{
						$final_price+=$room_base_price+$extra_bed_charge; // extra bed charges are applied to final price.
						$room_dtls[$keydate][$room_id]['final_price']=$room_base_price+$extra_bed_charge;
					}
					
					$room_dtls[$keydate][$room_id]['room_price_detail_id']=$detail['RoomPriceDetail']['id'];
					$room_dtls[$keydate][$room_id]['room_type']=$detail['RoomType']['type'];
					$room_dtls[$keydate][$room_id]['room_description']=$detail['RoomPriceDetail']['room_description'];
					$room_dtls[$keydate][$room_id]['room_base_price']=$detail['RoomPriceDetail']['room_base_price'];
					$room_dtls[$keydate][$room_id]['extra_bed_charge']=$detail['RoomPriceDetail']['extra_bed_charge'];
				    $room_dtls[$keydate][$room_id]['no_of_bed']=$detail['RoomPriceDetail']['no_of_bed'];
					$room_dtls[$keydate][$room_id]['extra_bed']=$detail['RoomPriceDetail']['extra_bed'];
					
				}
          // echo "<br/>final_price==>" . $final_price."<br/>";
				
        	}//room iteration for loop ends here.
			
		}//date wise room iteration for loop ends here.
		//echo "<br/>final_price<<<<  ==>" . $final_price."<br/>";
		if($multipleRoomAvail>1){
			   $detailsForBooking['multipleRoomAvail']='Yes';
		}elseif($flagSameRoomAvail=='no'){
			$detailsForBooking['multipleRoomAvail']='Yes';
		}
		else{
			 $detailsForBooking['multipleRoomAvail']='No';
		}
       $detailsForBooking['roomsSelected']=implode("_",$slctdRooms);		   
       $detailsForBooking['final_price']=$final_price;
	   $detailsForBooking['room_dtls']=$room_dtls;
	   $detailsForBooking['flagSameRoomAvail']=$flagSameRoomAvail;
	   $detailsForBooking['extraBedFlag']=$extra_bed_flag;
        // ################## END OF ROOM DETAILS ITERATION FOREACH #####################
        return json_encode($detailsForBooking);
        exit;  
        
  }
	public function findFinalPrice(){
        $this->autoRender=false;// ajax method for finding the right price for customer based on no of guests.         
        $details=json_decode($_REQUEST['details']); 
		$selectedRoomsString=$details->selectedRoomsString;
        $homestay_id=$details->homestay_id;
        $adultCount=$details->adultCount;
        $childCount=$details->childCount;
        $infantCount=$details->infantCount;
        $extraBed=$details->extraBed;		
		$room_list_arr=json_decode($selectedRoomsString);
		//debug($room_list_arr);
		$room_id_string=implode(",",$room_list_arr);
		$bookingDetail=array();
		//$no_of_guest=$adultCount+$childCount; //no more considering child as adult. child have seperate charges
        $no_of_guest=$adultCount;        
		$this->RoomPriceDetail->unbindModel(array('belongsTo' => array('RoomType')));
		$roomPriceDetails=$this->RoomPriceDetail->find('all',array(
				'joins' => array(
						array(
							'table' => 'room_types',
							'alias' => 'RoomType',
							'type' => 'LEFT',
							'conditions' => array(
								'RoomType.id = RoomPriceDetail.room_type_id'
							)
						), 
                        
				 ),
				'conditions'=>array("RoomPriceDetail.id IN($room_id_string)", "RoomPriceDetail.status"=>1),
				'fields'=>array('RoomPriceDetail.*','RoomType.*','Homestay.id','Homestay.per_child_charge'),
				'order' => 'RoomPriceDetail.room_base_price ASC'));
		
		//debug($roomPriceDetails);
		$bed_count=0; $final_booking_price=0; $room_chosen_string=""; $per_child_charge=0;
		foreach($roomPriceDetails as $keyDtl=>$detail){
			$room_base_price=$detail['RoomPriceDetail']['room_base_price'];//base price for the room with normal beds
			$no_of_bed=$detail['RoomPriceDetail']['no_of_bed']; // normal beds
			$extra_bed=$detail['RoomPriceDetail']['extra_bed'];// extra bed available at this room it can be chargeable
			$extra_bed_charge=$detail['RoomPriceDetail']['extra_bed_charge']; //extra bed charge
            
            $per_child_charge=$detail['Homestay']['per_child_charge']; //per_child_charge
            
			if($extraBed==1){
				$bed_count+=$no_of_bed+$extra_bed;
				$final_booking_price+=$room_base_price+$extra_bed_charge;
				$room_chosen_string.=$detail['RoomType']['type'].", ";
				//" @INR ".$detail['RoomPriceDetail']['room_base_price'].
			}else{
				$bed_count+=$no_of_bed;
				$final_booking_price+=$room_base_price;
				$room_chosen_string.=" ".$detail['RoomType']['type'].", ";
				//." @INR ".$detail['RoomPriceDetail']['room_base_price']
			}			
			
		}
		//echo $no_of_guest." ".$bed_count;
		//$no_of_days=round ((strtotime($endDate)-strtotime($startDate))/(60 * 60 * 24));
        if($childCount>0){
            $final_booking_price=($final_booking_price + ($childCount * $per_child_charge ));
        }
		if($no_of_guest <= $bed_count){
				$bookingDetail['room_chosen_string']=$room_chosen_string;
				$bookingDetail['final_booking_price']=$final_booking_price;
				$bookingDetail['roomsSelected']=implode("_",$room_list_arr);	
				$bookingDetail['error_msg']="";
			}else{
				$bookingDetail['error_msg']="not_enough_bed_count";
			}
		return json_encode($bookingDetail);
        exit;  
	}
	
public function stay_enquiry(){
		$this->layout=false;//ajax call  
		//debug($_REQUEST);
		$home_stay_id=$_REQUEST['home_stay_id'];
		$this->set('home_stay_id',$home_stay_id);
	}
	
public function experience_enquiry(){
		$this->layout=false;//ajax call  
		//debug($_REQUEST);
		$experience_id=$_REQUEST['experience_id'];
		$exprnce_dtls=$this->Experience->find('first',array('conditions'=>array('Experience.id'=>$experience_id)));
		$this->set('exprnce_dtls',$exprnce_dtls);
		$this->set('experience_id',$experience_id);
 }
	
 public function doEnquire(){
		$this->autoRender=false;//inside call   
		$formData=json_decode($_REQUEST['formDataJson']); 
		$dataArray=array();
	//	debug($_REQUEST['formDataJson']);
		$start_date=$formData->start_date;
        $end_date=$formData->end_date;
		$no_of_adult=$formData->no_of_adult;
		$no_of_children=$formData->no_of_children;
		$full_name=$formData->full_name;
		$contact_no=$formData->contact_no;
		$contact_email=$formData->contact_email;
		$message_box=$formData->message_box;
		$home_stay_id=$formData->home_stay_id;
	    $room_id_selected=$formData->room_id_selected;
        $final_price=$formData->final_price;
            
	 	$this->Homestay->recursive=2;
        $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
        $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
        $this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
	 	$conditions=array('Homestay.id'=>$home_stay_id);
        $homestayDetails=$this->Homestay->find('first', array(
			'joins' => array(
				array(
					'table' => 'homestay_files',
					'alias' => 'HomestayFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayFile.homestay_id'
					)
				),
                array(
					'table' => 'homestay_amenities',
					'alias' => 'HomestayAmenity',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayAmenity.homestay_id'
					)
				),
                array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Homestay.location_id'
					)
				),

			),
            'conditions'=>$conditions,
			'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path','Location.name','Location.latitude', 'Location.longitude'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.created DESC'
		));
        $room_id_arr=explode("_",$room_id_selected);
        $room_id_string=implode(",",$room_id_arr);
        $room_dtls="";
	    $dataRooms=$this->RoomPriceDetail->find('all',array('conditions'=>array("RoomPriceDetail.id IN($room_id_string)", "RoomPriceDetail.status"=>1 )));
        if(count($dataRooms)>0){
           foreach($dataRooms as $room_price_details){
			 if(is_array($room_price_details)){
				 $room_dtls.=$room_price_details['RoomPriceDetail']['room_no_name']." with ".$room_price_details['RoomPriceDetail']['no_of_bed']. " beds ";
				 if($room_price_details['RoomPriceDetail']['extra_bed']>0){
					 $room_dtls.= "  and ".$room_price_details['RoomPriceDetail']['extra_bed']." beds. ";
				 }
				  $room_dtls.="<br/>";
			 }
		  }
        }		
		$dataArray['BookingEnquiry']['start_date']=date('Y-m-d',strtotime($start_date));
		$dataArray['BookingEnquiry']['end_date']=date('Y-m-d',strtotime($end_date));
		$dataArray['BookingEnquiry']['no_of_adult']=$no_of_adult;
		$dataArray['BookingEnquiry']['no_of_children']=$no_of_children;
		$dataArray['BookingEnquiry']['full_name']=$full_name;
		$dataArray['BookingEnquiry']['contact_no']=$contact_no;
		$dataArray['BookingEnquiry']['contact_email']=$contact_email;
		$dataArray['BookingEnquiry']['message_box']=$message_box;
		$dataArray['BookingEnquiry']['home_stay_id']=$home_stay_id;
        $dataArray['BookingEnquiry']['room_id_selected']=$room_id_selected;
        $dataArray['BookingEnquiry']['special_price']=$final_price;
		$response=$this->BookingEnquiry->saveAll($dataArray);  
	 	$lastInsertBookingEngquiryID = $this->BookingEnquiry->getLastInsertID();  
	
	 	$unique_email_code=$this->generateRandomString();
	 	$dtaResponse['EnquiryHostResponse']['homestay_id']=$homestayDetails['Homestay']['id'];
	 	$dtaResponse['EnquiryHostResponse']['booking_enquiry_id']=$lastInsertBookingEngquiryID;
	 	$dtaResponse['EnquiryHostResponse']['unique_email_code']=$unique_email_code;
	 	$response=$this->EnquiryHostResponse->saveAll($dtaResponse);  
	 	$lastInsertEnquiryResponseID = $this->EnquiryHostResponse->getLastInsertID();  
	 
	 	$parameters="?searchId=".base64_encode($homestayDetails['Homestay']['id'])."&name="."&unique_email_code=".$unique_email_code."&enquiry_host_response_id=".$lastInsertEnquiryResponseID."&booking_enquiry_id=".$lastInsertBookingEngquiryID;
	 	$accept_url=Configure::read('app_root_path')."detail/accept_reject_enquiry_response".$parameters."&enquiry_accept_reject=accepted";		$reject_url=Configure::read('app_root_path')."detail/accept_reject_enquiry_response".$parameters."&enquiry_accept_reject=rejected";	
			
		 $homestay_head_image="";	
	    if(@is_array($homestayDetails['HomestayFile'][0])){
	 		@$homestay_head_image=Configure::read('app_root_path').$homestayDetails['Homestay']['HomestayFile'][0]['file_path']['file_name'];
	    }
	 
	    $host_name=$homestayDetails['Homestay']['owner_name'];
		if($homestayDetails['Homestay']['contact_person_name']!="" )
			 $host_name.=" / ".$homestayDetails['Homestay']['contact_person_name'];
	   
        if($homestayDetails['Homestay']['email']!="")
	 	     $host_email=$homestayDetails['Homestay']['email'];
        else
             $host_email=Configure::read('booking_email_cc');
	 
	 	if($homestayDetails['Homestay']['mobile1']!="")
	 		$host_telphone=$homestayDetails['Homestay']['mobile1'];
	 	elseif($homestayDetails['Homestay']['telephone']!="")
	 		$host_telphone=$homestayDetails['Homestay']['telephone'];
	 	else
			$host_telphone=Configure::read('host_default_phone');
			
	 	 $subject="Enquiry for ".$homestayDetails['Homestay']['name']." from ".date('Y-m-d',strtotime($start_date))
." to ".date('Y-m-d',strtotime($end_date));
	 
	  
	 
	 	//Email to host for confirmation. Host can either accept or reject by clicking on the links in email.
		$EmailHostAn = new CakeEmail();	
	// debug($EmailHostAn);
		$EmailHostAn->config('gmail');
		$EmailHostAn->config(array('from' => 'coorgexpressbooking@gmail.com'));
		$EmailHostAn->viewVars(array(
			 					'homestay_name' => $homestayDetails['Homestay']['name'],
								'homestay_head_image'=>$homestay_head_image,
							   	'full_name' =>$full_name,
							   	'no_of_adult' => ($no_of_adult),
							 	'no_of_children' =>$no_of_children,
								'check_in_date'=>date('Y-m-d',strtotime($start_date)),
								'check_out_date'=>date('Y-m-d',strtotime($end_date)),
								'contact_no'=>$contact_no,
								'message_box'=>$message_box,
								'host_name'=>$host_name,
								'host_telphone'=>$host_telphone,                                
								'room_dtls'=>$room_dtls,
								'accept_url'=>$accept_url,
			 					'reject_url'=>$reject_url
								
								
							   )); 
		 $EmailHostAn->template('homestay_enquiry_host',null)
								->emailFormat('html')
								->to($host_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();
	 
	 		//Email to customer for successful placing of enquiry.
	 	$message_success="We have received your enquiry. The host will get back to you shortly over phone or email. Thanks for your interest for booking with coorgexpress.com ";
		$EmailCustomerSS = new CakeEmail();	
		$EmailCustomerSS->config('gmail');
		$EmailCustomerSS->config(array('from' => 'coorgexpressbooking@gmail.com'));
		$EmailCustomerSS->viewVars(array(
			 					'heading' => "Thank you for the enquiry!",
								'message'=>$message_success,
							   	'display_style' =>'display:none;',
							   	'button_url' =>'',
							 	'button_text' =>''
								
								
							   )); 
		 $EmailCustomerSS->template('generic_response',null)
								->emailFormat('html')
								->to($contact_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();
	 
	     return 1;
}
 public function accept_reject_enquiry_response(){
		//$this->autoRender=false;//inside call   
     $success_msg="";
	if(count($_REQUEST)>0){        
            $homestay_id=base64_decode($_REQUEST['searchId']);
			$name=$_REQUEST['name'];
			$unique_email_code=$_REQUEST['unique_email_code'];
			$enquiry_host_response_id=$_REQUEST['enquiry_host_response_id'];
			$booking_enquiry_id=$_REQUEST['booking_enquiry_id'];
			$enquiry_accept_reject=$_REQUEST['enquiry_accept_reject'];
            
            $hostResponse=$this->EnquiryHostResponse->find('first',array('conditions'=>array( 'EnquiryHostResponse.id'=>$enquiry_host_response_id)));
            if($hostResponse['EnquiryHostResponse']['accept_reject']==0){
           
                $conditions=array("EnquiryHostResponse.homestay_id "=>$homestay_id, 'EnquiryHostResponse.id'=>$enquiry_host_response_id,  'BookingEnquiry.id'=>$booking_enquiry_id, 'Homestay.id'=>$homestay_id);						
                $this->Homestay->recursive=2;
                $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
                $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
                $this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));

                $homestayDetails=$this->Homestay->find('first', array(
                    'joins' => array(
                        array(
                            'table' => 'homestay_files',
                            'alias' => 'HomestayFile',
                            'type'  => 'LEFT',
                            'conditions' => array(
                                'Homestay.id = HomestayFile.homestay_id'
                            )
                        ),
                        array(
                            'table' => 'homestay_amenities',
                            'alias' => 'HomestayAmenity',
                            'type'  => 'LEFT',
                            'conditions' => array(
                                'Homestay.id = HomestayAmenity.homestay_id'
                            )
                        ),
                        array(
                            'table' => 'room_price_details',
                            'alias' => 'RoomPriceDetail',
                            'type'  => 'LEFT',
                            'conditions' => array(
                                'Homestay.id = RoomPriceDetail.homestay_id'
                            )
                        ),
                        array(
                            'table' => 'locations',
                            'alias' => 'Location',
                            'type' => 'INNER',
                            'conditions' => array(
                                'Location.id = Homestay.location_id'
                            )
                        ),
                        array(
                            'table' => 'enquiry_host_responses',
                            'alias' => 'EnquiryHostResponse',
                            'type' => 'INNER',
                            'conditions' => array(
                                'Homestay.id = EnquiryHostResponse.homestay_id'
                            )
                        ),					
                        array(
                            'table' => 'booking_enquiries',
                            'alias' => 'BookingEnquiry',
                            'type' => 'INNER',
                            'conditions' => array(
                                'Homestay.id = BookingEnquiry.home_stay_id'
                            )
                        ),

                    ),
                'conditions'=>$conditions,
                'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path','Location.name','Location.latitude', 'Location.longitude','BookingEnquiry.*','EnquiryHostResponse.*'),
                'group' => '`Homestay`.`id`',
                'order' => 'Homestay.created DESC'
               ));

    //debug($homestayDetails);
                $checkin_date=$homestayDetails['BookingEnquiry']['start_date'];
		        $checkout_date=$homestayDetails['BookingEnquiry']['end_date'];
                $room_id_selected=$homestayDetails['BookingEnquiry']['room_id_selected'];
                $price=$homestayDetails['BookingEnquiry']['special_price'];
                $no_of_guest=$homestayDetails['BookingEnquiry']['no_of_adult'];
		        $no_of_children=$homestayDetails['BookingEnquiry']['no_of_children'];
                $room_ids=json_encode($room_id_selected);
                
               // $parameters="?searchId=".base64_encode($homestay_id)."&name=".$homestayDetails['Homestay']['name']."&unique_email_code=".$unique_email_code."&enquiry_host_response_id=".$enquiry_host_response_id."&booking_enquiry_id=".$booking_enquiry_id;
                
                $parameters="?homestay_id=".$homestay_id."&unique_email_code=".$unique_email_code." &total_amount=".$price."&booking_enquiry_id=".$booking_enquiry_id."&startDate=".date('Y-m-d',strtotime($checkin_date))."&endDate=".date('Y-m-d',strtotime($checkout_date)). "&room_id_selected=".$room_id_selected."&flagSameRoomAvail=yes&qtyInputAdult=".$no_of_guest."&qtyInputChild=".$no_of_children."&qtyInputInfant=0"."&room_ids=".$room_ids."&rooms=".$room_ids;

               $customer_email=$homestayDetails['BookingEnquiry']['contact_email'];			
            if($enquiry_accept_reject=="accepted"){	
                    
                    $accept_url=Configure::read('app_root_path')."booking/index".$parameters."&enquiry_accept_reject=accepted";
                    //Email to customer for successful placing of enquiry.
                    $message_accept="The host has accepted your enquiry. Please click the below link and do the booking. Thanks for your interest for booking with coorgexpress.com ";

                    $subject="Host accepted your enquiry.";

                    $EmailCustomer = new CakeEmail();	
                    $EmailCustomer->config('gmail');
                    $EmailCustomer->config(array('from' => 'coorgexpressbooking@gmail.com'));
                    $EmailCustomer->viewVars(array(
                                            'heading' => "Host accepted your enquiry!",
                                            'message'=>$message_accept,
                                            'display_style' =>'display:block;',
                                            'button_url' =>$accept_url,
                                            'button_text' =>'Book Now'


                                           )); 
                     $EmailCustomer->template('generic_response',null)
                                            ->emailFormat('html')
                                            ->to($customer_email) 
                                            ->cc(Configure::read('booking_email_cc'))
                                            ->from(Configure::read('smtp_from_email'))
                                            ->subject($subject)
                                            ->send();
                 $dtaResponse['EnquiryHostResponse']['id']=$enquiry_host_response_id;
                 $dtaResponse['EnquiryHostResponse']['homestay_id']=$homestayDetails['Homestay']['id'];
                 $dtaResponse['EnquiryHostResponse']['unique_email_code']=$unique_email_code;
                 $dtaResponse['EnquiryHostResponse']['accept_reject']=19;

             }else{

                    //$reject_url=Configure::read('app_root_path')."details/index".$parameters."&enquiry_accept_reject=accepted";

                    //Email to customer for successful placing of enquiry.
                    $subject="Host Rejected your enquiry.";
                    $message_reject="Sorry, we can't accomadate you at this stay on selected date. The host has rejected your enquiry. Thanks for your interest for booking with coorgexpress.com. You can check other stays available at coorgexpress.com ";
                    $EmailCustomer = new CakeEmail();	
                    $EmailCustomer->config('gmail');
                    $EmailCustomer->config(array('from' => 'coorgexpressbooking@gmail.com'));
                    $EmailCustomer->viewVars(array(
                                            'heading' => "Host rejected your enquiry!",
                                            'message'=>$message_reject,
                                            'display_style' =>'display:none;',
                                            'button_url' =>'',
                                            'button_text' =>''


                                           )); 
                     $EmailCustomer->template('generic_response',null)
                                            ->emailFormat('html')
                                            ->to($customer_email) 
                                            ->cc(Configure::read('booking_email_cc'))
                                            ->from(Configure::read('smtp_from_email'))
                                            ->subject($subject)
                                            ->send();

                    $dtaResponse['EnquiryHostResponse']['id']=$enquiry_host_response_id;
                    $dtaResponse['EnquiryHostResponse']['homestay_id']=$homestayDetails['Homestay']['id'];
                    $dtaResponse['EnquiryHostResponse']['unique_email_code']=$unique_email_code;
                    $dtaResponse['EnquiryHostResponse']['accept_reject']=20;


                }

                $response=$this->EnquiryHostResponse->saveAll($dtaResponse);  
                //$lastInsertEnquiryResponseID = $this->EnquiryHostResponse->getLastInsertID();  
                $success_msg="Successfully $enquiry_accept_reject the enquiry.";
            }else{
                $success_msg="Already marked your response for this enquiry.";
            }
        }
		//$this->render('detail/index');
         //$this->redirect('/handpicked-stays');
        $this->set('success_msg',$success_msg);
        $this->set('page_title',@$homestayDetails['Homestay']['name']);
		$this->set('meta_title',@$homestayDetails['Homestay']['meta_title']);
		$this->set('meta_keyword',@$homestayDetails['Homestay']['meta_keyword']);
		$this->set('meta_description',@$homestayDetails['Homestay']['meta_description']);
		
}
	

public function doExperienceEnquiry(){
		$this->autoRender=false;//inside call   
		$formData=json_decode($_REQUEST['formDataJson']); 
		$dataArray=array();
	//	debug($_REQUEST['formDataJson']);
		$start_date=$formData->start_date;
		$no_of_adult=$formData->no_of_adult;
		$no_of_children=$formData->no_of_children;
		$full_name=$formData->full_name;
		$contact_no=$formData->contact_no;
		$contact_email=$formData->contact_email;
		$message_box=$formData->message_box;
		$experience_id=$formData->experience_id;
	    $experience_slot_id=$formData->experience_slot_id;
        $final_price=$formData->final_price;
            
	 	$this->Experience->recursive=2;
        $this->Experience->unbindModel(array('hasMany' => array('ExperienceSlot')));
        $this->ExperienceFile->unbindModel(array('belongsTo' => array('Experience')));
        $this->ExperienceLanguage->unbindModel(array('belongsTo' => array('Experience')));
        $this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));        
        unset($this->ExperienceSlot->virtualFields['no_of_slots']);
		$conditions=array('Experience.id'=>$experience_id,'ExperienceSlot.id'=>$experience_slot_id);
        $experienceDetails=$this->Experience->find('first', array(
			'joins' => array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),
                array(
					'table' => 'trail_types',
					'alias' => 'TrailType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.trail_type_id = TrailType.id'
					)
				),
                array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Experience.location_id'
					)
				),
				 array(
					'table' => 'experience_slots',
					'alias' => 'ExperienceSlot',
					'type' => 'INNER',
					'conditions' => array(
						'ExperienceSlot.experience_id = Experience.id'
					)
				),

			),
            'conditions'=>$conditions,
			'fields' => array('Experience.*','TrailType.*','Location.name','Location.latitude', 'Location.longitude', 'ExperienceSlot.*' ),
             //,'ExperienceFile.file_name','ExperienceFile.file_path', 'ExperienceLanguage.*', 'ExperienceType.*',
            'group' => '`Experience`.`id`',
			'order' => 'Experience.created DESC'
		));
	
        	
		$dataArray['BookingEnquiry']['start_date']=date('Y-m-d',strtotime($start_date));
		$dataArray['BookingEnquiry']['no_of_adult']=$no_of_adult;
		$dataArray['BookingEnquiry']['no_of_children']=$no_of_children;
		$dataArray['BookingEnquiry']['full_name']=$full_name;
		$dataArray['BookingEnquiry']['contact_no']=$contact_no;
		$dataArray['BookingEnquiry']['contact_email']=$contact_email;
		$dataArray['BookingEnquiry']['message_box']=$message_box;
		$dataArray['BookingEnquiry']['experience_id']=$experience_id;
        $dataArray['BookingEnquiry']['experience_slot_id']=$experience_slot_id;
        $dataArray['BookingEnquiry']['special_price']=$final_price;
		$response=$this->BookingEnquiry->saveAll($dataArray);  
	 	$lastInsertBookingEngquiryID = $this->BookingEnquiry->getLastInsertID();  
	
	 	$unique_email_code=$this->generateRandomString();
	 	$dtaResponse['EnquiryHostResponse']['experience_id']=$experienceDetails['Experience']['id'];
	 	$dtaResponse['EnquiryHostResponse']['booking_enquiry_id']=$lastInsertBookingEngquiryID;
	 	$dtaResponse['EnquiryHostResponse']['unique_email_code']=$unique_email_code;
	 	$response=$this->EnquiryHostResponse->saveAll($dtaResponse);  
	 	$lastInsertEnquiryResponseID = $this->EnquiryHostResponse->getLastInsertID();  
	 	
	 	$parameters="?searchId=".base64_encode($experienceDetails['Experience']['id'])."&experience_slot_id=".$experience_slot_id."&unique_email_code=".$unique_email_code."&enquiry_host_response_id=".$lastInsertEnquiryResponseID."&booking_enquiry_id=".$lastInsertBookingEngquiryID;
	 	$accept_url=Configure::read('app_root_path')."detail/accept_reject_experience_enquiry_response".$parameters."&enquiry_accept_reject=accepted";		$reject_url=Configure::read('app_root_path')."detail/accept_reject_experience_enquiry_response".$parameters."&enquiry_accept_reject=rejected";	
			
		 $homestay_head_image="";	
	    if(@is_array($experienceDetails['ExperienceFile'][0])){
	 		@$experience_head_image=Configure::read('app_root_path').$experienceDetails['ExperienceFile'][0]['file_path']."/".$experienceDetails['ExperienceFile'][0]['file_name'];
	    }
	 
	    $host_name=$experienceDetails['Experience']['hosted_by'];
	   
        if($experienceDetails['Experience']['host_contact_email']!="")
	 	     $host_email=$experienceDetails['Experience']['host_contact_email'];
        else
             $host_email=Configure::read('booking_email_cc');
	 
	 	if($experienceDetails['Experience']['host_contact_no']!="")
	 		$host_telphone=$experienceDetails['Experience']['host_contact_no'];
	 	elseif($experienceDetails['Experience']['host_contact_no']!="")
	 		$host_telphone=$experienceDetails['Experience']['host_contact_no'];
	 	else
			$host_telphone=Configure::read('host_default_phone');
	
		$slot_dtls=$experienceDetails['ExperienceSlot']['slot_start_time']." - ".$experienceDetails['ExperienceSlot']['slot_end_time'];
			
	 	$subject="Enquiry for ".$experienceDetails['Experience']['name']." from ".date('Y-m-d',strtotime($start_date))
." @ ".$experienceDetails['ExperienceSlot']['slot_start_time']." - ".$experienceDetails['ExperienceSlot']['slot_end_time'];
	 
	 
	 	//Email to host for confirmation. Host can either accept or reject by clicking on the links in email.
		$EmailHostAn = new CakeEmail();	
		$EmailHostAn->config('gmail');
		$EmailHostAn->config(array('from' => 'coorgexpressbooking@gmail.com'));
		$EmailHostAn->viewVars(array(
			 					'experience_name' => $experienceDetails['Experience']['name'],
								'experience_head_image'=>$experience_head_image,
							   	'full_name' =>$full_name,
							   	'no_of_adult' => ($no_of_adult),
							 	'no_of_children' =>$no_of_children,
								'check_in_date'=>date('Y-m-d',strtotime($start_date)),
								'contact_no'=>$contact_no,
								'message_box'=>$message_box,
								'host_name'=>$host_name,
								'host_telphone'=>$host_telphone,                                
								'slot_dtls'=>$slot_dtls,
								'accept_url'=>$accept_url,
			 					'reject_url'=>$reject_url
								
								
							   )); 
		 $EmailHostAn->template('experience_enquiry_host',null)
								->emailFormat('html')
								->to($host_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();
	 
	 		//Email to customer for successful placing of enquiry.
	 	$message_success="We have received your enquiry. The host will get back to you shortly over phone or email. Thanks for your interest for booking with coorgexpress.com ";
		$EmailCustomerSS = new CakeEmail();	
		$EmailCustomerSS->config('gmail');
		$EmailCustomerSS->config(array('from' => 'coorgexpressbooking@gmail.com'));
		$EmailCustomerSS->viewVars(array(
			 					'heading' => "Thank you for the enquiry!",
								'message'=>$message_success,
							   	'display_style' =>'display:none;',
							   	'button_url' =>'',
							 	'button_text' =>''
								
								
							   )); 
		 $EmailCustomerSS->template('generic_response',null)
								->emailFormat('html')
								->to($contact_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();
	 
	     return 1;
}

 public function accept_reject_experience_enquiry_response(){
		//$this->autoRender=false;//inside call   
     $success_msg="";
	if(count($_REQUEST)>0){        
            $experience_id=base64_decode($_REQUEST['searchId']);
			$experience_slot_id=$_REQUEST['experience_slot_id'];
			$unique_email_code=$_REQUEST['unique_email_code'];
			$enquiry_host_response_id=$_REQUEST['enquiry_host_response_id'];
			$booking_enquiry_id=$_REQUEST['booking_enquiry_id'];
			$enquiry_accept_reject=$_REQUEST['enquiry_accept_reject'];
            
            $hostResponse=$this->EnquiryHostResponse->find('first',array('conditions'=>array( 'EnquiryHostResponse.id'=>$enquiry_host_response_id)));
            if($hostResponse['EnquiryHostResponse']['accept_reject']==0){
           
                $conditions=array("EnquiryHostResponse.experience_id "=>$experience_id, 'EnquiryHostResponse.id'=>$enquiry_host_response_id,  'BookingEnquiry.id'=>$booking_enquiry_id, 'Experience.id'=>$experience_id,'ExperienceSlot.id'=>$experience_slot_id);						
                $this->Experience->recursive=2;
				$this->Experience->unbindModel(array('hasMany' => array('ExperienceSlot')));
        		$this->ExperienceFile->unbindModel(array('belongsTo' => array('Experience')));
        		$this->ExperienceLanguage->unbindModel(array('belongsTo' => array('Experience')));
        		$this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));        
        		unset($this->ExperienceSlot->virtualFields['no_of_slots']);
				$experienceDetails=$this->Experience->find('first', array(
			'joins' => array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),
                array(
					'table' => 'trail_types',
					'alias' => 'TrailType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.trail_type_id = TrailType.id'
					)
				),
                array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Experience.location_id'
					)
				),
				 array(
					'table' => 'experience_slots',
					'alias' => 'ExperienceSlot',
					'type' => 'INNER',
					'conditions' => array(
						'ExperienceSlot.experience_id = Experience.id'
					)
				),
				array(
                     'table' => 'enquiry_host_responses',
                      'alias' => 'EnquiryHostResponse',
                     'type' => 'INNER',
                     'conditions' => array(
                        'Experience.id = EnquiryHostResponse.experience_id'
                    )
               ),
               array(
                   'table' => 'booking_enquiries',
                   'alias' => 'BookingEnquiry',
                   'type' => 'INNER',
                   'conditions' => array(
                       'Experience.id = BookingEnquiry.experience_id'
                    )
              ),
			),
            'conditions'=>$conditions,
			'fields' => array('Experience.*','TrailType.*','Location.name','Location.latitude', 'Location.longitude', 'ExperienceSlot.*','BookingEnquiry.*','EnquiryHostResponse.*' ),
            'group' => '`Experience`.`id`',
			'order' => 'Experience.created DESC'
		 ));
              
    //debug($homestayDetails);
                $checkin_date=$experienceDetails['BookingEnquiry']['start_date'];
                $experience_slot_id=$experienceDetails['BookingEnquiry']['experience_slot_id'];
                $price=$experienceDetails['BookingEnquiry']['special_price'];
                $no_of_guest=$experienceDetails['BookingEnquiry']['no_of_adult'];
		        $no_of_children=$experienceDetails['BookingEnquiry']['no_of_children'];
				$slot_start_time=$experienceDetails['ExperienceSlot']['slot_start_time'];
				$slot_end_time=$experienceDetails['ExperienceSlot']['slot_end_time'];
				$price=$experienceDetails['ExperienceSlot']['price'];
				$min_guest=$experienceDetails['ExperienceSlot']['min_guest'];
            
                $parameters="?experience_id=".$experience_id."&unique_email_code=".$unique_email_code." &total_amount=".$price."&booking_enquiry_id=".$booking_enquiry_id."&date=".date('Y-m-d',strtotime($checkin_date))."&experience_slot_id=".$experience_slot_id."&no_of_guest=".$no_of_guest."&qtyInputChild=".$no_of_children."&slot_start_time=".$slot_start_time."&slot_end_time=".$slot_end_time."&price=".$price."&min_guest=".$min_guest;

               $customer_email=$experienceDetails['BookingEnquiry']['contact_email'];			
            if($enquiry_accept_reject=="accepted"){	
                    
                 $accept_url=Configure::read('app_root_path')."booking/experience".$parameters."&enquiry_accept_reject=accepted";
                    //Email to customer for successful placing of enquiry.
                 $message_accept="The host has accepted your enquiry. Please click the below link and do the booking. Thanks for your interest for booking with coorgexpress.com ";

                    $subject="Host accepted your enquiry.";

                    $EmailCustomer = new CakeEmail();	
                    $EmailCustomer->config('gmail');
                    $EmailCustomer->config(array('from' => 'coorgexpressbooking@gmail.com'));
                    $EmailCustomer->viewVars(array(
                                            'heading' => "Host accepted your enquiry!",
                                            'message'=>$message_accept,
                                            'display_style' =>'display:block;',
                                            'button_url' =>$accept_url,
                                            'button_text' =>'Book Now'


                                           )); 
                     $EmailCustomer->template('generic_response',null)
                                            ->emailFormat('html')
                                            ->to($customer_email) 
                                            ->cc(Configure::read('booking_email_cc'))
                                            ->from(Configure::read('smtp_from_email'))
                                            ->subject($subject)
                                            ->send();
                 $dtaResponse['EnquiryHostResponse']['id']=$enquiry_host_response_id;
                 $dtaResponse['EnquiryHostResponse']['experience_id']=$experienceDetails['Experience']['id'];
                 $dtaResponse['EnquiryHostResponse']['unique_email_code']=$unique_email_code;
                 $dtaResponse['EnquiryHostResponse']['accept_reject']=19;

             }else{

                    //$reject_url=Configure::read('app_root_path')."details/index".$parameters."&enquiry_accept_reject=accepted";

                    //Email to customer for successful placing of enquiry.
                    $subject="Host Rejected your enquiry.";
                    $message_reject="Sorry, we can't accomadate you at this stay on selected date. The host has rejected your enquiry. Thanks for your interest for booking with coorgexpress.com. You can check other stays available at coorgexpress.com ";
                    $EmailCustomer = new CakeEmail();	
                    $EmailCustomer->config('gmail');
                    $EmailCustomer->config(array('from' => 'coorgexpressbooking@gmail.com'));
                    $EmailCustomer->viewVars(array(
                                            'heading' => "Host rejected your enquiry!",
                                            'message'=>$message_reject,
                                            'display_style' =>'display:none;',
                                            'button_url' =>'',
                                            'button_text' =>''


                                           )); 
                     $EmailCustomer->template('generic_response',null)
                                            ->emailFormat('html')
                                            ->to($customer_email) 
                                            ->cc(Configure::read('booking_email_cc'))
                                            ->from(Configure::read('smtp_from_email'))
                                            ->subject($subject)
                                            ->send();

                    $dtaResponse['EnquiryHostResponse']['id']=$enquiry_host_response_id;
                    $dtaResponse['EnquiryHostResponse']['experience_id']=$experienceDetails['Experience']['id'];
                    $dtaResponse['EnquiryHostResponse']['unique_email_code']=$unique_email_code;
                    $dtaResponse['EnquiryHostResponse']['accept_reject']=20;


                }

                $response=$this->EnquiryHostResponse->saveAll($dtaResponse);  
                $success_msg="Successfully $enquiry_accept_reject the enquiry.";
            }else{
                $success_msg="Already marked your response for this enquiry.";
            }
        }
        $this->set('success_msg',$success_msg);
        $this->set('page_title',@$experienceDetails['Experience']['name']);
		$this->set('meta_title',@$experienceDetails['Experience']['meta_title']);
		$this->set('meta_keyword',@$experienceDetails['Experience']['meta_keyword']);
		$this->set('meta_description',@$experienceDetails['Experience']['meta_description']);
		
}
	
	
	public function generateRandomString($length = 10) {
		$this->autoRender=false;//inside call     
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
    }
	
	public function splitDates($start_date, $end_date) {	
		 date_default_timezone_set('UTC');
		 $start_date=date ("d-m-Y", strtotime($start_date)); 
		 $end_date=date ("d-m-Y", strtotime($end_date)); 
		 $aDays[] = date ("d-m-Y", strtotime($start_date)); 		
		 while (strtotime($start_date) < strtotime($end_date)) {  
		    $start_date = date ("d-m-Y", strtotime("+1 days", strtotime($start_date)));
			$aDays[] = $start_date;  
			// echo "test";
		 }		
		//debug($aDays);
		 return $aDays;         
    }
	
	
	
}
?>