<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {
	var $layout ="default";
	public $uses = array('User','Role','Status','Department','UserReset');

	// listing the users here.
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}
	public function view($id=""){
		$restult_users=$this->User->find('all', array(
			'joins' => array(

				array(
					'table' => 'statuses',
					'alias' => 'Status',
					'type' => 'INNER',
					'conditions' => array(
						'Status.id = User.status_id'
					)
				),
                                array(
					'table' => 'departments',
					'alias' => 'Department',
					'type' => 'INNER',
					'conditions' => array(
						'User.department_id = Department.id'
					)
				),

			),

			'conditions' => array('User.role_id IN(1,2)','User.status_id IN(1,2)'),
			'fields' => array('User.*','Status.*','Department.department_name'),
			'order' => 'User.created DESC'
		));


		$active_flag="";
		$user_id="";
		if($id!=""){
			 $restult_users_arr=$this->edit($id);
			// debug($restult_users_arr);
			if(count($restult_users_arr)==0 || empty($restult_users_arr)){
				$this->set('active_flag',$active_flag="list");
				//$this->Session->setFlash('You are trying something wrong..!!', 'default', array(), 'bad');
			}else{

				$this->request->data = $restult_users_arr[0];

			   	$this->set('user_id',$this->request->data['User']['id']);
				$this->set('active_flag',$active_flag="add_edit");
			}
		}
		else{
                    $this->set('active_flag',$active_flag="list");

                }


		$this->set('user_id',$id);
		$this->set('restult_status',$this->Status->find('list',array('fields'=>array('id','status'))));
                $this->set('restult_departments',$this->Department->find('list',array('fields'=>array('id','department_name'),'conditions'=>array('Department.status'=>'1'))));

		$this->set('restult_users',$restult_users);
	}

	
	public function edit($id=""){
		if($id!="")
			$restult_users=$this->User->find('all',array('conditions'=>array('User.id'=>$id)));

		return $restult_users;
	}
	public function change_password($user_id=""){
		$this->layout=false;
		$this->set('user_id',$user_id);
	}
	public function savePasswordData(){
		$this->autoRender = false;
		//debug($this->request);
		if(!empty($this->request->data)){
			$data['User']['password']=$this->request->data['password'];
			//$data['User']['email']=$this->request->data['email'];
			$data['User']['id']=$this->request->data['id'];
			$this->User->SaveAll($data);
			return true;
		}else
		 	return false;

	}

   public function updatePassword(){
		$this->autoRender = false;
		if(!empty($this->request->data)){
		$data['User']['status_id']=$this->request->data['statusId'];
		$data['User']['id']=$this->request->data['user_id'];
		$this->User->SaveAll($data);
			return true;
		}else
		 	return false;
	}

	public function index() {
		//echo $this->Session->id();
        $this->layout = 'login_layout';
		
	}
   public function reset($token='') {
            if($token!="")
            {
               $restult_status=$this->UserReset->find('first',array('joins' => array(
                array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'INNER',
                        'conditions' => array(
                                'User.username = UserReset.user_email'
                        )
                )),
                'conditions' => array('token'=>$token),
                 'fields' => array('User.*', 'UserReset.*')));
               $this->layout = 'login_layout';
               $this->set('restult_status',$restult_status);
            }
            else {
               $this->autoRender=false;
               $this->layout = '';
            }

	}
    public function login() {
        $this->autoRender = false;
        $this->Auth->logout();
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $email_id=$this->request->data['User']['username'];
                $restult_users=$this->User->find('first',array('conditions'=>array('username'=>$email_id)));
                if(!empty($restult_users))
                {
                    $role_id=$restult_users['User']['role_id'];
                    if($role_id=='1')
                    {
                        return  $this->redirect(array('controller'=>'dashboard','action'=>'view'));
                    }
                    else if($role_id=='2'){
                        return  $this->redirect(array('controller'=>'dashboard','action'=>'view'));
                    }
                    else if($role_id=='3'){
                        return  $this->redirect(array('controller'=>'dashboard','action'=>'view'));
                    }
                    else {
                        $this->Session->setFlash('Invalid user role, please try again.', 'default', array(), 'login_bad');
                        return  $this->redirect(array('controller'=>'users','action'=>'index'));
                    }
                }
                else
                {
                    $this->Session->setFlash('Invalid user details, try again.', 'default', array(), 'login_bad');
                    return  $this->redirect(array('controller'=>'users','action'=>'index'));
                }
            }else{
         
                $this->Session->setFlash('Invalid username or password, try again.', 'default', array(), 'login_bad');
                return  $this->redirect(array('controller'=>'users','action'=>'index'));
            }

         }else{
            $this->Session->setFlash('Login failed, please try again.', 'default', array(), 'login_bad');            
            return $this->redirect(array('controller'=>'users','action'=>'index'));

          }

           
  }

 public function customer_login() {
     $this->autoRender = false;
     
     if($this->request->is('post')) {
         $loginData=json_decode($_REQUEST['loginData']);
         $this->request->data['User']['username']=$loginData->username;
         $this->request->data['User']['password']=$loginData->password;            
         if ($this->Auth->login()) {
            
            $role_id=4;// customers
            
            $user_id =$this->Auth->user('id');
            
            if(!empty($user_id)){
            
                $restult_users=$this->User->find('first',array('conditions'=>array('username'=>$loginData->username, 'role_id'=>$role_id)));
             
                $error_flag="success";
            
            }else{
                
                $error_flag="Auth Login Failed";
            }
         
            
         }else{         
           $error_flag="Auth Login Failed";
          }
         
      }else{
           $error_flag="Request data failed";
      }
      
      
      $response['response']=$error_flag;
      echo json_encode($response);
      exit;
      
  }
 public function customer_register() {
     $this->autoRender = false;  
    // debug($_REQUEST);
     $registerData=json_decode($_REQUEST['register']);
     $response=array();
    // debug($registerData);
     $user_data['User']['username']=$registerData->register_username;
     $user_data['User']['email_id']=$registerData->register_username;
     $user_data['User']['password']=$registerData->register_password;
     $user_data['User']['first_name']=$registerData->register_first_name;
     $user_data['User']['last_name']=$registerData->register_last_name;
     $user_data['User']['dob']=date("Y-m-d",strtotime($registerData->register_dob));
     $user_data['User']['role_id']=4;
     $user_data['User']['status_id']=1;
    // debug($response);
	 $restult_users=$this->User->find('first',array('conditions'=>array('username'=>$registerData->register_username)));
	// debug($restult_users);
	 if(!empty($restult_users)){
		  $response['response']='email_exist';
	 }else{
     	$this->User->SaveAll($user_data);     
     	$response['response']='success';
	 }
     echo json_encode($response);
     exit;
     
 }
 
 public function submit_forgot_password(){     
        $this->autoRender = false;
        $forgotData=json_decode($_REQUEST['forgotData']);
        $this->request->data['User']['username']=$forgotData->forgot_email;
        $email_id=$forgotData->forgot_email;
        $restult_users=$this->User->find('first',array('conditions'=>array('username'=>$email_id)));
        if(!empty($restult_users)){
            $token = md5(uniqid(rand(111111,9999999999),true));
            $user_token=array('user_email'=>$restult_users['User']['username'],'user_id'=>$restult_users['User']['id'],'token'=>$token,'status_id'=>1);
            $this->UserReset->save($user_token);
            $email_id_usr=$restult_users['User']['email_id'];
       
     
            $parameters="?user_id=".base64_encode($restult_users['User']['id'])."&token=".$token."&email_id=".base64_encode($email_id);
	 	    $reset_url=Configure::read('app_root_path')."pages/reset_password".$parameters;
        
     	//Email to customer for successful placing of enquiry.
	 	    $message_success="We have received your forgot password request. Please follow the link to reset the password.";
            $subject="Reset password- Coorgexpress.com";
		    $EmailCustomerSS = new CakeEmail();	
		    $EmailCustomerSS->config('gmail');
		    $EmailCustomerSS->config(array('from' => 'coorgexpressbooking@gmail.com'));
		   $EmailCustomerSS->viewVars(array(
			 					'heading' => "Password Reset!",
								'message'=>$message_success,
							   	'display_style' =>'',
							   	'button_url' =>$reset_url,
							 	'button_text' =>'Reset Password'								
								
							   )); 
		   $EmailCustomerSS->template('generic_response',null)
								->emailFormat('html')
								->to($email_id_usr) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();
            $response['response']='success';
            echo json_encode($response); 
            exit;
                                              
      }else{
        $response['response']='failed';
        echo json_encode($response); 
            exit;
    }
     
 }
public function save_current_url(){
   
    $this->autoRender = false;
     if($this->request->is('post')) {
        $dataString=json_decode($_REQUEST['dataString']);
        $current_url=$dataString->current_url;
        $decoded_url = rawurldecode($current_url);
       
        
        $this->Session->write('social_redirect_url', $decoded_url);
        
         
        
         $response['response']='success';
         
      }
    echo json_encode($response);
            exit;
            
     
 }
 public function reset_password(){
          $this->autoRender = false;
          $resetData=json_decode($_REQUEST['resetPassData']);
              
          $this->request->data['UserReset']['username']=$resetData->username;
          $this->request->data['UserReset']['password']=$resetData->password;
          $this->request->data['UserReset']['token']=$resetData->token;
          $this->request->data['UserReset']['user_id']=$resetData->user_id;
          $this->request->data['UserReset']['id']=$resetData->user_reset_id;
     
          $this->User->set( $this->request->data['UserReset']);
            if ($this->User->validates()) {
                $reset['UserReset']['token']=$this->request->data['UserReset']['token'];
                $reset['UserReset']['status']='0';
                $reset['UserReset']['id']=$this->request->data['UserReset']['id'];
                $datauser=array();
                $datauser['User']['password']=$this->request->data['UserReset']['password'];
                $datauser['User']['id']=$this->request->data['UserReset']['user_id'];
                if($this->User->saveAll($datauser))
                {
                   $this->UserReset->deleteAll(array('UserReset.token'=>$this->request->data['UserReset']['token']));
                    $response['response']='success';
                    echo json_encode($response); 
                    exit;
                  
                }                

            }
            else
            {
                $errors = $this->User->validationErrors;
                $response['response']='failed';
                echo json_encode($response); 
                exit;
            }
 }

 public function forgot() {
    $this->autoRender = false;
    if(!empty($this->request->data)){
        $email_id=$this->request->data['email_id'];
        $restult_users=$this->User->find('first',array('conditions'=>array('username'=>$email_id)));
        if(!empty($restult_users))
            {
                $this->UserReset->deleteAll(array('UserReset.user_email'=>$restult_users['User']['username']));
                $token = md5(uniqid(rand(),true));
                $user_token=array('user_email'=>$restult_users['User']['username'],'token'=>$token,'status_id'=>1);
                $this->UserReset->save($user_token);
                $body="Dear ".$restult_users['User']['full_name'].",\r\n Click on the below link to reset your password.\r\n".Configure::read('nuwed_root_path').'users/reset/'.$token."\r\nRegards,\r\nNuwed.";
                $Email = new CakeEmail('default');
                $Email->from(array('admin@nuwed.com' => 'Nuwed'));
                $Email->to($restult_users['User']['username']);
                $Email->subject('Reset Password - Nuwed');
                $Email->send($body);
                return json_encode(true);
            }
            else
            {
                return json_encode(false);
            }
    }
}

 public function logout() {
		$this->autoRender = false;
	 
	 	$user_id =$this->Auth->user('id');
	  	$restult_users=$this->User->find('first',array('conditions'=>array('id'=>$user_id)));
	 	if(!empty($restult_users))
                {
                    $role_id=$restult_users['User']['role_id'];
		}
		$this->Session->delete('User');
                $this->Session->delete('Auth');
	 	$this->Auth->logout();
	 	if($role_id==1 || $role_id==2 || $role_id==3){
			return $this->redirect(array('controller'=>'users','action'=>'login'));
		}else{
                    //$redirect_url = $this->Session->read('social_redirect_url');
                    //$this->redirect($redirect_url);
                    return $this->redirect(array('controller'=>'index','action'=>'index'));
		}
		//$this->redirect($this->Auth->logout());
	}
}
?>