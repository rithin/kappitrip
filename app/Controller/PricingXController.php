<?php
App::uses('AppController', 'Controller');

/** 
 *This controller is for finding the best price for the homestay.
 * 
 */
class PricingXController extends AppController {
	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','Experience','RoomPriceDetail','BedroomDetail', 'OccupancyDetail', 'ExperienceBookingDetail', 'BookingEnquiry', 'DiscountCouponCode','BookingOrderDetail', 'BookingPaymentResponse', 'PriceX', 'PriceXHoliday','PriceXSeasonal','Deal');
	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
    
    public function bestPrice(){
		$this->autoRender=false;
		date_default_timezone_set('UTC');
        $jsonData=json_decode($_REQUEST['priceXJson']);
		//debug($jsonData);
		$check_in_date=date('Y-m-d', strtotime($jsonData->check_in_date));
		$check_out_date=date('Y-m-d', strtotime($jsonData->check_out_date));;
		$deal_id=$jsonData->deal_id;
		$homestay_id=$jsonData->homestay_id;
		$base_price_per_day=$jsonData->base_price_per_day;
		$special_price_applied_dates=array();
		$special_priced_dates=array();
		$final_price=0; $total_booking_special_price=0;
		$dataPriceX=$this->PriceX->find('first',array('conditions'=>array('PriceX.homestay_id'=>$homestay_id)));	
				
		//Taking the avg, variable price percentage.
		if(count($dataPriceX)>0){
			$avg_variable_price=$dataPriceX['PriceX']['avg_variable_price'];
		}
		//find the deal price
		//PRIORITY : 1 DEALS
		if($deal_id!=""){
			$dataDeals=$this->Deal->find('first',array('conditions'=>array('Deal.homestay_id'=>$homestay_id,'Deal.id'=>$deal_id )));
			$offer_type=$dataDeals['Deal']['offer_type'];
			$offer_value=$dataDeals['Deal']['offer_value'];
			$deal_start_date = date('Y-m-d', strtotime($dataDeals['Deal']['start_date']));
			$deal_end_date = date('Y-m-d', strtotime($dataDeals['Deal']['end_date']));
			$check_in_date_deal=$check_in_date;
			$check_out_date_deal=$check_out_date;
			if (( $deal_end_date >= $check_in_date_deal) && ($deal_start_date < $check_out_date_deal)){
				$deal_dates_arr=$this->splitDates($deal_start_date, $deal_end_date);					
				while (strtotime($check_in_date_deal) < strtotime($check_out_date_deal)) {	
					if(in_array($check_in_date_deal,$deal_dates_arr)){	
							//echo "deal inside==>".$check_in_date_deal;
						$data_price_change=array('type'=>'deal','special_price_date'=>$check_in_date_deal, 'operator'=>'-', 'price_type'=>$offer_type, 'price_type_value'=>$offer_value);
							array_push($special_price_applied_dates,$data_price_change);
							array_push($special_priced_dates,$check_in_date_deal);
						}
					$check_in_date_deal = date ("Y-m-d", strtotime("+1 day", strtotime($check_in_date_deal)));				
				}
			}
		}
		//debug($special_priced_dates);
		if(count($dataPriceX)>0){
			//finding the requested date falls on any seasonal price set by stay owner.
			//PRIORITY : 2 SEASONAL PRCING
			if(count($dataPriceX['PriceXSeasonal'])>0 ){
				foreach($dataPriceX['PriceXSeasonal'] as $key=>$PriceXSeasonal){
					$check_in_date_temp=$check_in_date;
					$check_out_date_temp=$check_out_date;
					$season_start_date = date('Y-m-d', strtotime($PriceXSeasonal['season_start_date']));
					$season_end_date = date('Y-m-d', strtotime($PriceXSeasonal['season_end_date']));			
					if (( $season_end_date >= $check_in_date_temp) && ($season_start_date < $check_out_date_temp)){
						//find the dates which user queried are applied for the special price.
						$season_dates_arr=$this->splitDates($season_start_date, $season_end_date);
						while (strtotime($check_in_date_temp) < strtotime($check_out_date_temp)) {	

							if(in_array($check_in_date_temp,$season_dates_arr)){	
								if(!in_array($check_in_date_temp,$special_priced_dates)){
									$data_price_change=array('type'=>'seasonal','special_price_date'=>$check_in_date_temp, 'operator'=>$PriceXSeasonal['season_operator'],'price_type'=>'percentage', 'price_type_value'=>$PriceXSeasonal['season_price_percent']);
									array_push($special_price_applied_dates,$data_price_change);
									array_push($special_priced_dates,$check_in_date_temp);
								}
							}
							$check_in_date_temp = date ("Y-m-d", strtotime("+1 day", strtotime($check_in_date_temp)));				
						}
					 }else{
						continue;
					 }
				}
			}
			//debug($special_priced_dates);
			//finding the requested date falls on any holiday price set by stay owner.
			//PRIORITY : 3 HOLIDAY PRCING
			if(count($dataPriceX['PriceXHoliday'])>0){
				foreach($dataPriceX['PriceXHoliday'] as $key=>$PriceXHoliday){
					$check_in_date_temp=$check_in_date;
					$check_out_date_temp=$check_out_date;
					$holiday_start_date = date('Y-m-d', strtotime($PriceXHoliday['holiday_start_date']));	
					if (( $holiday_start_date >= $check_in_date_temp) && ($holiday_start_date < $check_out_date_temp)){
						//echo "holiday inside==>".$holiday_start_date;
						//find the dates which user queried are applied for the special price.
						$selected_dates=$this->splitDates($check_in_date_temp, $check_out_date_temp);
						if(in_array($holiday_start_date,$selected_dates)){
						   if(!in_array($holiday_start_date,$special_priced_dates)){
							$data_price_change=array('type'=>'holiday','special_price_date'=>$holiday_start_date, 'operator'=>$PriceXHoliday['holiday_operator'], 'price_type'=>'percentage', 'price_type_value'=>$PriceXHoliday['holiday_price_percent']);
								array_push($special_price_applied_dates,$data_price_change);
								array_push($special_priced_dates,$holiday_start_date);
							}
						}		
					 }else{
						continue;
					 }
				}
			}
			//debug($special_priced_dates);
			$requested_dates_arr=$this->splitDates($check_in_date, $check_out_date);
			//if no conditons are applied on the date, we are checking if any chosen dates are falling on weekend.
			//PRIORITY : 4 WEEKEND PRCING
			if($avg_variable_price >0 && $dataPriceX['PriceX']['weekend_price_hike']=='Yes'){
				foreach($requested_dates_arr as $lineDate){	
				 if($check_out_date > $lineDate){
					if(!in_array($lineDate,$special_priced_dates)){
						$timestamp = strtotime($lineDate);
						$day = date('D', $timestamp);
						if($day=='Sat' || $day=='Sun'){
							$data_price_change=array('type'=>'weekend','special_price_date'=>$lineDate, 'operator'=>'+', 'price_type'=>'percentage', 'price_type_value'=>$avg_variable_price);
							array_push($special_price_applied_dates,$data_price_change);
							array_push($special_priced_dates,$lineDate);
						}
					 }
				 }
			   }
			}
		
			//debug($special_priced_dates);
			// the dates which are not having any special price is adding to the array with orginal price for average calculations
			foreach($requested_dates_arr as $lineDate){	
				if($check_out_date > $lineDate){
					if(!in_array($lineDate,$special_priced_dates)){
						$data_price_change=array('type'=>'no_special_price','special_price_date'=>$lineDate, 'operator'=>'+', 'price_type'=>'percentage', 'price_type_value'=>0);
						array_push($special_price_applied_dates,$data_price_change);
					}
				}
			}
	   }
			
		//debug($special_price_applied_dates);
		if(count($special_price_applied_dates)>0){
			//finding the price for each day
			foreach($special_price_applied_dates as $key=>$per_date_detail){
				$special_price_date=$per_date_detail['special_price_date'];
				$price_type=$per_date_detail['price_type'];
				$price_type_value=$per_date_detail['price_type_value'];
				$operator=$per_date_detail['operator'];
				
				if($price_type=='percentage')
					$price_variable_amount=(($base_price_per_day*$price_type_value)/100);
				else
					$price_variable_amount=$price_type_value;
				
				if($operator=='+')
				 $total_booking_special_price+= ($base_price_per_day + $price_variable_amount);
				else
				 $total_booking_special_price+= ($base_price_per_day - $price_variable_amount);			
			}
			//finding the per day avg price based on special price.
			$final_price=($total_booking_special_price/count($special_price_applied_dates));
			
			 return number_format(ceil($final_price),2);
		}else{
			
			 return number_format(ceil($base_price_per_day),2);
		}
    }
    
   public function splitDates($start_date, $end_date) {	
		 date_default_timezone_set('UTC');
		 $start_date=date ("Y-m-d", strtotime($start_date)); 
		 $end_date=date ("Y-m-d", strtotime($end_date)); 
		 $aDays[] = date ("Y-m-d", strtotime($start_date)); 		
		 while (strtotime($start_date) < strtotime($end_date)) {  
		    $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
			$aDays[] = $start_date;  
			// echo "test";
		 }		
		//debug($aDays);
		 return $aDays;         
    }
	
	
}
?>