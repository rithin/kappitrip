<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */


class BookingController extends AppController {
    
	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail', 'OccupancyDetail','Amenity','HomestayAmenity','BookingOrderDetail','BookingPaymentResponse','Experience','ExperienceFile', 'ExperienceLanguage','ExperienceType','ExperienceSlot','ExperienceBookingDetail','DiscountCouponCode');
     var $components = array('Encryption','General','Cookie');
	
	
	public function beforeFilter()
	{
        parent::beforeFilter();
        $this->Auth->allow();
	}
	public function index(){       
        $conditions=array(); $temp_order_id=""; $child_charges=0;
		$temp_order_id= $this->Cookie->read('0.OrderInfo.Order.order_id');
        $identity= $this->Cookie->read('0.know_the_bae.identity');
		if($identity!=""){
			$know_the_bae_identity=$identity;
            if($temp_order_id==""){
                $temp_order_id=md5(uniqid(rand(), true));
            }else{
                 $temp_order_id=$temp_order_id;
            }
		}else{
			$know_the_bae_identity=rand('111111','999999999');
		}
         if(count($_REQUEST)>0){            
            $startDate= $_REQUEST['startDate'];
            $endDate= $_REQUEST['endDate'];
            $room_id_selected=$_REQUEST['room_id_selected'];
            $homestay_id=$_REQUEST['homestay_id'];
            $qtyInputAdult=$_REQUEST['qtyInputAdult'];
            $qtyInputInfant=$_REQUEST['qtyInputInfant'];
            $qtyInputChild=$_REQUEST['qtyInputChild'];
            $total_amount=$_REQUEST['total_amount'];
			@$enquiry_accept_reject=$_REQUEST['enquiry_accept_reject'];
			if($enquiry_accept_reject=='accepted'){
				$room_ids=json_decode($_REQUEST['room_ids']);
			}else{
				$room_ids=$_REQUEST['room_ids'];
			}
             
			$flagSameRoomAvail=$_REQUEST['flagSameRoomAvail'];
          //  $searchId=base64_decode($_REQUEST['searchId']);
            $no_of_days=round ((strtotime($endDate)-strtotime($startDate))/(60 * 60 * 24));
			if($flagSameRoomAvail=="no"){
				$grand_total_for_room=($total_amount);
			}else {
				$grand_total_for_room=($total_amount*$no_of_days);
			}
             $slctd_rooms_array=explode("_",$room_id_selected);
            // debug($_REQUEST['rooms']);
             if(!isset($_REQUEST['rooms']) || count($_REQUEST['rooms'])==0){
                 $rooms=$slctd_rooms_array;
             }else{
			     @$rooms=$_REQUEST['rooms'];
             }
             
          //  debug($rooms);
			 
			if(isset($_REQUEST['unique_email_code'])){
				 $unique_email_code=$_REQUEST['unique_email_code'];
			 }else{
				$unique_email_code="";
			}
			$this->set('unique_email_code',$unique_email_code); 
            $this->set('temp_order_id',$temp_order_id); 
            $this->set('startDate',$startDate);
            $this->set('endDate',$endDate);
            $this->set('room_id_selected',$room_id_selected);          
            $this->set('qtyInputAdult',$qtyInputAdult);
            $this->set('qtyInputInfant',$qtyInputInfant);
            $this->set('qtyInputChild',$qtyInputChild);
            $this->set('total_amount',$total_amount);
            $this->set('grand_total_for_room',$grand_total_for_room);
            $this->set('no_of_days',$no_of_days);
            $this->set('homestay_id',$homestay_id);
			$this->set('room_ids',$room_ids);
			$this->set('flagSameRoomAvail',$flagSameRoomAvail);
            $conditions=array("Homestay.id "=> $homestay_id);
             
            $OccupancyDetail=$this->OccupancyDetail->find('all',array('conditions'=> array('OccupancyDetail.order_id'=>$temp_order_id), 'fields'=>array('order_id','homestay_id')));
             
			if((count($rooms)>0) && ($know_the_bae_identity!="") && (count($OccupancyDetail)==0) ){
				foreach($rooms as $key=>$value){
					$dataOccupancy[$key]['OccupancyDetail']['room_price_detail_id']=$value;
					$dataOccupancy[$key]['OccupancyDetail']['order_id']=$temp_order_id;
					$dataOccupancy[$key]['OccupancyDetail']['homestay_id']=$homestay_id;
					$dataOccupancy[$key]['OccupancyDetail']['check_in_date']=$startDate;           
					$dataOccupancy[$key]['OccupancyDetail']['check_out_date']=$endDate;
					$dataOccupancy[$key]['OccupancyDetail']['status']=4;
				}
				//debug($dataOccupancy);				
				$this->OccupancyDetail->saveAll($dataOccupancy);
                $this->Cookie->write('.OrderInfo.Order.room',$room_ids);
                $this->Cookie->write('.OrderInfo.Order.order_id',$temp_order_id);
               
			}
        
         $this->Homestay->recursive=2;
         $this->HomestayAmenity->unbindModel(array('belongsTo' => array('Homestay')));
         $this->HomestayFile->unbindModel(array('belongsTo' => array('Homestay')));
         $this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
         $homestayDetails=$this->Homestay->find('first', array(
			'joins' => array(
				array(
					'table' => 'homestay_files',
					'alias' => 'HomestayFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayFile.homestay_id'
					)
				),
                array(
					'table' => 'homestay_amenities',
					'alias' => 'HomestayAmenity',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayAmenity.homestay_id'
					)
				),
                array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),
				array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Homestay.location_id'
					)
				),

			),
            'conditions'=>$conditions,
			'fields' => array('Homestay.*','HomestayFile.file_name','HomestayFile.file_path','Location.name'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.created DESC'
		));
		//debug($homestayDetails);
        unset($homestayDetails['HomestayFile']['file_name']);
        unset($homestayDetails['HomestayFile']['file_path']);
        $this->set('homestayDetails',$homestayDetails);	
		$child_charges=$qtyInputChild *$homestayDetails['Homestay']['per_child_charge'];
        $this->set('child_charges',$child_charges);  

		if(($this->Session->read('Auth.User.id')!="") && ($this->Session->read('Auth.User.id')>0)){
        	$logged_in_user_id=$this->Session->read('Auth.User.id');
        	$current_user=$this->User->find('first',array('conditions'=>array('User.id'=>$logged_in_user_id)));
			$this->request->data['Booking']['billing_name']=ucfirst($current_user['User']['first_name'])." ".ucfirst($current_user['User']['last_name']);
			$this->request->data['Booking']['billing_email']=$current_user['User']['email_id'];
		}
		
		$this->set('meta_title',$homestayDetails['Homestay']['meta_title']);
		$this->set('meta_keyword',$homestayDetails['Homestay']['meta_keyword']);
		$this->set('meta_description',$homestayDetails['Homestay']['meta_description']);
       }
				
   }
    
  public function experience(){        
         $conditions=array();   
		//debug($_REQUEST);die;
         if(count($_REQUEST)>0){ 
			 
			if(@$_REQUEST['experience_type']==5){
				$experience_id = $_REQUEST['experience_id'];
				if(@$_REQUEST['eventdate']!="")
					$date = $_REQUEST['eventdate'];
				else
					 $date = $_REQUEST['date'];
				
				if(@$_REQUEST['event_price']!="")
					$unit_price = $_REQUEST['event_price'];
				else
					$unit_price = $_REQUEST['price'];
				
				$slot_start_time = $_REQUEST['slot_start_time'];
				if(@$_REQUEST['adultCount']!="")
					$no_of_guest = $_REQUEST['adultCount'];
				else
					$no_of_guest = $_REQUEST['no_of_guest'];
				
				if(@$_REQUEST['adultCount']!="")
					$no_of_people = $_REQUEST['adultCount'];	
				else
					$no_of_guest = $_REQUEST['no_of_guest'];
				$experience_slot_id = $_REQUEST['experience_slot_id'];
				$min_guest = 1;
				//if no of requested guest are less than min guest the price will automatically increase
				//this will be new perhead charges
				$final_price=$this->findFinalExperiencePrice($no_of_guest,$min_guest,$unit_price);
				
				$grand_total=($final_price*$no_of_guest);
				
				$this->set('experience_slot_id',$experience_slot_id);
				$this->set('experience_id',$experience_id);
				$this->set('unit_price',$final_price);
				$this->set('date',$date);
				$this->set('slot_start_time',$slot_start_time);
				$this->set('slot_end_time',"");
				$this->set('no_of_people',$no_of_people);
				$this->set('no_of_guest',$no_of_guest);
				$this->set('grand_total',$grand_total);
				
			}else{
				$experience_slot_id = $_REQUEST['experience_slot_id'];
				$experience_id = $_REQUEST['experience_id'];
				$unit_price = $_REQUEST['price'];
				$date = $_REQUEST['date'];
				$slot_start_time = $_REQUEST['slot_start_time'];
				$slot_end_time =$_REQUEST['slot_end_time'];
				
				if(@$_REQUEST['data']['Booking']['no_of_guest']!="")
					@$no_of_people =$_REQUEST['data']['Booking']['no_of_guest'];
				else
					$no_of_people =$_REQUEST['no_of_guest'];
				if(@$_REQUEST['data']['Booking']['no_of_guest']!="")
					@$no_of_guest = $_REQUEST['data']['Booking']['no_of_guest'];
				else
					$no_of_guest = $_REQUEST['no_of_guest'];
				$min_guest = $_REQUEST['min_guest'];
				
				//if no of requested guest are less than min guest the price will automatically increase
				//this will be new perhead charges
				$final_price=$this->findFinalExperiencePrice($no_of_guest,$min_guest,$unit_price);
				
				$grand_total=($final_price*$no_of_guest);

				$this->set('experience_slot_id',$experience_slot_id);
				$this->set('experience_id',$experience_id);
				$this->set('unit_price',$final_price);
				$this->set('date',$date);
				$this->set('slot_start_time',$slot_start_time);
				$this->set('slot_end_time',$slot_end_time);
				$this->set('no_of_people',$no_of_people);
				$this->set('no_of_guest',$no_of_guest);
				$this->set('grand_total',$grand_total);
			}
         }
      //debug($_REQUEST);
        $today=date('Y-m-d');
        $this->Experience->recursive=2;
        $this->Experience->unbindModel(array('hasMany' => array('ExperienceSlot')));
        $this->ExperienceFile->unbindModel(array('belongsTo' => array('Experience')));
        $this->ExperienceLanguage->unbindModel(array('belongsTo' => array('Experience')));
        $this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));        
        unset($this->ExperienceSlot->virtualFields['no_of_slots']);
        $conditions=array("Experience.id "=> $experience_id);    
        $experienceDetails=$this->Experience->find('first', array(
			'joins' => array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),  
				array(
					'table' => 'experience_slots',
					'alias' => 'ExperienceSlot',
					'type'  => 'LEFT',
					'conditions' => array(
						"Experience.id = ExperienceSlot.experience_id "
					)
				),

			),
            'conditions'=>$conditions,
			'fields' => array('Experience.*','ExperienceSlot.*'),
            'group' => '`Experience`.`id`',
			'order' => 'Experience.created DESC'
		));
		
		$time1 = strtotime($experienceDetails['ExperienceSlot']['slot_start_time']);
		$time2 = strtotime($experienceDetails['ExperienceSlot']['slot_end_time']);
		$duration = round(abs($time2 - $time1) / 3600,2);	
            
        $this->set('experienceDetails',$experienceDetails);	
		
		if(($this->Session->read('Auth.User.id')!="") && ($this->Session->read('Auth.User.id')>0)){
        	$logged_in_user_id=$this->Session->read('Auth.User.id');
        	$current_user=$this->User->find('first',array('conditions'=>array('User.id'=>$logged_in_user_id))); 
			$this->request->data['Booking']['billing_name']=ucfirst($current_user['User']['first_name'])." ".ucfirst($current_user['User']['last_name']);
			$this->request->data['Booking']['billing_email']=$current_user['User']['email_id'];
		}
		
		
		$this->set('meta_title',$experienceDetails['Experience']['meta_title']);
		$this->set('meta_keyword',$experienceDetails['Experience']['meta_keyword']);
		$this->set('meta_description',$experienceDetails['Experience']['meta_description']);
      
    }
	
	public function applyCoupon($coupon_code=null,$amount=null,$home_stay_id=null,$expereince_id=null){
		$this->autoRender=false;// ajax method 
		$couponData=json_decode($_REQUEST['couponDataJson']);    
        $coupon_code=$couponData->coupon_code_customer;
		$grand_total_for_room=$couponData->grand_total_for_room;
		$cleaning_charges=$couponData->cleaning_charges;
		
		$condition=array("coupon_code"=> $coupon_code,'status'=>1);
        $couponDataArray=$this->DiscountCouponCode->find('first', array('conditions'=>$condition, 'fields'=>array('DiscountCouponCode.*'))); 
		if(count($couponDataArray)>0){
			if($couponDataArray['DiscountCouponCode']['discount_type']=="percentage"){
				$discount_value=$couponDataArray['DiscountCouponCode']['value'];
				$discount_amount=(($grand_total_for_room*$discount_value)/100);
				$final_discounted_amount=(($grand_total_for_room-$discount_amount)+$cleaning_charges );
				
				$couponDataArray['DiscountCouponCode']['discount_amount']=number_format((float)($discount_amount), 2, '.', ',');
				$couponDataArray['DiscountCouponCode']['final_discounted_amount']=number_format((float)($final_discounted_amount), 2, '.', ',');
			}else if($couponDataArray['DiscountCouponCode']['discount_type']=="amount"){
				$discount_value=$couponDataArray['DiscountCouponCode']['value'];				
				$final_discounted_amount=(($grand_total_for_room-$discount_value)+$cleaning_charges);
				
				$couponDataArray['DiscountCouponCode']['discount_amount']=number_format((float)($discount_value), 2, '.', ',');
				$couponDataArray['DiscountCouponCode']['final_discounted_amount']=number_format((float)($final_discounted_amount), 2, '.', ',');
			}
			return json_encode($couponDataArray['DiscountCouponCode']);
		}
		else{
			return 'error';
		}
		//debug($couponDataArray);
				
	}
	
	public function findFinalExperiencePrice($adultCount=null,$min_guest=null,$price=null){
		 $this->render=false;
		$final_price=0; $min_price=0;
			//if no of requested guest are less than min guest the price will automatically increase
			if($adultCount < $min_guest){
				$min_price=($price*$min_guest);
				$final_price=$min_price/$adultCount; // new per head price
				
			}else{
				$final_price=$price;
			}
		return $final_price;
	}
   
    public function confirm_booking(){
		$_POST['data']['Booking']['amount']=(($_POST['data']['OccupancyDetails']['grand_total_for_room']-$_POST['data']['OccupancyDetails']['discount_amount']) +($_POST['data']['OccupancyDetails']['cleaning_charges']));
       
		$booking_data=$_POST['data']['Booking'];
        $merchant_data='4538';	
        $working_key=Configure::read('working_key');
        $access_code=Configure::read('access_code');
        foreach ($booking_data as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
		
        $order_id=$_POST['data']['Booking']['order_id'];
        $bookingData[$order_id]['temp_order_id']=$_POST['data']['Booking']['merchant_param1'];// temporary order id is assigned to merchan param 1
        $bookingData[$order_id]['startDate']=$_POST['data']['OccupancyDetails']['startDate'];
        $bookingData[$order_id]['endDate']=$_POST['data']['OccupancyDetails']['endDate'];
        $bookingData[$order_id]['room_ids']=$_POST['data']['OccupancyDetails']['room_ids'];
		$bookingData[$order_id]['room_id_selected']=$_POST['data']['OccupancyDetails']['room_id_selected'];
        $bookingData[$order_id]['qtyInputAdult']=$_POST['data']['OccupancyDetails']['qtyInputAdult'];
        $bookingData[$order_id]['qtyInputInfant']=$_POST['data']['OccupancyDetails']['qtyInputInfant'];
        $bookingData[$order_id]['qtyInputChild']=$_POST['data']['OccupancyDetails']['qtyInputChild'];
		
        $bookingData[$order_id]['unit_price']=$_POST['data']['OccupancyDetails']['unit_price'];
		$bookingData[$order_id]['total_room_charge']=$_POST['data']['OccupancyDetails']['grand_total_for_room'];
		
        $bookingData[$order_id]['grand_total_for_room']=(($_POST['data']['OccupancyDetails']['grand_total_for_room']-$_POST['data']['OccupancyDetails']['discount_amount']) +($_POST['data']['OccupancyDetails']['cleaning_charges']));
		
		$bookingData[$order_id]['discount_amount']=$_POST['data']['OccupancyDetails']['discount_amount'];
		$bookingData[$order_id]['cleaning_charges']=$_POST['data']['OccupancyDetails']['cleaning_charges'];
		$bookingData[$order_id]['applied_coupon']=$_POST['data']['OccupancyDetails']['applied_coupon'];
		
        $bookingData[$order_id]['no_of_days']=$_POST['data']['OccupancyDetails']['no_of_days'];
        $bookingData[$order_id]['homestay_id']=$_POST['data']['OccupancyDetails']['homestay_id'];
		$bookingData[$order_id]['billing_email']=$_POST['data']['Booking']['billing_email'];
		$bookingData[$order_id]['special_request']=$_POST['data']['Booking']['special_request'];
        
		//debug($bookingData);
        CakeSession::write('OccupancyDetails',$bookingData);
        $sessionBookingData=CakeSession::read('OccupancyDetails');
       
       // $this->Session->delete("OccupancyDetails");
      // echo $merchant_data;
        $encrypted_data=$this->Encryption->encrypt($merchant_data,$working_key); // Method for encrypting the data.
     //   echo $encrypted_data;
		
		//die;
        $this->set('encrypted_data',$encrypted_data);
        $this->set('access_code',$access_code);
        
        $seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		
		$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);

        
       //die;
 }
public function confirm_experience_booking(){
        
		 if(!isset($_SESSION)){
			 session_start();
		 }
        $booking_data=$_POST['data']['ExperienceBookingDetail'];
        $merchant_data='4538';         
        $working_key=Configure::read('working_key');
        $access_code=Configure::read('access_code');
        foreach ($booking_data as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
        $order_id=$_POST['data']['ExperienceBookingDetail']['order_id'];
        $bookingData[$order_id]['date']=$_POST['data']['ExperienceBookingDetail']['date'];        
        $bookingData[$order_id]['unit_price']=$_POST['data']['ExperienceBookingDetail']['unit_price'];
        $bookingData[$order_id]['no_of_guest']=$_POST['data']['ExperienceBookingDetail']['no_of_guest'];
		 
		$bookingData[$order_id]['discount_amount']=$_POST['data']['ExperienceBookingDetail']['discount_amount'];		
		$bookingData[$order_id]['applied_coupon']=$_POST['data']['ExperienceBookingDetail']['applied_coupon'];
		 
        $bookingData[$order_id]['grand_total']=$_POST['data']['ExperienceBookingDetail']['grand_total'];
        $bookingData[$order_id]['slot_start_time']=$_POST['data']['ExperienceBookingDetail']['slot_start_time'];
        $bookingData[$order_id]['slot_end_time']=$_POST['data']['ExperienceBookingDetail']['slot_end_time'];
        $bookingData[$order_id]['experience_id']=$_POST['data']['ExperienceBookingDetail']['experience_id'];
        $bookingData[$order_id]['experience_slot_id']=$_POST['data']['ExperienceBookingDetail']['experience_slot_id'];
		$bookingData[$order_id]['billing_email']=$_POST['data']['ExperienceBookingDetail']['billing_email'];
		$bookingData[$order_id]['special_request']=$_POST['data']['ExperienceBookingDetail']['special_request'];
        
     //   debug($bookingData);
        CakeSession::write('BookingExperience',$bookingData);
        $sessionBookingData=CakeSession::read('BookingExperience');
      // debug($sessionBookingData);
	//	 die;
       // $this->Session->delete("OccupancyDetails");
       
        $encrypted_data=$this->Encryption->encrypt($merchant_data,$working_key); // Method for encrypting the data.
        //echo $encrypted_data;
        $this->set('encrypted_data',$encrypted_data);
        $this->set('access_code',$access_code);
        
     }
    public function checkout_response(){
        $working_key=Configure::read('working_key');
        $access_code=Configure::read('access_code');
        $encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
        $rcvdString=$this->Encryption->decrypt($encResponse,$working_key);//Crypto Decryption used as per the specified working key. 
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);
		
		//debug($decryptValues);
        $responseDetailArray=array();        
        foreach($decryptValues as $key=>$value){
            $information=explode('=',$value);
            $responseDetailArray[$information[0]]=$information[1];
        }
        $sessionBookingData=CakeSession::read('OccupancyDetails'); 
        $order_status=$responseDetailArray['order_status'];
        $order_id=$responseDetailArray['order_id'];
        $temp_order_id=$responseDetailArray['merchant_param1'];// temporary order id is assigned to merchan param 1
        $billing_email=$responseDetailArray['billing_email'];
        $homestay_id=$sessionBookingData[$order_id]['homestay_id'];
		$room_ids=$sessionBookingData[$order_id]['room_ids'];
        $invoice_no=$this->General->generateInvoiceNo();
		
		
		//debug($sessionBookingData);
        $dataOccupancy=array();
        @$rooms=explode('_',$sessionBookingData[$order_id]['room_id_selected']);
		$room_id_objct_arr=json_decode($room_ids);
		$room_ids=null;$room_id_arr=array(); $oldkeydate=0;
		//debug($rooms);
		//this flag use when same room is not available for multiple dates choosing by customer.
		$flagSameRoomAvail="yes";
		foreach($room_id_objct_arr as $room){
			//debug($room);
			foreach($room as $keydate=>$itemsArr){				
				if($oldkeydate==0){
						$oldkeydate=$keydate;
					}elseif($oldkeydate==$keydate){
						$flagSameRoomAvail="yes";
					}elseif($oldkeydate!=$keydate){
						$flagSameRoomAvail="no";
						
					}
				foreach($itemsArr as $key=>$item){					
					//echo "item---->>".$item;
					$freeroom_ids[$keydate][]=$item;
					$room_ids=$room_ids."_".$item;
					array_push($room_id_arr,$item);
				}
			}
		}
		
		//debug($freeroom_ids);
		//die;
		
        $OccupancyDetail=$this->OccupancyDetail->find('all',array('conditions'=> array('OccupancyDetail.order_id'=>$temp_order_id))); 
       //  debug($OccupancyDetail);
        if(count($OccupancyDetail)>0){
           /* foreach($rooms as $key=>$value){
                $dataOccupancy[$key]['OccupancyDetail']['room_price_detail_id']=$value;
                $dataOccupancy[$key]['OccupancyDetail']['order_id']=$order_id;
                $dataOccupancy[$key]['OccupancyDetail']['homestay_id']=$sessionBookingData[$order_id]['homestay_id'];
                $dataOccupancy[$key]['OccupancyDetail']['check_in_date']=$sessionBookingData[$order_id]['startDate'];           
                $dataOccupancy[$key]['OccupancyDetail']['check_out_date']=$sessionBookingData[$order_id]['endDate'];
                $dataOccupancy[$key]['OccupancyDetail']['status']=14;
            }
            */
            foreach($OccupancyDetail as $key=>$dataArr){
                $dataOccupancy[$key]['OccupancyDetail']['id']=$dataArr['OccupancyDetail']['id'];
                $dataOccupancy[$key]['OccupancyDetail']['room_price_detail_id']=$dataArr['OccupancyDetail']['room_price_detail_id'];
                $dataOccupancy[$key]['OccupancyDetail']['order_id']=$order_id;
                $dataOccupancy[$key]['OccupancyDetail']['homestay_id']=$dataArr['OccupancyDetail']['homestay_id'];
                $dataOccupancy[$key]['OccupancyDetail']['check_in_date']=$dataArr['OccupancyDetail']['check_in_date'];          
                $dataOccupancy[$key]['OccupancyDetail']['check_out_date']=$dataArr['OccupancyDetail']['check_out_date'];
                $dataOccupancy[$key]['OccupancyDetail']['status']=14;
               
            }
            
            //debug($dataOccupancy); 
			//die;
            $this->OccupancyDetail->saveAll($dataOccupancy);
            $lastInsertOccupancyDetailID = $this->OccupancyDetail->getLastInsertID();  
            
            $bookingOrderDetails['BookingOrderDetail']['order_id']=$order_id;
            $bookingOrderDetails['BookingOrderDetail']['order_date']=date('Y-m-d h:i:sa');
            $bookingOrderDetails['BookingOrderDetail']['occupancy_detail_id']=$lastInsertOccupancyDetailID;
            $bookingOrderDetails['BookingOrderDetail']['homestay_id']=$sessionBookingData[$order_id]['homestay_id'];
            $bookingOrderDetails['BookingOrderDetail']['room_price_detail_id']=$sessionBookingData[$order_id]['room_id_selected'];
            $bookingOrderDetails['BookingOrderDetail']['no_of_adults']=$sessionBookingData[$order_id]['qtyInputAdult'];
            $bookingOrderDetails['BookingOrderDetail']['no_of_children']=$sessionBookingData[$order_id]['qtyInputChild'];
            $bookingOrderDetails['BookingOrderDetail']['no_of_infants']=$sessionBookingData[$order_id]['qtyInputInfant'];
            $bookingOrderDetails['BookingOrderDetail']['no_of_days']=$sessionBookingData[$order_id]['no_of_days'];
			
			$bookingOrderDetails['BookingOrderDetail']['total_room_charge']=$sessionBookingData[$order_id]['total_room_charge'];
			//$bookingOrderDetails['BookingOrderDetail']['unit_total_per_night']=$sessionBookingData[$order_id]['unit_total_per_night'];
			$bookingOrderDetails['BookingOrderDetail']['cleaning_charges']=$sessionBookingData[$order_id]['cleaning_charges'];
			$bookingOrderDetails['BookingOrderDetail']['applied_coupon']=$sessionBookingData[$order_id]['applied_coupon'];
			$bookingOrderDetails['BookingOrderDetail']['discount_amount']=$sessionBookingData[$order_id]['discount_amount'];
			
            $bookingOrderDetails['BookingOrderDetail']['quantity']=count($rooms);
            $bookingOrderDetails['BookingOrderDetail']['total_price']=$sessionBookingData[$order_id]['grand_total_for_room']; 
			$bookingOrderDetails['BookingOrderDetail']['special_request']=$sessionBookingData[$order_id]['special_request']; 
			$bookingOrderDetails['BookingOrderDetail']['status']=14;
            
            $this->BookingOrderDetail->saveAll($bookingOrderDetails);            
            $lastInsertBookingOrderDetailID = $this->BookingOrderDetail->getLastInsertID(); 
            
           
            
       
        // Email with invoice should send from here... 
		######################################### SUCCESS EMAIL SECTION BEGINS ################################################
     
		 $slctd_room_ids= implode(',',$rooms);
		 $conditions=array("Homestay.id "=> $sessionBookingData[$order_id]['homestay_id']);
		 $this->Homestay->recursive=1;
         $this->Homestay->unbindModel(array('hasMany' => array('HomestayAmenity')));
         $this->Homestay->unbindModel(array('hasMany' => array('HomestayFile')));
         $this->RoomPriceDetail->unbindModel(array('belongsTo' => array('Homestay')));
         $homestayDetails=$this->Homestay->find('first', array(
			'joins' => array(
				
                array(
					'table' => 'homestay_amenities',
					'alias' => 'HomestayAmenity',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = HomestayAmenity.homestay_id'
					)
				),
                array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id', "RoomPriceDetail.id IN ($slctd_room_ids)"
					)
				),
				 array(
					'table' => 'room_types',
					'alias' => 'RoomType',
					'type'  => 'LEFT',
					'conditions' => array(
						'RoomType.id = RoomPriceDetail.room_type_id'
					)
				),
    
			),
            'conditions'=>$conditions,
			'fields' => array('Homestay.*','HomestayAmenity.*'),
            'group' => '`Homestay`.`id`',
			'order' => 'Homestay.created DESC'
		 ));
		
		 @$agent_commission_percentage=$homestayDetails['Homestay']['agent_commission'];
		 if($agent_commission_percentage==""){
			 $agent_commission_percentage=Configure::read('agent_commission_percentage'); 
		 }	
			
		 $BookingPaymentResponseArr['BookingPaymentResponse']= $responseDetailArray;
         $BookingPaymentResponseArr['BookingPaymentResponse']['booking_order_detail_id']= $lastInsertBookingOrderDetailID; 
         $BookingPaymentResponseArr['BookingPaymentResponse']['homestay_id']= $sessionBookingData[$order_id]['homestay_id'];
		 $BookingPaymentResponseArr['BookingPaymentResponse']['agent_commission']= $agent_commission_percentage;
		 $BookingPaymentResponseArr['BookingPaymentResponse']['user_id']=$this->Session->read('Auth.User.id');
         $BookingPaymentResponseArr['BookingPaymentResponse']['status']=14;
         $this->BookingPaymentResponse->saveAll($BookingPaymentResponseArr);  	
			
		 
		$this->RoomPriceDetail->unbindModel(array('belongsTo' => array('RoomType')));
		$roomPriceDetails=$this->RoomPriceDetail->find('all',array(				
				'conditions'=>array("RoomPriceDetail.id IN($slctd_room_ids)", "RoomPriceDetail.status"=>1 ),
				'fields'=>array('RoomPriceDetail.*')));
		//debug($roomPriceDetails);
	//	$log = $this->RoomPriceDetail->getDataSource()->getLog(false, false); 
//debug($log);
		 $room_dtls="";
		 foreach($roomPriceDetails as $room_price_details){
			 if(is_array($room_price_details)){
				 $room_dtls.=$room_price_details['RoomPriceDetail']['room_no_name']." with ".$room_price_details['RoomPriceDetail']['no_of_bed']. " beds ";
				 if($room_price_details['RoomPriceDetail']['extra_bed']>0){
					 $room_dtls.= "  and ".$room_price_details['RoomPriceDetail']['extra_bed']." beds. ";
				 }
				  $room_dtls.="<br/>";
			 }
		 }
		 $host_name=$homestayDetails['Homestay']['owner_name'];
		 if($homestayDetails['Homestay']['contact_person_name']!="")
			 $host_name.=" / ".$homestayDetails['Homestay']['contact_person_name'];
		 
		 $discount_coupon="";
		 if($sessionBookingData[$order_id]['applied_coupon']!="")
		 $discount_coupon=$sessionBookingData[$order_id]['applied_coupon']."- INR".$sessionBookingData[$order_id]['discount_amount'];
		
		 //google maps with api key querying with latitude and longitude
		 $latitude=$homestayDetails['Homestay']['latitude'];
		 $longitude=$homestayDetails['Homestay']['longitude'];
		 $google_maps="https://www.maps.google.com/?&q=$latitude,$longitude";
		 
		 $subject="Booking Confirmed at ".$homestayDetails['Homestay']['name']." from ".$sessionBookingData[$order_id]['startDate']." to ".$sessionBookingData[$order_id]['endDate'];
		  
		 $customer_email=$sessionBookingData[$order_id]['billing_email'];
		 $host_email=$homestayDetails['Homestay']['email'];
		 
		 
		 $agent_commission_amount=(($responseDetailArray['amount']* $agent_commission_percentage)/ 100);
		
		 //Email to customer on confirmed booking....
		 $EmailCustomer = new CakeEmail();	
		 $EmailCustomer->config('gmail');
		 $EmailCustomer->config(array('from' => 'coorgexpressbooking@gmail.com'));
		 $EmailCustomer->viewVars(array(
			 					'homestay_name' => $homestayDetails['Homestay']['name'],
								'invoice_no'=>$invoice_no,
							   	'customer_name' => $responseDetailArray['billing_name'],
							   	'no_of_adult' => ($sessionBookingData[$order_id]['qtyInputAdult'] +$sessionBookingData[$order_id]['qtyInputChild']),
							 	'no_of_children' => $sessionBookingData[$order_id]['qtyInputInfant'],
								'check_in_date'=>$sessionBookingData[$order_id]['startDate'],
								'check_out_date'=>$sessionBookingData[$order_id]['endDate'],
								'cust_email_telphone'=>$responseDetailArray['billing_tel'],
								'room_dtls'=>$room_dtls,
								'meal_plan' => $homestayDetails['Homestay']['meal_plans'],
                                'inclusions' => $homestayDetails['Homestay']['inclusions'],
								'amount_paid' =>$responseDetailArray['amount'],  
								'discount' =>$discount_coupon,
								'special_request'=>$sessionBookingData[$order_id]['special_request'],
								'host_name'=>$host_name,
								'host_telephone'=>$homestayDetails['Homestay']['telephone'],
			 					'google_maps'=>$google_maps								
							   )); 
		 $EmailCustomer->template('booking_confirmation_customer',null)
								->emailFormat('html')
								->to($customer_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();

		 
		 ///Email to host on confirmed booking...		 
		 $EmailHost = new CakeEmail();	
		 $EmailHost->config('gmail');
		 $EmailHost->config(array('from' => Configure::read('smtp_from_email')));
		 $EmailHost->viewVars(array(
			 					'homestay_name' => $homestayDetails['Homestay']['name'],
								'invoice_no'=>$invoice_no,
							   	'customer_name' => $responseDetailArray['billing_name'],
							   	'no_of_adult' => ($sessionBookingData[$order_id]['qtyInputAdult'] +$sessionBookingData[$order_id]['qtyInputChild']),
							 	'no_of_children' => $sessionBookingData[$order_id]['qtyInputInfant'],
								'check_in_date'=>$sessionBookingData[$order_id]['startDate'],
								'check_out_date'=>$sessionBookingData[$order_id]['endDate'],
								'cust_email_telphone'=>$responseDetailArray['billing_tel'],
								'room_dtls'=>$room_dtls,
								'inclusions' => $homestayDetails['Homestay']['inclusions'],
								'amount_to_pay' =>($responseDetailArray['amount']- $agent_commission_amount),  
								'discount' =>$discount_coupon,
								'special_request'=>$sessionBookingData[$order_id]['special_request'],
			 					'agent_commission_amount'=>$agent_commission_amount
								
								
							   )); 
		 $EmailHost->template('booking_confirmation_host',null)
								->emailFormat('html')
								->to($host_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();
		 
		
       ######################################### SUCCESS EMAIL SECTION ENDS ##############################################
            $this->Session->delete("OccupancyDetails"); // deleting the session data of current booking
            $this->Cookie->delete('0.OrderInfo');//deleting the cookie data of current booking.
          //  debug($this->Cookie->read());
            $this->set('order_id',$order_id);
            $this->set('billing_email',$billing_email);
            $this->set('homestay_id',$homestay_id);
            $this->set('customer_email',$responseDetailArray['billing_email']);
        }else{
            $homestay_id=$OccupancyDetail['OccupancyDetail']['homestay_id'];
        }
        
        $seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
				
		$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);

    }
	
	public function checkout_cancel(){
	
        $working_key=Configure::read('working_key');
        $access_code=Configure::read('access_code');
        
        $encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
			
        $rcvdString=$this->Encryption->decrypt($encResponse,$working_key);//Crypto Decryption used as per the specified working key. 
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);
		
        $responseDetailArray=array();        
        foreach($decryptValues as $key=>$value){
            $information=explode('=',$value);
            $responseDetailArray[$information[0]]=$information[1];
        }
        $sessionBookingData=CakeSession::read('OccupancyDetails'); 
        $order_status=$responseDetailArray['order_status'];
        $order_id=$responseDetailArray['order_id'];
        $billing_email=$responseDetailArray['billing_email'];
        $homestay_id=$sessionBookingData[$order_id]['homestay_id'];
        $room_ids=$sessionBookingData[$order_id]['room_ids'];      
        $dataOccupancy=array();
        @$rooms=explode('_',$sessionBookingData[$order_id]['room_id_selected']);
		$invoice_no=$this->General->generateInvoiceNo();
		 
		$room_id_objct_arr=json_decode($room_ids);
		$room_ids=null;$room_id_arr=array(); $oldkeydate=0;
		//this flag use when same room is not available for multiple dates choosing by customer.
		$flagSameRoomAvail="yes";
		foreach($room_id_objct_arr as $room){
			//debug($room);
			foreach($room as $keydate=>$itemsArr){				
				if($oldkeydate==0){
						$oldkeydate=$keydate;
					}elseif($oldkeydate==$keydate){
						$flagSameRoomAvail="yes";
					}elseif($oldkeydate!=$keydate){
						$flagSameRoomAvail="no";
						
					}
				foreach($itemsArr as $key=>$item){					
					//echo "item---->>".$item;
					$freeroom_ids[$keydate][]=$item;
					$room_ids=$room_ids."_".$item;
					array_push($room_id_arr,$item);
				}
			}
		}
		//debug($rooms);
	//	debug($freeroom_ids);
	//die; 
        $OccupancyDetail=$this->OccupancyDetail->find('first',array('conditions'=> array('OccupancyDetail.order_id'=>$order_id), 'fields'=>array('order_id','homestay_id'))); 
         
        if(count($OccupancyDetail)==0){
            foreach($rooms as $key=>$value){
                $dataOccupancy[$key]['OccupancyDetail']['room_price_detail_id']=$value;
                $dataOccupancy[$key]['OccupancyDetail']['order_id']=$order_id;
                $dataOccupancy[$key]['OccupancyDetail']['homestay_id']=$sessionBookingData[$order_id]['homestay_id'];
                $dataOccupancy[$key]['OccupancyDetail']['check_in_date']=$sessionBookingData[$order_id]['startDate'];           
                $dataOccupancy[$key]['OccupancyDetail']['check_out_date']=$sessionBookingData[$order_id]['endDate'];
                $dataOccupancy[$key]['OccupancyDetail']['status']=7;
            }
            debug($dataOccupancy); 
            $this->OccupancyDetail->saveAll($dataOccupancy);
            $lastInsertOccupancyDetailID = $this->OccupancyDetail->getLastInsertID();  
            
            $bookingOrderDetails['BookingOrderDetail']['order_id']=$order_id;
			$bookingOrderDetails['BookingOrderDetail']['user_id']=$this->Session->read('Auth.User.id');
            $bookingOrderDetails['BookingOrderDetail']['order_date']=date('Y-m-d h:i:sa');
            $bookingOrderDetails['BookingOrderDetail']['occupancy_detail_id']=$lastInsertOccupancyDetailID;
            $bookingOrderDetails['BookingOrderDetail']['homestay_id']=$sessionBookingData[$order_id]['homestay_id'];
            $bookingOrderDetails['BookingOrderDetail']['room_price_detail_id']=$sessionBookingData[$order_id]['room_id_selected'];
            $bookingOrderDetails['BookingOrderDetail']['no_of_adults']=$sessionBookingData[$order_id]['qtyInputAdult'];
            $bookingOrderDetails['BookingOrderDetail']['no_of_children']=$sessionBookingData[$order_id]['qtyInputChild'];
            $bookingOrderDetails['BookingOrderDetail']['no_of_infants']=$sessionBookingData[$order_id]['qtyInputInfant'];
            $bookingOrderDetails['BookingOrderDetail']['no_of_days']=$sessionBookingData[$order_id]['no_of_days'];
			
			
			$bookingOrderDetails['BookingOrderDetail']['unit_price']=$sessionBookingData[$order_id]['unit_price'];			
			$bookingOrderDetails['BookingOrderDetail']['total_room_charge']=$sessionBookingData[$order_id]['total_room_charge'];
			$bookingOrderDetails['BookingOrderDetail']['unit_total_per_night']=$sessionBookingData[$order_id]['unit_total_per_night'];
			$bookingOrderDetails['BookingOrderDetail']['cleaning_charges']=$sessionBookingData[$order_id]['cleaning_charges'];
			$bookingOrderDetails['BookingOrderDetail']['applied_coupon']=$sessionBookingData[$order_id]['applied_coupon'];
			$bookingOrderDetails['BookingOrderDetail']['discount_amount']=$sessionBookingData[$order_id]['discount_amount'];
			
            $bookingOrderDetails['BookingOrderDetail']['quantity']=count($rooms);
            $bookingOrderDetails['BookingOrderDetail']['total_price']=$sessionBookingData[$order_id]['grand_total_for_room'];  
			$bookingOrderDetails['BookingOrderDetail']['status']=7;
            
            $this->BookingOrderDetail->saveAll($bookingOrderDetails);            
            $lastInsertBookingOrderDetailID = $this->BookingOrderDetail->getLastInsertID(); 			
            
            $BookingPaymentResponseArr['BookingPaymentResponse']= $responseDetailArray;
			$BookingPaymentResponseArr['BookingPaymentResponse']['invoice_no']=$invoice_no;
            $BookingPaymentResponseArr['BookingPaymentResponse']['booking_order_detail_id']= $lastInsertBookingOrderDetailID; 
            $BookingPaymentResponseArr['BookingPaymentResponse']['homestay_id']= $sessionBookingData[$order_id]['homestay_id'];
			$BookingPaymentResponseArr['BookingPaymentResponse']['user_id']=$this->Session->read('Auth.User.id');
			$BookingPaymentResponseArr['BookingPaymentResponse']['status']=7;
      
            $this->BookingPaymentResponse->saveAll($BookingPaymentResponseArr); 
			$applied_coupon=$sessionBookingData[$order_id]['applied_coupon'];
			if($applied_coupon!=""){
				$this->DiscountCouponCode->query("UPDATE `discount_coupon_codes` SET `status` = '2' WHERE `discount_coupon_codes`.`coupon_code` = '$applied_coupon';");
			}
            
        }else{
			//echo "else";
            $homestay_id=$OccupancyDetail['OccupancyDetail']['homestay_id'];
        }
		
		######################################### EMAIL SECTION BEGINS ####################################################
          // payment cancelled email should fire from here.
		
       ######################################### EMAIL SECTION BEGINS ####################################################
		unset($_SESSION['OccupancyDetails']);
        $this->set('order_id',$order_id);
        $this->set('billing_email',$billing_email);
        $this->set('homestay_id',$homestay_id);
        $this->set('customer_email',$responseDetailArray['billing_email']);
        
        $seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `homestays` WHERE rand() limit 0,1");
		$this->set('meta_title',$seoMetaDtls[0]['homestays']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['homestays']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['homestays']['meta_description']);
		
     }    
    
     public function checkout_response_experience(){
        
        $working_key='8B7C0D424D6ECF825FEA92094F6027B6';	//Working Key should be provided here.
        $encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
        $rcvdString=$this->Encryption->decrypt($encResponse,$working_key);//Crypto Decryption used as per the specified working key. 
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);
        $responseDetailArray=array();        
        foreach($decryptValues as $key=>$value){
            $information=explode('=',$value);
            $responseDetailArray[$information[0]]=$information[1];
        }
        $sessionBookingData=CakeSession::read('BookingExperience'); 
        $order_status=$responseDetailArray['order_status'];
        $order_id=$responseDetailArray['order_id'];
        $billing_email=$responseDetailArray['billing_email'];
        $experience_id=$sessionBookingData[$order_id]['experience_id'];
		$experience_slot_id=$sessionBookingData[$order_id]['experience_slot_id'];
        $invoice_no=$this->General->generateInvoiceNo();
		 
        $dataExperienceBookingDetail=array();        
        $ExperienceBookingDetail=$this->ExperienceBookingDetail->find('first',array('conditions'=> array('ExperienceBookingDetail.order_id'=>$order_id), 'fields'=>array('order_id','experience_id'))); 
         
		// debug($responseDetailArray);
		 
        if(count($ExperienceBookingDetail)==0){
          
               
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['order_id']=$order_id;
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['experience_id']=$experience_id;
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['experience_slot_id']=$experience_slot_id;
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['no_of_person']=$sessionBookingData[$order_id]['no_of_guest'];         
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['order_date']=date('Y-m-d');
			$dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['slot_date']=$sessionBookingData[$order_id]['date'];
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['unit_price']=$sessionBookingData[$order_id]['unit_price'];
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['total_price']=$sessionBookingData[$order_id]['grand_total'];
			$dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['special_request']=$sessionBookingData[$order_id]['special_request'];
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['status']=14;
            
           // debug($dataExperienceBookingDetail); 
            $this->ExperienceBookingDetail->saveAll($dataExperienceBookingDetail);
            $lastInsertExprBookingDetailID = $this->ExperienceBookingDetail->getLastInsertID();  
           
            
            
            
        }else{
            $experience_id=$ExperienceBookingDetail['ExperienceBookingDetail']['experience_id'];
        }
        
		######################################### EMAIL SECTION BEGINS ####################################################
        // Email with invoice should send from here... 
		// echo implode(',',$rooms);
		 $conditions=array("Experience.id "=>$sessionBookingData[$order_id]['experience_id'],'ExperienceSlot.id'=>$sessionBookingData[$order_id]['experience_slot_id']);
		 $this->Experience->recursive=2;
         $this->Experience->unbindModel(array('hasMany' => array('ExperienceSlot')));
         $this->ExperienceFile->unbindModel(array('belongsTo' => array('Experience')));
         $this->ExperienceLanguage->unbindModel(array('belongsTo' => array('Experience')));
        // $this->ExperienceSlot->unbindModel(array('belongsTo' => array('Experience')));    
         $experienceDetails=$this->Experience->find('first', array(
			'joins' => array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),                
                array(
					'table' => 'experience_booking_details',
					'alias' => 'ExperienceBookingDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						"Experience.id = ExperienceBookingDetail.experience_id "
					)
				),
				array(
					'table' => 'experience_slots',
					'alias' => 'ExperienceSlot',
					'type'  => 'LEFT',
					'conditions' => array(
						"Experience.id = ExperienceSlot.experience_id "
					)
				),

			),
            'conditions'=>$conditions,
			'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path', 'ExperienceLanguage.*', 'ExperienceType.*','ExperienceSlot.*'),
             
            'group' => '`Experience`.`id`',
			'order' => 'Experience.created DESC'
		));
        
		 @$agent_commission_percentage=$homestayDetails['Experience']['agent_commission'];
		 if($agent_commission_percentage==""){
			 $agent_commission_percentage=Configure::read('agent_commission_percentage'); 
		 }
		 $BookingPaymentResponseArr['BookingPaymentResponse']= $responseDetailArray;
         $BookingPaymentResponseArr['BookingPaymentResponse']['experience_booking_detail_id']= $lastInsertExprBookingDetailID; 
         $BookingPaymentResponseArr['BookingPaymentResponse']['experience_id']= $sessionBookingData[$order_id]['experience_id'];
		 $BookingPaymentResponseArr['BookingPaymentResponse']['agent_commission']= $agent_commission_percentage;
		 $BookingPaymentResponseArr['BookingPaymentResponse']['user_id']=$this->Session->read('Auth.User.id');
		 $BookingPaymentResponseArr['BookingPaymentResponse']['status']=14;
           // debug($BookingPaymentResponseArr);
         $this->BookingPaymentResponse->saveAll($BookingPaymentResponseArr);  

		 $host_name=$experienceDetails['Experience']['hosted_by'];
		 
		 $discount_coupon="";
		 if($sessionBookingData[$order_id]['applied_coupon']!="")
		 $discount_coupon=$sessionBookingData[$order_id]['applied_coupon']." - INR ".$sessionBookingData[$order_id]['discount_amount'];
		
		 //google maps with api key querying with latitude and longitude
		 $latitude=$experienceDetails['Experience']['latitude'];
		 $longitude=$experienceDetails['Experience']['longitude'];
		 $google_maps="https://www.maps.google.com/?&q=$latitude,$longitude";
		 
		 $subject="Booking Confirmed at ".$experienceDetails['Experience']['name']." from ".$sessionBookingData[$order_id]['date'];
		  
		 $customer_email=$sessionBookingData[$order_id]['billing_email'];
		 $host_email=$experienceDetails['Experience']['host_contact_email'];
		 
		 $agent_commission_amount=(($sessionBookingData[$order_id]['grand_total'] * $agent_commission_percentage)/ 100);
		 
		 $time1 = strtotime($experienceDetails['ExperienceSlot']['slot_start_time']);
		 $time2 = strtotime($experienceDetails['ExperienceSlot']['slot_end_time']);
		 $duration = round(abs($time2 - $time1) / 3600,2);		
		
		 //Email to customer on confirmed booking....
		 $EmailCustomer = new CakeEmail();	
		 $EmailCustomer->config('gmail');
		 $EmailCustomer->config(array('from' => 'coorgexpressbooking@gmail.com'));
		 $EmailCustomer->viewVars(array(
			 					'experience_name' => $experienceDetails['Experience']['name'],
								'invoice_no'=>$invoice_no,
							   	'customer_name' => $responseDetailArray['billing_name'],
							   	'no_of_adult' => $sessionBookingData[$order_id]['no_of_guest'] ,							 	
								'trip_start_date'=>$sessionBookingData[$order_id]['date'],
								'slot_start_time'=>$sessionBookingData[$order_id]['slot_start_time'],
			 					'slot_end_time'=>$sessionBookingData[$order_id]['slot_end_time'],
			 					'address'=>$experienceDetails['Experience']['address'],
								'cust_email_telphone'=>$responseDetailArray['billing_tel'],
			 					'duration' => $duration." hours ",
								'inclusion' => $experienceDetails['Experience']['includes_in'],
								'amount_paid' =>$sessionBookingData[$order_id]['grand_total'],  
								'discount' =>$discount_coupon,
								'special_request'=>$sessionBookingData[$order_id]['special_request'],
								'host_name'=>$host_name,
								'host_telephone'=>$experienceDetails['Experience']['host_contact_no'],
			 					'google_maps'=>$google_maps,
                                'where_we_meet'=>$experienceDetails['Experience']['where_we_meet'],
								
							   )); 
		 $EmailCustomer->template('booking_experience_confirmation_customer',null)
								->emailFormat('html')
								->to($customer_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();

		 
		 ///Email to host on confirmed booking...		 
		 $EmailHost = new CakeEmail();	
		 $EmailHost->config('gmail');
		 $EmailHost->config(array('from' => Configure::read('smtp_from_email')));
		 $EmailHost->viewVars(array(
			 					'experience_name' => $experienceDetails['Experience']['name'],
								'invoice_no'=>$invoice_no,
							   	'customer_name' => $responseDetailArray['billing_name'],
							   	'no_of_adult' => $sessionBookingData[$order_id]['no_of_guest'] ,							 	
								'trip_start_date'=>$sessionBookingData[$order_id]['date'],
								'slot_start_time'=>$sessionBookingData[$order_id]['slot_start_time'],
								'cust_email_telphone'=>$responseDetailArray['billing_tel'],
								'inclusion' => $experienceDetails['Experience']['includes_in'],
								'amount_paid' =>$sessionBookingData[$order_id]['grand_total'],  
								'discount' =>$discount_coupon,
								'special_request'=>$sessionBookingData[$order_id]['special_request'],
			 					'agent_commission_amount'=>$agent_commission_amount
								
								
							   )); 
		 $EmailHost->template('booking_experience_confirmation_host',null)
								->emailFormat('html')
								->to($host_email) 
			 					->cc(Configure::read('booking_email_cc'))
								->from(Configure::read('smtp_from_email'))
			 					->subject($subject)
								->send();
		 
		
       ######################################### EMAIL SECTION ENDS ####################################################
		
        
		unset($_SESSION['BookingExperience']);
        $this->set('order_id',$order_id);
        $this->set('billing_email',$billing_email);
        $this->set('experience_id',$experience_id);		
		$this->set('experience_slot_id',$experience_slot_id);
		$this->set('customer_email',$responseDetailArray['billing_email']);
         
         $seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `experiences` WHERE rand() limit 0,1");
		$this->set('experience_type_id',$experience_type_id);
		$this->set('meta_title',$seoMetaDtls[0]['experiences']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['experiences']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['experiences']['meta_description']);
     }
    
    public function checkout_cancel_experience(){
        
        $working_key='8B7C0D424D6ECF825FEA92094F6027B6';	//Working Key should be provided here.
        $encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
        $rcvdString=$this->Encryption->decrypt($encResponse,$working_key);//Crypto Decryption used as per the specified working key. 
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);
        $responseDetailArray=array();        
        foreach($decryptValues as $key=>$value){
            $information=explode('=',$value);
            $responseDetailArray[$information[0]]=$information[1];
        }
        $sessionBookingData=CakeSession::read('BookingExperience'); 
        $order_status=$responseDetailArray['order_status'];
        $order_id=$responseDetailArray['order_id'];
        $billing_email=$responseDetailArray['billing_email'];
        $experience_id=$sessionBookingData[$order_id]['experience_id'];
              
        $dataExperienceBookingDetail=array();        
        $ExperienceBookingDetail=$this->ExperienceBookingDetail->find('first',array('conditions'=> array('ExperienceBookingDetail.order_id'=>$order_id), 'fields'=>array('order_id','experience_id'))); 
         
        if(count($ExperienceBookingDetail)==0){        
               
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['order_id']=$order_id;
			$dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['user_id']=$this->Session->read('Auth.User.id');
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['experience_id']=$sessionBookingData[$order_id]['experience_id'];
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['experience_slot_id']=$sessionBookingData[$order_id]['experience_slot_id'];
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['no_of_person']=$sessionBookingData[$order_id]['no_of_guest'];         
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['order_date']=date('Y-m-d');
			$dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['slot_date']=$sessionBookingData[$order_id]['date'];
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['unit_price']=$sessionBookingData[$order_id]['unit_price'];
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['total_price']=$sessionBookingData[$order_id]['grand_total'];
            $dataExperienceBookingDetail[$key]['ExperienceBookingDetail']['status']=7; //customer booked
            
            //debug($dataExperienceBookingDetail); 
           $this->ExperienceBookingDetail->saveAll($dataExperienceBookingDetail);
           $lastInsertExprBookingDetailID = $this->ExperienceBookingDetail->getLastInsertID();             
            
            $BookingPaymentResponseArr['BookingPaymentResponse']= $responseDetailArray;
            $BookingPaymentResponseArr['BookingPaymentResponse']['experience_booking_detail_id']= $lastInsertExprBookingDetailID; 
            $BookingPaymentResponseArr['BookingPaymentResponse']['experience_id']= $sessionBookingData[$order_id]['experience_id'];
			$BookingPaymentResponseArr['BookingPaymentResponse']['user_id']=$this->Session->read('Auth.User.id');
			$BookingPaymentResponseArr['BookingPaymentResponse']['status']=7;
           // debug($BookingPaymentResponseArr);
            $this->BookingPaymentResponse->saveAll($BookingPaymentResponseArr);  
            
        }else{
            $experience_id=$ExperienceBookingDetail['ExperienceBookingDetail']['experience_id'];
        }
        // Email with invoice should send from here... 
		
		unset($_SESSION['BookingExperience']);
        $this->set('order_id',$order_id);
        $this->set('billing_email',$billing_email);
        $this->set('experience_id',$experience_id);
		$this->set('customer_email',$responseDetailArray['billing_email']);
        
        $seoMetaDtls=$this->Homestay->query("SELECT meta_title,meta_keyword,meta_description FROM `experiences` WHERE rand() limit 0,1");
		$this->set('experience_type_id',$experience_type_id);
		$this->set('meta_title',$seoMetaDtls[0]['experiences']['meta_title']);
		$this->set('meta_keyword',$seoMetaDtls[0]['experiences']['meta_keyword']);
		$this->set('meta_description',$seoMetaDtls[0]['experiences']['meta_description']);
     }
     
     public function booking_invoice($order_id_enc=null,$homestay_id_enc=null){
         $this->layout=false; 
         $order_id=base64_decode($order_id_enc);
         $homestay_id=base64_decode($homestay_id_enc);
        $OccupancyDetailArr=$this->OccupancyDetail->find('first',array('conditions'=> array('OccupancyDetail.order_id'=>$order_id), 'fields'=>array('OccupancyDetail.*')));
        $HomestayDetailArr=$this->Homestay->find('first',array(
            'joins' => array(
				array(
					'table' => 'room_price_details',
					'alias' => 'RoomPriceDetail',
					'type' => 'LEFT',
					'conditions' => array(
						'Homestay.id = RoomPriceDetail.homestay_id'
					)
				),  
                array(
					'table' => 'locations',
					'alias' => 'Location',
					'type' => 'INNER',
					'conditions' => array(
						'Location.id = Homestay.location_id'
					)
				),

			),
            'conditions'=> array('Homestay.id'=>$homestay_id),
            'fields'=>array('Homestay.*','RoomPriceDetail.*','Location.name')));
       // debug($productOrderDetailArr);
      //  debug($paymentResponseDetailArr);
        $BookingOrderDetailArr=$this->BookingOrderDetail->find('all',array(
            'joins' => array(
				array(
					'table' => 'booking_payment_responses',
					'alias' => 'BookingPaymentResponse',
					'type' => 'LEFT',
					'conditions' => array(
						'BookingOrderDetail.id = BookingPaymentResponse.booking_order_detail_id'
					)
				),               

			),
            'conditions'=> array('BookingOrderDetail.order_id'=>$order_id),
            'fields'=>array('BookingOrderDetail.*','BookingPaymentResponse.*')));
        
        $this->set('OccupancyDetailArr',$OccupancyDetailArr);
        $this->set('HomestayDetailArr',$HomestayDetailArr);
        $this->set('BookingOrderDetailArr',$BookingOrderDetailArr);
    }
	
	 public function booking_invoice_experience($order_id_enc=null,$experience_id_enc=null,$experience_slot_id_enc=null){
         $this->layout=false; 
		 $order_id=base64_decode($order_id_enc);
         $experience_id=base64_decode($experience_id_enc);
		 $experience_slot_id_enc=base64_decode($experience_slot_id_enc);
		 $conditions=array("Experience.id "=>$experience_id,'ExperienceBookingDetail.experience_slot_id'=> $experience_slot_id_enc);
		 $this->Experience->recursive=2;
         $this->Experience->unbindModel(array('hasMany' => array('ExperienceSlot')));
         $this->ExperienceFile->unbindModel(array('belongsTo' => array('Experience')));
         $this->ExperienceLanguage->unbindModel(array('belongsTo' => array('Experience')));
         $experienceDetails=$this->Experience->find('first', array(
			'joins' => array(
				array(
					'table' => 'experience_files',
					'alias' => 'ExperienceFile',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceFile.experience_id'
					)
				),
                array(
					'table' => 'experience_languages',
					'alias' => 'ExperienceLanguage',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.id = ExperienceLanguage.experience_id'
					)
				),
                array(
					'table' => 'experience_types',
					'alias' => 'ExperienceType',
					'type'  => 'LEFT',
					'conditions' => array(
						'Experience.experience_type = ExperienceType.id'
					)
				),                
                array(
					'table' => 'experience_booking_details',
					'alias' => 'ExperienceBookingDetail',
					'type'  => 'LEFT',
					'conditions' => array(
						"Experience.id = ExperienceBookingDetail.experience_id "
					)
				),
				array(
					'table' => 'experience_slots',
					'alias' => 'ExperienceSlot',
					'type'  => 'LEFT',
					'conditions' => array(
						"Experience.id = ExperienceSlot.experience_id "
					)
				),

			),
            'conditions'=>$conditions,
			'fields' => array('Experience.*','ExperienceFile.file_name','ExperienceFile.file_path', 'ExperienceLanguage.*', 'ExperienceType.*','ExperienceSlot.*'),             
            'group' => '`Experience`.`id`',
			'order' => 'Experience.created DESC'
		));
		$BookingOrderDetailArr=$this->ExperienceBookingDetail->find('all',array(
            'joins' => array(
				array(
					'table' => 'booking_payment_responses',
					'alias' => 'BookingPaymentResponse',
					'type' => 'LEFT',
					'conditions' => array(
						'ExperienceBookingDetail.id = BookingPaymentResponse.booking_order_detail_id'
					)
				),               

			),
            'conditions'=> array('ExperienceBookingDetail.order_id'=>$order_id),
            'fields'=>array('ExperienceBookingDetail.*','BookingPaymentResponse.*'))); 
		 
		 $experienceBookingDetail=$this->ExperienceBookingDetail->find('first',array('conditions'=> array('ExperienceBookingDetail.order_id'=>$order_id), 'fields'=>array('ExperienceBookingDetail.*'))); 
		// debug($BookingOrderDetailArr);
		// debug($experienceBookingDetail);
        $this->set('experienceDetails',$experienceDetails);
        $this->set('experienceBookingDetail',$experienceBookingDetail);
		$this->set('BookingOrderDetailArr',$BookingOrderDetailArr);
       
	 }
}
?>
