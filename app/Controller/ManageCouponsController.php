<?php

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageCouponsController extends AppController {

	var $layout ="default";
	public $uses = array('User','Homestay','HomestayFile','City','MasterBedType','RoomPriceDetail','BedroomDetail','OccupancyDetail', 'Amenity','HomestayAmenity','MasterBedType','PropertyType','RoomType','BookingOrderDetail','BookingPaymentResponse','BookingEnquiry','DiscountCouponCode');

	public function beforeFilter()
	{
        parent::beforeFilter();
        //$this->Auth->allow();
	}
    public function coupon_list(){ 
		
    }
    
    
        public function ajax_coupon_list(){ 
              
             $conditions=array();
             $couponDetails=$this->DiscountCouponCode->find('all', array(			
            'conditions'=>$conditions,
			'fields' => array('DiscountCouponCode.*'),           
			'order' => 'DiscountCouponCode.created DESC'
		));
		
            $list_view_array = array();
                
                foreach($couponDetails as $key=>$product_data){
                    
                    if($product_data['DiscountCouponCode']['status']==""){
                        $status_code="Enable";
                        $status = 'Disabled';
                    }elseif($product_data['DiscountCouponCode']['status']==1){
                        $status_code="Disable";
                        $status = 'Active';
                    }elseif($product_data['DiscountCouponCode']['status']==2){
                        $status_code="Enable";
                        $status = 'Disabled';
                    }else{
                        $status_code="Enable";
                        $status = 'Disabled';
                    }
                    
                    $list_view_array['data'][$key]['coupon_code'] = $product_data['DiscountCouponCode']['coupon_code'];
                    $list_view_array['data'][$key]['description'] = $product_data['DiscountCouponCode']['description'];
                    $list_view_array['data'][$key]['discount_type'] = $product_data['DiscountCouponCode']['discount_type'];
                    $list_view_array['data'][$key]['value'] = $product_data['DiscountCouponCode']['value'];
                    $list_view_array['data'][$key]['expiry_date'] = $product_data['DiscountCouponCode']['expiry_date'];
                    $list_view_array['data'][$key]['status'] = $status;
                    $list_view_array['data'][$key]['actions'] = "<a href='".Configure::read('app_root_path')."manage_coupons/updateStatus/".$product_data['DiscountCouponCode']['id']."/".$status_code."'>".$status_code."</a>";                
                    
                    }
                
                echo json_encode($list_view_array);
                
                exit; 
             

    }
    
	public function add(){
		
	}
	public function save(){
		debug($_REQUEST);
		$data=$_REQUEST['data']['Product'];
		$this->DiscountCouponCode->saveAll($data);
		$this->redirect(array('controller'=>'manage_coupons','action'=>'coupon_list'));
	}
	public function updateStatus($coupon_id=null,$status_code=null){
		$this->autoRender=false;  
		if($status_code=='Enable'){
			$status_id=1;
		}else{
			$status_id=2;
		}
		$couponStatusArr['DiscountCouponCode']['id'] =$coupon_id;
        $couponStatusArr['DiscountCouponCode']['status'] =$status_id;                
        $this->DiscountCouponCode->saveAll($couponStatusArr);
		$this->redirect(array('controller'=>'manage_coupons','action'=>'coupon_list'));
	}
   
    public function checkCouponAvailability($coupon_code=null){		
		$this->autoRender=false;// ajax method . 
//		$couponData=json_decode($_REQUEST['couponDataJson']);    
//		$coupon_code=$couponData->coupon_code;
//		$this->DiscountCouponCode->recursive=-1;
		$result=$this->DiscountCouponCode->query("SELECT * FROM `discount_coupon_codes` WHERE `coupon_code`='$coupon_code'");
		
		if(count($result)==0 && !isset($result[0]['discount_coupon_codes'])){
			return true;
		}else{
			return false;
		}	
		
	 }
	
 	public function couponGenerator($coupon_code=null){
		$this->autoRender=false;// inside call		
		$coupon_code=$this->random_string();		
		if($this->checkCouponAvailability($coupon_code)){
			return $coupon_code;
		}else{
			$this->couponGenerator();
		}
	}
	
	public function random_string()
	{
		$character_set_array = array();
		$character_set_array[] = array('count' => 4, 'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$character_set_array[] = array('count' => 3, 'characters' => '0123456789');
		$temp_array = array();
		foreach ($character_set_array as $character_set) {
			for ($i = 0; $i < $character_set['count']; $i++) {
				$temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
			}
		}
		shuffle($temp_array);
		return $datastring= implode('', $temp_array);
		 
	}
   
}
?>